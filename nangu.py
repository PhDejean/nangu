#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import sys, os

import threading
import psutil
from timeit import default_timer as timer
import platform
from datetime import datetime
from csv import DictWriter


if sys.stdout.encoding is None:
    os.putenv("PYTHONIOENCODING",'UTF-8')
    os.execv(sys.executable,['python']+sys.argv)

import nngsrc.nng
import nngsrc.services

#-----------------------------------------------------------------------------------
def profile(func,param=None):
    output = dict()
    output["Platforme"] = platform.version()
    output["Machine"] = platform.machine()
    output["Systeme"] = platform.system()
    output["Commande"] = ' '.join(param)
    output["Fonction"] = func.__name__
    py = psutil.Process(os.getpid())
    output["Lancement"] = datetime.fromtimestamp(py.create_time())
    start = timer()
    output["Retour"]=func(param)
    output["Durée"] = "%.2f s"%(timer()-start)
#    output["Nb. de threads actifs"] = threading.active_count()
    output["RAM"]=str(round(psutil.virtual_memory().total / (1024.0 **3)))+" GB"
    output["Memoire"] = "%.2f %%"%py.memory_percent()
    output["Memory Usage (Resident Set Size)"] = str(round(py.memory_info()[0] / (1024.0 **2)))+" MB"
    output["Memory Usage shared"] = py.memory_info()
    output["Cores"] = psutil.cpu_count()
    output["Cpu"] = "%.2f %%"%psutil.cpu_percent()
    output["Memory usage (Unique Set Size)"] = str(round(py.memory_full_info().uss / (1024.0 **2)))+" MB"
    print("-----------------------------------------------------------")
    print(" PROFILAGE")
    print("-----------------------------------------------------------")
    for i in ["Platforme","Machine","Systeme","Cores","RAM"]:
      print("%12s :"%i,output[i])
    print("-----------------------------------------------------------")
    for i in ["Commande","Lancement","Durée"]:
      print("%12s :"%i,output[i])
    print("-----------------------------------------------------------")
    for i in ["Memoire","Cpu"]:
      print("%12s :"%i,output[i])
    print("-----------------------------------------------------------")
    return output["Retour"]
#-----------------------------------------------------------------------------------
# Main general pour le noyau Nangu
if __name__ == "__main__":
#  codeRetour=profile(nngsrc.nng.LancementNangu,sys.argv)
  codeRetour=nngsrc.nng.LancementNangu(sys.argv)
  sys.exit(codeRetour)
#-----------------------------------------------------------------------------------
# Fin
