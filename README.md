# NANGU


**NANGU** est un outil logiciel couvrant quasiment tous les besoins de validation. Il permet :

 * La rédaction de plans de validation,

 * La rédaction de cahiers de validation pour des déroulements de tests sur des documents papier,

 * Le déroulement de phase de validation et la rédaction des rapports :

    * Validation automatique totalement instrumentée,
    * Validation manuelle assistée,
    * Validation mixte.

 * Le suivi et la rédaction des bilans de campagnes de validation,

 * La gestion des exigences et des fonctionnalités,

 * La génération des matrices de couvertures,

 * La gestion des faits techniques.




La documentation de NANGU se trouve dans le répertoire `ressources`.

---

*Le logo de Nangu a été désigné en utilisant https://pluspng.com/circle-png-1291.html*
