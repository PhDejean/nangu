#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# TUI : Ph Dejean 09.2018
#===============================================================================
#
# ------------------------------------------------
#  Chargement de l'environnement
# ------------------------------------------------
import json
import os,sys,time,copy

CLAVIER=False
TUIVERSION="3.0"

try:
  from colorama import *
  init(autoreset=True)
  COLORAMA=True
except:
  print("RESTRICTION : Module 'Colorama' non installe")
  print("              => Les affichages ne seront pas mis en couleur.")
  COLORAMA=False

if COLORAMA:
  fRED     = Fore.RED
  fGREEN   = Fore.GREEN
  fYELLOW  = Fore.YELLOW
  fWHITE   = Fore.WHITE
  fBLUE    = Fore.BLUE
  fMAGENTA = Fore.MAGENTA
  fCYAN    = Fore.CYAN
  fBLACK   = Fore.BLACK

  fL_BLACK   = Fore.LIGHTBLACK_EX
  fL_BLUE    = Fore.LIGHTBLUE_EX
  fL_CYAN    = Fore.LIGHTCYAN_EX
  fL_GREEN   = Fore.LIGHTGREEN_EX
  fL_MAGENTA = Fore.LIGHTMAGENTA_EX
  fL_RED     = Fore.LIGHTRED_EX
  fL_WHITE   = Fore.LIGHTWHITE_EX
  fL_YELLOW  = Fore.LIGHTYELLOW_EX

  bRED     = Back.RED
  bGREEN   = Back.GREEN
  bYELLOW  = Back.YELLOW
  bWHITE   = Back.WHITE
  bBLUE    = Back.BLUE
  bMAGENTA = Back.MAGENTA
  bCYAN    = Back.CYAN
  bBLACK   = Back.BLACK

  bL_BLACK   = Back.LIGHTBLACK_EX
  bL_BLUE    = Back.LIGHTBLUE_EX
  bL_CYAN    = Back.LIGHTCYAN_EX
  bL_GREEN   = Back.LIGHTGREEN_EX
  bL_MAGENTA = Back.LIGHTMAGENTA_EX
  bL_RED     = Back.LIGHTRED_EX
  bL_WHITE   = Back.LIGHTWHITE_EX
  bL_YELLOW  = Back.LIGHTYELLOW_EX

  sBRIGHT  = Style.BRIGHT
  sDIM     = Style.DIM
  sNORMAL  = Style.NORMAL
  sRESET   = Style.RESET_ALL
else:
  fRED     = ""
  fGREEN   = ""
  fYELLOW  = ""
  fWHITE   = ""
  fBLUE    = ""
  fMAGENTA = ""
  fCYAN    = ""
  fBLACK   = ""

  fL_BLACK   = ""
  fL_BLUE    = ""
  fL_CYAN    = ""
  fL_GREEN   = ""
  fL_MAGENTA = ""
  fL_RED     = ""
  fL_WHITE   = ""
  fL_YELLOW  = ""

  bRED     = ""
  bGREEN   = ""
  bYELLOW  = ""
  bWHITE   = ""
  bBLUE    = ""
  bMAGENTA = ""
  bCYAN    = ""
  bBLACK   = ""

  bL_BLACK   = ""
  bL_BLUE    = ""
  bL_CYAN    = ""
  bL_GREEN   = ""
  bL_MAGENTA = ""
  bL_RED     = ""
  bL_WHITE   = ""
  bL_YELLOW  = ""

  sBRIGHT  = ""
  sDIM     = ""
  sNORMAL  = ""
  sRESET   = ""

version=1.0
#---------------------------------------------------------------------------------
def is_digit(n):
  try:
      float(n)
      return True
  except ValueError:
      return  False
#---------------------------------------------------------------------------------
def TUIprint(pMessage,pType="Normal"):
    if pType=="Normal":
        print(fWHITE+pMessage,flush=True)
    elif pType=="Error":
        print(fRED+"%s> %s"%(pType,pMessage),flush=True)
    elif pType=="Warning":
        print(fYELLOW+"%s> %s"%(pType,pMessage),flush=True)
    elif pType=="Action":
        print(fGREEN+pMessage,flush=True)
    elif pType=="Config":
        print(fMAGENTA+pMessage,flush=True)
    elif pType=="Select":
        print(fBLUE+pMessage,flush=True)
    else:
        print(fRED,"type '%s' inconnu, message '%s'"%(pType,pMessage),flush=True)

#---------------------------------------------------------------------------------
class TUI:
  def __init__(self,pTUI,debug):
      self.debug=debug
      self.appliTUI=pTUI
      self.largeur=pTUI['LARGEUR']
      if self.largeur%2==1: self.largeur+=1
      self.bordure=2
      self.marge=pTUI['MARGE']*2
      self.espace=self.largeur-self.bordure-self.marge
      self.palette={}
      self._fixePalette(pTUI['PALETTE'])
      self.actions={}
      self.actionWidget={}
      self.widgets={}       # [id]=element
      self.groupes={}       # [nomGroupe]=[id,...]
      self.table={}         # [ligne]=[id,...]
      self.maxLignes=0
      self.indexWidgets={}  # [nom]=(ligne,colonne)
      self.tableDesBlocs={}
      self.version=pTUI['VERSION']
      self.listeCommandes=[]
      self.affichageTUI=True
      self.quitter=pTUI['QUITTER']
      numObjet=0
      if self.debug:
          print(">Largeur :",self.largeur)
          print(">Bordure :",self.bordure)
          print(">Marge   :",self.marge)
          print(">Espace  :",self.espace)
      numId=0
      # Boucle sur les lignes d'elements
      for ligne in range(len(self.appliTUI['ELEMENTS'])):
          ligneElement=[]
          # Pour chaque ligne de l'UI, boucle sur les elements
          for numElem in range(len(self.appliTUI['ELEMENTS'][ligne])):
              element=copy.deepcopy(self.appliTUI['ELEMENTS'][ligne][numElem])
              element['ID']=numId
              numId+=1
              element['SPLDEP']=True
              # On valide l'activité
              if not 'ACTIF' in element:
                  element['ACTIF']=True
              # On configure des éléments manquants
              if element['TYPE']=='CBX':
                  element['DEPLOIE']=False
              if element['TYPE']=='SEP':
                  if 'LARG' not in element:
                      element['LARG']=self.espace
              if 'ACTION' not in element:
                  element['ACTION']=None
              if 'LARG' not in element:
                  element['LARG']=0
              if 'TEXTE' not in element:
                  element['TEXTE']=""
              # Si l'alignement n'est pas défini : Gauche
              if not 'ALIGN' in element:
                  if element['TYPE'] in ["EDT","SPB"]:
                      element['ALIGN']='D'
                  elif element['TYPE'] in ["BTN"]:
                      element['ALIGN']='C'
                  else:
                      element['ALIGN']='G'
              if not 'OFFSET' in element:
                  element['OFFSET']=0
              # traitement du nom déclaré
              if 'NOM' not in element:
                  if element['ACTION'] is not None:
                      element['NOM']="%s%s"%(element['TYPE'],element['ACTION'])
                  else:
                      element['NOM']="tuiOBJET%d"%numObjet
                      numObjet+=1
              # traitement de l'action déclarée
              if 'ACTION' in element:
                  if element['ACTION'] is not None:
                      if element['ACTION'] in self.actions and self.actions[element['ACTION']]!=element['NOM']:
                          print(fRED+"/!\ Doublon d'action '%s' pour '%s' !"%(element['ACTION'],element['NOM']))
                      else:
                          self.actions[element['ACTION']]=element['NOM']
                  self.actionWidget[element['ACTION']]=element['ID']
              self.indexWidgets[element['NOM']]=element['ID']
              # traitement de la visibilité du widget
              if not 'VISIBLE' in element:
                  element['VISIBLE']=True
              # Groupes
              if 'GROUP' in element:
                  if element['GROUP'] not in self.groupes:
                      self.groupes[element['GROUP']]=[element['ID']]
                  else:
                      self.groupes[element['GROUP']].append(element['ID'])
              # enregistrement du widget dans la ligne
              ligneElement.append(element)
          # Sauvegarde...
          self.table[self.maxLignes]=[]
          self.maxLignes+=1
          for element in ligneElement:
              self.widgets[element['ID']]=element
              self.table[self.maxLignes-1].append(element['ID'])

  #---------------------------------------------------------------------------
  def TUI_ChargerCommandes(self,pFichierCommandes):
      if not os.path.exists(pFichierCommandes):
          print("Le fichier de commandes '%s' n'existe pas."%pFichierCommandes)
          return
      f=open(pFichierCommandes)
      commandes=f.readlines()
      f.close()
      self.listeCommandes=[]
      for ligne in commandes:
          if ligne[0]!=';':
              ligne=ligne.replace('\n','').split(' ')
              for cmd in ligne:
                  self.listeCommandes.append(cmd)
      self.affichageTUI=False

  #---------------------------------------------------------------------------------
  def TUI_ExtraitCommande(self):
    return self.listeCommandes.pop(0).split(':')

  #---------------------------------------------------------------------------------
  def TUI_ChargeConfig(self):
    if not os.path.exists("TUI_%s.cfg"%self.appliTUI['TITRE']): return
    f=open("TUI_%s.cfg"%self.appliTUI['TITRE'],"r")
    contenu=f.readlines()
    f.close()
    for var in self.appliTUI['VARIABLES'].keys():
      for ligne in contenu:
        ligne=ligne.replace('\n','').replace('\t','').split(':')
        if var==ligne[0]:
          self.appliTUI['VARIABLES'][var]=':'.join(ligne[1:])
          break
    for nom in self.indexWidgets:
      for ligne in contenu:
        ligne=ligne.replace('\n','').replace('\t','').split(':')
        if nom==ligne[0]:
          idObjet=self.indexWidgets[nom]
          objet=self.widgets[idObjet]
          objet['VISIBLE']=ligne[1]=="True"
          objet['ACTIF']=ligne[2]=="True"
          objet['SPLDEP']=ligne[3]=="True"
          break

  #---------------------------------------------------------------------------------
  def TUI_SauvegardeConfig(self):
      f=open("TUI_%s.cfg"%self.appliTUI['TITRE'],"w")
      for var in self.appliTUI['VARIABLES'].keys():
          f.write("%s:%s\n"%(var,self.appliTUI['VARIABLES'][var]))
      for nom in self.indexWidgets:
          idObjet=self.indexWidgets[nom]
          objet=self.widgets[idObjet]
          f.write("%s:%s:%s:%s\n"%(nom,objet['VISIBLE'],objet['ACTIF'],objet['SPLDEP']))
      f.close()
      f=open("TUI_%s.pal"%self.appliTUI['TITRE'],"w")
      json.dump(self.palette,f)
      f.close()

  #---------------------------------------------------------------------------------
  def _remplace_variable(self,elem):
      if type(elem)==int: return elem
      elif type(elem)==list:
          for i in range(len(elem)):
              elem[i]=self._remplace_variable(elem[i])
      else:
          for var in self.appliTUI['VARIABLES'].keys():
              Rvar="@%s@"%var
              #print("var:",var)
              elem=elem.replace(Rvar,self.appliTUI['VARIABLES'][var])
      return elem

  #---------------------------------------------------------------------------------
  def TUI_setVariable(self,variable,valeur):
    self.appliTUI['VARIABLES'][variable]=str(valeur)

  #---------------------------------------------------------------------------------
  def TUI_getVariable(self,variable):
    return self.appliTUI['VARIABLES'][variable]

  #---------------------------------------------------------------------------------
  def _fixePalette(self,nom):
    if nom is None:
      self.palette={'Titre'  :None,
                    'Action' :None,
                    'Bordure':None,
                    'Normal' :None,
                    'Onglet' :None,
                    'Bouton' :None,
                    'Editeur':None,
                    'Decorateur':None,
                    'Selection':None
                    }
    elif nom=='defaut' or nom=='NoirBlanc':
      self.palette={'Titre'     :(bWHITE,fBLACK,sBRIGHT),
                    'Action'    :(bBLACK,fL_WHITE,sBRIGHT),
                    'Bordure'   :(bWHITE,fBLACK,sDIM),
                    'Normal'    :(bBLACK,fWHITE,sNORMAL),
                    'Onglet'    :(bWHITE,fBLACK,sNORMAL),
                    'Bouton'    :(bWHITE,fBLACK,sNORMAL),
                    'Editeur'   :(bWHITE,fBLACK,sNORMAL),
                    'Options'   :(bWHITE,fBLACK,sNORMAL),
                    'Decorateur':(bWHITE,fBLACK,sNORMAL),
                    'Selection' :(bWHITE,fBLACK,sBRIGHT)
                    }
    elif nom=='Jaune':
      self.palette={'Titre'     :(bYELLOW,fBLACK,sBRIGHT),
                    'Action'    :(bBLACK,fL_YELLOW,sBRIGHT),
                    'Bordure'   :(bYELLOW,fBLACK,sDIM),
                    'Normal'    :(bBLACK,fYELLOW,sNORMAL),
                    'Onglet'    :(bYELLOW,fBLACK,sNORMAL),
                    'Bouton'    :(bYELLOW,fBLACK,sNORMAL),
                    'Editeur'   :(bYELLOW,fBLACK,sNORMAL),
                    'Options'   :(bYELLOW,fBLACK,sNORMAL),
                    'Decorateur':(bYELLOW,fBLACK,sNORMAL),
                    'Selection' :(bYELLOW,fBLACK,sBRIGHT)
                    }
    elif nom=='Bleu':
      self.palette={'Titre'     :(bBLUE,fBLACK,sBRIGHT),
                    'Action'    :(bBLACK,fL_BLUE,sBRIGHT),
                    'Bordure'   :(bBLUE,fBLACK,sDIM),
                    'Normal'    :(bBLACK,fBLUE,sNORMAL),
                    'Onglet'    :(bBLUE,fBLACK,sNORMAL),
                    'Bouton'    :(bBLUE,fBLACK,sNORMAL),
                    'Editeur'   :(bBLUE,fBLACK,sNORMAL),
                    'Options'   :(bBLUE,fBLACK,sNORMAL),
                    'Decorateur':(bBLUE,fBLACK,sNORMAL),
                    'Selection' :(bBLUE,fBLACK,sBRIGHT)
                    }
    elif nom=="BleuJaune":
        self.palette={"Titre": ["\u001b[40m", "\u001b[33m", "\u001b[1m"],
                      "Action": ["\u001b[40m", "\u001b[33m", "\u001b[1m"],
                      "Bordure": ["\u001b[40m", "\u001b[33m", "\u001b[2m"],
                      "Normal": ["\u001b[40m", "\u001b[37m", "\u001b[2m"],
                      "Onglet": ["\u001b[40m", "\u001b[33m", "\u001b[1m"],
                      "Bouton": ["\u001b[44m", "\u001b[33m", "\u001b[22m"],
                      "Editeur": ["\u001b[44m", "\u001b[37m", "\u001b[1m"],
                      "Decorateur": ["\u001b[44m", "\u001b[33m", "\u001b[22m"],
                      "Selection": ["\u001b[44m", "\u001b[33m", "\u001b[1m"],
                      "Options": ["\u001b[44m", "\u001b[30m", "\u001b[1m"]}
    elif os.path.exists(nom):
      f=open(nom,"r")
      self.palette=json.load(f)
      f.close()
    else:
      print(fRED+"Palette '%s' inconnue."%nom)
      self._fixePalette('defaut')

  #---------------------------------------------------------------------------------
  def _couleurID(self,texte,coul):
    f,t,s=self.palette[coul]
    return self._couleur(texte,f+t+s)
  def _couleur(self,texte,coul=None):
    #if self.debug>2: return texte
    if coul is None: return texte
    return coul+texte+sRESET
  def _couleurPerso(self,coul):
    if self.palette['Titre'] is None:
      return None
    #if self.debug>2: return coul
    (FG,BG)=coul
    if   FG=='ROUGE': coul=fRED
    elif FG=='VERT': coul=fGREEN
    elif FG=='JAUNE': coul=fYELLOW
    elif FG=='BLANC': coul=fWHITE
    elif FG=='BLEU': coul=fBLUE
    elif FG=='MAGENTA': coul=fMAGENTA
    elif FG=='CYAN': coul=fCYAN
    elif FG=='NOIR': coul=fBLACK
    if   BG=='ROUGE': coul+=bRED
    elif BG=='VERT': coul+=bGREEN
    elif BG=='JAUNE': coul+=bYELLOW
    elif BG=='BLANC': coul+=bWHITE
    elif BG=='BLEU': coul+=bBLUE
    elif BG=='MAGENTA': coul+=bMAGENTA
    elif BG=='CYAN': coul+=bCYAN
    elif BG=='NOIR': coul+=bBLACK
    return coul

  #---------------------------------------------------------------------------------
  ## Affichage d'un texte sur une ligne avec formatage sur 65 caracteres
  def _contenuSeparateur(self,pTexte,elem):
    if elem['OFFSET']!=0:
      n=elem['OFFSET']
    else:
      n=self.espace
    chaine=pTexte*n
    elem['LARG']=len(chaine)
    return chaine

  #---------------------------------------------------------------------------------
  def _AffLigneSeparateur(self,deb,elem,fin,pVersion=False):
    txtVersion=""
    n=self.espace+self.marge
    if pVersion:
      n-=17+len(TUIVERSION)
      txtVersion="TUI v%s (Ph.Dejean)"%TUIVERSION
    ligne=' '+self._couleurID(deb+elem*n,'Bordure')+self._couleurID(txtVersion,'Bordure')+self._couleurID(fin,'Bordure')
    print(ligne)

  #---------------------------------------------------------------------------------
  ## Affichage d'un texte centre sur une ligne avec formatage sur 65 caracteres
  def _AffLigneCentrerTexte(self,pTexte):
    if len(pTexte)%2==1: pTexte+=' '
    n=self.espace+self.marge-len(pTexte)
    if self.debug:
      pTexte='C'*(n//2)+pTexte+'C'*(n//2)
    else:
      pTexte=' '*(n//2)+pTexte+' '*(n//2)
    ligne=' '+self._couleurID('\u2503','Bordure')+self._couleurID(pTexte,'Titre')+self._couleurID('\u2503','Bordure')
    print(ligne)

  #---------------------------------------------------------------------------------
  def _AffLigneGaucheTexte(self,pTexte,lLen):
    n=self.espace # Les deux bordures extérieures
    if self.debug:
      chaine=self._couleurID('M'*(self.marge//2),'Normal')+pTexte+self._couleurID('G'*(n-lLen),'Normal')+self._couleurID('M'*(self.marge//2),'Normal')
    else:
      chaine=self._couleurID(' '*(self.marge//2),'Normal')+pTexte+self._couleurID(' '*(n-lLen),'Normal')+self._couleurID(' '*(self.marge//2),'Normal')
    ligne=' '+self._couleurID('\u2503','Bordure')+chaine+self._couleurID('\u2503','Bordure')
    print(ligne)

  #---------------------------------------------------------------------------------
  def _AffLigneDroiteTexte(self,pTexte,lLen):
    n=self.espace
    if self.debug:
      chaine=self._couleurID('D'*(n-lLen),'Normal')+pTexte
    else:
      chaine=self._couleurID(' '*(n-lLen),'Normal')+pTexte
    ligne=' '+self._couleurID('\u2503','Bordure')+chaine+self._couleurID('\u2503','Bordure')
    print(ligne)

  #---------------------------------------------------------------------------------
  def _decoration(self,pDebForm,pFinForm):
      f,t,s=self.palette['Decorateur']
      decorDeb=self._couleur(pDebForm,f+t+s)
      decorFin=self._couleur(pFinForm,f+t+s)
      return decorDeb,decorFin,len(pDebForm+pFinForm)

  #---------------------------------------------------------------------------------
  def _selection(self,pSelection):
      f,t,s=self.palette['Selection']
      selection=self._couleur(pSelection,f+t+s)
      return selection,len(selection)

  #---------------------------------------------------------------------------------
  def _FormateSEP(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      texte=texte*largeur
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # -s.o.-

      # Constitution des chaines
      chaine=coulOffset+coulTexte
      largElement=offset+len(texte)

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateLBL(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      # -s.o.-

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      coulAction=self._couleurID(action,'Action')

      # Constitution de la chaine
      if pElement['ALIGN']=='G':
          chaine=coulOffset+coulAction+coulTexte
          largElement=offset+len(action)+len(texte)
      else:
          chaine=coulOffset+coulTexte+coulAction
          largElement=offset+len(action)+len(texte)

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateEDT(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      coulAction=self._couleurID(action,'Action')

      # 6-Traitement de la sélection
      # contenu
      selection=str(self._remplace_variable(pElement['SELECT']))
      if largeur>0:
          if len(selection)<largeur:
              if pElement['ALIGN']=='D':
                  selection=' '*(largeur-len(selection))+selection
              else:
                  selection=selection+' '*(largeur-len(selection))
      # formatage
      decorDeb,decorFin,longForm=self._decoration("|","|")
      coulSelection=self._couleurID(selection,'Editeur')

      # Constitution de la chaine
      chaine=coulOffset+coulTexte+coulAction+decorDeb+coulSelection+decorFin
      largElement=offset+len(texte)+len(action)+len(selection)+longForm

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateSPB(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action="(%s)"%pElement['ACTION']
      # couleur
      coulAction=self._couleurID(action,'Action')

      # 6-Traitement de la sélection
      # contenu
      selection=str(self._remplace_variable(pElement['SELECT']))
      if largeur>0:
          if len(selection)<largeur:
              if pElement['ALIGN']=='D':
                  selection=' '*(largeur-len(selection))+selection
              else:
                  selection=' '*(largeur-len(selection))+selection
      # formatage
      decorDeb,decorFin,longForm=self._decoration("|","|")
      selectForm,_=self._selection("+-")
      longForm=5 # ||+-|
      coulSelection=self._couleurID(selection,'Editeur')

      # Constitution de la chaine
      #       offset     blabla      (a)        |      selection       |          +-         |
      chaine=coulOffset+coulTexte+coulAction+decorDeb+coulSelection+decorFin+selectForm+decorFin
      largElement=offset+len(texte)+len(action)+len(selection)+longForm

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateCKB(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      if largeur>0:
          while len(texte)<largeur:
              texte+=' '
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      coulAction=self._couleurID(action,'Action')

      # 6-Traitement de la sélection
      # contenu
      selection=" "
      if str(self._remplace_variable(pElement['SELECT']))=="1":
          selection="X"
      # formatage
      decorDeb,decorFin,longForm=self._decoration("[","]")
      coulSelection,_=self._selection(selection)
      # couleur

      # Constitution de la chaine
      if pElement['ALIGN']=='G':
          chaine=coulOffset+coulAction+decorDeb+coulSelection+decorFin+coulTexte
          largElement=offset+len(action)+len(selection)+len(texte)+longForm
      else:
          chaine=coulOffset+coulTexte+coulAction+decorDeb+coulSelection+decorFin
          largElement=offset+len(action)+len(texte)+len(selection)+longForm

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateBTN(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Bouton')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      _,t,s=self.palette['Action']
      f,_,_=self.palette['Bouton']
      coulAction=self._couleur(action,f+t+s)

      # 6-Traitement de la sélection
      # -s.o.-

      # 7-Traitement du decorateur
      decorDeb,decorFin,longForm=self._decoration("[","]")
      # largeur et alignement
      debTexte=""
      finTexte=""
      if largeur>0:
          largeur-=len(action)+longForm
          if len(texte)<largeur:
              if pElement['ALIGN']=='D':
                  debTexte=' '*(largeur-len(texte))
              elif pElement['ALIGN']=='G':
                  finTexte=' '*(largeur-len(texte))
              else:
                  debTexte=' '*((largeur-len(texte))//2)
                  finTexte=debTexte
                  if len(debTexte)*2+len(texte)!=largeur:
                      finTexte+=' '
      if largeur<len(texte):
          largeur=len(texte)
      coulDebTexte=self._couleurID(debTexte,'Bouton')
      coulFinTexte=self._couleurID(finTexte,'Bouton')

      # Constitution de la chaine
      chaine=coulOffset+decorDeb+coulAction+coulDebTexte+coulTexte+coulFinTexte+decorFin
      largElement=offset+len(action)+largeur+longForm
      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateSPL(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # largeur et alignement
      if largeur>0:
          if len(texte)<largeur:
              if pElement['ALIGN']=='D':
                  texte=' '*(largeur-len(texte))+texte
              elif pElement['ALIGN']=='G':
                  texte=texte+' '*(largeur-len(texte))
              else:
                  texte=' '*((largeur-len(texte))//2)+texte
                  texte=texte+' '*(largeur-len(texte))
      else:
          largeur=len(texte)
      # couleur
      coulTexte=self._couleurID(texte,'Onglet')

      # 4-Traitement de l'offset
      # -s.o.-

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      _,t,s=self.palette['Action']
      f,_,_=self.palette['Onglet']
      coulAction=self._couleur(action,f+t+s)

      # 6-Traitement de la sélection
      # -s.o.-

      # 7-Traitement du formatage
      if pElement['ACTION'] is None:
          debForm,finForm,longForm="","",0
      else:
          debForm,finForm,longForm="[","]",2
      if len(texte)!=0:
          cardeb='='*3
          carFin='='*(self.espace-len(action)-len(texte)-3-longForm)
      else:
          cardeb='-'*self.espace
          carFin=''
      debForm=cardeb+debForm
      finForm=finForm+carFin
      # couleur
      coulDeb=self._couleurID(debForm,'Onglet')
      coulFin=self._couleurID(finForm,'Onglet')

      # Constitution de la chaine
      chaine=coulDeb+coulAction+coulTexte+coulFin
      largElement=self.espace

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateCBX(self,pElement):
      # 1-Traitement des options
      largOptions=0
      options=self._remplace_variable(pElement['OPTIONS'])
      for i in range(len(options)):
          if len(options[i])>largOptions:
              largOptions=len(options[i])

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      if largeur==0:
          largeur=largOptions

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 5-Traitement de l'action
      # contenu
      action="(%s)"%pElement['ACTION']
      actionDeploie=pElement['ACTION']
      # couleur
      Acoul=self.palette['Action']
      coulAction=self._couleurID(action,'Action')
     # (Fcoul,Tcoul)=self.palette['Boites']
     # coulActionDeploie=self._couleur(actionDeploie,Tcoul)

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')
      offsetOptions=len(action)
      deuxPointsValeur=1+len(str(len(pElement['OPTIONS'])-1))
      coulOffsetOptions=self._couleurID(' '*(offset+len(texte)-deuxPointsValeur),'Normal')
      coulOffsetFin=self._couleurID(' '*(offset+len(texte)+offsetOptions),'Normal')

      # 6-Traitement de la sélection
      # contenu
      numSelection=int(self._remplace_variable(pElement['SELECT']))
      selection=""
      #print("DBG>",numSelection,pElement['OPTIONS'])
      if numSelection!=-1:
          if numSelection>=len(pElement['OPTIONS']):
              numSelection=0
          selection=pElement['OPTIONS'][numSelection]
      # formatage
      decorDeb,decorFin,longForm=self._decoration("|","|")
      deploieForm,_=self._selection("v")
      selectForm,_=self._selection(">")
      deselectForm,_=self._selection(" ")
      longForm=4 # ||v|
      if pElement['DEPLOIE']: f,t,s=self.palette['Normal']
      else: f,t,s=self.palette['Editeur']
      coulSelection=self._couleur(selection+' '*(largeur-len(selection)),f+t+s)

      # Constitution des chaines
      chaineDeploie=[]
      largElementDeploie=[]
      lenEspace=0
      if len(options)>10:lenEspace=1
      for i in range(len(options)):
          espace=''
          if len(options) > 10 > i:
              espace=' '
          coulNumerotation=self._couleurID('(%s:%d)%s'%(actionDeploie,i,espace),'Action')
          coulOption=self._couleurID(options[i],'Options')+self._couleurID(' '*(largeur-len(options[i])-1),'Options')
          if i==numSelection:
              coulSelect=selectForm
          else:
              coulSelect=deselectForm
          #                        offset            (a)            |      >            option      |
          chaineDeploie.append(coulOffsetOptions+coulNumerotation+decorDeb+coulSelect+coulOption+decorFin)
          largElementDeploie.append(offset+len(texte)+offsetOptions-deuxPointsValeur+longForm+largeur+lenEspace)
      # trait final
      traitFinal=' '+'-'*largeur+' '
      coulTraitFinal=self._couleurID(traitFinal,'Decorateur')
      chaineDeploie.append(coulOffsetFin+coulTraitFinal)
      largElementDeploie.append(offset+len(texte)+offsetOptions+largeur+2)

      #       offset     blabla    (a)          |      selection       |          v           |
      chaine=coulOffset+coulTexte+coulAction+decorDeb+coulSelection+decorFin+deploieForm+decorFin
      largElement=offset+len(texte)+len(action)+1+largeur+3

      deploie=None
      if pElement['DEPLOIE']:
          deploie=(chaineDeploie,largElementDeploie)
      pElement['DEPLOIE']=False

      return chaine,largElement,deploie

  #---------------------------------------------------------------------------------
  def _FormateLST(self,pElement):
      # 1-Traitement des options
      options=pElement['OPTIONS']
      largOptions=0
      for i in range(len(options)):
          options[i]=self._remplace_variable(options[i])
          if len(options[i])>largOptions:
              largOptions=len(options[i])

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      if largeur==0:
          largeur=largOptions

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')
      offsetOptions=3
      coulOffsetOptions=self._couleurID(' '*(offset+offsetOptions),'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']

      # 6-Traitement de la sélection
      # contenu
      selections=self._remplace_variable(pElement['SELECT'])
      if type(selections)!=list:
          selections=selections.split(',')
      selections=[int(x) for x in selections]
      # formatage
      decorDeb,decorFin,longForm=self._decoration("|","|")
      selectForm,_=self._selection(">")
      deselectForm,_=self._selection(" ")

      # Constitution des chaines
      chaine=[]
      largElement=[]
      if len(texte)>0:
          chaine.append(coulOffset+coulTexte)
          largElement.append(offset+len(texte))
      for i in range(len(options)):
          coulNumerotation=self._couleurID('(%s:%d)'%(action,i),'Action')
          if i in selections:
              coulSelect=selectForm
          else:
              coulSelect=deselectForm
          coulOption=self._couleurID(options[i]+' '*(largeur-len(options[i])),'Options')

          chaine.append(coulOffsetOptions+coulNumerotation+decorDeb+coulSelect+coulOption+decorFin)
          largElement.append(offset+3+5+1+2+largeur)

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateRAD(self,pElement):
      # 1-Traitement des options
      options=pElement['OPTIONS']
      largOptions=0
      for i in range(len(options)):
          options[i]=self._remplace_variable(options[i])
          if len(options[i])>largOptions:
              largOptions=len(options[i])
      offsetOptions=3
      coulOffsetOptions=self._couleurID(' '*offsetOptions,'Normal')

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      if largeur==0:
          largeur=largOptions

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      longFormat=8 # (X:n)(*)"
      if len(texte)>0:
          while len(texte)<largeur+longFormat+offsetOptions: # Largeur des options, longeur du format, complement d'offset
              texte+=' '
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']

      # 6-Traitement de la sélection
      # contenu
      selection=int(self._remplace_variable(pElement['SELECT']))
      # formatage
      decorDeb,decorFin,longForm=self._decoration("(",")")
      selectForm,_=self._selection("*")
      deselectForm,_=self._selection(" ")

      # Constitution des chaines
      chaine=[]
      largElement=[]
      if len(texte)>0:
          chaine.append(coulOffset+coulTexte)
          largElement.append(offset+len(texte))
      for i in range(len(options)):
          coulAction=self._couleurID('(%s:%d)'%(action,i),'Action')
          if i==selection:
              coulSelect=selectForm
          else:
              coulSelect=deselectForm
          coulOption=self._couleurID(options[i]+' '*(largeur-len(options[i])),'Options')

          chaine.append(coulOffset+coulOffsetOptions+coulAction+decorDeb+coulSelect+decorFin+coulOption)
          largElement.append(offset+offsetOptions+longFormat+largeur)

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateGCB(self,pElement):
      # 1-Traitement des options
      options=pElement['OPTIONS']
      largOptions=0
      for i in range(len(options)):
          options[i]=self._remplace_variable(options[i])
          if len(options[i])>largOptions:
              largOptions=len(options[i])
      offsetOptions=3
      coulOffsetOptions=self._couleurID(' '*offsetOptions,'Normal')

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      if largeur==0:
          largeur=largOptions

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      longFormat=8 # (X:n)(*)"
      if len(options)>10:longFormat+=1
      if len(texte)>0:
          while len(texte)<largeur+longFormat+offsetOptions: # Largeur des options, longeur du format, complement d'offset
              texte+=' '
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']

      # 6-Traitement de la sélection
      # contenu
      selections=self._remplace_variable(pElement['SELECT'])
      if selections!='':
          if type(selections)!=list:
              selections=selections.split(',')
      else:
          selections=[]
      selections=[int(x) for x in selections]
      # formatage
      decorDeb,decorFin,longForm=self._decoration("[","]")
      selectForm,_=self._selection("*")
      deselectForm,_=self._selection(" ")

      # Constitution des chaines
      chaine=[]
      largElement=[]
      if len(texte)>0:
          chaine.append(coulOffset+coulTexte)
          largElement.append(offset+len(texte))
      for i in range(len(options)):
          esp=" " if len(options)>10 and i<10 else ""
          # couleur
          coulAction=self._couleurID('(%s:%d)%s'%(action,i,esp),'Action')
          if i in selections:
              coulSelect=selectForm
          else:
              coulSelect=deselectForm
          coulOption=self._couleurID(options[i]+' '*(largeur-len(options[i])),'Options')

          chaine.append(coulOffset+coulOffsetOptions+coulAction+decorDeb+coulSelect+decorFin+coulOption)
          largElement.append(offset+offsetOptions+longFormat+largeur)

      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateZED(self,pElement):
      # 1-Traitement des options
      # -s.o.-

      # 2-Traitement de la largeur
      largeur=pElement['LARG']
      hauteur=pElement['HAUT']

      # 3-Traitement du texte
      # contenu
      texte=self._remplace_variable(pElement['TEXTE'])
      # couleur
      coulTexte=self._couleurID(texte,'Normal')

      # 4-Traitement de l'offset
      offset=pElement['OFFSET']
      coulOffset=self._couleurID(' '*offset,'Normal')

      # 5-Traitement de l'action
      # contenu
      action=pElement['ACTION']
      if action is None:
          action=""
      else:
          action="(%s)"%action
      # couleur
      coulAction=self._couleurID(action,'Action')

      # 6-Traitement de la sélection
      # contenu
      selection=self._remplace_variable(pElement['SELECT'])
      if largeur==0:
          largeur=len(selection)//hauteur
      listeTexte=[]
      while len(selection)>largeur:
          listeTexte.append(selection[:largeur])
          selection=selection[largeur:]
      selection=selection+' '*(largeur-len(selection))
      listeTexte.append(selection)
      # formatage
#      debForm,finForm,longForm="|","|",2
      decorDeb,decorFin,longForm=self._decoration("|","|")
      # couleur
#      Dcoul=self.palette['Decorateur']
##      coulDeb=self._couleur(debForm,Dcoul)
#      coulFin=self._couleur(finForm,Dcoul)

      # Constitution des chaines
      chaine=[]
      largElement=[]
      if texte!="":
          chaine.append(coulOffset+coulTexte)
          largElement.append(offset+len(texte))
      for i in range(len(listeTexte)):
          coulSelection=self._couleurID(listeTexte[i],'Editeur')
          chaine.append(decorDeb+coulSelection+decorFin)
          largElement.append(largeur+longForm)
      return chaine,largElement

  #---------------------------------------------------------------------------------
  def _FormateElement(self,pElement,numLig,numElem):
      #if self.debug>1:print(pElement)
      deploie=None
      if pElement['TYPE']=='SPL':
          (chaine,lenChaine)=self._FormateSPL(pElement)
      elif pElement['TYPE']=='LBL':
          (chaine,lenChaine)=self._FormateLBL(pElement)
      elif pElement['TYPE']=='BTN':
          (chaine,lenChaine)=self._FormateBTN(pElement)
      elif pElement['TYPE']=='EDT':
          (chaine,lenChaine)=self._FormateEDT(pElement)
      elif pElement['TYPE']=='CBX':
          (chaine,lenChaine,deploie)=self._FormateCBX(pElement)
      elif pElement['TYPE']=='SPB':
          (chaine,lenChaine)=self._FormateSPB(pElement)
      elif pElement['TYPE']=='SEP':
          (chaine,lenChaine)=self._FormateSEP(pElement)
      elif pElement['TYPE']=='ZED':
          (chaine,lenChaine)=self._FormateZED(pElement)
      elif pElement['TYPE']=='RAD':
          (chaine,lenChaine)=self._FormateRAD(pElement)
      elif pElement['TYPE']=='GCB':
          (chaine,lenChaine)=self._FormateGCB(pElement)
      elif pElement['TYPE']=='CKB':
          (chaine,lenChaine)=self._FormateCKB(pElement)
      elif pElement['TYPE']=='LST':
          (chaine,lenChaine)=self._FormateLST(pElement)
      else:
          chaine=""
          lenChaine=0
          print(pElement['TYPE'])
      return chaine,lenChaine,deploie

  #---------------------------------------------------------------------------------
  def _AffChaines(self,listChaines):
    if len(listChaines)==0:
      return
    # Si debug : affichage de la regle
    #if self.debug:
    #  regle1="|         1         2         3         4         5         6         7         8         9         10        11        12        13        14        15        16"
    #  regle2="|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|123456789|1"
    #  print(regle1[:(self.largeur+1)])
    #  print(regle2[:(self.largeur+1)])
    # chaine et la longueur
    chaine=''
    lenChaine=0
    c,_,_=listChaines[0]
    deploie=None
    if type(c)!=list:
      for c,l,d in listChaines:
        chaine+=c
        lenChaine+=l
        if d is not None:
          deploie=d
      self._AffLigneGaucheTexte(chaine,lenChaine)
      if deploie is not None:
        chaine,lenChaine=deploie
        for num in range(len(chaine)):
          self._AffLigneGaucheTexte(chaine[num],lenChaine[num])
    else:
      hauteurMax=0
      for multiChaine,_,_ in listChaines:
        if hauteurMax<len(multiChaine): hauteurMax=len(multiChaine)
      for idxHaut in range(hauteurMax):
        chaine=""
        lenChaine=0
        for multiChaine,multiLenChaine,_ in listChaines:
          chaine+=multiChaine[idxHaut]
          lenChaine+=multiLenChaine[idxHaut]
        self._AffLigneGaucheTexte(chaine,lenChaine)

  #---------------------------------------------------------------------------------
  def _fonctLBL(self,objet,val):
      pass

  #---------------------------------------------------------------------------------
  def _fonctLST(self,objet,val):
    if val!="--PAS_DE_VALEUR--":
      select=objet['SELECT']
      if type(select)==str and len(select)>0 and select[0]=='@':
        variable=select.replace('@','')
        select=[int(x) for x in self.TUI_getVariable(variable).split(',')]
      if val=='d':
        select=[]
      elif val=='s':
        select=list(range(len(objet['OPTIONS'])))
      elif val=='i':
        objInv=select
        select=list(range(len(objet['OPTIONS'])))
        for ival in objInv:
          select.remove(ival)
      elif is_digit(val):
        ival=int(val)
        if -1<ival<len(objet['OPTIONS']):
          if ival in select: select.remove(ival)
          else:
            select.append(ival)
      select.sort()
      if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
        variable=objet['SELECT'].replace('@','')
        valeur=[str(x) for x in select]
        self.TUI_setVariable(variable,",".join(valeur))
      else:
        objet['SELECT']=select

  #---------------------------------------------------------------------------------
  def _fonctRAD(self,objet,val):
    if val!="--PAS_DE_VALEUR--":
      if is_digit(val):
        ival=int(val)
        if -1<ival<len(objet['OPTIONS']):
          if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
            variable=objet['SELECT'].replace('@','')
            self.TUI_setVariable(variable,val)
          else:
            objet['SELECT']=ival

  #---------------------------------------------------------------------------------
  def _fonctGCB(self,objet,val):
    if val!="--PAS_DE_VALEUR--":
      select=objet['SELECT']
      if type(select)==str and len(select)>0 and select[0]=='@':
        variable=select.replace('@','')
        if self.TUI_getVariable(variable)=='':
            select=[]
        else:
            select=[int(x) for x in self.TUI_getVariable(variable).split(',')]
      if val=='d':
        select=[]
      elif val=='s':
        select=list(range(len(objet['OPTIONS'])))
      elif val=='i':
        objInv=select
        select=list(range(len(objet['OPTIONS'])))
        for ival in objInv:
          select.remove(ival)
      elif is_digit(val):
        ival=int(val)
        if -1<ival<len(objet['OPTIONS']):
          if ival in select: select.remove(ival)
          else:
            select.append(ival)
      select.sort()
      if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
        variable=objet['SELECT'].replace('@','')
        valeur=[str(x) for x in select]
        self.TUI_setVariable(variable,",".join(valeur))
      else:
        objet['SELECT']=select

  #---------------------------------------------------------------------------------
  # Fonction de bascule de la checkbox
  def _fonctCKB(self,objet,val):
    valeur=int(self.TUI_getValeurObjet(objet,'SELECT'))
    self.TUI_setValeurObjet(objet,'SELECT',1-valeur)

  #---------------------------------------------------------------------------------
  # Fonction de la combobox : récupère l'item
  def _fonctCBX(self,objet,val):
    if val!="--PAS_DE_VALEUR--":
      if is_digit(val):
        ival=int(val)
        if -1<ival<len(objet['OPTIONS']):
          if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
            variable=objet['SELECT'].replace('@','')
            self.TUI_setVariable(variable,val)
          else:
            objet['SELECT']=ival

  #---------------------------------------------------------------------------------
  def _fonctSPB(self,objet,val):
    (imin,imax,its,itx)=objet['INTERVALLE']
    if val!="--PAS_DE_VALEUR--":
      if is_digit(val):
        ival=int(val)
        if ival<imin:
          ival=imin
        elif imax<ival:
          ival=imax
      else:
        if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
          variable=objet['SELECT'].replace('@','')
          ival=int(self.TUI_getVariable(variable))
        else:
          ival=int(objet['SELECT'])
        if val=='++':
          ival+=itx
        elif val=='+':
          ival+=its
        if ival>imax:
          ival=imax
        if val=='-':
          ival-=its
        elif val=='--':
          ival-=itx
        if ival<imin:
          ival=imin
      if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
        variable=objet['SELECT'].replace('@','')
        self.TUI_setVariable(variable,ival)
      else:
        objet['SELECT']=ival

  #---------------------------------------------------------------------------------
  def _fonctEDT(self,objet,val):
    if val!="--PAS_DE_VALEUR--":
      if type(objet['SELECT'])==str and len(objet['SELECT'])>0 and objet['SELECT'][0]=='@':
        variable=objet['SELECT'].replace('@','')
        self.TUI_setVariable(variable,val)
        if self.debug:
            print(fMAGENTA+"DBG-TUI>_fonctEDT",variable,val)
      else:
        objet['SELECT']=val

  #---------------------------------------------------------------------------------
  def _fonctBTN(self,objet,val):
    pass

  #---------------------------------------------------------------------------------
  def _fonctSPL(self,pObjet,val):
      actif=not pObjet['SPLDEP']
      for idObjet in self.widgets:
          objet=self.widgets[idObjet]
          if 'SPL' in objet and objet['SPL']==pObjet['SPL']:
              objet['SPLDEP']=actif

  #---------------------------------------------------------------------------------
  def TUI_ActionWidget(self,action):
    if action[0] in self.actionWidget:
      idObjet=self.actionWidget[action[0]]
      objet=self.widgets[idObjet]
      #if objet['TYPE']!='SPL' and
      if not objet['ACTIF']: return [None]
      if objet['TYPE']=='CBX' and len(action)==2 and action[1]=="v" :
        self.widgets[idObjet]['DEPLOIE']=True
        action=[None]
      else:
        commande='self._fonct%s'%objet['TYPE']
        eval(commande+"(objet,'%s')"%(','.join(action[1:])))
        if objet['TYPE']=='CBX':
          self.widgets[idObjet]['DEPLOIE']=False
    return action

  #---------------------------------------------------------------------------------
  def TUI_Rafraichir(self):
    # Affichage de la bordure haute
    self._AffLigneSeparateur('\u250F','\u2501','\u2513')
    # Affichage du titre et de la version
    self._AffLigneCentrerTexte("%s %s"%(self.appliTUI['TITRE'],self.appliTUI['VERSION']))
    # Affichage de la separation Titre/Version vs Corps
    self._AffLigneSeparateur('\u2523','\u2501','\u252B')
    # Boucle sur les lignes d'elements
    for numLig in range(self.maxLignes):
      if numLig in self.table:
        ligne=self.table[numLig]
        #if self.debug:
        #  print("| Element texte"+" "*(self.largeur-15)+"| esp | EMPL | empl | LARG | larg | align | texte | reste | inLarg | outLarg | reste' |")
        #  self.sommeReste=self.espace
        chaines=[]
        # recolte des éléments visibles
        for numElem in range(len(ligne)):
           pElement=self.widgets[ligne[numElem]]
           # si l'element est visible et que le SPL est deployé
#           if pElement['TYPE']=='SPL': print(pElement['VISIBLE'],pElement['SPLDEP'])
           if pElement['VISIBLE'] and (pElement['TYPE']=='SPL' or pElement['SPLDEP']):
             chaines.append(self._FormateElement(pElement,numLig,numElem))
        # affichage des éléments visibles
        self._AffChaines(chaines)
    # Affichage de la bordure basse
    self._AffLigneSeparateur('\u2517','\u2501','\u251B',True)

  #---------------------------------------------------------------------------------
  def TUI_Afficher(self,pAction=None):
    if self.affichageTUI:
      self.TUI_Rafraichir()
      action=input(fYELLOW+'? ').split(':')
      if len(action)==1: action.append("--PAS_DE_VALEUR--")
    else:
      action=pAction
    if CLAVIER:
        print("CLV>",action)
    action=self.TUI_ActionWidget(action)
    if action[0]=="!d!":
        self.debug=not self.debug
        action=["","--PAS_DE_VALEUR--"]
    elif action[0]=="!nb!":
        self._fixePalette('NoirBlanc')
    elif action[0]=="!j!":
        self._fixePalette('Jaune')
    elif action[0]=="!b!":
        self._fixePalette('Bleu')
    elif action[0]=="!jb!":
        self._fixePalette('BleuJaune')
    elif action[0]=="!p":
        if os.path.exists(action[1]):
            f=open(action[1],"r")
            self.palette=json.load(f)
            f.close()
    elif action[0]=='!cmd':
        self.TUI_ChargerCommandes(action[1])
    elif action[0]=='!v!':
        self.TUI_SauvegardeConfig()
    elif action[0]=='!':
        print('!d! : debug')
        print('!p:<fichier> : charge la palette')
        print('!nb!,!j!,!b!,!jb! : palettes')
        print('!cmd:<fichier> : commandes')
        print('!v! : enregistre la configuration')
    self.affichageTUI=len(self.listeCommandes)==0
    return action

  #---------------------------------------------------------------------------------
  def TUI_getValeurParNom(self,pNomWidget,pPropriete,pRang=None):
    idObjet=self.indexWidgets[pNomWidget]
    objet=self.widgets[idObjet]
    return self.TUI_getValeurObjet(objet,pPropriete,pRang)

  #---------------------------------------------------------------------------------
  def TUI_getValeurObjet(self,pObjet,pPropriete,pRang=None):
    if type(pObjet[pPropriete])==list and pRang is not None:
      if pRang<len(pObjet[pPropriete]):
        return pObjet[pPropriete][pRang]
      else:
        return pObjet[pPropriete][0]
    if type(pObjet[pPropriete])==str and len(pObjet[pPropriete])>0 and pObjet[pPropriete][0]=='@':
      variable=pObjet[pPropriete].replace('@','')
      return self.appliTUI['VARIABLES'][variable]
    return pObjet[pPropriete]

  #---------------------------------------------------------------------------------
  def TUI_setValeurParId(self,pIdObjet,pPropriete,pValeur,prang=None):
    objet=self.widgets[pIdObjet]
    self.TUI_setValeurObjet(objet,pPropriete,pValeur,prang)

  #---------------------------------------------------------------------------------
  def TUI_setValeurParNom(self,pNomWidget,pPropriete,pValeur,prang=None):
    idObjet=self.indexWidgets[pNomWidget]
    objet=self.widgets[idObjet]
    self.TUI_setValeurObjet(objet,pPropriete,pValeur,prang)

  #---------------------------------------------------------------------------------
  def TUI_setValeurObjet(self,pObjet,pPropriete,pValeur,prang=None):
    if type(pObjet[pPropriete])==list and prang is not None:
      pObjet[pPropriete][prang]=pValeur
    elif type(pObjet[pPropriete])==str and len(pObjet[pPropriete])>0 and pObjet[pPropriete][0]=='@':
      variable=pObjet[pPropriete].replace('@','')
      self.appliTUI['VARIABLES'][variable]=str(pValeur)
    else:
      pObjet[pPropriete]=pValeur

  #---------------------------------------------------------------------------------
  def TUI_appendValeur(self,pNomWidget,pPropriete,pValeur):
    valcourante=self.TUI_getValeurParNom(pNomWidget,pPropriete)
    if valcourante is None:
      valcourante=[]
    valcourante.append(pValeur)
    self.TUI_setValeurParNom(pNomWidget,pPropriete,valcourante)

  #---------------------------------------------------------------------------------
  def TUI_getwidgetParNom(self,pNomWidget):
    idObjet=self.indexWidgets[pNomWidget]
    objet=self.widgets[idObjet]
    return objet

  #---------------------------------------------------------------------------------
  def TUI_getwidget(self,pIdWidget):
    objet=self.widgets[pIdWidget]
    return objet

  #---------------------------------------------------------------------------------
  def TUI_getIndexItem(self,pNomWidget,pPropriete,pValeur):
    valcourante=self.TUI_getValeurParNom(pNomWidget,pPropriete)
    if type(valcourante)==list:
      index=valcourante.index(pValeur)
    else: index=None
    return index

#===============================================================================
# FIN
