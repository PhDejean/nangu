#!/usr/bin/env bash
#=======================================================================
set -e
SDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

#--[NANGU]--------------------------------------------------------------
export EDIT=echo

#--[ENVIRONNEMENT]------------------------------------------------------
export PYTHONPATH=${NANGU}:${PYTHONPATH}

#--[NANGU-Projet]-------------------------------------------------------
export NANGUPRJ=${SDIR}
export NANGUWK=${SDIR}/work
mkdir -p ${NANGUWK}

export DonneesEntr=${SDIR}/DonneesEntr
export DonneesProd=${SDIR}/DonneesProd
export DonneesRef=${SDIR}/DonneesRef
export repertKAA=/home/philippe.dejean/ConfianceAI/kaa

FICHIERPRJ=${SDIR}/nngkaa.prj

#--[Lancement]-------------------------------------------------------
python3 nngTUI.py ${FICHIERPRJ}

#=======================================================================
# FIN
