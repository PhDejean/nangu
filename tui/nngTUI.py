#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#===============================================================================

import os,sys,time
import TUIapplication

#-------------------------------------------------------------------------------
AppliTUI={
    'TUIVERSION':"1.0",
    'TITRE'     :'Nangu',
    'VERSION'   :'v0.0',
    'PALETTE'   :"TUI_KAA.pal",
    'LARGEUR'   :80,
    'MARGE'     :1,
    'ELEMENTS':[
        [{        'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':1,'TYPE':'SPL','ACTION':'oC',                        'TEXTE':"CONFIGURATION"}],
        [{'SPL':1,'TYPE':'LBL',                                      'TEXTE':"Référence :"}],
        [{'SPL':1,'TYPE':'EDT','ACTION':'f',              'LARG':71, 'SELECT':"valeur Editeur",                'NOM':'edtReference'}],
        [{'SPL':1,'TYPE':'SEP',                                      'TEXTE':""}],

        [{'SPL':2,'TYPE':'SPL','ACTION':'oP',                        'TEXTE':"PLANS"}],
        [{'SPL':2,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':2,'TYPE':'CBX','ACTION':'cP','OFFSET':2,  'LARG':46, 'SELECT':'@cP@',          'OPTIONS':[""], 'NOM':'cbxPlansC'},
         {'SPL':2,'TYPE':'BTN','ACTION':'sP','OFFSET':4,  'LARG':16, 'TEXTE':" Supprimer",                     'NOM':'btnPSuppr'}],
        [{'SPL':2,'TYPE':'BTN','ACTION':'eP','OFFSET':60, 'LARG':16, 'TEXTE':"Editer",                         'NOM':'btnPEdit'}],
        [{'SPL':2,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':2,'TYPE':'LBL','ACTION':'xP',             'LARG':10, 'TEXTE':"Prefixe"},
         {'SPL':2,'TYPE':'LBL','ACTION':'rP','OFFSET':3,  'LARG':10, 'TEXTE':"Ss-Ref"},
         {'SPL':2,'TYPE':'LBL','ACTION':'lP','OFFSET':4,  'LARG':10, 'TEXTE':"libelle"},
         {'SPL':2,'TYPE':'BTN','ACTION':'mP','OFFSET':21, 'LARG':16, 'TEXTE':" Renomme",                       'NOM':'btnPrenomme'}],
        [{'SPL':2,'TYPE':'EDT',                           'LARG':10, 'SELECT':"PLN",                           'NOM':'edtPPref'},
         {'SPL':2,'TYPE':'EDT',              'OFFSET':2,  'LARG':10, 'SELECT':"",                              'NOM':'edtPSousRef'},
         {'SPL':2,'TYPE':'EDT',              'OFFSET':2,  'LARG':28, 'SELECT':"",                              'NOM':'edtPLibelle'},
         {'SPL':2,'TYPE':'BTN','ACTION':'aP','OFFSET':2,  'LARG':16, 'TEXTE':"Ajout",                          'NOM':'btnPAjout'}],
        [{'SPL':2,'TYPE':'SEP',                                      'TEXTE':""}],

        [{'SPL':3,'TYPE':'SPL','ACTION':'oS',                        'TEXTE':"SCENARIOS"}],
        [{'SPL':3,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':3,'TYPE':'CBX','ACTION':'cS','OFFSET':2,  'LARG':46, 'SELECT':'@cS@',          'OPTIONS':[""], 'NOM':'cbxScenarC'},
         {'SPL':3,'TYPE':'BTN','ACTION':'sS','OFFSET':4,  'LARG':16, 'TEXTE':" Supprimer",                     'NOM':'btnSSuppr'}],
        [{'SPL':3,'TYPE':'BTN','ACTION':'eS','OFFSET':60, 'LARG':16, 'TEXTE':"Editer",                         'NOM':'btnSEdit'}],
        [{'SPL':3,'TYPE':'BTN','ACTION':'saP','OFFSET':16,'LARG':30, 'TEXTE':"Ajouter au plan",                'NOM':'btnSAjoutPlan'}],
        [{'SPL':3,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':3,'TYPE':'LBL','ACTION':'xS',             'LARG':10, 'TEXTE':"Prefixe"},
         {'SPL':3,'TYPE':'LBL','ACTION':'rS','OFFSET':3,  'LARG':10, 'TEXTE':"Ss-Ref"},
         {'SPL':3,'TYPE':'LBL','ACTION':'lS','OFFSET':4,  'LARG':10, 'TEXTE':"libelle"},
         {'SPL':3,'TYPE':'BTN','ACTION':'mS','OFFSET':21, 'LARG':16, 'TEXTE':"Renomme",                        'NOM':'btnSrenomme'}],
        [{'SPL':3,'TYPE':'EDT',                           'LARG':10, 'SELECT':"SCN",                           'NOM':'edtSPref'},
         {'SPL':3,'TYPE':'EDT',              'OFFSET':2,  'LARG':10, 'SELECT':"",                              'NOM':'edtSSousRef'},
         {'SPL':3,'TYPE':'EDT',              'OFFSET':2,  'LARG':28, 'SELECT':"",                              'NOM':'edtSLibelle'},
         {'SPL':3,'TYPE':'BTN','ACTION':'aS','OFFSET':2,  'LARG':16, 'TEXTE':"Ajout",                          'NOM':'btnSAjout'}],
        [{'SPL':3,'TYPE':'SEP',                                      'TEXTE':""}],

        [{'SPL':4,'TYPE':'SPL','ACTION':'oF',                        'TEXTE':"FICHES"}],
        [{'SPL':4,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':4,'TYPE':'CBX','ACTION':'cF','OFFSET':2,  'LARG':46, 'SELECT':'@cF@',          'OPTIONS':[""], 'NOM':'cbxFicheC'},
         {'SPL':4,'TYPE':'BTN','ACTION':'sF','OFFSET':4,  'LARG':16, 'TEXTE':" Supprimer",                     'NOM':'btnFSuppr'}],
        [{'SPL':4,'TYPE':'BTN','ACTION':'eF','OFFSET':60, 'LARG':16, 'TEXTE':"Editer",                         'NOM':'btnFEdit'}],
        [{'SPL':4,'TYPE':'BTN','ACTION':'faS','OFFSET':16,'LARG':30, 'TEXTE':"Ajouter au scénario",            'NOM':'btnFAjoutScenar'}],
        [{'SPL':4,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':4,'TYPE':'LBL','ACTION':'xF',             'LARG':10, 'TEXTE':"Prefixe"},
         {'SPL':4,'TYPE':'LBL','ACTION':'rF','OFFSET':3,  'LARG':10, 'TEXTE':"Ss-Ref"},
         {'SPL':4,'TYPE':'LBL','ACTION':'lF','OFFSET':4,  'LARG':10, 'TEXTE':"libelle"},
         {'SPL':4,'TYPE':'BTN','ACTION':'mF','OFFSET':21, 'LARG':16, 'TEXTE':"Renomme",                        'NOM':'btnFrenomme'}],
        [{'SPL':4,'TYPE':'EDT',                           'LARG':10, 'SELECT':"FIC",                           'NOM':'edtFPref'},
         {'SPL':4,'TYPE':'EDT',              'OFFSET':2,  'LARG':10, 'SELECT':"",                              'NOM':'edtFSousRef'},
         {'SPL':4,'TYPE':'EDT',              'OFFSET':2,  'LARG':28, 'SELECT':"",                              'NOM':'edtFLibelle'},
         {'SPL':4,'TYPE':'BTN','ACTION':'aF','OFFSET':2,  'LARG':16, 'TEXTE':"aF.Ajout",                       'NOM':'btnFAjout'}],
        [{'SPL':4,'TYPE':'SEP',                                      'TEXTE':""}],

        [{'SPL':5,'TYPE':'SPL','ACTION':'oE',                        'TEXTE':u"Execution"}],
        [{'SPL':5,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':5,'TYPE':'BTN','ACTION':'B','OFFSET':12,  'LARG':14, 'TEXTE':"Bilan",                          'NOM':'btnBilan'},
         {'SPL':5,'TYPE':'BTN','ACTION':'C','OFFSET':20,  'LARG':14, 'TEXTE':"Contrôle",                       'NOM':'btnControle'}],
        [{'SPL':5,'TYPE':'SEP',                                      'TEXTE':""}],
        [{'SPL':5,'TYPE':'LBL',                                      'TEXTE':"Plans :"}],
        [{'SPL':5,'TYPE':'CBX','ACTION':'gP','OFFSET':2,  'LARG':46, 'SELECT':'@gP@',  'OPTIONS':["Plan 1","Plan 2","Plan 3","Plan 4"],'NOM':'cbxPlansE'},
         {'SPL':5,'TYPE':'BTN','ACTION':'E','OFFSET':4,   'LARG':16, 'TEXTE':"Exécuter",                       'NOM':'btnExecuter'}],
        [{        'TYPE':'SEP',                                      'TEXTE':""}],
    ],
    'VARIABLES':{'cP':'0','cS':'0','cF':'0','gP':'0'},
    'QUITTER':'X'
}
#-------------------------------------------------------------------------------
def TUI(pFichierPrj,pDebug=0):
    # Création du menu TUI
    menu=TUIapplication.MenuTUI(AppliTUI,pFichierPrj,pDebug)
    # Lancement menu TUI
    menu.MenuPrincipal()
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    if len(sys.argv)!=2:
      print("syntaxe : %s <fichier.prj>"%sys.argv[0])
    else:
      TUI(sys.argv[1],pDebug=0)

#===============================================================================
# FIN
