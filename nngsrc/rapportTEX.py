#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,time,datetime,sys,glob,tarfile
import nngsrc.services
import nngsrc.qualite

#-------------------------------------------------------------------------------

Atester="$\\blacktriangleright$ "
TResult="$\\blacktriangledown$"
DebFin ="$\\blacksquare$"
itemSCEAct="$\\bullet$"
itemSCE="$\\circ$"
REPERTRAPPORT="LaTeX"
DOCEXTENSION=".tex"
SDL="\n"
CLEARPAGE="\\clearpage\n\n"

LISTE_EXTENSION=[ ]
VIEWERLATEX=os.getenv("VIEWER_LATEX","evince")
#-------------------------------------------------------------------------------
def max_dispchr(res1,res2):
  if res1=='': return res2
  if res1=="OK":  return res2
  return res1

#-------------------------------------------------------------------------------
def sansAccent(texte):
  for s,a in [('o','ô'),('e','é'),('e','è'),('e','ê'),('e','ë'),('u','û')]:
    texte=texte.replace(a,s)
  return texte

#-------------------------------------------------------------------------------
def rapport_bilan_fin():
  ligne="\\end{tabular}\n"
  ligne+="\\normalsize\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan_debut(nbcol):
  ligne="\\newcolumntype{R}[1]{>{\\raggedleft\\hspace{0pt}}p{#1}}\n"
  ligne+="\\small\n"
  if nbcol==2:  ligne+="\\begin{tabular}{p{11cm}R{1cm}}\n"
  else:         ligne+="\\begin{tabular}{p{11cm}R{1cm}R{1.5cm}R{1.5cm}}\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan(nbcol,texte,valeur=None,soit=None,pourcent=None,italique=False):
  if italique:
    ligne="\\textit{-%s}"%texte
  else:
    ligne=texte

  if valeur=='':
    colonne=" \_ "
  else:
    colonne="%d"%valeur
  if italique:
    ligne+="&\\textit{%s}"%colonne
  else:
    ligne+="&%s"%colonne

  if soit:
    if italique:
      ligne+="&\\textit{%s}"%soit
    else:
      ligne+="&%s"%soit
  elif nbcol==4:
    ligne+="&"

  if pourcent is not None:
    if pourcent=='':
      colonne=" \_ \%"#
    else:
      colonne="%4.1f\%%"%(100*pourcent)
    if italique:
      ligne+="&\\textit{%s}"%colonne
    else:
      ligne+="&%s"%colonne
  elif nbcol>=3:
    ligne+="&"

  ligne+="\\tabularnewline\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan_separatrice(nbcol):
  return "\\hline\n"

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Fonction de définition des titres de chapitre et paragraphes
#-------------------------------------------------------------------------------
def Titre(num,chaine):
  if   num==1: return Titre1(chaine)
  elif num==2: return Titre2(chaine)
  elif num==3: return Titre3(chaine)
  elif num==4: return Titre4(chaine)
def Titre1(chaine,txtLabel=None):
  chaine=chaine.replace("--","$NANGU$")
  chaine=chaine.replace("-"," ")
  chaine=chaine.replace("$NANGU$","-")
  label=''
  if txtLabel is not None: label="\\label{%s}"%txtLabel
  return "\\chapter{%s}\n%s"%(chaine,label)
def Titre2(chaine,txtLabel=None):
  label=''
  if txtLabel is not None: label="\\label{%s}"%txtLabel
  return "\\section{%s}\n%s"%(chaine,label)
def Titre3(chaine):
  return "\\subsection{%s}\n"%chaine
def Titre4(chaine):
  return "\\underline{%s}\\\\\n"%BF(IT(LG(chaine)))

#-------------------------------------------------------------------------------
# Fonction d'indexation des exigences ou FT
#-------------------------------------------------------------------------------
def indexerExigFT(chaine,formater=False):
  if chaine is None:
    return chaine
  lstch=chaine.split(',')
  idxchaine=''
  for e in lstch:
    if e.find("MAGIDX")==-1:
      e=e.replace(" ",'')
      if formater:
        e=fmtCh(e)
      idxchaine+=", "+e+"\\index{exigences}{"+e+"}"
    else:
      idxchaine+=", "+e
  idxchaine=idxchaine[2:]
  idxchaine=idxchaine.replace("\\index{exigences}{\\n","\\index{exigences}{").replace("\\n}","}")
  return idxchaine

#-------------------------------------------------------------------------------
def largeursTableau(table):
  largeurs=[0]*len(table)
  for numCol in range(len(table)):
    for element in table[numCol]:
      if len(element)>largeurs[numCol]:
        largeurs[numCol]=len(element)
  for numCol in range(len(table)):
    for numElem in range(len(table[numCol])):
      table[numCol][numElem]=fmtCh(table[numCol][numElem])#,formatage=largeurs[numCol]
  return largeurs,table

#-------------------------------------------------------------------------------
# Mise en tableau des documents applicables, de référence et autre...
#-------------------------------------------------------------------------------
def MiseEnTableau(fichiersTables,ID=None,compteur=0):
  texte=''
  for ficTable in fichiersTables:
    if os.access(ficTable,os.F_OK):
      lignes=nngsrc.services.SRV_LireContenuListe(ficTable)
      if ID:
        for e in range(len(lignes)):
          if len(lignes[e])>1:
            texte+="{\\small %s%02d} & "%(ID,compteur)
            ligne=lignes[e].replace('\n','').replace('\r','').split('&')
            try:
              texte+="{\\small %s} & {\\small %s}\\\\\n"%(fmtCh(ligne[0]),fmtCh(ligne[1]))
            except:
              print("ERR:",ficTable,ligne)
            compteur+=1
      else:
        for e in range(len(lignes)):
          if len(lignes[e])>1:
            ligne=lignes[e].replace('\n','').replace('\r','').split('&')
            try:
              texte+="{\\small %s} & {\\small %s}\\\\\n"%(fmtCh(ligne[0]),fmtCh(ligne[1]))
            except:
              print("ERR:",ficTable,ligne)
  return texte,compteur-1

#-------------------------------------------------------------------------------
def ecritureTable(f,table,entete,double=True):
#  (largeurs,table)=largeursTableau(table)
  for numLig in range(len(table[0])):
    if numLig>0: f.write("\\hline\n")
    f.write("%s"%table[0][numLig])
    for numCol in range(1,len(table)):
      f.write("&%s"%table[numCol][numLig])
    f.write("\\\\\n")

#-------------------------------------------------------------------------------
# Fonction de formatage des chaines pour sortie rapport
#-------------------------------------------------------------------------------
def formateChaineLaTeX(objet,alaligne=0,complet=True,tabbing=False,repTravail=None,formatage=0):
  return fmtCh(objet)#,alaligne,complet,tabbing,repTravail,formatage)
#-------------------------------------------------------------------------------
#  chaine=chaine.replace(unicode('\xee\x80\x81','cp850'),"RIGHT")
#  chaine=chaine.replace(unicode('\xee\x80\xbb','cp850'),"NUM_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\x80',codage),"UP")
#  chaine=chaine.replace(unicode('\xee\x80\x82',codage),"DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x83',codage),"LEFT")
#  chaine=chaine.replace(unicode('\xee\x80\x84',codage),"PAGE_UP")
#  chaine=chaine.replace(unicode('\xee\x80\x85',codage),"PAGE_DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x86',codage),"DELETE")
#  chaine=chaine.replace(unicode('\xee\x80\x87',codage),"END")
#  chaine=chaine.replace(unicode('\xee\x80\x88',codage),"HOME")
#  chaine=chaine.replace(unicode('\xee\x80\x89',codage),"INSERT")
#
#  chaine=chaine.replace(unicode('\xee\x80\x91',codage),"F1")
#  chaine=chaine.replace(unicode('\xee\x80\x92',codage),"F2")
#  chaine=chaine.replace(unicode('\xee\x80\x93',codage),"F3")
#  chaine=chaine.replace(unicode('\xee\x80\x94',codage),"F4")
#  chaine=chaine.replace(unicode('\xee\x80\x95',codage),"F5")
#  chaine=chaine.replace(unicode('\xee\x80\x96',codage),"F6")
#  chaine=chaine.replace(unicode('\xee\x80\x97',codage),"F7")
#  chaine=chaine.replace(unicode('\xee\x80\x98',codage),"F8")
#  chaine=chaine.replace(unicode('\xee\x80\x99',codage),"F9")
#  chaine=chaine.replace(unicode('\xee\x80\x9a',codage),"F10")
#  chaine=chaine.replace(unicode('\xee\x80\x9b',codage),"F11")
#  chaine=chaine.replace(unicode('\xee\x80\x9c',codage),"F12")
#
#  chaine=chaine.replace(unicode('\xee\x80\xa0',codage),"SHIFT")
#  chaine=chaine.replace(unicode('\xee\x80\xa4',codage),"PRINTSCREEN")
#  chaine=chaine.replace(unicode('\xee\x80\xa5',codage),"SCROLL_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xa6',codage),"PAUSE")
#  chaine=chaine.replace(unicode('\xee\x80\xa7',codage),"CAPS_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\xb0',codage),"NUM0")
#  chaine=chaine.replace(unicode('\xee\x80\xb1',codage),"NUM1")
#  chaine=chaine.replace(unicode('\xee\x80\xb2',codage),"NUM2")
#  chaine=chaine.replace(unicode('\xee\x80\xb3',codage),"NUM3")
#  chaine=chaine.replace(unicode('\xee\x80\xb4',codage),"NUM4")
#  chaine=chaine.replace(unicode('\xee\x80\xb5',codage),"NUM5")
#  chaine=chaine.replace(unicode('\xee\x80\xb6',codage),"NUM6")
#  chaine=chaine.replace(unicode('\xee\x80\xb7',codage),"NUM7")
#  chaine=chaine.replace(unicode('\xee\x80\xb8',codage),"NUM8")
#  chaine=chaine.replace(unicode('\xee\x80\xb9',codage),"NUM9")
#  chaine=chaine.replace(unicode('\xee\x80\xba',codage),"SEPARATOR")
#  chaine=chaine.replace(unicode('\xee\x80\xbb',codage),"NUM_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xbc',codage),"ADD")
#  chaine=chaine.replace(unicode('\xee\x80\xbd',codage),"MINUS")
#  chaine=chaine.replace(unicode('\xee\x80\xbe',codage),"MULTIPLY")
#  chaine=chaine.replace(unicode('\xee\x80\xbf',codage),"DIVIDE")
#
#  chaine=chaine.replace(unicode('\x1b',codage),"ESC")
#  chaine=chaine.replace(unicode('\x08',codage),"BACKSPACE")

def fmtCh(objet,tt=False,repTravail=None):#,alaligne=0,complet=True,tabbing=False,formatage=0):,alaligne,complet,tabbing,repTravail,formatage
  if objet is None:
    return objet
  if type(objet)==str:
    chaine=objet
  elif type(objet)==list:
    return [fmtCh(ch) for ch in objet]
  else:
    chaine=str(objet)

  chaine=chaine.replace('\\','\\textbackslash')        # Parce qu'il le faut bien !
  chaine=chaine.replace('{','\\{')                     # Parce qu'il le faut bien !
  chaine=chaine.replace('}','\\}')                     # Parce qu'il le faut bien !
  chaine=chaine.replace('$','{\\textdollar}')          # Parce qu'il le faut bien !
  chaine=chaine.replace('^','{\\textasciicircum}')     # Parce qu'il le faut bien !
  chaine=chaine.replace('#','$\\sharp$')
  chaine=chaine.replace('n°','{\\no}')

  idxDeb=chaine.find("[IMAGE:")
  lstIMA=[]
  idx=-1
  while idxDeb!=-1:
    idxfin=chaine.find("]")
    aidx=chaine[idxDeb+7:idxfin]
    if os.access(aidx,os.F_OK):
      nngsrc.services.SRV_copier(aidx,os.path.join(repTravail,os.path.basename(aidx)),timeout=5,signaler=True)
    idx+=1
    chaine=chaine.replace("[IMAGE:"+aidx+"]","[IMAGE:"+str(idx)+"]")
    lstIMA.append("\\includegraphics[width=15.00cm]{"+aidx+"}")
    idxDeb=chaine.find("[IMAGE:")

  lstIHM=[]
  idIHM=-1
  idxDeb=chaine.find("[IHM:")
  while idxDeb!=-1:
    idxfin=chaine.find(":MHI]")
    aidx=chaine[idxDeb+5:idxfin]
    idxdim=aidx.find("#")
    # recherche de #l ou #h
    echelle="height=10mm"
    if idxdim!=-1:
      fichier=nngsrc.services.SRV_retraitIndexation(aidx[:idxdim])
      if aidx[idxdim+1]=="l":
        echelle="width=%smm"%(aidx[idxdim+2:])
      elif aidx[idxdim+1]=="h":
        echelle="height=%smm"%(aidx[idxdim+2:])
      else:
        echelle="height=10mm"
    else:
      fichier=nngsrc.services.SRV_retraitIndexation(aidx)

    if not os.path.exists(fichier):
      fichier=os.path.join(nngsrc.services.varNANGU,'ressources','FichierInexistant.png')

    destaidx=os.path.join(repTravail,REPERTRAPPORT,os.path.basename(fichier))
    if os.access(fichier,os.F_OK) and not os.access(destaidx,os.F_OK) :
      nngsrc.services.SRV_copier(fichier,destaidx,timeout=5,signaler=True)
    idIHM+=1
    chaine=chaine.replace("[IHM:"+aidx+":MHI]","[MHI:"+str(idIHM)+"-")
    lstIHM.append("\\includegraphics[%s]{%s}"%(echelle,os.path.basename(fichier)))
    idxDeb=chaine.find("[IHM:")

  # TT dans le cas où on récupère un rapport de sortie d'exécution ou on décrit une commande, il faut formater sans - d'hyphenation
  if tt:
    chaine=chaine.replace('_','{\\bku}')
    chaine=chaine.replace('/','/{\\sep}')
    chaine=chaine.replace('.','.{\\sep}')
  else:
    chaine=chaine.replace('_','\\-\\_')
    chaine=chaine.replace('/','/\\-')
  chaine=chaine.replace('`','\\`{}')
  chaine=chaine.replace('&','\\&')
  chaine=chaine.replace('%ficn%','}\\\\\n\\texttt{')

  chaine=chaine.replace('%','\\%')
  chaine=chaine.replace('$magvalid$index','\\index')        # Oui, à la fin !
  chaine=chaine.replace('$magvalid$','$\\backslash$\\-')    # Oui, à la fin !

  chaine=chaine.replace('$\\backslash$\\-textdollar','\\textdollar')                 # Parce qu'il le faut bien !
  chaine=chaine.replace('$\\backslash$\\-textasciicircum','\\textasciicircum')       # Parce qu'il le faut bien !

  # indexation MAGIDX:blabla:IDXMAG
  idxdeb=chaine.find("MAGIDX:")
  while idxdeb!=-1:
    idxfin=chaine.find(":IDX:")
    aidx=chaine[idxdeb+7:idxfin]
    labidx=chaine.find(":IDXMAG")
    lidx=chaine[idxfin+5:labidx]
    chaine=chaine.replace("MAGIDX:"+aidx+":IDX:"+lidx+":IDXMAG",aidx+"\\index{general}{"+lidx.replace("$\\backslash$",'/')+"}")
    idxdeb=chaine.find("MAGIDX:")

  chaine=chaine.replace('§','\\S ')
  chaine=chaine.replace('à','\\`a')
  chaine=chaine.replace('â','\\^a')
  chaine=chaine.replace('ç','\\c{c}')
  chaine=chaine.replace('è','\\`e')
  chaine=chaine.replace('é',"\\'e")
  chaine=chaine.replace('ê','\\^e')
  chaine=chaine.replace('î','\\^i')
  chaine=chaine.replace('ô','\\^o')
  chaine=chaine.replace('ù','\\`u')
  chaine=chaine.replace('û','\\^u')

  if idx!=-1:
    for i in range(idx+1):
      chaine=chaine.replace("[IMAGE:"+str(i)+"]",lstIMA[i])

  if idIHM!=-1:
    for i in range(idIHM+1):
      chaine=chaine.replace("[MHI:"+str(i)+"-",lstIHM[i])

  return chaine

#-------------------------------------------------------------------------------
def fmtStyle(sty,valeur):
  if sty=="tt":
    return TT(valeur)
  if sty=="it":
    return IT(valeur)
  if sty=="ittt":
    return IT(TT(valeur))
  return valeur
#-------------------------------------------------------------------------------
def IT(chaine):
  if chaine=='': return chaine
  return "\\textit{"+chaine+"}"

#-------------------------------------------------------------------------------
def BF(chaine):
  if chaine=='': return chaine
  return "\\textbf{"+chaine+"}"

#-------------------------------------------------------------------------------
def TT(chaine):
  if chaine=='': return chaine
  return "\\texttt{"+chaine+"}"

#-------------------------------------------------------------------------------
def FT(chaine):
  if chaine=='': return chaine
  return "\\footnotesize{"+chaine+"}"

#-------------------------------------------------------------------------------
def LG(chaine):
  if chaine=='': return chaine
  return "\\large{"+chaine+"}"

#-------------------------------------------------------------------------------
def retraitSautAvant(texte,sousTexte,deb,dBloc):
  if texte[deb-len(dBloc)-3:deb-len(dBloc)]=='\\\\\n':
    deb-=3
    if sousTexte: sousTexte='\n'+sousTexte
  return deb,sousTexte
def retraitSautApres(texte,sousTexte,fin,fBloc):
  if texte[fin+len(fBloc):fin+len(fBloc)+3]=='\\\\\n':
    fin+=3
#    sousTexte=sousTexte+'\n'
  return fin
#-------------------------------------------------------------------------------
def convertionBloc(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  sousTexte=texte[deb:fin]
  if dBloc=='[b]':                                                       # gras
    sousTexte=BF(sousTexte)
  elif dBloc=='[i]':                                                     # italique
    sousTexte=IT(sousTexte)
  elif dBloc=='[u]':                                                     # souligne
    sousTexte='\\underline{%s}'%sousTexte
  elif dBloc=='[font=Courier]':                                          # Courier
    sousTexte='\\texttt{%s}'%sousTexte
  elif dBloc=='[font=Serif]':                                            # Serif
    sousTexte='\\textrm{%s}'%sousTexte
  elif dBloc=='[size=7]':                                                # tiny
    sousTexte='{\\tiny %s}'%sousTexte
  elif dBloc=='[size=8]':                                                # scriptsize
    sousTexte='{\\scriptsize %s}'%sousTexte
  elif dBloc=='[size=9]':                                                # footnotesize
    sousTexte='{\\footnotesize %s}'%sousTexte
  elif dBloc=='[size=10]':                                               # small
    sousTexte='{\\small %s}'%sousTexte
  elif dBloc=='[size=11]':                                               # normalsize
    sousTexte='{\\normalsize %s}'%sousTexte
  elif dBloc=='[size=12]':                                               # large
    sousTexte='{\\large %s}'%sousTexte
  elif dBloc=='[size=13]':                                               # Large
    sousTexte='{\\Large %s}'%sousTexte
  elif dBloc=='[size=14]':                                               # LARGE
    sousTexte='{\\LARGE %s}'%sousTexte
  elif dBloc=='[size=15]':                                               # huge
    sousTexte='{\\huge %s}'%sousTexte
  elif dBloc=='[size=16]':                                               # Huge
    sousTexte='{\\Huge %s}'%sousTexte

  elif dBloc=='[color=red]':                                             # red
    sousTexte='\\textcolor{red}{%s}'%sousTexte
  elif dBloc=='[color=green]':                                           # green
    sousTexte='\\textcolor{green}{%s}'%sousTexte
  elif dBloc=='[color=blue]':                                            # blue
    sousTexte='\\textcolor{blue}{%s}'%sousTexte
  elif dBloc=='[color=cyan]':                                            # cyan
    sousTexte='\\textcolor{cyan}{%s}'%sousTexte
  elif dBloc=='[color=magenta]':                                         # magenta
    sousTexte='\\textcolor{magenta}{%s}'%sousTexte
  elif dBloc=='[color=yellow]':                                          # yellow
    sousTexte='\\textcolor{yellow}{%s}'%sousTexte

  elif dBloc=='[title=1]':                                               # titre1
    sousTexte=Titre1(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=2]':                                               # titre2
    sousTexte=Titre2(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=3]':                                               # titre3
    sousTexte=Titre3(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=4]':                                               # titre4
    sousTexte=Titre4(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[li]':                                                    # item
    sousTexte='\\item %s\n'%sousTexte
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[ol]':                                                    # enumerate
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{enumerate}\n%s\\end{enumerate}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[ul]':                                                    # itemize
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{itemize}\n\\setlength{\\itemsep}{0pt}\n%s\\end{itemize}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[left]':                                                  # left
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{flushleft}\n%s\\end{flushleft}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[right]':                                                 # right
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{flushright}\n%s\\end{flushright}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[center]':                                                # center
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{center}\n%s\\end{center}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[quote]':                                                 # label
    sousTexte='%s\\index{general}{%s}'%(sousTexte,sousTexte)

  elif dBloc=='[img]':                                                   # includegraphics
    sousTexte='\\includegraphics{%s}'%sousTexte

  elif dBloc=='[table col=':                                             # table
    icol=sousTexte.find(']')
    colonnes=sousTexte[:icol]
    sousTexte=sousTexte[icol+4:]
    sousTexte='\n\\begin{nngtabular}{%s}\n%s\\end{nngtabular}\n'%(colonnes,sousTexte)
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[tr][th]':                                                # table
    sousTexte='{%s}\n'%sousTexte
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[tr][td]':                                                # table
    sousTexte='%s'%sousTexte

  elif dBloc=='[anchor=':                                                # lien
    icol=sousTexte.find(']')
    ancre=sousTexte[:icol].replace('\\-\\_','').replace('$\\sharp$','')
    sousTexte=sousTexte[icol+1:]
    sousTexte='%s\\label{%s}'%(sousTexte,ancre)
#    deb,_=retraitSautAvant(texte,None,deb,dBloc)
#    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+convertionBloc(dBloc,fBloc,texte[fin:])

#-------------------------------------------------------------------------------
def convertionBlocImg(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  taille=''
  if texte[deb]!=']': # on récupère une taille
    taille="[%s]"%texte[deb+1:deb+1+texte[deb+1:].find(']')]
    decalage=len(taille)
  else:
    decalage=1
  fichier=texte[deb+decalage:fin].replace('\\-\\_','_')
  sousTexte='\\includegraphics%s{%s}'%(taille,fichier)
  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+texte[fin:],fichier

#-------------------------------------------------------------------------------
def convertionBlocInc(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  sousTexte=texte[deb:fin]
  fichierAutre=sousTexte.replace('\\-\\_','_')
  sousTexte='\\input %s\n'%fichierAutre
  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+texte[fin:],fichierAutre

#-----------------------------------------------------------------------------------
# Classe du rapport
#-----------------------------------------------------------------------------------
class TEX:

  #---------------------------------------------------------------------------------
  # Initialisation de la classe
  #---------------------------------------------------------------------------------
  def __init__(self,repTravail,repRessources,repRapport,ficRapport,projet,qualite,version,bilan,constantes,IDcahier):
    # Parametres communs du rapport
    self.repTravail=repTravail
    self.repRapport=repRapport
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.repCampagne=bilan
    self.repRessources=repRessources

    # Parametres specifique du rapport
    self.ficRapport=ficRapport
    self.numAnnexe=0
    self.indexGeneral=0
    self.indexExigences=0

    # Creation des repertoires
    if not os.path.exists(os.path.join(self.repTravail,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repTravail,REPERTRAPPORT))
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*%s'%DOCEXTENSION)):
      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
    if not os.path.exists(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))

    # Elements des Faits techniques
    if self.projet.REF_FT is None:
      self.refFT="%s-FT"%(self.projet.VALIDATION_REFERENCE+'-'+self.projet.DOCUMENT_EDITION+'-'+self.projet.DOCUMENT_REVISION)
    else:
      self.refFT=self.projet.REF_FT

    # Titre, sous-tire version et nom du projet
    if self.projet.PRODUIT_TITRE != '':
      titre=fmtCh(self.projet.PRODUIT_TITRE)
    else:
      titre=self.fmtLex("MOT_RAPPORT")
    sstitre=fmtCh(self.projet.PRODUIT_SOUS_TITRE)
    if sstitre=='':
      sstitre=fmtCh(self.projet.PRODUIT_SOUS_REFERENCE[1:])
    if self.projet.PRODUIT_NOM != '':
      nom=fmtCh(self.projet.PRODUIT_NOM)
    else:
      nom=self.fmtLex("LOCUT_SANS_NOM")
    if self.projet.PRODUIT_SOCIETE=='':
      self.projet.PRODUIT_SOCIETE="Magellium"
    self.version=version
    self.listeBBCimageInclusion=[]
    # Type de document
    if self.projet.execution=='P':
      doctype=self.projet.lexique.entree('DOCTYPE_P')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='C':
      doctype=self.projet.lexique.entree('DOCTYPE_C')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='S':
      doctype=self.projet.lexique.entree('DOCTYPE_S')%self.projet.PRODUIT_TYPE
    else:
      doctype=self.projet.lexique.entree('DOCTYPE_R')%self.projet.PRODUIT_TYPE
 #   doctype=fmtCh(doctype)
    methode="Test"
    if self.projet.DOCUMENT_METHODE is not None: # C'est le cas dans le mode campagne
      methode=self.projet.methode[self.projet.DOCUMENT_METHODE]['terme']
    docMethode=self.projet.lexique.methode(methode)
    if bilan:
      self.Campagne_InitRapport(nom)
    else:
      self.Valid_InitRapport(nom,titre,sstitre,doctype,docMethode,IDcahier)

  #-------------------------------------------------------------------------------
  def TexteConversion(self,texte,dirname='',varoperateur=None):
    if varoperateur:
      for v in varoperateur:
        if v['UTILE']:
          nom='['+v['NOM']+']'
          valeur=v['VALEUR']
          while texte.find(nom)!=-1:
            texte=texte.replace(nom,valeur)
    texte=fmtCh(texte)#,1)#PDN
    texte=texte.replace('\r','')
    texte=texte.replace('\n\n','\n$SDL$\n')
    texte=texte.replace('\n','\\\\\n')

    texte=convertionBloc('[b]','[/\\-b]',texte)
    texte=convertionBloc('[i]','[/\\-i]',texte)
    texte=convertionBloc('[u]','[/\\-u]',texte)
    texte=convertionBloc('[font=Courier]','[/\\-font]',texte)
    texte=convertionBloc('[font=Serif]','[/\\-font]',texte)

    texte=convertionBloc('[title=1]','[/\\-title]',texte)
    texte=convertionBloc('[title=2]','[/\\-title]',texte)
    texte=convertionBloc('[title=3]','[/\\-title]',texte)
    texte=convertionBloc('[title=4]','[/\\-title]',texte)

    if texte.find('[quote]')>=0:
      self.indexGeneral+=1
      texte=convertionBloc('[quote]','[/\\-quote]',texte)

    while texte.find('[inc]')!=-1:
      texte,fichierAutre=convertionBlocInc('[inc]','[/\\-inc]',texte)
      bbcode=nngsrc.services.SRV_LireContenu(os.path.join(dirname,fichierAutre+".bbc"))
      bbcode=self.TexteConversion(bbcode,dirname)
      fi=open(os.path.join(self.repTravail,REPERTRAPPORT,fichierAutre+DOCEXTENSION),"w",encoding='utf-8')
      fi.write(bbcode)
      fi.close()

    while texte.find('[img')!=-1:
      texte,fichierImg=convertionBlocImg('[img','[/\\-img]',texte)
      self.listeBBCimageInclusion.append(os.path.join(dirname,fichierImg))

    texte=convertionBloc('[color=red]','[/\\-color]',texte)
    texte=convertionBloc('[color=green]','[/\\-color]',texte)
    texte=convertionBloc('[color=blue]','[/\\-color]',texte)
    texte=convertionBloc('[color=cyan]','[/\\-color]',texte)
    texte=convertionBloc('[color=magenta]','[/\\-color]',texte)
    texte=convertionBloc('[color=yellow]','[/\\-color]',texte)

    texte=convertionBloc('[center]','[/\\-center]',texte)
    texte=convertionBloc('[left]','[/\\-left]',texte)
    texte=convertionBloc('[right]','[/\\-right]',texte)

    texte=convertionBloc('[size=7]','[/\\-size]',texte)
    texte=convertionBloc('[size=8]','[/\\-size]',texte)
    texte=convertionBloc('[size=9]','[/\\-size]',texte)
    texte=convertionBloc('[size=10]','[/\\-size]',texte)
    texte=convertionBloc('[size=11]','[/\\-size]',texte) # Par défaut
    texte=convertionBloc('[size=12]','[/\\-size]',texte)
    texte=convertionBloc('[size=13]','[/\\-size]',texte)
    texte=convertionBloc('[size=14]','[/\\-size]',texte)
    texte=convertionBloc('[size=15]','[/\\-size]',texte)
    texte=convertionBloc('[size=16]','[/\\-size]',texte)

    texte=convertionBloc('[ul]','[/\\-ul]',texte)
    texte=convertionBloc('[ol]','[/\\-ol]',texte)
    texte=convertionBloc('[li]','[/\\-li]',texte)

    texte=convertionBloc('[anchor=','[/\\-anchor]',texte)

    texte=convertionBloc('[table col=','[/\\-table]',texte)
    texte=convertionBloc('[tr][th]','[/\-th][/\\-tr]',texte)
    texte=convertionBloc('[tr][td]','[/\-td][/\\-tr]',texte)
    texte=texte.replace('[/\\-td][td]',' & ')
    texte=texte.replace('[/\\-th][th]',' & ')
    texte=texte.replace('[hr/\-]','\\hline')

    texte=texte.replace('[br/\\-]\\\\\n','\\-\\\\\n')
    texte=texte.replace('[br/\\-]','\\\\\n')
    texte=texte.replace('$SDL$\\\\\n','\\-\\\\\n')
    texte=texte.replace('$SDL$\n','\\-\\\\\n')
    if texte[-3:]=='\\\\\n':
      texte=texte[:-3]+'\n'
    return texte

  #-------------------------------------------------------------------------------
  def inclureFichier(self,f,fichier,bbc=False):
    fichierAinclure=os.path.basename(fichier)+DOCEXTENSION
    if bbc:
      bbcode=nngsrc.services.SRV_LireContenu(fichier+".bbc")
  #  bbcode=nngsrc.services.SRV_Encodage(bbcode,"ERREUR -%s- : Recuperation du fichier .txt"%REPERTRAPPORT)
      bbcode=self.TexteConversion(bbcode,os.path.dirname(fichier))
      fi=open(os.path.join(self.repTravail,REPERTRAPPORT,fichierAinclure),"w",encoding='utf-8')
      fi.write(bbcode)
      fi.close()
    f.write("\\input %s\n"%os.path.basename(fichierAinclure))

  #-------------------------------------------------------------------------------
  # formatage du lexique
  #-------------------------------------------------------------------------------
  def fmtLex(self,motclef,complement=None):
    if complement is not None:
      return fmtCh(self.projet.lexique.entree(motclef)%self.projet.lexique.entree(complement))
    return fmtCh(self.projet.lexique.entree(motclef))

  #-------------------------------------------------------------------------------
  def quelItem(self,inRapport):
    if inRapport!=-1:
      return itemSCEAct
    else:
      return itemSCE

  #-------------------------------------------------------------------------------
  # Ecriture du bloc commentaire de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_description_debut(self,texte=None,inRapport=0):
    if self.projet.execution=='S':
      return self.quelItem(inRapport)+" "
#    elif self.projet.execution=='C':
#      return "\\begin{tabularx}{16cm}{|Xp{1.2cm}p{1.2cm}|}\n\\hline\n\\rowcolor{nnggrey} & &\\\\\n"
    elif texte is not None:
      return "%-------------------------------------------------\n\\renewcommand{\\arraystretch}{0.8}\n\\begin{tabularx}{16cm}{|Xp{2.5cm}|}\n\\hline\n\\rowcolor{nnggrey} & {\\scriptsize "+texte+"} \\\\\n"
    else:
      return "%-------------------------------------------------\n\\renewcommand{\\arraystretch}{0.8}\n\\begin{tabularx}{16cm}{|X|}\n\\hline\n\\rowcolor{nnggrey} \\\\\n"

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_description_milieu(self,normal='',petit='',item='',exigence='',ft=None,inRapport=None):
    esperluette="&"
    if self.projet.execution=='S':
      esperluette=''
    texte=''
    if item!='':
      texte+=item+" - "
    texte+=normal
    if petit!='':
      texte+="{\\tiny "+petit+"}"
    if exigence!='' and exigence is not None:
      exigence=','.join(["[%s]"%e for e in exigence.split(',')])
      if self.projet.execution=='S':
        texte+="\\hspace*{1pc}{\\scriptsize %s %s}"%(self.fmtLex("LOCUT_EXIGENCES_SPECIFIQUES"),exigence)
      else:
        texte+="\\hspace*{1pc}{\\scriptsize %s %s}"%(self.fmtLex("LOCUT_EXIGENCES_SPECIFIQUES"),exigence)

    if ft is not None:
      if ft!=-1:
        if self.projet.execution=='S':
          texte+="%s\\\\ \\hspace*{1pc}{\\scriptsize %s '%s'}"%(esperluette,self.fmtLex("LOCUT_ACTION_JUGEE"),self.fmtLex("MOT_NIVEAU_CRITIQUE_%d"%ft))
        else:
          texte+="%s{\\scriptsize '%s'}"%(esperluette,self.fmtLex("MOT_NIVEAU_CRITIQUE_%d"%ft))
      else:
        texte+="%s "%esperluette
    else:
      texte+=" "
    if self.projet.execution=='S':
      if texte==" " or texte=="  ":
        return ''
      else:
        return "%s\\\\\n"%texte
    if inRapport is not None:
      texte="%s %s"%(self.quelItem(inRapport),texte)
    return "\\rowcolor{nnggrey} %s\\\\\n"%texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  def rapport_description_fin(self):
    if self.projet.execution=='S':
      return ''
    return "\\hline\n\\end{tabularx}\\\\\n\\renewcommand{\\arraystretch}{1.0}\n"

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_contenu_deb(self,message):
    if self.projet.execution=='S':
      if message=='':
        return ''
      else:
        return "\\textit{%s}\\\\\n"%message
    return "\\begin{tabularx}{16cm}{|X|}\n\\hline\n%s\\\\\n"%BF(message)

  # 2- Ecriture du contenu du bloc
  def rapport_contenu_mil(self,action,option):
    if self.projet.execution=='S':
      if action+str(option)=='':
        return ''
      else:
        return "%s %s\\\\\n"%(action,str(option))
    if type(option)==list:
      texte=''
      for (un,deux) in option:
        texte+="%s\\\\\n"%(action%(TT(fmtCh(un)),TT(fmtCh(deux))))
      return texte
    return "%s %s\\\\\n"%(action,str(option))

  # 3- Ecriture de la fin du bloc
  def rapport_contenu_fin(self):
    if self.projet.execution=='S':
      return ''
    return "\\hline\n\\end{tabularx}\\\\\n"

  # T- Tout en un
  def rapport_contenu(self,message,action,option,lien=False):
    texte =self.rapport_contenu_deb(message)
    texte+=self.rapport_contenu_mil(action,option)
    texte+=self.rapport_contenu_fin()
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  @staticmethod
  def rapport_contenuMode_deb(clef):
    #texte="\\begin{tabularx}{16cm}{|X|}\n\\hline\n%s\\\\\n"%BF(clef)
    texte="\\begin{longtable}{|p{15.55cm}|}\n\\hline\n%s\\\\\n"%BF(clef)
    return texte

  # 2- Ecriture du contenu du bloc
  @staticmethod
  def rapport_contenuMode_mil(valeur, substit, style, lien=False):
    if len(substit)>0:
      valeur+=" "
      valeur=valeur.split("%s")
      #texte=fmtCh(valeur[0])
      texte=valeur[0]
      for e in range(len(substit)):
        texte+="%s%s"%(fmtStyle(style,substit[e]),valeur[e+1])
      texte+="\\\\\n"
    else:
      texte="%s\\\\\n"%fmtStyle(style,valeur)
    return texte

  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_contenuMode_fin():
    #texte="\\hline\n\\end{tabularx}\\\\\n"
    texte="\\hline\n\\end{longtable}\n"
    return texte

  # T- Tout en un
  def rapport_contenuMode(self,clef,valeur,substit,style,lien=False):
    texte =self.rapport_contenuMode_deb(clef)
    texte+=self.rapport_contenuMode_mil(valeur,substit,style,lien)
    texte+=self.rapport_contenuMode_fin()
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action - Cas : Plan de Validation
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_aTester_debut(self,message,retour=None):
    texte=''
    if self.projet.execution=='S':
      texte=''
    elif self.projet.execution=='P':
      # Deux colonnes
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|}\n\\hline\n%s&%s\\\\\n"%(Atester,BF(message))
    else:
      if retour:
        # Quatre colonnes
        texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{3.2cm}|p{1.2cm}|}\n\\hline\n%s&\\textbf{%s} & %s &%s\\\\\n"%(Atester,message,retour,TResult)
      else:
        # Trois colonnes
        texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{1.2cm}|}\n\\hline\n%s&\\textbf{%s} &%s\\\\\n"%(Atester,message,TResult)
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_aTester_milieu(self,action,objet,inRapport,bilan='',suspendu=False,raison='',cssBilan=None,retour=None,valeur=''):
    if suspendu: bilan="SUS"
    if type(action)!=str:
      action=str(action)
    if type(objet)!=str:
      objet=str(objet)
    if type(valeur)!=str:
      valeur=str(valeur)
    if self.projet.execution=='S':
      texte=''
    elif self.projet.execution=='P':
      # Deux colonnes
      texte="&%s%s\\\\\n"%(action,FT(TT(objet)))
    else:
      if retour is None:
        # Trois colonnes
        texte="&%s%s&\\\\\n"%(action,FT(TT(objet)))
      else:
        # Quatre colonnes
        texte="&%s%s&%s&%s\\\\\n"%(action,FT(TT(objet)),BF(TT(valeur)),bilan)
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  def rapport_aTester_fin(self,bilan=None,raison=''):
    if self.projet.execution=='S':
      return ''
    texte=''
    if bilan=='AR': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='areserve'>AR</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  # T- Tout en un
  def rapport_aTester(self,message,action,objet,inRapport,bilan='',suspendu=False,raison=None,cssBilan=None,retour=None,valeur=None):
    texte =self.rapport_aTester_debut(message,retour)
    texte+=self.rapport_aTester_milieu(action,objet,inRapport,bilan,suspendu,raison,cssBilan,retour,valeur)
    texte+=self.rapport_aTester_fin(bilan,raison)
    return texte

  #-----------------------------------------------------------------------------------
  def Campagne_InitRapport(self,nom):
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()

    # Fichier principal
    nomBilan=os.path.basename(self.repCampagne)
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,nomBilan+DOCEXTENSION),"w",encoding='utf-8')
    f.write("\\documentclass[a4paper,11pt,openany]{book}\n")
    f.write("\\usepackage{%s}\n"%self.projet.lexique.entree('DOCTYPE_LATEX'))

    #--------Filigrane---
    if self.projet.DOCUMENT_FILIGRANE != '':
      f.write("\\AddToShipoutPicture*{\n")
      f.write("\\AtTextCenter{%\n")
      f.write("\\makebox(0,0)[c]{\\resizebox{\\textwidth}{!}{%\n")
      f.write("\\rotatebox{45}{\\textsf{\\textbf{\\color{lightgray}"+fmtCh(self.projet.DOCUMENT_FILIGRANE)+"}}}\n")
      f.write("}}}}\n")
      f.write("\\definecolor{grisversion}{gray}{0.90}\n")
      f.write("\\AddToShipoutPicture{%\n")
      f.write("\\unitlength=1mm %\n")
      f.write("\\put(5,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} \\centering {\\textcolor{gray}{\\textbf{\\Huge{%s}}}}\\end{minipage}}}\n"%fmtCh(self.projet.DOCUMENT_FILIGRANE))
      f.write("\\put(199,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} \\centering {\\textcolor{gray}{\\textbf{\\Huge{%s}}}}\\end{minipage}}}\n"%fmtCh(self.projet.DOCUMENT_FILIGRANE))
      f.write("\\put(5,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} {\\textcolor{gray}{\\tiny{%s \includegraphics[width=0.20cm]{nangu.png}NANGU v%s}}}\n"%(fmtCh(self.projet.lexique.entree('LOCUT_GENERE_PAR')),self.version))
      f.write("}\n")

    #--------Début du document---
    f.write("\\begin{document}\n\n")
    f.write("\\frontmatter\n\n")
    f.write("\\widowpenalty=10000\n")
    f.write("\\clubpenalty=10000\n")
    f.write("\\raggedbottom\n\n")
    #-  Nom du projet
    f.write("\\project{%s}\n"%nom)
    #-  Type de document
    f.write("\\documentType{%s}\n"%self.fmtLex('LOCUT_BILAN_QUALIF'))
    f.write("\\documentMethode{}\n")
    #-  Campagne
    f.write("\\documentCampagne{}\n")
    #-  Titre du document
    f.write("\\documentTitle{%s}\n"%self.fmtLex('MOT_CAMPAGNE'))
    f.write("\\documentSsTitle{v%s}\n"%fmtCh(self.projet.PRODUIT_VERSION))
    #-  Référence du document
    f.write("\\documentReference{"+fmtCh(nomBilan)+"}\n")
    #-  Edition/Révision du document
    f.write("\\documentRevision{1.0}\n") #TODO
    #-  Date du document
    f.write("\\documentDate{"+t.strftime("%d/%m/%Y")+"}\n")
    #-  Client
    f.write("\\client{"+fmtCh(self.projet.PRODUIT_CLIENT)+"}\n")
    f.write("\\logoClient{"+os.path.basename(self.projet.CLIENT_LOGO)+"}\n")
    f.write("\\consultationReference{"+fmtCh(self.projet.PRODUIT_CONTRAT)+"}\n\n")
    #-  Societe
    f.write("\\societe{"+fmtCh(self.projet.PRODUIT_SOCIETE)+"}\n")
    f.write("\\logoSociete{"+os.path.basename(self.projet.SOCIETE_LOGO)+"}\n")
    f.write("\\societeadresse{"+fmtCh(self.projet.SOCIETE_ADRESSE)+"}\n\n")
    f.write("\\societecoordonnees{"+fmtCh(self.projet.SOCIETE_COORDONNEES)+"}\n\n")
    f.write("\\societepropriete{"+fmtCh(self.projet.SOCIETE_PROPRIETE)+"}\n\n")
    #-  Rédigé par...
    f.write("\\writeBy{"+fmtCh(self.projet.DOCUMENT_REDIGE_PAR)+"}\n")
    #-  Rédigé org...
    f.write("\\writeByCorp{"+fmtCh(self.projet.DOCUMENT_REDIGE_ORG)+"}\n")
    #-  Rédigé le...
    f.write("\\writeDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_REDIGE_DATE,self.projet)+"}\n")
    #-  Rédigé sign...
    f.write("\\writeSign{"+os.path.basename(self.projet.DOCUMENT_REDIGE_SIGN)+"}\n")
    #-  Vérifié par...
    f.write("\\verifyBy{"+fmtCh(self.projet.DOCUMENT_VERIFIE_PAR)+"}\n")
    #-  Vérifié org...
    f.write("\\verifyByCorp{"+fmtCh(self.projet.DOCUMENT_VERIFIE_ORG)+"}\n")
    #-  Vérifié le...
    f.write("\\verifyDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_VERIFIE_DATE,self.projet)+"}\n")
    #-  Vérifié sign...
    f.write("\\verifySign{"+os.path.basename(self.projet.DOCUMENT_VERIFIE_SIGN)+"}\n")
    #-  Validé par...
    f.write("\\validateBy{"+fmtCh(self.projet.DOCUMENT_VALIDE_PAR)+"}\n")
    #-  Validé org...
    f.write("\\validateByCorp{"+fmtCh(self.projet.DOCUMENT_VALIDE_ORG)+"}\n")
    #-  Validé le...
    f.write("\\validateDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_VALIDE_DATE,self.projet)+"}\n")
    #-  Validé sign...
    f.write("\\validateSign{"+os.path.basename(self.projet.DOCUMENT_VALIDE_SIGN)+"}\n")
    f.write("\\writebisBy{}\n")
    f.write("\\writebisByCorp{}\n")
    f.write("\\writebisDate{}\n")
    f.write("\\writebisSign{}\n")
    f.write("\\verifybisBy{}\n")
    f.write("\\verifybisByCorp{}\n")
    f.write("\\verifybisDate{}\n")
    f.write("\\verifybisSign{}\n")
    f.write("\\maketitle\n\n")
    #--------Diffusion du document---
    f.write("\\begin{nnglistediffusion}\n")
    table=[[],[],[],[]]
    if self.projet.DOCUMENT_DIFFUSION=='':
      Lignes=[]
    else:
      Lignes=self.projet.DOCUMENT_DIFFUSION.split('*')
    Premiere=True
    for ligne in Lignes:
      if Premiere:
        Premiere=False
        table[0].append(BF(self.fmtLex('MOT_DESTINATAIRES')))
      else:
        table[0].append('')
      for numElem in range(len(ligne.split('&'))):
        table[numElem+1].append(fmtCh(ligne.split('&')[numElem]))
    table[0].append(BF(self.fmtLex('LOCUT_COPIE_INTERNE')))
    table[1].append(self.fmtLex('LOCUT_DOSSIER_PROJET'))
    table[2].append(fmtCh(self.projet.PRODUIT_SOCIETE))
    table[3].append(self.fmtLex('LOCUT_UN_NUMERIQUE'))#"1 (numérique)"))
    ecritureTable(f,table,True)
    f.write("\\end{nnglistediffusion}\n\\-\\\\\n\n")
    #--------Table des matières---
    f.write("\\tableofcontents\n\n")
    f.write("\\mainmatter\n\n")
    #-----------------------------
#    f.write(Titre1(self.fmtLex('LOCUT_FEUILLE_ROUTE')))
#    f.write(self.fmtLex('INTRO_FEUILLE_ROUTE')+"\\\\\n")
#    f.write("\\IfFileExists{feuilleRoute.tex}{\\input feuilleRoute}{}\n\n")
    f.write(Titre1(self.fmtLex('MOT_RAPPORTS')))
    f.write(Titre2(self.fmtLex('LOCUT_FEUILLE_ROUTE')))
    f.write(self.fmtLex('INTRO_FEUILLE_ROUTE')+"\\\\\n")
    f.write("\\IfFileExists{deroulement.tex}{\\input deroulement}{}\n\n")
    f.write(Titre2(self.fmtLex('LOCUT_FAITS_TECHS_OUVERTS')))
    f.write(self.fmtLex('INTRO_FAITS_TECHS_OUVERTS')+"\\\\\n")
    f.write("\\IfFileExists{FTemis.tex}{\\input FTemis}{}\n\n")
    f.write(Titre2(self.fmtLex('LOCUT_COUVERTURE_EXIGENCES')))
    f.write(self.fmtLex('INTRO_COUVERTURE_EXIGENCES')+"\\\\\n")
    f.write("\\IfFileExists{exigencesCouvertes.tex}{\\input exigencesCouvertes}{}\n\n")
    #-----------------------------
    f.write(Titre1(self.fmtLex('MOT_BILAN')))
    f.write(Titre2(self.fmtLex('LOCUT_BILAN_TODO')))
    f.write("\\IfFileExists{FTtodo.tex}{\\input FTtodo}{}\n\n")
    f.write(Titre2(self.fmtLex('LOCUT_BILAN_EXIGENCES')))
    f.write("\\IfFileExists{BilanExigences.tex}{\\input BilanExigences}{}\n\n")
    f.write(Titre2(self.fmtLex('LOCUT_BILAN_CONTROLES')))
    f.write("\\IfFileExists{BilanControles.tex}{\\input BilanControles}{}\n\n")
    #-----------------------------
    f.write("\\end{document}\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Feuille de route
  #-------------------------------------------------------------------------------
  def Campagne_FeuilleRouteDeroulement(self,Ref,CR):
    deb=CR[5].split(':')
    fin=CR[6].split(':')
    tdeb=datetime.datetime(2000,1,1,int(deb[0]),int(deb[1]),int(deb[2]))
    tfin=datetime.datetime(2000,1,1,int(fin[0]),int(fin[1]),int(fin[2]))
    if tdeb<=tfin:
      duree=str(tfin-tdeb).replace(":","h ",1).replace(":","mn ",1)+'s'
    else:
      tfin=datetime.datetime(2000,1,2,int(fin[0]),int(fin[1]),int(fin[2]))
      duree=str(tfin-tdeb).replace(":","h ",1).replace(":","mn ",1)+'s'
    if int(duree.split('h')[0])==0:
      duree=duree.split('h ')[1]
      if int(duree.split('m')[0])==0:
        duree=duree.split('mn ')[1]
    if duree[0]=='0':
      duree=duree[1:]
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"deroulement%s"%DOCEXTENSION),"a",encoding='utf-8')
    f.write("\\\\\n")
    f.write("Rapport \\textbf{%s} : \\\\\n%s - %s\\\\\n"%(fmtCh(Ref),fmtCh(CR[2]),fmtCh(CR[3])))
    f.write("\\begin{tabular}{l|rl}\n")
    f.write("\\-&%s :&%s \\texttt{%s:%s %s %s}\\\\\n"%(self.fmtLex('MOT_OPERATEUR'),fmtCh(CR[9]),self.fmtLex('MOT_COMPTE'),fmtCh(CR[8]),self.fmtLex('MOT_SUR'),fmtCh(CR[7])))
    f.write("&%s :&\\texttt{%s} %s \\texttt{%s (%s-%s)}\\\\\n"%(self.fmtLex('MOT_LE_DATE'),fmtCh(CR[4]),self.fmtLex('MOT_EN'),duree,fmtCh(CR[5]),fmtCh(CR[6])))
    f.write("\\end{tabular}\\\\\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Recuperation du bilan des FT
  #-------------------------------------------------------------------------------
  def Campagne_RecupereBilanFT(self,Ref):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"FTemis%s"%DOCEXTENSION),"a",encoding='utf-8')
    f.write(Titre3(fmtCh(Ref)))

    if os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesOuverts%s"%DOCEXTENSION),os.F_OK):
      FTlig=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesOuverts%s"%DOCEXTENSION))
      if len(FTlig)>5:
        f.write("%s :\\\\\n\n"%self.fmtLex('LOCUT_FAITS_TECHS_OUVERTS'))
        f.write("%s\n\n"%('\n'.join(FTlig[1:])))

    if os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesReOuverts%s"%DOCEXTENSION),os.F_OK):
      FTlig=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesReOuverts%s"%DOCEXTENSION))
      if len(FTlig)>5:
        f.write("%s :\\\\\n\n"%self.fmtLex('LOCUT_FAITS_TECHS_REOUVERTS'))
        f.write("%s\n\n"%('\n'.join(FTlig[1:])))

    if os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesAClore%s"%DOCEXTENSION),os.F_OK):
      FTlig=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"faitsTechniquesAClore%s"%DOCEXTENSION))
      if len(FTlig)>5:
        f.write("%s :\\\\\n\n"%self.fmtLex('LOCUT_FAITS_TECHS_A_CLORE'))
        f.write("%s\n\n"%('\n'.join(FTlig[1:])))

    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Couverture des exigences
  #-------------------------------------------------------------------------------
  def Campagne_CouvertureExigences(self,MatExigences,fichierGrExig):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"exigencesCouvertes%s"%DOCEXTENSION),"w",encoding='utf-8')
    nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(fichierGrExig)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(fichierGrExig)),timeout=5,signaler=True)
    f.write("\\includegraphics[width=10cm]{"+os.path.basename(fichierGrExig)+"}\\\\\n")

    f.write(Titre3(self.fmtLex('LOCUT_EXIGENCES_COUVERTES')))
    nb=0
    for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
      if MatExigences[ex] is not None:
        nb+=1
    if nb>0:
      f.write("\\renewcommand{\\arraystretch}{1}\n")
      f.write("\\begin{nngtabular}{|p{60mm}|p{4mm}|p{4mm}|p{4mm}|p{4mm}|p{50mm}|}{%s&%s&%s&%s&%s&%s}\n"%(self.fmtLex('MOT_EXIGENCE'),self.fmtLex('MOT_OK'),self.fmtLex('MOT_KO'),self.fmtLex('MOT_AV'),self.fmtLex('MOT_NT'),self.fmtLex('LOCUT_ID_TODO')))
      for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
        if MatExigences[ex] is not None:
          (sev,mant)=MatExigences[ex]
          if mant=='':
            f.writelines("{\\scriptsize %s}&{\\scriptsize X}&&&&\\\\\n"%fmtCh(ex))
          else: # TODO : verifier AV et NT
            if sev=='':sev=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_0')
            f.writelines("{\\scriptsize %s}&&{\\scriptsize %s}&&&{\\scriptsize %s}\\\\\n"%(fmtCh(ex),sev[0],fmtCh(mant)))
      f.write("\\end{nngtabular}\n")
      f.write("\\renewcommand{\\arraystretch}{1.5}\n")
    else:
      f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.write(Titre3(self.fmtLex('LOCUT_EXIGENCES_NON_COUVERTES')))
    self.NbExigNonCouv=0
    for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
      if MatExigences[ex] is None:
        self.NbExigNonCouv+=1
    if self.NbExigNonCouv>0:
      f.write("\\renewcommand{\\arraystretch}{1}\n")
      f.write("\\begin{longtable}{l|l|l|l}\n")
      col=0
      for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
        if MatExigences[ex] is None:
          f.writelines("{\\scriptsize %s}"%fmtCh(ex))
          col+=1
          if col==4:
            f.writelines("\\\\\n")
            col=0
          else:
            f.writelines("&")
      for i in range(3-col):
        f.writelines("&")
      f.write("\\\\\n\\end{longtable}\n")
      f.write("\\renewcommand{\\arraystretch}{1.5}\n")
    else:
      f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanFTtodo(self,fichierO,fichierR):
    lo=nngsrc.services.SRV_LireContenuListe(fichierO)[1:]
    lr=nngsrc.services.SRV_LireContenuListe(fichierR)[1:]

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"FTtodo%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(Titre3(self.fmtLex('LOCUT_FAITS_TECHS_OUVERTS')))

    texte=''
    for ligne in lo:
      ligne=ligne.split(';')
      texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[0]))
      texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[1]))
     # texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[2]))
      texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[3]))
      texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[4]))
      texte+="\\parbox{100mm}{\\scriptsize \\-\\\\%s\\\\ }\\\\ \\hline\n"%(fmtCh(nngsrc.services.SRV_Encodage(ligne[5])).replace(', ','\\\\')) # Description
    if len(texte)>0:
      f.write("\\renewcommand{\\arraystretch}{1}\n")
#      f.write("\\begin{nngtabular}{|p{8mm}|p{12mm}|p{18mm}|p{8mm}|p{12mm}|p{82mm}|}{%s&%s&%s&%s&%s&%s}\n"%(
      f.write("\\begin{nngtabular}{|p{8mm}|p{12mm}|p{8mm}|p{12mm}|p{100mm}|}{%s&%s&%s&%s&%s}\n"%(
                            self.fmtLex('MOT_ID'),
                            self.fmtLex('MOT_VERSION'),
                           # self.fmtLex('MOT_ECHEANCE'),
                            self.fmtLex('ABR_SEVERITE'),
                            self.fmtLex('MOT_ETAT'),
                            self.fmtLex('MOT_DESCRIPTION')))
      f.write(texte)
      f.write("\\end{nngtabular}\n")
      f.write("\\renewcommand{\\arraystretch}{1.5}\n")
    else:
      f.write("%s\\\\\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.write(Titre3(self.fmtLex('LOCUT_FAITS_TECHS_RESOLUS')))
    texte=''
    for ligne in lr:
      ligne=ligne.split(';')
      if ligne[4]==nngsrc.qualite.Aclore:
        texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[0]))
        texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[1]))
    #    texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[2]))
        texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[3]))
        texte+="{\\scriptsize %s}&"%fmtCh(nngsrc.services.SRV_Encodage(ligne[4]))
        texte+="\\parbox{100mm}{\\scriptsize \\-\\\\%s\\\\ }\\\\ \\hline\n"%(fmtCh(nngsrc.services.SRV_Encodage(ligne[5])).replace(', ','\\\\')) # Description
    if len(lr)>0:
      f.write("\\renewcommand{\\arraystretch}{1}\n")
#      f.write("\\begin{nngtabular}{|p{8mm}|p{12mm}|p{18mm}|p{8mm}|p{12mm}|p{82mm}|}{%s&%s&%s&%s&%s&%s}\n"%(
      f.write("\\begin{nngtabular}{|p{8mm}|p{12mm}|p{8mm}|p{12mm}|p{100mm}|}{%s&%s&%s&%s&%s}\n"%(
                            self.fmtLex('MOT_ID'),
                            self.fmtLex('MOT_VERSION'),
                           # self.fmtLex('MOT_ECHEANCE'),
                            self.fmtLex('ABR_SEVERITE'),
                            self.fmtLex('MOT_ETAT'),
                            self.fmtLex('MOT_DESCRIPTION')))
      f.write(texte)
      f.write("\\end{nngtabular}\n")
      f.write("\\renewcommand{\\arraystretch}{1.5}\n")
    else:
      f.write("%s\\\\\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanExigencesControles(self,fichierGrExig,ficoutGrExig,fichierGrCont,ficoutGrCont):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"BilanExigences%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write("%s\\\\\n"%self.fmtLex('INTRO_BILAN_EXIGENCES'))
    dat=nngsrc.services.SRV_LireContenuListe(fichierGrExig)
    try:
      nbexig=dat[5].split(' ')[2]
    except:
      nbexig=0
    sommes=[0,0,0,0,0,0,0]
    if nbexig!=0:
      f.write("%s\\\\\n"%fmtCh(self.projet.lexique.entree('LOCUT_NB_EXIGENCES_DANS_VALIDATION')%(nbexig,self.NbExigNonCouv)))
    f.write("\\renewcommand{\\arraystretch}{1}\n")
    f.write("\\begin{nngtabular}{|p{75mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|}{%s&%s&%s&%s&%s&%s&%s&%s}\n"%(self.fmtLex('MOT_REFERENCES'),self.fmtLex('ABR_NOMBRE'),self.fmtLex('ABR_CRITIQUE'),self.fmtLex('ABR_BLOQUANT'),self.fmtLex('ABR_MAJEUR'),self.fmtLex('ABR_MINEUR'),self.fmtLex('ABR_NON_QUALIFIE'),self.fmtLex('MOT_OK')))
    for l in dat:
      if l[0]!="#":
        ll=l.replace('\n','').split(' ')
        f.write("{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}\\\\\n"%(fmtCh(ll[1]),ll[8],ll[3],ll[4],ll[5],ll[6],ll[7],ll[9]))
        for i in range(7):
          sommes[i]=sommes[i]+int(ll[i+3])
    f.write("\\hline\n")
    f.write("\\textbf{%s}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}\\\\\n"%(self.fmtLex('MOT_TOTAUX'),sommes[5],sommes[0],sommes[1],sommes[2],sommes[3],sommes[4],sommes[6]))
    f.write("\\hline\n")
    f.write("\\end{nngtabular}\n")
    f.write("\\renewcommand{\\arraystretch}{1.5}\n")

    if os.path.exists(os.path.join(self.repTravail,os.path.basename(ficoutGrExig))):
      time.sleep(2)
      nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(ficoutGrExig)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(ficoutGrExig)),timeout=5,signaler=True)
      f.write("\\includegraphics[width=10cm]{"+os.path.basename(ficoutGrExig)+"}\\\\\n")
    f.close()

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"BilanControles%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(self.fmtLex('INTRO_BILAN_CONTROLES')+"\\\\\n")
    dat=nngsrc.services.SRV_LireContenuListe(fichierGrCont)
#    nbcontroles=dat[0].split(' ')[1]
    sommes=[0,0,0,0,0]
#    f.write(fmtCh(self.projet.lexique.entree('LOCUT_NB_EXIGENCES_DANS_VALIDATION')%(nbexig,self.NbExigNonCouv))+"\\\\\n")
    f.write("\\renewcommand{\\arraystretch}{1}\n")
    f.write("\\begin{nngtabular}{|p{75mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|p{8mm}|}{%s&%s&%s&%s&%s&%s}\n"%(self.fmtLex('MOT_REFERENCES'),self.fmtLex('ABR_NOMBRE'),self.fmtLex('MOT_OK'),self.fmtLex('MOT_AV'),self.fmtLex('MOT_KO'),self.fmtLex('MOT_NT')))
    for l in dat:
      if l[0]!="#":
        ll=l.replace('\n','').split(' ')
        f.write("{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}&{\\scriptsize %s}\\\\\n"%(fmtCh(ll[1]),ll[2],ll[3],ll[4],ll[5],ll[6]))
        for i in range(5):
          sommes[i]=sommes[i]+int(ll[i+2])
    f.write("\\hline\n")
    f.write("\\textbf{%s}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}&{\\scriptsize %d}\\\\\n"%(self.fmtLex('MOT_TOTAUX'),sommes[0],sommes[1],sommes[2],sommes[3],sommes[4]))
    f.write("\\hline\n")
    f.write("\\end{nngtabular}\n")
    f.write("\\renewcommand{\\arraystretch}{1.5}\n")

    if os.path.exists(os.path.join(self.repTravail,os.path.basename(ficoutGrCont))):
      time.sleep(2)
      nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(ficoutGrCont)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(ficoutGrCont)),timeout=5,signaler=True)
      f.write("\\includegraphics[width=10cm]{"+os.path.basename(ficoutGrCont)+"}\\\\\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Fin du bilan de campagne
  #-------------------------------------------------------------------------------
  def Campagne_Terminer(self,campagne):
    destination=os.path.join(self.repTravail,REPERTRAPPORT)
    if not os.access(self.repCampagne,os.F_OK):
      os.mkdir(self.repCampagne)

    destination=os.path.join(self.repCampagne,REPERTRAPPORT,campagne.refCampagne+'-%04d'%campagne.iterCampagne)
    if not os.access(destination,os.F_OK):
      if not os.access(os.path.join(self.repCampagne,REPERTRAPPORT),os.F_OK):
        os.mkdir(os.path.join(self.repCampagne,REPERTRAPPORT))
      os.mkdir(destination)

    if os.name=="posix":
      f=open(os.path.join(self.repCampagne,REPERTRAPPORT,"compiler.sh"),"w",encoding='utf-8')
      f.write("# !/usr/bin/env bash\n")
      f.write("# =======================================================================\n")
      f.write("cd `dirname $0`\n")
      f.write("cd %s\n"%campagne.refCampagne)
      f.write("chmod u+x compiler.sh\n")
      f.write("./compiler.sh $1\n")
      f.close()
      nngsrc.services.SRV_changePermission(os.path.join(self.repCampagne,REPERTRAPPORT,"compiler.sh"),perm=0o775,recursif=False)

      f=open(os.path.join(destination,"compiler.sh"),"w",encoding='utf-8')
      DEL="rm -f"
      VIEWER='if [ "$1" == "-v" ]; then\n  %s %s.pdf&\nfi\n'
      if os.getenv("OS",'')=="Windows_NT":
        VIEWER="%s %s.pdf&"
      f.write("# !/usr/bin/env bash\n")
      f.write("# =======================================================================\n")
    else:
      f=open(os.path.join(self.repCampagne,REPERTRAPPORT,"compiler.bat"),"w",encoding='utf-8')
      f.write("cd %s\n"%campagne.refCampagne)
      f.write("call compiler.bat %1\n")
      f.close()
      f=open(os.path.join(destination,"compiler.bat"),"w",encoding='utf-8')
      DEL="del"
      VIEWER='if "%%1"=="EDITER" (\n  %s %s.pdf\n)\n'

    f.write(DEL+" "+campagne.refCampagne+".toc\n")
    f.write(DEL+" "+campagne.refCampagne+".aux\n")
    f.write("pdflatex "+campagne.refCampagne+".tex\n")
    f.write("pdflatex "+campagne.refCampagne+".tex\n")
    f.write("pdflatex "+campagne.refCampagne+".tex\n")#-interaction=nonstopmode
    f.write(VIEWER%(VIEWERLATEX,campagne.refCampagne))
    f.close()
    nngsrc.services.SRV_changePermission(os.path.join(destination,"compiler.sh"),perm=0o775,recursif=False)

    # on deplace les rapports dans le repertoire final
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.*')):
      ficDestination=os.path.join(destination,os.path.basename(elem))
      if os.access(ficDestination,os.F_OK):
        nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    for f in [self.projet.DOCUMENT_REDIGE_SIGN,self.projet.DOCUMENT_REDIGE2_SIGN,self.projet.DOCUMENT_VERIFIE_SIGN,self.projet.DOCUMENT_VERIFIE2_SIGN,self.projet.DOCUMENT_VALIDE_SIGN,self.projet.DOCUMENT_REALISE_SIGN]:
      if f!='':
        ficSignature=os.path.basename(f)
        if ficSignature!='':
          nngsrc.services.SRV_copier(f,os.path.join(destination,ficSignature),timeout=5,signaler=True)

    os.rmdir(os.path.join(self.repTravail,REPERTRAPPORT))

    # Elements des ressources a chercher dans le projet
    if os.path.exists(self.projet.CLIENT_LOGO):
      nngsrc.services.SRV_copier(self.projet.CLIENT_LOGO,os.path.join(destination,"logoClient.png"))
    if os.path.exists(self.projet.SOCIETE_LOGO):
      nngsrc.services.SRV_copier(self.projet.SOCIETE_LOGO,os.path.join(destination,"logoSociete.png"))

    # Elements des ressources a chercher dans le projet
    for f in ['nangu_fr.sty','nangu_en.sty','nangu.png','action.png']:
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)

    num=1
    f="etape%d.png"%num
    while os.access(os.path.join(self.repRessources,f),os.F_OK):
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)
      num+=1
      f="etape%d.png"%num

    # Archivage
    self.projet.ARCHIVAGE=True
    if self.projet.ARCHIVAGE:
      fichCmplt=destination+".tgz"
      if os.access(fichCmplt,os.F_OK):
        nngsrc.services.SRV_detruire(fichCmplt,timeout=5,signaler=True)
      ficTGZ=tarfile.open(fichCmplt,mode="w:gz",encoding='utf-8')
      ficTGZ.add(destination,arcname=os.path.basename(destination))
      ficTGZ.close()
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ARCHIVE_DANS')%fichCmplt,style="r")

  #-------------------------------------------------------------------------------
  def Valid_InitRapport(self,nom,titre,sstitre,doctype,docMethode,IDcahier):
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()

    # Fichier principal
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION),"w",encoding='utf-8')
    f.write("\\documentclass[a4paper,11pt,openany]{book}\n")
    f.write("\\usepackage{%s}\n"%self.projet.lexique.entree('DOCTYPE_LATEX'))
    if self.projet.DOCUMENT_FILIGRANE != '':
      f.write("\\AddToShipoutPicture*{\n")
      f.write("\\AtTextCenter{%\n")
      f.write("\\makebox(0,0)[c]{\\resizebox{\\textwidth}{!}{%\n")
      f.write("\\rotatebox{45}{\\textsf{\\textbf{\\color{lightgray}"+fmtCh(self.projet.DOCUMENT_FILIGRANE)+"}}}\n")
      f.write("}}}}\n")
      f.write("\\definecolor{grisversion}{gray}{0.90}\n")
      f.write("\\AddToShipoutPicture{%\n")
      f.write("\\unitlength=1mm %\n")
      f.write("\\put(5,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} \\centering {\\textcolor{gray}{\\textbf{\\Huge{%s}}}}\\end{minipage}}}\n"%fmtCh(self.projet.DOCUMENT_FILIGRANE))
      f.write("\\put(199,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} \\centering {\\textcolor{gray}{\\textbf{\\Huge{%s}}}}\\end{minipage}}}\n"%fmtCh(self.projet.DOCUMENT_FILIGRANE))
      f.write("\\put(5,0){\\rotatebox{90}{\\begin{minipage}{\\paperheight} {\\textcolor{gray}{\\tiny{%s \includegraphics[width=0.20cm]{nangu.png}NANGU v%s}}}\\end{minipage}}}\n"%(fmtCh(self.projet.lexique.entree('LOCUT_GENERE_PAR')),self.version))
      f.write("}\n")
    f.write("\\makeindex{exigences}\n")
    f.write("\\makeindex{general}\n")

    #--------Début du document---
    f.write("\\begin{document}\n\n")
    f.write("\\frontmatter\n\n")
    f.write("\\widowpenalty=10000\n")
    f.write("\\clubpenalty=10000\n")
    f.write("\\raggedbottom\n\n")
    #-  Nom du projet
    f.write("\\project{%s}\n"%nom)
    #-  Type de document
    f.write("\\documentType{%s}\n"%fmtCh(doctype))
    f.write("\\documentMethode{%s}\n"%fmtCh(docMethode))
    #-  Campagne
    if not self.projet.execution in ['P','S']:
      f.write("\\documentCampagne{%s v%s}\n"%(self.fmtLex('MOT_CAMPAGNE'),fmtCh(self.projet.PRODUIT_VERSION)))
    else:
      f.write("\\documentCampagne{}\n")
    #-    Titre et sous-titre du document
    f.write("\\documentTitle{%s}\n"%titre)
    f.write("\\documentSsTitle{%s}\n"%sstitre)
    #-  PC
    f.write("\\utilisateurPoste{"+fmtCh(self.projet.COMPUTERNAME)+"}\n")
    #-  User
    f.write("\\utilisateurCompte{"+fmtCh(self.projet.USERNAME)+"}\n")
    #-  Référence du document
    if self.projet.version<9.0:
      if self.projet.execution in ['P','S']:
        f.write("\\documentReference{"+fmtCh(self.projet.PRODUIT_REFERENCE+self.projet.PRODUIT_SOUS_REFERENCE)+"}\n")
      else:
        f.write("\\documentReference{"+fmtCh(self.projet.PRODUIT_REFERENCE+"-"+self.projet.PRODUIT_VERSION+self.projet.PRODUIT_SOUS_REFERENCE)+"}\n")
    else:
      if self.projet.execution in ['P','S']:
        f.write("\\documentReference{"+fmtCh(self.projet.REFERENCE_IDENTIFIANT.replace("{MODE}",self.projet.execution).replace("%s{VERSION}"%self.projet.SEPARATEUR_CHAMPS,'').replace("{SOUSREF}",self.projet.PRODUIT_SOUS_REFERENCE))+"}\n")
      else:
        f.write("\\documentReference{"+fmtCh(self.projet.REFERENCE_IDENTIFIANT.replace("{MODE}",self.projet.execution).replace("{VERSION}",self.projet.PRODUIT_VERSION).replace("{SOUSREF}",self.projet.PRODUIT_SOUS_REFERENCE))+"}\n")

    #-  Edition/Révision du document
    if self.projet.execution in ['P','S']:
      f.write("\\documentRevision{"+fmtCh(self.projet.DOCUMENT_EDITION)+"."+fmtCh(self.projet.DOCUMENT_REVISION)+"}\n")
    else:
      f.write("\\documentRevision{"+fmtCh(self.projet.DOCUMENT_EDITION)+"."+fmtCh(self.projet.DOCUMENT_REVISION)+"."+fmtCh(self.projet.DOCUMENT_ITERATION)+"}\n")
    #-  Date du document
    f.write("\\documentDate{"+self.projet.lexique.date(t)+"}\n")
    #-  Client
    f.write("\\client{"+fmtCh(self.projet.PRODUIT_CLIENT)+"}\n")
    f.write("\\logoClient{"+os.path.basename(self.projet.CLIENT_LOGO)+"}\n")
    f.write("\\consultationReference{"+fmtCh(self.projet.PRODUIT_CONTRAT)+"}\n\n")
    #-  Societe
    f.write("\\societe{"+fmtCh(self.projet.PRODUIT_SOCIETE)+"}\n")
    f.write("\\logoSociete{"+os.path.basename(self.projet.SOCIETE_LOGO)+"}\n")
    f.write("\\societeadresse{"+fmtCh(self.projet.SOCIETE_ADRESSE)+"}\n\n")
    f.write("\\societecoordonnees{"+fmtCh(self.projet.SOCIETE_COORDONNEES)+"}\n\n")
    f.write("\\societepropriete{"+fmtCh(self.projet.SOCIETE_PROPRIETE)+"}\n\n")
    #-    Rédigé par...
    f.write("\\writeBy{"+fmtCh(self.projet.DOCUMENT_REDIGE_PAR)+"}\n")
    #-    Rédigé org...
    f.write("\\writeByCorp{"+fmtCh(self.projet.DOCUMENT_REDIGE_ORG)+"}\n")
    #-    Rédigé le...
    f.write("\\writeDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_REDIGE_DATE,self.projet)+"}\n")
    #-    Rédigé sign...
    f.write("\\writeSign{"+os.path.basename(self.projet.DOCUMENT_REDIGE_SIGN)+"}\n")
    #-    Rédigé par...
    f.write("\\writebisBy{"+fmtCh(self.projet.DOCUMENT_REDIGE2_PAR)+"}\n")
    #-    Rédigé org...
    f.write("\\writebisByCorp{"+fmtCh(self.projet.DOCUMENT_REDIGE2_ORG)+"}\n")
    #-    Rédigé le...
    f.write("\\writebisDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_REDIGE2_DATE,self.projet)+"}\n")
    #-    Rédigé sign...
    f.write("\\writebisSign{"+os.path.basename(self.projet.DOCUMENT_REDIGE2_SIGN)+"}\n")
    #-    Vérifié par...
    f.write("\\verifyBy{"+fmtCh(self.projet.DOCUMENT_VERIFIE_PAR)+"}\n")
    #-    Vérifié org...
    f.write("\\verifyByCorp{"+fmtCh(self.projet.DOCUMENT_VERIFIE_ORG)+"}\n")
    #-    Vérifié le...
    f.write("\\verifyDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_VERIFIE_DATE,self.projet)+"}\n")
    #-    Vérifié sign...
    f.write("\\verifySign{"+os.path.basename(self.projet.DOCUMENT_VERIFIE_SIGN)+"}\n")
    #-    Vérifié par...
    f.write("\\verifybisBy{"+fmtCh(self.projet.DOCUMENT_VERIFIE2_PAR)+"}\n")
    #-    Vérifié org...
    f.write("\\verifybisByCorp{"+fmtCh(self.projet.DOCUMENT_VERIFIE2_ORG)+"}\n")
    #-    Vérifié le...
    f.write("\\verifybisDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_VERIFIE2_DATE,self.projet)+"}\n")
    #-    Vérifié sign...
    f.write("\\verifybisSign{"+os.path.basename(self.projet.DOCUMENT_VERIFIE2_SIGN)+"}\n")
    #-    Validé par...
    f.write("\\validateBy{"+fmtCh(self.projet.DOCUMENT_VALIDE_PAR)+"}\n")
    #-    Validé org...
    f.write("\\validateByCorp{"+fmtCh(self.projet.DOCUMENT_VALIDE_ORG)+"}\n")
    #-    Validé le...
    f.write("\\validateDate{"+nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_VALIDE_DATE,self.projet)+"}\n")
    #-    Validé sign...
    f.write("\\validateSign{"+os.path.basename(self.projet.DOCUMENT_VALIDE_SIGN)+"}\n")
    f.write("\\maketitle\n\n")
    #--------Diffusion du document---
    f.write("\\begin{nnglistediffusion}\n")
    table=[[],[],[],[]]
    if self.projet.DOCUMENT_DIFFUSION=='':
      Lignes=[]
    else:
      Lignes=self.projet.DOCUMENT_DIFFUSION.split('*')
    Premiere=True
    for ligne in Lignes:
      if Premiere:
        Premiere=False
        table[0].append(BF(self.fmtLex('MOT_DESTINATAIRES')))
      else:
        table[0].append('')
      for numElem in range(len(ligne.split('&'))):
        table[numElem+1].append(fmtCh(ligne.split('&')[numElem]))
    table[0].append(BF(self.fmtLex('LOCUT_COPIE_INTERNE')))
    table[1].append(self.fmtLex('LOCUT_DOSSIER_PROJET'))
    table[2].append(fmtCh(self.projet.PRODUIT_SOCIETE))
    table[3].append(self.fmtLex('LOCUT_UN_NUMERIQUE'))
    ecritureTable(f,table,True)
    f.write("\\end{nnglistediffusion}\n\\-\\\\\n\n")
    #--------Evolution du document---
    f.write("\\begin{nngevolutiondocument}\n")
    table=[[],[],[],[],[]]
    Lignes=self.projet.DOCUMENT_VERSION.split('*')
    for ligne in Lignes:
      for numElem in range(len(ligne.split('&'))):
        table[numElem].append(fmtCh(ligne.split('&')[numElem]))
    ecritureTable(f,table,True)
    f.write("\\end{nngevolutiondocument}\n\n")
    #--------Table des matières---
    f.write("\\tableofcontents\n\n")
    f.write("\\mainmatter\n\n")
    #--------INTRODUCTION---
    f.write(Titre1(self.fmtLex('TITRE1_INTRODUCTION')))
    if self.projet.DOCUMENT_OBJET and self.projet.DOCUMENT_OBJET!='':
      try:
        if self.projet.execution in ['P','S']:
          intro=self.projet.DOCUMENT_OBJET%'P'
        else:
          intro=self.projet.DOCUMENT_OBJET%'C'
      except:
        intro=self.projet.DOCUMENT_OBJET
#      if os.access(logue+DOCEXTENSION,os.F_OK):
#        self.inclureFichier(f,logue+DOCEXTENSION,immediat=True)
#      el
      if os.access(intro+".bbc",os.F_OK):
        self.inclureFichier(f,intro,bbc=True)

    if self.projet.nivRapport==2:
      fichier=os.path.join(self.projet.PLANS,IDcahier)
      if os.path.exists(fichier+".bbc"):
        f.write(Titre2(self.fmtLex('TITRE2_OBJET_DU_DOCUMENT')))
        self.inclureFichier(f,fichier,bbc=True)

    methode="Test"
    if self.projet.version>=9.0 and self.projet.methode!={} and self.projet.DOCUMENT_METHODE is not None:
      f.write(Titre2(self.fmtLex('TITRE2_METHODOLOGIE')))
      f.write(self.fmtLex('PRESENTATION_METHODOLOGIE'))
      table=[[],[],[]]
      f.write("\\\\\n\\begin{nngmethode}\n")
      for item in self.projet.methode.keys():
        abbrev=BF(item)
        terme=BF(fmtCh(self.projet.methode[item]['terme']))
        description=fmtCh(self.projet.methode[item]['description'])
        table[0].append(terme)
        table[1].append("(%s)"%abbrev)
        table[2].append(description)
      ecritureTable(f,table,True)
      f.write("\\end{nngmethode}\n")
      methode=self.projet.methode[self.projet.DOCUMENT_METHODE]['terme']
    f.write("%s\n"%fmtCh(self.projet.lexique.entree("LOCUT_METHODE_DOCUMENT")%(self.projet.lexique.articleMethode(methode),methode.lower())))
    f.write(Titre2(self.fmtLex('TITRE2_DOCUMENTS_ASSOCIES')))
    f.write(Titre3(self.fmtLex('TITRE3_DOCUMENTS_APPLICABLES')))
    if self.projet.execution in ['P','S']:
      if self.projet.DOC_APPLICABLE:
        texte,_=MiseEnTableau(self.projet.DOC_APPLICABLE,"DA",1)
        if texte!='':
          f.write("\\begin{nngdocapplicable}\n")
          f.write(texte)
          f.write("\\end{nngdocapplicable}\n")
        else:
          f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
      else:
        f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    else:
      if self.projet.version<9.0:
        if self.projet.VALIDATION_IDENTIFIANT=="prefixe":
          refPlan="P-"+self.projet.VALIDATION_REFERENCE
        elif self.projet.VALIDATION_IDENTIFIANT=="suffixe":
          refPlan=self.projet.VALIDATION_REFERENCE+"-P"
        elif self.projet.VALIDATION_REFERENCE.find("%s")!=-1:
          refPlan=self.projet.VALIDATION_REFERENCE%'P'
        else:
          refPlan=self.projet.VALIDATION_REFERENCE
        refPlan=refPlan.replace(".","-")+' v'+self.projet.DOCUMENT_EDITION+'.'+self.projet.DOCUMENT_REVISION
      else:
        refPlan=self.projet.REFERENCE_IDENTIFIANT.replace('{MODE}','P').replace('{SOUSREF}','').replace('%s{VERSION}'%self.projet.SEPARATEUR_CHAMPS,'')+' v'+self.projet.DOCUMENT_EDITION+'.'+self.projet.DOCUMENT_REVISION
      if self.projet.DOC_APPLICABLE:
        texte,nbElem=MiseEnTableau(self.projet.DOC_APPLICABLE,"DA",1)
        if self.projet.PLAN_APPLICABLE!='':
          document=fmtCh(self.projet.PLAN_APPLICABLE.split(',')[0])
          reference=fmtCh(self.projet.PLAN_APPLICABLE.split(',')[1])
        else:
          document=self.fmtLex('DOCPLANGLOBAL')
          reference=refPlan
        texte+="{\\small DA%02d} & {\\small %s} & {\\small %s}\\\\\n"%(nbElem+1,document,reference)
        if texte!='':
          f.write("\\begin{nngdocapplicable}\n")
          f.write(texte)
          f.write("\\end{nngdocapplicable}\n")
        else:
          f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
      else:
        f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    f.write(Titre3(self.fmtLex('TITRE3_DOCUMENTS_DE_REFERENCE')))
    if self.projet.DOC_REFERENCE:
      texte,_=MiseEnTableau(self.projet.DOC_REFERENCE,"DR",1)
      if texte!='':
        f.write("\\begin{nngdocreference}\n")
        f.write(texte)
        f.write("\\end{nngdocreference}\n")
      else:
        f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    else:
      f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    f.write(Titre2(self.fmtLex('TITRE2_TERMINOLOGIE')))
    f.write(Titre3(self.fmtLex('TITRE3_ABREVIATIONS_ET_SIGLES')))
    if self.projet.SIGLES:
      texte,_=MiseEnTableau(self.projet.SIGLES)
      if texte!='':
        f.write("\\begin{nngabrevsigles}\n")
        f.write(texte)
        f.write("\\end{nngabrevsigles}\n")
      else:
        f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    else:
      f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    f.write(Titre3(self.fmtLex('TITRE3_GLOSSAIRES_DES_TERMES')))
    if self.projet.GLOSSAIRE:
      texte,_=MiseEnTableau(self.projet.GLOSSAIRE)
      if texte!='':
        f.write("\\begin{nngdefinitiontermes}\n")
        f.write(texte)
        f.write("\\end{nngdefinitiontermes}\n")
      else:
        f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    else:
      f.write("%s\n"%self.fmtLex('LOCUT_SANS_OBJET'))
    f.write(Titre3(self.fmtLex('TITRE3_NOTATIONS')))
    f.write("\\begin{nngnotations}\n")
    f.write("\\{ \\} & %s\\\\\n"%self.fmtLex('LOCUT_VARIABLES_ENVIRONNEMENT'))
    f.write("< > & %s\\\\\n"%self.fmtLex('LOCUT_VARIABLES_PROJET'))
    f.write(" %s & %s\\\\\n"%(DebFin,self.fmtLex('LOCUT_LEGENDE_BOUCLE')))
    if self.projet.NOTATIONS:
      texte,_=MiseEnTableau(self.projet.NOTATIONS)
      if texte!='':
        f.write(texte)
    f.write("\\end{nngnotations}\n")

     #--------PROLOGUE---
    if self.projet.PROLOGUE!='':
      try:
        if self.projet.execution in ['P','S']:
          logue=self.projet.PROLOGUE%'P'
        else:
          logue=self.projet.PROLOGUE%'C'
      except:
        logue=self.projet.PROLOGUE
      if self.projet.debug>1:
        if self.projet.PRODUIT_SOUS_REFERENCE!='' and not os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc",os.F_OK):
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Avertissement : Le fichier de prologue %s est absent."%(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc"),style="Wr")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Il sera remplace par %s"%(logue+".bbc"),style="Wr")
      if self.projet.PRODUIT_SOUS_REFERENCE!='' and os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc",os.F_OK):
        self.inclureFichier(f,logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc",bbc=True)
      else:
        if os.access(logue+".bbc",os.F_OK):
          self.inclureFichier(f,logue,bbc=True)
        else:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"ATTENTION : Fichier prologue %s.bbc non trouve."%logue,style="Wr")
    #--------Environnement---
    self.inclureFichier(f,"listeDonnees")
    #--------Grille de déroulement---
    self.inclureFichier(f,"bilanControles")
    f.close()
    grillePath=os.path.join(self.repTravail,REPERTRAPPORT,"bilanControles%s"%DOCEXTENSION)
    g=open(grillePath,"a",encoding='utf-8')
    g.write(Titre1(self.fmtLex('TITRE1_BILAN_CONTROLES')))
    if not self.projet.execution in ['P','S']:
      g.write(fmtCh(self.projet.lexique.entree('LOCUT_TRAVAUX_REALISES')%(self.projet.COMPUTERNAME,self.projet.USERNAME)))
      if self.projet.execution=='C':
        g.write(" %s"%fmtCh(self.projet.lexique.entree('LOCUT_DATE_TRAVAUX')%("__/__/____","__h__m","__h__m")))
      else:
        if self.projet.debug==2:
          t=datetime.datetime(2016,2,29,12,34,56)
        else:
          t=datetime.datetime.now()
        g.write(" %s"%fmtCh(self.projet.lexique.entree('LOCUT_DATE_TRAVAUX')%(t.strftime("%d/%m/%Y"),t.strftime("%Hh %Mm %Ss"),"HEUREFIN")))
      g.write(Titre2(self.fmtLex('TITRE2_GRILLE_DEROULEMENT')))
      colonne='MOT_CONTROLE'
      if self.projet.nivRapport==1: colonne='MOT_CONTROLE'
      elif self.projet.nivRapport==2:
        if self.projet.typePRJsimple: colonne='MOT_CONTROLE'
        else: colonne='MOT_SCENARIO'
      g.write("\\begin{nnggrilletests}{%s}{%s}{%s}{%s}\n"%(self.fmtLex(colonne),self.fmtLex('LOCUT_CONTROLE_PAR'),self.fmtLex('MOT_BILAN'),self.fmtLex('MOT_SEVERITE')))
    g.close()
    #--------Annexe---
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"annexes%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(Titre1(self.fmtLex('TITRE1_ANNEXES')))
    f.write("\\begin{description}")
    f.close()

  #---------------------------------------------------------------------------------
  def Valid_InscrireEtape(self,libelle):
    f=open(self.ficRapportFiche,"a",encoding='utf-8')
    f.write("        %s\n"%libelle)
    f.close()

  #-------------------------------------------------------------------------------
  # Initialisation du Bloc
  #-------------------------------------------------------------------------------
  def Valid_InitChapitre(self,chapitre):
    if self.projet.nivRapport==2: return
    fichier=os.path.join(self.projet.FICHES,chapitre,chapitre)
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION),"a",encoding='utf-8')
    if os.access(fichier+".bbc",os.F_OK):
      self.inclureFichier(f,fichier,bbc=True)
    f.close()

  #-------------------------------------------------------------------------------
  # Mise a jour du bilan
  #-------------------------------------------------------------------------------
  def Valid_TerminerChapitre(self,sousreference,TestPath,resultat):
#    print("DBG>")
#    print("DBG>","Resultat_Section",sousreference,resultat)
#    print("DBG>")
    reference=sousreference.split('_')[0]
    self.Valid_DeroulementTests(reference,resultat,0)

  #-------------------------------------------------------------------------------
  # Initialisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_InitRapportFiche(self,reference,rang,fiche,tagFiche,fichePath,txtLoop,nbaffloop):
    self.fiche=os.path.basename(fiche)
    fiche=fiche.replace('\\','_').replace('/','_')
    self.ficRapportFiche=os.path.join(self.repTravail,REPERTRAPPORT,'%s-%d-%s%s'%(reference,rang,fiche,DOCEXTENSION))
    self.ficRapportFiche=sansAccent(self.ficRapportFiche)

    f=open(self.ficRapportFiche,"w",encoding='utf-8')
    f.write(Titre2("%s%s"%(fmtCh(os.path.basename(self.fiche)),fmtCh(tagFiche))))
    if txtLoop:
      texte=fmtCh(self.projet.lexique.entree('LOCUT_BOUCLE_FICHE_R')%txtLoop)
      f.write(texte+'\\\\ \\vline \\\\\n')
    if nbaffloop:
      texte=fmtCh(self.projet.lexique.entree('LOCUT_BOUCLE_FICHE_PSC')%nbaffloop)
      f.write(texte+'\\\\ \\vline \\\\\n')
    f.close()

  #-------------------------------------------------------------------------------
  # Finalisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_TerminerRapportFiche(self,html,result,criticite,EXECUTE_FIN,pathFiche,rapportActions):
    rapportFTficheReserves=self.qualite.rapportFTficheReserves
    lstFTfichereOuvert=self.qualite.lstFTfichereOuvert
    lstFTficheaClore=self.qualite.lstFTficheaClore

    fiche=pathFiche.split('§')[0]
    fiche=fiche.replace('\\','_').replace('/','_')
#    self.ficRapportFiche=os.path.join(self.repTravail,REPERTRAPPORT,fiche+DOCEXTENSION)

    # - si rapport='oui' (0)
    # - si rapport='siNonOk' (1) et resultat(r)!=OK
    # - si rapport='siOk' (2) et resultat(r)=OK
    if rapportActions==0 or (rapportActions==1 and result!=self.constantes['STATUS_OK']) or (rapportActions==2 and result==self.constantes['STATUS_OK']):
      f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION),"a",encoding='utf-8')
      self.inclureFichier(f,os.path.basename(self.ficRapportFiche.replace(DOCEXTENSION,'')))#fiche)
      f.close()

  # Zone de suivi des FT sur la fiche
  # +-------------------------------------------------------------------------------------
  # | Suivi
  # |-------------------------------------------------------------------------------------
  # | Références des Faits Techniques ouverts       | Réserves éventuelles
  # | <action> <Référence> (<dejà émis>) <gravite>  | <texte>
  # |-------------------------------------------------------------------------------------
  # | Références des Faits Techniques re-ouverts    | Réserves éventuelles
  # | <action> <Référence>                          |
  # |-------------------------------------------------------------------------------------
  # | Références des Faits Techniques à clore       | Réserves éventuelles
  # | <action> <Référence>                          |
  # +-------------------------------------------------------------------------------------
    if not self.projet.execution in ['P','S']:
      f=open(self.ficRapportFiche,"a",encoding='utf-8')
      f.write("\\begin{tabularx}{16cm}{|X|}\n\\hline\n")
      f.write("\\rowcolor{nngblue}\\textbf{"+ self.fmtLex('MOT_SUIVI')+"} \\\\\n")
      f.write("\\end{tabularx}\\\\\n")
      f.write("\\begin{tabularx}{16cm}{|X|X|}\n\\hline\n")

      # FT nouveaux ouverts
      f.write(self.fmtLex('LOCUT_REFERENCES_FT_OUVERTS')+"&"+self.fmtLex('LOCUT_RESERVES')+"\\\\\n")
      if rapportFTficheReserves:
        for reserve in rapportFTficheReserves:
          f.write(" & %s\\\\\n"%fmtCh(reserve))
      nbRapportFiche=0
      for r in self.qualite.rapportFTfiche:
        if r['STATUT']!='A clore':
          nbRapportFiche+=1
      if (self.projet.execution=='R') and result==self.constantes['STATUS_PB']:
        for ft in self.qualite.rapportFTfiche:
          num=int(ft['ID'])
          qualif=ft['GRAVITE']
          if qualif==0: qualif=" "
          else: qualif=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%qualif)
          reserve=" " #TODO ajuster avec le texte entré par l'utilisateur
          act=ft['LIBELLE'].split('\n')[2].split(' : ')[1]
          if ft['STATUT']=="Suspendu":
            dirname=os.path.dirname(self.projet.PRODUIT_BASEFT)
            justification=os.path.join(dirname,"justif-%06d.bbc"%num)
            if os.path.exists(justification):
              reserve=nngsrc.services.SRV_LireContenu(justification)
              reserve=self.TexteConversion(reserve,dirname)
            else:
              reserve=self.fmtLex('LOCUT_PAS_JUSTIFIE')
            f.write("- %s %s-%06d (%s) {\\scriptsize %s} & {\\scriptsize %s : %s}\\\\\n"%(act,fmtCh(self.refFT),num,self.fmtLex('LOCUT_DEJA_EMIS'),qualif,self.fmtLex('MOT_SUSPENDU'),reserve))
          elif ft['STATUT']=="Doublon":
            f.write("- %s %s-%06d (%s) {\\scriptsize %s} & {\\scriptsize %s}\\\\\n"%(act,fmtCh(self.refFT),num,self.fmtLex('LOCUT_DEJA_EMIS'),qualif,reserve))
          else:
            f.write("- %s %s-%06d {\\scriptsize %s} & {\\scriptsize %s}\\\\\n"%(act,fmtCh(self.refFT),num,qualif,reserve))
      elif (self.projet.execution=='R') and nbRapportFiche==0:
        f.write(" %s & \\\\\n"%IT(self.fmtLex('LOCUT_SANS_OBJET')))
      elif self.projet.execution=='C':
        for i in range(5):
          f.write("& \\\\\n")
      f.write("& \\\\\n\\hline\n")

      # FT re-ouverts
      f.write(self.fmtLex('LOCUT_REFERENCES_FT_REOUVERTS')+"&"+self.fmtLex('LOCUT_RESERVES')+"\\\\\n")
      if len(lstFTfichereOuvert)==0:
        f.write(" %s & \\\\\n"%IT(self.fmtLex('LOCUT_SANS_OBJET')))
      else:
        for idx in lstFTfichereOuvert:
          (_,action,numFT,qualif)=idx
          if qualif==0: qualif=" "
          else: qualif=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%qualif)
          reserve='' #TODO ajuster avec le texte entré par l'utilisateur
          f.write(" %s %s-%06d {\\scriptsize %s} & {\\scriptsize %s}\\\\\n"%(action,fmtCh(self.refFT),numFT,qualif,reserve))
      f.write("& \\\\\n\\hline\n")

      # FT à clore
      f.write(self.fmtLex('LOCUT_REFERENCES_FT_A_CLORE')+"&"+self.fmtLex('LOCUT_RESERVES')+"\\\\\n")
      if len(lstFTficheaClore)==0:
        f.write(" %s & \\\\\n"%IT(self.fmtLex('LOCUT_SANS_OBJET')))
      else:
        for idx in lstFTficheaClore:
          (_,action,numFT,qualif)=idx
          if qualif==0: qualif=" "
          else: qualif=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%qualif)
          reserve='' #TODO ajuster avec le texte entré par l'utilisateur
          f.write(" %s %s-%06d {\\scriptsize %s} & {\\scriptsize %s}\\\\\n"%(action,fmtCh(self.refFT),numFT,qualif,reserve))
      f.write("& \\\\\n\\hline\n")
      f.write("\\end{tabularx}\n")
      f.close()

    if self.projet.execution!='S':
      if os.access(self.ficRapportFiche,os.F_OK):
        f=open(self.ficRapportFiche,"a",encoding='utf-8')
        f.write(CLEARPAGE)
        f.close()

    if self.projet.nivRapport==1 or \
       (self.projet.nivRapport==2 and self.projet.typePRJsimple):
      self.Valid_DeroulementTests(pathFiche,result,criticite)

  #-------------------------------------------------------------------------------
  # Gestion des boucles
  #-------------------------------------------------------------------------------
  def Valid_DebutBoucle(self,num,ident,condition,varList,execution):
    texte=DebFin+" \\textit{%s}\\\\\n"%(fmtCh(self.projet.lexique.entree("LOCUT_DEBUT_BOUCLE")%(num,ident)))
    if condition:
      texte+="\\\hspace{0.5cm}\\textit{%s}%s\\\\\n"%(fmtCh(self.projet.lexique.entree("LOCUT_CONDITION_EXEC")),fmtCh(condition))
    if varList:
      for i in varList:
        texte+="\\hspace{0.5cm}<%s> %s \\texttt{"%(fmtCh(i[0]),self.fmtLex("LOCUT_PREND_VALEUR_LISTE"))
        for e in range(len(i)):
          if e!=0:
            texte+=fmtCh(i[e])
            if e!=len(i)-1:
              texte+='}, \\texttt{'
        texte+='}\\\\\n'
    return texte

  #-------------------------------------------------------------------------------
  def Valid_FinBoucle(self,num,execution):
    return DebFin+" \\textit{%s}\\\\\n"%fmtCh(self.projet.lexique.entree("LOCUT_FIN_BOUCLE")%num)

  #-------------------------------------------------------------------------------
  # Gestion des débuts de parties
  # Specifique :
  #  - Affichage du Titre1 : "Scenario XXX" ou "Contrôles"
  #  - Insertion du texte (.txt) correspondant si existant
  #-------------------------------------------------------------------------------
  def Valid_DebutPartie(self,reference,separateurSuffixe,soustitre,scenario,repScenarios):
    if soustitre is not None:
      texte=Titre1(fmtCh(reference.replace('-','--')))
      texte=texte+fmtCh(self.projet.lexique.entree('INTRO_SCENARIO')%(reference,reference,soustitre))+SDL
    else:
      texte=Titre1(self.fmtLex('MOT_CONTROLES'))
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION),"a",encoding='utf-8')
    try:
      f.write(texte)
    except:
      print("------------------------------------------------------")
      nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : Ecriture du titre de la partie avec encodage impossible",style="Er")
      print(texte.encode(nngsrc.services.sys_stdout_encoding()))
      print("------------------------------------------------------")
    # Recherche du fichier texte (.bbc) de description de l'objectif dans le cas d'un scénario.
    if self.projet.nivRapport==2:
      if scenario is not None:
        fichierBBC=os.path.join(repScenarios,scenario)
        if repScenarios is not None and os.access(fichierBBC+".bbc",os.F_OK):
          f.write('\\\\\n')
          f.write("%s :\\\\\n"%BF(IT(self.fmtLex('MOT_OBJECTIF'))))
          self.inclureFichier(f,fichierBBC,bbc=True)
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_RapporterResultat(self,resFiche,html,description,pathFiche,tagFiche,varoperateur,rapport):
#    rapportFTfiche=html.rapportFTfiche
#    rapportFTficheReserves=html.rapportFTficheReserves
#    lstFTreOuvert=html.lstFTfichereOuvert
#    lstFTaClore=html.lstFTficheaClore

    fiche=os.path.basename(pathFiche)
    fiche=fiche.replace('\\','_').replace('/','_')
    (entite,methode,typo,nature,commentaire,exigences)=description
    if entite=='':
      entite=self.projet.lexique.entree('LOCUT_ND')
    exigences=exigences.replace(";",",").replace(",",", ")

    # Type,Entité,Méthode
    texte ="\\noindent\n"
    if self.projet.execution!='S':
      texte+="\\begin{tabularx}{16cm}{|Xr|}\n"
      texte+="\\hline\n"
      texte+="\\rowcolor{nngblue} \\textbf{%s : }%s & \\textbf{%s : } %s\\\\\n"%(self.fmtLex('MOT_TYPE'),fmtCh(typo),self.fmtLex('MOT_FONCTIONNALITE'),fmtCh(entite))
      texte+="\\multicolumn{2}{|>{\\columncolor{nngblue}}c|}{\\textbf{%s%s}} \\\\\n"%(fmtCh(fiche),fmtCh(tagFiche))
      texte+="\\rowcolor{nngblue} \\textbf{%s : }%s & \\textbf{%s : }%s  \\\\\n"%(self.fmtLex('MOT_METHODE'),fmtCh(methode),self.fmtLex('MOT_NATURE'),fmtCh(nature))
      texte+="\\hline\n"
      texte+="\\end{tabularx}\\\\\n"
      texte+="\\begin{tabularx}{16cm}{|X|}\n"
      texte+="\\hline\n"
      # ajout de la description
      texte+="\\textbf{%s : }%s\\\\\n"%(self.fmtLex('MOT_OBJECTIF'),self.TexteConversion(commentaire,os.path.join(self.projet.FICHES,os.path.dirname(pathFiche)),varoperateur))
      # ajout des exigences
      if exigences!='':
        texte+="\\hline\n"
        exigences=indexerExigFT(exigences,True)
        exigences=','.join(["[%s]"%e for e in exigences.split(',')])
        texte+="\\textbf{%s : }{\\scriptsize %s}\\\\\n"%(self.fmtLex('LOCUT_COUVERTURE_EXIGENCES'),exigences)
        self.indexExigences+=1
      texte+="\\hline\n"
      texte+="\\end{tabularx}\\\\\n"
    else:
      texte+=Titre4(self.fmtLex('MOT_DESCRIPTION'))
      texte+="{\\renewcommand{\\arraystretch}{1}\n"
      texte+="\\begin{tabular}{ll}\n"
      texte+="%s&%s\\\\\n"%(BF(self.fmtLex('MOT_ENTITE')),fmtCh(entite))
      texte+="%s&%s\\\\\n"%(BF(self.fmtLex('MOT_TYPE')),fmtCh(typo))
      texte+="%s&%s\\\\\n"%(BF(self.fmtLex('MOT_NATURE')),fmtCh(nature))
      texte+="%s&%s\\\\\n"%(BF(self.fmtLex('MOT_METHODE')),fmtCh(methode))
      texte +="\\end{tabular}}\\\\\n"
      # ajout de la description
      texte+="\\vspace{1pc}\\\\\n\\textbf{"+self.fmtLex('MOT_OBJECTIF')+" : }%s \\\\\n"%self.TexteConversion(commentaire,os.path.join(self.projet.FICHES,os.path.dirname(pathFiche)),varoperateur)
      # ajout des exigences
      if exigences!='':
        exigences=indexerExigFT(exigences,True)
        exigences=','.join(["[%s]"%e for e in exigences.split(',')])
        texte+="\\textbf{%s : }{\\scriptsize %s}\\\\\n\n"%(self.fmtLex('LOCUT_COUVERTURE_EXIGENCES'),exigences)
        self.indexExigences+=1
      # ajout des actions
      texte+=Titre4(self.fmtLex('MOT_ACTIONS'))
#      texte+=rapport

    if not self.projet.execution in ['P','S']:
      texte +="%=====================================================\n"
      texte +="\\begin{small}\n"
      texte +="\\begin{tabularx}{16cm}{|X|}\n"
      texte +="\\hline\n"
      texte +="\\rowcolor{nngblue} \\textbf{"+self.fmtLex('MOT_ACTIONS')+"}\\\\\n"
      texte +="\\hline\n"
      texte +="\\end{tabularx}\n"
      texte +="\\end{small}\\\\\n"
#    elif self.projet.execution=='S':
#
#      texte +="\\vspace{1pc}\\\\\n"
#      texte +=Titre4(self.fmtLex('MOT_ACTIONS'))
##      texte +="\\begin{small}\n"
##      texte +="\\begin{itemize}\n"
    elif self.projet.execution=='P':
      texte +="%=====================================================\n"
      texte +="\\begin{small}\n"
      texte +="\\begin{tabularx}{16cm}{|Xr|}\n"
      texte +="\\hline\n"
      texte +="\\rowcolor{nngblue} \\textbf{"+self.fmtLex('MOT_ACTIONS')+"} \\\\\n"
      texte +="\\hline\n"
      texte +="\\end{tabularx}\n"
      texte +="\\end{small}\\\\\n"

    texte+=rapport
    f=open(self.ficRapportFiche,"a",encoding='utf-8')
    # ajout du rapport de la fiche
#    disp=nngsrc.services.SRV_codeResultat(resFiche,self.constantes)
    # ajout du bilan
#    try:
    f.write(texte)
#    except :
#      print("------------------------------------------------------")
#      print(texte.encode(sys.stdout.encoding))
#      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ERREUR LaTeX : Ecriture du rapport de la fiche %s avec encodage impossible"%fiche,style="Er")
#      print("------------------------------------------------------")
    if not self.projet.execution in ['P','S']:
      # ecriture du tableau de controle en fin de test
      f.write('%=====================================================\n')
      f.write("\\begin{tabularx}{16cm}{|X|}\n\\hline\n")
      f.write("\\rowcolor{nngblue}\\textbf{"+self.fmtLex('MOT_CONTROLE')+"} \\\\\n")
      f.write("\\end{tabularx}\\\\\n")
      f.write("\\begin{tabularx}{16cm}{|X|X|X|X|X|X|}\n\\hline\n")
      if self.projet.debug==2:
        t=datetime.datetime(2016,2,29,12,34,56)
      else:
        t=datetime.datetime.now()
      case_OK="$\\Box$~"
      date_OK=''
      case_AV="$\\Box$~"
      date_AV=''
      case_AR="$\\Box$~"
      date_AR=''
      case_PB="$\\Box$~"
      date_PB=''
      case_NT="$\\Box$~"
      date_NT=''
      if self.projet.execution=='R':
        if resFiche==self.constantes['STATUS_OK']:
          case_OK="$\\boxtimes$~"
          date_OK=t.strftime("%d/%m/%Y")
        if resFiche==self.constantes['STATUS_AR']:
          case_AR="$\\boxtimes$~"
          date_AR=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_AV']:
          case_AV="$\\boxtimes$~"
          date_AV=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_PB']:
          case_PB="$\\boxtimes$~"
          date_PB=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_NT']:
          case_NT="$\\boxtimes$~"
          date_NT=t.strftime("%d/%m/%Y")
      f.write("%s & %s%s & %s%s & %s%s & %s%s & %s%s\\\\\n"%(self.fmtLex('MOT_BILAN'),case_OK,self.fmtLex('LOCUT_OK').replace(' ','~'),case_AV,self.fmtLex('LOCUT_A_VALIDER').replace(' ','~'),case_AR,self.fmtLex('LOCUT_AVEC_RESERVE').replace(' ','~'),case_PB,self.fmtLex('LOCUT_NOK').replace(' ','~'),case_NT,self.fmtLex('LOCUT_NON_TESTE').replace(' ','~')))
      f.write("%s & %s & %s & %s & %s & %s\\\\\n\\hline\n"%(self.fmtLex('MOT_DATE'),date_OK,date_AV,date_AR,date_PB,date_NT))
      f.write("\\end{tabularx}\n\\\\")
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_DeroulementTests(self,pathFiche,result,criticite):
    # ecriture de la grille de deroulement des tests
    if not self.projet.execution in ['P','S']:
      grillePath=os.path.join(self.repTravail,REPERTRAPPORT,"bilanControles%s"%DOCEXTENSION)
      f=open(grillePath,"a",encoding='utf-8')
      if self.projet.nivRapport==1:  # Niveau scenario
        pathFiche=pathFiche.replace('/',':').replace('\\',':').split('-')
        pathFiche='-'.join(pathFiche[2:])
      if self.projet.execution=='R':
        disp=nngsrc.services.SRV_codeResultat(result,self.constantes,self.projet.DOCUMENT_LANGUE)
        mot_criticite=self.fmtLex("MOT_NIVEAU_CRITIQUE_%s"%criticite)
        f.write("\\hline\n\\small{%s} & \\small{%s} & \\small{%s} & \\small{%s} \\\\\n"%(fmtCh(pathFiche),fmtCh(self.projet.DOCUMENT_REALISE_PAR),fmtCh(disp[3]),mot_criticite))
      #  f.write("\\hline\n\\small{" + fmtCh(pathFiche) + "} & \\small{" + fmtCh(self.projet.DOCUMENT_REALISE_PAR) + "} & \\small{" + fmtCh(disp[3]) + "} \\\\\n")
      if self.projet.execution=='C':
        f.write("\\hline\n%s &  &  &  \\\\\n"%fmtCh(pathFiche))
      f.close()

  #-------------------------------------------------------------------------------
  def Valid_RapporterNonApplicable(self,liste,ficliste,fichier,section,label,execution,retraitespace=True):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,fichier),"w",encoding='utf-8')
    f.write(Titre2(fmtCh(section)))
    minusSection=section.lower().split(' ')[0]

    lstindex=list(liste.keys())
    lstindex.sort()

    for index in lstindex:
      f.write(Titre3(fmtCh(ficliste[index][0])))
      if ficliste[index][1]=='':
        texte="%s\\\\\\-\\\\\n"%self.fmtLex('LOCUT_NONAPP_DESCRIPTION')
        texte=texte%minusSection
      else:
        texte="%s\\\\\\-\\\\\n"%fmtCh(ficliste[index][1])
      f.write(texte)
      nbColonnes=ficliste[index][2]
      taille=170//nbColonnes
      formatage="@{}L{%dmm}"%taille
      for e in range(nbColonnes-1): formatage+="L{%dmm}"%taille
      f.write("{\\setstretch{0.5}\n\\begin{longtable}{%s}\n"%formatage)
      trouve=False
      ncol=0
      ligne=1
      for elem,parent in liste[index]:
        if elem=='-':
          f.write("{\\scriptsize \\textit{%s}}"%fmtCh(parent[0]))#,0,True,True
        elif parent is None:
          f.write("{\\scriptsize %s}"%fmtCh(elem))#,0,True,True
        else:
          f.write("{\\scriptsize %s [{\\tiny\\textit{%s}}]}"%(fmtCh(elem),fmtCh(parent[0].replace(',',', '))))#,0,True,True,0,True,True
        trouve=True
        ncol+=1
        if ncol==nbColonnes:
          f.write("\\\\\n")
          ncol=0
          ligne+=1
        if ncol>0:
          f.write(" & ")
      if not trouve:
        f.write("{\\scriptsize \\textit{%s}}"%self.fmtLex('LOCUT_SANS_OBJET'))
      f.write("\n\\end{longtable}}\n")
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_AnnexesCouvertures(self,pListe,pDicoListe,pFicListe,fichier,section,label,execution,pourcentage,retraitespace=True):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,fichier),"w",encoding='utf-8')
    f.write(Titre2(fmtCh(section),txtLabel=label))
    minusSection=section[0].lower()+section[1:]
    f.write("%s\n"%fmtCh(self.projet.lexique.entree('LOCUT_ANNEXE_DESCRIPTION')%minusSection))

    # recuperation de la liste des exigences, puis classement (sort)
    lstsort=list()
    for elem in pListe:
      if retraitespace:
        lstsort.append(elem.replace('\n','').replace(' ',''))
      else:
        lstsort.append(elem.replace('\n',''))
    lstsort.sort()

    trouve=True
    while trouve:
      trouve=False
      if lstsort!=[] and lstsort[0]=='':
        lstsort=lstsort[1:]
        trouve=True

    # recuperation de la liste des index
    lstindex=[]
    if pFicListe is not None:
      lstindex=list(pFicListe.keys())
      lstindex.sort()

    if len(lstindex)==0:
      f.write("{\\scriptsize \\textit{%s}}\n"%self.fmtLex('LOCUT_SANS_OBJET'))
      f.close()
      return

    for uneMethode in self.projet.methode.keys():
      if self.projet.filtreMethode is None or uneMethode in self.projet.filtreMethode:
        methode=uneMethode
        terme=self.projet.methode[uneMethode]['terme']
        trouveMethode=False
        f.write(Titre3(fmtCh(terme)))
        for indexFichier in lstindex:
          # Introduction du paragraphe
          paragrapheIntro=''
          if pFicListe[indexFichier][0] is not None:
            paragrapheIntro=Titre4(fmtCh(pFicListe[indexFichier][0]))
            if pFicListe[indexFichier][1]=='':
              texte="%s\n"%self.fmtLex('LOCUT_ANNEXE_DESCRIPTION')
              texte=texte%minusSection
            else:
              texte="%s\\\\\n"%fmtCh(pFicListe[indexFichier][1])
            paragrapheIntro+=texte

          if True:
            if label in ['EXIGNONCOUV','FCTNONCOUV'] or execution!='R':
              listeEtats=[-1]
            else:
              listeEtats=[self.constantes['STATUS_PB'],self.constantes['STATUS_AV'],self.constantes['STATUS_OK'],self.constantes['STATUS_NT'],self.constantes['STATUS_AR']]
            # Accumulation des états
            trouve={}
            texte={}
            for etat in listeEtats:
              texte[etat]=''
              trouve[etat]=False
              if etat!=-1:
                disp=nngsrc.services.SRV_codeResultat(etat,self.constantes,self.projet.DOCUMENT_LANGUE)
                texte[etat]+="\\item %s %s\\\\\n"%(self.fmtLex('MOT_CONTROLES'),fmtCh(disp[4]))
              nbColonnes=pFicListe[indexFichier][2]
              taille=170//nbColonnes
              formatage="@{}L{%dmm}"%taille
              for e in range(nbColonnes-1): formatage+="L{%dmm}"%taille
              texte[etat]+="{\\setstretch{0.5}\n\\begin{longtable}{%s}\n"%formatage
              ncol=0
              ligne=1
              for elem in lstsort:
                if elem in pDicoListe and pDicoListe[elem]['ID']==indexFichier and methode in pDicoListe[elem]['methode']:# and (not horsNonAp or (horsNonAp and pDicoListe[elem]['applicable'])):
                  # Cas xxxCOUV : affiché si entree de pListe==self.constantes['STATUS_OK']
                  afficher=False
                  percent=''
                  if label in ['EXIGNONCOUV','FCTNONCOUV']:
                    afficher=True
                  elif etat==-1:
                    if label in ['EXIGCOUV','FCTCOUV'] and pListe[elem][0]==self.constantes['STATUS_OK']:
                      afficher=True
                      if pourcentage:
                        (_,nbOk,nbTot)=pListe[elem]
                        if self.projet.execution=='R': percent="(%3.2f\\%%)"%(nbOk*100.//nbTot)
                  elif label in ['EXIGCOUV','FCTCOUV'] and pListe[elem][0]==etat:
                      afficher=True
                      if pourcentage:
                        (_,nbOk,nbTot)=pListe[elem]
                        if self.projet.execution=='R': percent="(%3.2f\\%%)"%(nbOk*100.//nbTot)
                  if afficher:
                    if elem=='-':
                      texte[etat]+="{\\scriptsize \\textit{%s}}"%fmtCh(pDicoListe[elem]['parent'][0])#,0,True,True
                    elif pDicoListe[elem]['parent'] is not None:
                      texte[etat]+="{\\scriptsize %s%s [{\\tiny\\textit{%s}}]}"%(fmtCh(elem),percent,fmtCh(pDicoListe[elem]['parent'][0].replace(',',', ')))#,0,True,True,0,True,True
                    else:
                      texte[etat]+="{\\scriptsize %s%s}"%(fmtCh(elem),percent)#,0,True,True
                    trouve[etat]=True
                    trouveMethode=True
                    ncol+=1
                    if ncol==nbColonnes:
                      texte[etat]+="\\\\\n"
                      ncol=0
                      ligne+=1
                    if ncol>0:
                      texte[etat]+=" & "
              texte[etat]+="\n\\end{longtable}}\n"
            auMoinsUn=False
            for etat in listeEtats:
              auMoinsUn=auMoinsUn or trouve[etat]
            if auMoinsUn:
              f.write(paragrapheIntro)
              if len(listeEtats)!=1:
                f.write("{\\setstretch{0.5}\\begin{itemize}\n\\setlength{\\itemsep}{0pt}\n")
              for etat in listeEtats:
               # paragrapheIntro=''
                if trouve[etat]: f.write(texte[etat])
              if len(listeEtats)!=1:
                f.write("\\end{itemize}}\n")
              trouveMethode=True
        if not trouveMethode:
          f.write("{\\scriptsize \\textit{%s}}\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.close()

  #-------------------------------------------------------------------------------
  def rapport_donnees(self,listeKeyEnv,environnement,JDD,f):
    au_moins_un=False
    for orig in nngsrc.services.SRV_listeKeysDict(listeKeyEnv):
      if JDD is None: rapport=Titre3(self.fmtLex(orig))
      else: rapport=''
      rapportContenu=False
      for etat in listeKeyEnv[orig]:
        liste=environnement[etat]
        section=liste[0]
        rapportSectionContenu=False
        rapportSection=''
        if len(liste)>1:
          rapportSection+="\\-\\\\\n%s"%Titre4(fmtCh(section))
          rapportSection+="  \\begin{madescription}\n"
          for elem in range(len(liste)-1):
            donnee=liste[elem+1]
            if donnee[4]==JDD:
              valeur="        %s\n"%fmtCh(self.projet.lexique.entree('LOCUT_SA_VALEUR')%donnee[0])
              if donnee[1] is not None:
                commentaire=self.TexteConversion(donnee[1],'')
              else:
                commentaire="{\\itshape %s }"%self.fmtLex('LOCUT_DATA_NON_DEFINIE')
              envPrj=''
              variable="<%s>"%fmtCh(donnee[3])
              if donnee[2] is not None:
                eptype,epvar=donnee[2]
                if eptype=='ENV':
                  envPrj="\\\\\n        %s"%fmtCh(self.projet.lexique.entree('LOCUT_DONNEE_ENV')%epvar)
                  variable="\{%s\}"%fmtCh(donnee[3])
                  if self.projet.execution!='R':
                    valeur=''
                elif eptype=='PRJ':
                  envPrj="\\\\\n        %s"%fmtCh(self.projet.lexique.entree('LOCUT_DONNEE_PRJ')%epvar)
              rapportSection+="    \\item[%s] %s%s\\\\\n%s"%(variable,commentaire,envPrj,valeur)
              rapportSectionContenu=True
          rapportSection+="  \\end{madescription}\n"
        if rapportSectionContenu:
          rapport+=rapportSection
          rapportContenu=True
      if rapportContenu:
        try:
          f.write(rapport)
          au_moins_un=True
        except:
          print("------------------------------------------------------")
          nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : Ecriture des donnees produites avec encodage impossible",style="Er")
          print(rapport.encode(sys.stdout.encoding))
          print("------------------------------------------------------")
    if not au_moins_un:
      f.write(self.fmtLex('LOCUT_AUCUNE_DONNEES'))

  #-------------------------------------------------------------------------------
  def Valid_ListeDonnees(self,listeKeyEnv,environnement,jeuxDeDonnees,fichier):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,fichier),"w",encoding='utf-8')
    f.write(Titre1(self.fmtLex('TITRE1_LISTE_DONNEES'),txtLabel="LSTDATA"))
    f.write(Titre2(self.fmtLex('TITRE2_JEUX_DE_DONNEES')))
    if len(jeuxDeDonnees)>0:
      f.write("%s\n"%self.fmtLex('LOCUT_PARTIE_JEUX_DONNEES'))
      for JDD in jeuxDeDonnees:
        idJDD,descJDD=JDD
        f.write(Titre3(fmtCh(idJDD)))
        f.write("%s\\\\\n"%self.TexteConversion(descJDD,''))
        self.rapport_donnees(listeKeyEnv,environnement,idJDD,f)
      f.write("\\-\\\\\n%s\n"%IT(self.fmtLex('LOCUT_AVERTISSEMENT_JEUX_DONNEES')))
    else:
      f.write("\\-\\\\\n%s\n"%self.fmtLex('LOCUT_AUCUN_JEUX_DONNEES'))
    f.write(Titre2(self.fmtLex('TITRE2_AUTRES_DONNEES')))
    self.rapport_donnees(listeKeyEnv,environnement,None,f)
    f.close()

  #-------------------------------------------------------------------------------
  # Mise à jour et fin
  #-------------------------------------------------------------------------------
  def Valid_Terminer(self,bilan,rapportFT,listeKeyEnv,repImage,environnement,jeuxDeDonnees,nbControles):
    (total,nbCtrlBLOQ)=self.qualite.OkArAvPbNt
    methode=''
    for m in self.projet.methode.keys():
      methode+=m

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION),"a",encoding='utf-8')

    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      f.write(Titre1(self.fmtLex('TITRE1_FONCTIONNALITES')))
      motFctExg=self.projet.lexique.entree('MOT_FONCTIONNALITES').lower()
      texte="%s\n"%self.projet.lexique.entree('LOCUT_SELON_METHODE')%(motFctExg,methode)
      if self.projet.filtreMethode is not None and methode!=''.join(self.projet.filtreMethode):
        texte+="%s\n"%self.projet.lexique.entree('LOCUT_RESTRICTION_METHODE')%(', '.join(["'%s'"%self.projet.methode[m]['terme'] for m in self.projet.filtreMethode]))
      f.write(fmtCh(texte))
      if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
        texte="\n\n%s\n"%self.projet.lexique.entree('LOCUT_DECLINEES')%(motFctExg,motFctExg)
        f.write(fmtCh(texte))
      if self.projet.execution=='R' and self.projet.PRODUIT_FONCTIONNALITES_POURCENTAGE:
        f.write("%s\n"%self.fmtLex('LOCUT_ANNEXE_POURCENTAGE'))
      if len(self.projet.fonctionnalitesNap)>0:
        self.inclureFichier(f,"fonctionsNonApp")
      self.inclureFichier(f,"fonctionsNonCouvertes")
      if self.projet.MATTRAC_CTL_FCT or self.projet.MATTRAC_FCT_CTL:
        self.inclureFichier(f,"fonctionsCouvertes")

    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      f.write(Titre1(self.fmtLex('TITRE1_EXIGENCES')))
      motFctExg=self.projet.lexique.entree('MOT_EXIGENCES').lower()
      texte="%s\n"%self.projet.lexique.entree('LOCUT_SELON_METHODE')%(motFctExg,methode)
      if self.projet.filtreMethode is not None and methode!=''.join(self.projet.filtreMethode):
        texte+="%s\n"%self.projet.lexique.entree('LOCUT_RESTRICTION_METHODE')%(', '.join(["'%s'"%self.projet.methode[m]['terme'] for m in self.projet.filtreMethode]))
      f.write(fmtCh(texte))
      if self.projet.PRODUIT_EXIGENCES_DECLINEES:
        texte="\n\n%s\n"%self.projet.lexique.entree('LOCUT_DECLINEES')%(motFctExg,motFctExg)
        f.write(fmtCh(texte))
      if self.projet.execution=='R' and self.projet.PRODUIT_EXIGENCES_POURCENTAGE:
        f.write("%s\n"%self.fmtLex('LOCUT_ANNEXE_POURCENTAGE'))
      if len(self.projet.exigencesNap)>0:
        self.inclureFichier(f,"exigencesNonApp")
      self.inclureFichier(f,"exigencesNonCouvertes")
      if self.projet.MATTRAC_CTL_EXG or self.projet.MATTRAC_EXG_CTL:
        self.inclureFichier(f,"exigencesCouvertes")

      if self.projet.MATTRAC_CTL_EXG or self.projet.MATTRAC_CTL_FCT or self.projet.MATTRAC_EXG_CTL or self.projet.MATTRAC_FCT_CTL:
        self.inclureFichier(f,"matTracabilite")

    if not self.projet.execution in ['P','S']:
      f.write(Titre1(self.fmtLex('TITRE1_FAITTECH')))
      self.inclureFichier(f,"faitsTechniquesCouverts")
      self.inclureFichier(f,"faitsTechniquesOuverts")
      if len(self.qualite.lstFTreOuvert)>0:
        self.inclureFichier(f,"faitsTechniquesReOuverts")
      if len(self.qualite.lstFTaClore)>0:
        self.inclureFichier(f,"faitsTechniquesAClore")

    if self.projet.execution=='R':
      if os.access(os.path.join(self.repTravail,"notesManuelles.bbc"),os.F_OK):
        self.inclureFichier(f,"notesManuelles")
        texte=nngsrc.services.SRV_LireContenu(os.path.join(self.repTravail,"notesManuelles.bbc"))
#        texte+="[/ol]\n"
        g=open(os.path.join(self.repTravail,REPERTRAPPORT,"notesManuelles%s"%DOCEXTENSION),"w",encoding='utf-8')
        g.write(Titre1("Notes et commentaires"))
        g.write("%s%s"%(self.TexteConversion(texte,''),SDL))
        g.close()

    #--------EPILOGUE---
    if self.projet.EPILOGUE!='':
      try:
        if self.projet.execution in ['P','S']:
          logue=self.projet.EPILOGUE%'P'
        else:
          logue=self.projet.EPILOGUE%'C'
      except:
        logue=self.projet.EPILOGUE

      if self.projet.debug>1:
        if self.projet.PRODUIT_SOUS_REFERENCE!='' and not os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc",os.F_OK):
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Avertissement : Le fichier d'epilogue %s est absent."%(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc"),style="Wr")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Il sera remplace par %s"%(logue+".bbc"),style="Wr")
      if self.projet.PRODUIT_SOUS_REFERENCE!='' and os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+".bbc",os.F_OK):
        self.inclureFichier(f,logue+self.projet.PRODUIT_SOUS_REFERENCE,bbc=True)
      else:
        if os.access(logue+".bbc",os.F_OK):
          self.inclureFichier(f,logue,bbc=True)
        else:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"ATTENTION : Fichier epilogue %s.bbc non trouve."%logue,style="Wr")
    #--------ANNEXES---
    g=open(os.path.join(self.repTravail,REPERTRAPPORT,"annexes%s"%DOCEXTENSION),"a",encoding='utf-8')
    g.write("\\end{description}\n")
    g.close()
    if self.numAnnexe!=0:
      self.inclureFichier(f,"annexes")

    #--------Index---
    if self.indexExigences+self.indexGeneral!=0:
      f.write("\\chapter{Index}\n")
      if self.indexExigences!=0:
        if self.projet.execution in ['P','S']:
          f.write("\\printindex{exigences}{"+self.fmtLex('INDEX_EXIGENCES')+"}\n")
        else:
          f.write("\\printindex{exigences}{"+self.fmtLex('INDEX_EXIGENCES_FAITTECH')+"}\n")
      if self.indexGeneral!=0:
        f.write("\\printindex{general}{"+self.fmtLex('INDEX_GENERAL')+"}\n")

    #--------Fin du document---
    f.write("\\end{document}\n")
    f.close()

    #--------FT Ouvertes---
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"faitsTechniquesOuverts%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(Titre2(self.fmtLex('LOCUT_FAITS_TECHS_OUVERTS'),txtLabel="FT"))
    f.write("\\begin{nngftouverts}{%s}{%s}{%s}\n"%(self.fmtLex('MOT_REFERENCES'),self.fmtLex('MOT_SEVERITE'),self.fmtLex('MOT_CONTROLES')))
   # if self.projet.execution!='R' or self.projet.rapportManuel:
    if self.projet.execution=='R':
      for ft in rapportFT:
        qualif=ft['GRAVITE']
        if qualif==0: qualif=IT(self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%qualif))
        else: qualif=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%qualif)
        fiche=ft['LIBELLE'].split('\n')[1].split(' : ')[1].replace('/',' : ').replace('\\',' : ').split('@')[0]
        act=ft['LIBELLE'].split('\n')[2].split(' : ')[1]
        num=int(ft['ID'])
        if ft['STATUT']=="Doublon":
          f.write("{\\scriptsize %s-%06d (%s)}&{\\scriptsize %s}&{\\scriptsize %s (%s)}\\\\\n"%(fmtCh(self.refFT),num,self.fmtLex('LOCUT_DEJA_EMIS'),qualif,fmtCh(fiche),act))
        elif ft['STATUT'] not in ["Re-ouvert","A clore"]:
          f.write("{\\scriptsize %s-%06d}&{\\scriptsize %s}&{\\scriptsize %s (%s)}\\\\\n"%(fmtCh(self.refFT),num,qualif,fmtCh(fiche),act))
      f.write("& &\\\\\n")
#    elif self.qualite.newNumFT==0:
#      f.write("& &\\\\\n")
#      f.write("---"+self.fmtLex('LOCUT_SANS_OBJET')+"---& &\\\\\n")
#      f.write("& &\\\\\n")
    elif self.projet.execution=='C':
      f.write("& &\\\\\n")
      f.write("& &\\\\\n")
      f.write("& &\\\\\n")
      f.write("& &\\\\\n")
      f.write("& &\\\\\n")
      f.write("& &\\\\\n")
    f.write("\\hline\n\\end{nngftouverts}\n")
    f.close()

    #--------FT Re-Ouvertes---
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"faitsTechniquesReOuverts%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(Titre2(self.fmtLex('LOCUT_FAITS_TECHS_REOUVERTS')))
    f.write("\\begin{nngftouverts}{%s}{%s}{%s}\n"%(self.fmtLex('MOT_REFERENCES'),self.fmtLex('MOT_SEVERITE'),self.fmtLex('MOT_CONTROLES')))
    for idx in self.qualite.lstFTreOuvert:
      (fiche,action,numFT,gravite)=idx
      if gravite==0: gravite=IT(self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%gravite))
      else: gravite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%gravite)
      fiche=fiche.replace('/',':').replace('\\',':')
      if len(fiche.split(':'))>1: fiche=fiche.split(':')[1]
      f.write("{\\scriptsize %s-%06d}&{\\scriptsize %s}&{\\scriptsize %s (%s)}\\\\\n"%(fmtCh(self.refFT),numFT,gravite,fmtCh(fiche),action))
    f.write("& &\\\\\n")
    f.write("\\hline\n\\end{nngftouverts}\n")
    f.close()

    #--------FT A clore---
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"faitsTechniquesAClore%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(Titre2(self.fmtLex('LOCUT_FAITS_TECHS_A_CLORE')))
    f.write("\\begin{nngftouverts}{%s}{%s}{%s}\n"%(self.fmtLex('MOT_REFERENCES'),self.fmtLex('MOT_SEVERITE'),self.fmtLex('MOT_CONTROLES')))
    for idx in self.qualite.lstFTaClore:
      (fiche,action,numFT,gravite)=idx
      if gravite==0: gravite=IT(self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%gravite))
      else: gravite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%gravite)
      fiche=fiche.replace('/',':').replace('\\',':')
      if len(fiche.split(':'))>1: fiche=fiche.split(':')[1]
      f.write("{\\scriptsize %s-%06d}&{\\scriptsize %s}&{\\scriptsize %s (%s)}\\\\\n"%(fmtCh(self.refFT),numFT,gravite,fmtCh(fiche.replace('/',' : ').replace('\\',' : ')),action))
    f.write("& &\\\\\n")
    f.write("\\hline\n\\end{nngftouverts}\n")
    f.close()

    n_OK=''
    n_AR=''
    n_AV=''
    n_PB=''
    n_NT=''
    if self.projet.execution=='R':
      n_OK=bilan["vert"]
      n_AR=bilan["violet"]
      n_AV=bilan["orange"]
      n_PB=bilan["rouge"]
      n_NT=bilan["noir"]
    elif self.projet.execution=='C':
      n_OK="\\ \\ \\ \\ "
      n_AR="\\ \\ \\ \\ "
      n_AV="\\ \\ \\ \\ "
      n_PB="\\ \\ \\ \\ "
      n_NT="\\ \\ \\ \\ "
    total=bilan["vert"]+bilan["orange"]+bilan["rouge"]+bilan["noir"]+bilan["violet"]

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"bilanControles%s"%DOCEXTENSION),"a",encoding='utf-8')
    if not self.projet.execution in ['P','S']:
      f.write("\\end{nnggrilletests}\\\\\n\\\\\n")
      f.write("\\begin{tabular}{|p{3.5cm}|p{7cm}|b{5.05cm}|}\n\\hline\n")
      f.write(self.fmtLex('LOCUT_DATE_CONTROLE')+ " : & "+self.fmtLex('MOT_PAR')+" : & "+self.fmtLex('MOT_SIGNATURE')+" : \\\\\n")

      if self.projet.execution=='R':
        if self.projet.DOCUMENT_REALISE_DATE:
          f.write(fmtCh(nngsrc.services.SRV_dateOuAuto(self.projet.DOCUMENT_REALISE_DATE,self.projet)))
        f.write(" & ")
        if self.projet.DOCUMENT_REALISE_PAR:
          f.write(fmtCh(self.projet.DOCUMENT_REALISE_PAR))
        else:
          f.write("\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ ")
        if self.projet.DOCUMENT_REALISE_ORG!='':
          f.write(" (" + fmtCh(self.projet.DOCUMENT_REALISE_ORG) + ")")
        f.write(" & ")
        if self.projet.DOCUMENT_REALISE_SIGN!='':
          f.write("\\includegraphics[height=1.0cm,width=2.5cm]{"+os.path.basename(self.projet.DOCUMENT_REALISE_SIGN)+"}")
        f.write("\\\\\n")
      else:
        f.write(" & \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\ \\  & \\\\\n")
      f.write("& & \\\\\n& & \\\\\n\\hline\n")
      f.write("\\end{tabular}\n")
      f.write(Titre2(self.fmtLex('TITRE2_BILAN_CHIFFRE')))
      f.write(Titre3(self.fmtLex('MOT_GENERALITES')))
      titreGraphiques=Titre3(self.fmtLex('MOT_GRAPHIQUES'))
    else:
      f.write(Titre2(self.fmtLex('MOT_GENERALITES')))
      titreGraphiques=Titre2(self.fmtLex('MOT_GRAPHIQUES'))
    if self.projet.PRODUIT_EXIGENCES_TRACEES and self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      titreGraphiques+="%s\\\\\n"%self.fmtLex('LOCUT_PRESENTATION_GRAPHIQUES')
    elif self.projet.PRODUIT_EXIGENCES_TRACEES:
      titreGraphiques+="%s\\\\\n"%self.fmtLex('LOCUT_PRESENTATION_GRAPHIQUES_EXIG')
    elif self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      titreGraphiques+="%s\\\\\n"%self.fmtLex('LOCUT_PRESENTATION_GRAPHIQUES_FCT')
    divisExig=len(self.qualite.listeExig.keys())
    if divisExig==0:
      divisExig=1
    divisExig=float(divisExig)
    divisFct=len(self.qualite.listeFct.keys())
    if divisFct==0:
      divisFct=1
    divisFct=float(divisFct)
    f.write(rapport_bilan_debut(2))
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_CONTROLES_PLAN'),nbControles))
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_CONTROLES_DOCUMENT'),total))
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      f.write(rapport_bilan_separatrice(2))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_TOTAL_EXIG'),len(self.qualite.exigenceVSft)))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys())))
    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      f.write(rapport_bilan_separatrice(2))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_TOTAL_FCT'),len(self.qualite.fonctionVSft)))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys())))
    f.write(rapport_bilan_separatrice(2))
    if self.projet.execution in ['R','C','M']:
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_COUVERTS'),len(self.qualite.listeFT.keys())))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_BASE'),self.qualite.nbFTenCours))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_SUSPENDUS_BASE'),self.qualite.nbFTSuspendus))
    if self.projet.execution=='R':
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_NOUVEAUX'),self.qualite.newNumFT-self.qualite.NUM_FT))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_DEJA_EMIS'),self.qualite.nbFTdejaEmis))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_REOUVERTS'),len(self.qualite.lstFTreOuvert)))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_A_CLORE'),len(self.qualite.lstFTaClore)))
    elif self.projet.execution=='C':
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_NOUVEAUX'),''))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_DEJA_EMIS'),''))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_REOUVERTS'),''))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_A_CLORE'),''))
    f.write(rapport_bilan_fin())
    f.write(titreGraphiques)
    texte=''
    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      texte+="\\includegraphics[width=8cm]{graphiqueExig.png}"
    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      texte+="\\includegraphics[width=8cm]{graphiqueFonct.png}"
    f.write("%s\n"%texte)

    if not self.projet.execution in ['P','S'] :
      f.write(Titre3(self.fmtLex('MOT_CONTROLES')))
      f.write(rapport_bilan_debut(4))
      f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CONTROLES_DOCUMENT'),total))
      f.write(rapport_bilan_separatrice(4))
      if self.projet.execution=='R':
        if total!=0:
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_SUCCES'),n_OK+n_AR,self.fmtLex('MOT_SOIT'),(n_OK+n_AR)/float(total)))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_OK'),n_OK,self.fmtLex('MOT_SOIT'),n_OK/float(total),True))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AR'),n_AR,self.fmtLex('MOT_SOIT'),n_AR/float(total),True))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_ECHEC'),n_AV+n_PB+n_NT,self.fmtLex('MOT_SOIT'),(n_AV+n_PB+n_NT)/float(total)))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AV'),n_AV,self.fmtLex('MOT_SOIT'),n_AV/float(total),True))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NOK'),n_PB,self.fmtLex('MOT_SOIT'),n_PB/float(total),True))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NT'),n_NT,self.fmtLex('MOT_SOIT'),n_NT/float(total)))
          f.write(rapport_bilan_separatrice(4))
          f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NON_BLOQUANT'),nbCtrlBLOQ,self.fmtLex('MOT_SOIT'),nbCtrlBLOQ/float(total)))
      else:
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_SUCCES'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_OK'),'',self.fmtLex('MOT_SOIT'),'',True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AR'),'',self.fmtLex('MOT_SOIT'),'',True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_ECHEC'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AV'),'',self.fmtLex('MOT_SOIT'),'',True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NOK'),'',self.fmtLex('MOT_SOIT'),'',True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NT'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan_separatrice(4))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NON_BLOQUANT'),'',self.fmtLex('MOT_SOIT'),''))
      f.write(rapport_bilan_fin())
      if self.projet.execution=='R':
        f.write("\\\\\n~\\\\\n\\includegraphics[width=8cm]{graphiqueCtrl.png}\n")

      f.write(Titre3(self.fmtLex('LOCUT_FT_SUR_ACTIONS')))
      f.write(rapport_bilan_debut(2))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
      f.write(rapport_bilan_separatrice(2))
      if self.projet.execution=='R':
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_CRITIQUES_OUVERTS'),self.qualite.couvertureFT[4]))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_BLOQUANTS_OUVERTS'),self.qualite.couvertureFT[3]))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MAJEURS_OUVERTS'),self.qualite.couvertureFT[2]))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MINEURS_OUVERTS'),self.qualite.couvertureFT[1]))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_NQ_OUVERTS'),self.qualite.couvertureFT[0]))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_EMIS_SUSPENDUS'),self.qualite.couvertureFT[5]))
      elif self.projet.execution=='C':
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_BLOQUANTS_OUVERTS'),''))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MAJEURS_OUVERTS'),''))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MINEURS_OUVERTS'),''))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_NQ_OUVERTS'),''))
        f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_EMIS_SUSPENDUS'),''))
      f.write(rapport_bilan_fin())

      f.write(Titre3(self.fmtLex('LOCUT_FT_SUR_EXIG')))
      f.write(rapport_bilan_debut(4))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys())))
      f.write(rapport_bilan_separatrice(3))
      if self.projet.execution=='R':
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES'),self.qualite.couvertureExig[4],self.fmtLex('MOT_SOIT'),self.qualite.couvertureExig[4]/divisExig))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS'),self.qualite.couvertureExig[3],self.fmtLex('MOT_SOIT'),self.qualite.couvertureExig[3]/divisExig))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS'),self.qualite.couvertureExig[2],self.fmtLex('MOT_SOIT'),self.qualite.couvertureExig[2]/divisExig))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MINEURS'),self.qualite.couvertureExig[1],self.fmtLex('MOT_SOIT'),self.qualite.couvertureExig[1]/divisExig))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_NQ'),self.qualite.couvertureExig[0],self.fmtLex('MOT_SOIT'),self.qualite.couvertureExig[0]/divisExig))
      elif self.projet.execution=='C':
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MINEURS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_NQ'),'',self.fmtLex('MOT_SOIT'),''))
      f.write(rapport_bilan_fin())

      f.write(Titre3(self.fmtLex('LOCUT_FT_SUR_FCT')))
      f.write(rapport_bilan_debut(4))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys())))
      f.write(rapport_bilan_separatrice(3))
      if self.projet.execution=='R':
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_CRITIQUES'),self.qualite.couvertureFonc[4],self.fmtLex('MOT_SOIT'),self.qualite.couvertureFonc[4]/divisFct))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_BLOQUANTS'),self.qualite.couvertureFonc[3],self.fmtLex('MOT_SOIT'),self.qualite.couvertureFonc[3]/divisFct))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MAJEURS'),self.qualite.couvertureFonc[2],self.fmtLex('MOT_SOIT'),self.qualite.couvertureFonc[2]/divisFct))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MINEURS'),self.qualite.couvertureFonc[1],self.fmtLex('MOT_SOIT'),self.qualite.couvertureFonc[1]/divisFct))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_NQ'),self.qualite.couvertureFonc[0],self.fmtLex('MOT_SOIT'),self.qualite.couvertureFonc[0]/divisFct))
      elif self.projet.execution=='C':
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_CRITIQUES'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_BLOQUANTS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MAJEURS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MINEURS'),'',self.fmtLex('MOT_SOIT'),''))
        f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_NQ'),'',self.fmtLex('MOT_SOIT'),''))
      f.write(rapport_bilan_fin())

    f.close()

    # Non Applicable
    self.Valid_RapporterNonApplicable(self.projet.fonctionnalitesNap,self.projet.ficFonctionnalites,"fonctionsNonApp%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_FONCTIONS_NAP'),"FCTCOUV",self.projet.execution,False)
    self.Valid_RapporterNonApplicable(self.projet.exigencesNap,self.projet.ficExigences,"exigencesNonApp%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_EXIGENCES_NAP'),"EXIGCOUV",self.projet.execution)
    # Couverture
    self.Valid_AnnexesCouvertures(self.qualite.listeExig,self.projet.exigences,self.projet.ficExigences,"exigencesCouvertes%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_EXIGENCES_REF'),"EXIGCOUV",self.projet.execution,self.projet.PRODUIT_EXIGENCES_POURCENTAGE)
    self.Valid_AnnexesCouvertures(self.qualite.listeFct,self.projet.fonctionnalites,self.projet.ficFonctionnalites,"fonctionsCouvertes%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_FONCTIONS_REF'),"FCTCOUV",self.projet.execution,self.projet.PRODUIT_FONCTIONNALITES_POURCENTAGE,retraitespace=False)
    self.Valid_AnnexesCouvertures(self.qualite.listeFT,None,None,"faitsTechniquesCouverts%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_FAITS_TECHS_REF'),"FTCOUV",self.projet.execution,False)

    self.Valid_ListeDonnees(listeKeyEnv,environnement,jeuxDeDonnees,"listeDonnees%s"%DOCEXTENSION)

    # Non couverture
    nc=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repTravail,'nonCouv.txt'))

    #--- Liste de non couverture des exigences -------------------------------------------------------------------------------
    ind=1
    LISTENC=[]
    while nc[ind].find("--Fonctionnalites non couvertes--")!=0:
      LISTENC.append(nc[ind].replace('\n','').replace('\r',''))
      ind+=1
    self.Valid_AnnexesCouvertures(LISTENC,self.projet.exigences,self.projet.ficExigences,"exigencesNonCouvertes%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_NON_COUV_EXIG'),"EXIGNONCOUV",self.projet.execution,False)

    #--- Liste de non couverture des fonctionnalités -------------------------------------------------------------------------------
    ind+=1
    LISTENC=[]
    while ind<len(nc):
      LISTENC.append(nc[ind].replace('\n','').replace('\r',''))
      ind+=1
    self.Valid_AnnexesCouvertures(LISTENC,self.projet.fonctionnalites,self.projet.ficFonctionnalites,"fonctionsNonCouvertes%s"%DOCEXTENSION,self.projet.lexique.entree('LOCUT_NON_COUV_FCT'),"FCTNONCOUV",self.projet.execution,False,retraitespace=False)

    self.MatriceTracabilite()

    data=nngsrc.services.SRV_LireContenu(os.path.join(self.repTravail,REPERTRAPPORT,"bilanControles%s"%DOCEXTENSION))
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()
    data=data.replace("HEUREFIN",t.strftime("%Hh %Mm %Ss"))
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"bilanControles%s"%DOCEXTENSION),"w",encoding='utf-8')
    f.write(data)
    f.close()

    # on deplace les rapports dans le repertoire final
    destination=os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport)
    if not os.access(destination,os.F_OK):
      if not os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT),os.F_OK):
        os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))
      os.mkdir(destination)

    if self.projet.DOCUMENT_OBJET and self.projet.DOCUMENT_OBJET!='':
      try:
        if self.projet.execution in ['P','S']:
          logue=self.projet.DOCUMENT_OBJET%'P'
        else:
          logue=self.projet.DOCUMENT_OBJET%'C'
      except:
        logue=self.projet.DOCUMENT_OBJET

#    try:
#      if self.projet.execution in ['P','S']:
#        logue=self.projet.PROLOGUE%'P'
#      else:
#        logue=self.projet.PROLOGUE%'C'
#    except:
#      logue=self.projet.PROLOGUE
#    if os.access(logue+DOCEXTENSION,os.F_OK):
#      nngsrc.services.SRV_copier(logue+DOCEXTENSION,os.path.join(destination,os.path.basename(logue)+DOCEXTENSION),timeout=5,signaler=True)
#    if os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+DOCEXTENSION,os.F_OK):
#      nngsrc.services.SRV_copier(logue+self.projet.PRODUIT_SOUS_REFERENCE+DOCEXTENSION,os.path.join(destination,os.path.basename(logue+self.projet.PRODUIT_SOUS_REFERENCE)+DOCEXTENSION),timeout=5,signaler=True)

#    for fichier in self.projet.PROLOGUE_FICHIERS:
#      if os.access(fichier,os.F_OK):
#        nngsrc.services.SRV_copier(fichier,os.path.join(destination,os.path.basename(fichier)),timeout=5,signaler=True)

#    try:
#      if self.projet.execution in ['P','S']:
#        logue=self.projet.EPILOGUE%'P'
#      else:
#        logue=self.projet.EPILOGUE%'C'
#    except:
#      logue=self.projet.EPILOGUE
#    if os.access(logue+DOCEXTENSION,os.F_OK):
#      nngsrc.services.SRV_copier(logue+DOCEXTENSION,os.path.join(destination,os.path.basename(logue)+DOCEXTENSION),timeout=5,signaler=True)
#    if os.access(logue+self.projet.PRODUIT_SOUS_REFERENCE+DOCEXTENSION,os.F_OK):
#      nngsrc.services.SRV_copier(logue+self.projet.PRODUIT_SOUS_REFERENCE+DOCEXTENSION,os.path.join(destination,os.path.basename(logue+self.projet.PRODUIT_SOUS_REFERENCE)+DOCEXTENSION),timeout=5,signaler=True)
#
#    for fichier in self.projet.EPILOGUE_FICHIERS:
#      if os.access(fichier,os.F_OK):
#        nngsrc.services.SRV_copier(fichier,os.path.join(destination,os.path.basename(fichier)),timeout=5,signaler=True)

    #fichierTex=os.path.basename(self.ficRapport).split('.')[0]
    fichierTex=os.path.basename(self.ficRapport).replace(DOCEXTENSION,'')
    if os.name=="posix":
      f=open(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,"compiler.sh"),"w",encoding='utf-8')
      f.write("# !/usr/bin/env bash\n")
      f.write("# =======================================================================\n")
      f.write("cd `dirname $0`\n")
      f.write("cd %s\n"%self.ficRapport)
      f.write("chmod u+x compiler.sh\n")
      f.write("./compiler.sh $1\n")
      f.close()
      nngsrc.services.SRV_changePermission(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,"compiler.sh"),perm=0o775,recursif=False)

      f=open(os.path.join(destination,"compiler.sh"),"w",encoding='utf-8')
      DEL="rm -f"
      VIEWER='if [ "$1" == "-v" ]; then\n  %s %s.pdf&\nfi\n'
      f.write("# !/usr/bin/env bash\n")
      f.write("# =======================================================================\n")
    else:
      f=open(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,"compiler.bat"),"w",encoding='utf-8')
      f.write("cd %s\n"%self.ficRapport)
      f.write("call compiler.bat %1\n")
      f.close()
      f=open(os.path.join(destination,"compiler.bat"),"w",encoding='utf-8')
      DEL="del"
      VIEWER='if "%%1"=="EDITER" (\n  %s %s.pdf\n)\n'

    for idx in ("exigences","general"):
      f.write(DEL+" "+idx+".idx\n")
      f.write(DEL+" "+idx+".ilg\n")
      f.write(DEL+" "+idx+".ind\n")
      f.write(DEL+" "+idx+".tex\n")
    f.write(DEL+" "+fichierTex+".toc\n")
    f.write(DEL+" "+fichierTex+".aux\n")
    f.write('NONSTOP=''\n')
    f.write('if [ "$1" == "-ns" ]; then\n  NONSTOP="-interaction=nonstopmode"\nfi\n')
    f.write("pdflatex ${NONSTOP} "+fichierTex+".tex\n")
    for idx in ("exigences","general"):
      f.write("makeindex -s index.ist "+idx+".idx\n")
    f.write("pdflatex ${NONSTOP} "+fichierTex+".tex\n")
    f.write("pdflatex ${NONSTOP} "+fichierTex+".tex\n")#-interaction=nonstopmode
    f.write(VIEWER%(VIEWERLATEX,fichierTex))
    f.close()
    nngsrc.services.SRV_changePermission(os.path.join(destination,"compiler.sh"),perm=0o775,recursif=False)

    f=open(os.path.join(destination,"index.ist"),"w",encoding='utf-8')
    f.write('headings_flag       1\n')
    f.write('heading_prefix      "\\n  \\\\item \\\\textbf{"\n')
    f.write('heading_suffix      "}"\n')
    f.write('delim_0   "\\\\ \\\\dotfill\\\\ "\n')
    f.write('delim_1   "\\\\ \\\\dotfill\\\\ "\n')
    f.write('delim_2   "\\\\ \\\\dotfill\\\\ "\n')
    f.close()

    for extension in LISTE_EXTENSION:
      for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*'+extension)):
       # if not os.path.basename(elem) in ['bilan.txt','nonCouv.txt','nonList.txt']:
          ficDestination=os.path.join(destination,'.'.join(os.path.basename(elem).split('.')[:-1])+extension)
          if os.access(ficDestination,os.F_OK):
            nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
          nngsrc.services.SRV_deplacer(elem,ficDestination,timeout=5,signaler=True)

    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,"*%s"%DOCEXTENSION)):
      ficDestination=os.path.join(destination,os.path.basename(elem))
      if os.access(ficDestination,os.F_OK):
        nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    for elem in ["graphiqueCtrl.png","graphiqueExig.png","graphiqueFonct.png"]:
      ficdest=os.path.join(destination,elem)
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      if os.access(os.path.join(self.repTravail,elem),os.F_OK):
        nngsrc.services.SRV_copier(os.path.join(self.repTravail,elem),ficdest,timeout=5,signaler=True)

    if os.path.exists(self.projet.CLIENT_LOGO):
      nngsrc.services.SRV_copier(self.projet.CLIENT_LOGO,os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport,"logoClient.png"))
    if os.path.exists(self.projet.SOCIETE_LOGO):
      nngsrc.services.SRV_copier(self.projet.SOCIETE_LOGO,os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport,"logoSociete.png"))

    for f in ['FichierInexistant.png','nangu_fr.sty','nangu_en.sty','nangu.png','action.png']:
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)

    num=1
    f="etape%d.png"%num
    while os.access(os.path.join(self.repRessources,f),os.F_OK):
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)
      num+=1
      f="etape%d.png"%num


    for f in self.listeBBCimageInclusion:
      nngsrc.services.SRV_copier(f,os.path.join(destination,os.path.basename(f)),timeout=5,signaler=True)

    # Elements d'IHM
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.png')):
      ficDestination=os.path.join(destination,os.path.basename(elem))
      if os.access(ficDestination,os.F_OK):
        nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    if repImage is not None:
      for elem in glob.glob(os.path.join(repImage,'*.*')):
        ficDestination=os.path.join(destination,os.path.basename(elem))
        if os.access(ficDestination,os.F_OK):
          nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
        nngsrc.services.SRV_copier(elem,destination,timeout=5,signaler=True)

    for f in [self.projet.DOCUMENT_REDIGE_SIGN,self.projet.DOCUMENT_REDIGE2_SIGN,self.projet.DOCUMENT_VERIFIE_SIGN,self.projet.DOCUMENT_VERIFIE2_SIGN,self.projet.DOCUMENT_VALIDE_SIGN,self.projet.DOCUMENT_REALISE_SIGN]:
      if f!='':
        ficSignature=os.path.basename(f)
        if ficSignature!='':
          nngsrc.services.SRV_copier(f,os.path.join(destination,ficSignature),timeout=5,signaler=True)

    os.rmdir(os.path.join(self.repTravail,REPERTRAPPORT))

    # Archivage
    if self.projet.ARCHIVAGE:
      if not self.projet.execution in ['X','K']:
        fichier='-'.join(self.ficRapport.split('-')[:-1])
        fichCmplt=os.path.join(self.repRapport,self.projet.execution,"LaTeX",fichier+".tgz")
        if os.access(fichCmplt,os.F_OK):
          nngsrc.services.SRV_detruire(fichCmplt,timeout=5,signaler=True)
        ficTGZ=tarfile.open(fichCmplt,mode="w:gz",encoding='utf-8')
        ficTGZ.add(os.path.join(self.repRapport,self.projet.execution,"LaTeX",self.ficRapport),arcname=self.ficRapport)
        ficTGZ.close()
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ARCHIVE_DANS')%fichCmplt,style="r")

  #-------------------------------------------------------------------------------
  def sousMatriceDirecte(self,f,fiche,tag,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.fmtLex('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Reference du contrôle, sa methode, sa typo
    f.write('%s'%(fmtCh(os.path.basename(fiche)+tag+extAction)))
    f.write(' & %s'%fmtCh(methode))
    f.write(' & %s'%fmtCh(typo))
    # Si mode R ou C, le fait technique ou vide
    if self.projet.execution=='R':
      f.write(' & %s'%strFT)
    elif self.projet.execution=='C':
      f.write(' & \_\_\_\_\_\_\_\_ ')
    # Exigence et type d'exigence
    if exigFct=='':
      f.write(' & -')
      f.write(' & -')
    else:
      f.write(' & %s'%fmtCh(exigFct))
      f.write(' & %s'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Severite
    f.write(' & %s'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write(' & %s'%' '.join(fmtCh(n) for n in faitTechCouv))
      else:
        f.write(' & %s'%IT(self.fmtLex('LOCUT_SANS_OBJET')))
    f.write('\\\\\n')

  #-------------------------------------------------------------------------------
  def sousMatriceInverse(self,f,fiche,tag,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.fmtLex('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Exigence et type d'exigence
    f.write('%s'%fmtCh(exigFct))
    if exigFct=='': f.write(' & -')
    else: f.write(' & %s'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Reference du contrôle, sa methode, sa typo
    f.write(' & %s'%fmtCh(os.path.basename(fiche)+tag+extAction))
    f.write(' & %s'%fmtCh(methode))
    f.write(' & %s'%fmtCh(typo))
    # Si mode R ou C, le fait technique ou vide
    if self.projet.execution=='R':
      f.write(' & %s'%strFT)
    elif self.projet.execution=='C':
      f.write(' & \_\_\_\_\_\_\_\_ ')
    # Severite
    f.write(' & %s'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write(' & %s'%' '.join(fmtCh(n) for n in faitTechCouv))
      else:
        f.write(' & %s'%IT(self.fmtLex('LOCUT_SANS_OBJET')))
    f.write('\\\\\n')

  #-------------------------------------------------------------------------------
  def MatriceTracabilite(self):
    #--- OUVERTURE ET PREPARATION DU FICHIER
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,'matTracabilite%s'%DOCEXTENSION),"w",encoding='utf-8')
    #if self.projet.execution not in ['P','S']:
    f.write("\\begin{landscape}\n")
    f.write(Titre1(self.fmtLex('LOCUT_MATRICE_TRACABILITE')))


    #--- MATRICE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_EXG:
      f.write(Titre2(self.fmtLex('LOCUT_MATRICE_COUV_EXIG')))
      f.write("{\\scriptsize\n")
      if self.projet.execution in ['R','C']:
        f.write("\\begin{nngtabular}{|lc|c|c|lc|c|c|}{%s&%s&%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('LOCUT_FAIT_TECH'),
                 self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        f.write("\\begin{nngtabular}{|lc|c|lc|c|}{%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        # On parcours les actions de niveau k
        for act in self.qualite.matTracabilite[k]:
          ((rang,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,exigences)=act
          entite,methode,typo,nature,_,exigGlobales=desc
          exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
          faitTechCouv=faitTechCouv.split(',')
          listNumFT=[]
          if numFT is not None: listNumFT.append(numFT)
          while '' in faitTechCouv: faitTechCouv.remove('')
          ficheTagFiche="%s%s"%(fiche,tagFiche)
          if True: #numFT:
            for eg in exigGlobales:
              if eg!='':
                if eg not in ListeExigGlobales:
                  ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                else:
                  precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                  if k>precedk or numFT is not None:
                    if numFT is not None:
                      precednumFT.append(numFT)
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
          exigences=exigences.replace(';',',').replace('\\n',',').split(',')
          while '' in exigences: exigences.remove('')
          for e in exigences:
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,severite,' (%s)'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((_,fiche,_,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,severite,' (%s)'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
          else:
            for numFT in listNumFT:
              self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,severite,' (%s)'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
      f.write("\\end{nngtabular}}\n")

    #--- MATRICE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_FCT:
      f.write(Titre2(self.fmtLex('LOCUT_MATRICE_COUV_FCT')))
      f.write("{\\scriptsize\n")
      # En exécution R ou C on ajoute les colonnes fait tech., controleur et date
      if self.projet.execution in ['R','C']:
        f.write("\\begin{nngtabular}{|lc|c|c|lc|c|c|}{%s&%s&%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('LOCUT_FAIT_TECH'),
                 self.fmtLex('MOT_FONCTIONNALITE'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        f.write("\\begin{nngtabular}{|lc|c|lc|c|}{%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('MOT_FONCTIONNALITE'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_SEVERITE')))
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        for act in self.qualite.matTracabilite[k]:
          ((rang,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,exigences)=act
          entite,methode,typo,nature,_,exigGlobales=desc
          if action=="act1": #action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,entite,self.projet.fonctionnalites,severite,'')
      f.write("\\end{nngtabular}}\n")

    #--- MATRICE INVERSE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------
    if self.projet.MATTRAC_EXG_CTL:
      f.write(Titre2(self.fmtLex('LOCUT_MATRICE_INV_COUV_EXIG')))
      f.write("{\\scriptsize\n")
      if self.projet.execution in ['R','C']:
        f.write("\\begin{nngtabular}{|lc|lc|c|c|c|c|}{%s&%s&%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('LOCUT_FAIT_TECH'),
                 self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        f.write("\\begin{nngtabular}{|lc|lc|c|c|c|}{%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeExig):
        # On parcours les actions de niveau k
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          if k==0: severite=IT(severite)
          for act in self.qualite.matTracabilite[k]:
            ((rang,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,exigences)=act
            entite,methode,typo,nature,_,exigGlobales=desc
            faitTechCouv=faitTechCouv.split(',')
            listNumFT=[]
            if numFT is not None: listNumFT.append(numFT)
            while '' in faitTechCouv: faitTechCouv.remove('')
            ficheTagFiche="%s%s"%(fiche,tagFiche)
            if True: #numFT: # if action == "act1":  # action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
              exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
              for eg in exigGlobales:
                if eg!='':
                  if eg not in ListeExigGlobales:
                    ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                  elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                  else:
                    precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                    if k>precedk or numFT is not None:
                      if numFT is not None and numFT not in precednumFT:
                        precednumFT.append(numFT)
                      ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
            exigences=exigences.replace(';',',').replace('\\n',',').split(',')
            while '' in exigences: exigences.remove('')
            if e in exigences:
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,severite,' (%s)'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((rang,fiche,scenario,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,severite,' (%s)'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
          else:
            for numFT in listNumFT:
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,severite,' (%s)'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
      f.write("\\end{nngtabular}}\n")

    #--- MATRICE INVERSE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------
    if self.projet.MATTRAC_FCT_CTL:
      f.write(Titre2(self.fmtLex('LOCUT_MATRICE_INV_COUV_FCT')))
      f.write("{\\scriptsize\n")
      if self.projet.execution in ['R','C']:
        f.write("\\begin{nngtabular}{|lc|lc|c|c|c|c|}{%s&%s&%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_FONCTIONNALITE'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('LOCUT_FAIT_TECH'),
                 self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        f.write("\\begin{nngtabular}{|lc|lc|c|c|}{%s&%s&%s&%s&%s&%s}\n"%
                (self.fmtLex('MOT_FONCTIONNALITE'),self.fmtLex('MOT_METHODE'),
                 self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                 self.fmtLex('MOT_SEVERITE')))
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeFct):
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          if k==0: severite=IT(severite)
          listeFCT=[]
          for act in self.qualite.matTracabilite[k]:
            ((rang,fiche,scenario,tagFiche,action,desc,faitTechCouv),numFT,etatFT,exigences)=act
            entite,methode,typo,nature,_,exigGlobales=desc
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            if e==entite and not fiche in listeFCT:
              listeFCT.append(fiche)
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.fonctionnalites,severite,'')
      f.write("\\end{nngtabular}}\n")

    #if self.projet.execution not in ['P','S']:
    f.write("\\end{landscape}\n")
    f.close()

  #-----------------------------------------------------------------------------------
  def Valid_DonneesAnnexe(self,execution,fichier,titre,sizeh,sizel):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"annexes%s"%DOCEXTENSION),"a",encoding='utf-8')
    f.write("\n\\item["+self.fmtLex('MOT_ANNEXE')+" %d] :\\label{D%d}\n"%(self.numAnnexe,self.numAnnexe))
    fichierDisque=nngsrc.services.SRV_retraitIndexation(fichier[4:])
    if fichier[:4]=="Txt_":
      if not '.txt' in LISTE_EXTENSION:
        LISTE_EXTENSION.append('.txt')
      annexe="Annexe%d.txt"%self.numAnnexe
      f.write(self.fmtLex('LOCUT_FICHIER_TEXTE')+"\\\\\n"+fmtCh(titre)+"\\\\\n")
      if execution=='R':
        if os.access(fichierDisque,os.F_OK):
          nngsrc.services.SRV_copier(fichierDisque,os.path.join(self.repTravail,REPERTRAPPORT,annexe),timeout=5,signaler=True)
#         Début des modifs, MGM - 2011-07-22
#         Pour mettre en forme le fichier de sortie de fonction avant de l'écrire dans les annexes
          lignes=nngsrc.services.SRV_LireContenuListe(fichierDisque)
          if len(lignes)==0:
            f.write("\\textit{%s}\\\\\n"%self.fmtLex('LOCUT_FICHIER_VIDE'))
          else:
            f.write("\\begin{lstlisting}[frame=single]\n")
            for ligne in lignes:
              if ligne:
                ligne=nngsrc.services.SRV_Encodage(ligne,"Recuperation du fichier de sortie de fonction").replace('\n','')
                f.write(ligne+"\n")
            f.write("\\end{lstlisting}\n")
#         Fin des modifs, MGM - 2011-07-22
        else:
          f.write("\\\\\n"+self.fmtLex('LOCUT_FICHIER_INEXISTANT')+"\\\\\n")
    else:
      if not os.path.splitext(fichierDisque)[1] in LISTE_EXTENSION:
        LISTE_EXTENSION.append(os.path.splitext(fichierDisque)[1])
      annexe="Annexe%d%s"%(self.numAnnexe,os.path.splitext(fichierDisque)[1])
      f.write(self.fmtLex('LOCUT_FICHIER_IMAGE')+"\\\\\n"+TT(fmtCh(fichier[4:]))+"\n")
      if execution=='R':
        if os.access(fichierDisque,os.F_OK):
          nngsrc.services.SRV_copier(fichierDisque,os.path.join(self.repTravail,REPERTRAPPORT,annexe),timeout=5,signaler=True)
          f.write("\\begin{figure}[H]\n")
          f.write("\\caption{%s}"%annexe.split('.')[0].replace('Annexe',self.fmtLex('MOT_ANNEXE')))
          f.write("\\begin{center}\n")
          taille=''
          if sizeh is not None:
            taille="height=%smm,"%sizeh
          if sizel is not None:
            taille="width=%smm,"%sizel
          if taille=='':
            taille="width=90mm"
          else:
            taille=taille[:len(taille)-1]
          f.write("\\includegraphics[%s]{%s}\n"%(taille,annexe))
          f.write("\\end{center}\n")
          f.write("\\end{figure}\n")
        else:
          f.write(self.fmtLex('LOCUT_FICHIER_INEXISTANT')+"\\\\\n")
    f.close()

  #-----------------------------------------------------------------------------------
  def GestionConfirmationFT(self,etatAction,disp,FTconfirme,FTreserve,etatFT):
    if etatAction!="NOK":
      etatAction=disp[2]
      if etatAction=="NOK" and self.projet.CONFIRMATION_FT=="oui" and not FTconfirme:
        disp=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
        etatAction=disp[2]
    return etatAction,disp

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_2colonnes_debut(self,clfAction,bilan):
    if self.projet.execution=='P':
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|}\n"
      texte+="\\hline\n%s&%s\\\\\n"%(Atester,BF(fmtCh(clfAction)))
    else:
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{1.5cm}|}\n"
      texte+="\\hline\n%s&\\textbf{%s} &%s\\\\\n"%(Atester,fmtCh(clfAction),bilan) #TResult)
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_2colonnes_milieu(self,inRapport,valeur,style,bilan='',suspendu=False,raison=None,cssBilan=''):
  #  if option!='': option+="O"
    if suspendu: bilan="SUS"
    if type(valeur)==str:
      if valeur=="'*n*'": valeur=''
      if self.projet.execution=='P':
        texte="&%s\\\\\n"%(fmtStyle(style,fmtCh(valeur)))
      else:
        texte="&%s&%s\\\\\n"%(fmtStyle(style,fmtCh(valeur)),bilan)
    else:
      texte=''
      for e in valeur:
        if type(e) is str:
          if e=="'*n*'": e=''
          if self.projet.execution=='P':
            texte+="&%s\\\\\n"%(fmtStyle(style,fmtCh(e)))
          else:
            texte+="&%s&%s\\\\\n"%(fmtStyle(style,fmtCh(e)),bilan)
        else:
          com,doc,titre=e
          val=self.fmtLex(com)
          sty='tt'
          if com=='LOCUT_SUIVRE_LIEN' or com=='LOCUT_CLIQUER_DEPLIER':
            self.numAnnexe+=1
            document=doc.split("|")
            sizeh=None
            sizel=None
            for s in range(len(document)-1):
              if document[s+1][0]=='h': sizeh=document[s+1][1:]
              if document[s+1][0]=='l': sizel=document[s+1][1:]
            self.Valid_DonneesAnnexe(self.projet.execution,document[0],titre,sizeh,sizel)
            com=''
            val="%s (%s)"%(self.fmtLex('LOCUT_VOIR_DONNEE_ANNEXE'),self.fmtLex('LOCUT_ANNEXE') + str(self.numAnnexe))
            sty='it'
          else:
            com=self.fmtLex(com)
            val=fmtCh(doc)#,2
          if self.projet.execution=='P':
            texte+="&%s %s\\\\\n"%(com,fmtStyle(sty,val))
          else:
            texte+="&%s %s&%s\\\\\n"%(com,fmtStyle(sty,val),bilan)

        bilan=''
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_2colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  def rapport_2colonnes(self,inRapport,clfAction,valAction,styAction,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    texte =self.rapport_2colonnes_debut(clfAction,TResult+BF(self.fmtLex('ABR_BILAN')))
    texte+=self.rapport_2colonnes_milieu(inRapport,valAction,styAction,bilan,suspendu,raison,cssBilan)
    texte+=self.rapport_2colonnes_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  def rapport_3colonnes_debut(self,clfAction,clfConstat,bilan):
    if self.projet.execution=='P':
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|}\n"
      texte+="\\hline\n%s&"%Atester
      texte+="%s"%BF(clfAction)
    else:
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{3.2cm}|p{1.5cm}|}\n"
      texte+="\\hline\n%s&"%Atester
      texte+="%s&"%BF(clfAction)
      texte+="%s&"%BF(clfConstat)
      texte+="%s"%bilan #%TResult
    texte+="\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  def rapport_3colonnes_action(self,valAction,styAction):
    texte ="&"
    for e in range(len(valAction)):
      texte+="%s "%fmtStyle(styAction[e],fmtCh(valAction[e]))
    if self.projet.execution!='P':
      texte+="& &"
    texte+="\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  def rapport_3colonnes_milieu(self,inRapport,clfAttendu,valAttendu,valConstat,styConstat,bilan='',suspendu=False,raison=None,cssBilan=''):
    if suspendu: bilan="SUS"
    texte ="&"
    texte+="%s %s"%(clfAttendu,TT(valAttendu))
    if self.projet.execution!='P':
      if type(valConstat)==str:
        if valConstat=="'*n*'": valConstat=''
        texte+="&%s&"%fmtStyle(styConstat,valConstat)
        texte+=bilan
#      else: #--liste--
#        texte=''
#        for v in valAction:
#          texte+="&%s&%s\\\\\n"%(TT(v),bilan)
#          bilan=''
    texte+="\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_3colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #                                                                <-- fin... plus tard
  def rapport_3colonnes(self,inRapport,clfAction,valact,styact,clfConstat,valConstat,styConstat,clfAttendu,valAttendu,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,vvaleur=''):
    texte =self.rapport_3colonnes_debut(clfAction,clfConstat,TResult+BF(self.fmtLex('ABR_BILAN')))
    if len(valact)>0:
      texte+=self.rapport_3colonnes_action(valact,styact)
    vBilan=bilan
    for num in range(len(clfAttendu)):
      texte+=self.rapport_3colonnes_milieu(inRapport,clfAttendu[num],valAttendu[num],valConstat[num],styConstat,vBilan,suspendu,raison,cssBilan)
      if num==0:
        cssBilan=''
        vBilan=''
    texte+=self.rapport_3colonnes_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  def Valid_BilanMode(self,fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,execution):
    CTL_MODE=params['CTL_MODE']
    CTL_COMMENT=params['CTL_COMMENT'].split('\n')
    CTL_BOUCLE=params['CTL_BOUCLE']
    CTL_FT=params['CTL_FT']
    CTL_EXIGENCES=params['CTL_EXIGENCES']
    CTL_INDEX=self.TexteConversion(str(params['CTL_INDEX']),'')
    CTL_ID=params['CTL_ID']
    CTL_CONDITION=params['CTL_CONDITION']
    CTL_FTQUALIF=params['CTL_FTQUALIF']
    if CTL_FTQUALIF==0:
      CTL_FTQUALIF=None
    inRapport=params['CTL_RAPPORT']

    #----------------------------------
    # Bandeau de description de l'action
    #
    #--1- Item et Description ---
    sautBandeau=True
    ftech=CTL_FTQUALIF

    resDocument=''
    if ftech:
      resDocument+=self.rapport_description_debut("%s"%(self.fmtLex('LOCUT_ACTION_JUGEE')))
    else:
      resDocument+=self.rapport_description_debut(inRapport=inRapport)
    for l in range(len(CTL_COMMENT)):
      if l==0:
        resDocument+=self.rapport_description_milieu(self.TexteConversion(CTL_COMMENT[l]),item=CTL_INDEX,ft=ftech,inRapport=inRapport)
        if ftech:
          ftech=-1
      else:
        resDocument+=self.rapport_description_milieu(self.TexteConversion(CTL_COMMENT[l]),ft=ftech,inRapport=inRapport)

    if self.projet.execution in ['E','S','P']:
      if 'RES_COMMENT' in params and params['RES_COMMENT'] is not None:
        if type(params['RES_COMMENT'])==str:
          resDocument+=self.rapport_description_milieu("\\hspace*{1pc}{\\scriptsize %s}"%(fmtCh(self.projet.lexique.entree('LOCUT_RESULTAT_ATTENDU'))+fmtCh(params['RES_COMMENT'])),ft=ftech)
        else:
          for e in range(len(params['RES_COMMENT'])):
            if e==0:
              resDocument+=self.rapport_description_milieu("\\hspace*{1pc}{\\scriptsize %s}"%fmtCh(self.projet.lexique.entree('LOCUT_RESULTAT_ATTENDU')),ft=ftech)
            resDocument+=self.rapport_description_milieu("\\hspace*{2pc}{\\scriptsize - %s}"%fmtCh(fmtCh(params['RES_COMMENT'][e])),ft=ftech)
    #
    #--2- Ajout des exigences éventuelles ---
    if CTL_EXIGENCES!='':
      resDocument+=self.rapport_description_milieu(exigence=indexerExigFT(CTL_EXIGENCES,True),ft=ftech)
      sautBandeau=False
    #
    #--3- Ajout des faits techniques éventuels ---
    if CTL_FT!='' and CTL_FT is not None :
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_FAITS_TECHS')+indexerExigFT(CTL_FT)),ft=ftech)
      sautBandeau=False
    #
    #--4- Identifiant de condition ---
    if CTL_ID!='':
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_IDENTIFIANT')+CTL_ID),ft=ftech)#,2,False))
      sautBandeau=False
    #
    if CTL_CONDITION is not None:
      CTL_CONDITION=CTL_CONDITION.replace('[','<').replace(']','>')
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_CONDITION_EXEC')+CTL_CONDITION),ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--5- Identifiant de boucle ---
    if CTL_BOUCLE is not None:
      resDocument+=self.rapport_description_milieu(petit=self.fmtLex('LOCUT_BOUCLE_ACTION')+CTL_BOUCLE[0],ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--6- Fin du bandeau ---
    if sautBandeau:
      resDocument+=self.rapport_description_milieu(ft=ftech)

    if self.projet.execution in ['S','P','C'] and inRapport!=0:
      resDocument+=self.rapport_description_milieu("\\hspace*{1pc}{\\scriptsize \\textit{%s}}"%fmtCh(self.projet.lexique.entree('LOCUT_ACTION_HORS_RAPPORT')))

    resDocument+=self.rapport_description_fin()
    #----------------------------------

  # Rapport Bilan de l'action
  # Mode 1,2              CAHIER/RAPPORT                                                      PLAN
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  # Mode 1,3,1
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   3  |> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #      |  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   1  | CLF_RAPPORT                                        |     1  | CLF_RAPPORT                                        |
  #      | VAL_RAPPORT                                        |        |                                                    |
  #      +----------------------------------------------------+        +----------------------------------------------------+
    if self.projet.execution!='S' and inRapport==0:
      # Mode niveau 1 à 1
      clef=self.fmtLex(params['CLF_COMMENT'])
      valeur=fmtCh(params['VAL_COMMENT'].replace('%s','£s'),tt=True).replace('£s','%s')
      substit=[fmtCh(x) for x in params['SUB_COMMENT']]
      style=params['STY_COMMENT']
      lien=params['LNK_COMMENT']
      resDocument+=self.rapport_contenuMode(clef,valeur,substit,style,lien)
      # Mode niveau 2 à 1
      if len(CTL_MODE)>1 and CTL_MODE[1]=='1':
        clef=self.fmtLex(params['CLF_COMPLEMENT'])
        valeur=fmtCh(params['VAL_COMPLEMENT'].replace('%s','£s')).replace('£s','%s')
        substit=[fmtCh(x) for x in params['SUB_COMPLEMENT']]
        style=params['STY_COMPLEMENT']
        lien=params['LNK_COMPLEMENT']
        liste=params['LST_COMPLEMENT']
        if len(liste)>0:
          resDocument+=self.rapport_contenuMode_deb(clef)
          resDocument+=self.rapport_contenuMode_mil(valeur,substit,style,lien)
          for c,v in liste:
            if type(v)==list:
              resDocument+=self.rapport_contenu_mil(fmtCh(self.projet.lexique.entree(c).replace('%s','£s')).replace('£s','%s'),v)
            else:
              resDocument+=self.rapport_contenu_mil(self.fmtLex(c),fmtCh(v))
          resDocument+=self.rapport_contenuMode_fin()
      # Mode niveau 3 à 2
      if len(CTL_MODE)>2 and CTL_MODE[2]=='2':
        clf=params['CLF_ACTION']
        val=params['VAL_ACTION']
        sty=params['STY_ACTION']
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty)
        elif execution=='P':
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty)
      # Mode niveau 3 à 3
      if len(CTL_MODE)>2 and CTL_MODE[2]=='3':
        clfact=fmtCh(params['CLF_ACTION'])
        valact=params['VAL_ACTION']
        styact=params['STY_ACTION']
        clfcst=self.fmtLex(params['CLF_CONSTAT'])
        valcst=fmtCh(params['VAL_CONSTAT'])#,2
        stycst=params['STY_CONSTAT']
        clfatt=[self.fmtLex(x) for x in params['CLF_ATTENDU']]
        valatt=fmtCh(params['VAL_ATTENDU'])#,2
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt)
        elif execution=='P':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt)
      # Mode niveau 4 à 1
      if len(CTL_MODE)>3 and CTL_MODE[3]=='1':
        clef=self.fmtLex(params['CLF_RAPPORT'])
        valeur=params['VAL_RAPPORT'].replace('\n','%ficn%')
        valeur=fmtCh(valeur,tt=True)
        style=params['STY_RAPPORT']
        resDocument+=self.rapport_contenuMode(clef,valeur,[],style)
      # Mode niveau 4 à 3
      if len(CTL_MODE)>4 and CTL_MODE[4]=='3':
        clfact=fmtCh(params['CLF2_ACTION'])
        clfcst=self.fmtLex(params['CLF2_CONSTAT'])
        valcst=[fmtCh(x) for x in params['VAL2_CONSTAT']]#,2)
        stycst=params['STY2_CONSTAT']
        clfatt=[fmtCh(x) for x in params['CLF2_ATTENDU']]
        valatt=[fmtCh(x) for x in params['VAL2_ATTENDU']]#,2)
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL2_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)
        elif execution=='P':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)

    #----------------------------------
    # Remplacement de l'ancre
    # sans objet

    #----------------------------------
    # Fin et export
    if resDocument.find("\\index{general}")!=-1:
      self.indexGeneral+=1
    if resDocument.find("\\index{exigences}")!=-1:
      self.indexExigences+=1

    return resDocument

  #-------------------------------------------------------------------------------
  def EtapeDeTest(self,pEtape,pNum):
    pEtape=self.TexteConversion(pEtape,"").split('|')
    niveau=pEtape[0]
    etape=pEtape[1]
    if etape[-1]!='.':etape+='.'
    libelle=etape
#    libelle=str(pNum)
#    if len(pEtape)==3:
#      libelle=pEtape[2][:-1]

    if self.projet.etapesSeules:
      resDocument="\\includegraphics[scale=0.4]{etape%s.png} %s\\\\\n"%(niveau,libelle)
    else:  
      resDocument="\\begin{tabularx}{16cm}{|X|}\n"
      resDocument+="\\cellcolor{nngecru} \\includegraphics[scale=0.4]{etape%s.png} %s\\\\\n"%(niveau,libelle)
      resDocument+="\\end{tabularx}\\\n\n"
    return resDocument

#-------------------------------------------------------------------------------
# FIN
