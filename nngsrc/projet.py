#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import sys,os
import nngsrc.servicesXML 
import nngsrc.services 
import nngsrc.lexique
import nngsrc.qualite

#-----------------------------------------------------------------------------------
def recuperationVERSION(listRacine):
  versSTR=''
  OBSERV=''
  for xmlSection in listRacine:
    if nngsrc.servicesXML.SRVxml_recupNomNoeud(xmlSection)=="VERSION":
      for xmlTag in xmlSection.childNodes:
        for i in xmlTag.childNodes:
          if i.nodeType==i.TEXT_NODE:
            if xmlTag.nodeName=="EDITION":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="REVISION":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="DATE":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="OBJET":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="OBSERVATION":
              OBSERV=i.nodeValue
      if OBSERV=='':
        OBSERV="-sans objet-"
      versSTR+=OBSERV+"*"
  versSTR=versSTR[:len(versSTR)-1]
  return versSTR

#-----------------------------------------------------------------------------------
def recuperationDESTINATAIRE(listRacine):
  versSTR=''
  for xmlSection in listRacine:
    if xmlSection.nodeName=="DESTINATAIRE":
      for xmlTag in xmlSection.childNodes:
        for i in xmlTag.childNodes:
          if i.nodeType==i.TEXT_NODE:
            if xmlTag.nodeName=="NOM":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="SOCIETE":
              versSTR+=i.nodeValue+"&"
            elif xmlTag.nodeName=="EXEMPLAIRE":
              versSTR+=i.nodeValue+"*"
  versSTR=versSTR[:len(versSTR)-1]
  return versSTR

#-----------------------------------------------------------------------------------
def fichierJoin(chemin,repFiches):
  if chemin:
    f=chemin.replace(repFiches,'').replace('/',',').replace('\\',',')
    if f!='' and f[0]==',': f=f[1:]
    if f.find(',')!=-1:
      f="join("+f+")"
    return f
  else:
    return ''

#-----------------------------------------------------------------------------------
def ouiNon(booleen):
  if booleen: return "oui"
  return "non"

#-----------------------------------------------------------------------------------
def filtreMethode(methode,filtre):
  if filtre is None: return True
  for m in methode:
    if m in filtre: return True
  return False

#-----------------------------------------------------------------------------------
class Projet:

  #---------------------------------------------------------------------------------
  def __init__(self,execution,configxml,repertoires,typePRJsimple,fichier_remarque,versCFG):
    self.projetOK=False
    # self.lexique=Lexique('FR')
    if not configxml:
      print("Le fichier de configuration n'est pas indiqué.")
      return None
    racine=nngsrc.servicesXML.SRVxml_parser(configxml)
    if racine is None:
      print("La racine du fichier de configuration %s est nulle."%configxml)
      return
    self.version=0.0
    if nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)!="CONFIGURATION":
      print("Le fichier de configuration est incomplet (tag CONFIGURATION)")
      return None
    self.strVersion=nngsrc.servicesXML.SRVxml_recupValAttribut(racine,'version')
    trtVersion=self.strVersion.split('.')
    if len(trtVersion)==3:
      trtVersion="%s%s.%s"%(trtVersion[0],trtVersion[1],trtVersion[2])
      self.version=float(trtVersion)-2000.0
    else:
      self.version=float(self.strVersion)
    if self.version<105.23:
      print("Le fichier de configuration du projet est ancien (v%s). Une version plus récente (v%s) existe."%(self.strVersion,versCFG))
      return None
    self.log=None
    self.nivRapport=0
    self.typePRJsimple=typePRJsimple

    self.PLANS=repertoires[0]
    self.SCENARIOS=repertoires[1]
    self.FICHES=repertoires[2]
    self.RAPPORTS=repertoires[3]
    self.PRODUIT_SOUS_TITRE=''
    self.PRODUIT_SOUS_REFERENCE=''
    self.DOCUMENT_METHODE=None
    self.ajoutBaseFT=[]
    self.dicoVariables= {'indefini': []}
    self.lstNoComment=[]
    self.fichierXml=''
    self.fichier_remarque=fichier_remarque
    self.nbRemarque=0
    self.uiTerm=None
    self.FTconfirme=True
    self.ARRET=False
    self.enregistrement=0
    self.nomFicheALaVolee=''
    self.nomRapportALaVolee=''
    self.nomRepertoireALaVolee=''
    self.nomScenarioALaVolee=''
    self.ficheCourante=None
    self.etapesSeules=False
    self.rapportManuel=False
    if execution in ['S','P','C','R','X','K']:
      self.execution=execution
    elif execution=='M':
      self.execution='R'
      self.rapportManuel=True
    elif execution=='E':
      self.execution='S'
      self.etapesSeules=True
    elif execution in ["VP","VC","VS"]:
      self.execution="VP"
    elif execution in ["VR","VM"]:
      self.execution="VR"
    else:
      print(u"Le code du document à générer doit être VP,VR,P,C,R,S ou M et non",execution)
      self.execution=''

    # Gestion du bloc <PRODUIT>
    self.PRODUIT_NOM=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,NOM"))
    self.PRODUIT_SOCIETE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,SOCIETE,NOM"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,SOCIETE,LOGO"))
    self.SOCIETE_LOGO=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.SOCIETE_ADRESSE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,SOCIETE,ADRESSE"))
    self.SOCIETE_COORDONNEES=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,SOCIETE,COORDONNEES"))
    self.SOCIETE_PROPRIETE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,SOCIETE,PROPRIETE"))
    self.PRODUIT_VERSION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,VERSION"))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,REFERENCE")
    self.REFERENCE_IDENTIFIANT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"IDENTIFIANT"))
    self.SEPARATEUR_SUFFIXE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"SEPARATEUR_SUFFIXE"))
    self.SEPARATEUR_CHAMPS=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"SEPARATEUR_CHAMPS"))
    self.NOMBRE_CHAMPS=int(nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"NOMBRE_CHAMPS")))
    self.PRODUIT_TITRE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,TITRE"))
    self.PRODUIT_TYPE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,TYPE"))
    self.PRODUIT_CLIENT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,CLIENT,NOM"))
    self.PRODUIT_CONTRAT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,CLIENT,CONTRAT"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,CLIENT,LOGO"))
    self.CLIENT_LOGO=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,METHODE"))
    self.PRODUIT_METHODE=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,EXIGENCES")
    self.PRODUIT_EXIGENCES=[]
    self.tailleEntete=3
    self.PRODUIT_EXIGENCES_TRACEES=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"TRACEES"),valDefaut="oui")=="oui"
    self.PRODUIT_EXIGENCES_DECLINEES=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"DECLINEES"))=="oui"
    self.PRODUIT_EXIGENCES_POURCENTAGE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"POURCENTAGE"))=="oui"
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"LISTE")
    for noeud in listeNoeuds:
      fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
      self.PRODUIT_EXIGENCES.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRODUIT,FONCTIONNALITES")
    self.PRODUIT_FONCTIONNALITES=[]
    self.PRODUIT_FONCTIONNALITES_TRACEES=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"TRACEES"),valDefaut="oui")=="oui"
    self.PRODUIT_FONCTIONNALITES_DECLINEES=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"DECLINEES"))=="oui"
    self.PRODUIT_FONCTIONNALITES_POURCENTAGE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(noeud,"POURCENTAGE"))=="oui"
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"LISTE")
    for noeud in listeNoeuds:
      fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
      self.PRODUIT_FONCTIONNALITES.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))

    # Gestion du bloc <DOCUMENT>
    self.DOCUMENT_LANGUE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,LANGUE"))
    self.lexique=nngsrc.lexique.Lexique(self.DOCUMENT_LANGUE)
    self.DOCUMENT_FILIGRANE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,FILIGRANE"))
    self.DOCUMENT_EDITION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,EDITION"))
    self.DOCUMENT_REVISION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REVISION"))
    self.DOCUMENT_ITERATION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,ITERATION"))
    self.filtreMethode=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,METHODE"))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERSIONS")
    listVersionsXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"VERSION")
    self.DOCUMENT_VERSION=recuperationVERSION(listVersionsXML)
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,DESTINATAIRES")
    listDestinatairesXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"DESTINATAIRE")
    self.DOCUMENT_DIFFUSION=recuperationDESTINATAIRE(listDestinatairesXML)
    self.DOCUMENT_REDIGE_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION,NOM"))
    self.DOCUMENT_REDIGE_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION,SOCIETE"))
    self.DOCUMENT_REDIGE_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION,SIGNATURE"))
    self.DOCUMENT_REDIGE_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.DOCUMENT_REDIGE2_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION_2,NOM"))
    self.DOCUMENT_REDIGE2_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION_2,SOCIETE"))
    self.DOCUMENT_REDIGE2_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION_2,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REDACTION_2,SIGNATURE"))
    self.DOCUMENT_REDIGE2_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.DOCUMENT_VALIDE_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VALIDATION,NOM"))
    self.DOCUMENT_VALIDE_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VALIDATION,SOCIETE"))
    self.DOCUMENT_VALIDE_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VALIDATION,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VALIDATION,SIGNATURE"))
    self.DOCUMENT_VALIDE_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.DOCUMENT_VERIFIE_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION,NOM"))
    self.DOCUMENT_VERIFIE_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION,SOCIETE"))
    self.DOCUMENT_VERIFIE_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION,SIGNATURE"))
    self.DOCUMENT_VERIFIE_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.DOCUMENT_VERIFIE2_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION_2,NOM"))
    self.DOCUMENT_VERIFIE2_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION_2,SOCIETE"))
    self.DOCUMENT_VERIFIE2_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION_2,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,VERIFICATION_2,SIGNATURE"))
    self.DOCUMENT_VERIFIE2_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    self.DOCUMENT_REALISE_PAR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REALISATION,NOM"))
    self.DOCUMENT_REALISE_ORG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REALISATION,SOCIETE"))
    self.DOCUMENT_REALISE_DATE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REALISATION,DATE"))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DOCUMENT,REALISATION,SIGNATURE"))
    self.DOCUMENT_REALISE_SIGN=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))

    # Gestion du bloc <RAPPORT>
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION,OBJET"),None)
    self.DOCUMENT_OBJET=None
    if fichier:
      self.DOCUMENT_OBJET=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION")
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"DOC_APPLICABLE")
    self.DOC_APPLICABLE=[]
    for noeud in listeNoeuds:
       fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
       self.DOC_APPLICABLE.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    self.PLAN_APPLICABLE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION,PLAN_APPLICABLE"))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION")
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"DOC_REFERENCE")
    self.DOC_REFERENCE=[]
    for noeud in listeNoeuds:
       fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
       self.DOC_REFERENCE.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION")
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"SIGLES")
    self.SIGLES=[]
    for noeud in listeNoeuds:
       fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
       self.SIGLES.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION")
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"GLOSSAIRE")
    self.GLOSSAIRE=[]
    for noeud in listeNoeuds:
       fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
       self.GLOSSAIRE.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,INTRODUCTION")
    listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"NOTATIONS")
    self.NOTATIONS=[]
    for noeud in listeNoeuds:
       fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
       self.NOTATIONS.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,PROLOGUE,CHAPITRE"),None)
    self.PROLOGUE=''
    self.PROLOGUE_FICHIERS=[]
    if fichier:
      self.PROLOGUE=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
      noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,PROLOGUE")
      listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"FICHIER")
      for noeud in listeNoeuds:
         fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
         self.PROLOGUE_FICHIERS.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,EPILOGUE,CHAPITRE"),None)
    self.EPILOGUE=''
    self.EPILOGUE_FICHIERS=[]
    if fichier:
      self.EPILOGUE=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
      noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,EPILOGUE")
      listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,"FICHIER")
      for noeud in listeNoeuds:
         fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud)
         self.EPILOGUE_FICHIERS.append(os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier)))
    self.RAPPORT_EN_LIGNE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,EN_LIGNE"),"non")
    self.RAPPORT_TEX=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,LATEX"))
    self.ARCHIVAGE=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RAPPORT,ARCHIVAGE"))=="oui"

    # Gestion du bloc <AVERTISSEMENTS>
    self.SON_ACTIVATION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,SON,MUET"))=="non"
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,SON,SON_OK"))
    self.SON_FICHIER_OK=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,SON,SON_NOK"))
    self.SON_FICHIER_NOK=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,SON,SON_KO"))
    self.SON_FICHIER_NOK=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,SON,SON_SAISIE"))
    self.SON_FICHIER_ALARME=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))

    self.MAIL_ACTIVATION=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,ENVOI"))=="oui"
    self.MAIL_SMTP=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,SMTP"))
    self.MAIL_EXPEDITEUR=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,EXPEDITEUR"))
    self.MAIL_DESTINATAIRE1=[None,True]
    self.MAIL_DESTINATAIRE1[0]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_1"))
    self.MAIL_DESTINATAIRE1[1]=nngsrc.servicesXML.SRVxml_recupValAttribut(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_1"),'cond')=="nok"
    self.MAIL_DESTINATAIRE2=[None,True]
    self.MAIL_DESTINATAIRE2[0]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_2"))
    self.MAIL_DESTINATAIRE2[1]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_2"),'cond')=="nok"
    self.MAIL_DESTINATAIRE3=[None,True]
    self.MAIL_DESTINATAIRE3[0]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_3"))
    self.MAIL_DESTINATAIRE3[1]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,DESTINATAIRE_3"),'cond')=="nok"
    self.MAIL_QUALITE=[None,True]
    self.MAIL_QUALITE[0]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,QUALITE"))
    self.MAIL_QUALITE[1]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"AVERTISSEMENTS,MAIL,QUALITE"))

    # Gestion du bloc <ANOMALIES>
    self.REF_FT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,REF_FT"))
    nivArretFT=self.lexique.niveauCriticite(nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,ARRET_FT")))

    self.ACTIVATION_BASEFT=nngsrc.servicesXML.SRVxml_recupValAttribut(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,BASE_FT"),'activation')=="oui"
    fichier=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,BASE_FT"))
    self.PRODUIT_BASEFT=os.path.join(self.FICHES,nngsrc.services.SRV_TraiterJoin(fichier))

    self.CONFIRMATION_FT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,CONFIRMATION_FT"))
    if self.PRODUIT_FONCTIONNALITES_TRACEES:
      self.MATTRAC_CTL_FCT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,CTL_FCT"))=="oui"
      self.MATTRAC_FCT_CTL=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,FCT_CTL"))=="oui"
    else:
      self.MATTRAC_CTL_FCT=False
      self.MATTRAC_FCT_CTL=False
    if self.PRODUIT_EXIGENCES_TRACEES:
      self.MATTRAC_CTL_EXG=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,CTL_EXG"))=="oui"
      self.MATTRAC_EXG_CTL=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,EXG_CTL"))=="oui"
    else:
      self.MATTRAC_CTL_EXG=False
      self.MATTRAC_EXG_CTL=False
    self.MATTRAC_NON_CVT=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,NON_CVT"))=="oui"
    self.MATTRAC_FORMAT_COLONNE=int(nngsrc.servicesXML.SRVxml_recupValAttribut(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MATRICES,NON_CVT"),'col',1))
    self.MANTIS=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ANOMALIES,MANTIS"),None)

    # Gestion du bloc <DEBUG>
    self.debug=int(nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DEBUG,NIVEAU"),0))
    self.PasAPas=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DEBUG,PAS_A_PAS"),None)
    if self.PasAPas=="": self.PasAPas=None
    if self.PasAPas is not None:
      self.PasAPas=os.path.join(self.FICHES, nngsrc.services.SRV_TraiterJoin(self.PasAPas))
    self.LOG_COMPLET=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DEBUG,LOG_COMPLET"))=="oui"

    if sys.platform=="win32" or sys.platform=="mingw32":
      self.PRODUIT_NOM=self.PRODUIT_NOM.replace("\"",'')
      self.PRODUIT_TITRE=self.PRODUIT_TITRE.replace("\"",'')
      self.PRODUIT_CLIENT=self.PRODUIT_CLIENT.replace("\"",'')
      self.PRODUIT_CONTRAT=self.PRODUIT_CONTRAT.replace("\"",'')

      self.VALIDATION_REFERENCE=self.VALIDATION_REFERENCE.replace("\"",'')

      self.DOCUMENT_REDIGE_PAR=self.DOCUMENT_REDIGE_PAR.replace("\"",'')
      self.DOCUMENT_REDIGE2_PAR=self.DOCUMENT_REDIGE2_PAR.replace("\"",'')
      self.DOCUMENT_VERIFIE_PAR=self.DOCUMENT_VERIFIE_PAR.replace("\"",'')
      self.DOCUMENT_VERIFIE2_PAR=self.DOCUMENT_VERIFIE2_PAR.replace("\"",'')
      self.DOCUMENT_VALIDE_PAR=self.DOCUMENT_VALIDE_PAR.replace("\"",'')
      self.DOCUMENT_REALISE_PAR=self.DOCUMENT_REALISE_PAR.replace("\"",'')
      self.DOCUMENT_REDIGE_ORG=self.DOCUMENT_REDIGE_ORG.replace("\"",'')
      self.DOCUMENT_REDIGE2_ORG=self.DOCUMENT_REDIGE2_ORG.replace("\"",'')
      self.DOCUMENT_VERIFIE_ORG=self.DOCUMENT_VERIFIE_ORG.replace("\"",'')
      self.DOCUMENT_VERIFIE2_ORG=self.DOCUMENT_VERIFIE2_ORG.replace("\"",'')
      self.DOCUMENT_VALIDE_ORG=self.DOCUMENT_VALIDE_ORG.replace("\"",'')
      self.DOCUMENT_REALISE_ORG=self.DOCUMENT_REALISE_ORG.replace("\"",'')
      self.DOCUMENT_REDIGE_SIGN=self.DOCUMENT_REDIGE_SIGN.replace("\"",'')
      self.DOCUMENT_REDIGE2_SIGN=self.DOCUMENT_REDIGE2_SIGN.replace("\"",'')
      self.DOCUMENT_VERIFIE_SIGN=self.DOCUMENT_VERIFIE_SIGN.replace("\"",'')
      self.DOCUMENT_VERIFIE2_SIGN=self.DOCUMENT_VERIFIE2_SIGN.replace("\"",'')
      self.DOCUMENT_VALIDE_SIGN=self.DOCUMENT_VALIDE_SIGN.replace("\"",'')
      self.DOCUMENT_REALISE_SIGN=self.DOCUMENT_REALISE_SIGN.replace("\"",'')
      self.DOCUMENT_DIFFUSION=self.DOCUMENT_DIFFUSION.replace("\"",'')

      self.RAPPORT_TEX=self.RAPPORT_TEX.replace("\"",'')

      self.COMPUTERNAME=os.getenv('COMPUTERNAME','ND')
      self.USERNAME=os.getenv('USERNAME','ND')
    else:
      self.COMPUTERNAME=os.uname()[1]
      self.USERNAME=os.getenv('LOGNAME','ND')

    if self.execution in ['P','S']:
      self.COMPUTERNAME=''
      self.USERNAME=''
    elif self.execution=='C':
      self.COMPUTERNAME=u"________________"
      self.USERNAME=u"________________"

    # gestion de la methode
    methodeDefaut=''
    self.methode={}
    if self.PRODUIT_METHODE is not None and os.access(self.PRODUIT_METHODE,os.F_OK):
      methode=nngsrc.services.SRV_LireContenuListe(self.PRODUIT_METHODE)
      for item in methode:
        abbrev=item.split(' ')[1]
        terme=' '.join(item.split(' : ')[0].split(' ')[2:])
        description=item.split(' : ')[1]
        self.methode[abbrev]={"terme":terme,"description":description}
        if item.split(' ')[0]==">": methodeDefaut=abbrev

    # gestion des exigences
    self.exigencesNap={}
    self.exigences={}
    self.ficExigences=None
    if self.version<9.0:
      if os.access(self.PRODUIT_EXIGENCES[0],os.F_OK):
        self.ficExigences={"-":[None,None,self.MATTRAC_FORMAT_COLONNE]}
        ExigencesLues=nngsrc.services.SRV_LireContenuListe(self.PRODUIT_EXIGENCES[0])
        for e in range(len(ExigencesLues)):
          exigenceLue=ExigencesLues[e]
          if exigenceLue=='':continue
          if exigenceLue[0]!=';':
            exigenceLue=exigenceLue.split(':')
            libelle=exigenceLue[0]
            if len(exigenceLue)==1:
              methode=methodeDefaut
            else:
              methode=exigenceLue[1]
            self.exigences[libelle]={'ID':"-",'applicable':True,'methode':methode,'parent':None}
    else:
      # exigences : dictionnaire de libellés
      # 'libellés': {ID,applicable,methode,parent[reference,applicable,methode]}
      # ficExigences : dictionnaire d'ID
      # 'ID' : ['TYPE_EXIGENCES','COMMENTAIRE','NOMBRE_COLONNE']
      self.ficExigences={}
      for fichier in self.PRODUIT_EXIGENCES:
        if not os.path.exists(fichier):
          nngsrc.services.SRV_Terminal(self.uiTerm,self.debug,None,"Fichier '%s' inexistant."%fichier,style="Er")
          continue
        ExigencesLues=nngsrc.services.SRV_LireContenuListe(fichier,';')
        ID=ExigencesLues[0].split(' ')[1]
        self.ficExigences[ID]=[' '.join(ExigencesLues[1].split(' ')[1:]),' '.join(ExigencesLues[2].split(' ')[1:]),int(ExigencesLues[3].split(' ')[1])]
        for e in range(4,len(ExigencesLues)):
          exigenceLue=ExigencesLues[e]
          if exigenceLue=='':continue
          if exigenceLue[0]!=';':
            exigenceLue=exigenceLue.split('|')
            libelle=exigenceLue[0]
            applicable=exigenceLue[1]=='Ap'
            if not applicable:
              if ID not in self.exigencesNap: self.exigencesNap[ID]=[]
              if len(exigenceLue)==3:
                parent=None
              else:
                methParent=exigenceLue[5]
                if methParent=='':
                  methParent=methodeDefaut
                parent=[exigenceLue[3],exigenceLue[4],methParent]
              self.exigencesNap[ID].append((libelle,parent))
            if True:
            #else:
              methode=exigenceLue[2].split(',')
              if methode==['']:
                methode=[methodeDefaut]
              if len(exigenceLue)==3:
                parent=None
              else:
                methParent=exigenceLue[5]
                if methParent=='':
                  methParent=methodeDefaut
                parent=[exigenceLue[3],exigenceLue[4],methParent]
              if filtreMethode(methode,self.filtreMethode) or execution=='K':
                self.exigences[libelle]={'ID':ID,'applicable':applicable,'methode':methode,'parent':parent}

    # gestion des fonctionnalités
    self.fonctionnalitesNap={}
    self.ficFonctionnalites=None
    if self.version<9.0:
      if os.access(self.PRODUIT_FONCTIONNALITES[0],os.F_OK):
        self.ficFonctionnalites={"-":[None,None,self.MATTRAC_FORMAT_COLONNE]}
        FonctionnalitesLues=nngsrc.services.SRV_LireContenuListe(self.PRODUIT_FONCTIONNALITES[0])
        self.fonctionnalites={}
        for e in range(len(FonctionnalitesLues)):
          fonctionnalitesLue=FonctionnalitesLues[e]
          if fonctionnalitesLue=='':continue
          if fonctionnalitesLue[0]!=';':
            fonctionnalitesLue=fonctionnalitesLue.split('|')
            libelle=fonctionnalitesLue[0]
            if len(fonctionnalitesLue)==1:
              methode=methodeDefaut
            else:
              methode=fonctionnalitesLue[1]
            self.fonctionnalites[libelle]={'ID':"-",'applicable':True,'methode':methode,'parent':None}
    else:
      self.fonctionnalites={}
      self.ficFonctionnalites={}
      for fichier in self.PRODUIT_FONCTIONNALITES:
        FonctionnalitesLues=nngsrc.services.SRV_LireContenuListe(fichier)
        ID=FonctionnalitesLues[0].split(' ')[1]
        self.ficFonctionnalites[ID]=[' '.join(FonctionnalitesLues[1].split(' ')[1:]),' '.join(FonctionnalitesLues[2].split(' ')[1:]),int(FonctionnalitesLues[3].split(' ')[1])]
        for e in range(4,len(FonctionnalitesLues)):
          fonctionnalitesLue=FonctionnalitesLues[e]
          if fonctionnalitesLue=='':continue
          if fonctionnalitesLue[0]!=';':
            fonctionnalitesLue=fonctionnalitesLue.split('|')
            libelle=fonctionnalitesLue[0]
            applicable=fonctionnalitesLue[1]=='Ap'
            if not applicable:
              if ID not in self.fonctionnalitesNap: self.fonctionnalitesNap[ID]=[]
              if len(fonctionnalitesLue)==3:
                parent=None
              else:
                methParent=fonctionnalitesLue[5]
                if methParent=='':
                  methParent=methodeDefaut
                parent=[fonctionnalitesLue[3],fonctionnalitesLue[4],methParent]
              self.fonctionnalitesNap[ID].append((libelle,parent))
            else:
              methode=fonctionnalitesLue[2].split(',')
              if len(fonctionnalitesLue)==3:
                parent=None
              else:
                methParent=fonctionnalitesLue[5]
                if methParent=='':
                  methParent=methodeDefaut
                parent=[fonctionnalitesLue[3],fonctionnalitesLue[4],methParent]
              if filtreMethode(methode,self.filtreMethode):
                self.fonctionnalites[libelle]={'ID':ID,'applicable':applicable,'methode':methode,'parent':parent}

    # gestion des arrets sur FT :
    #  - BLOQUAGE : Arret de la fiche après un non OK
    #  - ARRET_FT : Arret de la validation après un non OK
    # par défaut et ARRET_FT=4
    self.ARRET_FT=[4]
    self.BLOQUAGE=[3,4]

    if nivArretFT==3:
      self.ARRET_FT=[3,4]
      self.BLOQUAGE=self.ARRET_FT
    elif nivArretFT==2:
      self.ARRET_FT=[2,3,4]
      self.BLOQUAGE=self.ARRET_FT
    elif nivArretFT==1:
      self.ARRET_FT=[1,2,3,4]
      self.BLOQUAGE=self.ARRET_FT
    elif nivArretFT==0:
      self.ARRET_FT=[0,1,2,3,4]
      self.BLOQUAGE=self.ARRET_FT

    self.GenereProduitReference()

    if self.execution[0]=="V":
      nngsrc.services.SRV_Terminal(self.uiTerm,self.debug,None,'')
      nngsrc.services.SRV_Terminal(self.uiTerm,self.debug,None,self.lexique.entree('LOCUT_DEMANDE_SOUS_XXX')%self.PRODUIT_REFERENCE,style="W<br>")
      self.PRODUIT_SOUS_TITRE=nngsrc.services.SRV_AttenteOperateur(self.uiTerm,self.debug,self.lexique.entree('LOCUT_DEMANDE_SOUS_TITRE'),"SAISIE")
      self.PRODUIT_SOUS_REFERENCE='-'+nngsrc.services.SRV_AttenteOperateur(self.uiTerm,self.debug,self.lexique.entree('LOCUT_DEMANDE_SOUS_REFERENCE'),"SAISIE")
      self.PRODUIT_SOUS_REFERENCE.replace(' ','')
    if self.debug==2:
      nngsrc.services.SRV_Terminal(self.uiTerm,self.debug,None,"Projet correctement chargé\n")
    self.projetOK=True

  #---------------------------------------------------------------------------------
  def GenereProduitReference(self):
    if self.version<9.0:
      if self.VALIDATION_IDENTIFIANT=="prefixe":
        if self.execution[0]=="V":
          self.PRODUIT_REFERENCE="V-"+self.VALIDATION_REFERENCE
        else:
          self.PRODUIT_REFERENCE="%s-"%self.execution+self.VALIDATION_REFERENCE
      elif self.VALIDATION_IDENTIFIANT=="suffixe":
        if self.execution[0]=="V":
          self.PRODUIT_REFERENCE=self.VALIDATION_REFERENCE+"-V"
        else:
          self.PRODUIT_REFERENCE=self.VALIDATION_REFERENCE+"-%s"%self.execution
      elif self.VALIDATION_REFERENCE.find("%s")!=-1:
        if self.execution[0]=="V":
          self.PRODUIT_REFERENCE=self.VALIDATION_REFERENCE%"V"
        else:
          self.PRODUIT_REFERENCE=self.VALIDATION_REFERENCE%self.execution
      else:
        self.PRODUIT_REFERENCE=self.VALIDATION_REFERENCE
    else: # version 9.0
      if self.execution in ['R','C']:
        self.PRODUIT_REFERENCE="%s%s{EDITION}%s{REVISION}%s{ITERATION}"%(self.REFERENCE_IDENTIFIANT,self.SEPARATEUR_CHAMPS,self.SEPARATEUR_CHAMPS,self.SEPARATEUR_CHAMPS)
      else:
        self.PRODUIT_REFERENCE="%s%s{EDITION}%s{REVISION}"%(self.REFERENCE_IDENTIFIANT,self.SEPARATEUR_CHAMPS,self.SEPARATEUR_CHAMPS)
      if self.rapportManuel:
        self.PRODUIT_REFERENCE=self.PRODUIT_REFERENCE.replace('{MODE}','M')
      else:
        self.PRODUIT_REFERENCE=self.PRODUIT_REFERENCE.replace('{MODE}',self.execution)

  #---------------------------------------------------------------------------------
  def GenereReference(self):
    if self.version<9.0:
      if self.PRODUIT_REFERENCE!='':
        if self.execution in ['P','S']:
          reference=self.PRODUIT_REFERENCE+self.PRODUIT_SOUS_REFERENCE+'-'+self.DOCUMENT_EDITION+'-'+self.DOCUMENT_REVISION
          referenceIter=self.PRODUIT_REFERENCE+self.PRODUIT_SOUS_REFERENCE+'-'+self.DOCUMENT_EDITION+'-'+self.DOCUMENT_REVISION
        else:
#          if self.CAMPAGNE==:
            reference=self.PRODUIT_REFERENCE+"-"+self.PRODUIT_VERSION.replace('.','')+self.PRODUIT_SOUS_REFERENCE+'-'+self.DOCUMENT_EDITION+'-'+self.DOCUMENT_REVISION
            referenceIter=self.PRODUIT_REFERENCE+"-"+self.PRODUIT_VERSION.replace('.','')+self.PRODUIT_SOUS_REFERENCE+'-'+self.DOCUMENT_EDITION+'-'+self.DOCUMENT_REVISION+'-%04d'%int(self.DOCUMENT_ITERATION)
#          else:
#            reference=self.PRODUIT_REFERENCE+self.PRODUIT_SOUS_REFERENCE+'-'+self.DOCUMENT_EDITION+'-'+self.DOCUMENT_REVISION+'-%04d'%int(self.DOCUMENT_ITERATION)
      else:
        reference="REF"
        referenceIter="REF"
    else:
      reference=self.PRODUIT_REFERENCE
      reference=reference.replace("{SOUSREF}",self.PRODUIT_SOUS_REFERENCE).replace("{EDITION}",self.DOCUMENT_EDITION).replace("{REVISION}",self.DOCUMENT_REVISION)
      if self.execution in ['P','S']:
        reference=reference.replace("%s{VERSION}"%self.SEPARATEUR_CHAMPS,'')
        referenceIter=reference
      else:
        reference=reference.replace("{VERSION}",self.PRODUIT_VERSION.replace('.',''))
        referenceIter=reference.replace("{ITERATION}",'%04d'%int(self.DOCUMENT_ITERATION))
        reference=reference.replace("%s{ITERATION}"%self.SEPARATEUR_CHAMPS,'')
    return reference,referenceIter

  #---------------------------------------------------------------------------------
  def Sauvegarde(self,configxml,repFiches):
    fXml=open(configxml,"w",encoding='utf-8')
    fXml.write('<?xml version="1.0" encoding="utf-8"?>\n')
    fXml.write('<CONFIGURATION version="%s">\n'%self.strVersion)
    fXml.write('  <PRODUIT>\n')
    fXml.write('    <NOM>%s</NOM>\n'%self.PRODUIT_NOM)
    fXml.write('    <VERSION>%s</VERSION>\n'%self.PRODUIT_VERSION)
    fXml.write('    <REFERENCE>\n')
    fXml.write('      <IDENTIFIANT>%s</IDENTIFIANT>\n'%self.REFERENCE_IDENTIFIANT)
    fXml.write('      <SEPARATEUR_SUFFIXE>%s</SEPARATEUR_SUFFIXE>\n'%self.SEPARATEUR_SUFFIXE)
    fXml.write('      <SEPARATEUR_CHAMPS>%s</SEPARATEUR_CHAMPS>\n'%self.SEPARATEUR_CHAMPS)
    fXml.write('      <NOMBRE_CHAMPS>%d</NOMBRE_CHAMPS>\n'%self.NOMBRE_CHAMPS)
    fXml.write('    </REFERENCE>\n')
    fXml.write('    <TITRE>%s</TITRE>\n'%self.PRODUIT_TITRE)
    fXml.write('    <TYPE>%s</TYPE>\n'%self.PRODUIT_TYPE)
    fXml.write('    <METHODE>%s</METHODE>\n'%fichierJoin(self.PRODUIT_METHODE,repFiches))
    fXml.write('    <CLIENT>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.PRODUIT_CLIENT)
    fXml.write('      <LOGO>%s</LOGO>\n'%fichierJoin(self.CLIENT_LOGO,repFiches))
    fXml.write('      <CONTRAT>%s</CONTRAT>\n'%self.PRODUIT_CONTRAT)
    fXml.write('    </CLIENT>\n')
    fXml.write('    <SOCIETE>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.PRODUIT_SOCIETE)
    fXml.write('      <LOGO>%s</LOGO>\n'%fichierJoin(self.SOCIETE_LOGO,repFiches))
    fXml.write('      <ADRESSE>%s</ADRESSE>\n'%self.SOCIETE_ADRESSE)
    fXml.write('      <COORDONNEES>%s</COORDONNEES>\n'%self.SOCIETE_COORDONNEES)
    fXml.write('      <PROPRIETE>%s</PROPRIETE>\n'%self.SOCIETE_PROPRIETE)
    fXml.write('    </SOCIETE>\n')
    fXml.write('    <EXIGENCES>\n')
    fXml.write('      <TRACEES>%s</TRACEES>\n'%ouiNon(self.PRODUIT_EXIGENCES_TRACEES))
    fXml.write('      <DECLINEES>%s</DECLINEES>\n'%ouiNon(self.PRODUIT_EXIGENCES_DECLINEES))
    for fichier in self.PRODUIT_EXIGENCES:
      fXml.write('      <LISTE>%s</LISTE>\n'%fichierJoin(fichier,repFiches))
    fXml.write('      <POURCENTAGE>%s</POURCENTAGE>\n'%ouiNon(self.PRODUIT_EXIGENCES_POURCENTAGE))
    fXml.write('    </EXIGENCES>\n')
    fXml.write('    <FONCTIONNALITES>\n')
    fXml.write('      <TRACEES>%s</TRACEES>\n'%ouiNon(self.PRODUIT_FONCTIONNALITES_TRACEES))
    fXml.write('      <DECLINEES>%s</DECLINEES>\n'%ouiNon(self.PRODUIT_FONCTIONNALITES_DECLINEES))
    for fichier in self.PRODUIT_FONCTIONNALITES:
      fXml.write('      <LISTE>%s</LISTE>\n'%fichierJoin(fichier,repFiches))
    fXml.write('      <POURCENTAGE>%s</POURCENTAGE>\n'%ouiNon(self.PRODUIT_FONCTIONNALITES_POURCENTAGE))
    fXml.write('    </FONCTIONNALITES>\n')
    fXml.write('  </PRODUIT>\n')
    fXml.write('  <DOCUMENT>\n')
    fXml.write('    <EDITION>%s</EDITION>\n'%self.DOCUMENT_EDITION)
    fXml.write('    <REVISION>%s</REVISION>\n'%self.DOCUMENT_REVISION)
    fXml.write('    <ITERATION>%d</ITERATION>\n'%(int(self.DOCUMENT_ITERATION)+1))
    fXml.write('    <METHODE>%s</METHODE>\n'%self.filtreMethode)
    fXml.write('    <VERSIONS>\n')
    versions=self.DOCUMENT_VERSION.split('*')
    for version in versions:
      version=version.split('&')
      if version!=['']:
        fXml.write('      <VERSION>\n')
        fXml.write('        <EDITION>%s</EDITION>\n'%version[0])
        fXml.write('        <REVISION>%s</REVISION>\n'%version[1])
        fXml.write('        <DATE>%s</DATE>\n'%version[2])
        fXml.write('        <OBJET>%s</OBJET>\n'%version[3])
        fXml.write('        <OBSERVATION>%s</OBSERVATION>\n'%version[4])
        fXml.write('      </VERSION>\n')
    fXml.write('    </VERSIONS>\n')
    fXml.write('    <REDACTION>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_REDIGE_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_REDIGE_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_REDIGE_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_REDIGE_SIGN,repFiches))
    fXml.write('    </REDACTION>\n')
    fXml.write('    <REDACTION_2>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_REDIGE2_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_REDIGE2_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_REDIGE2_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_REDIGE2_SIGN,repFiches))
    fXml.write('    </REDACTION_2>\n')
    fXml.write('    <VERIFICATION>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_VERIFIE_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_VERIFIE_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_VERIFIE_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_VERIFIE_SIGN,repFiches))
    fXml.write('    </VERIFICATION>\n')
    fXml.write('    <VERIFICATION_2>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_VERIFIE2_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_VERIFIE2_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_VERIFIE2_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_VERIFIE2_SIGN,repFiches))
    fXml.write('    </VERIFICATION_2>\n')
    fXml.write('    <VALIDATION>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_VALIDE_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_VALIDE_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_VALIDE_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_VALIDE_SIGN,repFiches))
    fXml.write('    </VALIDATION>\n')
    fXml.write('    <REALISATION>\n')
    fXml.write('      <NOM>%s</NOM>\n'%self.DOCUMENT_REALISE_PAR)
    fXml.write('      <SOCIETE>%s</SOCIETE>\n'%self.DOCUMENT_REALISE_ORG)
    fXml.write('      <DATE>%s</DATE>\n'%self.DOCUMENT_REALISE_DATE)
    fXml.write('      <SIGNATURE>%s</SIGNATURE>\n'%fichierJoin(self.DOCUMENT_REALISE_SIGN,repFiches))
    fXml.write('    </REALISATION>\n')
    fXml.write('    <DESTINATAIRES>\n')
    destinataires=self.DOCUMENT_DIFFUSION.split('*')
    if destinataires!=['']:
     for destinataire in destinataires:
      destinataire=destinataire.split('&')
      fXml.write('      <DESTINATAIRE>\n')
      fXml.write('        <NOM>%s</NOM>\n'%destinataire[0])
      fXml.write('        <SOCIETE>%s</SOCIETE>\n'%destinataire[1])
      fXml.write('        <EXEMPLAIRE>%s</EXEMPLAIRE>\n'%destinataire[2])
      fXml.write('      </DESTINATAIRE>\n')
    fXml.write('    </DESTINATAIRES>\n')
    fXml.write('    <LANGUE>%s</LANGUE>\n'%self.DOCUMENT_LANGUE)
    fXml.write('    <FILIGRANE>%s</FILIGRANE>\n'%self.DOCUMENT_FILIGRANE)
    fXml.write('  </DOCUMENT>\n')
    fXml.write('  <RAPPORT>\n')
    fXml.write('    <INTRODUCTION>\n')
    fXml.write('      <OBJET>%s</OBJET>\n'%fichierJoin(self.DOCUMENT_OBJET,repFiches))
    for f in self.DOC_APPLICABLE:
      fXml.write('      <DOC_APPLICABLE>%s</DOC_APPLICABLE>\n'%fichierJoin(f,repFiches))
    fXml.write('      <PLAN_APPLICABLE>%s</PLAN_APPLICABLE>\n'%self.PLAN_APPLICABLE)
    for f in self.DOC_REFERENCE:
      fXml.write('      <DOC_REFERENCE>%s</DOC_REFERENCE>\n'%fichierJoin(f,repFiches))
    for f in self.SIGLES:
      fXml.write('      <SIGLES>%s</SIGLES>\n'%fichierJoin(f,repFiches))
    for f in self.GLOSSAIRE:
      fXml.write('      <GLOSSAIRE>%s</GLOSSAIRE>\n'%fichierJoin(f,repFiches))
    for f in self.NOTATIONS:
      fXml.write('      <NOTATIONS>%s</NOTATIONS>\n'%fichierJoin(f,repFiches))
    fXml.write('    </INTRODUCTION>\n')
    fXml.write('    <PROLOGUE>\n')
    fXml.write('      <CHAPITRE>%s</CHAPITRE>\n'%fichierJoin(self.PROLOGUE,repFiches))
    for f in self.PROLOGUE_FICHIERS:
      fXml.write('      <FICHIER>%s</FICHIER>\n'%fichierJoin(f,repFiches))
    fXml.write('    </PROLOGUE>\n')
    fXml.write('    <EPILOGUE>\n')
    fXml.write('      <CHAPITRE>%s</CHAPITRE>\n'%fichierJoin(self.EPILOGUE,repFiches))
    for f in self.EPILOGUE_FICHIERS:
      fXml.write('      <FICHIER>%s</FICHIER>\n'%fichierJoin(f,repFiches))
    fXml.write('    </EPILOGUE>\n')
    if self.version>=9.0:
      fXml.write('    <EN_LIGNE>%s</EN_LIGNE>\n'%self.RAPPORT_EN_LIGNE)
    fXml.write('    <LATEX>%s</LATEX>\n'%self.RAPPORT_TEX)
    fXml.write('    <ARCHIVAGE>%s</ARCHIVAGE>\n'%ouiNon(self.ARCHIVAGE))
    fXml.write('  </RAPPORT>\n')
    fXml.write('  <AVERTISSEMENTS>\n')
    fXml.write('    <SON>\n')
    fXml.write('      <MUET>%s</MUET>\n'%ouiNon(not self.SON_ACTIVATION))
    fXml.write('      <SON_OK>%s</SON_OK>\n'%fichierJoin(self.SON_FICHIER_OK,repFiches))
    fXml.write('      <SON_KO>%s</SON_KO>\n'%fichierJoin(self.SON_FICHIER_NOK,repFiches))
    fXml.write('    </SON>\n')
    fXml.write('    <MAIL>\n')
    fXml.write('      <ENVOI>%s</ENVOI>\n'%ouiNon(self.MAIL_ACTIVATION))
    fXml.write('      <SMTP>%s</SMTP>\n'%self.MAIL_SMTP)
    fXml.write('      <EXPEDITEUR>%s</EXPEDITEUR>\n'%self.MAIL_EXPEDITEUR)
    if self.MAIL_DESTINATAIRE1[0]:
      if self.MAIL_DESTINATAIRE1[1]:
        fXml.write('      <DESTINATAIRE_1>%s</DESTINATAIRE_1>\n'%self.MAIL_DESTINATAIRE1[0])
      else:
        fXml.write('      <DESTINATAIRE_1 cond="nok">%s</DESTINATAIRE_1>\n'%self.MAIL_DESTINATAIRE1[0])
    else:
      fXml.write('      <DESTINATAIRE_1></DESTINATAIRE_1>\n')

    if self.MAIL_DESTINATAIRE2[0]:
      if self.MAIL_DESTINATAIRE2[1]:
        fXml.write('      <DESTINATAIRE_2>%s</DESTINATAIRE_2>\n'%self.MAIL_DESTINATAIRE2[0])
      else:
        fXml.write('      <DESTINATAIRE_2 cond="nok">%s</DESTINATAIRE_2>\n'%self.MAIL_DESTINATAIRE2[0])
    else:
      fXml.write('      <DESTINATAIRE_2></DESTINATAIRE_2>\n')

    if self.MAIL_DESTINATAIRE3[0]:
      if self.MAIL_DESTINATAIRE3[1]:
        fXml.write('      <DESTINATAIRE_3>%s</DESTINATAIRE_3>\n'%self.MAIL_DESTINATAIRE3[0])
      else:
        fXml.write('      <DESTINATAIRE_3 cond="nok">%s</DESTINATAIRE_3>\n'%self.MAIL_DESTINATAIRE3[0])
    else:
      fXml.write('      <DESTINATAIRE_3></DESTINATAIRE_3>\n')

    if self.MAIL_QUALITE[0]:
      if self.MAIL_QUALITE[1]:
        fXml.write('      <QUALITE>%s</QUALITE>\n'%self.MAIL_QUALITE[0])
      else:
        fXml.write('      <QUALITE cond="nok">%s</QUALITE>\n'%self.MAIL_QUALITE[0])
    else:
      fXml.write('      <QUALITE></QUALITE>\n')

    fXml.write('    </MAIL>\n')
    fXml.write('  </AVERTISSEMENTS>\n')
    fXml.write('  <ANOMALIES>\n')
    fXml.write('    <REF_FT>%s</REF_FT>\n'%self.REF_FT)
#    if self.ARRET_FT is None:
#      fXml.write('    <ARRET_FT></ARRET_FT>\n')
#    else:
    mot="MOT_NIVEAU_CRITIQUE_%d"%self.ARRET_FT[0]
    fXml.write('    <ARRET_FT>%s</ARRET_FT>\n'%self.lexique.entree(mot))
    fXml.write('    <CONFIRMATION_FT>%s</CONFIRMATION_FT>\n'%self.CONFIRMATION_FT)
    fXml.write('    <BASE_FT activation="%s">%s</BASE_FT>\n'%(ouiNon(self.ACTIVATION_BASEFT),fichierJoin(self.PRODUIT_BASEFT,repFiches)))
#    if self.version<9.0:
#      fXml.write('    <CAMPAGNE>%s</CAMPAGNE>\n'%fichierJoin(self.PRODUIT_CAMPAGNE,repFiches))
    fXml.write('    <MATRICES>\n')
    fXml.write('      <CTL_EXG>%s</CTL_EXG>\n'%ouiNon(self.MATTRAC_CTL_EXG))
    fXml.write('      <CTL_FCT>%s</CTL_FCT>\n'%ouiNon(self.MATTRAC_CTL_FCT))
    fXml.write('      <EXG_CTL>%s</EXG_CTL>\n'%ouiNon(self.MATTRAC_EXG_CTL))
    fXml.write('      <FCT_CTL>%s</FCT_CTL>\n'%ouiNon(self.MATTRAC_FCT_CTL))
    fXml.write('      <NON_CVT col="%d">%s</NON_CVT>\n'%(self.MATTRAC_FORMAT_COLONNE,ouiNon(self.MATTRAC_NON_CVT)))
    fXml.write('    </MATRICES>\n')
    if self.MANTIS:
      fXml.write('    <MANTIS>%s</MANTIS>\n'%self.MANTIS)
    fXml.write('  </ANOMALIES>\n')
    fXml.write('  <DEBUG>\n')
    fXml.write('    <NIVEAU>%d</NIVEAU>\n'%self.debug)
    fXml.write('    <LOG_COMPLET>%s</LOG_COMPLET>\n'%ouiNon(self.LOG_COMPLET))
#    if self.PasAPas:
    fXml.write('    <PAS_A_PAS>%s</PAS_A_PAS>\n' % fichierJoin(self.PasAPas,repFiches))
    fXml.write('  </DEBUG>\n')
    fXml.write('</CONFIGURATION>\n')
    fXml.close()

#---------------------------------------------------------------------------------
