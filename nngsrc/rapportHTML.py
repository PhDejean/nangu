#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,datetime,locale,glob,tarfile
import nngsrc.services
import nngsrc.qualite

#-------------------------------------------------------------------------------

Atester="<img src='action.png'/>"
TResult="<font class='bilan'>Bilan</font>"
DebFin="&#11200;"
REPERTRAPPORT="HTML"
DOCEXTENSION=".html"

LISTE_EXTENSION=[ DOCEXTENSION ]
#-------------------------------------------------------------------------------
def rapport_bilan(nbcol,texte,valeur=None,soit=None,pourcent=None,italique=False):
  ligne="            <tr>"
  if italique:
    ligne+=u"<td>%s</td>"%IT("-%s"%texte)
  else:
    ligne+=u"<td>%s</td>"%texte

  if valeur=='':
    colonne="&nbsp;"
  else:
    colonne="%d"%valeur
  if italique:
    ligne+="<td align='right'>%s</td>"%IT(colonne)
  else:
    ligne+="<td align='right'>%s</td>"%colonne

  if soit:
    if italique:
      ligne+="<td>%s</td>"%IT(soit)
    else:
      ligne+="<td>%s</td>"%soit

  if pourcent is not None:
    if pourcent=='':
      colonne="&nbsp;"
    else:
      colonne="%4.1f%%"%(100*pourcent)
    if italique:
      ligne+="<td align='right'><i>%s</i></td>"%colonne
    else:
      ligne+="<td align='right'>%s</td>"%colonne

  ligne+="</tr>\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan_separatrice(nbcol):
  return u"            <tr><td colspan=%d><hr/></td></tr>\n"%nbcol

#-------------------------------------------------------------------------------
def max_dispchr(res1,res2):
  if res1=='': return res2
  if res1=="OK":  return res2
  return res1

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Fonction de définition des titres de chapitre et paragraphes
#-------------------------------------------------------------------------------
def Titre(num,chaine):
  if   num==1: return Titre1(chaine)
  elif num==2: return Titre2(chaine)
  elif num==3: return Titre3(chaine)
  elif num==4: return Titre4(chaine)
def Titre1(chaine):
  return chaine
def Titre2(chaine):
  return chaine
def Titre3(chaine):
  return chaine
def Titre4(chaine):
  return chaine

#-------------------------------------------------------------------------------
# Fonction d'indexation des exigences ou FT
#-------------------------------------------------------------------------------
def indexerExigFT(chaine,formater=False):
  return chaine

#-------------------------------------------------------------------------------
# Fonction de formatage des chaines pour sortie rapport
#-------------------------------------------------------------------------------
def formateChaineHTML(objet,alaligne=0,complet=True,tabbing=False,repTravail=None,formatage=0):
  return fmtCh(objet,alaligne,complet,tabbing,repTravail,formatage)
#-------------------------------------------------------------------------------
def fmtCh(objet,alaligne=0,complet=True,tabbing=False,repTravail=None,formatage=0):
  if objet is None:
    return objet
  if type(objet)==str:
    chaine=objet
  elif type(objet)==list:
    return [fmtCh(ch,alaligne,complet,tabbing,repTravail,formatage) for ch in objet]
  else:
    chaine=str(objet)

  idxDeb=chaine.find("[IMAGE:")
  lstIMA=[]
  id=-1
  while idxDeb!=-1:
    idxfin=chaine.find("]")
    aidx=chaine[idxDeb+7:idxfin]
#    chaine=chaine.replace("[IMAGE:"+aidx+"]",)
    id+=1
    chaine=chaine.replace("[IMAGE:"+aidx+"]","[IMAGE:"+str(id)+"]")
    lstIMA.append("\\includegraphics[width=15.00cm]{"+aidx+"}")
    idxDeb=chaine.find("[IMAGE:")

  chaine=chaine.replace('&','&amp;')       # Parce qu'il le faut bien !
  if complet:
    chaine=chaine.replace('<','&lt;')
    chaine=chaine.replace('>','&gt;')
    chaine=chaine.replace('\n','<br/>')
    chaine=chaine.replace('\\\\n','$fic$')
    chaine=chaine.replace('\\n',' ')
    chaine=chaine.replace('$fic$','\\\\n')
  else:
    chaine=chaine.replace(' ','&nbsp;')

  chaine=chaine.replace('$magvalid$index','\\index')       # Oui, à la fin !
  chaine=chaine.replace('$magvalid$','$\\backslash$\\-')   # Oui, à la fin !
  idxdeb=chaine.find("MAGIDX:")
  while idxdeb!=-1:
    idxfin=chaine.find(":IDX:")
    aidx=chaine[idxdeb+7:idxfin]
    labidx=chaine.find(":IDXMAG")
    lidx=chaine[idxfin+5:labidx]
    chaine=chaine.replace("MAGIDX:"+aidx+":IDX:"+lidx+":IDXMAG",aidx)
    idxdeb=chaine.find("MAGIDX:")

  codage=locale.getdefaultlocale()[1]

#  chaine=chaine.replace(unicode('\xee\x80\x81','cp850'),"RIGHT")
#  chaine=chaine.replace(unicode('\xee\x80\xbb','cp850'),"NUM_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\x80',codage),"UP")
#  chaine=chaine.replace(unicode('\xee\x80\x82',codage),"DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x83',codage),"LEFT")
#  chaine=chaine.replace(unicode('\xee\x80\x84',codage),"PAGE_UP")
#  chaine=chaine.replace(unicode('\xee\x80\x85',codage),"PAGE_DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x86',codage),"DELETE")
#  chaine=chaine.replace(unicode('\xee\x80\x87',codage),"END")
#  chaine=chaine.replace(unicode('\xee\x80\x88',codage),"HOME")
#  chaine=chaine.replace(unicode('\xee\x80\x89',codage),"INSERT")
#
#  chaine=chaine.replace(unicode('\xee\x80\x91',codage),"F1")
#  chaine=chaine.replace(unicode('\xee\x80\x92',codage),"F2")
#  chaine=chaine.replace(unicode('\xee\x80\x93',codage),"F3")
#  chaine=chaine.replace(unicode('\xee\x80\x94',codage),"F4")
#  chaine=chaine.replace(unicode('\xee\x80\x95',codage),"F5")
#  chaine=chaine.replace(unicode('\xee\x80\x96',codage),"F6")
#  chaine=chaine.replace(unicode('\xee\x80\x97',codage),"F7")
#  chaine=chaine.replace(unicode('\xee\x80\x98',codage),"F8")
#  chaine=chaine.replace(unicode('\xee\x80\x99',codage),"F9")
#  chaine=chaine.replace(unicode('\xee\x80\x9a',codage),"F10")
#  chaine=chaine.replace(unicode('\xee\x80\x9b',codage),"F11")
#  chaine=chaine.replace(unicode('\xee\x80\x9c',codage),"F12")
#
#  chaine=chaine.replace(unicode('\xee\x80\xa0',codage),"SHIFT")
#  chaine=chaine.replace(unicode('\xee\x80\xa4',codage),"PRINTSCREEN")
#  chaine=chaine.replace(unicode('\xee\x80\xa5',codage),"SCROLL_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xa6',codage),"PAUSE")
#  chaine=chaine.replace(unicode('\xee\x80\xa7',codage),"CAPS_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\xb0',codage),"NUM0")
#  chaine=chaine.replace(unicode('\xee\x80\xb1',codage),"NUM1")
#  chaine=chaine.replace(unicode('\xee\x80\xb2',codage),"NUM2")
#  chaine=chaine.replace(unicode('\xee\x80\xb3',codage),"NUM3")
#  chaine=chaine.replace(unicode('\xee\x80\xb4',codage),"NUM4")
#  chaine=chaine.replace(unicode('\xee\x80\xb5',codage),"NUM5")
#  chaine=chaine.replace(unicode('\xee\x80\xb6',codage),"NUM6")
#  chaine=chaine.replace(unicode('\xee\x80\xb7',codage),"NUM7")
#  chaine=chaine.replace(unicode('\xee\x80\xb8',codage),"NUM8")
#  chaine=chaine.replace(unicode('\xee\x80\xb9',codage),"NUM9")
#  chaine=chaine.replace(unicode('\xee\x80\xba',codage),"SEPARATOR")
#  chaine=chaine.replace(unicode('\xee\x80\xbb',codage),"NUM_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xbc',codage),"ADD")
#  chaine=chaine.replace(unicode('\xee\x80\xbd',codage),"MINUS")
#  chaine=chaine.replace(unicode('\xee\x80\xbe',codage),"MULTIPLY")
#  chaine=chaine.replace(unicode('\xee\x80\xbf',codage),"DIVIDE")
#
#  chaine=chaine.replace(unicode('\x1b',codage),"ESC")
#  chaine=chaine.replace(unicode('\x08',codage),"BACKSPACE")

  if id!=-1:
    for i in range(id+1):
      chaine=chaine.replace("[IMAGE:"+str(i)+"]",lstIMA[i])

  while len(chaine)<formatage: chaine+=' '
  return chaine

#-------------------------------------------------------------------------------
def fmtStyle(sty,valeur):
  if sty=="tt":
    return TT(valeur)
  if sty=="it":
    return IT(valeur)
  if sty=="ittt":
    return IT(TT(valeur))
  return valeur

#-------------------------------------------------------------------------------
def IT(chaine):
  if chaine=='': return chaine
  return "<i>%s</i>"%chaine

#-------------------------------------------------------------------------------
def BF(chaine):
  if chaine=='': return chaine
  return "<b>%s</b>"%chaine

#-------------------------------------------------------------------------------
def TT(chaine):
  if chaine=='': return chaine
  return "<font class='tt'>%s</font>"%chaine
#-------------------------------------------------------------------------------
def FT(chaine):
  return chaine

#-------------------------------------------------------------------------------
def LG(chaine):
  return chaine

#-------------------------------------------------------------------------------
def convertionBlocImg(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  taille=''
  if texte[deb]!=']': # on récupère une taille
    taille="[%s]"%texte[deb+1:deb+1+texte[deb+1:].find(']')]
    decalage=len(taille)
  else:
    decalage=1
  fichier=texte[deb+decalage:fin]
#  if taille!='':
#    sousTexte="<img src='%s' size=%s/>"%(fichier,taille)
#  else:
  sousTexte="<img src='%s'/>"%fichier

  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+texte[fin:],fichier

#-----------------------------------------------------------------------------------
# Classe du rapport
#-----------------------------------------------------------------------------------
class HTML:

  #---------------------------------------------------------------------------------
  # Initialisation de la classe
  #---------------------------------------------------------------------------------
  def __init__(self,repTravail,repRessources,repRapport,ficRapport,projet,qualite,version,bilan,constantes,IDcahier):
    # Parametres communs du rapport
    self.repTravail=repTravail
    self.repRapport=repRapport
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.repCampagne=bilan
    self.repRessources=repRessources
    self.listePgnIHM={}
    self.collapseJS=False

    # Parametres specifique du rapport
    self.ficBandeau=os.path.join(self.repTravail,REPERTRAPPORT,'bandeau.html')
    self.ficCampagne=os.path.join(self.repTravail,REPERTRAPPORT,'campagne.cr')
    self.ficFTemis=os.path.join(self.repTravail,REPERTRAPPORT,'FTemis.html')
    self.ficBilan=os.path.join(self.repTravail,REPERTRAPPORT,'bilan.html')
    self.ficBilanTxt=os.path.join(self.repTravail,'bilan.txt')
    self.ficMenu=os.path.join(self.repTravail,REPERTRAPPORT,'menu.html')
    self.ficRapport=fmtCh(ficRapport)
    self.numAnnexe=0
    self.numMenu=0
    self.parallel=False

    # Creation des repertoires
    if not os.path.exists(os.path.join(self.repTravail,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repTravail,REPERTRAPPORT))
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*%s'%DOCEXTENSION)):
      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
    if not os.path.exists(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))

    # Elements des Faits techniques
    if self.projet.REF_FT is None:
      self.refFT="%s-FT"%(self.projet.VALIDATION_REFERENCE+'-'+self.projet.DOCUMENT_EDITION+'-'+self.projet.DOCUMENT_REVISION)
    else:
      self.refFT=self.projet.REF_FT
    self.numAncre=-1
    self.strAncre=''
    self.indexAncre=''
    self.valAncre=self.constantes['STATUS_OK']
    self.actAncre=''

    # Titre, sous-tire version et nom du projet
    if self.projet.PRODUIT_TITRE != '':
      titre=fmtCh(self.projet.PRODUIT_TITRE)
    else:
      titre=self.fmtLex("MOT_RAPPORT")
    sstitre=fmtCh(self.projet.PRODUIT_SOUS_TITRE)
    if self.projet.PRODUIT_NOM != '':
      nom=fmtCh(self.projet.PRODUIT_NOM)
    else:
      nom=self.fmtLex("LOCUT_SANS_NOM")
    societe=self.projet.PRODUIT_CLIENT
    self.version=version
    self.listeBBCimageInclusion=[]

    # Type de document
    if self.projet.execution=='P':
      doctype=self.projet.lexique.entree('DOCTYPE_P')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='C':
      doctype=self.projet.lexique.entree('DOCTYPE_C')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='S':
      doctype=self.projet.lexique.entree('DOCTYPE_S')%self.projet.PRODUIT_TYPE
    else:
      doctype=self.projet.lexique.entree('DOCTYPE_R')%self.projet.PRODUIT_TYPE
    doctype=fmtCh(doctype)
    if bilan:
      self.Campagne_InitRapport(nom)
    else:
      self.Valid_InitRapport(nom,societe,titre,sstitre,doctype)

  #-------------------------------------------------------------------------------
  def TexteConversion(self,texte,dirname='',varoperateur=None):
    if varoperateur:
      for v in varoperateur:
        if v['UTILE']:
          nom='['+v['NOM']+']'
          valeur=v['VALEUR']
          while texte.find(nom)!=-1:
            texte=texte.replace(nom,valeur)
    t=fmtCh(texte,1).replace('\r','')
    t=t.replace('[br/]','<br/>')
    t=t.replace('<br/>','<br/>\n')

    t=t.replace('[b]','<b>').replace('[/b]','</b>')
    t=t.replace('[i]','<i>').replace('[/i]','</i>')
    t=t.replace('[u]','').replace('[/u]','')

    for coul in ['red','green','blue','cyan','magenta','yellow']:
      t=t.replace('[color=%s]'%coul,'<font color=%s>'%coul).replace('[/color]','</font>')

    t=t.replace('[font=Courier]','').replace('[/font]','')
    t=t.replace('[font=Serif]','').replace('[/font]','')
    t=t.replace('[center]','<center>').replace('[/center]','</center>')
    t=t.replace('[left]','').replace('[/left]','')
    t=t.replace('[right]','').replace('[/right]','')

  #  t=t.replace("[img]","<img src='")
  #  t=t.replace("[/img]","'/>")
    while t.find('[img')!=-1:
      t,fichierImg=convertionBlocImg('[img','[/img]',t)
      self.listeBBCimageInclusion.append(os.path.join(dirname,fichierImg))

    t=t.replace("[size=7]","<font size='1pt'>")
    t=t.replace("[size=8]","<font size='2pt'>")
    t=t.replace("[size=9]","<font size='3pt'>")
    t=t.replace("[size=10]","<font size='4pt'>")
    t=t.replace("[size=11]","<font size='5pt'>")
    t=t.replace("[size=12]","<font size='6pt'>")
    t=t.replace("[size=13]","<font size='7pt'>")
    t=t.replace("[size=14]","<font size='7pt'>")
    t=t.replace("[size=15]","<font size='7pt'>")
    t=t.replace("[size=16]","<font size='7pt'>")
    t=t.replace("[/size]","</font>")

   # t=t.replace('[size=7]' ,'').replace('[/size]','')
   # t=t.replace('[size=8]' ,'').replace('[/size]','')
   # t=t.replace('[size=9]' ,'').replace('[/size]','')
   # t=t.replace('[size=10]','').replace('[/size]','')
   # t=t.replace('[size=11]','').replace('[/size]','')
   # t=t.replace('[size=12]','').replace('[/size]','')
   # t=t.replace('[size=13]','').replace('[/size]','')
   # t=t.replace('[size=14]','').replace('[/size]','')
   # t=t.replace('[size=15]','').replace('[/size]','')
   # t=t.replace('[size=16]','').replace('[/size]','')
   #
   # t=t.replace('[img]','').replace('[/img]','')

    id=t.find('[goto=')
    while id!=-1:
      soustexte=t[id:]
      idfin=soustexte.find('[/goto]')
      soustexte=soustexte[:(idfin+len('[/goto]'))]

      # Retrait de l'ancre
      idd=soustexte.find('[anchor=')
      while idd!=-1:
        soussoustexte=soustexte[idd:]
        iddfin=soussoustexte.find('[/anchor]')
        soussoustexte=soussoustexte[:(iddfin+len('[/anchor]'))]
        soussoustextenew=soussoustexte.replace('[/anchor]','').replace('[anchor=','')
        soussoustextenew=soussoustextenew[soussoustextenew.find(']')+1:]
        soustexte2=soustexte.replace(soussoustexte,soussoustextenew)
        t=t.replace(soustexte,soustexte2)
        soustexte=soustexte2
        idd=soustexte.find('[anchor=')

      soustextenew=soustexte.replace('[goto=',"<a href='")
      soustextenew=soustextenew.replace('[/goto]',"</a>")
      soustextenew=soustextenew.replace('#',".html#")
      soustextenew=soustextenew.replace(']',"'>")

      t=t.replace(soustexte,soustextenew)
      id=t.find('[goto=')

    id=t.find('[anchor=')
    while id!=-1:
      soustexte=t[id:]
      idfin=soustexte.find('[/anchor]')
      soustexte=soustexte[:(idfin+len('[/anchor]'))]
      soustextenew=soustexte.replace('[anchor=',"<a name='")
      soustextenew=soustextenew.replace('[/anchor]',"</a>")
      soustextenew=soustextenew.replace(']',"'>")
      ancre=soustextenew[soustextenew.find("=")+2:soustextenew.find("#")+1]
      soustextenew=soustextenew.replace(ancre,'')

      t=t.replace(soustexte,soustextenew)
      id=t.find('[anchor=')

    t=t.replace('<br/>[ul]','[ul]')
    t=t.replace('[ul]<br/>','[ul]')
    t=t.replace('<br/>[/ul]','[/ul]')
    t=t.replace('[/ul]<br/>','[/ul]')
    t=t.replace('[/li]<br/>','[/li]')
    t=t.replace('[li]','<li> ').replace('[/li]','</li>')
    id=t.find('[ul]')
    while id!=-1:
      soustexte=t[id:]
      idfin=soustexte.find('[/ul]')
      soustexte=soustexte[:(idfin+len('[/ul]'))]
      soustextenew=soustexte.replace('[ul]','<ul>')
      soustextenew=soustextenew.replace('[/ul]','</ul>')
      t=t.replace(soustexte,soustextenew)
      id=t.find('[ul]')

    t=t.replace('<br/>[ol]','[ol]')
    t=t.replace('[ol]<br/>','[ol]')
    t=t.replace('<br/>[/ol]','[/ol]')
    t=t.replace('[/ol]<br/>','[/ol]')
    id=t.find('[ol]')
    while id!=-1:
      soustexte=t[id:]
      idfin=soustexte.find('[/ol]')
      soustexte=soustexte[:(idfin+len('[/ol]'))]
      soustextenew=soustexte.replace('[ol]','<ol>')
      soustextenew=soustextenew.replace('[/ol]','</ol>')
      t=t.replace(soustexte,soustextenew)
      id=t.find('[ol]')

  #  t=t.replace('\\\\\n[table col=','\n[table col=')
  #  id=t.find('[table col=')
  #  while id!=-1:
  #    formatcol=t[id:]
  #    idfin=formatcol.find(']\\\\')
  #    formatcol=formatcol[:(idfin+len(']\\\\'))]
  #    formattex=formatcol.replace('[table col=','\\begin{nngtabular}{').replace(']\\\\','}')
  #    t=t.replace(formatcol+'\n',formattex).replace('[/\-table]','\\end{nngtabular}')
  #    id=t.find('[table col=')

  #  t=t.replace('[tr][th]','{').replace('[/\-th][/\-tr]\\\\','}')
  #  t=t.replace('[tr][td]','').replace('[/\-td][/\-tr]','')
  #  t=t.replace('[/\-td][td]',' & ').replace('[/\-th][th]',' & ')

    t=t.replace('[hr/]','<hr/>')
  #  for item in ['enumerate','itemize','nngtabular','center','flushleft','flushright']:
  #    t=t.replace('\\begin{%s}\\\\\n'%item,'\\begin{%s}\n'%item)
  #  for item in ['nngtabular','center','flushleft','flushright']:
  #    t=t.replace('\\end{%s}\\\\\n'%item,'\\end{%s}\n'%item)
  #  for item in ['enumerate','itemize']:
  #    t=t.replace('\\end{%s}\\\\\n'%item,'\\end{%s}\n\\vspace{1pc}\n'%item)

  #  t=t.replace('[br/\-]','<br>\n')
  #  while t.find('\n\\\\\n')!=-1:
  #    t=t.replace('\n\\\\\n','\n')

  #  t=t.replace('$SDL$\\\\\n','\n')
    return t

  #-------------------------------------------------------------------------------
  # formatage du lexique
  #-------------------------------------------------------------------------------
  def fmtLex(self,motclef,complement=None):
    if complement is not None:
      return fmtCh(self.projet.lexique.entree(motclef)%self.projet.lexique.entree(complement))
    return fmtCh(self.projet.lexique.entree(motclef))

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action - Cas : Plan de Validation
  #-------------------------------------------------------------------------------
  def ancreResultat(self,res,suspendu,inRapport):
    if inRapport!=0: return
    if res=='': return
    fichier=os.path.basename(self.ficIdxRapportFiche)[4:]
    resPlusCritique=False
    if res!='OK':
      if res=='NOK':
        resPlusCritique=True
        self.valAncre=self.constantes['STATUS_PB']
      elif res=='AV':
        resPlusCritique=self.valAncre!=self.constantes['STATUS_PB']
        self.valAncre=self.constantes['STATUS_AV']
      elif res=='AR':
        resPlusCritique=(self.valAncre!=self.constantes['STATUS_PB'] and self.valAncre!=self.constantes['STATUS_AV'])
        self.valAncre=self.constantes['STATUS_AR']
      elif res=='NT':
        resPlusCritique=(self.valAncre==self.constantes['STATUS_OK'])
        self.valAncre=self.constantes['STATUS_NT']

    if resPlusCritique:
      self.numAncre+=1
      self.strAncre="<a name='anc%d'></a>"%self.numAncre
      if res=='NOK':
        couleur="rouge"
        if suspendu: couleur="brun"
        self.indexAncre="        <center><a target='mainFrame' href='%s#anc%d'>%s<br><img src='%s.png' width='12px' border='0'></a></center><br/>\n"%(fichier,self.numAncre,self.actAncre,couleur)
      elif res=='AV':
        couleur="orange"
        if suspendu: couleur="brun"
        self.indexAncre="        <center><a target='mainFrame' href='%s#anc%d'>%s<br><img src='%s.png' width='12px' border='0'></a></span></center><br/>\n"%(fichier,self.numAncre,self.actAncre,couleur)
      elif res=='AR':
        self.indexAncre="        <center><a target='mainFrame' href='%s#anc%d'>%s<br><img src='violet.png' width='12px' border='0'></a></span></center><br/>\n"%(fichier,self.numAncre,self.actAncre)
      else: # NT
        self.indexAncre="        <center><a target='mainFrame' href='%s#anc%d'>%s<br><img src='noir.png' width='12px' border='0'></a></span></center><br/>\n"%(fichier,self.numAncre,self.actAncre)

  #-------------------------------------------------------------------------------
  def rapport_IndexAncre(self,index=None):
    if self.ficIdxRapportFiche is None: return
    f=open(self.ficIdxRapportFiche,"a",encoding='utf-8')
    if index is not None:
      f.write(index)
    else:
      f.write(self.indexAncre)
    f.close()

  #-------------------------------------------------------------------------------
  # Ecriture du bloc commentaire de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  @staticmethod
  def rapport_description_debut():
    return "    <table class='rapdescription'>\n        <tr>\n        <td class='item'>"

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_description_milieu(self,normal='',petit='',texte='',item='',exigence='',ft=None):
    if item!='':
      texte+=item+"</td><td>"
    texte+=normal
    if ft and ft!=0:
      texte+=" (%s '%s')"%(self.projet.lexique.entree("LOCUT_ACTION_JUGEE"),self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%ft))
    if petit!='':
      texte+=u"<br><font class='condition'>"+petit+"</font>"
    if exigence!='' and exigence is not None:
      texte+=u"<br><font class='exigence'>"+exigence+"</font>"
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_description_fin():
    return u"</td>\n        </tr>\n    </table>\n"

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  @staticmethod
  def rapport_contenu_deb(message):
    return u"    <table class='rapcontenu'>\n        <tr><th>%s</th></tr>\n"%message

  # 2- Ecriture du contenu du bloc
  @staticmethod
  def rapport_contenu_mil(action, option, lien=False):
    if type(option)==list:
      #texte="        <tr><td><font class='norm'>%s</font> <a target='blank' href='file:///%s'><font class='tt'>%s</font></a> <a target='blank' href='file:///%s'><font class='tt'>%s</font></a></td></tr>\n"%(option[0],option[1],option[1],option[2],option[2])
      texte=''
      for (un,deux) in option:
        texteAction=action%("<font class='tt'>%s</font>"%fmtCh(un),"<font class='tt'>%s</font>"%fmtCh(deux))
        texte+="        <tr><td><font class='norm'>%s</font> </font></td></tr>\n"%texteAction
    else:
      if lien:
        texte="        <tr><td><font class='norm'>%s</font> <a target='blank' href='file:///%s'><font class='tt'>%s</font></a></td></tr>\n"%(action,option,option)
      else:
        texte="        <tr><td><font class='norm'>%s</font> <font class='nobd'>%s</font></td></tr>\n"%(action,option)
    return texte

  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_contenu_fin():
    return u"    </table>\n"

  # T- Tout en un
  def rapport_contenu(self,clef,Ajeter,valeur,lien=False):
    texte =self.rapport_contenu_deb(clef)
    texte+=self.rapport_contenu_mil('',valeur,lien)
    texte+=self.rapport_contenu_fin()
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  @staticmethod
  def rapport_contenuMode_deb(clef):
    return "    <table class='rapcontenu'>\n        <tr><th>%s</th></tr>\n"%clef

  # 2- Ecriture du contenu du bloc
  def rapport_contenuMode_mil(self, valeur, substit, style, lien=False, bouton=False):
    if len(substit)>0:
      valeur+=" "
      valeur=valeur.split("%s")
      texte="        <tr><td><font class='nobd'>%s</font>"%valeur[0]
      for e in range(len(substit)):
        if lien:
          texte+="<a target='blank' href='file:///%s'><font class='%s_nobd'>%s</font></a><font class='nobd'>%s</font>"%(substit[e],style,substit[e],valeur[e+1])
        else:
          texte+="<font class='%s_nobd'>%s</font><font class='nobd'>%s</font>"%(style,substit[e],valeur[e+1])
      texte+="</td></tr>\n"
    else:
      if lien:
        texte="        <tr><td><a target='blank' href='file:///%s'><font class='%s_nobd'>%s</font></a></td></tr>\n"%(valeur,style,valeur)
      else:
        if bouton and valeur!=self.projet.lexique.entree('LOCUT_AUCUNE_SORTIE'):
          texte ="        <tr><td><button type='button' class='collapsible'>%s</button>\n"%self.fmtLex('LOCUT_CLIQUER_DEPLIER')
          texte+="        <div class='content'>\n"
          texte+="          <font class='%s_nobd'>%s</font>\n"%(style,valeur)
          texte+="        </div></td>"
          self.collapseJS=True
        else:
          texte="        <tr><td><font class='%s_nobd'>%s</font></td></tr>\n"%(style,valeur)
    return texte
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_contenuMode_fin():
    return u"    </table>\n"

  # T- Tout en un
  def rapport_contenuMode(self,clef,valeur,substit,style,lien=False,bouton=False):
    texte =self.rapport_contenuMode_deb(clef)
    texte+=self.rapport_contenuMode_mil(valeur,substit,style,lien,bouton)
    texte+=self.rapport_contenuMode_fin()
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action - Cas : Plan de Validation
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_aTester_debut(self,message,retour=None):
    texte=''
    if self.projet.execution in ['S','P']:
      # Deux colonnes
      texte="    <table class='raptester'>\n        <tr><th class='symbact'>%s</th><th class='nobd'>%s</th></tr>\n"%(Atester,message)
    else:
      if retour:
        # Quatre colonnes
        texte="    <table class='raptester'>\n        <tr><th class='symbact'>%s</th><th>%s</th><th class='rapretour'>%s</th><th class='rapbilan'>%s</th></tr>\n"%(Atester,message,retour,self.projet.lexique.entree('MOT_BILAN'))
      else:
        # Trois colonnes
        texte="    <table class='raptester'>\n        <tr><th class='symbact'>%s</th><th>%s</th><th class='rapbilan'>%s</th></tr>\n"%(Atester,fmtCh(message),self.projet.lexique.entree('ABR_BILAN'))
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_aTester_milieu(self,action,objet,inRapport,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    if type(action)!=str:
      action=str(action)
    if type(objet)!=str:
      objet=str(objet)
    if type(valeur)!=str:
      valeur=str(valeur)
#    if self.projet.execution=='S':
#      texte=''
    if self.projet.execution in ['S','P']:
      # Deux colonnes
      texte="        <tr><td class='symbact'></td><td class='nobd'>%s <font class='tt'>%s</font></td></tr>\n"%(action,fmtCh(objet))
    else:
      if retour is None:
        # Trois colonnes
        texte="        <tr><td class='symbact'></td><td>%s <font class='tt'>%s</font></td><td class='rapbilan'></td></tr>\n"%(action,objet)
      else:
        # Quatre colonnes
        self.ancreResultat(bilan,suspendu,inRapport)
        texte="        <tr><td class='symbact'></td><td>%s <font class='tt'>%s</font></td><td class='rapretour'>%s</td>"%(action,objet,valeur)
        if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
        else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>\n"%(cssBilan,bilan)
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  def rapport_aTester_fin(self,bilan='',raison=''):
    if self.projet.execution=='S':
      return ''
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='raptester'>\n"
        texte+="        <tr><td class='symbact'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="    </table>\n"
    return texte

  #-------------------------------------------------------------------------------
  # T- Tout en un
  def rapport_aTester(self,message,action,objet,inRapport,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    texte =self.rapport_aTester_debut(message,retour)
    texte+=self.rapport_aTester_milieu(action,objet,inRapport,bilan,suspendu,raison,cssBilan,retour,valeur)
    texte+=self.rapport_aTester_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  def rapport_aAnnexer(self,execution,message,action,objet,inRapport,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    texte =self.rapport_aTester_debut(message,retour)
    if type(action)!=str:
      action=str(action)
    if type(valeur)!=str:
      valeur=str(valeur)
    if objet[:4]=="Txt_":
      self.ancreResultat(bilan,suspendu,inRapport)
      texte+="        <tr><td class='symbact'></td><td><a target='blank' href='file:///%s'>%s</a></td><td class='rapretour'>%s</td>"%(objet[4:],action,valeur)
      if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
      else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>\n"%(cssBilan,bilan)
    else:
      self.ancreResultat(bilan,suspendu,inRapport)
      texte+="        <tr><td class='symbact'></td><td><a target='blank' href='file:///%s'>%s</a></td><td class='rapretour'>%s</td>"%(fmtCh(objet[4:]),fmtCh(action),fmtCh(valeur))
      if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
      else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>\n"%(cssBilan,bilan)
    texte+=self.rapport_aTester_fin(bilan,raison)
    return texte

  #-----------------------------------------------------------------------------------
  def Campagne_InitRapport(self,nom):
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()

    # Fichier principal
    nomBilan=os.path.basename(self.repCampagne)
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,'index.html'),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("        <link href='favicon.ico' rel='shortcut icon'>\n")
    f.write("        <title>"+nom+" v"+fmtCh(self.projet.PRODUIT_VERSION)+" - "+self.fmtLex('LOCUT_BILAN_QUALIF')+"</title>\n")
    f.write("    </head>\n")
    f.write("    <frameset rows='240,*' frameborder='no' border='1' framespacing='0'>\n")
    f.write("        <frame src='bandeau.html' name='topFrame' id='topFrame' title='topFrame' noresize='yes' scrolling='no'/>\n")
    f.write("        <frameset cols='300,*' frameborder='no' border='1' framespacing='1'>\n")
    f.write("            <frame src='menu.html' name='menuFrame' id='menuFrame' title='menuFrame' noresize='yes' scrolling='auto'/>\n")
    f.write("            <frame src='videMU.html' name='mainFrame' id='mainFrame' title='mainFrame' scrolling='auto'/>\n")
    f.write("        </frameset>\n")
    f.write("        <noframes>\n")
    f.write("            <body>Votre navigateur ne reconnait pas les cadres.</body>\n")
    f.write("        </noframes>\n")
    f.write("    </frameset>\n")
    f.write("</html>\n")
    f.close()

    #--------Début du document---
    # Fichier bandeau.html : Debut
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,'bandeau.html'),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bandeau'>\n")
    f.write("        <table>\n")
    f.write("            <tr><td rowspace='2'><img src='logoClient.png' height='80px' border='0' alt=''/></td>\n")
    f.write("                <td class='titredoc' width='100%%'>%s v%s<br/>\n"%(nom,fmtCh(self.projet.PRODUIT_VERSION)))
    f.write("                    <center>"+self.fmtLex('LOCUT_BILAN_QUALIF')+"<br/>\n")
    f.write("                        %s v%s\n"%(self.fmtLex('MOT_CAMPAGNE'),fmtCh(self.projet.PRODUIT_VERSION))+"\n")
    f.write("                    </center></td></tr>\n")
    f.write("        </table>\n")
    f.write("        <table>\n")
    f.write("            <tr>\n")
    f.write("                <td width='100%%'><h2>%s %s</h2>\n"%(self.projet.lexique.entree("LOCUT_BILAN_AU"),t.strftime("%d/%m/%Y %H:%M:%S")))
    f.write("                <td align='right'><img src='nangu.png' border='0' width='50px'/><p>Nangu&nbsp;v%s</p></td>\n"%self.version)
    f.write("            </tr>\n")#%self.version)
    f.write("        </table>\n")
    f.close()

    # Fichier menu.html : Creation
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,'menu.html'),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("        <script type='text/javascript'>\n")
    f.write("            function clicMenu(num) {\n")
    f.write("                isIE=(document.all)\n")
    f.write("                isNN6=(!isIE) && (document.getElementById)\n")
    f.write("                if (isIE) menu=document.all['menu' + num];\n")
    f.write("                if (isNN6) menu=document.getElementById('menu' + num);\n")
    f.write("                if (menu.style.display=='none'){\n")
    f.write("                    menu.style.display='' }\n")
    f.write("                else {\n")
    f.write("                    menu.style.display='none' }\n")
    f.write("                }\n")
    f.write("        </script>\n    </head>\n")
    f.write("    <body class='menu'>\n")
    f.write("        <table>\n")
#    f.write(Titre1(self.fmtLex('LOCUT_FEUILLE_ROUTE')))
#    f.write(self.fmtLex('INTRO_FEUILLE_ROUTE')+"\\\\\n")
#    f.write("\\IfFileExists{feuilleRoute.tex}{\\input feuilleRoute}{}\n\n")
    f.write("            <h2>%s</h2>\n"%self.fmtLex('MOT_RAPPORTS'))
    f.write("            <table>\n")
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='deroulement.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_FEUILLE_ROUTE'))
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='FTemis.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_FAITS_TECHS'))
 #   f.write(self.fmtLex('INTRO_FAITS_TECHS_OUVERTS')+"\\\\\n")
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='exigencesCouvertes.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_COUVERTURE_EXIGENCES'))
 #   f.write(self.fmtLex('INTRO_COUVERTURE_EXIGENCES')+"\\\\\n")
    f.write("            </table>\n")
    f.write("        </table>\n")
    f.write("        <hr class='menu'/>\n")
    f.write("        <table>\n")
    f.write("            <h2>%s</h2>\n"%self.fmtLex('MOT_BILAN'))
    f.write("            <table>\n")
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='FTtodo.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_BILAN_TODO'))
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='BilanExigences.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_BILAN_EXIGENCES'))
    f.write("                <tr><td><img src='cyan.png' align='middle'/></td>\n")
    f.write("                    <td><a target='mainFrame' href='BilanControles.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_BILAN_CONTROLES'))
    f.write("            </table>\n")
    f.write("        </table>\n")
    f.write("        <hr class='menu'/>\n")
    f.write("    </body>\n")
    f.write("</html>\n")
    f.close()

    #-----------------------------
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"deroulement.html"),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>%s</h1>\n"%self.fmtLex('LOCUT_FEUILLE_ROUTE'))
    f.write(self.fmtLex('INTRO_FEUILLE_ROUTE')+"<br/><br/>\n")
    f.close()

    f=open(self.ficFTemis,"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.close()

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"exigencesCouvertes.html"),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>%s</h1>\n"%self.fmtLex('LOCUT_COUVERTURE_EXIGENCES'))
    f.write(self.fmtLex('INTRO_COUVERTURE_EXIGENCES')+"<br/><br/>\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Feuille de route
  #-------------------------------------------------------------------------------
  def Campagne_FeuilleRouteDeroulement(self,Ref,CR):
    deb=CR[5].split(':')
    fin=CR[6].split(':')
    tdeb=datetime.datetime(2000,1,1,int(deb[0]),int(deb[1]),int(deb[2]))
    tfin=datetime.datetime(2000,1,1,int(fin[0]),int(fin[1]),int(fin[2]))
    if tdeb<=tfin:
      duree=str(tfin-tdeb).replace(":","h ",1).replace(":","mn ",1)+'s'
    else:
      tfin=datetime.datetime(2000,1,2,int(fin[0]),int(fin[1]),int(fin[2]))
      duree=str(tfin-tdeb).replace(":","h ",1).replace(":","mn ",1)+'s'
    if int(duree.split('h')[0])==0:
      duree=duree.split('h ')[1]
      if int(duree.split('m')[0])==0:
        duree=duree.split('mn ')[1]
    if duree[0]=='0':
      duree=duree[1:]
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"deroulement.html"),"a",encoding='utf-8')
    f.write(u"    <h2>Rapport <b>%s</b></h2>%s - %s<br/>"%(fmtCh(Ref),fmtCh(CR[2]),fmtCh(CR[3])))
    f.write(u"    <table>\n")
    f.write(u"        <tr><td>%s :</td><td>%s<i>%s:%s</i> %s <i>%s</i></td></tr>\n"%(self.fmtLex('MOT_OPERATEUR'),fmtCh(CR[9]),self.fmtLex('MOT_COMPTE'),fmtCh(CR[8]),self.fmtLex('MOT_SUR'),fmtCh(CR[7])))
    f.write(u"        <tr><td>%s</td><td><i>%s</i> %s <i>%s (%s-%s)</i></td></tr>\n"%(self.fmtLex('MOT_LE_DATE'),fmtCh(CR[4]),self.fmtLex('MOT_EN'),duree,fmtCh(CR[5]),fmtCh(CR[6])))
    f.write(u"    </table><br/>\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Recuperation du bilan des FT
  #-------------------------------------------------------------------------------
  def Campagne_RecupereBilanFT(self,Ref):
    if os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"FTemis.html"),os.F_OK):
      FTlig=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,Ref,"FTemis.html"))
      f=open(self.ficFTemis,"a",encoding='utf-8')
      f.write("    <h1>%s</h1>\n"%Ref)
      repRapports=os.path.join('..','..','..','..','Rapports',self.projet.execution,REPERTRAPPORT,Ref)
      for l in FTlig[7:-2]:
        l=l.replace('href="','target="blank" href="%s/'%repRapports)
        f.write("%s\n"%l)                                                        # Ligne 7 : <h1>Faitstechniques</h1>
    #  f.write("    <br/><br/>\n")
      f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Couverture des exigences
  #-------------------------------------------------------------------------------
  def Campagne_CouvertureExigences(self,MatExigences,fichierGrExig):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"exigencesCouvertes.html"),"a",encoding='utf-8')
    f.write("        <center><img src=%s height='300pix'></center>\n"%os.path.basename(fichierGrExig))
    f.write("        <h2>%s</h2>\n"%self.projet.lexique.entree('LOCUT_EXIGENCES_COUVERTES'))
    f.write('        <table class="matTrac">\n')
    f.write('            <tr><th class="matTrac">'+self.fmtLex('MOT_EXIGENCE')+'</th><th class="matTrac">'+self.fmtLex('MOT_OK')+'</th><th class="matTrac">'+self.fmtLex('MOT_KO')+'</th><th class="matTrac">'+self.fmtLex('MOT_AV')+'</th><th class="matTrac">'+self.fmtLex('MOT_NT')+'</th><th class="matTrac">'+self.fmtLex('LOCUT_ID_TODO')+'</th></tr>\n')
    for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
      if MatExigences[ex] is not None:
        (sev,mant)=MatExigences[ex]
        if mant=='':
          f.write('            <tr><td class="matTrac">%s</td><td><center>X</center></td><td></td><td></td><td></td><td></td></tr>\n'%fmtCh(ex))
        else: # TODO : verifier AV et NT
          if sev=='': sev=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_0')
          f.write('            <tr><td class="matTrac">%s</td><td></td><td class="matTrac"><center>%s</center></td><td></td><td></td><td class="matTrac">%s</td></tr>\n'%(fmtCh(ex),sev,fmtCh(mant)))
    f.write("        </table>\n")
    f.write("        <br>\n")

    f.write("        <h2>%s</h2>\n"%self.projet.lexique.entree('LOCUT_EXIGENCES_NON_COUVERTES'))
    f.write('        <table>\n')
    f.write('            <tr>\n')
    col=0
    self.NbExigNonCouv=0
    for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
      if MatExigences[ex] is None:
        f.writelines("<td>%s</td>"%fmtCh(ex))
        col=col+1
        self.NbExigNonCouv=self.NbExigNonCouv+1
        if col==4:
          f.writelines("</tr>\n            <tr>")
          col=0
   # for i in range(3-col):
   #   f.writelines("&")
    f.write("</tr>\n")
    f.write('        </table>\n')
    f.close()
    nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(fichierGrExig)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(fichierGrExig)),timeout=5,signaler=True)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanFTtodo(self,fichierO,fichierR):
    lo=nngsrc.services.SRV_LireContenuListe(fichierO)[1:]
    lr=nngsrc.services.SRV_LireContenuListe(fichierR)[1:]

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"FTtodo.html"),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>"+self.fmtLex('LOCUT_FAITS_TECHS_OUVERTS')+"</h1>\n")

    texte=''
    for ligne in lo:
      ligne=ligne.split(';')
      texte+="        <tr>"
      texte+="<td>%s</td>"%fmtCh(ligne[0])
      texte+="<td>%s</td>"%fmtCh(ligne[1])
    #  texte+="<td>%s</td>"%fmtCh(ligne[2])
      texte+="<td>%s</td>"%fmtCh(ligne[3])
      texte+="<td>%s</td>"%fmtCh(ligne[4])
      texte+="<td class='small'>%s</td>"%(fmtCh(ligne[5]).replace(', ','<br>'))
      texte+="</tr>\n"
    if len(texte)>0:
      f.write("    <table class='matTrac'>\n")
      f.write("        <tr>")
      f.write("<th>%s</th>"%self.fmtLex('MOT_ID'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_VERSION'))
      #f.write("<th>%s</th>"%self.fmtLex('MOT_ECHEANCE'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_SEVERITE'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_ETAT'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_DESCRIPTION'))
      f.write("</tr>\n")
      f.write(texte)
      f.write("    </table><br>\n")
    else:
      f.write("%s<br><br>\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.write("    <h1>"+self.fmtLex('LOCUT_FAITS_TECHS_RESOLUS')+"</h1>\n")
    texte=''
    for ligne in lr:
      ligne=ligne.split(';')
      if ligne[4]==nngsrc.qualite.Aclore:
        texte+="        <tr>"
        texte+="<td>%s</td>"%fmtCh(ligne[0])
        texte+="<td>%s</td>"%fmtCh(ligne[1])
       # texte+="<td>%s</td>"%fmtCh(ligne[2])
        texte+="<td>%s</td>"%fmtCh(ligne[3])
        texte+="<td>%s</td>"%fmtCh(ligne[4])
        texte+="<td class='small'>%s</td>"%(fmtCh(ligne[5]).replace(', ','<br>'))
        texte+="</tr>\n"
    if len(lr)>0:
      f.write("    <table class='matTrac'>\n")
      f.write("        <tr>")
      f.write("<th>%s</th>"%self.fmtLex('MOT_ID'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_VERSION'))
      #f.write("<th>%s</th>"%self.fmtLex('MOT_ECHEANCE'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_SEVERITE'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_ETAT'))
      f.write("<th>%s</th>"%self.fmtLex('MOT_DESCRIPTION'))
      f.write("</tr>\n")
      f.write(texte)
      f.write("    </table><br>\n")
    else:
      f.write("%s<br><br>\n"%self.fmtLex('LOCUT_SANS_OBJET'))

    f.write("    <hr/></body>\n")
    f.write("</html>\n")
    f.close()

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanExigencesControles(self,fichierGrExig,ficoutGrExig,fichierGrCont,ficoutGrCont):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"BilanExigences.html"),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>"+self.fmtLex('LOCUT_BILAN_EXIGENCES')+"</h1>\n")
    f.write(self.fmtLex('INTRO_BILAN_EXIGENCES')+"<br>\n")
    f.write("        <center><img src=%s alt=''></center>\n"%os.path.basename(ficoutGrExig))
    dat=nngsrc.services.SRV_LireContenuListe(fichierGrExig)
    try:
      nbexig=dat[5].split(' ')[2]
    except:
      nbexig=0
    sommes=[0,0,0,0,0,0,0]
    if nbexig!=0:
      f.write(fmtCh(self.projet.lexique.entree('LOCUT_NB_EXIGENCES_DANS_VALIDATION')%(nbexig,self.NbExigNonCouv))+"<br>\n")
    f.write('        <table class="matTrac">\n')
    f.write('            <tr><th class="matTrac">'+self.fmtLex('MOT_REFERENCES')  +'</th><th class="matTrac">'+self.fmtLex('ABR_NOMBRE')  +'</th>\
                       <th class="matTrac">'+self.fmtLex('ABR_CRITIQUE')    +'</th><th class="matTrac">'+self.fmtLex('ABR_BLOQUANT')+'</th>\
                       <th class="matTrac">'+self.fmtLex('ABR_MAJEUR')      +'</th><th class="matTrac">'+self.fmtLex('ABR_MINEUR')  +'</th>\
                       <th class="matTrac">'+self.fmtLex('ABR_NON_QUALIFIE')+'</th><th class="matTrac">'+self.fmtLex('MOT_OK')      +'</th></tr>\n')
    for l in dat:
      if l[0]!="#":
        ll=l.replace('\n','').split(' ')
        f.write("            <tr><td>%s</td><td><center>%s</center></td>\
                           <td><center>%s</center></td><td><center>%s</center></td>\
                           <td><center>%s</center></td><td><center>%s</center></td>\
                           <td><center>%s</center></td><td><center>%s</center></td></tr>\n"%(fmtCh(ll[1]),ll[8],ll[3],ll[4],ll[5],ll[6],ll[7],ll[9]))
        for i in range(7):
          sommes[i]=sommes[i]+int(ll[i+3])
    f.write("            <tr><td><b>%s</b></td><td><center><b>%d</b></center></td>\
          <td><b><center>%d</center></b></td><td><center><b>%d</b></center></td>\
          <td><b><center>%d</center></b></td><td><center><b>%d</b></center></td>\
          <td><b><center>%d</center></b></td><td><center><b>%d</b></center></td></tr>\n"%(self.fmtLex('MOT_TOTAUX'),sommes[5],sommes[0],sommes[1],sommes[2],sommes[3],sommes[4],sommes[6]))
    f.write("        </table>\n")
    f.write("        <br>\n")

    f.write("    </body>\n")
    f.write("</html>\n")
    f.close()
    nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(ficoutGrExig)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(ficoutGrExig)),timeout=5,signaler=True)

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"BilanControles.html"),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>"+self.fmtLex('LOCUT_BILAN_CONTROLES')+"</h1>\n")
    f.write(self.fmtLex('INTRO_BILAN_CONTROLES')+"<br>\n")
    f.write("        <center><img src=%s alt=''></center>\n"%os.path.basename(ficoutGrCont))
    dat=nngsrc.services.SRV_LireContenuListe(fichierGrCont)
    sommes=[0,0,0,0,0]
    f.write('        <table class="matTrac">\n')
    f.write('            <tr><th class="matTrac">'+self.fmtLex('MOT_REFERENCES')+'</th><th class="matTrac">'+self.fmtLex('ABR_NOMBRE')  +'</th>\
                       <th class="matTrac">'+self.fmtLex('MOT_OK')        +'</th><th class="matTrac">'+self.fmtLex('MOT_AV')+'</th>\
                       <th class="matTrac">'+self.fmtLex('MOT_KO')        +'</th><th class="matTrac">'+self.fmtLex('MOT_NT')  +'</th></tr>\n')
    for l in dat:
      if l[0]!="#":
        ll=l.replace('\n','').split(' ')
        f.write("            <tr><td>%s</td><td><center>%s</center></td>\
                           <td><center>%s</center></td><td><center>%s</center></td>\
                           <td><center>%s</center></td><td><center>%s</center></td></tr>\n"%(fmtCh(ll[1]),ll[2],ll[3],ll[4],ll[5],ll[6]))
        for i in range(5):
          sommes[i]=sommes[i]+int(ll[i+2])
    f.write("            <tr><td><b>%s</b></td><td><center><b>%d</center></b></td>\
          <td><b><center>%d</center></b></td><td><center><b>%d</b></center></td>\
          <td><b><center>%d</center></b></td><td><center><b>%d</b></center></td></tr>\n"%(self.fmtLex('MOT_TOTAUX'),sommes[0],sommes[1],sommes[2],sommes[3],sommes[4]))
    f.write("        </table>\n")

    f.write("    </body>\n")
    f.write("</html>\n")
    f.close()
    nngsrc.services.SRV_copier(os.path.join(self.repTravail,os.path.basename(ficoutGrCont)),os.path.join(self.repTravail,REPERTRAPPORT,os.path.basename(ficoutGrCont)),timeout=5,signaler=True)

  #-------------------------------------------------------------------------------
  # Fin du bilan de campagne
  #-------------------------------------------------------------------------------
  def Campagne_Terminer(self,campagne):
    f=open(self.ficFTemis,"a",encoding='utf-8')
    f.write("    </body>\n</html>\n")
    f.close()

    if not os.access(self.repCampagne,os.F_OK):
      os.mkdir(self.repCampagne)

    destination=os.path.join(self.repCampagne,REPERTRAPPORT,campagne.refCampagne+'-%04d'%campagne.iterCampagne)
    if not os.access(destination,os.F_OK):
      if not os.access(os.path.join(self.repCampagne,REPERTRAPPORT),os.F_OK):
        os.mkdir(os.path.join(self.repCampagne,REPERTRAPPORT))
      os.mkdir(destination)

    # On redirige'
    f=open(os.path.join(self.repCampagne,REPERTRAPPORT,'index.html'),"w",encoding='utf-8')
    f.write("<html><head></head><body>\n")
    f.write("<script type='text/javascript'>\n")
    f.write("<!--\n")
    f.write("location.replace('%s/index.html');\n"%(campagne.refCampagne+'-%04d'%campagne.iterCampagne))
    f.write("//-->\n")
    f.write("</script>\n")
    f.write("</body></html>\n")
    f.close()

    # on deplace les rapports dans le repertoire final
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.*')):
      ficDestination=os.path.join(destination,os.path.basename(elem))
      if os.access(ficDestination,os.F_OK):
        nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    for f in [self.projet.DOCUMENT_REDIGE_SIGN,self.projet.DOCUMENT_REDIGE2_SIGN,self.projet.DOCUMENT_VERIFIE_SIGN,self.projet.DOCUMENT_VERIFIE2_SIGN,self.projet.DOCUMENT_VALIDE_SIGN,self.projet.DOCUMENT_REALISE_SIGN]:
      if f!='':
        ficSignature=os.path.basename(f)
        if ficSignature!='':
          nngsrc.services.SRV_copier(f,os.path.join(destination,ficSignature),timeout=5,signaler=True)

    # Elements d'IHM
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.png')):
      ficdest=os.path.join(destination,os.path.basename(elem))
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    # Elements des ressources a chercher dans le projet
    if os.path.exists(self.projet.CLIENT_LOGO):
      nngsrc.services.SRV_copier(self.projet.CLIENT_LOGO,os.path.join(destination,"logoClient.png"))
    else:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree("LOCUT_LOGO_CLIENT_ABSENT")%self.projet.CLIENT_LOGO,style="Wr")
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,'nangu.png'),os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport,"logoClient.png"))
    if os.path.exists(self.projet.SOCIETE_LOGO):
      nngsrc.services.SRV_copier(self.projet.SOCIETE_LOGO,os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport,"logoSociete.png"))

    for f in ['vert.png','violet.png','brun.png','cyan.png','orange.png','rouge.png','noir.png','action.png','arrow_up.png','nangu.css','favicon.ico']:
      if os.access(os.path.join(self.repTravail,f),os.F_OK):
        nngsrc.services.SRV_copier(os.path.join(self.repTravail,f),os.path.join(destination,f),timeout=5,signaler=True)
      else:
        nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)

    num=1
    f="etape%d.png"%num
    while os.access(os.path.join(self.repRessources,f),os.F_OK):
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)
      num+=1
      f="etape%d.png"%num

    # Elements des ressources a chercher dans le projet
    for f in ('videMU.html','videIDX.html','videETP.html','nangu.png'):
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)

    # Archivage
    self.projet.ARCHIVAGE=True
    if self.projet.ARCHIVAGE:
      fichCmplt=destination+".tgz"
      if os.access(fichCmplt,os.F_OK):
        nngsrc.services.SRV_detruire(fichCmplt,timeout=5,signaler=True)
      ficTGZ=tarfile.open(fichCmplt,mode="w:gz",encoding='utf-8')
      ficTGZ.add(destination,arcname=os.path.basename(destination))
      ficTGZ.close()
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ARCHIVE_DANS')%fichCmplt,style="r")

  #-------------------------------------------------------------------------------
  def Valid_InitRapport(self,nom,societe,titre,sstitre,doctype):
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()

    # Fichier principal
    f=open(os.path.join(self.repTravail,'HTML','index.html'),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("        <link href='favicon.ico' rel='shortcut icon'>\n")
    f.write("        <title>"+nom+" v"+fmtCh(self.projet.PRODUIT_VERSION)+" - "+self.ficRapport+"</title>\n")
    f.write("    </head>\n")
    f.write("    <frameset rows='220,*' frameborder='no' border='1' framespacing='0'>\n")
    f.write("        <frame src='bandeau.html' name='topFrame' id='topFrame' title='topFrame' noresize='yes' scrolling='no'/>\n")
    f.write("        <frameset cols='300,*,80,80' frameborder='no' border='1' framespacing='1'>\n")
    f.write("            <frame src='menu.html' name='menuFrame' id='menuFrame' title='menuFrame' noresize='yes' scrolling='auto'/>\n")
    f.write("            <frame src='videMU.html' name='mainFrame' id='mainFrame' title='mainFrame' scrolling='auto'/>\n")
    f.write("            <frame src='videETP.html' name='etapeFrame' id='etapeFrame' title='etapeFrame' scrolling='auto'/>\n")
    f.write("            <frame src='videIDX.html' name='indexFrame' id='indexFrame' title='indexFrame' scrolling='auto'/>\n")
    f.write("        </frameset>\n")
    f.write("        <noframes>\n")
    f.write("            <body>Votre navigateur ne reconnait pas les cadres.</body>\n")
    f.write("        </noframes>\n")
    f.write("    </frameset>\n")
    f.write("</html>\n")
    f.close()

    #--------Début du document---
    # Fichier bandeau.html : Debut
    f=open(self.ficBandeau,"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bandeau'>\n")
    f.write("        <table>\n")
    f.write("            <tr><td rowspace='2'><img src='logoClient.png' height='80px' border='0' alt=''/></td>\n")
    f.write("                <td class='titredoc' width='100%%'>%s v%s -- %s<br/><center>%s<br/>%s</center></td>\n"%(nom,fmtCh(self.projet.PRODUIT_VERSION),societe,titre,sstitre))
    f.write("                <td><img src='logoSociete.png' border='0' alt=''/></td></tr>\n")
    f.write("        </table>\n")
    if self.ficRapport!='':
      texte=self.projet.lexique.entree("LOCUT_BILAN_DU_DE_A")
      texte=texte%(self.ficRapport,t.strftime("%d/%m/%Y"),t.strftime("%H:%M:%S"))
      f.write("        <table>\n            <tr>\n                <td width='100%%'><h2>%s HEUREFIN</h2>\n"%texte)
    else:
      f.write("        <table>\n            <tr>\n                <td width='100%%'><h2>%s %s</h2>\n"%(self.projet.lexique.entree("LOCUT_BILAN_AU"),t.strftime("%d/%m/%Y %H:%M:%S")))
    f.close()

    #--------Début du document---
    # Fichier campagne.cr : Debut
    f=open(self.ficCampagne,"w",encoding='utf-8')
    f.write("%s\n"%nom)
    f.write("%s\n"%fmtCh(self.projet.PRODUIT_VERSION))
    f.write("%s\n"%titre)
    f.write("%s\n"%sstitre)
    f.write("%s\n"%t.strftime("%d/%m/%Y"))
    f.write("%s\n"%t.strftime("%H:%M:%S"))
    f.close()

    #--------Début du document---
    # Fichier bilan.html : Debut
    f=open(self.ficBilan,"w",encoding='utf-8')
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>%s</h1><br>\n"%self.fmtLex('TITRE2_BILAN_CHIFFRE'))
    f.close()

    # Fichier FTEmis.html : Debut
    f=open(self.ficFTemis,"w",encoding='utf-8')
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>%s</h1>\n"%self.fmtLex('TITRE1_FAITTECH'))
    f.close()

    # Fichier menu.html : Creation
    f=open(self.ficMenu,"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("        <script type='text/javascript'>\n")
    f.write("            function clicMenu(num) {\n")
    f.write("                isIE=(document.all)\n")
    f.write("                isNN6=(!isIE) && (document.getElementById)\n")
    f.write("                if (isIE) menu=document.all['menu' + num];\n")
    f.write("                if (isNN6) menu=document.getElementById('menu' + num);\n")
    f.write("                if (menu.style.display=='none'){\n")
    f.write("                    menu.style.display='' }\n")
    f.write("                else {\n")
    f.write("                    menu.style.display='none' }\n")
    f.write("                }\n")
    f.write("            function load() {\n")
    f.write("                parent.mainFrame.location.href=arguments[0]+'#anc0';\n")
    f.write("                parent.etapeFrame.location.href='etp-'+arguments[0];\n")
    f.write("                parent.indexFrame.location.href='idx-'+arguments[0];\n")
    f.write("                }\n")
    f.write("        </script>\n    </head>\n")
    f.write("    <body class='menu'>\n")
    f.write("        <h2>%s</h2><br/>\n"%self.projet.lexique.entree('LOCUT_LISTE_DES_TESTS'))
    f.write("        <table>\n")
    f.close()

    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"bilanRapport%s"%DOCEXTENSION),"w",encoding='utf-8')
    colonne='MOT_CONTROLE'
    if self.projet.nivRapport==1: colonne='MOT_CONTROLE'
    elif self.projet.nivRapport==2:
      if self.projet.typePRJsimple: colonne='MOT_CONTROLE'
      else: colonne='MOT_SCENARIO'
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("        <h1>%s</h1>\n"%self.projet.lexique.entree('TITRE1_BILAN_CONTROLES'))
    f.write('        <table class="matTrac">\n')
    f.write("            <tr><th>%s</th><th>%s</th><th>%s</th></tr>\n"%(self.fmtLex(colonne),self.fmtLex('MOT_BILAN'),self.fmtLex('MOT_SEVERITE')))
    f.close()

  #---------------------------------------------------------------------------------
  def Valid_InscrireEtape(self,libelle):
    f=open(self.ficRapportFiche,"a",encoding='utf-8')
    f.write("        %s\n"%libelle)
    f.close()

  #-------------------------------------------------------------------------------
  # Initialisation du Bloc
  #-------------------------------------------------------------------------------
  def Valid_InitChapitre(self,chapitre):
    if self.projet.nivRapport>0: return
    f=open(self.ficMenu,"a",encoding='utf-8')
    f.write("        </table>\n")
    f.write("        <a href='#' onClick='clicMenu(\""+str(self.numMenu)+"\"); return false'><h3>$"+chapitre+"$.png " + chapitre + "</h3></a>\n")
    f.write("        <table style='display:none' id='menu"+str(self.numMenu)+"'>\n")
    f.close()
    self.numMenu+=1

  #-------------------------------------------------------------------------------
  # Mise a jour du bilan
  #-------------------------------------------------------------------------------
  def Valid_TerminerChapitre(self,sousreference,TestPath,resultat,criticiteSection):
    #print("DBG-Valid_TerminerChapitre>")
    #print("DBG-Valid_TerminerChapitre>","Resultat_Section",sousreference,resultat)
    #print("DBG-Valid_TerminerChapitre>")
    disp=nngsrc.services.SRV_codeResultat(resultat,self.constantes,self.projet.DOCUMENT_LANGUE)
    reference=sousreference.split('_')[0]
    data=nngsrc.services.SRV_LireContenu(self.ficMenu)
    data=data.replace('$'+reference+"$.png",'$'+disp[2]+"$.png",1)
    f=open(self.ficMenu,"w",encoding='utf-8')
    f.write(data)
    f.close()
    if disp[2]=="PB":
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,"noir.png"),os.path.join(self.repTravail,'HTML',reference+".png"),timeout=5,signaler=True)
    if disp[2]=="NOK":
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,"rouge.png"),os.path.join(self.repTravail,'HTML',reference+".png"),timeout=5,signaler=True)
    if disp[2]=="OK":
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,"vert.png"),os.path.join(self.repTravail,'HTML',reference+".png"),timeout=5,signaler=True)
    if disp[2]=="AV":
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,"orange.png"),os.path.join(self.repTravail,'HTML',reference+".png"),timeout=5,signaler=True)
    if disp[2]=="AR":
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,"violet.png"),os.path.join(self.repTravail,'HTML',reference+".png"),timeout=5,signaler=True)
    reference=sousreference.split('_')[0]
    self.Valid_DeroulementTests(reference,resultat,max(criticiteSection),fiche=False)

  #-------------------------------------------------------------------------------
  # Initialisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_InitRapportFiche(self,rapportActions,reference,rang,fiche,tagFiche,fichePath,txtLoop,nbaffloop):
    self.ficIdxRapportFiche=None
    self.ficRapportFiche=None
    self.numAncre=-1
    self.strAncre=''
    self.indexAncre=''
    self.valAncre=self.constantes['STATUS_OK']
    self.actAncre=''
    self.qualite.rapportFTfiche=[]
    self.qualite.rapportFTficheReserves=[]  # v8.2rc1 Confirmation des FT
    self.qualite.lstFTficheaClore=[]
    self.qualite.lstFTfichereOuvert=[]
    self.section=fichePath
    self.fiche=os.path.basename(fiche)
    self.tagFiche=tagFiche
    self.collapseJS=False
    if rapportActions==-1: return

    fiche=fiche.replace('\\','_').replace('/','_')
    titre="%s %s%s"%(self.projet.lexique.entree('LOCUT_RAPPORT_DE'),self.fiche,tagFiche)

    self.ficRapportFiche=os.path.join(self.repTravail,REPERTRAPPORT,'%s-%d-%s%s'%(reference,rang,fiche,DOCEXTENSION))
    f=open(self.ficRapportFiche,"w",encoding='utf-8')
    # Entete fichier
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>"+ titre +"</title>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='fiches'>\n")
    # Titre
    if fichePath=='':
       f.write("    <h1>"+ titre +"</h1>\n")
    else:
       f.write("    <h1>%s %s<br/>%s</h1>\n"%(self.projet.lexique.entree('MOT_SECTION'),fichePath,titre))
    if self.projet.DOCUMENT_FILIGRANE!='':
       f.write("    <h1 class='filigrane'>"+ self.projet.DOCUMENT_FILIGRANE +"</h1>\n")
    f.close()

    self.ficIdxRapportFiche=os.path.join(self.repTravail,'HTML','idx-%s-%d-%s.html'%(reference,rang,fiche))
    f=open(self.ficIdxRapportFiche, "w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='index'>\n")
    f.write("        <h2>Idx</h2></br>\n")
    f.close()

    self.ficEtpRapportFiche=os.path.join(self.repTravail,'HTML','etp-%s-%d-%s.html'%(reference,rang,fiche))
    f=open(self.ficEtpRapportFiche, "w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='index'>\n")
    f.write("        <h2>%s</h2></br>\n"%self.projet.lexique.entree('ABR_ETAPE'))
    f.close()

  #-------------------------------------------------------------------------------
  # Finalisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_TerminerRapportFiche(self,result,criticite,EXECUTE_FIN,pathFiche,gestionFTfiches,rapportActions):
    # Mise à jour du rapport de la fiche
    fiche=pathFiche.split('§')[0]
    fiche=fiche.replace('\\','_').replace('/','_')
    if os.access(self.ficRapportFiche, os.F_OK):
       f=open(self.ficRapportFiche, "a",encoding='utf-8')
       if os.access('erreur.err', os.F_OK):
          erreur=nngsrc.services.SRV_LireContenuListe('erreur.err')
          for l in range(len(erreur)):
             f.write("        %s<br/>\n"%erreur[l])
       if self.collapseJS:
         f.write("   <script type='text/javascript' src='collapse.js'></script>\n")
       f.write("    </body>\n</html>\n")
       f.close()

    if os.access(self.ficIdxRapportFiche, os.F_OK):
      fichier=os.path.basename(self.ficIdxRapportFiche)[4:]
      f=open(self.ficIdxRapportFiche, "a",encoding='utf-8')
      f.write("        <center><a target='mainFrame' href='%s#'><br><img src='arrow_up.png' width='16px' border='0'><br>%s</a></center><br/>\n"%(fichier,self.projet.lexique.entree('MOT_HAUT')))
      f.write("    </body>\n</html>\n")
      f.close()

    if os.access(self.ficEtpRapportFiche, os.F_OK):
      fichier=os.path.basename(self.ficEtpRapportFiche)[4:]
      f=open(self.ficEtpRapportFiche, "a",encoding='utf-8')
      f.write("        <center><a target='mainFrame' href='%s#'><br><img src='arrow_up.png' width='16px' border='0'></a><br>%s</center><br/>\n"%(fichier,self.projet.lexique.entree('MOT_HAUT')))
      f.write("    </body>\n</html>\n")
      f.close()

    # Mise à jour des identifiants des FT en cas de parallelisation
    tableFT={}
    if self.parallel:
      f=open(self.ficRapportFiche,"r",encoding='utf-8')
      contenu=f.read()
      f.close()
      for e in gestionFTfiches:
        (FT,numBase,libelleFT,ft,elem,lafiche,action)=e
        if ft and ft>0:
          self.qualite.newNumFT+=1
          tableFT[ft]=self.qualite.newNumFT
          ancStr="$%06d$"%ft
          nouStr="%06d"%self.qualite.newNumFT
          contenu=contenu.replace(ancStr,nouStr)
      f=open(self.ficRapportFiche,"w",encoding='utf-8')
      f.write(contenu)
      f.close()

    # Fichier menu.html : Mise a jour
    if EXECUTE_FIN:
      disp=nngsrc.services.SRV_imageResultat(result,self.constantes)
    else:
      disp="noir"
    # - si rapport='oui' (0)
    # - si rapport='siNonOk' (1) et resultat(r)!=OK
    # - si rapport='siOk' (2) et resultat(r)=OK
    if rapportActions==0 or (rapportActions==1 and result!=self.constantes['STATUS_OK']) or (rapportActions==2 and result==self.constantes['STATUS_OK']):
      tpl="            <tr><td><img src='%s.png' align='middle'/></td><td><a href='#' onClick='load(\"%s\"); return false'>%s%s</a></td></tr>\n"%(disp,os.path.basename(self.ficRapportFiche),self.fiche,self.tagFiche)
      f=open(self.ficMenu, "a",encoding='utf-8')
      f.write(tpl)
      f.close()
      self.Valid_DeroulementTests(pathFiche,result,criticite)
    return tableFT

  #-------------------------------------------------------------------------------
  # Gestion des boucles
  #-------------------------------------------------------------------------------
  def Valid_DebutBoucle(self,num,ident,condition,varList,execution):
    texte="    %s %s<br>\n"%(DebFin,IT(fmtCh(self.projet.lexique.entree("LOCUT_DEBUT_BOUCLE")%(num,ident))))
    if condition:
      texte+="    &nbsp; &nbsp; %s%s<br>\n"%(IT(fmtCh(self.projet.lexique.entree("LOCUT_CONDITION_EXEC"))),fmtCh(condition))
    if varList:
      for i in varList:
        texte+="    %s %s "%(fmtCh("<%s>"%i[0]),self.fmtLex("LOCUT_PREND_VALEUR_LISTE"))
        for e in range(len(i)):
          if e!=0:
            texte+=TT(fmtCh(i[e]))
            if e!=len(i)-1:
              texte+=', '
        texte+='<br>\n'
    return texte

  #-------------------------------------------------------------------------------
  def Valid_FinBoucle(self,num,execution):
    return "    %s %s<br>\n"%(DebFin,IT(fmtCh(self.projet.lexique.entree("LOCUT_FIN_BOUCLE")%num)))

  #-------------------------------------------------------------------------------
  # Gestion des débuts de parties
  # Specifique :
  # - Mise en place du menu déroulant
  #-------------------------------------------------------------------------------
  def Valid_DebutPartie(self,reference,separateurSuffixe,soustitre,scenario,repScenarios):
    reference=os.path.basename(reference)
    f=open(self.ficMenu, "a",encoding='utf-8')
    f.write("        </table>\n")
    f.write("        <a href='#' onClick='clicMenu(\""+str(self.numMenu)+"\"); return false'><h3>$"+reference+"$.png "+reference+"</h3></a>\n")
    f.write("        <table style='display:none' id='menu"+str(self.numMenu)+"'>\n")
    f.close()
    self.numMenu+=1

  #-------------------------------------------------------------------------------
  def Valid_RapporterResultat(self,resFiche,description,pathFiche,tagFiche,varoperateur,rapport):
  #  exigences: object
    (entite,methode,typo,nature,commentaire,exigences)=description
    # ajout de la description
    texte =u"        <h2>"+self.fmtLex('MOT_OBJECTIF')+" : <em>%s</em></h2>\n"%self.TexteConversion(commentaire,os.path.join(self.projet.FICHES,os.path.dirname(pathFiche)),varoperateur)
    # ajout des exigences
    if exigences!='':
      texte+=u"        <p><b>"+self.fmtLex('LOCUT_COUVERTURE_EXIGENCES')+" :</b> <em>%s</em></p>\n" % fmtCh(indexerExigFT(exigences),True,True).replace(';','<br/>')
    # ajout du rapport de la fiche
    disp=nngsrc.services.SRV_codeResultat(resFiche,self.constantes,self.projet.DOCUMENT_LANGUE)
    texte +=u"        <hr/>\n        <h2>%s : <font class='%s'>%s</font></h2>\n        <hr/>\n" % (self.fmtLex("LOCUT_RESULTAT_GLOBAL"),disp[0],fmtCh(disp[1],False,True))
    texte+=rapport
    # ajout du bilan
    f=open(self.ficRapportFiche,"a",encoding='utf-8')
# todo    try:
    f.write(texte)
#    except :
#      print("------------")
#      print("TYPE DE",texteCODE)
#      print(type(texteCODE))
#      print("------------")
#      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ERREUR HTML : Ecriture du rapport de la fiche %s avec encodage impossible"%fiche,style="Er")
#      print("------------------------------------------------------")
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_DeroulementTests(self,pathFiche,result,criticite,fiche=True):
    # ecriture de la grille de deroulement des tests
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"bilanRapport%s"%DOCEXTENSION),"a",encoding='utf-8')
    if self.projet.nivRapport==1:  # Niveau scenario
      fiche=False
      if pathFiche.find('-')!=-1:
        pathFiche=pathFiche.replace('/',':').replace('\\',':').split('-')
        pathFiche='-'.join(pathFiche[2:])
        fiche=True
    if self.projet.execution=='R':
      disp=nngsrc.services.SRV_codeResultat(result,self.constantes,self.projet.DOCUMENT_LANGUE)
      mot_criticite=self.fmtLex("MOT_NIVEAU_CRITIQUE_%s"%criticite)
      if fiche:
        f.write("            <tr><td>%s</td><td><center>$%s$</center></td><td><center>%s</center></td></tr>\n"%(fmtCh(pathFiche),fmtCh(disp[2]),mot_criticite))
      else:
        f.write("            <tr><td class='scn'>%s</td><td class='scn'><center>$%s$</center></td><td class='scn'><center>%s</center></td></tr>\n"%(fmtCh(pathFiche),fmtCh(disp[2]),mot_criticite))
    f.close()

  #-------------------------------------------------------------------------------
  # Mise à jour et fin
  #-------------------------------------------------------------------------------
  def Valid_Terminer(self,reference,rang,bilan,nbControles):
    (total,nbCtrlBLOQ)=self.qualite.OkArAvPbNt
    n_OK=bilan["vert"]
    n_AR=bilan["violet"]
    n_AV=bilan["orange"]
    n_PB=bilan["rouge"]
    n_NT=bilan["noir"]

    # Fichier Menu :
    #  1- On ajoute la fin du tableau
    f=open(self.ficMenu, "a",encoding='utf-8')
#    f.write("            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>\n")
    f.write("        </table>\n        <br/><hr/>\n")

    #  2- On ajoute les éventuels notes manuelles
    if self.projet.execution=='R':
      if os.access(os.path.join(self.repTravail,"notesManuelles.bbc"),os.F_OK):
        f.write("        <a target='mainFrame' href='notesManuelles.html'><b>%s</b></a><br>\n"%fmtCh("Notes et commentaires"))
        texte=nngsrc.services.SRV_LireContenu(os.path.join(self.repTravail,"notesManuelles.bbc"))

        g=open(os.path.join(self.repTravail,'HTML',"notesManuelles.html"), "w",encoding='utf-8')
        g.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
        g.write("<html>\n")
        g.write("    <head>\n")
        g.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
        g.write("        <link rel='stylesheet' href='nangu.css'/>\n")
        g.write("    </head>\n")
        g.write("    <body>\n")
        g.write("<h1>Notes et commentaires</h1><br>\n")
        g.write(self.TexteConversion(texte,''))
        g.write("    </body>\n</html>\n")
        g.close()
    f.write("        <h2>%s</h2>\n"%self.fmtLex('MOT_BILAN'))
    f.write("        <table>\n")
    f.write("          <tr><td><a target='mainFrame' href='bilan.html'>%s</a></td></tr>\n"%self.fmtLex('TITRE2_BILAN_CHIFFRE'))
    f.write("          <tr><td><a target='mainFrame' href='bilanRapport.html'>%s</a></td></tr>\n"%self.fmtLex('TITRE1_BILAN_CONTROLES'))
    f.write("          <tr><td><a target='mainFrame' href='FTemis.html'>%s</a></td></tr>\n"%self.fmtLex('TITRE1_FAITTECH'))
    f.write("          <tr><td><a target='mainFrame' href='matTracabilite.html'>%s</a></td></tr>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        </table>\n")
    f.write("        <hr/>\n")
    f.write("    </body>\n</html>\n")
    f.close()

    #  3- On remplace les pseudo-image par les images source
    data=nngsrc.services.SRV_LireContenu(self.ficMenu)
    data=data.replace("$NT$.png" ,"<img src='noir.png' border='0'/> ")
    data=data.replace("$NOK$.png","<img src='rouge.png' border='0'/> ")
    data=data.replace("$OK$.png" ,"<img src='vert.png' border='0'/> ")
    data=data.replace("$AV$.png" ,"<img src='orange.png' border='0'/> ")
    data=data.replace("$AR$.png" ,"<img src='violet.png' border='0'/> ")
    f=open(self.ficMenu,"w",encoding='utf-8')
    f.write(data)
    f.close()

    # Fichier Bilan
    grille=os.path.join(self.repTravail,REPERTRAPPORT,"bilanRapport%s"%DOCEXTENSION)
    f=open(grille,"a",encoding='utf-8')
    f.write("        </table>\n    </body>\n</html>\n")
    f.close()
    data=nngsrc.services.SRV_LireContenu(grille)
    data=data.replace("$NT$" ,"<img src='noir.png' border='0'/> ")
    data=data.replace("$NOK$","<img src='rouge.png' border='0'/> ")
    data=data.replace("$OK$" ,"<img src='vert.png' border='0'/> ")
    data=data.replace("$AV$" ,"<img src='orange.png' border='0'/> ")
    data=data.replace("$AR$" ,"<img src='violet.png' border='0'/> ")
    f=open(grille,"w",encoding='utf-8')
    f.write(data)
    f.close()

    # Fichier Bandeau
    data=nngsrc.services.SRV_LireContenu(self.ficBandeau)
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()
    data=data.replace("HEUREFIN",t.strftime("%H:%M:%S"))
    f=open(self.ficBandeau,"w",encoding='utf-8')
    f.write(data)
    f.close()

    # Fichier Campagne
    f=open(self.ficCampagne,"a",encoding='utf-8')
    f.write("%s\n"%t.strftime("%H:%M:%S"))
    f.close()

    tpl=", <font class='%s'>&nbsp;%s&nbsp;</font>&nbsp;%s"
    disp_OK=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_OK'],self.constantes,self.projet.DOCUMENT_LANGUE)
    disp_AR=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
    disp_AV=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AV'],self.constantes,self.projet.DOCUMENT_LANGUE)
    disp_PB=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_PB'],self.constantes,self.projet.DOCUMENT_LANGUE)
    disp_NT=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_NT'],self.constantes,self.projet.DOCUMENT_LANGUE)

    newdata="                    <h2>&nbsp;&nbsp;"+str(total) + "&nbsp;%s" + tpl + tpl + tpl + tpl + tpl + "</h2></td>\n"
    newdata=newdata % (self.projet.lexique.entree('MOT_TESTS'),disp_OK[0],str(n_OK),fmtCh(disp_OK[4]),disp_AR[0],str(n_AR),fmtCh(disp_AR[4]), disp_AV[0], str(n_AV), fmtCh(disp_AV[4]), disp_PB[0], str(n_PB), fmtCh(disp_PB[4]), disp_NT[0], str(n_NT), fmtCh(disp_NT[4]) )

    f=open(self.ficBandeau,"a",encoding='utf-8')
    f.write(newdata)
    f.write("                <td align='right'><img src='nangu.png' border='0' width='50px'/><p>Nangu&nbsp;v%s</p></td>\n            </tr>\n"%self.version)
    f.write("        </table>\n")
    if not self.projet.execution in ['P','S']:
      f.write("        <p>%s %s - %s %s</p>\n"%(self.projet.lexique.entree('MOT_POSTE'),self.projet.COMPUTERNAME,self.projet.lexique.entree('MOT_UTILISATEUR'),self.projet.USERNAME))
    f.write("        <hr/>\n    </body>\n</html>\n")
    f.close()
    f=open(self.ficBilanTxt,"w",encoding='utf-8')
    f.write("%d\n%d\n%d\n%d\n%d\n%d\n"%(total,n_OK,n_AR,n_AV,n_PB,n_NT))
    f.close()

    f=open(self.ficCampagne,"a",encoding='utf-8')
    f.write(u"%s\n"%self.projet.COMPUTERNAME)
    f.write(u"%s\n"%self.projet.USERNAME)
    f.write(u"%s\n"%self.projet.DOCUMENT_REALISE_PAR)
    f.close()

    # Fichier Bilan et Bilan Mail
    f=open(self.ficBilan,"a",encoding='utf-8')
    f.write("<table class='stats'>\n")
    f.write("<tr><td>\n")
    f.write("    %s :<br>\n"%self.fmtLex('LOCUT_COMPLETUDE_RAPPORT'))
    f.write("    -%s : %d/%d<br>\n"%(self.fmtLex('MOT_CONTROLES'),total,nbControles))
    f.write("    -%s : %d/%d<br>\n"%(self.fmtLex('MOT_EXIGENCES'),len(self.qualite.listeExig.keys()),len(self.qualite.exigenceVSft)))
    f.write("    -%s : %d/%d<br><hr/>\n"%(self.fmtLex('MOT_FONCTIONS'),len(self.qualite.listeFct.keys()),len(self.qualite.fonctionVSft)))
    f.write("</td><td>\n")
    texte="    "
    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      texte+="<img src=graphiqueExig.png height='240px'>&nbsp;"
    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      texte+="<img src=graphiqueFonct.png height='240px'>&nbsp;"
    if self.projet.execution=='R':
      texte+="<img src=graphiqueCtrl.png height='240px'>"
    f.write("%s\n"%texte)
    f.write("</td></tr>\n")
    f.write("</table>\n")
    f.write("<hr/>\n")

    divisExig=len(self.qualite.listeExig.keys())
    if divisExig==0:
      divisExig=1
    divisExig=float(divisExig)
    divisFct=len(self.qualite.listeFct.keys())
    if divisFct==0:
      divisFct=1
    divisFct=float(divisFct)
    f.write("    <table class='stats'><tr><td>\n")
    f.write("    <h2>"+self.fmtLex('MOT_GENERALITES')+u"</h2>\n")
    f.write("        <table width=100% valign='top'>\n")
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_CONTROLES_PLAN'),nbControles))
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_CONTROLES_DOCUMENT'),total))
    f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      f.write(rapport_bilan_separatrice(2))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_TOTAL_EXIG'),len(self.qualite.exigenceVSft)))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys())))
    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      f.write(rapport_bilan_separatrice(2))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_TOTAL_FCT'),len(self.qualite.fonctionVSft)))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys())))
    f.write(rapport_bilan_separatrice(2))
    fc=open(self.ficCampagne,"a",encoding='utf-8')
    if self.projet.execution in ['R','C','M']:
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_COUVERTS'),len(self.qualite.listeFT.keys())))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_BASE'),self.qualite.nbFTenCours))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_SUSPENDUS_BASE'),self.qualite.nbFTSuspendus))
      fc.write("%s %d\n"%(self.projet.lexique.entree("LOCUT_NB_FT_COUVERTS"),len(self.qualite.listeFT.keys())))
      fc.write("%s %d\n"%(self.projet.lexique.entree('LOCUT_NB_FT_OUVERTS_BASE'),self.qualite.nbFTenCours))
      fc.write("%s %d\n"%(self.projet.lexique.entree('LOCUT_NB_FT_SUSPENDUS_BASE'),self.qualite.nbFTSuspendus))
    if self.projet.execution=='R':
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_NOUVEAUX'),self.qualite.newNumFT-self.qualite.NUM_FT))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_OUVERTS_DEJA_EMIS'),self.qualite.nbFTdejaEmis))
      f.write(rapport_bilan(2,self.fmtLex("LOCUT_NB_FT_REOUVERTS"),len(self.qualite.lstFTreOuvert)))
      f.write(rapport_bilan(2,self.fmtLex("LOCUT_NB_FT_A_CLORE"),len(self.qualite.lstFTaClore)))
      fc.write("%s %d\n"%(self.projet.lexique.entree("LOCUT_NB_FT_OUVERTS_NOUVEAUX"),self.qualite.newNumFT-self.qualite.NUM_FT))
      fc.write("%s %d\n"%(self.projet.lexique.entree("LOCUT_NB_FT_OUVERTS_DEJA_EMIS"),self.qualite.nbFTdejaEmis))
      fc.write("%s %d\n"%(self.projet.lexique.entree("LOCUT_NB_FT_REOUVERTS"),len(self.qualite.lstFTreOuvert)))
      fc.write("%s %d\n"%(self.projet.lexique.entree("LOCUT_NB_FT_A_CLORE"),len(self.qualite.lstFTaClore)))
    else:
      fc.write("\n\n\n")
    f.write("        </table>\n")
    f.write("        <hr/>\n")

    for ft in self.qualite.rapportFT:
      fc.write("ID      : %s\n"%ft['ID'])
      fc.write("TAG     : %s\n"%ft['TAG'])
      fc.write("STATUT  : %s\n"%ft['STATUT'])
      libelle=ft['LIBELLE'].replace('\n','-/-')
      fc.write("LIBELLE : %s\n"%libelle)
    fc.close()

#    if not self.projet.execution=='R':
#      f.write("    </td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign='top'>\n")
#      f.write("    <h2>"+self.fmtLex('LOCUT_FAITS_TECHS')+"</h2>\n")
#      f.write("        <table width=100%>\n")
#      f.write(rapport_bilan(2,self.fmtLex('LOCUT_FAITS_TECHS_QUALIFIES','MOT_CRITIQUE'),self.qualite.couvertureFT[4]))
#      f.write(rapport_bilan(2,self.fmtLex('LOCUT_FAITS_TECHS_QUALIFIES','MOT_BLOQUANT'),self.qualite.couvertureFT[3]))
#      f.write(rapport_bilan(2,self.fmtLex('LOCUT_FAITS_TECHS_QUALIFIES','MOT_MAJEUR'),self.qualite.couvertureFT[2]))
#      f.write(rapport_bilan(2,self.fmtLex('LOCUT_FAITS_TECHS_QUALIFIES','MOT_MINEUR'),self.qualite.couvertureFT[1]))
#      f.write(rapport_bilan(2,self.fmtLex('LOCUT_FAITS_TECHS_NON_QUALIFIES'),self.qualite.couvertureFT[0]))
#      f.write("        </table>\n")
#    else:
    if self.projet.execution in ['R','M']:
      f.write("    </td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign='top'>\n")
      f.write("    <h2>"+self.fmtLex('MOT_CONTROLES')+"</h2>\n")
      if total!=0:
        f.write("        <table width=100%>\n")
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_SUCCES'),n_OK+n_AR,self.fmtLex('MOT_SOIT'),(n_OK+n_AR)/float(total)))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_OK'),n_OK,self.fmtLex('MOT_SOIT'),n_OK/float(total),True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AR'),n_AR,self.fmtLex('MOT_SOIT'),n_AR/float(total),True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_ECHEC'),n_AV+n_PB,self.fmtLex('MOT_SOIT'),(n_AV+n_PB)/float(total)))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_AV'),n_AV,self.fmtLex('MOT_SOIT'),n_AV/float(total),True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NOK'),n_PB,self.fmtLex('MOT_SOIT'),n_PB/float(total),True))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NT'),n_NT,self.fmtLex('MOT_SOIT'),n_NT/float(total)))
        f.write(rapport_bilan(4,self.fmtLex('LOCUT_NB_CTRL_NON_BLOQUANT'),nbCtrlBLOQ,self.fmtLex('MOT_SOIT'),nbCtrlBLOQ/float(total)))
        f.write("        </table>\n")
      f.write("        <hr/>\n")

      f.write("    <h2>"+self.fmtLex('LOCUT_FT_SUR_ACTIONS')+u"</h2>\n")
      f.write("        <table width=100%>\n")
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_CRITIQUES_OUVERTS'),self.qualite.couvertureFT[4]))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_BLOQUANTS_OUVERTS'),self.qualite.couvertureFT[3]))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MAJEURS_OUVERTS'),self.qualite.couvertureFT[2]))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_MINEURS_OUVERTS'),self.qualite.couvertureFT[1]))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_NQ_OUVERTS'),self.qualite.couvertureFT[0]))
      f.write(rapport_bilan(2,self.fmtLex('LOCUT_NB_FT_EMIS_SUSPENDUS'),self.qualite.couvertureFT[5]))
      f.write("        </table>\n")
      f.write("        <hr/>\n")
      f.write("    </td></tr><tr><td valign='top'>\n")

      f.write("    <h2>"+self.fmtLex('LOCUT_FT_SUR_EXIG')+u"</h2>\n")
      f.write("        <table width=100%>\n")
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys())))
      f.write(rapport_bilan_separatrice(3))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES'),self.qualite.couvertureExig[4],None,self.qualite.couvertureExig[4]/divisExig))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS'),self.qualite.couvertureExig[3],None,self.qualite.couvertureExig[3]/divisExig))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS'),self.qualite.couvertureExig[2],None,self.qualite.couvertureExig[2]/divisExig))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_MINEURS'),self.qualite.couvertureExig[1],None,self.qualite.couvertureExig[1]/divisExig))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_EXIG_AVEC_FT_NQ'),self.qualite.couvertureExig[0],None,self.qualite.couvertureExig[0]/divisExig))
      f.write("        </table>\n")
      f.write("        <hr/>\n")
      f.write("    </td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>\n")

      f.write("    <h2>"+self.fmtLex('LOCUT_FT_SUR_FCT')+u"</h2>\n")
      f.write("        <table width=100% valign='top'>\n")
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"]))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys())))
      f.write(rapport_bilan_separatrice(3))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_CRITIQUES'),self.qualite.couvertureFonc[4],None,self.qualite.couvertureFonc[4]/divisFct))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_BLOQUANTS'),self.qualite.couvertureFonc[3],None,self.qualite.couvertureFonc[3]/divisFct))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MAJEURS'),self.qualite.couvertureFonc[2],None,self.qualite.couvertureFonc[2]/divisFct))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_MINEURS'),self.qualite.couvertureFonc[1],None,self.qualite.couvertureFonc[1]/divisFct))
      f.write(rapport_bilan(3,self.fmtLex('LOCUT_TAUX_FCT_AVEC_FT_NQ'),self.qualite.couvertureFonc[0],None,self.qualite.couvertureFonc[0]/divisFct))
      f.write("        </table>\n")

    f.write("        <hr/>\n")
    f.write("    </td></tr></table>\n")
    f.write("</body>\n</html>\n")
    f.close()

    f=open(self.ficFTemis,"a",encoding='utf-8')
    if self.projet.execution=='R':
      # Faits techniques ouverts
      f.write('    <a name="ft"></a><h2>'+self.fmtLex('LOCUT_REFERENCES_FT_OUVERTS')+'</h2>\n')
      for k in [4,3,2,1,0]:
        aucune=True
        if k==0:
            f.write("    <h3>"+self.fmtLex('LOCUT_FAITS_TECHS_NON_QUALIFIES')+"</h3>\n")
        else:
          f.write("    <h3>"+self.fmtLex('LOCUT_FAITS_TECHS')+" '%s'</h3>\n"%self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%k))
        if k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          for ctrl in self.qualite.matTracabilite[k]:
            ((rang,fiche,scenario,_,(action,_),desc,_),numFT,etatFT,_)=ctrl
         #   entite,methode,typo,nature,_,exigGlobales=desc
            if etatFT!='LOCUT_A_CLORE' and numFT is not None:
              aucune=False
              strFT='%06d'%abs(numFT)
              f.write('    %s %s : <a href="%s-%d-%s.html#%s-%s">%s-%s</a><br/>\n'%(fiche.replace("/",":").replace("\\",":"),action,os.path.basename(scenario),rang,fiche.replace("/","_").replace("\\","_"),self.refFT,strFT,self.refFT,strFT))
        if aucune:
          f.write("    <i>%s</i>\n"%self.fmtLex('LOCUT_SANS_OBJET'))

      # Faits techniques re-ouverts
      f.write('    <h2>'+self.fmtLex('LOCUT_REFERENCES_FT_REOUVERTS')+'</h2>\n')
      if len(self.qualite.lstFTreOuvert)==0:
        f.write('    <i>%s</i><br/>\n'%self.fmtLex('LOCUT_SANS_OBJET'))
      else:
        for idx in self.qualite.lstFTreOuvert:
          (fiche,action,numFT,gravite)=idx
          strFT='%06d'%numFT
          f.write('    %s %s : <a href="%s-%d-%s.html#%s-%s">%s-%s</a><br/>\n'%(fiche.replace("/",":").replace("\\",":"),action,os.path.basename(scenario),rang,fiche.replace("/","_").replace("\\","_"),self.refFT,strFT,self.refFT,strFT))

      # Faits techniques a clore
      f.write('    <h2>'+self.fmtLex('LOCUT_REFERENCES_FT_A_CLORE')+'</h2>\n')
      if len(self.qualite.lstFTaClore)==0:
        f.write('    <i>%s</i><br/>\n'%self.fmtLex('LOCUT_SANS_OBJET'))
      else:
        for idx in self.qualite.lstFTaClore:
          (fiche,action,numFT,gravite)=idx
          strFT='%06d'%numFT
          f.write('    %s %s : <a href="%s-%d-%s.html#%s-%s">%s-%s</a><br/>\n'%(fiche.replace("/",":").replace("\\",":"),action,scenario,rang,fiche.replace("/","_").replace("\\","_"),self.refFT,strFT,self.refFT,strFT))

    f.write("</body>\n</html>\n")
    f.close()

    self.MatriceTracabilite(reference,rang)

    # on deplace vers l'emplacement final
    if os.access(self.ficBilanTxt,os.F_OK):
      nngsrc.services.SRV_deplacer(self.ficBilanTxt,os.path.join(self.repRapport,self.projet.execution,'bilan.txt'),timeout=5,signaler=True)

    # On redirige'
    f=open(os.path.join(self.repRapport,self.projet.execution,'HTML','index.html'),"w",encoding='utf-8')
    f.write("<html><head></head><body>\n")
    f.write("<script type='text/javascript'>\n")
    f.write("<!--\n")
    f.write("location.replace('%s/index.html');\n"%self.ficRapport)
    f.write("//-->\n")
    f.write("</script>\n")
    f.write("</body></html>\n")
    f.close()

    # on deplace les rapports dans le repertoire final
    destination=os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport)
    if not os.access(destination,os.F_OK):
      os.mkdir(destination)

    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.html')):
      ficdest=os.path.join(destination,os.path.basename(elem))
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    for elem in ["graphiqueCtrl.png","graphiqueExig.png","graphiqueFonct.png"]:
      ficdest=os.path.join(destination,elem)
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      if os.access(os.path.join(self.repTravail,elem),os.F_OK):
        nngsrc.services.SRV_copier(os.path.join(self.repTravail,elem),ficdest,timeout=5,signaler=True)

    for elem in glob.glob(os.path.join(self.repTravail,'*.png')):
      if not os.path.basename(elem) in ["graphiqueCtrl.png","graphiqueExig.png","graphiqueFonct.png"]:
        ficdest=os.path.join(destination,'..',os.path.basename(elem))
        if os.access(ficdest,os.F_OK):
          nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
        nngsrc.services.SRV_deplacer(elem,os.path.join(destination,'..'),timeout=5,signaler=True)

    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.cr')):
      ficdest=os.path.join(destination,os.path.basename(elem))
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    for elem in self.listeBBCimageInclusion:
      nngsrc.services.SRV_copier(elem,os.path.join(destination,os.path.basename(elem)),timeout=5,signaler=True)

    # Elements d'IHM
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.png')):
      ficdest=os.path.join(destination,os.path.basename(elem))
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    # Elements des ressources a chercher dans le projet
    if os.path.exists(self.projet.CLIENT_LOGO):
      nngsrc.services.SRV_copier(self.projet.CLIENT_LOGO,os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport,"logoClient.png"))
    else:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree("LOCUT_LOGO_CLIENT_ABSENT")%self.projet.CLIENT_LOGO,style="Wr")
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,'nangu.png'),os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport,"logoClient.png"))
    if os.path.exists(self.projet.SOCIETE_LOGO):
      nngsrc.services.SRV_copier(self.projet.SOCIETE_LOGO,os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport,"logoSociete.png"))

    for f in ['vert.png','violet.png','brun.png','cyan.png','orange.png','rouge.png','noir.png','action.png','arrow_up.png','nangu.css','collapse.js','favicon.ico']:
      if os.access(os.path.join(self.repTravail,f),os.F_OK):
        nngsrc.services.SRV_copier(os.path.join(self.repTravail,f),os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport,f),timeout=5,signaler=True)
      else:
        nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport,f),timeout=5,signaler=True)

    num=1
    f="etape%d.png"%num
    while os.access(os.path.join(self.repRessources,f),os.F_OK):
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(destination,f),timeout=5,signaler=True)
      num+=1
      f="etape%d.png"%num

    # Elements des ressources a chercher dans le projet
    for f in ['videMU.html','videIDX.html','videETP.html','nangu.png']:
      nngsrc.services.SRV_copier(os.path.join(self.repRessources,f),os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport,f),timeout=5,signaler=True)
    nngsrc.services.SRV_copier(os.path.join(self.repRessources,'ManuelUtilisateur.pdf'),os.path.join(self.repRapport,self.projet.execution,"HTML",'ManuelUtilisateur.pdf'),timeout=5,signaler=True)

    os.rmdir(os.path.join(self.repTravail,REPERTRAPPORT))

    # Archivage
    if self.projet.ARCHIVAGE:
      if not self.projet.execution in ['X','K']:
        fichier='-'.join(self.ficRapport.split('-')[:-1])
        fichCmplt=os.path.join(self.repRapport,self.projet.execution,"HTML",fichier+".tgz")
        if os.access(fichCmplt,os.F_OK):
          nngsrc.services.SRV_detruire(fichCmplt,timeout=5,signaler=True)
        ficTGZ=tarfile.open(fichCmplt,mode="w:gz",encoding='utf-8')
        ficTGZ.add(os.path.join(self.repRapport,self.projet.execution,"HTML",self.ficRapport),arcname=self.ficRapport)
        if self.projet.execution=='R':
          ficTGZ.add(os.path.join(self.repRapport,self.projet.execution,"HTML",fichier+".png"),arcname=fichier+".png")
        ficTGZ.close()
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ARCHIVE_DANS')%fichCmplt,style="r")

  #-------------------------------------------------------------------------------
  def sousMatriceDirecte(self,f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,decline,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.fmtLex('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Reference de l'action
    if numFT is None:
      f.write('    <tr><td class="matTrac">%s</td>'%(os.path.basename(fiche)+tagFiche+extAction))
    else:
      f.write('    <tr><td class="matTrac"><a href="%s-%d-%s.html#%s-%06d">%s</a></td>'%(os.path.basename(reference),rang,fiche.replace('/','_').replace('\\','_'),self.refFT,numFT,os.path.basename(fiche)+tagFiche+extAction))
    # Methode, typo
    f.write('<td class="matTrac">%s</td>'%fmtCh(methode))
    f.write('<td class="matTrac">%s</td>'%fmtCh(typo))
    # Si mode R, Etat de la FT, contrôleur et date
    if self.projet.execution=='R':
      f.write('<td class="matTrac">%s</td>'%strFT)
    elif self.projet.execution=='C':
      f.write('<td class="matTrac"></td>')
    # Exigence et type d'exigence
    if exigFct=='':
      f.write('<td class="matTrac">-</td>')
      if decline:
        f.write('<td class="matTrac">-</td>')
      f.write('<td class="matTrac">-</td>')
    elif exigFct in dictExigFct:
      f.write('<td class="matTrac">%s</td>'%fmtCh(exigFct))
      if decline:
        if dictExigFct[exigFct]['parent'] is not None:
          f.write('<td class="matTrac">%s</td>'%fmtCh(dictExigFct[exigFct]['parent'][0]))
        else:
          f.write('<td class="matTrac">-/-</td>')
      f.write('<td class="matTrac">%s</td>'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Severite
#    if severite==0:
#      severite=IT(self.projet.lexique.entree("MOT_NON_QUALIFIE"))
#    else:
#      severite=self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%severite)
    f.write('<td class="matTrac">%s</td>'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write('<td class="matTrac">%s</td>'%' '.join(n for n in faitTechCouv))
      else:
        f.write('<td class="matTrac">-</td>')
    f.write('</tr>\n')

  #-------------------------------------------------------------------------------
  def sousMatriceInverse(self,f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,decline,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.fmtLex('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Exigence et type d'exigence
    f.write('    <tr><td class="matTrac">%s</td>'%fmtCh(exigFct))
    if decline and exigFct in dictExigFct:
      if dictExigFct[exigFct]['parent'] is not None:
        f.write('<td class="matTrac">%s</td>'%fmtCh(dictExigFct[exigFct]['parent'][0]))
      else:
        f.write('<td class="matTrac">-/-</td>')
    if exigFct=='': f.write(' & -')
    elif exigFct in dictExigFct:
      f.write('<td class="matTrac">%s</td>'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Reference du contrôle,sa methode,sa typo
    if numFT is None:
      f.write('<td class="matTrac">%s</td>'%(os.path.basename(fiche)+tagFiche+extAction))
    else:
      f.write('<td class="matTrac"><a href="%s-%d-%s.html#%s-%06d">%s</a></td>'%(os.path.basename(reference),rang,fiche.replace('/','_').replace('\\','_'),self.refFT,numFT,os.path.basename(fiche)+tagFiche+extAction))
    f.write('<td class="matTrac">%s</td>'%fmtCh(methode))
    f.write('<td class="matTrac">%s</td>'%fmtCh(typo))
    # Si mode R ou C,le fait technique ou vide
    if self.projet.execution=='R':
      f.write('<td class="matTrac">%s</td>'%strFT)
    elif self.projet.execution=='C':
      f.write('<td class="matTrac"></td>')
    # Severite
#    if severite==0: severite=IT(self.projet.lexique.entree("MOT_NON_QUALIFIE"))
#    else: severite=self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%severite)
    f.write('<td class="matTrac">%s</td>'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write('<td class="matTrac">%s</td>'%' '.join(n for n in faitTechCouv))
      else:
        f.write('<td class="matTrac">-</td>')
    f.write('</tr>\n')

  #-------------------------------------------------------------------------------
  def MatriceTracabilite(self,reference,rang):
    #--- OUVERTURE ET PREPARATION DU FICHIER
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,'matTracabilite%s'%DOCEXTENSION),"w",encoding='utf-8')
    f.write("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n")
    f.write("<html>\n")
    f.write("    <head>\n")
    f.write("        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>\n")
    f.write("        <title>%s</title>\n"%self.fmtLex('LOCUT_MATRICE_TRACABILITE'))
    f.write("        <link rel='stylesheet' href='nangu.css' type='text/css'/>\n")
    f.write("    </head>\n")
    f.write("    <body class='bilans'>\n")
    f.write("    <h1>"+self.fmtLex('LOCUT_MATRICE_TRACABILITE')+"</h1>\n")
    f.write("    <h2><a name='index'></a>"+fmtCh("  Index")+"</h2>\n")
    if self.projet.MATTRAC_NON_CVT:
      if self.projet.PRODUIT_EXIGENCES_TRACEES:
        f.write("    <a href='#lst1'>"+self.fmtLex('LOCUT_NON_COUV_EXIG')+"</a><br>\n")
      if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
        f.write("    <a href='#lst2'>"+self.fmtLex('LOCUT_NON_COUV_FCT')+"</a><br>\n")
    if self.projet.MATTRAC_CTL_EXG:
      f.write("    <a href='#mat1'>"+self.fmtLex('LOCUT_MATRICE_COUV_EXIG')+"</a><br>\n")
    if self.projet.MATTRAC_CTL_FCT:
      f.write("    <a href='#mat2'>"+self.fmtLex('LOCUT_MATRICE_COUV_FCT')+"</a><br>\n")
    if self.projet.MATTRAC_EXG_CTL:
      f.write("    <a href='#mat3'>"+self.fmtLex('LOCUT_MATRICE_INV_COUV_EXIG')+"</a><br>\n")
    if self.projet.MATTRAC_FCT_CTL:
      f.write("    <a href='#mat4'>"+self.fmtLex('LOCUT_MATRICE_INV_COUV_FCT')+"</a><br>\n")
    f.write("    <br>\n")

    nc=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repTravail,'nonCouv.txt'))

    #--- LISTE DE NON COUVERTURE DES EXIGENCES -------------------------------------------------------------------------------
    if self.projet.MATTRAC_NON_CVT:
      if self.projet.PRODUIT_EXIGENCES_TRACEES:
        f.write("    <h2><a name='mat1'></a>"+self.fmtLex('LOCUT_NON_COUV_EXIG')+"</h2><br>\n")
        ind=1
        while nc[ind].find("--Fonctionnalites non couvertes--")!=0:
          f.write("%s"%nc[ind].replace('\n','').replace('\r',''))
          ind+=1
          if nc[ind].find("--Fonctionnalites non couvertes--")!=0:
            f.write(", ")
        f.write("    <br><a href='#index'>index</a>\n")

    #--- LISTE DE NON COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------------------
      if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
        f.write("    <h2><a name='mat1'></a>"+self.fmtLex('LOCUT_NON_COUV_FCT')+"</h2><br>\n")
        ind+=1
        while ind<len(nc):
          f.write("%s"%nc[ind].replace('\n','').replace('\r',''))
          ind+=1
          if ind<len(nc):
            f.write(", ")
        f.write("    <br><a href='#index'>index</a>\n")

    #--- MATRICE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_EXG:
      f.write("    <h2><a name='mat1'></a>"+self.fmtLex('LOCUT_MATRICE_COUV_EXIG')+"</h2><br>\n")
      f.write('    <table class="matTrac">\n')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_EXIGENCES'),self.fmtLex('LOCUT_EXIGENCES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_EXIGENCES'),self.fmtLex('LOCUT_EXIGENCES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        # On parcours les actions de niveau k
        for act in self.qualite.matTracabilite[k]:
          ((rang,fiche,_,tagFiche,(action,_),desc,faitTechCouv),numFT,etatFT,exigences)=act
          _,methode,typo,_,_,exigGlobales=desc
          exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
          faitTechCouv=faitTechCouv.split(',')
          listNumFT=[]
          if numFT is not None: listNumFT.append(numFT)
          while '' in faitTechCouv: faitTechCouv.remove('')
          ficheTagFiche="%s%s"%(fiche,tagFiche)
          if True: #numFT:
            for eg in exigGlobales:
              if eg!='':
                if eg not in ListeExigGlobales:
                  ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                else:
                  precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                  if k>precedk or numFT is not None:
                    if numFT is not None:
                      precednumFT.append(numFT)
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
          exigences=exigences.replace(';',',').replace('\\n',',').split(',')
          while '' in exigences: exigences.remove('')
          for e in exigences:
            self.sousMatriceDirecte(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((rang,fiche,_,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
            self.sousMatriceDirecte(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%fmtCh(self.projet.lexique.entree('LOCUT_EXIG_GLOBALE')))
          else:
            for numFT in listNumFT:
              self.sousMatriceDirecte(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%fmtCh(self.projet.lexique.entree('LOCUT_EXIG_GLOBALE')))
      f.write("    </table><a href='#index'>index</a>\n")

    #--- MATRICE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_FCT:
      f.write("    <h2><a name='mat2'></a>"+self.fmtLex('LOCUT_MATRICE_COUV_FCT')+"</h2><br>\n")
      f.write('    <table class="matTrac">\n')
      # En exécution R ou C on ajoute les colonnes fait tech., controleur et date
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('LOCUT_FONCTIONNALITES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('LOCUT_FONCTIONNALITES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_SEVERITE')))
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        for act in self.qualite.matTracabilite[k]:
          ((rang,fiche,_,tagFiche,(action,_),desc,faitTechCouv),numFT,etatFT,_)=act
          entite,methode,typo,_,_,_=desc
          if action=="act1": #action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            self.sousMatriceDirecte(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,entite,self.projet.fonctionnalites,self.projet.PRODUIT_FONCTIONNALITES_DECLINEES,severite,'')
      f.write("    </table><a href='#index'>index</a>\n")

    #--- MATRICE INVERSE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------
    if self.projet.MATTRAC_EXG_CTL:
      f.write("    <h2><a name='mat3'></a>"+self.fmtLex('LOCUT_MATRICE_INV_COUV_EXIG')+"</h2><br>\n")
      f.write('    <table class="matTrac">\n')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('LOCUT_EXIGENCES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('LOCUT_EXIGENCES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_SEVERITE')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_EXIGENCES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeExig):
        # On parcours les actions de niveau k
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          if k==0: severite=IT(severite)
          for act in self.qualite.matTracabilite[k]:
            ((rang,fiche,_,tagFiche,(action,_),desc,faitTechCouv),numFT,etatFT,exigences)=act
            _,methode,typo,_,_,exigGlobales=desc
            faitTechCouv=faitTechCouv.split(',')
            listNumFT=[]
            if numFT is not None: listNumFT.append(numFT)
            while '' in faitTechCouv: faitTechCouv.remove('')
            ficheTagFiche="%s%s"%(fiche,tagFiche)
            if True: #numFT: # if action == "act1":  # action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
              exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
              for eg in exigGlobales:
                if eg!='':
                  if eg not in ListeExigGlobales:
                    ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                  elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                  else:
                    precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                    if k>precedk or numFT is not None:
                      if numFT is not None and numFT not in precednumFT:
                        precednumFT.append(numFT)
                      ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
            exigences=exigences.replace(';',',').replace('\\n',',').split(',')
            while '' in exigences: exigences.remove('')
            if e in exigences:
              self.sousMatriceInverse(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((rang,fiche,_,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
            self.sousMatriceInverse(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
          else:
            for numFT in listNumFT:
              self.sousMatriceInverse(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%self.fmtLex('LOCUT_EXIG_GLOBALE'))
      f.write("    </table><a href='#index'>index</a>\n")

    #--- MATRICE INVERSE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------
    if self.projet.MATTRAC_FCT_CTL:
      f.write("    <h2><a name='mat4'></a>"+self.fmtLex('LOCUT_MATRICE_INV_COUV_FCT')+"</h2><br>\n")
      f.write('    <table class="matTrac">\n')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('LOCUT_FONCTIONNALITES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('LOCUT_FAIT_TECH'),
                   self.fmtLex('MOT_SEVERITE'),self.fmtLex('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('LOCUT_FONCTIONNALITES_PARENTES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_SEVERITE')))
        else:
          f.write('    <tr><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th><th class="matTrac">%s</th></tr>\n'%
                  (self.fmtLex('MOT_FONCTIONNALITES'),self.fmtLex('MOT_METHODE'),
                   self.fmtLex('MOT_CONTROLE'),self.fmtLex('MOT_METHODE'),self.fmtLex('MOT_TYPE'),
                   self.fmtLex('MOT_SEVERITE')))
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeFct):
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          if k==0: severite=IT(severite)
          listeFCT=[]
          for act in self.qualite.matTracabilite[k]:
            ((rang,fiche,_,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
            entite,methode,typo,_,_,_=desc
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            if e==entite and not fiche in listeFCT:
              listeFCT.append(fiche)
              self.sousMatriceInverse(f,reference,rang,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.fonctionnalites,self.projet.PRODUIT_FONCTIONNALITES_DECLINEES,severite,'')
      f.write("    </table><a href='#index'>index</a>\n")

    f.write("    </body>\n</html>\n")
    f.close()

  #-----------------------------------------------------------------------------------
  def GestionConfirmationFT(self,etatAction,disp,FTconfirme,FTreserve,etatFT):
    if etatAction!="NOK":
      etatAction=disp[2]
      if etatAction=="NOK" and self.projet.CONFIRMATION_FT=="oui" and not FTconfirme:
        disp=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
        etatAction=disp[2]
    return etatAction,disp

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_2colonnes_debut(self,clfAction,bilan):
    if self.projet.execution=='P':
      texte="    <table class='raptester'>\n"
      texte+="        <tr><th class='symbact'>%s</th><th class='nobd'>%s</th></tr>\n"%(Atester,fmtCh(clfAction))
    else:
      texte="    <table class='raptester'>\n"
      texte+="        <tr><th class='symbact'>%s</th><th>%s</th><th class='rapbilan'>%s</th></tr>\n"%(Atester,fmtCh(clfAction),bilan)
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_2colonnes_milieu(self,inRapport,valeur,style,bilan='',suspendu=False,raison=None,cssBilan=''):
#        if type(params['VAL_ACTION'])==list:
#          com,_=params['VAL_ACTION'][0]
#          val=self.fmtLex(com)
#        else:
#          val=fmtCh(params['VAL_ACTION'],2)
    if self.projet.execution=='P':
      texte="        <tr><td class='symbact'></td><td class='tt'>%s</td></tr>\n"%valeur
    else:
      self.ancreResultat(bilan,suspendu,inRapport)
      if type(valeur)==str:
        valeur=fmtCh(valeur,2)
        texte="        <tr><td class='symbact'></td><td class='tt'>%s</td>"%valeur
        if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
        else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>\n"%(cssBilan,bilan)
      else:
        texte=''
        for e in valeur:
          if type(e) is str:
            texte+="        <tr><td class='symbact'></td><td>%s</td>"%fmtStyle(style,e)
          else:
            c,v,_=e
            if c=='LOCUT_CLIQUER_DEPLIER':
              if v[:3]=='Txt':
                texte+="        <tr><td class='symbact'></td><td><button type='button' class='collapsible'>%s</button>\n"%self.fmtLex(c)
                texte+="        <div class='content'>\n"
                f=open(v[4:],"r")
                contenu=f.read()
                f.close()
                if contenu=='': contenu=self.projet.lexique.entree('LOCUT_FICHIER_VIDE')
                texte+=fmtCh(contenu)
                texte+="\n        </div></td>"
                self.collapseJS=True
            elif c=='LOCUT_SUIVRE_LIEN':
              texte+="        <tr><td class='symbact'></td><td>%s</td>"%self.fmtLex(c)
            else:
              texte+="        <tr><td class='symbact'></td><td>%s %s</td>"%(self.fmtLex(c),fmtStyle(style,v))
          if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
          else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>\n"%(cssBilan,bilan)
          cssBilan=''
          bilan=''
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_2colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='raptester'>\n"
        texte+="        <tr><td class='symbact'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="    </table>\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  def rapport_2colonnes(self,inRapport,clfAction,valAction,styAction,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    texte =self.rapport_2colonnes_debut(clfAction,self.fmtLex('MOT_BILAN'))
    texte+=self.rapport_2colonnes_milieu(inRapport,valAction,styAction,bilan,suspendu,raison,cssBilan)
    texte+=self.rapport_2colonnes_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  def rapport_3colonnes_debut(self,clfAction,clfConstat,bilan):
    texte="    <table class='raptester'>\n"
    texte+="        <tr><th class='symbact'>%s</th>"%Atester
    texte+="<th>%s</th>"%clfAction
    if self.projet.execution!='P':
      texte+="<th class='rapretour'>%s</th>"%clfConstat
      texte+="<th class='rapbilan'>%s</th></tr>"%bilan
    texte+="\n"
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  def rapport_3colonnes_action(self,valAction,styAction):
    texte="        <tr><td class='symbact'></td><td>"
    for e in range(len(valAction)):
      texte+="<font class='%s'>%s</font> "%(styAction[e],fmtCh(valAction[e]))
    if self.projet.execution!='P':
      if self.projet.execution=='R':
        texte+="</td><td class='rapretour'></td><td class='rapbilan'>"
    texte+="</td></tr>\n"
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  def rapport_3colonnes_milieu(self,inRapport,clfAttendu,valAttendu,valConstat,styConstat,bilan='',suspendu=False,raison=None,cssBilan=''):
    texte="        <tr><td class='symbact'></td>"
    texte+="<td>%s %s</td>"%(clfAttendu,TT(valAttendu))
    if self.projet.execution!='P':
      if self.projet.execution=='R': self.ancreResultat(bilan,suspendu,inRapport)
      if type(valConstat)==str:
        texte+="<td class='rapretour'>%s</td>"%valConstat
        if suspendu: texte+="<td class='rapbilan'><font class='suspendu'>&nbsp;SUS&nbsp;</font></td></tr>\n"
        else: texte+="<td class='rapbilan'><font class='%s'>&nbsp;%s&nbsp;</font></td></tr>"%(cssBilan,bilan)
 #     else: #--liste--
 #       texte=''
 #       for v in valConstat:
 #         texte ="        <tr><td class='symbact'></td>"
 #         texte+="<td class='tt'>%s</td>"%v
 #         texte+="<td class='tt'>%s</td>"%valAttendu
 #         texte+="<td class='rapbilan'><font class='%s'> %s </font></td></tr>\n"%(cssBilan,bilan)
 #         cssBilan=''
 #         bilan=''
    texte+="\n"
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_3colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='raptester'>\n"
        texte+="        <tr><td class='symbact'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="    </table>\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #                                                                <-- fin... plus tard
  def rapport_3colonnes(self,inRapport,clfAction,valact,styact,clfConstat,valConstat,styConstat,clfAttendu,valAttendu,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,vvaleur=''):
    texte =self.rapport_3colonnes_debut(clfAction,clfConstat,self.fmtLex('ABR_BILAN'))
    if len(valact)>0:
      texte+=self.rapport_3colonnes_action(valact,styact)
    vBilan=bilan
    for num in range(len(clfAttendu)):
      texte+=self.rapport_3colonnes_milieu(inRapport,clfAttendu[num],valAttendu[num],valConstat[num],styConstat,vBilan,suspendu,raison,cssBilan)
      if num==0:
        cssBilan=''
        vBilan=''
    texte+=self.rapport_3colonnes_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  def Valid_BilanMode(self,fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,execution):
    CTL_MODE=params['CTL_MODE']
    CTL_COMMENT=params['CTL_COMMENT'].split('\n')
    CTL_BOUCLE=params['CTL_BOUCLE']
    CTL_FT=params['CTL_FT']
    CTL_EXIGENCES=params['CTL_EXIGENCES']
    CTL_INDEX=self.TexteConversion(str(params['CTL_INDEX']),'')
    CTL_ID=params['CTL_ID']
    CTL_CONDITION=params['CTL_CONDITION']
    CTL_FTQUALIF=params['CTL_FTQUALIF']
    inRapport=params['CTL_RAPPORT']

    # Raz du systeme d'ancres
    if params['CTL_INDEX'].find(']')>0:
      self.actAncre=params['CTL_INDEX'].split(']')[1].split('[')[0]
    else:
      self.actAncre=params['CTL_INDEX']
    self.strAncre=''
    self.indexAncre=''
    self.valAncre=self.constantes['STATUS_OK']

    #----------------------------------
    # Bandeau de description de l'action
    #
    #--1- Item et Description ---
    sautBandeau=True
    ftech=CTL_FTQUALIF

    resDocument=self.rapport_description_debut()
    for l in range(len(CTL_COMMENT)):
      if l==0:
        resDocument+=self.rapport_description_milieu(self.TexteConversion(CTL_COMMENT[l]),texte="*ANCRE*",item=CTL_INDEX,ft=ftech)
        if ftech:
          ftech=None
      else:
        resDocument+=self.rapport_description_milieu(self.TexteConversion(CTL_COMMENT[l]),ft=ftech)
    #
    #--2- Ajout des exigences éventuelles ---
    if CTL_EXIGENCES!='':
      resDocument+=self.rapport_description_milieu(exigence=indexerExigFT(CTL_EXIGENCES,True),ft=ftech)
      sautBandeau=False
    #
    #--3- Ajout des faits techniques éventuels ---
    if CTL_FT!='' and CTL_FT is not None :
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_FAITS_TECHS')+indexerExigFT(CTL_FT)),ft=ftech)
      sautBandeau=False
    #
    #--4- Identifiant de condition ---
    if CTL_ID!='':
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_IDENTIFIANT')+CTL_ID),ft=ftech)#,2,False))
      sautBandeau=False
    #
    if CTL_CONDITION is not None:
      resDocument+=self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_CONDITION_EXEC')+CTL_CONDITION),ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--5- Identifiant de boucle ---
    if CTL_BOUCLE is not None:
      resDocument+=self.rapport_description_milieu(petit=self.fmtLex('LOCUT_BOUCLE_ACTION')+CTL_BOUCLE[0],ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--6- Fin du bandeau ---
    if sautBandeau:
      resDocument+=self.rapport_description_milieu(ft=ftech)
    resDocument+=self.rapport_description_fin()
    #----------------------------------

    if self.projet.execution=='S':
      if 'RES_COMMENT' in params and params['RES_COMMENT'] is not None:
        resDocument+="    <table>\n"
        if type(params['RES_COMMENT'])==str:
          resDocument+="      <tr><td>%s %s</td><td>%s</td></tr>\n"%(Atester,fmtCh(self.projet.lexique.entree('LOCUT_RESULTAT_ATTENDU')),fmtCh(params['RES_COMMENT']))
        else:
          for e in range(len(params['RES_COMMENT'])):
            if e==0: resDocument+="      <tr><td>%s %s</td>"%(Atester,fmtCh(self.projet.lexique.entree('LOCUT_RESULTAT_ATTENDU')))
            else: resDocument+="      <tr><td></td>"
            resDocument+="<td>%s</td></tr>\n"%fmtCh(params['RES_COMMENT'][e])
        resDocument+="    </table>\n"

  # Rapport Bilan de l'action
  # Mode 1,2              CAHIER/RAPPORT                                                      PLAN
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  # Mode 1,3,1
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   3  |> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #      |  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   1  | CLF_RAPPORT                                        |     1  | CLF_RAPPORT                                        |
  #      | VAL_RAPPORT                                        |        |                                                    |
  #      +----------------------------------------------------+        +----------------------------------------------------+
    if self.projet.execution!='S':
      # Mode niveau 1 à 1
      clef=self.fmtLex(params['CLF_COMMENT'])
      valeur=fmtCh(params['VAL_COMMENT'],2)
      substit=[fmtCh(x) for x in params['SUB_COMMENT']]
      style=params['STY_COMMENT']
      lien=params['LNK_COMMENT']
      resDocument+=self.rapport_contenuMode(clef,valeur,substit,style,lien)
      # Mode niveau 2 à 1
      if len(CTL_MODE)>1 and CTL_MODE[1]=='1':
        clef=self.fmtLex(params['CLF_COMPLEMENT'])
        valeur=fmtCh(params['VAL_COMPLEMENT'],2)
        substit=[fmtCh(x) for x in params['SUB_COMPLEMENT']]
        style=params['STY_COMPLEMENT']
        lien=params['LNK_COMPLEMENT']
        liste=params['LST_COMPLEMENT']
        if len(liste)>0:
          resDocument+=self.rapport_contenuMode_deb(clef)
          resDocument+=self.rapport_contenuMode_mil(valeur,substit,style,lien=lien)
          for c,v in liste:
            if type(v)==list:
              resDocument+=self.rapport_contenu_mil(self.fmtLex(c),v)
            else:
              resDocument+=self.rapport_contenu_mil(self.fmtLex(c),fmtCh(v))
          resDocument+=self.rapport_contenu_fin()
      # Mode niveau 3 à 2
      if len(CTL_MODE)>2 and CTL_MODE[2]=='2':
        clf=params['CLF_ACTION']
        val=params['VAL_ACTION']
        sty=params['STY_ACTION']
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
          self.rapport_IndexAncre()
        elif execution=='C':
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty)
        elif execution=='P':
          resDocument+=self.rapport_2colonnes(inRapport,clf,val,sty)
      # Mode niveau 3 à 3
      if len(CTL_MODE)>2 and CTL_MODE[2]=='3':
        clfact=fmtCh(params['CLF_ACTION'])
        valact=params['VAL_ACTION']
        styact=params['STY_ACTION']
        clfcst=self.fmtLex(params['CLF_CONSTAT'])
        valcst=fmtCh(params['VAL_CONSTAT'])#,2
        stycst=params['STY_CONSTAT']
        clfatt=[self.fmtLex(x) for x in params['CLF_ATTENDU']]
        valatt=fmtCh(params['VAL_ATTENDU'])#,2
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
          self.rapport_IndexAncre()
        elif execution=='C':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt)
        elif execution=='P':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt)
      # Mode niveau 4 à 1
      if len(CTL_MODE)>3 and CTL_MODE[3]=='1':
        clef=self.fmtLex(params['CLF_RAPPORT'])
        valeur=params['VAL_RAPPORT']
        valeur=fmtCh(valeur)#,alaligne=2,complet=True)
        style=params['STY_RAPPORT']
        resDocument+=self.rapport_contenuMode(clef,valeur,[],style,bouton=True)
      # Mode niveau 4 à 3
      if len(CTL_MODE)>4 and CTL_MODE[4]=='3':
        clfact=fmtCh(params['CLF2_ACTION'])
        clfcst=self.fmtLex(params['CLF2_CONSTAT'])
        valcst=[fmtCh(x) for x in params['VAL2_CONSTAT']]#,2)
        stycst=params['STY2_CONSTAT']
        clfatt=[fmtCh(x) for x in params['CLF2_ATTENDU']]
        valatt=[fmtCh(x) for x in params['VAL2_ATTENDU']]#,2)
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL2_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)
        elif execution=='P':
          resDocument+=self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)

    #----------------------------------
    # Remplacement de l'ancre
    resDocument=resDocument.replace("*ANCRE*",self.strAncre)
#    resDocument+="</table>ici\n"

    if numFT and params['CTL_RAPPORT']!=-1 and params['CTL_FTTQ']: # ... et boucle tant que ...
      (_,_,_,_,action,_,_)=fiche
      TAGaction="%s-%s"%(params['CTL_TAG'],action)
      if params['CTL_FTQUALIF']==0:
        txtqualif=''
      else:
        txtqualif=' (%s)'%self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%params['CTL_FTQUALIF'])
      if etatAction=="NOK":
        if FTconfirme==True or FTconfirme is None and (self.projet.execution=='R'):
          if numFT>=0:
            strFT='%06d'%numFT
            resDocument+='    <font class="ft">%s <a href="FTemis.html#ft" name="%s-%s">%s-%s%s</a></font>\n'%(self.fmtLex('LOCUT_FAIT_TECH'),fmtCh(self.refFT),strFT,fmtCh(self.refFT),strFT,txtqualif)
          else:
            strFT='%06d'%-numFT
            resDocument+='    <font class="ft">%s <a href="FTemis.html#ft" name="%s-%s">%s-%s%s (%s)</a></font>\n'%(self.fmtLex('LOCUT_FAIT_TECH'),fmtCh(self.refFT),strFT,fmtCh(self.refFT),strFT,txtqualif,self.fmtLex(etatFT))
      elif etatAction=="OK":
       # if TAGaction in self.tagActFT:
       #   ftbase=self.baseFT[self.tagActFT[TAGaction]]
          if etatFT=='LOCUT_A_CLORE':
       #     if projet.rapportManuel:
       #       SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,u"Fait Technique à clore : [FT#%06d]"%(int(ftbase['ID'])),style="W")
       #     else:
       #       libelleFT=u"[FT#%06d à clore]"%(int(ftbase['ID']))
            strFT='%06d'%numFT
            resDocument+='    <font class="ft">%s <a href="FTemis.html#ft" name="%s-%s">%s-%s</a> %s%s</font>\n'%(self.fmtLex('LOCUT_FAIT_TECH'),fmtCh(self.refFT),strFT,fmtCh(self.refFT),strFT,self.fmtLex(etatFT),txtqualif)

    #----------------------------------
    # Fin et export
    resDocument+="    <hr/>\n"
    return resDocument

  #-------------------------------------------------------------------------------
  def EtapeDeTest(self,pEtape,pNum):
    pEtape=pEtape.split('|')
    niveau=pEtape[0]
    etape=pEtape[1]
    if etape[-1]!='.':etape+='.'
    libelle=str(pNum)
    if len(pEtape)==3:
      libelle=pEtape[2][:-1]

    if os.access(self.ficEtpRapportFiche, os.F_OK):
      fichier=os.path.basename(self.ficEtpRapportFiche)[4:]
      f=open(self.ficEtpRapportFiche, "a",encoding='utf-8')
      espace="&nbsp;&nbsp;"*int(niveau)
      f.write("        <a target='mainFrame' href='%s#etp%d'>%s<img src='etape%s.png' width='16px' border='0'>%s</a><br/>\n"%(fichier,pNum,espace,niveau,libelle))
      f.close()

    resDocument="    <table class='rapetape'>\n"
    resDocument+="        <tr>\n"
    resDocument+="        <td><a name='etp%d'><img src='etape%s.png'/></a></td><td class='rapetape'>%s</td>\n"%(pNum,niveau,etape)
    resDocument+="        </tr>\n"
    resDocument+="    </table>\n"
    return resDocument

#-------------------------------------------------------------------------------
# FIN
