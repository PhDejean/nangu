#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,os.path,xml
import xml.dom.minidom

try:
  from colorama import *
  init(autoreset=True)
  COLORAMA=True
except:
  print(u"RESTRICTION (servicesXML) : Module 'Colorama' non installé")
  print(u"              => Les affichages ne seront pas mis en couleur.")
  COLORAMA=False

if COLORAMA:
  FRED    = Fore.RED
  FGREEN  = Fore.GREEN
  FYELLOW = Fore.YELLOW
  FWHITE  = Fore.WHITE
  BBLACK  = Back.BLACK
  SBRIGHT = Style.BRIGHT
  SNORMAL = Style.NORMAL
else:
  FRED    = ''
  FGREEN  = ''
  FYELLOW = ''
  FWHITE  = ''
  BBLACK  = ''
  SBRIGHT = ''
  SNORMAL = ''

#-----------------------------------------------------------------------------------
def SRVxml_parser(fichier):
#  print "DBG-SRVxml_parser>",fichier
  if not os.path.exists(fichier): return None
  fXml=open(fichier,'r',encoding='utf8')
  #try:
  XML=xml.dom.minidom.parse(fXml)
  racine=XML.documentElement
  #except:
  #  racine=None
  fXml.close()
  return racine

#-----------------------------------------------------------------------------------
def SRVxml_recupValAttribut(racine,nomAttribut,valDefaut=''):
  if not racine.hasAttribute(nomAttribut):
    return valDefaut
  return racine.getAttribute(nomAttribut)

#-----------------------------------------------------------------------------------
def SRVxml_modifValAttribut(racine,nomAttribut,valAttribut):
  if racine.hasAttribute(nomAttribut):
    rootattr=racine.getAttributeNode(nomAttribut)
    rootattr.nodeValue=valAttribut

#-----------------------------------------------------------------------------------
#def SRV_recupValBaliseXML(racine,nomBalise):
#  # Verification que le nom de la balise soit bien celui de l'element
#  val=''
#  if racine.nodeName==nomBalise:
#    for i in racine.childNodes:
#      if i.nodeType==i.TEXT_NODE:
#        # Recuperation de la valeur de la balise
#        val=i.nodeValue
#    return val
#  # Erreur, la balise 'nomBalise' n'est pas l'element 'racine'
#  return None

#-----------------------------------------------------------------------------------
def SRVxml_recupTexteNoeud(racine,valDefaut=''):
  val=''
  if racine:
    for i in racine.childNodes:
      if i.nodeType==i.TEXT_NODE:
        val=i.nodeValue
    return val
  return valDefaut

#-----------------------------------------------------------------------------------
def SRVxml_modifTexteNoeud(racine,texte):
  for nd in racine.childNodes:
    if nd.nodeType==nd.TEXT_NODE:
      nd.nodeValue=texte

#-----------------------------------------------------------------------------------
#def SRV_getNoeudsXML(racine,nomBalises):
#  listeNoeuds=[]
#  for tag in nomBalises:
#    noeuds=racine.getElementsByTagName(tag)
#    listeNoeuds.append(noeuds)
#  return listeNoeuds
#-----------------------------------------------------------------------------------
def SRVxml_recupTousNoeuds(racine,nomBalise):
  listeFils=SRVxml_recupNoeudFils(racine)
  listeNoeuds=[]
  for nd in listeFils:
    if nd.nodeName==nomBalise:
      listeNoeuds.append(nd)
  return listeNoeuds

#-----------------------------------------------------------------------------------
def SRVxml_recupNoeudFils(racine):
  liste=[]
  for nd in racine.childNodes:
    if nd.nodeType==nd.ELEMENT_NODE:
      liste.append(nd)
  return liste

#-----------------------------------------------------------------------------------
def _recupNoeud(racine,nomBalise):
  if racine is not None:
    for nd in racine.childNodes:
      if nd.nodeType==nd.ELEMENT_NODE:
        if nd.nodeName==nomBalise:
          return nd
  return None

#-----------------------------------------------------------------------------------
def SRVxml_recupNoeud(racine,nomBalise):
  noeud=racine
  nomBalise=nomBalise.split(',')
  for balise in nomBalise:
    noeud=_recupNoeud(noeud,balise)
    if noeud is None:
      return None
  return noeud

#-----------------------------------------------------------------------------------
def SRVxml_recupNomNoeud(racine):
  return racine.nodeName

#-----------------------------------------------------------------------------------
# Cree le document
#-----------------------------------------------------------------------------------
def SRVxml_creerDocument():
  return xml.dom.minidom.Document()

#-----------------------------------------------------------------------------------
# Cree un noeud
#-----------------------------------------------------------------------------------
def SRVxml_creerNoeud(comment=None,balise=None,texte=None,frere=None,pere=None):
  newdoc=xml.dom.minidom.Document()
  if comment:
    noeud=newdoc.createComment(comment)
  elif texte:
    noeud=newdoc.createElement(balise)
    ndText=newdoc.createTextNode(texte)
    noeud.appendChild(ndText)
  else:
    noeud=newdoc.createElement(balise)
  if pere:
    if frere:
      pere.insertBefore(noeud,frere)
    else:
      pere.appendChild(noeud)
  return noeud

#-----------------------------------------------------------------------------------
# Ajoute un attribut à un noeud
#-----------------------------------------------------------------------------------
def SRVxml_ajoutAttribut(racine,attribut,valeur):
  newdoc=xml.dom.minidom.Document()
  rootattr=newdoc.createAttribute(attribut)
  rootattr.nodeValue=valeur
  racine.setAttributeNode(rootattr)

#-----------------------------------------------------------------------------------
pretty_print = lambda f: '\n'.join([line for line in f.toprettyxml(indent=' '*2).split('\n') if line.strip()])
def SRVxml_print(racine):
  #print("DBG>",170,type(racine))
  return pretty_print(racine)

#---------------------------------------------------------------------------------
