#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------
import os,time,math

#from services import *

#-------------------------------------------------------------------------------
def LireContenu(fichier):
  contenu=[]
  if os.access(fichier, os.F_OK):
    f=open(fichier,"r")
    contenu=f.read()
    f.close()
    contenu=contenu.replace('\r','').split('\n')
    if contenu[-1]=='':
      contenu=contenu[:-1]
  return contenu

#-------------------------------------------------------------------------------
def TABLEgenerePrologue(donneeDAT,listeCriteres,listeMesures=None):
  fTabComplete=open(donneeDAT,"w")
  fTabComplete.write("#\n# %s \n#\n"%donneeDAT)
  ligne=' '.join(listeCriteres)
  if listeMesures:
    ligne+=" %s"%(' '.join(listeMesures))
  fTabComplete.write("# %s\n"%ligne)
  fTabComplete.write("#%s\n"%('-'*(len(ligne)+2)))
  fTabComplete.close()

#-------------------------------------------------------------------------------
def TABLEgenereEpilogue(donneeDAT):
  fTabComplete=open(donneeDAT,"a")
  fTabComplete.write("#%s\n"%('-'*6))
  fTabComplete.write("# Fin\n")
  fTabComplete.close()

#-------------------------------------------------------------------------------
def TABLElireFichier(fichierDonnees):
  contenu=LireContenu(fichierDonnees)
  contenuDAT=[]
  ligneMesures=contenu[3].split(' ')[1:]
  for numLigne in range(len(contenu)):
    if contenu[numLigne][0]!='#':
      contenuDAT.append(contenu[numLigne].split(' '))
  return contenuDAT,ligneMesures

#-------------------------------------------------------------------------------
def TABLEextraire(contenuTab,champs,reels=True):
  contenuDAT,ligneMesures=contenuTab
  valeursChamps=[]
  for n in range(len(champs)):
    valeurs=[]
    idxCourbe=ligneMesures.index(champs[n])
    for ligne in contenuDAT:
      if reels:
        valeurs.append(float(ligne[idxCourbe]))
      else:
        valeurs.append(ligne[idxCourbe])
    valeursChamps.append(valeurs)
  return valeursChamps

#===============================================================================
# FIN
