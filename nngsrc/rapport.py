#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,glob

import nngsrc.graphes
import nngsrc.services
import nngsrc.rapportCSV
import nngsrc.rapportHTML
import nngsrc.rapportTEX
import nngsrc.rapportTXT

idxHTML=0
idxTEX=1

#-------------------------------------------------------------------------------
class Rapport:
  csv=None
  html=None
  xml=None
  tex=None
  txt=None
  projet=None
  bilan=None
  editRapport=True

  #-------------------------------------------------------------------------------
  def __init__(self,IDcahier,constantes,repTravail,repRessources,repRapport,projet,qualite,version,editRapport=True,bilan=None):
    self.editRapport=editRapport
    # creation des repertoires destination
    if not os.path.exists(repRapport):
      os.mkdir(repRapport)
    if not os.path.exists(os.path.join(repRapport,projet.execution)):
      os.mkdir(os.path.join(repRapport,projet.execution))
    if not os.path.exists(repTravail):
      os.mkdir(repTravail)
    # accessibilite des elements
    for f in os.listdir(repRapport):
      os.chmod(os.path.join(repRapport,f),0o777)
    for f in os.listdir(repTravail):
      os.chmod(os.path.join(repTravail,f),0o777)
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.ficNonList=os.path.join(repTravail,'nonList.txt')
    self.repTravail=repTravail
    self.repRapport=repRapport
    self.repRessources=repRessources
    self.listePgnIHM={}

    # Reference et Fichier du rapport
    self.ficRapportGenerique,self.ficRapport=self.projet.GenereReference()
#    self.ficRapport=formateChaine(self.ficRapport) # Vers HTML ?
    if self.ficRapport is None:
      self.ficRapport=self.formateLexique("LOCUT_SANS_REFERENCE")%''

    self.qualite.newNumFT=self.qualite.NUM_FT
    self.bilan=({'vert':0,'violet':0,'orange':0,'rouge':0,'noir':0,'actions':0})

    # - Rapport CSV --
    if self.editRapport:
      self.csv=nngsrc.rapportCSV.CSV(repTravail,repRessources,repRapport,self.ficRapport,projet,qualite,version,bilan,constantes,IDcahier)
    # - Rapport HTML --
    self.html=nngsrc.rapportHTML.HTML(repTravail,repRessources,repRapport,self.ficRapport,projet,qualite,version,bilan,constantes,IDcahier)
    # - Rapport LaTeX --
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex=nngsrc.rapportTEX.TEX(repTravail,repRessources,repRapport,self.ficRapport,projet,qualite,version,bilan,constantes,IDcahier)
    # - Rapport Texte en ligne --
    if self.editRapport and self.projet.RAPPORT_EN_LIGNE=="oui" :
      self.txt=nngsrc.rapportTXT.TXT(repTravail,repRessources,repRapport,self.ficRapport,projet,qualite,version,bilan,constantes,IDcahier)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Feuille de route
  #-------------------------------------------------------------------------------
  def Campagne_FeuilleRouteDeroulement(self,Ref,CR):
    self.html.Campagne_FeuilleRouteDeroulement(Ref,CR)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Campagne_FeuilleRouteDeroulement(Ref,CR)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Recuperation du bilan des FT
  #-------------------------------------------------------------------------------
  def Campagne_RecupereBilanFT(self,Ref):
    self.html.Campagne_RecupereBilanFT(Ref)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Campagne_RecupereBilanFT(Ref)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Couverture des exigences
  #-------------------------------------------------------------------------------
  def Campagne_CouvertureExigences(self,MatExigences,fichierGrExig):
    self.html.Campagne_CouvertureExigences(MatExigences,fichierGrExig)
    self.csv.Campagne_CouvertureExigences(MatExigences,fichierGrExig)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Campagne_CouvertureExigences(MatExigences,fichierGrExig)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanFTtodo(self,fichierO,fichierR):
    self.html.Campagne_BilanFTtodo(fichierO,fichierR)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Campagne_BilanFTtodo(fichierO,fichierR)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Exigences
  #-------------------------------------------------------------------------------
  def Campagne_BilanExigencesControles(self,fichierGrExig,ficoutGrExig,fichierGrCont,ficoutGrCont):
    self.html.Campagne_BilanExigencesControles(fichierGrExig,ficoutGrExig,fichierGrCont,ficoutGrCont)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Campagne_BilanExigencesControles(fichierGrExig,ficoutGrExig,fichierGrCont,ficoutGrCont)

  #-------------------------------------------------------------------------------
  # Fin du bilan de campagne
  #-------------------------------------------------------------------------------
  def Campagne_Terminer(self,campagne):
    self.html.Campagne_Terminer(campagne)
    self.csv.Campagne_Terminer(campagne)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui":
      self.tex.Campagne_Terminer(campagne)

  #-------------------------------------------------------------------------------
  # Initialisation du chapitre
  #-------------------------------------------------------------------------------
  def Valid_InitChapitre(self,chapitre):
    self.html.Valid_InitChapitre(chapitre)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Valid_InitChapitre(chapitre)

  #-------------------------------------------------------------------------------
  # mise a jour de la puce de section
  #-------------------------------------------------------------------------------
  def Valid_TerminerChapitre(self,sousreference,TestPath,resultat,criticiteSection,fin=True):
    self.html.Valid_TerminerChapitre(sousreference,TestPath,resultat,criticiteSection)
    if fin:
      if self.editRapport and self.projet.RAPPORT_TEX=="oui":
        self.tex.Valid_TerminerChapitre(sousreference,TestPath,resultat)

  #-------------------------------------------------------------------------------
  # Initialisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_InitRapportFiche(self,rapportActions,reference,rang,fiche,tagFiche,fichePath,txtLoop,nbaffloop):
    self.html.Valid_InitRapportFiche(rapportActions,reference,rang,fiche,tagFiche,fichePath,txtLoop,nbaffloop)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Valid_InitRapportFiche(reference,rang,fiche,tagFiche,fichePath,txtLoop,nbaffloop)

  #-------------------------------------------------------------------------------
  # Finalisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_TerminerRapportFiche(self,result,criticite,nbactions,execute_fin,ficRapportFiche,rapportActions):
    # - Rapport LaTeX --
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      reference,rang,fiche=ficRapportFiche
      self.tex.Valid_TerminerRapportFiche(self.html,result,criticite,execute_fin,"%s-%s-%s"%(reference,rang,fiche),rapportActions)
    # - Rapport Texte en ligne --
    if self.editRapport and self.projet.RAPPORT_EN_LIGNE=="oui" :
      self.txt.Valid_TerminerRapportFiche(self.html,result,criticite,execute_fin,ficRapportFiche,rapportActions)
    self.bilan[nngsrc.services.SRV_imageResultat(result,self.constantes)]+=1
    self.bilan['actions']+=nbactions

  #-------------------------------------------------------------------------------
  # Gestion des boucles
  #-------------------------------------------------------------------------------
  def Valid_DebutBoucle(self,resultat,num,ident,condition,varList,execution):
    if execution=='R':ident=''
    (bilanHTML,bilanTEX)=resultat
    bilanHTML=self.html.Valid_DebutBoucle(num,ident,condition,varList,execution)+bilanHTML
    if self.tex:
      return bilanHTML,self.tex.Valid_DebutBoucle(num,ident,condition,varList,execution)+bilanTEX
    else:
      return bilanHTML,bilanTEX

  #-------------------------------------------------------------------------------
  def Valid_FinBoucle(self,resultat,num,execution):
    (bilanHTML,bilanTEX)=resultat
    bilanHTML=self.html.Valid_FinBoucle(num,execution)+bilanHTML
    if self.tex:
      return bilanHTML,bilanTEX+self.tex.Valid_FinBoucle(num,execution)
    else:
      return bilanHTML,bilanTEX

  #-------------------------------------------------------------------------------
  # Gestion des parties
  #-------------------------------------------------------------------------------
  def Valid_DebutPartie(self,sousreference,separateurSuffixe,soustitre,scenario,typePRJsimple,repScenarios):
    self.html.Valid_DebutPartie(sousreference,separateurSuffixe,soustitre,scenario,repScenarios)
    if self.editRapport:
      if self.projet.RAPPORT_TEX=="oui":
        if typePRJsimple: self.tex.Valid_DebutPartie(None,None,None,None,None)
        else: self.tex.Valid_DebutPartie(sousreference,separateurSuffixe,soustitre,scenario,repScenarios)

  #-------------------------------------------------------------------------------
  def Valid_RapporterResultat(self,rang,resultats,fiche,tagFiche,description,varoperateur):
    # Boucle sur les resultats
    resFiche=self.constantes['STATUS_OK']
    critFiche=nngsrc.services.FT_NQ
    reserves=0
    rapportHTML=''
    rapportTEX=''
    graviteFT=0
    nbEtapes=0
    for item in resultats:
      if item is not None : # Cas fin de boucle
        (valeur,listRapport,tupleFT,_)=item
        if tupleFT is not None and tupleFT[4] is not None:
          graviteFT=tupleFT[4]['GRAVITE']
        rapportHTML+=listRapport[idxHTML]
        if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
          rapportTEX+=listRapport[idxTEX]
        if valeur is None:
          nbEtapes+=1
        else:
          if valeur!=self.constantes['STATUS_OK']:
            if valeur==self.constantes['STATUS_PB']:
              resFiche=valeur
              if graviteFT>critFiche: critFiche=graviteFT
            elif valeur==self.constantes['STATUS_AV'] and resFiche!=self.constantes['STATUS_PB']:
              resFiche=valeur
              if graviteFT>critFiche: critFiche=graviteFT
            elif valeur==self.constantes['STATUS_AR'] and resFiche not in [self.constantes['STATUS_AV'],self.constantes['STATUS_PB']]:
              resFiche=valeur
              if graviteFT>critFiche: critFiche=graviteFT
            elif valeur==self.constantes['STATUS_NT'] and resFiche not in [self.constantes['STATUS_AR'],self.constantes['STATUS_AV'],self.constantes['STATUS_PB']]:
              resFiche=valeur
              reserves+=1
    if resFiche==self.constantes['STATUS_NT'] and reserves!=len(resultats)-nbEtapes:
      resFiche=self.constantes['STATUS_AR']
    self.html.Valid_RapporterResultat(resFiche,description,fiche,tagFiche,varoperateur,rapportHTML)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Valid_RapporterResultat(resFiche,self.html,description,fiche,tagFiche,varoperateur,rapportTEX)

# DONE // creer un fichier dédié à la fiche
    flg=open("execute_fin_%d.flg"%rang,"w",encoding='utf-8')
    flg.write("ok")
    flg.close()
    return resFiche,critFiche

  #-------------------------------------------------------------------------------
  # Mise a jour et fin
  #-------------------------------------------------------------------------------
  def Valid_Terminer(self,reference,rang,listeKeyEnv,environnement,jeuxDeDonnees,repImage,nbControles,IDcahier):
    self.qualite.couvertureFT=[0,0,0,0,0,0]
    self.qualite.couvertureExig=[0,0,0,0,0]
    self.qualite.couvertureFonc=[0,0,0,0,0]
    self.qualite.exigenceVSft={}
    self.qualite.fonctionVSft={}
    nbCtrlBLOQ=0

    f=open(self.ficNonList,"w",encoding='utf-8')
    nonListe=False
    nngsrc.services.SRV_Terminal(None,None,f,self.projet.lexique.entree("LOCUT_EXIGENCES_NON_LISTEES"),False)
    for e in self.projet.exigences:
      if self.projet.exigences[e]['applicable']:
        self.qualite.exigenceVSft[e]={0:0,1:0,2:0,3:0,4:0,"Couv":False}
    for e in self.qualite.listeExig:
      if e not in self.projet.exigences:
        nonListe=True
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree("LOCUT_EXIGENCE_X_NON_LISTEE")%e,style="Er")
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,f,"%s"%e,False)
      self.qualite.exigenceVSft[e]={0:0,1:0,2:0,3:0,4:0,"Couv":False}

    nngsrc.services.SRV_Terminal(None,None,f,self.projet.lexique.entree("LOCUT_FONCTIONNALITES_NON_LISTEES"),False)
    for e in self.projet.fonctionnalites:
      if self.projet.fonctionnalites[e]['applicable']:
        self.qualite.fonctionVSft[e]={0:0,1:0,2:0,3:0,4:0,"Couv":False}
    for e in self.qualite.listeFct:
      if e not in self.projet.fonctionnalites:
        if self.projet.PRODUIT_FONCTIONNALITES!='':
          nonListe=True
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree("LOCUT_FONCTIONNALITE_X_NON_LISTEE")%e,style="Er")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,f,"%s"%e,False)
      self.qualite.fonctionVSft[e]={0:0,1:0,2:0,3:0,4:0,"Couv":False}
    f.close()

    for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
      if k>2:
        ctrlBloquant=[]
        for ctrl in self.qualite.matTracabilite[k]:
          ((_,fiche,_,_,_,_,_),_,_,_,)=ctrl
          if fiche not in ctrlBloquant:
            ctrlBloquant.append(fiche)
        nbCtrlBLOQ=len(ctrlBloquant)

      for ctrl in self.qualite.matTracabilite[k]:
        ((rang,_,_,_,_,desc,_),numFT,etatFT,exigences)=ctrl
        entite,_,_,_,_,exigGlobales=desc
        if numFT:
          if etatFT=='MOT_SUSPENDU':
            self.qualite.couvertureFT[5]+=1
          elif etatFT!='LOCUT_A_CLORE':
            self.qualite.couvertureFT[k]+=1
        exigences+=","+exigGlobales
        exigences=exigences.replace(';',',').replace('\\n',',').split(',')
        while '' in exigences: exigences.remove('')
        if numFT and etatFT!='LOCUT_A_CLORE':
          for e in exigences:
            self.qualite.exigenceVSft[e][k]+=1
          if entite!='':
            self.qualite.fonctionVSft[entite][k]+=1
        e=''
        try:
          for e in exigences:
            self.qualite.exigenceVSft[e]['Couv']=True
        except:
          print("Exigence %s inconnue"%e)
        if entite!='':
          self.qualite.fonctionVSft[entite]['Couv']=True

    if self.projet.execution in ['P','S']:
      echo=True
    else:
      echo=False
    echo=False
    #nngsrc.services.SRV_Terminal(None,None,f,"--La trace des Exigences et Fonctionnalites non couvertes est desactivee--",echo)
    ficNonCouv=os.path.join(self.repTravail,'nonCouv.txt')
    f=open(ficNonCouv,"w",encoding='utf-8')
    nngsrc.services.SRV_Terminal(None,None,f,"--Exigences non couvertes--",echo)
    for e in nngsrc.services.SRV_listeKeysDict(self.qualite.exigenceVSft):
      if e!='':
        if not self.qualite.exigenceVSft[e]['Couv']:
          nngsrc.services.SRV_Terminal(None,None,f,"%s"%e,echo)
    nngsrc.services.SRV_Terminal(None,None,f,"--Fonctionnalites non couvertes--",echo)
    for e in nngsrc.services.SRV_listeKeysDict(self.qualite.fonctionVSft):
      if not self.qualite.fonctionVSft[e]['Couv']:
        nngsrc.services.SRV_Terminal(None,None,f,"%s"%e,echo)
    f.close()

    if self.projet.execution=='S':
      print("------------------------------------------")
      f=open(os.path.join(self.repRapport,"nng-analyses","IHMpngOK.txt"),"w",encoding='utf-8')
      for rep in nngsrc.services.SRV_listeKeysDict(self.listePgnIHM):
        self.listePgnIHM[rep].sort()
        nngsrc.services.SRV_Terminal(None,None,f,"%s :"%rep,echo)
        for elem in self.listePgnIHM[rep]:
          nngsrc.services.SRV_Terminal(None,None,f,"%s"%elem,echo)
      f.close()

      f=open(os.path.join(self.repRapport,"nng-analyses","IHMpngInutilises.txt"),"w",encoding='utf-8')
      out=False
      for rep in nngsrc.services.SRV_listeKeysDict(self.listePgnIHM):
        for elem in glob.glob(os.path.join(rep,'*.*')):
          if os.path.basename(elem) not in self.listePgnIHM[rep]:
            nngsrc.services.SRV_Terminal(None,None,f,"%s"%elem,echo)
            out=True
      f.close()
      if out:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,u"La liste des fichiers image IHM inutilisés est dans %s"%os.path.join(self.repRapport,"nng-analyses","IHMpngInutilises.txt"),style="Er")

      f=open(os.path.join(self.repRapport,"nng-analyses","IHMpngKO.txt"),"w",encoding='utf-8')
      out=False
      for rep in nngsrc.services.SRV_listeKeysDict(self.listePgnIHM):
        for elem in self.listePgnIHM[rep]:
          if not os.path.exists(os.path.join(rep,elem)):
            nngsrc.services.SRV_Terminal(None,None,f,"%s"%os.path.join(rep,elem),echo)
            out=True
      f.close()
      if out:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,u"La liste des fichiers image IHM inexistant est dans %s"%os.path.join(self.repRapport,"IHMpngKO.txt"),style="Er")

    for e in nngsrc.services.SRV_listeKeysDict(self.qualite.exigenceVSft):
      for k in range(5):
        if self.qualite.exigenceVSft[e][k]!=0:
          self.qualite.couvertureExig[k]+=1
    for e in nngsrc.services.SRV_listeKeysDict(self.qualite.fonctionVSft):
      for k in range(5):
        if self.qualite.fonctionVSft[e][k]!=0:
          self.qualite.couvertureFonc[k]+=1

    n_OK=self.bilan["vert"]
    n_AR=self.bilan["violet"]
    n_AV=self.bilan["orange"]
    n_PB=self.bilan["rouge"]
    n_NT=self.bilan["noir"]
    total=n_OK+n_AR+n_AV+n_PB+n_NT
    etatfinal="noir.png"
    if n_PB>0: etatfinal="rouge.png"
    elif n_AV>0: etatfinal="orange.png"
    elif n_AR>0: etatfinal="violet.png"
    elif n_OK==total: etatfinal="vert.png"
    nngsrc.services.SRV_copier(os.path.join(self.repRessources,etatfinal),os.path.join(self.repTravail,self.ficRapportGenerique+".png"),timeout=5,signaler=True)
    self.qualite.OkArAvPbNt=(total,nbCtrlBLOQ)
    # graphique des controles
    if self.projet.execution=='R':
      fichierGr=os.path.join(self.repTravail,"graphiqueCtrl")
      parametres=nngsrc.graphes.GRAPH_parametresDefaut(pMATPLOTLIB=True)
      parametres['couleurs']=['#8DB600','#FF033E','#FFBF00','#89CFF0','#6E7F80']

      _,_,s_OK,_,_,_=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_OK'],self.constantes,self.projet.DOCUMENT_LANGUE)
      _,_,s_PB,_,_,_=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_PB'],self.constantes,self.projet.DOCUMENT_LANGUE)
      _,_,s_AV,_,_,_=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AV'],self.constantes,self.projet.DOCUMENT_LANGUE)
      _,_,s_AR,_,_,_=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
      _,_,s_NT,_,_,_=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_NT'],self.constantes,self.projet.DOCUMENT_LANGUE)
      X=[s_OK,s_PB,s_AV,s_AR,s_NT]
      Y=[n_OK,n_PB,n_AV,n_AR,n_NT]
      parametres['explode']=(0.1,0,0,0,0)
      nngsrc.graphes.GRAPH_Camembert(fichierGr,self.projet.lexique.entree("MOT_CONTROLES"),X,Y,parametres)

    # graphique des exigences
    CouvOK=0
    CouvKO=0
    nbexig=0
    for e in self.projet.exigences:
      if self.projet.exigences[e]['applicable']:
        nbexig+=1
    for e in self.qualite.listeExig:
      if e in self.projet.exigences:
        if self.qualite.exigenceVSft[e]["Couv"]:
          KO=0
          for crit in range(5):
            KO+=self.qualite.exigenceVSft[e][crit]
          if KO:
            CouvKO+=1
          else:
            CouvOK+=1
    nonCouv=nbexig-CouvOK-CouvKO
    fichierGrExig=os.path.join(self.repTravail,"graphiqueExig")
    parametres=nngsrc.graphes.GRAPH_parametresDefaut(pMATPLOTLIB=True)
    if self.projet.execution=='R':
      X=[ self.projet.lexique.entree("ABR_NON_COUVRANT") ,  '%s %s'%(self.projet.lexique.entree("ABR_COUVRANT"),self.projet.lexique.entree("MOT_OK"))  ,  '%s %s'%(self.projet.lexique.entree("ABR_COUVRANT"),self.projet.lexique.entree("MOT_KO")) ]
      Y=[ nonCouv     ,  CouvOK      ,  CouvKO     ]
      parametres['explode']=(0,0.1,0)
    else:
      X=[ self.projet.lexique.entree("ABR_NON_COUVRANT") ,  self.projet.lexique.entree("ABR_COUVRANT") ]
      Y=[ nonCouv     ,  CouvOK     ]
      parametres['explode']=(0.1,0)
    nngsrc.graphes.GRAPH_Camembert(fichierGrExig,self.projet.lexique.entree("MOT_EXIGENCES"),X,Y,parametres)

    # graphique des fonctionnalites
    CouvOK=0
    CouvKO=0
    nbexig=0
    for e in self.projet.fonctionnalites:
      if self.projet.fonctionnalites[e]['applicable']:
        nbexig+=1
    for e in self.qualite.listeFct:
      if e in self.projet.fonctionnalites:
        if self.qualite.fonctionVSft[e]["Couv"]:
          KO=0
          for crit in range(5):
            KO+=self.qualite.fonctionVSft[e][crit]
          if KO:
            CouvKO+=1
          else:
            CouvOK+=1
    nonCouv=nbexig-CouvOK-CouvKO
    fichierGrFonct=os.path.join(self.repTravail,"graphiqueFonct")
    parametres=nngsrc.graphes.GRAPH_parametresDefaut(pMATPLOTLIB=True)
    if self.projet.execution=='R':
      X=[ self.projet.lexique.entree("ABR_NON_COUVRANT") ,  '%s %s'%(self.projet.lexique.entree("ABR_COUVRANT"),self.projet.lexique.entree("MOT_OK"))  ,  '%s %s'%(self.projet.lexique.entree("ABR_COUVRANT"),self.projet.lexique.entree("MOT_KO")) ]
      Y=[ nonCouv     ,  CouvOK      ,  CouvKO     ]
      parametres['explode']=(0,0.1,0)
    else:
      X=[ self.projet.lexique.entree("ABR_NON_COUVRANT") ,  self.projet.lexique.entree("ABR_COUVRANT") ]
      Y=[ nonCouv     ,  CouvOK     ]
      parametres['explode']=(0.1,0)
    nngsrc.graphes.GRAPH_Camembert(fichierGrFonct,self.projet.lexique.entree("MOT_FONCTIONNALITES"),X,Y,parametres)

    self.html.Valid_Terminer(reference,rang,self.bilan,nbControles)
    if self.editRapport:
      self.csv.Valid_Terminer(self.bilan,nbControles,IDcahier)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Valid_Terminer(self.bilan,self.qualite.rapportFT,listeKeyEnv,repImage,environnement,jeuxDeDonnees,nbControles)

    for fichier in ["graphiqueCtrl.png","graphiqueExig.png","graphiqueFonct.png"]:
      if os.access(os.path.join(self.repTravail,fichier),os.F_OK):
        os.remove(os.path.join(self.repTravail,fichier))

    if os.access(ficNonCouv,os.F_OK):
      nngsrc.services.SRV_deplacer(ficNonCouv,os.path.join(self.repRapport,self.projet.execution,'nonCouv.txt'),timeout=5,signaler=True)
    if nonListe:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,u"Consulter le fichier %s des exigences/fonctionnalités non listées"%os.path.join(self.repRapport,self.projet.execution,'nonList.txt'),style="Er")
      if os.access(self.ficNonList,os.F_OK):
        nngsrc.services.SRV_deplacer(self.ficNonList,os.path.join(self.repRapport,self.projet.execution,'nonList.txt'),timeout=5,signaler=True)
    else:
      os.remove(self.ficNonList)
      if os.access(os.path.join(self.repRapport,self.projet.execution,'nonList.txt'),os.F_OK):
        os.remove(os.path.join(self.repRapport,self.projet.execution,'nonList.txt'))

    # - Rapport Texte en ligne --
    if self.editRapport and self.projet.RAPPORT_EN_LIGNE=="oui" :
      self.txt.Valid_Terminer()
    return total==n_OK

  #-----------------------------------------------------------------------------------
  def GestionConfirmationFT(self,fiche,RBilan,disp,rapporter,etatAction):
    FTconfirme=True
    FTreserve=''
    if etatAction!="NOK" and etatAction!="AR":
      etatAction=disp[2]
      if rapporter!=-1 and etatAction in ["NOK","AV","AR"] and self.projet.CONFIRMATION_FT=="oui":
        k=RBilan['CTL_FTQUALIF']
        if k==0: txtqualif="<i>(%s)</i>"%self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%k)
        else: txtqualif="(%s)"%self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%k)
        (_,lafiche,_,_,_,_,_)=fiche
        nBase=txtqualif+" "
        nBase+=lafiche.replace('\\','/')+":"
        idxAction=RBilan['CTL_INDEX'].replace('[/anchor]','')
        nBase+=idxAction[idxAction.find(']')+1:]+" "
        nBase+=RBilan['CTL_COMMENT']
        numBase=0
        for eBase in self.qualite.baseFT:
#          print
#          print eBase['LIBELLE']
          if eBase['LIBELLE'].find('\n')!=-1:
            eBase=eBase['LIBELLE'].split('\n')
#            print
#            print type(eBase[1]),eBase[1]
#            print type(nBase),nBase
            if eBase[1]==nBase:
              numBase=int(eBase[0])
              break
        if numBase==0:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'')
          confirme=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree('LOCUT_CONFIRMER_FT'),"CONFIRMATION",style="W",espacement=10).lower()
          FTconfirme=(confirme=='' or confirme=="o")
          if confirme=="t":                       #v8.2rc1 Arret possible dans le mode Manuel ou la fonction Etape Manuelle
            self.projet.ARRET=True
            disp=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
            etatAction=disp[2]
          elif not FTconfirme:
            FTreserve=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree('LOCUT_RESERVE'),"SAISIE",style="W",espacement=15)
            disp=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_AR'],self.constantes,self.projet.DOCUMENT_LANGUE)
            etatAction=disp[2]
          elif FTconfirme:
            disp=nngsrc.services.SRV_codeResultat(self.constantes['STATUS_PB'],self.constantes,self.projet.DOCUMENT_LANGUE)
            etatAction=disp[2]
#    print
#    print "etatAction        ",etatAction
#    print "disp      ",disp
#    print "FTconfirme",FTconfirme
#    print "FTreserve ",FTreserve
    return etatAction,FTconfirme,FTreserve

  #-------------------------------------------------------------------------------
  def Valid_ConfirmationFT(self,fiche,params,execution,rapporter):
    FTconfirme=None
    FTreserve=''
    CTL_MODE=params['CTL_MODE']

    etatAction="OK"
    if self.projet.execution!='S':
      # Mode niveau 3 à 2
      if len(CTL_MODE)>2 and CTL_MODE[2]=='2':
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,FTconfirme,FTreserve)=self.GestionConfirmationFT(fiche,params,disp,rapporter,etatAction)
      # Mode niveau 3 à 3
      if len(CTL_MODE)>2 and CTL_MODE[2]=='3':
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,FTconfirme,FTreserve)=self.GestionConfirmationFT(fiche,params,disp,rapporter,etatAction)
      # Mode niveau 4 à 3
      if len(CTL_MODE)>4 and CTL_MODE[4]=='3':
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL2_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          (etatAction,FTconfirme,FTreserve)=self.GestionConfirmationFT(fiche,params,disp,rapporter,etatAction)
    return etatAction,FTconfirme,FTreserve

#---------------------------------------------------------------------------------
  def Valid_InscrireEtape(self,libelle):
    self.html.Valid_InscrireEtape(libelle)
    if self.editRapport and self.projet.RAPPORT_TEX=="oui" :
      self.tex.Valid_InscrireEtape(libelle)

#---------------------------------------------------------------------------------
# Fin
