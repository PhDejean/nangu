#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

# ------------------------------------------------
#  Chargement de l'environnement
# ------------------------------------------------
import datetime,copy,os

import nngsrc.servicesXML
import nngsrc.services

# ------------------------------------------------
dictRapport={'HTML':0,'CSV':1,'LaTeX':2,'Markdown':3}
dictTypeDocument={'S':"Scenarios",'P':"Plan",'C':"Cahier",'R':"Rapport"}
listeTypeDoc=['S','P','C','R']
versionPRJ="1.0"
iconesScript={'-1':"script_delete.png",'0':"script_red.png",'1':"script_green.png"}
iconesFiches={'-1':"document_yellow_delete.png",'0':"document_red.png",'1':"document_green.png"}
iconesTag={'normal':"tag_green.png",'persist':"tag_blue.png",'varenv':"tag_red.png",'persistvarenv':"tag_purple.png"}
iconesActions={'fichier':"document_yellow.png",'action':"cog.png"}

prioritesFT={4:'MOT_URGENTE',3:'MOT_URGENTE',2:'MOT_ELEVEE',1:'MOT_NORMALE',0:'MOT_BASSE'}
Aclore='LOCUT_A_CLORE_MAJ'
DejaEmisFT=['LOCUT_A_FAIRE','LOCUT_EN_COURS']
SuspenduFT=['LOCUT_EN_DISCUSSION','LOCUT_A_VALIDER','MOT_SUSPENDU']
DejaClosFT=[Aclore,'MOT_TERMINE','MOT_ABANDONNE']
Couls=["#FFFFB0","#C8C8FF","#AAAA7F","#DC6DA5","#FFBB66","#3DC983","#CCEEDD","#E8E8E8"]

# ------------------------------------------------
ongletProjet=0
ongletDocuments=1
ongletScenarios=2
ongletCahiers=3
ongletFiches=4
# ------------------------------------------------
IDX_MAXID  = 10
IDX_TACHES = 13
IDX_TAGS   = 14

TODOVERSION="0.0"
TODOxmlVERSION="1.1"
LISTECHAMPS=["ID","TAG","EMETTEUR","REFERENCE","DATE","MODIFIE","TYPE","COMPOSANT","VERSION","RESPONSABLE","CIBLE","ECHEANCE","PRIORITE","GRAVITE","STATUT","LIBELLE"]

#-----------------------------------------------------------------------------------
def TODOFTparID(baseFT,idx):
  for ft in baseFT:
    if ft['ID']==idx:
      return ft
  return None

#-----------------------------------------------------------------------------------
def ajouterListe(terme,liste,defaut=False):
  liste=liste.split(',')
  if terme not in liste and "*%s"%terme not in liste:
    liste.append(terme)
  liste=','.join(liste)
  if defaut:
    liste=liste.replace('*','').replace('%s'%terme,'*%s'%terme)
  if liste[0]==',':liste=liste[1:]
  return liste

#-----------------------------------------------------------------------------------
# Classe du rapport
#-----------------------------------------------------------------------------------
class Qualite:

  #---------------------------------------------------------------------------------
  # Initialisation de la classe
  #---------------------------------------------------------------------------------
  def __init__(self,repRapport,projet):
    self.todoFT=None
    self.NUM_FT=None
    self.reference=None
    self.newNumFT=0
    self.baseFT=[]
    self.tagActFT={}
    self.tagFT=[]
    self.matTracabilite={}
    self.couvertureFT=[0,0,0,0,0,0]
    self.couvertureExig=[0,0,0,0,0]
    self.couvertureFonc=[0,0,0,0,0]
    self.OkArAvPbNt=None
    self.exigenceVSft={}
    self.fonctionVSft={}
    self.rapportFT=[]
    self.rapportFTfiche=[]
    self.rapportFTficheReserves=[]
    self.lstFTreOuvert=[]
    self.lstFTfichereOuvert=[]
    self.lstFTaClore=[]
    self.lstFTficheaClore=[]
    self.listeFT={}
    self.listeExig={}
    self.listeFct={}
    self.nbFTenCours=0
    self.nbFTSuspendus=0
    self.nbFTdejaEmis=0
    self.DejaEmisFTlex=[]
    for loc in DejaEmisFT:
      self.DejaEmisFTlex.append(projet.lexique.entree(loc))
    self.SuspenduFTlex=[]
    for loc in SuspenduFT:
      self.SuspenduFTlex.append(projet.lexique.entree(loc))
    self.DejaClosFTlex=[]
    for loc in DejaClosFT:
      self.DejaClosFTlex.append(projet.lexique.entree(loc))
    self.repRapport=repRapport
    self.projet=projet

    # gestion de la base FT
    if os.access(projet.PRODUIT_BASEFT,os.F_OK):
      if projet.debug==2:
        nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,None,"Lecture de la base %s\n"%projet.PRODUIT_BASEFT)
      if projet.MANTIS is not None:
        fichierChampsMantis=os.path.join(projet.FICHES,nngsrc.services.SRV_TraiterJoin(projet.MANTIS))
        # Fichier XML Mantis
        if fichierChampsMantis is not None:
          self.dicoMantis={'GRAVITES':[]}
          racine=nngsrc.servicesXML.SRVxml_parser(fichierChampsMantis)
          self.dicoMantis['PROJET']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PROJET"))
          self.dicoMantis['REPORTER']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"REPORTER"))
          self.dicoMantis['PRIVE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIVE"))
          self.dicoMantis['CATEGORIE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"CATEGORIE"))
          self.dicoMantis['REPRODUCTIBILITE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"REPRODUCTIBILITE"))
          self.dicoMantis['STATUT']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"STATUT"))
          self.dicoMantis['RESOLUTION']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RESOLUTION"))
          self.dicoMantis['PROJECTION']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PROJECTION"))

          self.dicoMantis['PRIORITE_BASSE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIORITES,BASSE"))
          self.dicoMantis['PRIORITE_NORMALE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIORITES,NORMALE"))
          self.dicoMantis['PRIORITE_ELEVEE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIORITES,ELEVEE"))
          self.dicoMantis['PRIORITE_URGENTE']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIORITES,URGENTE"))
          for nc in range(5):
            self.dicoMantis['GRAVITES'].append(nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"GRAVITES,NIVEAU_CRITIQUE_%d"%nc)))
      ok=self.TODOlireXML(projet.PRODUIT_BASEFT,projet.lexique,projet.PRODUIT_NOM,projet.PRODUIT_VERSION)
      if not ok:
        nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,None,"Base de fait techniques non lue correctement.\n",style="Er")
    else:
       nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,None,"Base de fait techniques %s inaccessible.\n"%projet.PRODUIT_BASEFT)

  #---------------------------------------------------------------------------------
  def TODOlireXML(self,fichier,lexique,nom,version):
    racine=nngsrc.servicesXML.SRVxml_parser(fichier)
    if racine and nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)!="TODO":
      print(u"ERREUR : Le fichier est incomplet (tag '%s' vs '%s')"%("TODO",nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)))
      return False

    resVersion=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"VERSION"))
  #  if resVersion!=TODOxmlVERSION:
  #    print("Le fichier XML v%s ne correspond pas a la version souhaitee : v%s"%(resVersion,TODOxmlVERSION))
  #    return False

    resProjet=nom
    # Ajout des types
    resTypes=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"TYPES"))
    resTypes=ajouterListe(lexique.entree('LOCUT_FAIT_TECH'),resTypes,defaut=True)
    # Ajout des fonctionnalités
    resComposants=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"COMPOSANTS"))

    # Ajout de le version courante
    resVersions=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"VERSIONS"))
    resVersions=ajouterListe(version,resVersions,defaut=True)
    # Ajustement des priorites
    resPriorites=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"PRIORITES"))
    for idx in prioritesFT.keys():
      resPriorites=ajouterListe(lexique.entree(prioritesFT[idx]),resPriorites,defaut=(idx==1))
    # Ajustement des gravites
    resGravites=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"GRAVITES"))
    for elem in [4,3,2,1,0]:
      resGravites=ajouterListe(lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%idx),resGravites,defaut=(elem==0))
    resResponsables=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"RESPONSABLES"))
    # Ajustement des statuts
    resStatuts=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"STATUTS"))
    for elem in DejaEmisFT:
      resStatuts=ajouterListe(lexique.entree(elem),resStatuts,defaut=(elem=='LOCUT_A_FAIRE'))
    for elem in DejaClosFT:
      resStatuts=ajouterListe(lexique.entree(elem),resStatuts)
    # Ajustement des couleurs
    resCouleurs=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"COULEURS")).split(',')
    idx=0
    while len(resCouleurs)<len(resStatuts.split(',')):
      resCouleurs.append(Couls[idx])
      idx+=1
    resCouleurs=','.join(resCouleurs)

    resFiltres=[False,0,0,0,0,0,0]
    ndFiltres=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"FILTRES")
    if ndFiltres:
      ndFiltres=nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndFiltres)
      resFiltres=[True]
      resFiltres.extend([int(x) for x in ndFiltres.split(',')])

    resCaches=[0,0,0,0,0,0,0,0,0,0]
    ndCaches=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"CACHES")
    if ndCaches:
      ndCaches=nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndCaches)
      resCaches=[]
      resCaches.extend([int(x) for x in ndCaches.split(',')])
    while len(resCaches)<12: resCaches.extend([0])

    ndTaches=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"TACHES")
    self.NUM_FT = nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(ndTaches,"MAXID"))
    #  print "DBG-self.NUM_FT>",self.NUM_FT
    if not self.NUM_FT:
      self.NUM_FT=0
    else:
      self.NUM_FT=int(self.NUM_FT)

    self.baseFT=[]
    self.tagActFT={}
    self.tagFT=[]
    self.nbFTenCours=0
    self.nbFTSuspendus=0
    items=nngsrc.servicesXML.SRVxml_recupTousNoeuds(ndTaches,"TACHE")
    for item in items:
      dicoMenu={}
      for balise in LISTECHAMPS:
        valNoeud=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(item,balise),'')
        if balise=="GRAVITE":
          gravite=0
          for i in range(5):
            if lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%i)==valNoeud:
              gravite=i
          valNoeud=gravite
        dicoMenu[balise]=valNoeud
  #    dicoMenu['TAG']=None
  #    tag=dicoMenu['LIBELLE'].split('\n')
  #    #print "DBG>",tag,len(tag),"-%s-"%tag[3][:3]
  #    if len(tag)>4 and tag[3][:3]=="TAG":
  #      dicoMenu['TAG']=tag[3].split(' : ')[1]
  #    print "DBG>",dicoMenu
      self.baseFT.append(dicoMenu)
      # on sauve le rang de la FT dans le dictionnaire de tag pour la retrouver plus rapidement
      if dicoMenu['TAG'] is not None:
        self.tagActFT[dicoMenu['TAG']]=len(self.baseFT)-1
        self.tagFT.append(dicoMenu['TAG'].split('-')[0])
      if dicoMenu['STATUT'] in self.DejaEmisFTlex:
        self.nbFTenCours+=1
      if dicoMenu['STATUT'] in self.SuspenduFTlex:
        self.nbFTSuspendus+=1

    self.newNumFT=self.NUM_FT

    self.todoFT=(resVersion,              #0
             resProjet,                   #1
             resTypes.split(','),         #2
             resComposants.split(','),    #3
             resVersions.split(','),      #4
             resPriorites.split(','),     #5
             resGravites.split(','),      #6
             resResponsables.split(','),  #7
             resStatuts.split(','),       #8
             resCouleurs.split(','),      #9
             resFiltres,                  #10
             resCaches)                   #11

    return True

  #-----------------------------------------------------------------------------------
  @staticmethod
  def trierID(Taches):
    tacheTrie=[]
    lID=range(len(Taches)+1)
    for idx in lID:
      for t in Taches:
        if t["ID"]=="%06d"%idx:
          tacheTrie.append(t)
          break
    return tacheTrie

  #-----------------------------------------------------------------------------------
  def TODOecrireXML(self,fichier,lexique,ajoutBase,MaxIdFT,backup=False):
  #  self.todoTaches=[]
  #  for tache in range(self.ui.tableWidget.rowCount()):
  #    contenu={}b
  #    for champ in range(len(LISTECHAMPS)):
  #      contenu[LISTECHAMPS[champ]]=self.ui.tableWidget.item(tache,champ).text()
  #    self.todoTaches.append(contenu)
    todo=self.todoFT
    Taches=self.baseFT
    (Version,Projet,Types,Composants,Versions,Priorites,Gravites,Responsables,Statuts,Couleurs,Filtres,Caches)=todo
    if backup:
      fXml=open("%s.backup"%fichier,"w",encoding='utf-8')
    else:
      fXml=open(fichier,"w",encoding='utf-8')
    fXml.write("<?xml version='1.0' encoding='utf-8'?>\n")
    fXml.write("<TODO>\n")
    fXml.write("  <VERSION>%s</VERSION>\n"%Version)
    fXml.write("  <PROJET>%s</PROJET>\n"%Projet)
    fXml.write("  <TYPES>%s</TYPES>\n"%','.join(Types))
    fXml.write("  <COMPOSANTS>%s</COMPOSANTS>\n"%','.join(Composants))
    fXml.write("  <VERSIONS>%s</VERSIONS>\n"%','.join(Versions))
    fXml.write("  <PRIORITES>%s</PRIORITES>\n"%','.join(Priorites))
    fXml.write("  <GRAVITES>%s</GRAVITES>\n"%','.join(Gravites))
    fXml.write("  <RESPONSABLES>%s</RESPONSABLES>\n"%','.join(Responsables))
  #  print type(Statuts[0]),Statuts
  #  print ','.join(Statuts)
    fXml.write("  <STATUTS>%s</STATUTS>\n"%','.join(Statuts))
    fXml.write("  <COULEURS>%s</COULEURS>\n"%','.join(Couleurs))
    if Filtres[0]:
      fXml.write("  <FILTRES>%s</FILTRES>\n"%','.join(map(str,Filtres[1:])))
    fXml.write("  <CACHES>%s</CACHES>\n"%','.join(map(str,Caches)))
    fXml.write("  <TACHES>\n")
    fXml.write("    <MAXID>%06d</MAXID>\n"%int(MaxIdFT))

    if ajoutBase:
      for tache in ajoutBase:
        Taches.append(tache)
    Taches=self.trierID(Taches)
    for tache in Taches:
      #print "DBG>Taches", tache
      fXml.write("    <TACHE>\n")
      fXml.write("      <ID>%s</ID>\n"%tache["ID"])
      fXml.write("      <TAG>%s</TAG>\n"%tache["TAG"])
      fXml.write("      <REFERENCE>%s</REFERENCE>\n"%tache["REFERENCE"])
      fXml.write("      <EMETTEUR>%s</EMETTEUR>\n"%tache["EMETTEUR"])
      fXml.write("      <TYPE>%s</TYPE>\n"%tache["TYPE"])
      fXml.write("      <COMPOSANT>%s</COMPOSANT>\n"%tache["COMPOSANT"])
      fXml.write("      <VERSION>%s</VERSION>\n"%tache["VERSION"])
      fXml.write("      <LIBELLE>%s</LIBELLE>\n"%tache["LIBELLE"])
      fXml.write("      <RESPONSABLE>%s</RESPONSABLE>\n"%tache["RESPONSABLE"])
      fXml.write("      <DATE>%s</DATE>\n"%tache["DATE"])
      fXml.write("      <MODIFIE>%s</MODIFIE>\n"%tache["MODIFIE"])
      fXml.write("      <CIBLE>%s</CIBLE>\n"%tache["CIBLE"])
      fXml.write("      <ECHEANCE>%s</ECHEANCE>\n"%tache["ECHEANCE"])
      fXml.write("      <PRIORITE>%s</PRIORITE>\n"%tache["PRIORITE"])
      fXml.write("      <GRAVITE>%s</GRAVITE>\n"%lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%tache["GRAVITE"]))
      fXml.write("      <STATUT>%s</STATUT>\n"%tache["STATUT"])
      fXml.write("    </TACHE>\n")
    fXml.write("  </TACHES>\n")
    fXml.write("</TODO>\n")
    fXml.close()

  #-----------------------------------------------------------------------------------
  def QUAL_CouvertureMatrice(self,matTracabilite,rang,fiche,tableFT,gestionFTfiches,pParallel):
    # Mise à jour de la matrice de tracabilite
    for elemMatTrac in matTracabilite:
      (lerang,lafiche,lescenario,qualif,doc,numPar,exigences)=elemMatTrac
      (numFT,etatFT,parallel)=numPar
      if not qualif in self.matTracabilite.keys():
        self.matTracabilite[qualif]=[]
      element=(doc,numFT,etatFT,exigences)
      # En cas de parallelisation : changement de l'ID de la FT
      if pParallel:
        if numFT and parallel and numFT in tableFT:
#          print "DBG> -qualif-",qualif
#          print "DBG> numFT   ",numFT
#          print "DBG> newNumFT",tableFT[numFT]
#          print "DBG> parallel",parallel
          element=(doc,tableFT[numFT],exigences) # /!\ Il manque etatFT
      self.matTracabilite[qualif].append(element)

    # Mise à jour des liste de FT pour les compte-rendus
    self.rapportFTfiche=[]
    self.lstFTficheaClore=[]
    self.lstFTfichereOuvert=[]

    for e in gestionFTfiches:
      (FT,numBase,libelleFT,ft,elem,lerang,lafiche,action)=e
      if ft:
        if ft>0:
          if pParallel:
            elem["ID"]='%06d'%tableFT[ft]
          self.rapportFT.append(elem)
          if elem['STATUT']!='Re-ouvert':
            self.rapportFTfiche.append(elem)
        else: # Doublon
          self.rapportFT.append(elem)
          if elem['STATUT']!='Re-ouvert':
            self.rapportFTfiche.append(elem)
          else:
            ajout=(lafiche,action,int(elem['ID']),elem['GRAVITE'])
            self.lstFTreOuvert.append(ajout)
            if lafiche==fiche and lerang==rang:
              self.lstFTfichereOuvert.append(ajout)
      if FT=='OK':
        ajout=(lafiche,action,int(elem['ID']),elem['GRAVITE'])
        self.lstFTaClore.append(ajout)
        if lafiche==fiche:
          self.lstFTficheaClore.append(ajout)

  #---------------------------------------------------------------------------------
  @staticmethod
  def TraiterListes(lstComplete, lstCourante, res, constantes):
    ok=0
    if res==constantes['STATUS_OK'] or res==constantes['STATUS_AR']: ok=1
    lstCourante=lstCourante.replace('\\n','')
    lstCourante=lstCourante.replace(';',',')
    lstCourante=lstCourante.split(',')
    if '' in lstCourante:
      lstCourante.remove('')
    for e in lstCourante:
      e=e.replace('\\n','')
      if e!='':
        e=e.strip()
        if e in lstComplete:
          (oldRes,nbOk,nbTot)=lstComplete[e]
          if oldRes==constantes['STATUS_OK'] or oldRes==constantes['STATUS_AR']: # oldRes=OK
            lstComplete[e]=(res,nbOk+ok,nbTot+1)                                 #  => res
          elif oldRes==constantes['STATUS_AV'] and res==constantes['STATUS_PB']: # oldRes=AV et res=KO
            lstComplete[e]=(constantes['STATUS_PB'],nbOk+ok,nbTot+1)             #  => KO
          else:                                                                  # sinon
            lstComplete[e]=(oldRes,nbOk+ok,nbTot+1)                              #  => oldRes
        else:
          if res==constantes['STATUS_OK']: ok=1
          lstComplete[e]=(res,ok,1) # status, nbOk, nbTot pour statistique de OK
    return lstComplete

  #-----------------------------------------------------------------------------------
  def QUAL_TraiterListes(self,faitstechniques,exigences,entite,result,constantes):
    self.listeFT  =self.TraiterListes(self.listeFT,faitstechniques,result,constantes)
    self.listeExig=self.TraiterListes(self.listeExig,exigences,result,constantes)
    self.listeFct =self.TraiterListes(self.listeFct,entite,result,constantes)

  #-------------------------------------------------------------------------------
  def rapportMantis(self,ficRapport,etatFT,numFT,strFT=None,priorite=None,rapport=None,description=None,tag=None):
    if self.projet.MANTIS is None: return
    fichier=os.path.join(self.repRapport,"Mantis","%s_%s.txt"%(ficRapport,'_'.join(etatFT.split('_')[1:])))
    f=open(fichier,"a")
    f.write("%s\n"%numFT)
    f.close()
    if etatFT=='MOT_EMIS':
      fichier=os.path.join(self.repRapport,"Mantis","%s_%s.xml"%(ficRapport,'_'.join(etatFT.split('_')[1:])))
      contenu=["<?xml version='1.0' encoding='UTF-8'?>\n","<mantis>\n","</mantis>\n"]
      if os.path.exists(fichier):
        f=open(fichier,"r")
        contenu=f.readlines()
        f.close()
      contenu=contenu[:-1]
      contenu.append("  <issue>\n")
      contenu.append("      <id>%s</id>\n"%numFT)
      element=self.dicoMantis['PROJET'].split(':')
      contenu.append("      <project id='%s'>%s</project>\n"%(element[0],element[1]))
      element=self.dicoMantis['REPORTER'].split(':')
      contenu.append("      <reporter id='%s'>%s</reporter>\n"%(element[0],element[1]))

      element=prioritesFT[priorite].replace("MOT","PRIORITE")
      element=self.dicoMantis[element].split(':')
      contenu.append("      <priority id='%s'>%s</priority>\n"%(element[0],element[1]))
      element=self.dicoMantis['GRAVITES'][priorite].split(':')
      contenu.append("      <severity id='%s'>%s</severity>\n"%(element[0],element[1]))

      element=self.dicoMantis['REPRODUCTIBILITE'].split(':')
      contenu.append("      <reproducibility id='%s'>%s</reproducibility>\n"%(element[0],element[1]))
      element=self.dicoMantis['STATUT'].split(':')
      contenu.append("      <status id='%s'>%s</status>\n"%(element[0],element[1]))
      element=self.dicoMantis['RESOLUTION'].split(':')
      contenu.append("      <resolution id='%s'>%s</resolution>\n"%(element[0],element[1]))
      element=self.dicoMantis['PROJECTION'].split(':')
      contenu.append("      <projection id='%s'>%s</projection>\n"%(element[0],element[1]))
      element=self.dicoMantis['CATEGORIE'].split(':')
      contenu.append("      <category id='%s'>%s</category>\n"%(element[0],element[1]))

      element=self.dicoMantis['PRIVE'].split(':')
      contenu.append("      <view_state id='%s'>%s</view_state>\n"%(element[0],element[1]))

      contenu.append("      <summary>%s --/-- %s --/-- FT:%s</summary>\n"%(rapport,self.reference,strFT))
      contenu.append("      <due_date>1</due_date>\n")
      description=description.split('\n')
      element=description[0].split('@')
      description[0]="%s (%s)"%(element[0],element[1])
      contenu.append("      <description>FT : %s\n%s</description>\n"%(strFT,'\n'.join(description)))
      contenu.append("  </issue>\n")
      contenu.append("</mantis>\n")
      f=open(fichier,"w")
      f.writelines(contenu)
      f.close()

  #-------------------------------------------------------------------------------
  # Traitement des faits techniques ouverts par la fiche
  def QUAL_FaitsTechniquesFiche(self,fiche,RBilan,FT,FTconfirme,FTreserve,projet,ficRapport,parallel):
    numFT=None
    etatFT=None
    libelleFT=''
    qualif=RBilan['CTL_FTQUALIF']
    gestionFT=(None,None,libelleFT,None,None,None,None,None)
    elemMatTrac=None

    # Si l'action est à prendre en compte dans le rapport (attribut rapport="oui")
    # et si on a une gestion des FT...
    if RBilan['CTL_RAPPORT']!=-1 and RBilan['CTL_FTTQ']: # ... et boucle tant que ...
      (lerang,lafiche,lescenario,letag,(action,tagAction),_,_)=fiche
      lafiche=lafiche.replace('\\','/')
      TAGaction="%s-%s"%(RBilan['CTL_TAG'],action)
      if projet.execution in ['M','R']:
        # cas de l'action KO-----------------------------
        if FT=="NOK":
          # on récupère la fiche, le rang et l'ID de l'action de la fiche
          libelleBase ="%s : %s@%d%s\n"%(projet.lexique.entree('MOT_FICHE'),lafiche,lerang,letag)
          libelleBase+="%s : %s\n"%(projet.lexique.entree('MOT_ACTION'),action)
          libelleBase+="TAG : %s\n"%tagAction
          libelleBase+=RBilan['CTL_COMMENT'].replace('<','*').replace('>','*')

          # On controle si le 'CTL_TAG+action' est dans la base de FT
          # si oui, numBase est l'ID de la FT dans la base
          #         elemBaseFT est l'element complet
          numBase=-1
          elemBaseFT=None
          if TAGaction in self.tagActFT:
            numBase=int(self.baseFT[self.tagActFT[TAGaction]]["ID"])
            elemBaseFT=self.baseFT[self.tagActFT[TAGaction]]

          # Si l'élement n'est pas dans la base, on le cree et on l'ajoute
          if numBase==-1:
            self.newNumFT+=1
            numFT=self.newNumFT
            etatFT='MOT_EMIS'
            nouvElemBaseFT={"ID": '%06d' % abs(numFT)}
            if parallel: nouvElemBaseFT["ID"]='$%06d$'%numFT
            nouvElemBaseFT["TAG"]="%s-%s"%(RBilan['CTL_TAG'],action)
            nouvElemBaseFT["REFERENCE"]=''
            nouvElemBaseFT["EMETTEUR"]=projet.USERNAME
            nouvElemBaseFT["TYPE"]=projet.lexique.entree('LOCUT_FAIT_TECH')
            nouvElemBaseFT["COMPOSANT"]=projet.PRODUIT_NOM
            nouvElemBaseFT["VERSION"]=projet.PRODUIT_VERSION
            nouvElemBaseFT["LIBELLE"]="%s : %s -/- %s\n%s"%(projet.lexique.entree('MOT_RAPPORT'),ficRapport,os.path.basename(self.reference),nngsrc.services.SRV_retraitIndexation(libelleBase))
            nouvElemBaseFT["RESPONSABLE"]=''
            if projet.debug==2:
              nouvElemBaseFT["DATE"]="29/02/2016"
            else:
              nouvElemBaseFT["DATE"]=datetime.datetime.now().strftime("%d/%m/%Y")
            nouvElemBaseFT["MODIFIE"]=''
            nouvElemBaseFT["ECHEANCE"]=''
            nouvElemBaseFT["CIBLE"]=''
            nouvElemBaseFT["PRIORITE"]=projet.lexique.entree(prioritesFT[RBilan['CTL_FTQUALIF']])
            nouvElemBaseFT["GRAVITE"]=RBilan['CTL_FTQUALIF']
            nouvElemBaseFT["STATUT"]=projet.lexique.entree('LOCUT_A_FAIRE')

            # Libelle et ajout
            libelleFT="[FT#%06d]"%numFT
            strFT='%06d'%numFT
            if parallel: strFT='$%06d$'%numFT
            projet.ajoutBaseFT.append(nouvElemBaseFT)
            gestionFT=(FT,numBase,libelleFT,numFT,nouvElemBaseFT,lerang,lafiche,action)
            self.rapportMantis(ficRapport,etatFT,numFT,strFT=strFT,priorite=RBilan['CTL_FTQUALIF'],rapport=ficRapport,description=nngsrc.services.SRV_retraitIndexation(libelleBase),tag=RBilan['CTL_TAG'])
          # Si l'élement est pas dans la base, on regarde son statut
          else:
            if elemBaseFT['STATUT'] in self.DejaEmisFTlex:
              numFT=-numBase
              etatFT='LOCUT_DEJA_EMIS'
              self.nbFTdejaEmis+=1
              if projet.rapportManuel:
                nngsrc.services.SRV_afficher_texte(projet.uiTerm,projet.debug,"Fait Technique déjà émis : [FT#%06d]"%numBase,style="||Wr")
              libelleFT="[FT#%06d %s]"%(numBase,projet.lexique.entree('LOCUT_DEJA_EMIS'))
         #     strFT='%06d'%numBase
              elemDoublon=copy.deepcopy(elemBaseFT)
              elemDoublon['STATUT']='Doublon'
              gestionFT=(FT,numBase,libelleFT,numFT,elemDoublon,lerang,lafiche,action)
              self.rapportMantis(ficRapport,etatFT,numBase)
            elif elemBaseFT['STATUT'] in self.DejaClosFTlex:
              numFT=-numBase
              etatFT='LOCUT_REOUVERT'
              if projet.rapportManuel:
                nngsrc.services.SRV_afficher_texte(projet.uiTerm,projet.debug,"Fait Technique re-ouvert : [FT#%06d]"%numBase,style="||Er")
              libelleFT="[FT#%06d %s]"%(numBase,projet.lexique.entree('LOCUT_REOUVERT'))
              elemBaseFT["STATUT"]=projet.lexique.entree('LOCUT_A_FAIRE')
              if projet.debug==2:
                dateEvenement="29/02/2016"
              else:
                dateEvenement=datetime.datetime.now().strftime("%d/%m/%Y")
              correction="\n--------------------\n%s : %s\n%s : %s"%(dateEvenement,projet.lexique.entree('LOCUT_REOUVERT'),projet.lexique.entree('MOT_RAPPORT'),ficRapport)
              elemBaseFT["LIBELLE"]+=correction
              elemDoublon=copy.deepcopy(elemBaseFT)
              elemDoublon['STATUT']=projet.lexique.entree('LOCUT_REOUVERT')
              gestionFT=(FT,numBase,libelleFT,numFT,elemDoublon,lerang,lafiche,action)
              self.rapportMantis(ficRapport,etatFT,numBase)
            elif elemBaseFT['STATUT'] in self.SuspenduFTlex:
              numFT=-numBase
              etatFT='MOT_SUSPENDU'
          #    if projet.rapportManuel:
          #      nngsrc.services.SRV_afficher_texte(projet.uiTerm,projet.debug,u"Fait Technique suspendu : [FT#%06d]"%numBase,style="E")
              libelleFT="[FT#%06d %s]"%(numBase,elemBaseFT['STATUT'])
          #    elemBaseFT["STATUT"]=projet.lexique.entree('LOCUT_A_FAIRE')
          #    if projet.debug==2:
          #      dateEvenement="29/02/2016"
          #    else:
          #      dateEvenement=datetime.datetime.now().strftime("%d/%m/%Y")
          #    correction="\n--------------------\n%s : %s\n%s : %s"%(dateEvenement,projet.lexique.entree('LOCUT_REOUVERT'),projet.lexique.entree('MOT_RAPPORT'),ficRapport)
          #    elemBaseFT["LIBELLE"]+=correction
          #    elemDoublon=copy.deepcopy(elemBaseFT)
          #    elemDoublon['STATUT']=projet.lexique.entree('LOCUT_REOUVERT')
              gestionFT=(FT,numBase,libelleFT,numFT,elemBaseFT,lerang,lafiche,action)
              self.rapportMantis(ficRapport,etatFT,numBase)
            else:
              numFT=None
              etatFT=None
              print(u"ATTENTION : statut %s non traité"%elemBaseFT["STATUT"])

        # cas de l'action AR-----------------------------
        elif FT=="AR":
          if FTreserve=='':
            FTreserve="%s : %s"%(RBilan['CTL_INDEX'],projet.lexique.entree('LOCUT_SANS_COMMENTAIRE'))
          else:
            FTreserve="%s : %s"%(RBilan['CTL_INDEX'],FTreserve)
        # cas de l'action OK-----------------------------
        elif FT=="OK":
          if TAGaction in self.tagActFT:
            ftbase=self.baseFT[self.tagActFT[TAGaction]]
            if ftbase['STATUT'] in self.DejaEmisFTlex:
              numFT=int(ftbase['ID'])
              etatFT='LOCUT_A_CLORE'
              if projet.rapportManuel:
                nngsrc.services.SRV_afficher_texte(projet.uiTerm,projet.debug,"Fait Technique à clore : [FT#%06d]"%numFT,style="||Wr")
              else:
                libelleFT="[FT#%06d %s]"%(numFT,projet.lexique.entree('LOCUT_A_CLORE'))
              self.baseFT[self.tagActFT[TAGaction]]['STATUT']=projet.lexique.entree('LOCUT_A_CLORE_MAJ')
              if projet.debug==2:
                dateEvenement="29/02/2016"
              else:
                dateEvenement=datetime.datetime.now().strftime("%d/%m/%Y")
              correction="\n--------------------\n%s : %s\n%s : %s"%(dateEvenement,projet.lexique.entree('LOCUT_A_CLORE_MAJ'),projet.lexique.entree('MOT_RAPPORT'),ficRapport)
              self.baseFT[self.tagActFT[TAGaction]]['LIBELLE']+=correction
              gestionFT=(FT,None,libelleFT,numFT,ftbase,lerang,lafiche,action)
              self.rapportMantis(ficRapport,etatFT,numFT)

      elemMatTrac=(lerang,lafiche,lescenario,qualif,fiche,(numFT,etatFT,parallel),RBilan['CTL_EXIGENCES'])
    return FTconfirme,FTreserve,gestionFT,numFT,etatFT,elemMatTrac

#-----------------------------------------------------------------------------------
# FIN
