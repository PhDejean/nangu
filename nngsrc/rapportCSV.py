#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,datetime,glob,tarfile
import nngsrc.services 

#-------------------------------------------------------------------------------
REPERTRAPPORT="CSV"
DOCEXTENSION=".csv"

LISTE_EXTENSION = [ DOCEXTENSION ]

#-------------------------------------------------------------------------------
# Fonction de formatage des chaines pour sortie rapport
#-------------------------------------------------------------------------------
def formateChaineCSV(objet):
  fmtCh(objet)
#-------------------------------------------------------------------------------
def fmtCh(objet):
  if objet is None:
    return objet
  if type(objet)!=str:
    chaine=str(objet)
  else:
    chaine=objet
  return chaine

#-------------------------------------------------------------------------------
def IT(chaine):
  return chaine

#-------------------------------------------------------------------------------
def BF(chaine):
  return chaine

#-------------------------------------------------------------------------------
def TT(chaine):
  return chaine

#-------------------------------------------------------------------------------
def FT(chaine):
  return chaine

#-------------------------------------------------------------------------------
def FTTT(chaine):
  return FT(TT(chaine))

#-------------------------------------------------------------------------------
def BFTT(chaine):
  return BF(TT(chaine))

#-------------------------------------------------------------------------------
def centrer(texte,taille):
  blancs=" "*((taille-len(texte))//2)
  return blancs, texte

#-----------------------------------------------------------------------------------
# Classe du rapport
#-----------------------------------------------------------------------------------
class CSV:
  repTravail=None
  repRapport=None
  refFT=None
  lexique=None
  ficBilan=None
  ficBilanMail=None

  #---------------------------------------------------------------------------------
  # Initialisation de la classe
  #---------------------------------------------------------------------------------
  def __init__(self,repTravail,repRessources,repRapport,ficRapport,projet,qualite,version,bilan,constantes,elem):
    # Parametres communs du rapport
    self.repTravail=repTravail
    self.repRapport=repRapport
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.repCampagne=bilan

    # Parametres specifique du rapport
    self.ficRapport=fmtCh(ficRapport)
    self.ficBilan=os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+DOCEXTENSION)
    ficMail='-'.join(ficRapport.split('-')[:-1])
    self.ficBilanMail=os.path.join(self.repTravail,REPERTRAPPORT,"%s.txt"%ficMail)

    # Creation des repertoires
    if not os.path.exists(os.path.join(self.repTravail,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repTravail,REPERTRAPPORT))
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*%s'%DOCEXTENSION)):
      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
    if not os.path.exists(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT)):
      os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))

    # Elements des Faits techniques
    if self.projet.REF_FT is None:
      self.refFT="%s-FT"%(self.projet.VALIDATION_REFERENCE+'-'+self.projet.DOCUMENT_EDITION+'-'+self.projet.DOCUMENT_REVISION)
    else:
      self.refFT=self.projet.REF_FT

    # Titre, sous-tire version et nom du projet
    # Sans objet

    # Type de document
    # Sans objet

  #-------------------------------------------------------------------------------
  # formatage du lexique
  #-------------------------------------------------------------------------------
  def formateLexique(self,motclef):
    return fmtCh(self.projet.lexique.entree(motclef))

  #-------------------------------------------------------------------------------
  def rapport_bilan(self,nbcol,lmax,ficBilan,ficBilanMail,texte,valeur=None,soit=None,pourcent=None,italique=False):
    if italique:
      ligne=u"-%s"%texte
      mail=u"        -%s%s"%(texte," "*(lmax-len(texte)-5))
    else:
      ligne=u"%s"%texte
      mail=u"    %s%s"%(texte," "*(lmax-len(texte)))

    if valeur=='':
      colonne=''
    else:
      colonne="%d"%valeur
      mail+="%4d"%int(colonne)
    ligne+=",%s"%colonne
    if pourcent is not None:
      if pourcent=='':
        colonne=''
      else:
        colonne=" %s %5.1f%%"%(self.projet.lexique.entree('MOT_SOIT'),100*pourcent)
      ligne+=",%s"%colonne
      mail+="%s"%colonne

    ficBilan.write("%s\n"%ligne)
    ficBilanMail.write("%s\n"%mail)

  #-------------------------------------------------------------------------------
  # Bilan de campagne § Couverture des exigences
  #-------------------------------------------------------------------------------
  def Campagne_CouvertureExigences(self,MatExigences,fichierGrExig):
    f=open(os.path.join(self.repTravail,REPERTRAPPORT,"exigencesCouvertes.csv"),"a",encoding='utf-8')
    f.write("%s;%s;%s;%s;%s\n"%(self.projet.lexique.entree('MOT_EXIGENCE'),self.projet.lexique.entree('MOT_OK'),self.projet.lexique.entree('MOT_KO'),self.projet.lexique.entree('LOCUT_ID_TODO'),self.projet.lexique.entree('LOCUT_NON_COUVERTES')))
    for ex in nngsrc.services.SRV_listeKeysDict(MatExigences):
      if MatExigences[ex] is not None:
        (sev,mant)=MatExigences[ex]
        if mant=='':
          f.write("%s;%s;%s;%s;%s\n"%(fmtCh(ex),"X","","",""))
        else: # TODO : verifier AV et NT
          if sev=='': sev=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_0')
          f.write("%s;%s;%s;%s;%s\n"%(fmtCh(ex),"",sev,fmtCh(mant),""))
      else:
          f.write("%s;%s;%s;%s;%s\n"%(fmtCh(ex),"","","","X"))
    f.close()

  #-------------------------------------------------------------------------------
  # Fin du bilan de campagne
  #-------------------------------------------------------------------------------
  def Campagne_Terminer(self,campagne):
    if not os.access(self.repCampagne,os.F_OK):
      os.mkdir(self.repCampagne)

    destination=os.path.join(self.repCampagne,REPERTRAPPORT,campagne.refCampagne+'-%04d'%campagne.iterCampagne)
    if not os.access(destination,os.F_OK):
      if not os.access(os.path.join(self.repCampagne,REPERTRAPPORT),os.F_OK):
        os.mkdir(os.path.join(self.repCampagne,REPERTRAPPORT))
      os.mkdir(destination)

    # on deplace les rapports dans le repertoire final
    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*.*')):
      ficDestination=os.path.join(destination,os.path.basename(elem))
      if os.access(ficDestination,os.F_OK):
        nngsrc.services.SRV_detruire(ficDestination,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

  #-------------------------------------------------------------------------------
  # Mise à jour et fin
  #-------------------------------------------------------------------------------
  def Valid_Terminer(self,bilan,nbControles,IDcahier):
    (total,nbCtrlBLOQ)=self.qualite.OkArAvPbNt
    if self.projet.execution=='R':
      n_OK=bilan["vert"]
      n_AR=bilan["violet"]
      n_AV=bilan["orange"]
      n_PB=bilan["rouge"]
      n_NT=bilan["noir"]
    else: # TODO Verifier ...
      n_OK="\\ \\ \\ \\ "
      n_AR="\\ \\ \\ \\ "
      n_AV="\\ \\ \\ \\ "
      n_PB="\\ \\ \\ \\ "
      n_NT="\\ \\ \\ \\ "

    # Fichier bilan mail
    lmax=len(self.formateLexique('LOCUT_NB_CTRL_NON_BLOQUANT'))+2
    m=open(self.ficBilanMail,"w",encoding='utf-8')
    m.write("==================================================================================\n")
    (blancs,texte)=centrer(self.formateLexique('TITRE2_BILAN_CHIFFRE'),82)
    m.write("%s%s\n"%(blancs,texte.upper()))
    (blancs,texte)=centrer(self.ficRapport,82)
    m.write("%s%s\n"%(blancs,texte))
    (blancs,texte)=centrer(self.formateLexique('LOCUT_TRAVAUX_REALISES_POSTE')+fmtCh(self.projet.COMPUTERNAME),82)
    m.write("%s%s\n"%(blancs,texte))
    (blancs,texte)=centrer(self.formateLexique('LOCUT_TRAVAUX_REALISES_COMPTE')+fmtCh(self.projet.USERNAME),82)
    m.write("%s%s\n"%(blancs,texte))
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()
    m.write(t.strftime("%d/%m/%Y - %H:%M:%S"))
    m.write("\n==================================================================================\n")
    m.write("    %s :\n"%self.formateLexique('LOCUT_COMPLETUDE_RAPPORT'))
    m.write("    -%s : %d/%d\n"%(self.formateLexique('MOT_CONTROLES'),total,nbControles))
    m.write("    -%s : %d/%d\n"%(self.formateLexique('MOT_EXIGENCES'),len(self.qualite.listeExig.keys()),len(self.qualite.exigenceVSft)))
    m.write("    -%s : %d/%d\n"%(self.formateLexique('MOT_FONCTIONS'),len(self.qualite.listeFct.keys()),len(self.qualite.fonctionVSft)))
    m.write("==================================================================================\n\n")

    # Fichier Bilan
    f=open(self.ficBilan,"w",encoding='utf-8')
    divisExig=len(self.qualite.listeExig.keys())
    if divisExig==0:
      divisExig=1
    divisExig=float(divisExig)
    divisFct=len(self.qualite.listeFct.keys())
    if divisFct==0:
      divisFct=1
    divisFct=float(divisFct)
    strLex=self.formateLexique('MOT_GENERALITES')
    f.write("%s\n"%strLex)
    m.write("%s\n"%strLex)
    m.write("%s\n"%("-"*len(strLex)))
    self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_CONTROLES_PLAN'),nbControles)
    self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_CONTROLES_DOCUMENT'),total)
    self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"])
    if self.projet.PRODUIT_EXIGENCES_TRACEES:
      m.write("\n")
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_TOTAL_EXIG'),len(self.qualite.exigenceVSft))
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys()))
    if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
      m.write("\n")
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_TOTAL_FCT'),len(self.qualite.fonctionVSft))
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys()))
    m.write("\n")
    if self.projet.execution in ['R','C','M']:
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_COUVERTS'),len(self.qualite.listeFT.keys()))
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_OUVERTS_BASE'),self.qualite.nbFTenCours)
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_SUSPENDUS_BASE'),self.qualite.nbFTSuspendus)
    if self.projet.execution=='R':
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_OUVERTS_NOUVEAUX'),self.qualite.newNumFT-self.qualite.NUM_FT)
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_OUVERTS_DEJA_EMIS'),self.qualite.nbFTdejaEmis)
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_REOUVERTS'),len(self.qualite.lstFTreOuvert))
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_A_CLORE'),len(self.qualite.lstFTaClore))
    elif self.projet.execution=='C':
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_OUVERTS_NOUVEAUX'),'')
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_OUVERTS_DEJA_EMIS'),'')
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_REOUVERTS'),'')
      self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_A_CLORE'),'')

    if self.projet.execution in ['R','C','M']:
      strLex=self.formateLexique('MOT_CONTROLES')
      f.write("\n%s\n" % strLex)
      m.write("\n%s\n" % strLex)
      m.write("%s\n" % ("-" * len(strLex)))
      if self.projet.execution=='R':
        if total!=0:
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_OK'),n_OK,self.formateLexique('MOT_SOIT'),n_OK/float(total))
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_ECHEC'),n_AV+n_PB+n_NT,self.formateLexique('MOT_SOIT'),(n_AV+n_PB+n_NT)/float(total))
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_AV'),n_AV,self.formateLexique('MOT_SOIT'),n_AV/float(total),True)
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NOK'),n_PB,self.formateLexique('MOT_SOIT'),n_PB/float(total),True)
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NT'),n_NT,self.formateLexique('MOT_SOIT'),n_NT/float(total))
          self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NON_BLOQUANT'),nbCtrlBLOQ,self.formateLexique('MOT_SOIT'),(total-nbCtrlBLOQ)/float(total))
      else:
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_OK'),'',self.formateLexique('MOT_SOIT'),'')
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_ECHEC'),'',self.formateLexique('MOT_SOIT'),'')
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_AV'),'',self.formateLexique('MOT_SOIT'),'',True)
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NOK'),'',self.formateLexique('MOT_SOIT'),'',True)
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NT'),'',self.formateLexique('MOT_SOIT'),'')
        self.rapport_bilan(4,lmax,f,m,self.formateLexique('LOCUT_NB_CTRL_NON_BLOQUANT'),'',self.formateLexique('MOT_SOIT'),'')

      strLex=self.formateLexique('LOCUT_FT_SUR_ACTIONS')
      f.write("\n%s\n" % strLex)
      m.write("\n%s\n" % strLex)
      m.write("%s\n" % ("-" * len(strLex)))
      if self.projet.execution=='R':
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_CRITIQUES_OUVERTS'),self.qualite.couvertureFT[4])
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_BLOQUANTS_OUVERTS'),self.qualite.couvertureFT[3])
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_MAJEURS_OUVERTS'),self.qualite.couvertureFT[2])
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_MINEURS_OUVERTS'),self.qualite.couvertureFT[1])
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_NQ_OUVERTS'),self.qualite.couvertureFT[0])
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_EMIS_SUSPENDUS'),self.qualite.couvertureFT[5])
      else:
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_CRITIQUES_OUVERTS'),'')
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_BLOQUANTS_OUVERTS'),'')
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_MAJEURS_OUVERTS'),'')
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_MINEURS_OUVERTS'),'')
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_NQ_OUVERTS'),'')
        self.rapport_bilan(2,lmax,f,m,self.formateLexique('LOCUT_NB_FT_EMIS_SUSPENDUS'),'')

      strLex=self.formateLexique('LOCUT_FT_SUR_EXIG')
      f.write("\n%s\n" % strLex)
      m.write("\n%s\n" % strLex)
      m.write("%s\n" % ("-" * len(strLex)))
      self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"])
      self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_NB_EXIG_COUVERTES'),len(self.qualite.listeExig.keys()))
      m.write("\n")
      if self.projet.execution=='R':
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES'),self.qualite.couvertureExig[4],None,self.qualite.couvertureExig[4]/divisExig)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS'),self.qualite.couvertureExig[3],None,self.qualite.couvertureExig[3]/divisExig)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS'),self.qualite.couvertureExig[2],None,self.qualite.couvertureExig[2]/divisExig)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_MINEURS'),self.qualite.couvertureExig[1],None,self.qualite.couvertureExig[1]/divisExig)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_NQ'),self.qualite.couvertureExig[0],None,self.qualite.couvertureExig[0]/divisExig)
      else:
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_MINEURS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_EXIG_AVEC_FT_NQ'),'',None,'')

      strLex=self.formateLexique('LOCUT_FT_SUR_FCT')
      f.write("\n%s\n" % strLex)
      m.write("\n%s\n" % strLex)
      m.write("%s\n" % ("-" * len(strLex)))
      self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_NB_ACTIONS_DOCUMENT'),bilan["actions"])
      self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_NB_FCT_COUVERTES'),len(self.qualite.listeFct.keys()))
      m.write("\n")
      if self.projet.execution=='R':
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_CRITIQUES'),self.qualite.couvertureFonc[4],None,self.qualite.couvertureFonc[4]/divisFct)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_BLOQUANTS'),self.qualite.couvertureFonc[3],None,self.qualite.couvertureFonc[3]/divisFct)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_MAJEURS'),self.qualite.couvertureFonc[2],None,self.qualite.couvertureFonc[2]/divisFct)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_MINEURS'),self.qualite.couvertureFonc[1],None,self.qualite.couvertureFonc[1]/divisFct)
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_NQ'),self.qualite.couvertureFonc[0],None,self.qualite.couvertureFonc[0]/divisFct)
      else:
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_CRITIQUES'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_BLOQUANTS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_MAJEURS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_MINEURS'),'',None,'')
        self.rapport_bilan(3,lmax,f,m,self.formateLexique('LOCUT_TAUX_FCT_AVEC_FT_NQ'),'',None,'')

      if IDcahier is not None:
        strLex=self.formateLexique('LOCUT_IDENTIFIANT_CAHIER')
        f.write("\n%s : %s\n"%(strLex,IDcahier))
    f.close()
    m.write("\n==================================================================================\n")
    m.close()

    self.MatriceTracabilite()

    # on deplace les rapports dans le repertoire final
    destination=os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT,self.ficRapport)
    if not os.access(destination,os.F_OK):
      if not os.access(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT),os.F_OK):
        os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))
      os.mkdir(destination)

    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,"*%s"%DOCEXTENSION)):
      ficdest=os.path.join(destination,os.path.basename(elem))
      if os.access(ficdest,os.F_OK):
        nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
      nngsrc.services.SRV_deplacer(elem,destination,timeout=5,signaler=True)

    ficdest=os.path.join(destination,'..',os.path.basename(self.ficBilanMail))
    if os.access(ficdest,os.F_OK):
      nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
    nngsrc.services.SRV_deplacer(self.ficBilanMail,os.path.join(destination,'..'),timeout=5,signaler=True)
    self.ficBilanMail=ficdest

    os.rmdir(os.path.join(self.repTravail,REPERTRAPPORT))

    # Archivage
    if self.projet.ARCHIVAGE:
      if not self.projet.execution in ['X','K']:
        fichier='-'.join(self.ficRapport.split('-')[:-1])
        fichCmplt=os.path.join(self.repRapport,self.projet.execution,"CSV",fichier+".tgz")
        if os.access(fichCmplt,os.F_OK):
          nngsrc.services.SRV_detruire(fichCmplt,timeout=5,signaler=True)
        ficTGZ=tarfile.open(fichCmplt,mode="w:gz",encoding='utf-8')
        ficTGZ.add(os.path.join(self.repRapport,self.projet.execution,"CSV",self.ficRapport),arcname=self.ficRapport)
        ficTGZ.add(os.path.join(self.repRapport,self.projet.execution,"CSV",fichier+".txt"),arcname=fichier+".txt")
        ficTGZ.close()
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ARCHIVE_DANS')%fichCmplt,style="r")

  #-------------------------------------------------------------------------------
  def sousMatriceDirecte(self,f,fiche,tag,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,decline,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.formateLexique('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Reference de l'action
    f.write('%s'%(os.path.basename(fiche)+tag+extAction))
    # Methode, typo
    f.write(';%s'%fmtCh(methode))
    f.write(';%s'%fmtCh(typo))
    # Si mode R, Etat de la FT, contrôleur et date
    if self.projet.execution=='R':
      f.write(';%s'%strFT)
    elif self.projet.execution=='C':
      f.write(';')
    # Exigence et type d'exigence
    f.write(';%s'%exigFct)
    if decline:
      if dictExigFct[exigFct]['parent'] is not None:
        f.write(";%s"%dictExigFct[exigFct]['parent'][0])
      else:
        f.write(";")
    if exigFct=='': f.write(';_')
    elif exigFct in dictExigFct:
      f.write(';%s'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Severite
    f.write(';%s'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write(';%s'%' '.join(n for n in faitTechCouv))
      else:
        f.write(';-')
    f.write('\n')

  #-------------------------------------------------------------------------------
  def sousMatriceInverse(self,f,fiche,tag,methode,typo,faitTechCouv,numFT,etatFT,exigFct,dictExigFct,decline,severite,extAction):
    # Etat de la FT : aucune FT, deja emise, emise
    if etatFT=='LOCUT_A_CLORE':
      numFT=None
    if numFT is None:
      strFT='-'
    else:
      if numFT<0:
        numFT=-numFT
        strFT='%s-%06d (%s)'%(fmtCh(self.refFT),numFT,self.formateLexique('LOCUT_DEJA_EMIS'))
      else:
        strFT='%s-%06d'%(fmtCh(self.refFT),numFT)
    # Exigence et type d'exigence
    f.write('%s'%exigFct)
    if decline:
      if dictExigFct[exigFct]['parent'] is not None:
        f.write(";%s"%dictExigFct[exigFct]['parent'][0])
      else:
        f.write(";")
    if exigFct=='': f.write('_')
    elif exigFct in dictExigFct:
      f.write(';%s'%fmtCh(', '.join(dictExigFct[exigFct]['methode'])))
    # Reference de l'action
    f.write(';%s'%(os.path.basename(fiche)+tag+extAction))
    # Methode, typo
    f.write(';%s'%fmtCh(methode))
    f.write(';%s'%fmtCh(typo))
    # Si mode R, Etat de la FT, contrôleur et date
    if self.projet.execution=='R':
      f.write(';%s'%strFT)
    elif self.projet.execution=='C':
      f.write(';')
    # Severite
    f.write(';%s'%severite)
    # FT couvertes
    if not self.projet.execution in ['P','S']:
      if len(faitTechCouv)>0:
        f.write(';%s'%' '.join(n for n in faitTechCouv))
      else:
        f.write(';-')
    f.write('\n')

  #-------------------------------------------------------------------------------
  def MatriceTracabilite(self):
    #--- OUVERTURE ET PREPARATION DU FICHIER
    nc=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repTravail,'nonCouv.txt'))

    #--- LISTE DE NON COUVERTURE DES EXIGENCES -------------------------------------------------------------------------------
    if self.projet.MATTRAC_NON_CVT:
      if self.projet.PRODUIT_EXIGENCES_TRACEES:
        f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-ncE"+DOCEXTENSION),"w",encoding='utf-8')
        ind=1
        while nc[ind].find("--Fonctionnalites non couvertes--")!=0:
          f.write("%s\n"%nc[ind].replace('\n','').replace('\r',''))
          ind+=1
        f.close()

    #--- LISTE DE NON COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------------------
      if self.projet.PRODUIT_FONCTIONNALITES_TRACEES:
        f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-ncF"+DOCEXTENSION),"w",encoding='utf-8')
        ind+=1
        while ind<len(nc):
          f.write("%s\n"%nc[ind].replace('\n','').replace('\r',''))
          ind+=1
        f.close()

    #--- MATRICE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_EXG:
      f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-MTCvE"+DOCEXTENSION),"w",encoding='utf-8')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_EXIGENCES'),self.formateLexique('LOCUT_EXIGENCES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_EXIGENCES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_EXIGENCES'),self.formateLexique('LOCUT_EXIGENCES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE')))
        else:
          f.write("%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_EXIGENCES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        # On parcours les actions de niveau k
        for act in self.qualite.matTracabilite[k]:
          ((_,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,exigences)=act
          _,methode,typo,_,_,exigGlobales=desc
          exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
          faitTechCouv=faitTechCouv.split(',')
          listNumFT=[]
          if numFT is not None: listNumFT.append(numFT)
          while '' in faitTechCouv: faitTechCouv.remove('')
          ficheTagFiche="%s%s"%(fiche,tagFiche)
          if True: #numFT:
            for eg in exigGlobales:
              if eg!='':
                if eg not in ListeExigGlobales:
                  ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                else:
                  precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                  if k>precedk or numFT is not None:
                    if numFT is not None:
                      precednumFT.append(numFT)
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
          exigences=exigences.replace(';',',').replace('\\n',',').split(',')
          while '' in exigences: exigences.remove('')
          for e in exigences:
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((_,fiche,scenario,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%fmtCh(self.projet.lexique.entree('LOCUT_EXIG_GLOBALE')))
          else:
            for numFT in listNumFT:
              self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%fmtCh(self.projet.lexique.entree('LOCUT_EXIG_GLOBALE')))
      f.close()

    #--- MATRICE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------------
    if self.projet.MATTRAC_CTL_FCT:
      f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-MTCvF"+DOCEXTENSION),"w",encoding='utf-8')
      # En exécution R ou C on ajoute les colonnes fait tech., controleur et date
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('LOCUT_FONCTIONNALITES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('LOCUT_FONCTIONNALITES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE')))
        else:
          f.write("%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_SEVERITE')))
      for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
        severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
        if k==0: severite=IT(severite)
        for act in self.qualite.matTracabilite[k]:
          ((_,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,_)=act
          entite,methode,typo,_,_,_=desc
          if action=="act1": #action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            self.sousMatriceDirecte(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,fmtCh(entite),self.projet.fonctionnalites,self.projet.PRODUIT_FONCTIONNALITES_DECLINEES,severite,'')
      f.close()

    #--- MATRICE INVERSE DE COUVERTURE DES EXIGENCES -------------------------------------------------------------------------
    if self.projet.MATTRAC_EXG_CTL:
      f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-MTEvC"+DOCEXTENSION),"w",encoding='utf-8')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_EXIGENCES'),self.formateLexique('LOCUT_EXIGENCES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_EXIGENCES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_EXIGENCES'),self.formateLexique('LOCUT_EXIGENCES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_SEVERITE')))
        else:
          f.write("%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_EXIGENCES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_SEVERITE')))
      ListeExigGlobales={}
      # On parcours les niveaux de severite k de la matrice
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeExig):
        # On parcours les actions de niveau k
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          if k==0: severite=IT(severite)
          for act in self.qualite.matTracabilite[k]:
            ((_,fiche,scenario,tagFiche,(action,tagAction),desc,faitTechCouv),numFT,etatFT,exigences)=act
            _,methode,typo,_,_,exigGlobales=desc
            faitTechCouv=faitTechCouv.split(',')
            listNumFT=[]
            if numFT is not None: listNumFT.append(numFT)
            while '' in faitTechCouv: faitTechCouv.remove('')
            ficheTagFiche="%s%s"%(fiche,tagFiche)
            if True: #numFT: # if action == "act1":  # action.find("act1")!=-1: # EMETSAT_DEMO/!\ Revoir ça avec les boucles !!!!
              exigGlobales=exigGlobales.replace(';',',').replace('\\n',',').split(',')
              for eg in exigGlobales:
                if eg!='':
                  if eg not in ListeExigGlobales:
                    ListeExigGlobales[eg]={ficheTagFiche:(k,severite,listNumFT,act)}
                  elif ficheTagFiche not in ListeExigGlobales[eg]:
                    ListeExigGlobales[eg][ficheTagFiche]=(k,severite,listNumFT,act)
                  else:
                    precedk,_,precednumFT,_=ListeExigGlobales[eg][ficheTagFiche]
                    if k>precedk or numFT is not None:
                      if numFT is not None and numFT not in precednumFT:
                        precednumFT.append(numFT)
                      ListeExigGlobales[eg][ficheTagFiche]=(k,severite,precednumFT,act)
            exigences=exigences.replace(';',',').replace('\\n',',').split(',')
            while '' in exigences: exigences.remove('')
            if e in exigences:
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%action)
      for eg in ListeExigGlobales:
        for ficheTagFiche in ListeExigGlobales[eg]:
          _,severite,listNumFT,act=ListeExigGlobales[eg][ficheTagFiche]
          ((_,fiche,_,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
          _,methode,typo,_,_,_=desc
          faitTechCouv=faitTechCouv.split(',')
          while '' in faitTechCouv: faitTechCouv.remove('')
          if len(listNumFT)==0:
            self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,None,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%self.formateLexique('LOCUT_EXIG_GLOBALE'))
          else:
            for numFT in listNumFT:
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,eg,self.projet.exigences,self.projet.PRODUIT_EXIGENCES_DECLINEES,severite,' [%s]'%self.formateLexique('LOCUT_EXIG_GLOBALE'))
      f.close()

    #--- MATRICE INVERSE DE COUVERTURE DES FONCTIONNALITES -------------------------------------------------------------------
    if self.projet.MATTRAC_FCT_CTL:
      f=open(os.path.join(self.repTravail,REPERTRAPPORT,self.ficRapport+"-MTFvC"+DOCEXTENSION),"w",encoding='utf-8')
      if self.projet.execution in ['R','C']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('LOCUT_FONCTIONNALITES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
        else:
          f.write("%s;%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('LOCUT_FAIT_TECH'),
                   self.formateLexique('MOT_SEVERITE'),self.formateLexique('LOCUT_FAITS_TECHS_COUVERTS')))
      elif self.projet.execution in ['P','S']:
        if self.projet.PRODUIT_FONCTIONNALITES_DECLINEES:
          f.write("%s;%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('LOCUT_FONCTIONNALITES_PARENTES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_SEVERITE')))
        else:
          f.write("%s;%s;%s;%s;%s;%s\n"%
                  (self.formateLexique('MOT_FONCTIONNALITES'),self.formateLexique('MOT_METHODE'),
                   self.formateLexique('MOT_CONTROLE'),self.formateLexique('MOT_METHODE'),self.formateLexique('MOT_TYPE'),
                   self.formateLexique('MOT_SEVERITE')))
      for e in nngsrc.services.SRV_listeKeysDict(self.qualite.listeFct):
        for k in nngsrc.services.SRV_listeKeysDict(self.qualite.matTracabilite):
          if k==0: severite=IT(self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k))
          else: severite=self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%k)
          listeFCT=[]
          for act in self.qualite.matTracabilite[k]:
            ((_,fiche,scenario,tagFiche,_,desc,faitTechCouv),numFT,etatFT,_)=act
            entite,methode,typo,_,_,_=desc
            faitTechCouv=faitTechCouv.split(',')
            while '' in faitTechCouv: faitTechCouv.remove('')
            if e==entite and not fiche in listeFCT:
              listeFCT.append(fiche)
              self.sousMatriceInverse(f,fiche,tagFiche,methode,typo,faitTechCouv,numFT,etatFT,e,self.projet.fonctionnalites,self.projet.PRODUIT_FONCTIONNALITES_DECLINEES,severite,'')

      f.close()

#-------------------------------------------------------------------------------
# FIN
