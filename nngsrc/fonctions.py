#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os, shutil, time, stat, json, copy, glob
import nngsrc.services
import nngsrc.servicesSons
import nngsrc.servicesXML

NANGU_NETCDF4=True
try:
  import netCDF4
except:
  nngsrc.services.SRV_Print(nngsrc.services.FRED+nngsrc.services.SNORMAL+"RESTRICTION (Nangu) : Module 'netCDF4' non installé.")
  nngsrc.services.SRV_Print(nngsrc.services.FRED+nngsrc.services.SNORMAL+"              => La fonction de validation 'VALID_InspecteNetCDF' ne sera pas utilisable.")
  NANGU_NETCDF4=False

ERR_PARAMETRE          = (-1,"ERR_PARAMETRE")
ERR_TIMEOUT            = (-2,"ERR_TIMEOUT")
ERR_TROP_PARAMETRE     = (-3,"ERR_TROP_PARAMETRE")
ERR_PERMISSION         = (-4,"ERR_PERMISSION")
ERR_DESTRUCTION        = (-5,"ERR_DESTRUCTION")
ERR_EXISTE_DESTINATION = (-6,"ERR_EXISTE_DESTINATION")
ERR_ACCES_SOURCE       = (-7,"ERR_ACCES_SOURCE")
ERR_COPIE_DEPLACE_CREE = (-8,"ERR_COPIE_DEPLACE_CREE")

# Remplacement des caractères des séquences de Colorama :
COLORseq=[('\033[1m' ,''),
          ('\033[22m',''),
          ('\033[31m',''),
          ('\033[32m',''),
          ('\033[33m',''),
          ('\033[34m',''),
          ('\033[36m',''),
          ('\033[37m',''),
          ('\033[40m','')
         ]

#-------------------------------------------------------------------------------

listTag=['CTL_COMMENT',     # Contrôle      : Commentaire du contrôle
         'CTL_RESULTAT',    # Contrôle      : Résultat du contrôle
         'CTL_INDEX',       # Contrôle      : Index du contrôle
         'CTL_ID',          # Contrôle      : Identifiant du contrôle
         'CTL_CONDITION',   # Contrôle      : Condition d'exécution du contrôle
         'CTL_TAG',         # Contrôle      : TAG de l'action
         'CTL_EXIGENCES',   # Contrôle      : Exigences du contrôle
         'CTL_FT',          # Contrôle      : Faits techniques du contrôle
         'CTL_LIBELLE',     # Contrôle      : Libellé éventuel de la fonction du contrôle
         'CTL_FTQUALIF',    # Contrôle      : Qualité du fait technique généré
         'CTL_FTQUALIF',    # Controle      : Qualité du fait technique généré
         'CTL_RAPPORT',     # Contrôle      : Prise en compte dans les rapports
         'CTL_VALIDER',     # Contrôle      : Pour décider du constat de validation
         'CTL_VALRAISON',   # Contrôle      :   Raison de la validation utilisateur
         'CTL_BOUCLE',      # Contrôle      : Boucle TantQue sur une action
         'CTL_SCRIPT',      # Contrôle      : Elements de creation des scripts python
         'CTL_FTTQ',        # Contrôle      : Condition d'activation des FT sur boucle TantQue

         'EXE_IHM',         # Execution     : Action IHM à exécuter
         'EXE_COMMANDE',    # Execution     : Commande à exécuter
         'EXE_FICHIER',     # Execution     : Une donnée fichier pour support d'exécution
         'EXE_FICHREP',     # Execution     : Fichier ou répertoire traité
         'SUP_FICHREP',     # Execution     : Fichier ou répertoire supprimé
         'EXE_CHERCHER',    # Execution     : Chercher une liste de chaines

         'PAR_SOURCE',      # Paramètre     : Identification d'une donnée en tant que source
         'PAR_DESTINATION', # Paramètre     : Identification d'une donnée en tant que destination
         'PAR_CASSE',       # Paramètre     : Respecter la casse dans la recherche de chaine
         'PAR_CASSEP',      # Paramètre     : Respecter la casse dans les paramètres

         'PAR_DEBUT',       # Paramètre     : Ligne de début de bloc
         'PAR_FIN',         # Paramètre     : Ligne de fin de bloc
         'PAR_TEXTE',       # Paramètre     : Texte des lignes d'exception
         'PAR_LIGNES',      # Paramètre     : Lignes d'exception
         'PAR_REMP',        # Paramètre     : Remplacement de texte lors de comparaisons

         'RES_RETOUR',      # Résultat      : Retour de la commande
         'RES_SORTIE',      # Résultat      : Sortie de la commande
         'RES_CONSTAT',     # Résultat      : Constat
         'RES_OCCURENCES',  # Résultat      : Nombre d'occurences rencontrées
         'RES_PRODUIT',     # Résultat      : Donnée RES_PRODUITe
         'RES_PRESENCE',    # Résultat      : Disponibilité d'un élément dont on doit contrôler la présence
         'RES_ETAT',        # Résultat      : Etat du test présence vs disponibilité

         'ATD_REFERENCE',   # Attendu       : Donnée de ATD_REFERENCE
         'ATD_ACCES',       # Attendu       : Mode d'accès aux fichiers
         'ATD_RETOUR',      # Attendu       : Retour attendu
         'ATD_RETOURS_OK',  # Attendu       : Liste de retours corrects attendu
         'ATD_RETOURS_NOK', # Attendu       : Liste de retours incorrects attendu
         'ATD_RETOURS_AV',  # Attendu       : Liste de retours à valider
         'ATD_CONSTAT',     # Attendu       : Constat attendu
         'ATD_OCCURENCES',  # Attendu       : Nombre d'occurences attendues
         'ATD_PRESENCE',    # Attendu       : Liste de présence ou abscence de chaine

         'DOC_FICHIER',     # Documentation : Identification d'une donnée texte ou image pour le rapport
         'DOC_CONTENU',     # Documentation : Identification du contenu d'une donnée fichier pour le rapport
         'DOC_MESSAGE'      # Documentation : Message pour le rapport
         ]
#-------------------------------------------------------------------------------
def messageErreur(code):
  if code=="-1": return "ERR_PARAMETRE"
  if code=="-2": return "ERR_TIMEOUT"
  if code=="-3": return "ERR_TROP_PARAMETRE"
  if code=="-4": return "ERR_PERMISSION"
  if code=="-5": return "ERR_DESTRUCTION"
  if code=="-6": return "ERR_EXISTE_DESTINATION"
  if code=="-7": return "ERR_ACCES_SOURCE"
  if code=="-8": return "ERR_COPIE_DEPLACE_CREE"
  return None

#-------------------------------------------------------------------------------
def nbLignes(RES_PRODUIT):
    if RES_PRODUIT is None: return None
    nb=None
    if os.path.isfile(RES_PRODUIT) and os.access(RES_PRODUIT,os.F_OK):
      nb=-1
      fr=open(RES_PRODUIT,'r',encoding='utf-8')
      ligne="vide"
      while ligne!='':
        nb+=1
        try:
          ligne=nngsrc.services.SRV_Encodage(fr.readline())#.decode('utf-8')
        except Exception:
          print("Erreur de lecture d'une ligne de %s."%RES_PRODUIT)
          fr.close()
          return nb
      fr.close()
    return nb

#-------------------------------------------------------------------------------
def nbTrouves(ligne,pattern):
    nbtrouve=0
    idx=ligne.find(pattern)
    while idx != -1:
      nbtrouve=nbtrouve+1
      ligne=ligne[idx+1:]
      idx=ligne.find(pattern)
    return nbtrouve

#-------------------------------------------------------------------------------
def run(mode,app,args):
    try:
        addargs=[app]
        addargs.extend(args)
        ret=os.spawnv(mode,app,tuple(addargs))
        if mode==os.P_NOWAIT:
          return 0
        else:
          return ret
    except os.error:
        pass

#-------------------------------------------------------------------------------
# Change me mode d'execution si rapportManuel
def ExecutionMode(execution,rapportManuel):
  if rapportManuel:
    return 'M'
  return execution

#-------------------------------------------------------------------------------
def AnalyseTrace(parCommande,idxAction):
  trace=parCommande.find(">")>0
  traceErr="temp%s.err"%idxAction
  ajoutErr=True
  traceOut="temp%s.out"%idxAction
  ajoutOut=True
  if trace:
    idx=parCommande.find(">")
    while idx!=-1:
      liste=parCommande[idx+1:].split(' ')
      while liste[0]=='':liste.pop(0)
      if parCommande[idx-1]=="2":
        traceErr=liste[0]
        ajoutErr=False
      else:
        traceOut=liste[0]
        ajoutOut=False
      idx=parCommande.find(">",idx+1)
  command=parCommande
  if ajoutOut: command=command+" >%s"%traceOut
  if ajoutErr: command=command+" 2>%s"%traceErr

  return trace, traceOut, traceErr, command

#-------------------------------------------------------------------------------
def printFonction(fonction,fiche,continuer,commande,varoperateur,CTL_INDEX,CTL_IDENT,CTL_CONDITION,CTL_FTTQ):
  print("----------------------------------------------------------")
  print("FONCTION       ",fonction)
  print("  FICHE        ",fiche)
  print("  LANCER       ",continuer)
  print("  COMMANDE     ")
  clefs=commande.keys()
#  clefs.sort()
  for key in clefs:
    print("    %s : %s"%(key,commande[key]))
  print("  varOPERATEUR ",varoperateur)
  print("  CTL_INDEX    ",CTL_INDEX)
  print("  CTL_IDENT    ",CTL_IDENT)
  print("  CTL_CONDITION",CTL_CONDITION)
  print("  CTL_FTTQ     ",CTL_FTTQ)
  print("  EXECUTION :")

#-------------------------------------------------------------------------------
def printParams(params):
  print("    PARAMS ",params)
#  print "    commande RAPPORT :",commande['RAPPORT']
#  print "    RES_RETOUR       :",RES_RETOUR

#-------------------------------------------------------------------------------
def forcageDroitsTimer(pControle,pNumParam):
  paramForce=False
  paramTimeOut=0
  if len(pControle.CTL_PARAMETRES)==pNumParam+1:
    param=pControle.CTL_PARAMETRES[pNumParam]
    if param=="FORCE":
      paramForce=True
    elif pControle.CTL_PARAMETRES[pNumParam].isdigit():
      paramTimeOut=int(pControle.CTL_PARAMETRES[pNumParam])
    else:
      print("\n/!\ Attention le paramètre '%s' n'est pas 'FORCE' ou un entier."%pControle.CTL_PARAMETRES[pNumParam])
  if len(pControle.CTL_PARAMETRES)==pNumParam+2:
    paramForce=pControle.CTL_PARAMETRES[pNumParam]=="FORCE"
    paramTimeOut=int(pControle.CTL_PARAMETRES[pNumParam+1])
  return paramForce,paramTimeOut

#-------------------------------------------------------------------------------
def restrictionsDebFin(pCommande,executionCMRY,reference,cible):
  nbLignesReference=nbLignes(reference)
  nbLignesCible=nbLignes(cible)
  calculDebut=1  # du début
  calculFin=0    # a la fin

  # Si un intervalle est précisé on en tient compte
  # Sinon, on prend tout le document
  if 'INTERVALLE' in pCommande:
    borne=pCommande['INTERVALLE'].split('|')[0]
    if borne!='':
      calculDebut=int(borne)
    borne=pCommande['INTERVALLE'].split('|')[1]
    if borne!='':
      calculFin=int(borne)

  # Si calculFin<=0 : c'est le nombre de lignes à partir de la fin
  if executionCMRY in ['R','M'] and calculFin<=0 and nbLignesCible is not None:
    calculFin=nbLignesCible+calculFin

  # Si calculDebut=0 : on considère la référence par rapport à calculFin et à la taille de l'extrait en référence
  if executionCMRY in ['R','M'] and calculDebut==0 and nbLignesReference is not None:
    calculDebut=calculFin-nbLignesReference+1

  return calculDebut,calculFin

#-------------------------------------------------------------------------------
def restrictions(pCommande,executionCMRY,reference,cible,projet):
  listeComplement=[]
  numLignesExclues=[]
  textesExclus=[]
  extensionsExclues=[]
  textesRemplaces=copy.deepcopy(COLORseq)
  rapFin=0

  calculDebut,calculFin=restrictionsDebFin(pCommande,executionCMRY,reference,cible)

  if calculFin!=0:
    if executionCMRY not in ['R','M'] and calculFin<0:
      rapFin=projet.lexique.entree('LOCUT_FIN_FICHIER')+str(calculFin)
    else:
      rapFin=str(calculFin)

  if 'INTERVALLE' in pCommande:
    if calculDebut==0:
      listeComplement.append(('LOCUT_DE_LA_LIGNE',rapFin+'-'+projet.lexique.entree('LOCUT_NB_LIGNES_REFERENCE')))
    else:
      listeComplement.append(('LOCUT_DE_LA_LIGNE',str(calculDebut)))
    if calculFin!=0:
      listeComplement.append(('LOCUT_A_LA_LIGNE',rapFin))

  # Recupération des numéros de lignes exclues
  if pCommande['EXCLURE_LIGNES']!='':
    liste=pCommande['EXCLURE_LIGNES'].split('|')
    for l in liste:
      numLignesExclues.append(int(l))
    listeComplement.append(('LOCUT_EXCEPTE_LES_LIGNES_NUMERO',pCommande['EXCLURE_LIGNES'].replace('|',', ')))

  # Recupération des extensions des fichiers exclus
  if 'EXCLURE_EXTENSIONS' in pCommande:
    extensionsExclues=pCommande['EXCLURE_EXTENSIONS'].split('|')
    listeComplement.append(('LOCUT_EXCEPTE_LES_FICHIERS',pCommande['EXCLURE_EXTENSIONS'].replace('|',', ')))

  # Recupération des textes des lignes exclues
  if 'EXCLURE_TEXTES' in pCommande:
    textesExclus=pCommande['EXCLURE_TEXTES'].split('|')
    listeComplement.append(('LOCUT_EXCEPTE_LES_LIGNES_CONTENANT',"'%s'"%pCommande['EXCLURE_TEXTES'].replace('|',', ')))

  # Recupération des textes à rempacer exclues
  if 'REMPLACER_TEXTES' in pCommande:
    liste=pCommande['REMPLACER_TEXTES'].split('|')
    rapTextesRemplaces=[]
    for remp in range(len(liste)//2):
      textesRemplaces.append((liste[remp*2],liste[remp*2+1]))
      rapTextesRemplaces.append((liste[remp*2],liste[remp*2+1]))
    listeComplement.append(('LOCUT_APRES_REMPLACEMENT_DE',rapTextesRemplaces))

  return calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement

#-------------------------------------------------------------------------------
def reduitFichier(ficSrc,ficDst,parametres):
  DEB,FIN,EXCLURE_LIGNES,EXCLURE_TEXTES,REMPLACER_TEXTES=parametres
  if os.path.exists(ficDst):
    nngsrc.services.SRV_detruire(ficDst,timeout=5,signaler=True)
  if os.path.exists(ficSrc):
    fr=open(ficSrc,'r',encoding='utf-8')
    fw=open(ficDst,'w',encoding='utf-8')
    l=1
    ligne="vide"
    while DEB!='' and l<DEB:
      ligne=nngsrc.services.SRV_Encodage(fr.readline())#.decode('utf-8')
      l+=1
    while (FIN=='' or l<=FIN) and ligne!='':
      try:
        ligne=nngsrc.services.SRV_Encodage(fr.readline())#.decode('utf-8')
      except Exception:
        nngsrc.services.SRV_Terminal(None,None,None,"ATTENTION la ligne %d du fichier '%s' est impossible à décoder.\nVérifier que ce ne soit pas un fichier binaire !"%(l-1,ficSrc),style="Er")
        ligne="--ligne impossible à décoder--"
      if l not in EXCLURE_LIGNES:
        trouve=False
        for tx in EXCLURE_TEXTES:
          if ligne.find(tx)!=-1:
            trouve=True
        if not trouve:
          for (src,dst) in REMPLACER_TEXTES:
            ligne=ligne.replace(src,dst)
          fw.write(ligne)
      l+=1
    fr.close()
    fw.close()

#-------------------------------------------------------------------------------
def retraitMAGIDX(chaine):
  idxdeb=chaine.find("MAGIDX:")
  while idxdeb!=-1:
    idxfin=chaine.find(":IDX:")
    aidx=chaine[idxdeb+7:idxfin]
    labidx=chaine.find(":IDXMAG")
    lidx=chaine[idxfin+5:labidx]
    chaine=chaine.replace("MAGIDX:"+aidx+":IDX:"+lidx+":IDXMAG",aidx)
    idxdeb=chaine.find("MAGIDX:")
  return chaine

#-------------------------------------------------------------------------------
def controleFichier(res,vf):
    controle=''
    attendu=''
    if res==os.F_OK:
      if vf:
        controle="LOCUT_CONTROLE_EXISTENCE"
        attendu="LOCUT_ATTENDU_EXISTENCE"
      else:
        controle="LOCUT_CONTROLE_ABSENCE"
        attendu="LOCUT_ATTENDU_ABSENCE"
        #resultat="-1"
    elif res==os.R_OK:
      if vf:
        controle="LOCUT_CONTROLE_LECTURE"
        attendu="LOCUT_ATTENDU_LECTURE"
      else:
        controle="LOCUT_CONTROLE_LECTURE_IMPOSSIBLE"
        attendu="LOCUT_ATTENDU_LECTURE_IMPOSSIBLE"
        #resultat="-1"
    elif res==os.W_OK:
      if vf:
        controle="LOCUT_CONTROLE_ECRITURE"
        attendu="LOCUT_ATTENDU_ECRITURE"
      else:
        controle="LOCUT_CONTROLE_ECRITURE_IMPOSSIBLE"
        attendu="LOCUT_ATTENDU_ECRITURE_IMPOSSIBLE"
    elif res==os.X_OK:
      if vf:
        controle="LOCUT_CONTROLE_EXECUTABLE"
        attendu="LOCUT_ATTENDU_EXECUTABLE"
      else:
        controle="LOCUT_CONTROLE_NON_EXECUTABLE"
        attendu="LOCUT_ATTENDU_NON_EXECUTABLE"
    return controle,attendu

#-------------------------------------------------------------------------------
class Controle:

  #-------------------------------------------------------------------------------
  def __init__(self,pFonction,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,pProjet):
    self.CTL_COMMENT=pProjet.lexique.entree(pFonction)
    self.CTL_RESCOMMENT=pProjet.lexique.entree(pFonction+'_ResAtt')
    self.CTL_IDXACTION=pIndex.replace('[/anchor]','')
    self.CTL_IDXACTION=self.CTL_IDXACTION[self.CTL_IDXACTION.find(']')+1:]
    self.CTL_INDEX=pIndex
    self.CTL_IDENT=pIndent
    self.CTL_CONDITION=pCondition
    self.CTL_FTTQ=pFTTQ

    if 'TAG' in pCommande: self.CTL_TAG=pCommande['TAG']
    else: self.CTL_TAG=''
    if 'EXIGENCES' in pCommande: self.CTL_EXIGENCES=pCommande['EXIGENCES']
    else: self.CTL_EXIGENCES=''
    if 'FAITS_TECHNIQUES' in pCommande: self.CTL_FT=pCommande['FAITS_TECHNIQUES']
    else: self.CTL_FT=''
    if 'COMMENTAIRE' in pCommande:
      if pCommande['COMMENTAIRE']=="LOCUT_SANS_COMMENTAIRE":
        self.CTL_COMMENT=pProjet.lexique.entree(pCommande['COMMENTAIRE'])
      else:
        self.CTL_COMMENT=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['COMMENTAIRE'],pProjet.execution)
    if self.CTL_COMMENT[len(self.CTL_COMMENT)-1]!='.': self.CTL_COMMENT+='.'
    if 'PARAMETRE' in pCommande and pCommande['PARAMETRE'] is not None:
      self.CTL_PARAMETRES=pCommande['PARAMETRE'].replace('[PIPE~|]','[PIPE]').split('|')
      for e in range(len(self.CTL_PARAMETRES)):
        self.CTL_PARAMETRES[e]=nngsrc.services.SRV_CleanOperateur(pVarOperateur,self.CTL_PARAMETRES[e].replace('[PIPE]','|'),pProjet.execution)
    else: self.CTL_PARAMETRES=None
    if 'RETOUR' in pCommande: self.CTL_RETOUR=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['RETOUR'],pProjet.execution)
    else: self.CTL_RETOUR=None
    if 'VALIDER' in pCommande: self.CTL_VALIDER=pCommande['VALIDER']
    else: self.CTL_VALIDER=False
    if 'VALIDER_RAISON' in pCommande: self.CTL_VALRAISON=pCommande['VALIDER_RAISON']
    else: self.CTL_VALRAISON=None
    if 'LIBELLE' in pCommande: self.CTL_LIBELLE=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['LIBELLE'],pProjet.execution)
    else: self.CTL_LIBELLE=None
    if 'FT' in pCommande: self.CTL_FTQUALIF=pCommande['FT']
    else: self.CTL_FTQUALIF=None
    if 'RAPPORT' in pCommande: self.CTL_RAPPORT=pCommande['RAPPORT']
    else: self.CTL_RAPPORT=None
    if 'ATTENTE' in pCommande: self.CTL_ATTENTE=pCommande['ATTENTE']
    else: self.CTL_ATTENTE=0
    if 'BOUCLE' in pCommande:
      self.CTL_BOUCLE=pCommande['BOUCLE']
    else: self.CTL_BOUCLE=None
    if 'VARIABLE' in pCommande:
      self.CTL_VARIABLE=pCommande['VARIABLE']
      if self.CTL_VARIABLE is not None:
        self.CTL_COMMENT+=pProjet.lexique.entree('Variable_COMMENT')%self.CTL_VARIABLE
    else: self.CTL_VARIABLE=None
    self.CTL_SCRIPT={}

  #---------------------------------------------------------------------------------
  def ParametresBaseMode(self,pMode,pResBilan):
    return {'CTL_MODE':list(str(pMode)),
            'SUB_COMMENT':[],
            'STY_COMMENT':"norm",
            'LNK_COMMENT':False,
            'CTL_COMMENT': self.CTL_COMMENT,
            'RES_COMMENT': self.CTL_RESCOMMENT,
            'VAL_ACTION':[],
            'STY_ACTION':[],

            'CTL_LIBELLE':self.CTL_LIBELLE,
            'CTL_TAG':self.CTL_TAG,
            'CTL_EXIGENCES':self.CTL_EXIGENCES,
            'CTL_FT':self.CTL_FT,
            'CTL_INDEX':self.CTL_INDEX,
            'CTL_ID':self.CTL_IDENT,
            'CTL_CONDITION':self.CTL_CONDITION,
            'CTL_VALRAISON':self.CTL_VALRAISON,
            'CTL_FTQUALIF':self.CTL_FTQUALIF,
            'CTL_RAPPORT':self.CTL_RAPPORT,
            'CTL_BOUCLE':self.CTL_BOUCLE,
            'CTL_SCRIPT':self.CTL_SCRIPT,
            'CTL_FTTQ':self.CTL_FTTQ,
            'CTL_RESULTAT':pResBilan,
            }

#-------------------------------------------------------------------------------
class Fonctions:
  rapport=None

  #---------------------------------------------------------------------------------
  def __init__(self,projet,qualite,constantes):
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.STATUS_PB=constantes['STATUS_PB']
    self.STATUS_OK=constantes['STATUS_OK']
    self.STATUS_AV=constantes['STATUS_AV']
    self.STATUS_NT=constantes['STATUS_NT']
    self.STATUS_AR=constantes['STATUS_AR']
    self.parallel=False
    self.etape=0

  #-------------------------------------------------------------------------------
  def gereGlobal(self,resultatGlobal,resultatEtat):
    if resultatGlobal==self.STATUS_PB: return resultatGlobal
    if resultatGlobal==self.STATUS_AV: return resultatGlobal
    if resultatGlobal==self.STATUS_OK:
      if resultatEtat==self.STATUS_AV: return resultatEtat
      if resultatEtat==self.STATUS_PB: return resultatEtat
      if resultatEtat==self.STATUS_NT: return resultatEtat
    return resultatGlobal

  #-------------------------------------------------------------------------------
  # On recadre le mode C M R Y
  def recadrageExecutionMode(self,pContinuer):
    if pContinuer:
      resBilan=self.STATUS_PB
      if self.projet.rapportManuel:
        executionCMRY='M'
      else: executionCMRY=self.projet.execution
    else:
      # Si continuer est faux, on a eu un événement bloquant ou critique :
      # On fait du cahier non rempli et on met le statut à NT
      executionCMRY='C'
      resBilan=self.STATUS_NT
    return executionCMRY,resBilan

  #---------------------------------------------------------------------------------
  def Initialisations(self,pContinuer,pCommande,pControle,pResAttenduDefaut=0,pListeRetour=False,pExtensionResAttendu=None):
    executionCMRY,resBilan=self.recadrageExecutionMode(pContinuer)

    paramRetour=pControle.CTL_RETOUR
    if paramRetour:
      lst_resAttendu=["EGALE_A",paramRetour]
      if paramRetour.find('|')!=-1:
        lst_resAttendu=paramRetour.split('|')
    else:
      lst_resAttendu=["EGALE_A",pResAttenduDefaut]
      if pResAttenduDefaut is None:
        lst_resAttendu=["EGALE_A",self.projet.lexique.entree("LOCUT_SANS_OBJET")]
        resBilan=self.STATUS_OK
      pControle.CTL_RETOUR=pResAttenduDefaut
    if pExtensionResAttendu is not None: lst_resAttendu.append(pExtensionResAttendu)
    if not pListeRetour:
      lst_resAttendu=lst_resAttendu[1]

    if pCommande['SORTIE_SI_VIDE']: paramSortieSiVide=pCommande['SORTIE_SI_VIDE']
    else: paramSortieSiVide="LOCUT_AUCUNE_SORTIE"

    resSortie=''
    if self.projet.rapportManuel and not pContinuer:
      resRetour=0
    else:
      resRetour=''

    if pCommande['SORTIE_SI_VIDE']: paramSortieSiVide=pCommande['SORTIE_SI_VIDE']

    return executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,pControle

  #---------------------------------------------------------------------------------
  def AnalyseResultat(self,pControle,pRetVSRes,pStatus,pResBilan,pResSortie,pParamSortieSiVide):
    # Analyse du code de retour
    if pRetVSRes:
      pResBilan=pStatus
    # Reajustement du bilan en fonction de l'ordre de validation
    if pControle.CTL_VALIDER and pResBilan==self.STATUS_PB:
      pResBilan=self.STATUS_AV
    # Reajustement de la sortie vide
    if pResSortie=='':
      pResSortie=pParamSortieSiVide
    # Reajustement de la sortie en fonction du bilan
    if pResBilan==self.STATUS_NT:
      pResSortie=''
    return pResBilan,pResSortie

  #-------------------------------------------------------------------------------
  def AnalyseCondition(self,pControle,pFonction,pResAttendu,pResRetour,pResBilan,pResMessage=''):
    if pResMessage=='' and pControle.CTL_RETOUR is not None:
      if pFonction in ["EGALE_A","EGALE"]:
        if self.projet.execution=='R' and pResRetour==pResAttendu:
           pResBilan=self.STATUS_OK
        pResAttendu=self.projet.lexique.entree('LOCUT_EGALE_A')%pResAttendu
      elif pFonction=="DIFFERENT_DE":
        if self.projet.execution=='R' and int(pResRetour)!=int(pResAttendu):
           pResBilan=self.STATUS_OK
        pResAttendu=self.projet.lexique.entree('LOCUT_DIFFERENT_DE')%pResAttendu
      elif pFonction=="MOINS_DE":
        if self.projet.execution=='R' and int(pResRetour)<int(pResAttendu):
           pResBilan=self.STATUS_OK
        pResAttendu=self.projet.lexique.entree('LOCUT_MOINS_DE')%pResAttendu
      elif pFonction=="PLUS_DE":
        if self.projet.execution=='R' and int(pResRetour)>int(pResAttendu):
           pResBilan=self.STATUS_OK
        pResAttendu=self.projet.lexique.entree('LOCUT_PLUS_DE')%pResAttendu
      else:
        pResRetour="ERREUR DE CONDITION : %s %s %s"%(pFonction,pResRetour,pResAttendu)
      # Reajustement du bilan en fonction de l'ordre de validation
      if self.projet.execution=='R' and pControle.CTL_VALIDER and pResBilan==self.STATUS_PB:
        pResBilan=self.STATUS_AV
    return pResAttendu,pResRetour,pResBilan

  #---------------------------------------------------------------------------------
  def AnalyseCommande(self,pVarOperateur,commande,commentaire,index):
    idxAction=index.replace('[/anchor]','')
    idxAction=idxAction[idxAction.find(']')+1:]

    if 'TAG' in commande: CTL_TAG=commande['TAG']
    else: CTL_TAG=''
    if 'EXIGENCES' in commande: CTL_EXIGENCES=commande['EXIGENCES']
    else: CTL_EXIGENCES=''
    if 'FAITS_TECHNIQUES' in commande: CTL_FT=commande['FAITS_TECHNIQUES']
    else: CTL_FT=''
    if 'COMMENTAIRE' in commande:
      if commande['COMMENTAIRE']=="LOCUT_SANS_COMMENTAIRE":
        CTL_COMMENT=self.projet.lexique.entree(commande['COMMENTAIRE'])
      else:
        CTL_COMMENT=nngsrc.services.SRV_CleanOperateur(pVarOperateur,commande['COMMENTAIRE'],self.projet.execution)
    else: CTL_COMMENT=commentaire
    if 'PARAMETRE' in commande:
      CTL_PARAMETRES=commande['PARAMETRE'].replace('[PIPE~|]','[PIPE]').split('|')
      for e in range(len(CTL_PARAMETRES)):
        CTL_PARAMETRES[e]=nngsrc.services.SRV_CleanOperateur(pVarOperateur,CTL_PARAMETRES[e].replace('[PIPE]','|'),self.projet.execution)
    else: CTL_PARAMETRES=None
    if 'RETOUR' in commande: CTL_RETOUR=nngsrc.services.SRV_CleanOperateur(pVarOperateur,commande['RETOUR'],self.projet.execution)
    else: CTL_RETOUR=None
    if 'VALIDER' in commande: CTL_VALIDER=commande['VALIDER']
    else: CTL_VALIDER=False
    if 'VALIDER_RAISON' in commande: CTL_VALRAISON=commande['VALIDER_RAISON']
    else: CTL_VALRAISON=None
    if 'LIBELLE' in commande: CTL_LIBELLE=nngsrc.services.SRV_CleanOperateur(pVarOperateur,commande['LIBELLE'],self.projet.execution)
    else: CTL_LIBELLE=None
    if 'FT' in commande: CTL_FTQUALIF=commande['FT']
    else: CTL_FTQUALIF=None
    if 'RAPPORT' in commande: CTL_RAPPORT=commande['RAPPORT']
    else: CTL_RAPPORT=None
    if 'ATTENTE' in commande: CTL_ATTENTE=commande['ATTENTE']
    else: CTL_ATTENTE=0
    if 'BOUCLE' in commande:
      CTL_BOUCLE=commande['BOUCLE']
    else: CTL_BOUCLE=None
    if 'VARIABLE' in commande:
      CTL_VARIABLE=commande['VARIABLE']
    else: CTL_VARIABLE=None
    CTL_SCRIPT="%s NON IMPLEMENTE !!"%commentaire
    return CTL_TAG, CTL_EXIGENCES, CTL_FT, CTL_COMMENT, CTL_PARAMETRES, CTL_RETOUR, CTL_VALIDER, CTL_VALRAISON, CTL_LIBELLE, CTL_FTQUALIF, CTL_RAPPORT, CTL_ATTENTE, CTL_BOUCLE, CTL_VARIABLE, CTL_SCRIPT, idxAction

  #---------------------------------------------------------------------------------
  @staticmethod
  def ParametresBaseMode(mode, CTL_COMMENT, CTL_LIBELLE, CTL_TAG, CTL_EXIGENCES, CTL_FT, CTL_INDEX, CTL_IDENT, CTL_CONDITION, CTL_RESULTAT, CTL_VALRAISON, CTL_FTQUALIF, CTL_RAPPORT, CTL_BOUCLE, CTL_SCRIPT, CTL_FTTQ):
    return {'CTL_MODE':list(str(mode)),
            'SUB_COMMENT':[],
            'STY_COMMENT':"norm",
            'LNK_COMMENT':False,
            'CTL_COMMENT': CTL_COMMENT,
            'VAL_ACTION':[],
            'STY_ACTION':[],

            'CTL_LIBELLE':CTL_LIBELLE,
            'CTL_TAG':CTL_TAG,
            'CTL_EXIGENCES':CTL_EXIGENCES,
            'CTL_FT':CTL_FT,
            'CTL_INDEX':CTL_INDEX,
            'CTL_ID':CTL_IDENT,
            'CTL_CONDITION':CTL_CONDITION,
            'CTL_RESULTAT':CTL_RESULTAT,
            'CTL_VALRAISON':CTL_VALRAISON,
            'CTL_FTQUALIF':CTL_FTQUALIF,
            'CTL_RAPPORT':CTL_RAPPORT,
            'CTL_BOUCLE':CTL_BOUCLE,
            'CTL_SCRIPT':CTL_SCRIPT,
            'CTL_FTTQ':CTL_FTTQ}

  #---------------------------------------------------------------------------------
  def RapportBilanMode(self,attente,resultat,fiche,params,rapporter):
    bilanHTML=''
    bilanTEX=''

    etatAction,FTconfirme,FTreserve=self.rapport.Valid_ConfirmationFT(fiche,params,self.projet.execution,rapporter)
    (FTconfirme,FTreserve,gestionFT,numFT,etatFT,elemMatTrac)=self.qualite.QUAL_FaitsTechniquesFiche(fiche,params,etatAction,FTconfirme,FTreserve,self.projet,self.rapport.ficRapport,self.parallel)

    if FTconfirme is not None:
#      self.rapport.html.rapportFTreserves.append(FTreserve)
#      resultat=self.STATUS_AR
      if FTconfirme:
        if gestionFT[0] is not None:
          resultat=nngsrc.services.SRV_resultatCode(gestionFT[0],self.constantes)

    if not self.projet.etapesSeules:
      bilanHTML=self.rapport.html.Valid_BilanMode(fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,self.projet.execution)
      if self.rapport.editRapport and self.rapport.projet.RAPPORT_TEX=="oui" :
        bilanTEX=self.rapport.tex.Valid_BilanMode(fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,self.projet.execution)
      if self.rapport.editRapport and self.rapport.projet.RAPPORT_EN_LIGNE=="oui" :
        self.rapport.txt.Valid_BilanMode(fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,self.projet.execution)

    time.sleep(int(attente))

    return resultat, (bilanHTML, bilanTEX,), gestionFT, elemMatTrac

  #---------------------------------------------------------------------------------
  def SaisieResultatActionManuelle(self,rapport,fiche,idxAction,message=None,objet=None,actions=None,attendu=None,saisie=None):
    commentaireKO=''
    resRetour=''
    resBilan=''
    resSortie=''

    # Affichages
    if message is not None:
      if rapport!=-1:
        nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".","-")
      else:
        nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,message,style="||Or",espacement=10)
    if objet is not None:
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,objet)
    if actions is not None:
      for action in actions:
        nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,action,style="||Or",espacement=10)

    # Saisies
    if saisie is not None:
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||Or")
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"SAISIE OPERATEUR :",style="||Or")
      nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
      if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
      resRetour=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"=> ","SAISIE")
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||Or")

    # Demande de retour de test
    resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_CORRECT'),"SAISIE",espacement=0).lower()
    while not resultat in ["n", "o", "k", "t"]:
      if resultat=="c":
        remarque=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_REMARQUE'),"SAISIE",espacement=0)
        (self.projet.nbRemarque,self.projet.ficheCourante)=nngsrc.services.SRV_ecrireFichierRemarque(fiche,idxAction,remarque,self.projet.fichier_remarque,self.projet.nbRemarque,self.projet.ficheCourante)
        resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_CORRECT'),"SAISIE",espacement=0).lower()
      elif resultat=="k":
        commentaireKO=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_KO'),"SAISIE",espacement=0)
      else:
        resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_REPONDRE'),"SAISIE",espacement=0).lower()
    if resultat=="o":
      resBilan=self.STATUS_OK
      if attendu is not None:
        resRetour=attendu
      resSortie=self.projet.lexique.entree('SaisieManuelle_RES_SANS_ERREURS')
    elif resultat=="n" or resultat=="t":
      resBilan=self.STATUS_NT
      resSortie=self.projet.lexique.entree('LOCUT_NON_TESTE')
      if resultat=="t":
        self.projet.ARRET=True
    elif resultat=="k":
      resBilan=self.STATUS_PB
      if self.projet.uiTerm:
        resSortie=commentaireKO
      else:
        resSortie=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_COMMENTAIRE'),"SAISIE",espacement=0)
        if nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,' | '+self.projet.lexique.entree('SaisieManuelle_ARRET'),"OUINON",espacement=0)=='o':
          self.projet.ARRET=True
    if rapport!=-1:
      nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".","-")
    return resRetour,resBilan,resSortie

  #-------------------------------------------------------------------------------
  def EtapeDeTest(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    # Analyse de la commande
    controle=Controle('EtapeDeTest',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    bilanTEX=''
    bilanHTML=self.rapport.html.EtapeDeTest(controle.CTL_COMMENT,self.etape)
    if self.rapport.editRapport and self.rapport.projet.RAPPORT_TEX=="oui" :
      bilanTEX=self.rapport.tex.EtapeDeTest(controle.CTL_COMMENT,self.etape)
    self.etape+=1

    return [(None,(bilanHTML,bilanTEX),None,None)]

  ##-------------------------------------------------------------------------------
  # DOCUMENTATION
  # CATEGORIE   : Commandes
  # FONCTION    : VALID_Commande
  # DESCRIPTION : Exécute une commande et vérifie le status de sortie et l'affichage retour.
  # PARAMETRES  : {commande}[|{BLOQUER,CONTINUER}]
  # RETOUR      : {execution}
  # VARIABLE    : {sortie stdout/stderr)
  #-------------------------------------------------------------------------------
  def VALID_Commande(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_Commande",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_Commande',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Commande
    paramCommande=controle.CTL_PARAMETRES[0]
    # 1- Mode (optionnel)
    mode="BLOQUER"
    if len(controle.CTL_PARAMETRES)==2:
      if controle.CTL_PARAMETRES[1] in ["BLOQUER","CONTINUER"]:
        mode=controle.CTL_PARAMETRES[1]

    # Retrait d'index
    paramCommandeSansIndex=nngsrc.services.SRV_retraitIndexation(paramCommande)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       actions=[paramCommandeSansIndex,self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR')+' : '+str(resAttendu)],
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      # Execution
      if mode=="BLOQUER":
        (resRetour,resSortie)=nngsrc.services.SRV_Execute(paramCommandeSansIndex,"commands")
      else:
        resRetour=run(os.P_NOWAIT,paramCommandeSansIndex.split(' ')[0],paramCommandeSansIndex.split(' ')[1:])
        resSortie=''

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1031,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':paramCommandeSansIndex,'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%resAttendu,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],             'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resSortie,controle.CTL_VARIABLE)

  ##-------------------------------------------------------------------------------
  # DOCUMENTATION
  # CATEGORIE   : Commandes
  # FONCTION    : VALID_CommandeRecherche
  # DESCRIPTION : Exécute une commander, vérifier le status de sortie et rechercher la présence ou l'absence d'une liste de chaînes de caractères dans la sortie.
  # PARAMETRES  : {commande}[|{BLOQUER,CONTINUER}]
  # RETOUR      : {execution}
  #-------------------------------------------------------------------------------
  def VALID_CommandeRecherche(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CommandeRecherche",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CommandeRecherche',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Commande
    paramCommande=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramCommandeSansIndex=nngsrc.services.SRV_retraitIndexation(paramCommande)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    listeDeChaines=[]
    listeDeResults=[]
    listeResAttendu=[]
    listeDePresence=[]
    listeDOccurence=[]
    LstChaineOK=[]
    LstChainePB=[]
    resultatEtat=self.STATUS_NT
    resultatGlobal=self.STATUS_NT
    optionCasse=''

    # Casse
    paramCasse=int(pCommande['CASSE'])
    if paramCasse==1:
      optionCasse=" "+self.projet.lexique.entree('LOCUT_CASSE_EXACTE')

    # Trouver : oui ou non ?
    if 'TROUVER_OUI' in pCommande:
      chaine=retraitMAGIDX(nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_OUI'],self.projet.execution))
      if paramCasse==0: chaine=chaine.lower()
      LstChaineOK=chaine.split('|')
    if 'TROUVER_NON' in pCommande:
      chaine=retraitMAGIDX(nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_NON'],self.projet.execution))
      if paramCasse==0: chaine=chaine.lower()
      LstChainePB=chaine.split('|')

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       actions=[paramCommandeSansIndex,self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR')+' : '+str(resAttendu)],
                                                                       attendu=resAttendu)
      resultatGlobal=resBilan
      if resultatGlobal==self.STATUS_OK:
        for pat in LstChaineOK:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'))
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower()))
          actions=[self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower())]
          (_,resultatEtat,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                           message=self.projet.lexique.entree('LOCUT_CHERCHER_DANS_SORTIE')+optionCasse+":",
                                                                           actions=actions)
          if resultatEtat==self.STATUS_OK:
            listeDOccurence.append(self.projet.lexique.entree('MOT_PRESENT'))
          else:
            listeDOccurence.append(self.projet.lexique.entree('MOT_ABSENT'))
          resultatGlobal=self.gereGlobal(resultatGlobal,resultatEtat)
        for pat in LstChainePB:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'))
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower()))
          actions=[self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower())]
          (_,resultatEtat,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                           message=self.projet.lexique.entree('LOCUT_CHERCHER_DANS_SORTIE')+optionCasse+":",
                                                                           actions=actions)
          if resultatEtat==self.STATUS_OK:
            listeDOccurence.append(self.projet.lexique.entree('MOT_ABSENT'))
          else:
            listeDOccurence.append(self.projet.lexique.entree('MOT_PRESENT'))
          resultatGlobal=self.gereGlobal(resultatGlobal,resultatEtat)
#      else:
#        resultatEtat=self.STATUS_NT
#        for pat in LstChaineOK:
#          pat=retraitMAGIDX(pat)
#          listeDeChaines.append(pat)
#          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'))
#          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower()))
#          listeDeResults.append(self.STATUS_NT)
#          listeDOccurence.append('')
#        for pat in LstChainePB:
#          pat=retraitMAGIDX(pat)
#          listeDeChaines.append(pat)
#          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'))
#          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower()))
#          listeDeResults.append(self.STATUS_NT)
#          listeDOccurence.append('')

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resultatEtat=self.STATUS_OK
      # ETAPE O : Execution
      (resRetour,resSortie)=nngsrc.services.SRV_Execute(paramCommandeSansIndex,"commands")
      if resSortie=='':
        resSortie=paramSortieSiVide
      if paramCasse==0: resSortieCasse=nngsrc.services.SRV_Encodage(resSortie.lower())
      else: resSortieCasse=nngsrc.services.SRV_Encodage(resSortie)
      # ETAPE 1 : Analyse code de retour
      if resRetour==int(resAttendu): resBilan=self.STATUS_OK
      resultatGlobal=resBilan
      # ETAPE 2 : Analyse chaines dans sorties
      if resBilan==self.STATUS_OK:
        for pat in LstChaineOK:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'))#self.STATUS_OK)
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower()))
          if resSortieCasse.find(pat)!=-1:
            listeDeResults.append(self.STATUS_OK)
            listeDOccurence.append(self.projet.lexique.entree("MOT_PRESENT"))
          else:
            listeDeResults.append(self.STATUS_PB)
            listeDOccurence.append(self.projet.lexique.entree("MOT_ABSENT"))
            resultatEtat=self.STATUS_PB
            resultatGlobal=self.gereGlobal(resultatGlobal,resultatEtat)
        for pat in LstChainePB:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'))#self.STATUS_PB)
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower()))
          if resSortieCasse.find(pat)==-1:
            listeDeResults.append(self.STATUS_OK)
            listeDOccurence.append(self.projet.lexique.entree("MOT_ABSENT"))
          else:
            listeDeResults.append(self.STATUS_PB)
            listeDOccurence.append(self.projet.lexique.entree("MOT_PRESENT"))
            resultatEtat=self.STATUS_PB
            resultatGlobal=self.gereGlobal(resultatGlobal,resultatEtat)
      else:
        resultatEtat=self.STATUS_NT
        for pat in LstChaineOK:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'))
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower()))
          listeDeResults.append(self.STATUS_NT)
          listeDOccurence.append('')
        for pat in LstChainePB:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'))
          listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower()))
          listeDeResults.append(self.STATUS_NT)
          listeDOccurence.append('')
      # Reajustement du bilan en fonction de l'ordre de validation
      if controle.CTL_VALIDER:
        if resultatGlobal==self.STATUS_PB: resultatGlobal=self.STATUS_AV
        if resultatEtat==self.STATUS_PB: resultatEtat=self.STATUS_AV
        if resBilan==self.STATUS_PB: resBilan=self.STATUS_AV

    #----------------------------
    # Traitement des modes P et C
    if executionCMRY in ['P','C','S']:
      for pat in LstChaineOK:
        pat=retraitMAGIDX(pat)
        listeDeChaines.append(pat)
        listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_PRESENT').lower()))
        listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'))
        listeDOccurence.append('')
      for pat in LstChainePB:
        pat=retraitMAGIDX(pat)
        listeDeChaines.append(pat)
        listeResAttendu.append(self.projet.lexique.entree('VALID_CommandeRecherche_Sortie')%(pat,optionCasse,self.projet.lexique.entree('MOT_ABSENT').lower()))
        listeDePresence.append(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'))
        listeDOccurence.append('')

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(10313,resBilan)
    resComment=[controle.CTL_RESCOMMENT%resAttendu]
    resComment.extend(listeResAttendu)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':paramCommandeSansIndex,'STY_COMMENT':"tt",
        'RES_COMMENT':resComment,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                     #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],            'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt",
        #------------------------------------------
        'CLF2_ACTION' :self.projet.lexique.entree('LOCUT_CHERCHER_DANS_SORTIE')+optionCasse,        'CLF2_CONSTAT':'MOT_CONSTAT',                        #CLF_BILAN
        'CLF2_ATTENDU':listeDePresence,'VAL2_ATTENDU':listeDeChaines,                               'VAL2_CONSTAT':listeDOccurence,'STY2_CONSTAT':"tt",  'VAL2_BILAN':resultatEtat})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resultatGlobal,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_CopieFichier
  # DESCRIPTION : Copie un fichier. Le fichier destination, s'il existe est détruit avant copie.
  # PARAMETRES  : {fichier_source}|{fichier_destination}[|F][|{paramTimeOut}]
  # RETOUR      : {erreur}
  # TEXTE       : Copier le fichier {fichier_source} en {fichier_destination}.[ Forcer la copie.][ Tenter pendant {time_out} s.]
  # IHM         : Copier le fichier {fichier_source} en {fichier_destination}.[ Forcer la copie.][ Tenter pendant {time_out} s.]
  #-------------------------------------------------------------------------------
  def VALID_CopieFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CopieFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CopieFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier source
    paramSource=controle.CTL_PARAMETRES[0]
    # 1- Fichier destination
    paramDestination=controle.CTL_PARAMETRES[1]
    # 2/3- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,2)

    # Retrait d'index
    paramSourceSansIndex=nngsrc.services.SRV_retraitIndexation(paramSource)
    paramDestinationSansIndex=nngsrc.services.SRV_retraitIndexation(paramDestination)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_COPIER_FICHIER')%(paramSourceSansIndex,paramDestinationSansIndex),
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      execOk=True
      resRetour=0
      if os.access(paramDestinationSansIndex,os.F_OK):
        if paramForce:
          if not nngsrc.services.SRV_changePermission(paramDestinationSansIndex,0o777,False):
            resRetour,resMessage=ERR_PERMISSION
            resMessage=self.projet.lexique.entree(resMessage)
            execOk=False
          elif not nngsrc.services.SRV_detruire(paramDestinationSansIndex,timeout=paramTimeOut):
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
            execOk=False
        else:
          execOk=False
          resRetour,resMessage=ERR_EXISTE_DESTINATION
          resMessage=self.projet.lexique.entree(resMessage)
      if execOk:
        if not nngsrc.services.SRV_acceder(paramSourceSansIndex,os.F_OK,timeout=paramTimeOut):
          resRetour,resMessage=ERR_ACCES_SOURCE
          resMessage=self.projet.lexique.entree(resMessage)
        else:
          if not nngsrc.services.SRV_copier(paramSourceSansIndex,paramDestinationSansIndex,timeout=paramTimeOut):
            resRetour,resMessage=ERR_COPIE_DEPLACE_CREE
            resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_COPIER_FICHIER'),'SUB_COMMENT':[paramSource,paramDestination],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_DeplaceFichier
  # DESCRIPTION : Déplace un fichier. Le fichier destination, s'il existe est détruit avant déplacement.
  # PARAMETRES  : {fichier_source}|{fichier_destination}[|F][|{paramTimeOut}]
  # RETOUR      : {erreur}
  # TEXTE       : Deplace le fichier {fichier_source} en {fichier_destination}.[ Forcer la copie.][ Tenter pendant {time_out} s.]
  # IHM         : Deplace le fichier {fichier_source} en {fichier_destination}.[ Forcer la copie.][ Tenter pendant {time_out} s.]
  #-------------------------------------------------------------------------------
  def VALID_DeplaceFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_DeplaceFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_DeplaceFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier source
    paramSource=controle.CTL_PARAMETRES[0]
    # 1- Fichier destination
    paramDestination=controle.CTL_PARAMETRES[1]
    # 2/3- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,2)

    # Retrait d'index
    paramSourceSansIndex=nngsrc.services.SRV_retraitIndexation(paramSource)
    paramDestinationSansIndex=nngsrc.services.SRV_retraitIndexation(paramDestination)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_DEPLACER_FICHIER')%(paramSourceSansIndex,paramDestinationSansIndex),
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      execOk=True
      resRetour=0
      if os.access(paramDestinationSansIndex,os.F_OK):
        if paramForce:
          if not nngsrc.services.SRV_changePermission(paramDestinationSansIndex,0o777,False):
            resRetour,resMessage=ERR_PERMISSION
            resMessage=self.projet.lexique.entree(resMessage)
            execOk=False
          elif not nngsrc.services.SRV_detruire(paramDestinationSansIndex,timeout=paramTimeOut):
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
            execOk=False
        else:
          execOk=False
          resRetour,resMessage=ERR_EXISTE_DESTINATION
          resMessage=self.projet.lexique.entree(resMessage)
      if execOk:
        if not nngsrc.services.SRV_acceder(paramSourceSansIndex,os.F_OK,timeout=paramTimeOut):
          resRetour,resMessage=ERR_ACCES_SOURCE
          resMessage=self.projet.lexique.entree(resMessage)
        elif not nngsrc.services.SRV_deplacer(paramSourceSansIndex,paramDestinationSansIndex,paramTimeOut):
            resRetour,resMessage=ERR_COPIE_DEPLACE_CREE
            resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_DEPLACER_FICHIER'),'SUB_COMMENT':[paramSource,paramDestination],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_DetruitFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_DetruitFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_DetruitFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier à détruire
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1/2- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,1)

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_SUPPRIMER_FICHIER')%'',
                                                               objet=paramFichierSansIndex,
                                                               actions=actions,
                                                               attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      if paramForce:
        nngsrc.services.SRV_changePermission(paramFichierSansIndex,0o777,False)
      if not nngsrc.services.SRV_acceder(paramFichierSansIndex,os.F_OK,timeout=paramTimeOut):
        resRetour,resMessage=ERR_ACCES_SOURCE
        resMessage=self.projet.lexique.entree(resMessage)
      elif not nngsrc.services.SRV_detruire(paramFichierSansIndex,timeout=paramTimeOut):
        resRetour,resMessage=ERR_DESTRUCTION
        resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_SUPPRIMER_FICHIER'),'SUB_COMMENT':[paramFichierSansIndex],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CreeRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CreeRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CreeRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Repertoire à créer
    parRepertoire=controle.CTL_PARAMETRES[0]
    # 1- Timer (optionnel)
    paramTimeOut=0
    if len(controle.CTL_PARAMETRES)==2:
      paramTimeOut=int(controle.CTL_PARAMETRES[1])

    # Retrait d'index
    parRepertoireSansIndex=nngsrc.services.SRV_retraitIndexation(parRepertoire)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_CREER_REPERTOIRE')%'',
                                                               objet=parRepertoireSansIndex,
                                                               actions=actions,
                                                               attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      if not nngsrc.services.SRV_creerPath(parRepertoireSansIndex,paramTimeOut):
        resRetour,resMessage=ERR_COPIE_DEPLACE_CREE
        resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_CREER_REPERTOIRE'),'SUB_COMMENT':[parRepertoireSansIndex],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),              'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],             'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_CopieRepertoire
  # DESCRIPTION : Copie un répertoire. Le répertoire destination, s'il existe est détruit avant copie.
  # PARAMETRE   : {repertoire_source}|{repertoire_destination}[|F]
  # RETOUR      : {erreur}
  # TEXTE       : Copier le repertoire {repertoire_source} en {repertoire_destination}.[ Forcer la copie.]
  # IHM         : Copier le repertoire {repertoire_source} en {repertoire_destination}.[ Forcer la copie.]
  #-------------------------------------------------------------------------------
  def VALID_CopieRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CopieRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CopieRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier source
    paramSource=controle.CTL_PARAMETRES[0]
    # 1- Fichier destination
    paramDestination=controle.CTL_PARAMETRES[1]
    # 2/3- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,2)

    # Retrait d'index
    paramSourceSansIndex=nngsrc.services.SRV_retraitIndexation(paramSource)
    paramDestinationSansIndex=nngsrc.services.SRV_retraitIndexation(paramDestination)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_COPIER_REPERTOIRE')%(paramSourceSansIndex,paramDestinationSansIndex),
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      execOk=True
      resRetour=0
      if os.access(paramDestinationSansIndex,os.F_OK):
        if paramForce:
          if not nngsrc.services.SRV_changePermission(paramDestinationSansIndex,0o777,True):
            execOk=False
            resRetour,resMessage=ERR_PERMISSION
            resMessage=self.projet.lexique.entree(resMessage)
          elif not nngsrc.services.SRV_razPath(paramDestinationSansIndex,10,self.projet.debug>0):
            execOk=False
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
          elif not nngsrc.services.SRV_detruirePath(paramDestinationSansIndex,10,self.projet.debug>0):
            execOk=False
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
        else:
          execOk=False
          resRetour,resMessage=ERR_EXISTE_DESTINATION
          resMessage=self.projet.lexique.entree(resMessage)
      if execOk:
        if not os.access(paramSourceSansIndex, os.F_OK):
           resRetour,resMessage=ERR_ACCES_SOURCE
           resMessage=self.projet.lexique.entree(resMessage)
        else:
           try:
             shutil.copytree(paramSourceSansIndex,paramDestinationSansIndex)
           except Exception:
             resRetour,resMessage=ERR_COPIE_DEPLACE_CREE
             resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_COPIER_REPERTOIRE'),'SUB_COMMENT':[paramSource,paramDestination],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_DeplaceRepertoire
  # DESCRIPTION : Déplace un répertoire. Le répertoire destination, s'il existe est détruit avant copie.
  # PARAMETRE   : {repertoire_source}|{repertoire_destination}[|F]
  # RETOUR      : {erreur}
  # TEXTE       : Deplace le repertoire {repertoire_source} en {repertoire_destination}.[ Forcer la copie.]
  # IHM         : Deplace le repertoire {repertoire_source} en {repertoire_destination}.[ Forcer la copie.]
  #-------------------------------------------------------------------------------
  def VALID_DeplaceRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_DeplaceRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_DeplaceRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Répertoire source
    paramSource=controle.CTL_PARAMETRES[0]
    # 1- Fichier destination
    paramDestination=controle.CTL_PARAMETRES[1]
    # 2/3- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,2)

    # Retrait d'index
    paramSourceSansIndex=nngsrc.services.SRV_retraitIndexation(paramSource)
    paramDestinationSansIndex=nngsrc.services.SRV_retraitIndexation(paramDestination)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_DEPLACER_REPERTOIRE')%(paramSourceSansIndex,paramDestinationSansIndex),
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      execOk=True
      resRetour=0
      if os.access(paramDestinationSansIndex,os.F_OK):
        if paramForce:
          if not nngsrc.services.SRV_changePermission(paramDestinationSansIndex,0o777,True):
            resRetour,resMessage=ERR_PERMISSION
            resMessage=self.projet.lexique.entree(resMessage)
          elif not nngsrc.services.SRV_razPath(paramDestinationSansIndex,10,self.projet.debug>0):
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
          elif not nngsrc.services.SRV_detruirePath(paramDestinationSansIndex,10,self.projet.debug>0):
            resRetour,resMessage=ERR_DESTRUCTION
            resMessage=self.projet.lexique.entree(resMessage)
        else:
          execOk=False
          resRetour,resMessage=ERR_EXISTE_DESTINATION
          resMessage=self.projet.lexique.entree(resMessage)
      if execOk:
        if not os.access(paramSourceSansIndex, os.F_OK):
           resRetour,resMessage=ERR_ACCES_SOURCE
           resMessage=self.projet.lexique.entree(resMessage)
        else:
           try:
             shutil.move(paramSourceSansIndex,paramDestinationSansIndex)
           except Exception:
             resRetour,resMessage=ERR_COPIE_DEPLACE_CREE
             resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,int(resRetour)==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_DEPLACER_REPERTOIRE'),'SUB_COMMENT':[paramSource,paramDestination],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_DetruitRepertoire
  # DESCRIPTION : Détruit le répertoire et tout son contenu, sous-répertoires compris.
  # PARAMETRES  : {repertoire}[|{paramTimeOut}]
  # RETOUR      : {erreur}
  # TEXTE       : Detruit le repertoire {repertoire}.[ Tenter pendant {time_out} s.]
  # IHM         : Detruit le repertoire {repertoire}.[ Tenter pendant {time_out} s.]
  #-------------------------------------------------------------------------------
  def VALID_DetruitRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_DetruitRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_DetruitRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Répertoire à détruire
    paramRepertoire=controle.CTL_PARAMETRES[0]
    # 1/2- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,1)

    # Retrait d'index
    paramRepertoireSansIndex=nngsrc.services.SRV_retraitIndexation(paramRepertoire)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=None
      if resAttendu!=0:
        actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+self.projet.lexique.entree(messageErreur(resAttendu))]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_SUPPRIMER_REPERTOIRE')%'',
                                                                       objet=paramRepertoireSansIndex,
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      if paramForce:
        if not nngsrc.services.SRV_changePermission(paramRepertoireSansIndex,0o777,True):
          print("CHMOD Impossible")
      if not nngsrc.services.SRV_acceder(paramRepertoireSansIndex,os.F_OK,timeout=paramTimeOut):
        resRetour,resMessage=ERR_ACCES_SOURCE
        resMessage=self.projet.lexique.entree(resMessage)
      elif not nngsrc.services.SRV_razPath(paramRepertoireSansIndex,paramTimeOut,self.projet.debug>0):
        resRetour,resMessage=ERR_DESTRUCTION
        resMessage=self.projet.lexique.entree(resMessage)
      elif not nngsrc.services.SRV_detruirePath(paramRepertoireSansIndex,paramTimeOut,self.projet.debug>0):
        resRetour,resMessage=ERR_DESTRUCTION
        resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_SUPPRIMER_REPERTOIRE'),'SUB_COMMENT':[paramRepertoire],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_VideRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_VideRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_VideRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Repertoire à vider
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1/2- Forcage des droits et timer (optionnel)
    paramForce,paramTimeOut=forcageDroitsTimer(controle,1)

    # Retrait d'index
    paramRepertoireSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_VIDER_REPERTOIRE')%'',
                                                               objet=paramRepertoireSansIndex)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      if paramForce:
        nngsrc.services.SRV_changePermission(paramRepertoireSansIndex,0o777,True)
      if not nngsrc.services.SRV_acceder(paramRepertoireSansIndex,os.F_OK,timeout=paramTimeOut):
        resRetour,resMessage=ERR_ACCES_SOURCE
        resMessage=self.projet.lexique.entree(resMessage)
      elif not nngsrc.services.SRV_razPath(paramRepertoireSansIndex,paramTimeOut,self.projet.debug>0):
        resRetour,resMessage=ERR_DESTRUCTION
        resMessage=self.projet.lexique.entree(resMessage)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_VIDER_REPERTOIRE'),'SUB_COMMENT':[paramRepertoireSansIndex],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_Compare(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_Compare",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_Compare',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    paramOutil=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramCible=controle.CTL_PARAMETRES[2]

    # Retrait d'index
    paramReferenceSansIndex=nngsrc.services.SRV_retraitIndexation(paramReference)
    paramCibleSansIndex=nngsrc.services.SRV_retraitIndexation(paramCible)
    paramCommandeSansIndex=' '.join([nngsrc.services.SRV_retraitIndexation(paramOutil),paramReferenceSansIndex,paramCibleSansIndex])

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       objet=paramCommandeSansIndex,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      (resRetour,resSortie)=nngsrc.services.SRV_Execute(paramCommandeSansIndex,"commands")

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1031,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':'%s %s %s'%(paramOutil,paramReference,paramCible),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],            'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareBin(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareBin",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareBin',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    paramOutil=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramCible=controle.CTL_PARAMETRES[2]

    # Retrait d'index
    paramReferenceSansIndex=nngsrc.services.SRV_retraitIndexation(paramReference)
    paramCibleSansIndex=nngsrc.services.SRV_retraitIndexation(paramCible)
    paramCommandeSansIndex=' '.join([nngsrc.services.SRV_retraitIndexation(paramOutil),paramReferenceSansIndex+"-hex",paramCibleSansIndex+"-hex"])

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       objet=paramCommandeSansIndex,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      # rendre lisible les fichiers binaires
      ligneCommande='hexdump -C %s > %s-hex'%(paramReferenceSansIndex,paramReferenceSansIndex)
      nngsrc.services.SRV_Execute(ligneCommande,"commands")
      ligneCommande='hexdump -C %s > %s-hex'%(paramCibleSansIndex,paramCibleSansIndex)
      nngsrc.services.SRV_Execute(ligneCommande,"commands")
      # execution
      (resRetour,resSortie)=nngsrc.services.SRV_Execute(paramCommandeSansIndex,"commands")

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1031,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':'%s %s %s'%(paramOutil,paramReference,paramCible),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareExtrait(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareExtrait",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareExtrait',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    paramOutil=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramCible=controle.CTL_PARAMETRES[2]

    # Retrait d'index
    paramReferenceSansIndex=nngsrc.services.SRV_retraitIndexation(paramReference)
    paramCibleSansIndex=nngsrc.services.SRV_retraitIndexation(paramCible)
    paramCommandeSansIndex=' '.join([nngsrc.services.SRV_retraitIndexation(paramOutil),paramReferenceSansIndex,paramCibleSansIndex])
    droit=os.access(paramCibleSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,paramReferenceSansIndex,paramCibleSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramCibleSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramCibleSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))
    tempReference=paramReferenceSansIndex

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=[]
      if calculDebut!='': rapDebut=calculDebut
      if calculFin!='': rapFin=calculFin-1
      actions.append(self.projet.lexique.entree('MOT_RESTRICTION'))
      actions.append(self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT')%paramCibleSansIndex)
      if calculDebut!='':
        actions.append(self.projet.lexique.entree('LOCUT_DE_LA_LIGNE')+" %d"%calculDebut)
      if calculFin!='':
        actions.append(self.projet.lexique.entree('LOCUT_A_LA_LIGNE')+" %d"%calculFin)
      if len(numLignesExclues)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_NUMERO')+" %s"%', '.join(["%d"%e for e in numLignesExclues]))
      if len(textesExclus)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_CONTENANT')+" %s"%', '.join(textesExclus))
      if len(textesRemplaces)>len(COLORseq):
        for src,dst in textesRemplaces[len(COLORseq):]:
          actions.append(self.projet.lexique.entree('LOCUT_APRES_REMPLACEMENT_DE')%(src,dst))
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       objet=paramCommandeSansIndex,
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      # execution
      ligneCommande=' '.join([paramOutil,tempReference,tempCible])
      (resRetour,resSortie)=nngsrc.services.SRV_Execute(ligneCommande,"commands")

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1131,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':'%s %s %s'%(paramOutil,paramReference,paramCible),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramCible],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    paramOutil=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramCible=controle.CTL_PARAMETRES[2]

    # Retrait d'index
    paramReferenceSansIndex=nngsrc.services.SRV_retraitIndexation(paramReference)
    paramCibleSansIndex=nngsrc.services.SRV_retraitIndexation(paramCible)
    paramCommandeSansIndex=' '.join([nngsrc.services.SRV_retraitIndexation(paramOutil),paramReferenceSansIndex,paramCibleSansIndex])
    droit=os.access(paramCibleSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    paramNumLignesExclues=''
    paramTextesExclus=''
    paramTextesRemplaces=''

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,paramReferenceSansIndex,paramCibleSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramCibleSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramCibleSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))
    tempReference="tempReference.txt"
    reduitFichier(paramReferenceSansIndex,tempReference,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=[]
      if calculDebut!='': rapDebut=calculDebut
      if calculFin!='': rapFin=calculFin-1
      actions.append(self.projet.lexique.entree('MOT_RESTRICTION'))
      actions.append(self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT')%paramCibleSansIndex)
      if calculDebut!='':
        actions.append(self.projet.lexique.entree('LOCUT_DE_LA_LIGNE')+" %d"%calculDebut)
      if calculFin!='':
        actions.append(self.projet.lexique.entree('LOCUT_A_LA_LIGNE')+" %d"%calculFin)
      if len(numLignesExclues)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_NUMERO')+" %s"%', '.join(["%d"%e for e in numLignesExclues]))
      if len(textesExclus)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_CONTENANT')+" %s"%', '.join(textesExclus))
      if len(textesRemplaces)>len(COLORseq):
        for src,dst in textesRemplaces[len(COLORseq):]:
          actions.append(self.projet.lexique.entree('LOCUT_APRES_REMPLACEMENT_DE')%(src,dst))
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       objet=paramCommandeSansIndex,
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      # execution
      ligneCommande=' '.join([paramOutil,tempReference,tempCible])
      (resRetour,resSortie)=nngsrc.services.SRV_Execute(ligneCommande,"commands")
      resSortie=resSortie.replace(tempReference,paramReferenceSansIndex).replace(tempCible,paramCibleSansIndex)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1131,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':'%s %s %s'%(paramOutil,paramReference,paramCible),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramCible],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    paramOutil=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramCible=controle.CTL_PARAMETRES[2]

    # Retrait d'index
    paramReferenceSansIndex=nngsrc.services.SRV_retraitIndexation(paramReference)
    paramCibleSansIndex=nngsrc.services.SRV_retraitIndexation(paramCible)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    rapDebut=-1
    rapFin=-1
    paramNumLignesExclues=''
    paramTextesExclus=''
    paramExtensionsExclues=''
    paramTextesRemplaces=''
    dicoRES={}
    resRepRetour=''
    resRepSortie=''

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,paramReferenceSansIndex,paramCibleSansIndex,self.projet)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=[]
      if calculDebut!='': rapDebut=calculDebut
      if calculFin!='': rapFin=calculFin-1
      actions.append(self.projet.lexique.entree('MOT_RESTRICTION'))
      actions.append(self.projet.lexique.entree('LOCUT_EXAMEN_FICHIERS_PRODUITS')%paramCibleSansIndex)
      if len(extensionsExclues)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_FICHIERS')+" %s"%', '.join(extensionsExclues))
      if calculDebut!='':
        actions.append(self.projet.lexique.entree('LOCUT_DE_LA_LIGNE')+" %d"%calculDebut)
      if calculFin!='':
        actions.append(self.projet.lexique.entree('LOCUT_A_LA_LIGNE')+" %d"%calculFin)
      if len(numLignesExclues)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_NUMERO')+" %s"%', '.join(["%d"%e for e in numLignesExclues]))
      if len(textesExclus)>0:
        actions.append(self.projet.lexique.entree('LOCUT_EXCEPTE_LES_LIGNES_CONTENANT')+" %s"%', '.join(textesExclus))
      if len(textesRemplaces)>len(COLORseq):
        for src,dst in textesRemplaces[len(COLORseq):]:
          actions.append(self.projet.lexique.entree('LOCUT_APRES_REMPLACEMENT_DE')%(src,dst))
      (resRepRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXECUTION_COMMANDE'),
                                                                       objet="%s %s %s"%(paramOutil,paramReferenceSansIndex,paramCibleSansIndex),
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      resRepRetour=0
      if not os.path.exists(paramReferenceSansIndex):
        resBilan=self.STATUS_PB
        resRepRetour=self.projet.lexique.entree("LOCUT_REPERTOIRE_INEXISTANT")
      else:
        # Parcours des fichiers du répertoire cible
        for fichCible in glob.glob(os.path.join(paramCibleSansIndex,"*.*")):
          _,extension=os.path.splitext(fichCible)
          if extension in extensionsExclues: continue

          fichReference=os.path.join(paramReferenceSansIndex,os.path.basename(fichCible))
          calculDebut,calculFin=restrictionsDebFin(pCommande,executionCMRY,fichReference,fichCible)

          # Droit d'accès
          droit=os.access(fichCible, os.F_OK)
          if not droit:
            tempCible=fichCible
          else:
            tempCible="tempCible.txt"
            reduitFichier(fichCible,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))
          tempReference="tempReference.txt"
          reduitFichier(fichReference,tempReference,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))
          ligneCommande=' '.join([paramOutil,tempReference,tempCible])

          # Traitement
          (resRetour,resSortie)=nngsrc.services.SRV_Execute(ligneCommande,"commands")
          if not(resRetour==int(resAttendu)):
            resBilan=self.STATUS_PB
            resRepRetour=resRetour
          dicoRES[fichCible]=(resRetour,resSortie)
          # Reajustement de la sortie vide
          if resSortie!='':
            resRepSortie+='---------------------------------\n%s : %s vs %s\n'%(self.projet.lexique.entree('MOT_FICHIER'),fichReference,fichCible)
            resRepSortie+='%s\n'%resSortie.replace(tempReference,fichReference).replace(tempCible,fichCible)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)
      # Reajustement de la sortie vide
      if resRepSortie=='':
        resRepSortie=paramSortieSiVide

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1131,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EXECUTION_COMMANDE',
        'VAL_COMMENT':'%s %s %s'%(paramOutil,paramReference,paramCible),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIERS_PRODUITS'),'SUB_COMPLEMENT':[paramCible],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),       'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],            'VAL_CONSTAT':[resRepRetour],'STY_CONSTAT':"tt", 'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resRepSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareValeur(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareValeur",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareValeur',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- outil de comparaison
    FONCTION=controle.CTL_PARAMETRES[0]
    # 1- Reference
    paramReference=controle.CTL_PARAMETRES[1]
    # 2- Cible
    paramOutil=controle.CTL_PARAMETRES[2]

    if controle.CTL_LIBELLE is None:
      controle.CTL_LIBELLE=self.projet.lexique.entree('LOCUT_EVALUER_VAL')

    # Retrait d'index
    paramCommandeSansIndex=' '.join([nngsrc.services.SRV_retraitIndexation(paramOutil)])

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    if FONCTION in ["EGALE_A","EGALE"]:
      resAttendu=self.projet.lexique.entree('LOCUT_EGALE_A')%paramReference
    elif FONCTION=="DIFFERENTE_DE":
      resAttendu=self.projet.lexique.entree('LOCUT_DIFFERENTE_DE')%paramReference
    elif FONCTION=="MOINS_DE":
      resAttendu=self.projet.lexique.entree('LOCUT_MOINS_DE')%paramReference
    elif FONCTION=="PLUS_DE":
      resAttendu=self.projet.lexique.entree('LOCUT_PLUS_DE')%paramReference
    elif FONCTION=="ENTRE":
      Binf=paramReference.split(' ')[0]
      Bsup=paramReference.split(' ')[1]
      resAttendu=self.projet.lexique.entree('LOCUT_ENTRE')%(Binf,Bsup)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EVALUER_VAL'),
                                                                       actions=[paramCommandeSansIndex,
                                                                                self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_RETOUR'),
                                                                                self.projet.lexique.entree('LOCUT_VALEUR_ATTENDUE')+" "+FONCTION+" "+paramReference],
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      try:
        resRetour=str(float(paramCommandeSansIndex))
        resSortie=''
      except Exception:
        (resRetour,resSortie)=nngsrc.services.SRV_Execute(paramCommandeSansIndex,"commands")
      resBilan=self.STATUS_PB
      try:
        if FONCTION in ["EGALE_A","EGALE"]:
          if float(resRetour)==float(paramReference):
            resBilan=self.STATUS_OK
        elif FONCTION=="DIFFERENTE_DE":
          if float(resRetour)==float(paramReference):
            resBilan=self.STATUS_OK
        elif FONCTION=="MOINS_DE":
          if float(resRetour)<float(paramReference):
            resBilan=self.STATUS_OK
        elif FONCTION=="PLUS_DE":
          if float(resRetour)>float(paramReference):
            resBilan=self.STATUS_OK
        elif FONCTION=="ENTRE":
          Binf=paramReference.split(' ')[0]
          Bsup=paramReference.split(' ')[1]
          if float(Binf) < float(resRetour) < float(Bsup):
            resBilan=self.STATUS_OK
      except Exception:
        resBilan=self.STATUS_PB

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

#      # Reajustement du bilan en fonction de l'ordre de validation
#      if resBilan==self.STATUS_PB and controle.CTL_VALIDER:
#        resBilan=self.STATUS_AV
#      # Reajustement de la sortie vide
#      if resSortie=='':
#        resSortie=paramSortieSiVide

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EVALUER_VAL',
        'VAL_COMMENT':paramOutil,'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),                   'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],                  'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_CompareChaine(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_CompareChaine",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_CompareChaine',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Reference
    CHAINE_1=controle.CTL_PARAMETRES[0]
    # 1- Cible
    CHAINE_2=controle.CTL_PARAMETRES[1]

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    decoupe=[]
    textedecoupe=''
    if len(controle.CTL_PARAMETRES)==3:
      decoupe=controle.CTL_PARAMETRES[2].split(":")
      textedecoupe=" sous-chaîne de '"+controle.CTL_PARAMETRES[1]+"'"
      if decoupe[1]!='':
        CHAINE_2=CHAINE_2[:int(decoupe[1])]
      if decoupe[0]!='':
        CHAINE_2=CHAINE_2[int(decoupe[0])-1:]
    chaine1=CHAINE_1
    chaine2=CHAINE_2

    CASSE=int(pCommande['CASSEP'])
    if CASSE==0:
      chaine1=CHAINE_1.lower()
      chaine2=CHAINE_2.lower()
    optionCasse=''
    if CASSE is not None:
      if CASSE==1:
        optionCasse=" "+self.projet.lexique.entree('LOCUT_CASSE_EXACTE')

    if controle.CTL_LIBELLE is None:
      controle.CTL_LIBELLE=self.projet.lexique.entree('LOCUT_EVALUER_EGAL_INCLUSION')

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EVALUER_EGAL_INCLUSION')+optionCasse,
                                                                       objet="'%s' vs '%s'%s"%(CHAINE_1,CHAINE_2,textedecoupe),
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_PB
      resRetour=-1
      resSortie=''
      if not decoupe:
        if chaine1==chaine2:
          if int(resAttendu)==0:
            resBilan=self.STATUS_OK
          resRetour=0
          resSortie="'"+chaine1+"'='"+chaine2+"'"
        else:
          if int(resAttendu)==-1:
            resBilan=self.STATUS_OK
          resRetour=-1
          resSortie="'"+chaine1+"'!='"+chaine2+"'"
      else:
        if chaine1.find(chaine2)!=-1:
          if int(resAttendu)==0:
            resBilan=self.STATUS_OK
          resRetour=0
          resSortie="'"+chaine1+"' contient '"+chaine2+"'"
        else:
          if int(resAttendu)==-1:
            resBilan=self.STATUS_OK
          resRetour=-1
          resSortie="'"+chaine1+"' ne contient pas '"+chaine2+"'"

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(1031,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_EVALUER_VAL',
        'VAL_COMMENT':"'%s' vs '%s'%s"%(CHAINE_1,CHAINE_2,textedecoupe),'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_EVALUER_EGAL_INCLUSION')+optionCasse,  'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],                     'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan,
        #------------------------------------------
        'CLF_RAPPORT':'LOCUT_RAPPORTER_SORTIE_EXECUTION',
        'VAL_RAPPORT':resSortie,'STY_RAPPORT':"tt"})
        #------------------------------------------
    if 'RETOUR' in pCommande:
      params.update({'ATD_RETOUR':resAttendu})
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration
  # CATEGORIE  : Recherche
  # FONCTION   : VALID_RechercheXTexte
  # DESCRIPTION: Cherche la présence ou l'absence de chaîne de caractères
  # PARAMETRES : {fichier}
  # RETOUR     : TODO
  # TEXTE      : TODO
  #-------------------------------------------------------------------------------
  def VALID_RechercheTexte(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RechercheTexte",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RechercheTexte',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)
    droit=os.access(paramFichierSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    optionCasse=''
    DicoChaine={0:'LOCUT_VERIFIER_ABSENCE',1:'LOCUT_VERIFIER_PRESENCE'}
    DicoOccurence={0:self.projet.lexique.entree("MOT_ABSENT"),1:self.projet.lexique.entree("MOT_PRESENT"),'':''}
    resMessage=[DicoOccurence[resRetour]]

    # Casse
    paramCasse=int(pCommande['CASSE'])
    if paramCasse==1:
      optionCasse=" "+self.projet.lexique.entree('LOCUT_CASSE_EXACTE')

    # Trouver : oui ou non ?
    if 'TROUVER_OUI' in pCommande:
      chaine=retraitMAGIDX(nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_OUI'],self.projet.execution))
      if paramCasse==0: chaine=chaine.lower()
      resAttendu=1
    else:
      chaine=retraitMAGIDX(nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_NON'],self.projet.execution))
      if paramCasse==0: chaine=chaine.lower()
      resAttendu=0

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,None,paramFichierSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramFichierSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramFichierSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=["%s %s"%(self.projet.lexique.entree('MOT_CHAINE'),chaine)
              ,"%s %s"%(self.projet.lexique.entree('LOCUT_NB_OCCURENCES_ATTENDUES'),resAttendu)]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse,
                                                                       objet=paramFichierSansIndex,
                                                                       actions=actions,
                                                                       attendu=resAttendu)
      resMessage=[DicoOccurence[resRetour]]
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      resBilan=self.STATUS_PB
      if not os.access(tempCible, os.F_OK):
        resMessage=[self.projet.lexique.entree("ERR_ACCES_SOURCE")]
      else:
        f=open(tempCible,'r',encoding='utf-8')
        for text in f:
            if paramCasse==0:
               text=text.lower()
            text=nngsrc.services.SRV_Encodage(text,"Decodage du texte dans VALID_RechercheTexte")
            if text.find(chaine) != -1:
               resRetour=1
               break
        f.close()
        resMessage=[DicoOccurence[resRetour]]
        # Analyse du code de retour
        if resRetour*resAttendu==1:
           resBilan=1
        if resRetour+resAttendu==0:
           resBilan=-1
        if resBilan==-1:
           resBilan=1

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(113,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RELEVER_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramFichier],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse,         'CLF_CONSTAT':'MOT_CONSTAT', #CLF_BILAN
        'CLF_ATTENDU':[DicoChaine[resAttendu]],'VAL_ATTENDU':[chaine],                               'VAL_CONSTAT':resMessage         ,'STY_CONSTAT':"tt",  'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration
  # FONCTION   : VALID_RechercheXTexte
  # DESCRIPTION: Cherche la présence et/ou l'absence de chaîne de caractères
  # PARAMETRES : fichier
  # RETOUR     : TODO
  # TEXTE      : TODO
  #-------------------------------------------------------------------------------
  def VALID_RechercheXTexte(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RechercheXTexte",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RechercheXTexte',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)
    droit=os.access(paramFichierSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
#    resRetour=''
#    resSortie=''
    listeDeChaines=[]
    listeDeResults=[]
    listeDePresence=[]
    listeDOccurence=[]
    DicoChaine={}
    ListChaine=[]
    LstChaineOui=[]
    LstChaineNon=[]
    chaineOui=''
    chaineNon=''
    optionCasse=''

    # Casse
    paramCasse=int(pCommande['CASSE'])
    if paramCasse==1:
      optionCasse=" "+self.projet.lexique.entree('LOCUT_CASSE_EXACTE')

    # Trouver : oui ou non ?
    if 'TROUVER_OUI' in pCommande:
      chaineOui=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_OUI'],self.projet.execution)
      if paramCasse==0: chaineOui=chaineOui.lower()
      LstChaineOui=chaineOui.split('|')
    if 'TROUVER_NON' in pCommande:
      chaineNon=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_NON'],self.projet.execution)
      if paramCasse==0: chaineNon=chaineNon.lower()
      LstChaineNon=chaineNon.split('|')

    for pat in LstChaineOui:
      pat=retraitMAGIDX(pat)
      ListChaine.append(pat)
      DicoChaine[pat]=(1,'LOCUT_VERIFIER_PRESENCE')
    for pat in LstChaineNon:
      pat=retraitMAGIDX(pat)
      ListChaine.append(pat)
      DicoChaine[pat]=(0,'LOCUT_VERIFIER_ABSENCE')

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,None,paramFichierSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramFichierSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramFichierSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      for pat in LstChaineOui:
        pat=retraitMAGIDX(pat)
        listeDeChaines.append(pat)
        trouverChaine,message=DicoChaine[pat]
        listeDePresence.append(message)
        actions=["%s %s"%(self.projet.lexique.entree('LOCUT_VERIFIER_PRESENCE'),pat)]
        (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                         message=self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse+":",
                                                                         objet=paramFichierSansIndex,
                                                                         actions=actions,
                                                                         attendu=self.projet.lexique.entree("MOT_PRESENT"))
        listeDOccurence.append(resRetour)
      for pat in LstChaineNon:
        pat=retraitMAGIDX(pat)
        listeDeChaines.append(pat)
        trouverChaine,message=DicoChaine[pat]
        listeDePresence.append(message)
        actions=["%s %s"%(self.projet.lexique.entree('LOCUT_VERIFIER_ABSENCE'),pat)]
        (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                         message=self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse+":",
                                                                         objet=paramFichierSansIndex,
                                                                         actions=actions,
                                                                         attendu=self.projet.lexique.entree("MOT_ABSENT"))
        listeDOccurence.append(resRetour)

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      resBilan=self.STATUS_PB
      textecomplet=''
      if not os.access(tempCible, os.F_OK):
        for n in range(len(ListChaine)):
          pat=retraitMAGIDX(ListChaine[n])
          listeDeChaines.append(pat)
          trouverChaine,message=DicoChaine[pat]
          listeDePresence.append(message)
          if n==0:
            listeDOccurence.append(self.projet.lexique.entree("ERR_ACCES_SOURCE"))
          else:
            listeDOccurence.append('')
      else:
        resBilan=self.STATUS_OK
        f=open(tempCible,'r',encoding='utf-8')
        for text in f:
          if paramCasse==0:
            text=text.lower()
          text=nngsrc.services.SRV_Encodage(text,"Decodage du texte dans VALID_RechercheXTexte")
          textecomplet+=text
        f.close()
        for pat in ListChaine:
          pat=retraitMAGIDX(pat)
          listeDeChaines.append(pat)
          trouverChaine,message=DicoChaine[pat]
          listeDePresence.append(message)
          if textecomplet.find(pat)!=-1:
            listeDOccurence.append(self.projet.lexique.entree("MOT_PRESENT"))
            resRetour=1
          else:
            listeDOccurence.append(self.projet.lexique.entree("MOT_ABSENT"))
            resRetour=0
          listeDeResults.append(resRetour)
          # Analyse du code de retour
          if resBilan==self.STATUS_OK:
            if resRetour*trouverChaine==1:
              resBilan=self.STATUS_OK
            elif resRetour+trouverChaine==0:
              resBilan=self.STATUS_OK
            else:
              resBilan=self.STATUS_PB

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    #----------------------------
    # Traitement des modes P et C
    if executionCMRY in ['P','C','S']:
      for pat in ListChaine:
        pat=retraitMAGIDX(pat)
        listeDeChaines.append(pat)
        _,message=DicoChaine[pat]
        listeDePresence.append(message)
        listeDOccurence.append('')

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(113,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RELEVER_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramFichier],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse,      'CLF_CONSTAT':'MOT_CONSTAT',                        #CLF_BILAN
        'CLF_ATTENDU':listeDePresence,'VAL_ATTENDU':listeDeChaines,                               'VAL_CONSTAT':listeDOccurence,'STY_CONSTAT':"tt",  'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration
  # FONCTION   : VALID_RechercheMultipleTexte
  # DESCRIPTION: Cherche "retour" présence de chaîne de caractères
  # PARAMETRES : fichier
  # RETOUR     : TODO
  # TEXTE      : TODO
  #-------------------------------------------------------------------------------
  def VALID_RechercheMultipleTexte(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RechercheMultipleTexte",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RechercheMultipleTexte',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)
    droit=os.access(paramFichierSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    optionCasse=''

    # Casse
    paramCasse=int(pCommande['CASSE'])
    if paramCasse==1:
      optionCasse=" "+self.projet.lexique.entree('LOCUT_CASSE_EXACTE')

    # Trouver : oui ou non ?
    chaine=nngsrc.services.SRV_CleanOperateur(pVarOperateur,pCommande['TROUVER_OUI'],self.projet.execution)
    if paramCasse==0: chaine=chaine.lower()

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,None,paramFichierSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramFichierSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramFichierSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      actions=["%s%s:"%(self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER'),optionCasse)
              ,"  %s %s"%(self.projet.lexique.entree('MOT_CHAINE'),chaine)
              ,"  %s %s"%(self.projet.lexique.entree('LOCUT_NB_OCCURENCES_ATTENDUES'),resAttendu)]
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_RELEVER_FICHIER')%'',
                                                                       objet=paramFichierSansIndex,
                                                                       actions=actions,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resRetour=0
      if not os.access(tempCible, os.F_OK):
        resBilan=self.STATUS_PB
        resRetour=self.projet.lexique.entree("ERR_ACCES_SOURCE")
      else:
        f=open(tempCible, 'r',encoding='utf-8')
        for text in f:
            if paramCasse==0:
               text=text.lower()
            resRetour=resRetour+nbTrouves(text,chaine)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,resRetour==int(resAttendu),self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(113,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RELEVER_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramFichier],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_CHERCHER_DANS_FICHIER')+optionCasse,         'CLF_CONSTAT':'MOT_CONSTAT',                     #CLF_BILAN
        'VAL_ACTION' :[self.projet.lexique.entree('MOT_CHAINE'),chaine],'STY_ACTION':["norm","tt"],
        'CLF_ATTENDU':['LOCUT_NB_OCCURENCES_ATTENDUES'],'VAL_ATTENDU':[resAttendu],                  'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_ControleAccesFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_ControleAccesFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_ControleAccesFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Mode d'acces
    paramMode=controle.CTL_PARAMETRES[0]
    # 1- Nom du fichier
    paramFichier=controle.CTL_PARAMETRES[1]
    # 2- Timer (optionnel)
    paramTimeOut=0
    if len(controle.CTL_PARAMETRES)==3:
      paramTimeOut=int(controle.CTL_PARAMETRES[2])

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    if paramMode=="LECTURE":
      modeAcces=os.R_OK
      openAcces="r"
    elif paramMode=="ECRITURE":
      modeAcces=os.W_OK
      openAcces="a"
    elif paramMode=="EXECUTE":
      modeAcces=os.X_OK
      openAcces=''
    else:           # EXISTE
      modeAcces=os.F_OK
      openAcces=''

    paramRetour=True
    if controle.CTL_RETOUR is not None: paramRetour=(controle.CTL_RETOUR=='True')
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=paramRetour)
#    resRetour=''
    resAttendu,resComment=controleFichier(modeAcces,paramRetour)
    resAttendu=self.projet.lexique.entree(resAttendu)
    resComment=self.projet.lexique.entree(resComment)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_VERIFIER_ACCES'),
                                                               objet=paramFichierSansIndex,
                                                               actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+resAttendu],
                                                               attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      droit=nngsrc.services.SRV_acceder(paramFichierSansIndex,modeAcces,paramTimeOut)
      if os.path.isfile(paramFichierSansIndex) and (paramMode=="LECTURE" or paramMode=="ECRITURE"):
        try:
          f=open(paramFichierSansIndex,openAcces,encoding='utf-8')
          f.close()
        except Exception:
          droit=False
      resRetour,_=controleFichier(modeAcces,droit)
      resRetour=self.projet.lexique.entree(resRetour)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,droit==paramRetour,self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_VERIFIER_ACCES_FICHIER'),'SUB_COMMENT':[paramFichierSansIndex],'LNK_COMMENT':True,'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%resComment,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_CONSTAT_ATTENDU'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_NombreFichiersRepertoires(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_NombreFichiersRepertoires",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_NombreFichiersRepertoires',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Repertoire
    paramRepertoire=controle.CTL_PARAMETRES[0]
    # 1- Flag Repertoire/Fichier
    paramFlagRF=controle.CTL_PARAMETRES[1]
    # 2- Pattern
    paramPattern=None
    strPattern=""
    if len(controle.CTL_PARAMETRES)==3:
      paramPattern=controle.CTL_PARAMETRES[2]
      strPattern=" (%s)"%paramPattern

    # Retrait d'index
    paramRepertoireSansIndex=nngsrc.services.SRV_retraitIndexation(paramRepertoire)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pListeRetour=True)
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_COMPTER_%s'%paramFlagRF)%'',
                                                                       objet=paramRepertoireSansIndex,
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resAttendu=int(resAttendu)
      if os.access(paramRepertoireSansIndex,os.F_OK):
        if paramPattern is None:
          listeElements=glob.glob(os.path.join(paramRepertoireSansIndex,"*"))
          listeElements.extend(glob.glob(os.path.join(paramRepertoireSansIndex,".*")))
        else:
          listeElements=glob.glob(os.path.join(paramRepertoireSansIndex,paramPattern))
        if paramFlagRF=="FICHIERS":
          resRetour=len([name for name in listeElements if os.path.isfile(name)])
        elif paramFlagRF=="REPERTOIRES":
          resRetour=len([name for name in listeElements if os.path.isdir(name)])
      else:
        resRetour=0

    # Analyse et ajustements
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_COMPTER_%s'%paramFlagRF)+strPattern,'SUB_COMMENT':[paramRepertoire],'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%(self.projet.lexique.entree("MOT_%s"%paramFlagRF),resAttendu),
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_ControleAccesRepertoire(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_ControleAccesRepertoire",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_ControleAccesRepertoire',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Mode d'acces
    paramMode=controle.CTL_PARAMETRES[0]
    # 1- Nom du répertoire
    paramRepert=controle.CTL_PARAMETRES[1]
    # 2- Timer (optionnel)
    paramTimeOut=0
    if len(controle.CTL_PARAMETRES)==3:
      paramTimeOut=int(controle.CTL_PARAMETRES[2])

    # Retrait d'index
    paramRepertSansIndex=nngsrc.services.SRV_retraitIndexation(paramRepert)

    # Initialisations
    if paramMode=="LECTURE":
      modeAcces=os.R_OK
    elif paramMode=="ECRITURE":
      modeAcces=os.W_OK
    elif paramMode=="EXECUTE":
      modeAcces=os.X_OK
    else:          # EXISTE
      modeAcces=os.F_OK
    paramRetour=True
    if controle.CTL_RETOUR is not None: paramRetour=(controle.CTL_RETOUR=='True')
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=paramRetour)
#    resRetour=''
    resAttendu,resComment=controleFichier(modeAcces,paramRetour)
    resAttendu=self.projet.lexique.entree(resAttendu)
    resComment=self.projet.lexique.entree(resComment)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_VERIFIER_ACCES'),
                                                               objet=paramRepertSansIndex,
                                                               actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+resAttendu],
                                                               attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      droit=nngsrc.services.SRV_acceder(paramRepertSansIndex,modeAcces,paramTimeOut)
      if os.path.isdir(paramRepertSansIndex):
        if paramMode=="LECTURE":
          try:
            dirs=os.listdir(paramRepertSansIndex)
          except Exception:
            droit=False
        elif paramMode=="ECRITURE":
          try:
            testDirNangu=os.path.join(paramRepertSansIndex,"testDirNangu")
            testFileNangu=os.path.join(paramRepertSansIndex,"testFileNangu")
            os.mkdir(testDirNangu)
            os.rmdir(testDirNangu)
            f=open(testFileNangu,"w",encoding='utf-8')
            f.close()
            os.remove(testFileNangu)
          except Exception:
            droit=False
      resRetour,_=self.projet.lexique.entree(controleFichier(modeAcces,droit))
      resRetour=self.projet.lexique.entree(resRetour)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,droit==paramRetour,self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree("LOCUT_VERIFIER_ACCES_REPERTOIRE"),'SUB_COMMENT':[paramRepertSansIndex],'LNK_COMMENT':True,'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%resComment,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree("LOCUT_RESULTAT_ACTION"),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_CONSTAT_ATTENDU'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_InspecteFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_InspecteFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_InspecteFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    vchamp=None
    separateur=''
    # 0- Fichier texte à inspecter
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1- Numero de ligne
    ligne=controle.CTL_PARAMETRES[1]
    # 2/3- Séparateur/champ (optionnnel)
    if len(controle.CTL_PARAMETRES)>2:
      separateur=controle.CTL_PARAMETRES[2]
      champ=controle.CTL_PARAMETRES[3]
      if champ!='':
        vchamp=int(champ)
        docMessage=self.projet.lexique.entree('InspecteFichier_LIBELLE_CHAMP')%(vchamp,ligne,separateur) #"|Rechercher le champ n°%d de la ligne n°%s délimitée par '%s'"%(vchamp,ligne,separateur)
      else:
        docMessage=self.projet.lexique.entree('InspecteFichier_LIBELLE_LIGNE')%ligne #"|Rechercher la ligne n°%s"%ligne
    else:
      docMessage=self.projet.lexique.entree('InspecteFichier_LIBELLE_LIGNE')%ligne #"|Rechercher la ligne n°%s"%ligne

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None,pListeRetour=True)
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_INSPECTE_FICHIER'),
                                                                       objet=paramFichierSansIndex,
                                                                       actions=[docMessage,"%s %s"%(self.projet.lexique.entree('LOCUT_VALEUR_ATTENDUE'),resAttendu)])
      if resBilan==self.STATUS_OK:
        resRetour=resAttendu
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      ligne=int(ligne)
      resRetour=self.projet.lexique.entree('Fonction_ERREUR_LECTURE')
      if os.path.exists(paramFichierSansIndex):
        contenu=nngsrc.services.SRV_LireContenu(paramFichierSansIndex).split('\n')
        resRetour=self.projet.lexique.entree('Inspection_ERREUR_LIGNE')
        parser=None
        if 1<=ligne<=len(contenu):
          parser=contenu[ligne-1]
        elif -len(contenu)<=ligne<=-1:
          parser=contenu[ligne]
        if parser is not None:
          if vchamp is not None:
            parser=parser.split(separateur)
            resRetour=self.projet.lexique.entree('Inspection_ERREUR_CHAMP')
            parsligne=None
            if 1<=vchamp<=len(parser):
              parsligne=parser[vchamp-1]
            elif -len(parser)<=vchamp<=-1:
              parsligne=parser[vchamp]
            if parsligne is not None:
              resRetour=parsligne
          else:
            resRetour=parser
    # Analyse du code de retour
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_INSPECTE_FICHIER',
        'VAL_COMMENT':paramFichier,'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION' :docMessage,                                                        'CLF_CONSTAT':'MOT_CONSTAT',                        #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",      'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_InspecteNetCDF(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_InspecteNetCDF",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_InspecteNetCDF',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    paramLigne=None
    paramColonne=None
    # 0- Fichier NetCDF à inspecter
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1- Champ
    docMessage="%s %s"%(self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR_CHAMP'),controle.CTL_PARAMETRES[1])
    paramChamps=controle.CTL_PARAMETRES[1].split(':')
    # 2- Ligne de matrice ou index de tableau
    if len(controle.CTL_PARAMETRES)>2:
      paramLigne=int(controle.CTL_PARAMETRES[2])
      docMessage+=", indice [%d]"%paramLigne
    # 3- Colonne de matrice
    if len(controle.CTL_PARAMETRES)>3:
      paramColonne=int(controle.CTL_PARAMETRES[3])
      docMessage+="[%d]"%paramColonne

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None,pListeRetour=True)
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_INSPECTE_NETCDF'),
                                                                       objet=paramFichierSansIndex,
                                                                       actions=[docMessage,"%s %s"%(self.projet.lexique.entree('LOCUT_VALEUR_ATTENDUE'),resAttendu)])
      if resBilan==self.STATUS_OK:
        resRetour=resAttendu
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if os.access(paramFichierSansIndex,os.F_OK) and NANGU_NETCDF4:
        ncf=netCDF4.Dataset(paramFichierSansIndex,'r')
        resRetour=str(nngsrc.services.SRV_recursNCF(ncf,paramChamps,'N'))
        ncf.close()
      else:
        resRetour=''
    # Analyse du code de retour
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_INSPECTE_NETCDF',
        'VAL_COMMENT':paramFichierSansIndex,'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION' :docMessage,                                                        'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'], 'VAL_ATTENDU': [resAttendu],           'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",      'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_InspecteJSON(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_InspecteJSON",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_InspecteJSON',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    paramLigne=None
    paramColonne=None
    # 0- Fichier json à inspecter
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1- Champ
    docMessage="Contrôler la valeur du champ %s"%controle.CTL_PARAMETRES[1]
    paramChamps=controle.CTL_PARAMETRES[1].split(':')
    # 2- Ligne de matrice ou index de tableau
    if len(controle.CTL_PARAMETRES)>2:
      paramLigne=int(controle.CTL_PARAMETRES[2])
      docMessage+=", indice [%d]"%paramLigne
    # 3- Colonne de matrice
    if len(controle.CTL_PARAMETRES)>3:
      paramColonne=int(controle.CTL_PARAMETRES[3])
      docMessage+="[%d]"%paramColonne

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None,pListeRetour=True)
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_INSPECTE_JSON'),
                                                                       objet=paramFichierSansIndex,
                                                                       actions=[docMessage,"%s %s"%(self.projet.lexique.entree('LOCUT_VALEUR_ATTENDUE'),resAttendu)])
      if resBilan==self.STATUS_OK:
        resRetour=resAttendu
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if os.access(paramFichierSansIndex,os.F_OK):
        with open(paramFichierSansIndex,"r",encoding='utf-8') as read_file:
          donJson=json.load(read_file)
        for champ in paramChamps:
          if champ in donJson:
            donJson=donJson[champ]
          else:
            donJson="Champ '%s' inconnu"%champ
            break
        if paramLigne is not None:
          donJson=donJson[paramLigne]
        if paramColonne is not None:
          donJson=donJson[paramColonne]
        resRetour=str(donJson)
      else:
        resRetour=''
    # Analyse du code de retour
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_INSPECTE_JSON',
        'VAL_COMMENT':paramFichierSansIndex,'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION' :docMessage,                                                        'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'], 'VAL_ATTENDU': [resAttendu],           'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",      'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_InspecteXML(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):#
    if self.projet.debug>2: printFonction("VALID_InspecteXML",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_InspecteXML',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    paramLigne=None
    paramColonne=None
    # 0- Fichier xml à inspecter
    paramFichier=controle.CTL_PARAMETRES[0]
    # 1- Champ
    docMessage="Contrôler la valeur du champ %s"%controle.CTL_PARAMETRES[1]
    paramChamps=controle.CTL_PARAMETRES[1].split(':')
    # 2- Ligne de matrice ou index de tableau
    if len(controle.CTL_PARAMETRES)>2:
      paramLigne=int(controle.CTL_PARAMETRES[2])
      docMessage+=", indice [%d]"%paramLigne
    # 3- Colonne de matrice
    if len(controle.CTL_PARAMETRES)>3:
      paramColonne=int(controle.CTL_PARAMETRES[3])
      docMessage+="[%d]"%paramColonne

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None,pListeRetour=True)
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_EXAMINER_FICHIER')+" "+self.projet.lexique.entree('LOCUT_CONSTATER_EVENEMENT'),
                                                                       objet=paramFichierSansIndex,
                                                                       actions=[docMessage,"%s %s"%(self.projet.lexique.entree('LOCUT_VALEUR_ATTENDUE'),resAttendu)])
      if resBilan==self.STATUS_OK:
        resRetour=resAttendu
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if os.access(paramFichierSansIndex,os.F_OK):
        donXML=nngsrc.servicesXML.SRVxml_parser(paramFichierSansIndex)
        noeudCourant=donXML
        for champ in paramChamps:
          directives=champ.split('.')
          if directives[0] in ["N","V"] and len(directives)==2:
            # Cas N.<noeud> ou V.<noeud> : on recupère un noeud
            noeudCourant=nngsrc.servicesXML.SRVxml_recupNoeud(noeudCourant,directives[1])
            if noeudCourant is None:
              retour="Champ '%s' inconnu"%directives[1]
              break
            # Cas V.<noeud> : on recupère la valeur du noeud
            if directives[0]=="V":
              retour=nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeudCourant,valDefaut='')
          elif directives[0]=="N" and len(directives)==3:
            # Cas N.<numero>.<noeud> : on recupère un noeud d'une liste
            noeudCourant=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeudCourant,directives[2])
            numero=int(directives[1])-1
            if len(noeudCourant)>numero:
              noeudCourant=noeudCourant[numero]
            else:
              retour="Champ '%s' inconnu"%directives[1]
              break
          elif directives[0]=="N" and len(directives)==4:
            # Cas N.<noeud>.A.<attribut)=<valeur> : on recupère le noeud d'une liste dont l'attrubut a pour valeur
            listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeudCourant,directives[1])
            [attribut,valeur]=directives[3].split('=')
            for noeud in listeNoeuds:
               if nngsrc.servicesXML.SRVxml_recupValAttribut(noeud,attribut,valDefaut=None)==valeur:
                 noeudCourant=noeud
                 break
          elif directives[0]=="A":
              # Cas A.<attribut> : on recupère l'attribut du noeud
              retour=nngsrc.servicesXML.SRVxml_recupValAttribut(noeudCourant,directives[1],valDefaut='')
          else:
            retour="Directive '%s' inconnu"%champ
            break
        if paramLigne is not None:
          retour=retour[paramLigne]
        if paramColonne is not None:
          retour=retour[paramColonne]
        resRetour=str(retour)
      else:
        resRetour=''
    # Analyse du code de retour
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'LOCUT_INSPECTE_FICHIER',
        'VAL_COMMENT':paramFichierSansIndex,'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION' :docMessage,                                                        'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU': ['LOCUT_VALEUR_ATTENDUE'], 'VAL_ATTENDU': [resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",      'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_DateFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_DateFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_DateFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Nom du fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pListeRetour=True,pExtensionResAttendu='')
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]
    amjhs=lst_resAttendu[2]

    #-- délai --
    resDateMaintenant=time.strftime(self.projet.lexique.dateheure(),time.localtime(time.time()))
    if resAttendu!="MAINTENANT":
      if self.projet.debug==2:
        if self.projet.lexique.langue=="FR":
          resDateMaintenant="29/02/2016 12:34"
        elif self.projet.lexique.langue=="EN":
          resDateMaintenant="2016/02/29 12:34"

    (an,mois,jour,heure,minute,_,_,_,_)=time.strptime(resDateMaintenant,self.projet.lexique.dateheure())
    resDateMaintenantMins=time.mktime((an,mois,jour,heure,minute,-1,-1,-1,-1))

    # Gestion des quantification an, mois, jour, heure, seconde
    resDate=resAttendu #resDateMaintenant
    resComment=resAttendu #resDateMaintenant
    if amjhs!='':
      delta=0
      quant=''
      if lst_resAttendu[2]=="AN" or lst_resAttendu[2]=="ANS":
        delta=int(resAttendu)*365*24*3600-1
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      elif lst_resAttendu[2]=="MOIS":
        delta=int(resAttendu)*30*24*3600-1
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      elif lst_resAttendu[2]=="JOUR" or lst_resAttendu[2]=="JOURS" :
        delta=int(resAttendu)*24*3600-1
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      elif lst_resAttendu[2]=="HEURE" or lst_resAttendu[2]=="HEURES" :
        delta=int(resAttendu)*3600-1
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      elif lst_resAttendu[2]=="MINUTE" or lst_resAttendu[2]=="MINUTES" :
        delta=int(resAttendu)*60-1
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      elif lst_resAttendu[2]=="SECONDE" or lst_resAttendu[2]=="SECONDES" :
        delta=int(resAttendu)
        quant=self.projet.lexique.entree("MOT_%s"%lst_resAttendu[2])
      resDate=time.strftime(self.projet.lexique.dateheure(),time.localtime(resDateMaintenantMins-delta))
      resComment="%s %s"%(self.projet.lexique.entree("LOCUT_%s"%fonction)%lst_resAttendu[1],quant)
      resAttendu=resDate

    if fonction in ["EGALE_A","EGALE"]:
      if resAttendu=="MAINTENANT": # La fonction est EGALE_A obligatoirement !
        quant="'%s'"%self.projet.lexique.entree("MOT_MAINTENANT")
        resComment=self.projet.lexique.entree("LOCUT_%s"%lst_resAttendu[0])%quant
        resAttendu=self.projet.lexique.entree('LOCUT_MAINTENANT')
    else:
      if fonction=="MOINS_DE":
        resAttendu=resComment
      elif fonction=="PLUS_DE":
        resAttendu=resComment

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                               message=self.projet.lexique.entree('LOCUT_VERIFIER_DATE'),
                                                               objet=paramFichierSansIndex,
                                                               actions=[self.projet.lexique.entree('MOT_CONTROLE')+' : '+resAttendu])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if os.access(paramFichierSansIndex,os.F_OK):
        statinfo=os.stat(paramFichierSansIndex)
        resRetour=time.strftime(self.projet.lexique.dateheure(),time.localtime(statinfo[stat.ST_MTIME]))
      else:
        resRetour=time.strftime(self.projet.lexique.dateheure(),time.localtime(0))
      if fonction in ["EGALE_A","EGALE"]:
        if lst_resAttendu[1]=="MAINTENANT":
          if resRetour==resDateMaintenant:
            resBilan=self.STATUS_OK
          resAttendu+="("+resDateMaintenant+")"
        elif resRetour==resDate:
           resBilan=self.STATUS_OK
      elif fonction=="MOINS_DE":
        if time.mktime(time.strptime(resDate,self.projet.lexique.dateheure()))<time.mktime(time.strptime(resRetour,self.projet.lexique.dateheure())):
          resBilan=self.STATUS_OK
        resAttendu=self.projet.lexique.entree('LOCUT_APRES_LE')+resDate # Ne peut se faire qu'en R ou M
      elif fonction=="PLUS_DE":
        if time.mktime(time.strptime(resDate,self.projet.lexique.dateheure()))>time.mktime(time.strptime(resRetour,self.projet.lexique.dateheure())):
          resBilan=self.STATUS_OK
        resAttendu=self.projet.lexique.entree('LOCUT_AVANT_LE')+resDate # Ne peut se faire qu'en R ou M

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_DATE_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%resComment,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_TailleFichier
  # DESCRIPTION : Contrôle la taille d'un fichier.
  # PARAMETRES  : <PARAMETRE>{fichier}</PARAMETRE>
  # RETOUR      : {valeur}
  # TEXTE       : Controler que la taille du fichier est de {valeur} o.
  # IHM       : Controler que la taille du fichier est de {valeur} o.
  #-------------------------------------------------------------------------------
  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_TailleFichier
  # DESCRIPTION : Contrôle la taille d'un fichier.
  # PARAMETRES  : <PARAMETRE>{fichier}</PARAMETRE>
  # RETOUR      : {operation}|{valeur}|{unite}
  # TEXTE       : Controler que la taille du fichier est {operation} {valeur} {unite}.
  # IHM         : Controler que la taille du fichier est {operation} {valeur} {unite}.
  #-------------------------------------------------------------------------------
  def VALID_TailleFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_TailleFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_TailleFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,lst_resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controlme=self.Initialisations(pContinuer,pCommande,controle,pListeRetour=True,pExtensionResAttendu="o")
    fonction=lst_resAttendu[0]
    resAttendu=lst_resAttendu[1]
    unite=lst_resAttendu[2]
    if   unite=="Ko": resAttendu=str(int(resAttendu)*1024)
    elif unite=="Mo": resAttendu=str(int(resAttendu)*1024*1024)
    elif unite!="o":  resAttendu="unité "+unite+" inconnue."
    resMessage=''

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_TAILLE_FICHIER'),
                                                                       objet=paramFichierSansIndex+" "+resAttendu+"o",
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if not os.path.exists(paramFichierSansIndex):
        if controle.CTL_VALIDER: resBilan=self.STATUS_AV
        else: resBilan=self.STATUS_PB
        resRetour,resMessage=ERR_ACCES_SOURCE
        resMessage=self.projet.lexique.entree(resMessage)
      else:
        statinfo=os.stat(paramFichierSansIndex)
        resRetour=str(statinfo[stat.ST_SIZE])

    # Analyse du code de retour
    resAttendu,resRetour,resBilan=self.AnalyseCondition(controle,fonction,resAttendu,resRetour,resBilan,resMessage)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_TAILLE_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],                 'VAL_CONSTAT':[str(resRetour)+resMessage],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_FichierImage(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_FichierImage",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_FichierImage',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier à relever
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    comment=''
    style="norm"

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (_,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                       message=self.projet.lexique.entree('LOCUT_RELEVER_FICHIER')%'',
                                                       objet=paramFichierSansIndex)

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if not os.access(paramFichierSansIndex, os.F_OK):
        comment="LOCUT_FICHIER_INEXISTANT"
        style="tt"
      else:
        comment="LOCUT_SUIVRE_LIEN"
        resRetour="Img_"+pCommande['PARAMETRE']
        resBilan=self.STATUS_OK

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

#      # Reajustement du bilan en fonction de l'ordre de validation
#      if resBilan==self.STATUS_PB and controle.CTL_VALIDER:
#        resBilan=self.STATUS_AV

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(102,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RELEVER_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION':self.projet.lexique.entree('LOCUT_AFFICHAGE_CONTENU'),               #CLF_BILAN
        'VAL_ACTION':[(comment,resRetour,paramFichier)],'STY_ACTION':style,                             'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_FichierTexte(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_FichierTexte",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_FichierTexte',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier à relever
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)
    droit=os.access(paramFichierSansIndex,os.F_OK)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    comment=''
    style="norm"

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,None,paramFichierSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramFichierSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramFichierSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (_,resBilan,_)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                       message=self.projet.lexique.entree('LOCUT_RELEVER_FICHIER')%'',
                                                       objet=paramFichierSansIndex)

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if not os.access(paramFichierSansIndex,os.F_OK):
        comment="LOCUT_FICHIER_INEXISTANT"
        style="tt"
      else:
        comment='LOCUT_CLIQUER_DEPLIER'
        resRetour="Txt_"+tempCible
        resBilan=self.STATUS_OK

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

#      # Reajustement du bilan en fonction de l'ordre de validation
#      if resBilan==self.STATUS_PB and controle.CTL_VALIDER:
#        resBilan=self.STATUS_AV

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(112,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RELEVER_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramFichier],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION':self.projet.lexique.entree('LOCUT_AFFICHAGE_CONTENU'),               #CLF_BILAN
        'VAL_ACTION':[(comment,resRetour,paramFichier)],'STY_ACTION':style,                             'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_VariableEnvironnement
  # DESCRIPTION : Vérifier la valeur d'une variable d'environnement.
  # PARAMETRES  : {1:variable}
  # RETOUR      : {2:valeur}
  # TEXTE       : Vérifier que la variable d'environnement %s vaut %s.
  # IHM         : Vérifier que la variable d'environnement {1:variable} vaut {2:valeur}.
  #-------------------------------------------------------------------------------
  def VALID_VariableEnvironnement(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_VariableEnvironnement",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_VariableEnvironnement',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- nom de variable
    paramVarEnv=controle.CTL_PARAMETRES[0]

    if controle.CTL_LIBELLE is None:
      controle.CTL_LIBELLE="Rechercher la variable d'environnement"

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_RECHERCHER_VARENV')%'',
                                                                       objet=paramVarEnv,
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      resRetour=os.getenv(paramVarEnv,"$£IMPOSSIBLE£$")
      # Analyse du code de retour
      if resRetour=="$£IMPOSSIBLE£$":
        resBilan=self.STATUS_PB
        resRetour=self.projet.lexique.entree('LOCUT_VARIABLE_INEXISTANTE')
      else:
        if controle.CTL_RETOUR:
          if resRetour!=resAttendu:
            resBilan=self.STATUS_PB

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

#      # Reajustement du bilan en fonction de l'ordre de validation
#      if resBilan==self.STATUS_PB and controle.CTL_VALIDER:
#        resBilan=self.STATUS_AV

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(102,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RECHERCHER_VARENV'),'SUB_COMMENT':[paramVarEnv],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION':self.projet.lexique.entree('LOCUT_CONTROLER_VALEUR'),                                         #CLF_BILAN
        'VAL_ACTION':[('LOCUT_VALEUR_ATTENDUE',resAttendu,None),('MOT_CONSTAT',resRetour,None)],'STY_ACTION':"tt",           'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_MethodeIAD(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_MethodeIAD",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_MethodeIAD',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Message
    paramMessage=controle.CTL_PARAMETRES[0]
    # 1- Fichier à relever
    paramFichier=controle.CTL_PARAMETRES[1]

    # Retrait d'index
    paramMessageSansIndex=nngsrc.services.SRV_retraitIndexation(paramMessage)
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
    resRetour="NT"
    resSortie=['','','']

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=paramMessageSansIndex)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r",parallel=self.parallel)
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,paramMessageSansIndex,style="r",espacement=10,parallel=self.parallel)
      # Traitement du fichier
      if not os.path.exists(paramFichierSansIndex):
        resSortie=self.projet.lexique.entree('LOCUT_SANS_COMMENTAIRE')
      else:
        f=open(paramFichierSansIndex,'r',encoding='utf-8')
        contenu=f.readlines()
        f.close()
        resRetour=contenu[0].replace('\n','').replace('\r','')
        if resRetour!='NT':
          resSortie=[l.replace('\n','') for l in contenu[1:]]

      resBilan=nngsrc.services.SRV_resultatCode(resRetour,self.constantes)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(102,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':paramMessage,
        #------------------------------------------
        'CLF_ACTION':self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),                        #CLF_BILAN
        'VAL_ACTION':resSortie,'STY_ACTION':"ittt",                                             'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  #-------------------------------------------------------------------------------
  def VALID_MessageAction(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_MessageAction",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_MessageAction',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Message
    paramMessage=controle.CTL_PARAMETRES[0]
    # 1- Attente (optionnel)
    paramWait=None
    if len(controle.CTL_PARAMETRES)>1:
      paramWait=controle.CTL_PARAMETRES[1]

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle)
#    resRetour=''
    resMessage=None

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      resBilan=self.STATUS_OK
      if paramWait:
        if paramWait=="ENTREE":
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||Or")
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree('LOCUT_ACTION_OPERATEUR'),style="||Or")
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug," %s"%paramMessage,style="||Or")
          nngsrc.servicesSons.SRVson_Prononcer(paramMessage,parallel=self.parallel)
          if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
          nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PUIS_APPUYER_ENTREE"),"CLAV_ENTREE")
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||Or")
        else:
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'%s'%paramMessage,style="||O",espacement=10)
      else:
        nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,nngsrc.services.SRV_retraitIndexation(paramMessage),style="||O")

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      if paramWait:
        if paramWait=="ENTREE":
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r",parallel=self.parallel)
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or",parallel=self.parallel)
          #nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ACTION_OPERATEUR'),style="Or",parallel=self.parallel)
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None," %s %s"%(paramMessage,self.projet.lexique.entree('LOCUT_APPUYER_ENTREE')),style="Or",parallel=self.parallel)
          nngsrc.servicesSons.SRVson_Prononcer(paramMessage,parallel=self.parallel)
          if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
          nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,">","CLAV_ENTREE")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or",parallel=self.parallel)
        else:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r",parallel=self.parallel)
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(paramMessage),style="r",espacement=10,parallel=self.parallel)
#          print "DBG>", self.parallel,paramMessage #TODO A supprimer !
          nngsrc.servicesSons.SRVson_Prononcer(paramMessage,parallel=self.parallel)
          try:
            time.sleep(int(paramWait))
          except Exception:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"%s n'est pas une valeur numérique pour le timer."%paramWait,style="Wr",espacement=5)
      else:
        if self.rapport.projet.RAPPORT_EN_LIGNE=="non":
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r",parallel=self.parallel)
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(paramMessage),style="r",espacement=10,parallel=self.parallel)

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,True,self.STATUS_OK,resBilan,resSortie,paramSortieSiVide)
      # Reajustement du bilan en fonction de l'ordre de validation
      if controle.CTL_VALIDER:
        resBilan=self.STATUS_AV

    if executionCMRY=='S':
      resMessage=paramMessage

    # Envoi des paramètres du rapport
    if resAttendu=="AR":
      resBilan=self.STATUS_AR
      params=controle.ParametresBaseMode(102,resBilan)
      params.update({
          #------------------------------------------
          'CLF_COMMENT':'MOT_ACTION',
          'VAL_COMMENT':paramMessage,
          #------------------------------------------
          'CLF_ACTION':self.projet.lexique.entree('LOCUT_RESERVES'),                        #CLF_BILAN
          'VAL_ACTION':'','STY_ACTION':"ittt",                                             'VAL_BILAN':resBilan})
#          'RES_COMMENT':resMessage,           'VAL_BILAN':resBilan})
          #------------------------------------------
    else:
      params=controle.ParametresBaseMode(0,resBilan)
      params.update({
          #------------------------------------------
          'CLF_COMMENT':'MOT_ACTION',
          'VAL_COMMENT':paramMessage,
          'RES_COMMENT':resMessage}
           )
          #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_EtapeManuelle
  # DESCRIPTION : Affiche un message pour l'action à mener.
  # PARAMETRES  : {1:message}
  # RETOUR      : None
  # TEXTE       : Effectuer l'action %s. Rendre compte.
  # IHM         : Effectuer l'action {1:message}.
  #-------------------------------------------------------------------------------
  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_EtapeManuelle
  # DESCRIPTION : Affiche un message pour l'action à mener et attendre un delai précisé.
  # PARAMETRES  : {1:message}|{2:attente}
  # RETOUR      : None
  # TEXTE       : Effectuer l'action %s. Attendre %s s. Rendre compte.
  # IHM         : Effectuer l'action {1:message}. Attendre {2:attente} s.
  #-------------------------------------------------------------------------------
  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_EtapeManuelle
  # DESCRIPTION : Affiche un message pour l'action à mener et appuyer sur 'ENTREE'.
  # PARAMETRES  : {1:message}|ENTREE
  # RETOUR      : None
  # TEXTE       : Effectuer l'action %s. Appuyer sur 'ENTREE'. Rendre compte.
  # IHM         : Effectuer l'action {1:message}. Appuyer sur 'ENTREE'.
  #-------------------------------------------------------------------------------
  def VALID_EtapeManuelle(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_EtapeManuelle",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_EtapeManuelle',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Message
    message=controle.CTL_PARAMETRES[0]
    # 1- Attente (optionnel)
    paramWait=None
    if len(controle.CTL_PARAMETRES)>1:
      paramWait=controle.CTL_PARAMETRES[1]

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      if paramWait:
        if paramWait=="ENTREE":
          nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".","-")
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,message,style="||O",espacement=10)
          nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PUIS_APPUYER_ENTREE"),"CLAV_ENTREE")
          (_,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex)
        else:
          nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".","-")
          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,message,style="||O",espacement=10)
          try:
            time.sleep(int(paramWait))
          except Exception:
            print("%s n'est pas une valeur numérique pour le timer.\n"%paramWait)
          (_,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex)
      else:
        (_,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,message)
      disp=nngsrc.services.SRV_codeResultat(resBilan,self.constantes,self.projet.DOCUMENT_LANGUE)
      resRetour=disp[2]

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      if paramWait:
        if paramWait=="ENTREE":
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ACTION_OPERATEUR'),style="Or")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,message,style="Or")
          nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
          if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
          nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PUIS_APPUYER_ENTREE"),"CLAV_ENTREE")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or")
        else:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(message),style="r",espacement=10)
          nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
          try:
            time.sleep(int(paramWait))
          except Exception:
            print("%s n'est pas une valeur numérique pour le timer.\n"%paramWait)
      else:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(message),style="r",espacement=10)

      # Analyse du code de retour
      (_,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,None,None)
      disp=nngsrc.services.SRV_codeResultat(resBilan,self.constantes,self.projet.DOCUMENT_LANGUE)
      resRetour=disp[2]
      # Reajustement du bilan en fonction de l'ordre de validation
      if controle.CTL_VALIDER:
        resBilan=self.STATUS_AV
      # Reajustement de la sortie en fonction du bilan
      # - sans objet-

#    if executionCMRY=='R':
#    # Traitement
#    if pContinuer:
#      resBilan=self.STATUS_OK
#      executionCMRY=ExecutionMode(self.projet.execution,self.projet.rapportManuel)
#      if executionCMRY=='R':
#        if paramWait:
#          if paramWait=="ENTREE":
#            if executionCMRY=='M':
#              nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="Or")
#              nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree('LOCUT_ACTION_OPERATEUR'),style="Or")
#              nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug," %s"%message,style="Or")
#              nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
#              if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
#              nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PUIS_APPUYER_ENTREE"),"CLAV_ENTREE")
#              nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="Or")
#            else:
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or")
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_ACTION_OPERATEUR'),style="Or")
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,message,style="Or")
#              nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
#              if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
#              nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PUIS_APPUYER_ENTREE"),"CLAV_ENTREE")
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or")
#          else:
#            if executionCMRY=='M':
#              nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'%s'%message,style="||O",espacement=10)
#            else:
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
#              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(message),style="r",espacement=10)
#            nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
#            try:
#              time.sleep(int(paramWait))
#            except Exception:
#              print("%s n'est pas une valeur numérique pour le timer.\n"%paramWait)
#        else:
#          if executionCMRY=='M':
#            nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,nngsrc.services.SRV_retraitIndexation(message),style="||O")
#          elif self.projet.enregistrement!=0:                            #v8.2rc2 Enregistrement à la volée
#            nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,nngsrc.services.SRV_retraitIndexation(message),style="||O")
#          else:
#            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
#            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,nngsrc.services.SRV_retraitIndexation(message),style="r",espacement=10)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':message,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_CONSTAT_ATTENDU'],'VAL_ATTENDU':["OK"],                    'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)]

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_SaisieOperateur
  # DESCRIPTION : Affiche un message et enregistre la chaîne saisie dans la variable 'OPERATEUR'.
  # PARAMETRES  : {1:message}
  # RETOUR      : None
  # TEXTE       : Afficher le message %s. Enregistrer la saisie.
  # IHM         : Afficher le message {1:message}. Enregistrer la saisie.
  #-------------------------------------------------------------------------------
  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_SaisieOperateur
  # DESCRIPTION : Affiche un message et enregistre la chaîne saisie dans la variable precisée.
  # PARAMETRES  : {1:message}|{2:variable}
  # RETOUR      : None
  # TEXTE       : Afficher le message %s. Enregistrer la saisie dans la variable %s.
  # IHM         : Afficher le message {1:message}. Enregistrer la saisie dans la variable {2:variable}.
  #-------------------------------------------------------------------------------
  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Affichage
  # FONCTION    : VALID_SaisieOperateur
  # DESCRIPTION : Affiche un message et enregistre la chaîne saisie dans la variable precisée et la compare à la valeur de retour
  # PARAMETRES  : {1:message}|{2:variable}
  # RETOUR      : {3:valeur}
  # TEXTE       : Afficher le message %s. Enregistrer la saisie dans la variable %s. La comparer à %s.
  # IHM         : Afficher le message {1:message}. Enregistrer la saisie dans la variable {2:variable}. La comparer à {3:valeur}.
  #-------------------------------------------------------------------------------
  def VALID_SaisieOperateur(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_SaisieOperateur",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_SaisieOperateur',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Message
    message=controle.CTL_PARAMETRES[0]

    if controle.CTL_VARIABLE is None:
      controle.CTL_VARIABLE="OPERATEUR"
      controle.CTL_COMMENT+=self.projet.lexique.entree('Variable_COMMENT')%controle.CTL_VARIABLE

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)
#    if controle.CTL_RETOUR is None:
    resRetour="'*n*'"

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=message,
                                                                       saisie=["=> ","SAISIE"])
#      resRetour=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"=> ","SAISIE")
#          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||O")
#          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"SAISIE OPERATEUR :",style="||O")
#          nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug," %s"%message,style="||O")
#          nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
 #         if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
 #         resRetour=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"=> ","SAISIE")
 #         nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"------------------------------------------",style="||O")

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style="r")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Or")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"SAISIE OPERATEUR :",style="Or")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None," %s"%message,style="Or")
      nngsrc.servicesSons.SRVson_Prononcer(message,parallel=self.parallel)
      if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU,parallel=self.parallel)
      resRetour=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"=> ","SAISIE")
      print("------------------------------------------")

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,controle.CTL_RETOUR and resRetour!=resAttendu,self.STATUS_PB,resBilan,resSortie,paramSortieSiVide)

#      # Analyse du code de retour
#      if controle.CTL_RETOUR:
#        if resRetour!=resAttendu:
#          resBilan=self.STATUS_PB
#      # Reajustement du bilan en fonction de l'ordre de validation
#      if resBilan==self.STATUS_PB and controle.CTL_VALIDER:
#        resBilan=self.STATUS_AV

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':message,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_CONSTAT_ATTENDU'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
#    params.update({'DOC_MESSAGE':message,
#                   'resRetour':resRetour,
#                   'resAttendu':resAttendu,
#                   'CTL_VALIDER':CTL_VALIDER})
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_RecupereVarEnv(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RecupereVarEnv",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RecupereVarEnv',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- nom de variable
    paramVarEnv=controle.CTL_PARAMETRES[0]

    # TODO libelle
    if controle.CTL_LIBELLE is None:
      controle.CTL_LIBELLE="Recuperer la variable d'environnement"

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_RECUPERER_VARENV')%'',
                                                                       objet=paramVarEnv,
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      resRetour=os.getenv(paramVarEnv,"$£IMPOSSIBLE£$")
      if resRetour=="$£IMPOSSIBLE£$":
        resBilan=self.STATUS_PB
        resRetour=self.projet.lexique.entree('LOCUT_VARIABLE_INEXISTANTE')

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RECUPERER_VARENV'),'SUB_COMMENT':[paramVarEnv],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_RecupereNbFicRepert(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RecupereNbFicRepert",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RecupereNbFicRepert',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Repertoire
    paramRepertoire=controle.CTL_PARAMETRES[0]
    # 1- Flag
    paramFlagRF=controle.CTL_PARAMETRES[1]

    # Retrait d'index
    paramRepertoireSansIndex=nngsrc.services.SRV_retraitIndexation(paramRepertoire)

    # TODO libelle
    if controle.CTL_LIBELLE is None:
     controle. CTL_LIBELLE="Recuperer le nombre de fichiers ou de répertoires"

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_RECUPERER_NOMBRE_%s'%paramFlagRF)%'',
                                                                       objet=paramRepertoireSansIndex,
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      if os.access(paramRepertoireSansIndex,os.F_OK):
        if paramFlagRF=="FICHIERS":
          resRetour=len([name for name in os.listdir(paramRepertoireSansIndex) if os.path.isfile(os.path.join(paramRepertoireSansIndex,name))])
        elif paramFlagRF=="REPERTOIRES":
          resRetour=len([name for name in os.listdir(paramRepertoireSansIndex) if os.path.isdir(os.path.join(paramRepertoireSansIndex,name))])
      else:
        resRetour=0
        resBilan=self.STATUS_PB

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,controle.CTL_RETOUR and resRetour!=int(resAttendu),self.STATUS_PB,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RECUPERER_NOMBRE_%s'%paramFlagRF),'SUB_COMMENT':[paramRepertoire],'STY_COMMENT':"tt",
        'RES_COMMENT':controle.CTL_RESCOMMENT%self.projet.lexique.entree("MOT_%s"%paramFlagRF),
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_RecupereNbLignesFic(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_RecupereNbLignesFic",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_RecupereNbLignesFic',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Repertoire
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)
    droit=os.access(paramFichierSansIndex,os.F_OK)

    # TODO CTL_LIBELLE
    if controle.CTL_LIBELLE is None:
      controle.CTL_LIBELLE="Recuperer le nombre de lignes du fichier"

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    # Traitement des restrictions (intervalle, lignes) et remplacements de texte
    calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces,extensionsExclues,listeComplement=restrictions(pCommande,executionCMRY,None,paramFichierSansIndex,self.projet)
    # Réduction
    if not droit:
      tempCible=paramFichierSansIndex
    else:
      tempCible="tempCible.txt"
      reduitFichier(paramFichierSansIndex,tempCible,(calculDebut,calculFin,numLignesExclues,textesExclus,textesRemplaces))

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_RECUPERER_LIGNES_FICHIER')%'',
                                                                       objet=paramFichierSansIndex,
                                                                       saisie=["=> ","SAISIE"])
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      if os.access(tempCible,os.F_OK):
        f=open(tempCible,'r',encoding='utf-8')
        contenu=f.readlines()
        f.close()
        resRetour=len(contenu)
      else:
        resRetour=0
        resBilan=self.STATUS_PB

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,controle.CTL_RETOUR and resRetour!=int(resAttendu),self.STATUS_PB,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(113,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_RECUPERER_LIGNES_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",
        #------------------------------------------
        'CLF_COMPLEMENT':'MOT_RESTRICTION',
        'VAL_COMPLEMENT':self.projet.lexique.entree('LOCUT_EXAMEN_FICHIER_PRODUIT'),'SUB_COMPLEMENT':[paramFichier],'STY_COMPLEMENT':"tt",'LNK_COMPLEMENT':True,
        'LST_COMPLEMENT':listeComplement,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    if self.projet.debug>2: printParams(params)
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resRetour,controle.CTL_VARIABLE)

  #-------------------------------------------------------------------------------
  def VALID_Timer(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_Timer",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_Timer',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- interrupteur
    paramOnOff=controle.CTL_PARAMETRES[0]

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      executionCMRY='R'

    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      resRetour="-"
      if paramOnOff=="ON":
        resRetour=str(time.time())
      else:
        for (v,d,_) in pVarOperateur:
          if v=='[TIMER]':
            resRetour=str(time.time()-float(d))

      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,controle.CTL_RETOUR and resRetour!=int(resAttendu),self.STATUS_PB,resBilan,resSortie,paramSortieSiVide)

    message='Timer %s'%paramOnOff
    # Envoi des paramètres du rapport
    if paramOnOff=="ON":
      params=controle.ParametresBaseMode(1,resBilan)
      params.update({
          #------------------------------------------
          'CLF_COMMENT':'MOT_ACTION',
          'VAL_COMMENT':message,
          'RES_COMMENT':controle.CTL_RESCOMMENT%paramOnOff.lower()})
          #------------------------------------------
    else:
      params=controle.ParametresBaseMode(103,resBilan)
      params.update({
          #------------------------------------------
          'CLF_COMMENT':'MOT_ACTION',
          'VAL_COMMENT':message,
          'RES_COMMENT':controle.CTL_RESCOMMENT%paramOnOff.lower(),
          #------------------------------------------
          'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
          'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],               'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
          #------------------------------------------
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],resRetour

  ##-------------------------------------------------------------------------------
  # Integration IHM
  # CATEGORIE   : Fichier
  # FONCTION    : VALID_LitFichier
  # DESCRIPTION : Lire le contenu d'un fichier et l'enregistre dans la variable 'OPERATEUR' si la variable d'accueil n'est pas précisée.
  # PARAMETRES  : {fichier}[|{variable}]
  # RETOUR      : {erreur}
  # TEXTE       : Lire le contenu du fichier {fichier}.[ L'enregistrer dans la variable {variable}.]
  # IHM         : Lire le contenu du fichier {fichier}.[ L'enregistrer dans la variable {variable}.]
  #-------------------------------------------------------------------------------
  def VALID_LitFichier(self,pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ):
    if self.projet.debug>2: printFonction("VALID_LitFichier",pFiche,pContinuer,pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ)

    # Analyse de la commande
    controle=Controle('VALID_LitFichier',pCommande,pVarOperateur,pIndex,pIndent,pCondition,pFTTQ,self.projet)

    # Traitement des paramètres
    # 0- Fichier
    paramFichier=controle.CTL_PARAMETRES[0]

    # Retrait d'index
    paramFichierSansIndex=nngsrc.services.SRV_retraitIndexation(paramFichier)

    # Initialisations
    executionCMRY,resAttendu,resSortie,resRetour,resBilan,paramSortieSiVide,controle=self.Initialisations(pContinuer,pCommande,controle,pResAttenduDefaut=None)
    resultat="'*n*'"

    #-----------------------
    # Traitement du mode M
    if executionCMRY=='M':
      (resRetour,resBilan,resSortie)=self.SaisieResultatActionManuelle(controle.CTL_RAPPORT,pFiche,pIndex,
                                                                       message=self.projet.lexique.entree('LOCUT_LIRE_FICHIER')%'',
                                                                       objet=paramFichierSansIndex,
                                                                       attendu=resAttendu)
    #-----------------------
    # Traitement du mode R
    if executionCMRY=='R':
      resBilan=self.STATUS_OK
      resRetour="0"
      try:
        resultat=nngsrc.services.SRV_LireContenu(paramFichierSansIndex,"LOCUT_LECTURE_INCORRECTE")
        if resultat=="LOCUT_LECTURE_INCORRECTE":
          resRetour='-1'
          resBilan=self.STATUS_PB
          resultat=self.projet.lexique.entree('LOCUT_LECTURE_INCORRECTE')
      except Exception:
        resBilan=self.STATUS_PB
        resRetour='-1'

      # Analyse du code de retour
      if controle.CTL_RETOUR:
        if resRetour!=controle.CTL_RETOUR:
          resBilan=self.STATUS_PB
        else:
          resBilan=self.STATUS_OK
      # Analyse et ajustements
      resBilan,resSortie=self.AnalyseResultat(controle,False,None,resBilan,resSortie,paramSortieSiVide)

    # Envoi des paramètres du rapport
    params=controle.ParametresBaseMode(103,resBilan)
    params.update({
        #------------------------------------------
        'CLF_COMMENT':'MOT_ACTION',
        'VAL_COMMENT':self.projet.lexique.entree('LOCUT_LIRE_FICHIER'),'SUB_COMMENT':[paramFichier],'STY_COMMENT':"tt",'LNK_COMMENT':True,
        #------------------------------------------
        'CLF_ACTION' :self.projet.lexique.entree('LOCUT_RESULTAT_ACTION'),               'CLF_CONSTAT':'MOT_CONSTAT',                      #CLF_BILAN
        'CLF_ATTENDU':['LOCUT_VALEUR_ATTENDUE'],'VAL_ATTENDU':[resAttendu],              'VAL_CONSTAT':[resRetour],'STY_CONSTAT':"tt",    'VAL_BILAN':resBilan})
        #------------------------------------------
    return [self.RapportBilanMode(controle.CTL_ATTENTE,resBilan,pFiche,params,controle.CTL_RAPPORT)],(resultat,controle.CTL_VARIABLE)

#-------------------------------------------------------------------------------
# FIN
