#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import zipfile, sys, os, glob, time, locale, datetime, shutil
from copy import deepcopy
import nngsrc.tables
import nngsrc.graphes
import nngsrc.services
import nngsrc.servicesSons
import nngsrc.servicesXML
import nngsrc.smtp
import nngsrc.projet
import nngsrc.fichesXML
import nngsrc.qualite
import nngsrc.fonctions
import nngsrc.rapport
import nngsrc.campagne

try:
  import mpJobs
  PARALLELISATION=True
except:
#  nngsrc.services.SRV_Print(FRED+SNORMAL+"RESTRICTION (nangu-core) : Module 'pp' non installe.")
#  nngsrc.services.SRV_Print(FRED+SNORMAL+"              => Nangu ne pourra pas paralleliser les executions de fiches.")
  PARALLELISATION=False
simulationPP=False

#try:
#  import jaro
#except:
#  nngsrc.services.SRV_Print(FRED+SNORMAL+"RESTRICTION (nangu-core) : Module 'jaro' non installe.")
#  nngsrc.services.SRV_Print(FRED+SNORMAL+"              => Nangu ne pourra pas convertir des fichiers textes en fiches.")

ESPACEMENT=120
BILAN_NB_LIGNES=51
#-----------------------------------------------------------------------------------
def sorted_ls(repert,feuilleRoute):
  mtime = lambda f: os.stat(os.path.join(repert, f)).st_mtime
  listeRapports=list(sorted(os.listdir(repert),key=mtime))
  rapportFeuille=[]

  for rapport in listeRapports:
    # lecture du fichier bilan de couverture
    bilan=os.path.join(repert,rapport,"%s.csv"%rapport)
    billignes=nngsrc.services.SRV_LireContenuListe(bilan)

    # Recuperation du cahier d'origine
    # Identifié dans le bilan csv par la derniere ligne particuliere (cf.BILAN_NB_LIGNES):
    # Bilan etabli a partir du cahier : xxxxx
    cahier=None
    if len(billignes)==BILAN_NB_LIGNES:
      cahier=billignes[BILAN_NB_LIGNES-1].split(' : ')[1]
    if cahier in feuilleRoute:
      rapportFeuille.append(rapport)
  #print "DBG>",rapportFeuille
  return rapportFeuille

#---------------------------------------------------------------------------------
def chrono(condition,quant):
  cond= ''
  t=0
  while cond!=condition:
    t=datetime.datetime.today()
    cond=t.strftime(quant)
  return t

#-----------------------------------------------------------------------------------
# Fonction de traitement d'un fichier de listes
#-----------------------------------------------------------------------------------
def locTraiteFichierListe(input_dir,elem,ficherListe):
  lotFiches=[]
  f=open(os.path.join(input_dir,elem,ficherListe),'r',encoding='utf-8')
  fiche="vide"
  while fiche!= '':
    fiche=f.readline()
    fiche=fiche.replace('\n','').replace('\r','')
    if fiche!='':
      if fiche[0]!=";":
        lotFiches.append(os.path.join(elem,fiche))
  f.close()
  return lotFiches

#---------------------------------------------------------------------------------
def chargeProjet(projet):
  if not os.path.exists(projet): return None
  lignes=nngsrc.services.SRV_LireContenuListe(projet)
  version=lignes[0].split("=")[1]
  if version=="21.05.23":
    return chargeProjet_21_05_23(lignes)
  else:
    print("Cette version %s de fichier .prj est inconnue ou n'est plus supportée."%version)
    return None

#---------------------------------------------------------------------------------
def chargeProjet_21_05_23(lignes):
  NANGUPRJ=os.getenv("NANGUPRJ",'')
  dicoEnv={}
  numligne=0
  version=lignes[numligne].split("=")[1]
  numligne+=1
  typePRJsimple=lignes[numligne].split("=")[1]=="simple"
  numligne+=1
  repConfiguration=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repFiches=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repRapports=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repScenarios=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repPlans=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repCampagnes=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repAutres=lignes[numligne].split("=")[1].replace("[NANGUPRJ]",NANGUPRJ)
  numligne+=1
  repCourant=lignes[numligne].split("=")[1]
  numligne+=1
  configurationCourante=lignes[numligne].split("=")[1]
  numligne+=1
  typeDocument=lignes[numligne].split("=")[1]
  numligne+=1
  listeCourante=lignes[numligne].split("=")[1]
  numligne+=1
  planCourant=lignes[numligne].split("=")[1]
  numligne+=1
  menuRepertoires=lignes[numligne].split("=")[1]
  numligne+=1
  menuListes=lignes[numligne].split("=")[1]
  numligne+=1
  ouverts=lignes[numligne].split("=")[1]
  numligne+=1
  resolus=lignes[numligne].split("=")[1]
  numligne+=1
  lstRapports=lignes[numligne].split("=")[1]
  numligne+=1
  controleFiches=lignes[numligne].split("=")[1]
  numligne+=2

  referenceSubst=None

  stop=False
  while not stop:
    try:
      entree=lignes[numligne].split("=",1)
      dicoEnv[entree[0]]=entree[1].replace("[NANGUPRJ]",NANGUPRJ)
      numligne+=1
    except:
      stop=True
  for varenv in dicoEnv:
    valeur=os.getenv(varenv,None)
    if valeur is not None:
      dicoEnv[varenv]=valeur
  return version, typePRJsimple, (repConfiguration, configurationCourante), (repCampagnes, repPlans, repScenarios, repFiches, repRapports, repAutres, repCourant), (lstRapports, controleFiches, None, referenceSubst), (typeDocument, listeCourante, planCourant, menuRepertoires, menuListes, ouverts, resolus), dicoEnv

#-----------------------------------------------------------------------------------
def ecrireProjet_MAJ(fichierPRJ,projetPRJ):
  _, typePRJsimple, (repConfiguration, configurationCourante), (repCampagnes, repPlans, repScenarios, repFiches, repRapports, repAutres, repCourant), (lstRapports, controleFiches, radicalScenarios, referenceSubst), (typeDocument, listeCourante, planCourant, menuRepertoires, menuListes, ouverts, resolus), _, dicoEnv = projetPRJ

  NANGUPRJ=os.getenv("NANGUPRJ",'')
  f=open(fichierPRJ,"w",encoding='utf-8')
  f.write("version=%s\n"%VERSION_PRJ)
  if typePRJsimple:
    f.write("TypeProjet=simple\n")
  else:
    f.write("TypeProjet=complexe\n")
  f.write("Configuration=%s\n"%repConfiguration.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write("Fiches=%s\n"%repFiches.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write("Rapports=%s\n"%repRapports.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write("Scenarios=%s\n"%repScenarios.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write("Plans=%s\n"%repPlans.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write('Campagnes=%s\n'%repCampagnes.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write('Autres=%s\n'%repAutres.replace(NANGUPRJ,"[NANGUPRJ]"))
  f.write("repertoireCourant=\n")
  f.write("configurationCourante=%s\n"%configurationCourante)
  f.write("typeDocument=P\n")
  f.write("listeCourante=\n")
  f.write("planCourant=\n")
  f.write("menuRepertoires=N\n")
  f.write("menuListes=O\n")
  f.write("ouverts=-\n")
  f.write("resolus=-\n")
  f.write("lstRapports=listeRapports.cfg\n")
  f.write("controleFiches=controlerFiches.lst\n")
  f.write("-----Variables--------------------------\n")
  for k in dicoEnv.keys():
    f.write("%s=%s\n"%(k,dicoEnv[k].replace(NANGUPRJ,"[NANGUPRJ]")))
  f.close()

#-----------------------------------------------------------------------------------
def recolteRapports(cahiersCampagne,repRapport,projet):
  # Recolte des rapports executes
  csvRapports={}
  feuilleRoute=[]
  projet.execution='R'
  projet.GenereProduitReference()
 # print "cC>",cahiersCampagne
  for cahier in cahiersCampagne:
    if cahier["select"]=='X':
      feuilleRoute.append(cahier['cahier'])
  listetriee=sorted_ls(os.path.join(repRapport,projet.execution,"CSV"),feuilleRoute)
 # print "lt>",listetriee
  # Arrêter après version ?
  baseReference="%s%s%s"%(projet.SEPARATEUR_CHAMPS,projet.PRODUIT_VERSION.replace('.',''),projet.SEPARATEUR_CHAMPS)
  for elem in listetriee:
    rapport=elem
 #   print "elem>",elem,"%s-%s-"%(projet.PRODUIT_REFERENCE,projet.PRODUIT_VERSION.replace('.',''))
    if rapport.find(baseReference)!=-1:
      if elem.find('.zip')!=-1:
        ficzip=zipfile.ZipFile(os.path.join(repRapport,projet.execution,"CSV",elem),"r")
        elem=elem.replace('-CSV.zip','')
        ficzip.extractall(os.path.join(repRapport,projet.execution,"CSV",elem))
        ficzip.close()
      # On recherche les CSV xxxxxx-MTEvC.csv
      for MTE in os.listdir(os.path.join(repRapport,projet.execution,"CSV",elem)):
        if MTE.find("-MTEvC")!=-1:
          SsRefProduit=MTE.split('-')[-5]
          #SsRefProduit=SsRefProduit[SsRefProduit.rfind('-')+1:]
          if SsRefProduit in csvRapports:  #ici PRODUIT_SOUS_REFERENCE = ''
            csvRapports[SsRefProduit].append(os.path.join(elem,MTE.replace('.csv','')))
          else:
            csvRapports[SsRefProduit]=[os.path.join(elem,MTE.replace('.csv',''))]

  # Ne récupere dans csvRapports[''] que les dernières versions de rapports joués pour chaque sous-reference
  for k in csvRapports.keys():
    csvRapports[k]=sorted(csvRapports[k])
    csvRapports[k].reverse()
  csvRapports['']=[]
  for k in csvRapports.keys():
    if k!='':
      csvRapports[''].append(csvRapports[k][0])
      csvRapports[k].reverse()
  return csvRapports

#-----------------------------------------------------------------------------------
class NANGU:
  repRapport=None
  repTravail=None
  repFiches=None
  repScenarios=None
  lot=None
  projet=None
  fonctions=None
  itemIndex=None
  listeEnv=None
  environnement=None
  log=None
  dictionnaire=None
  configFile=None
  variables=[]
  notRep=[]
  #Variable globale informant si le serveur Sikuli est actif
  isStartSikuli=False
  deviceUInput=None

  #---------------------------------------------------------------------------------
  def __init__(self,execution,typePRJsimple,configxml,referenceSubst,repPlans,repScenarios,repFiches,repRapport,repWorkDir,dico=None):
    self.nangu_OK=False
    if dico is None: dico={}
    self.constantes={'sys_stdout_encoding':sys.stdout.encoding,'STATUS_PB':0,'STATUS_OK':1,'STATUS_AV':2,'STATUS_NT':3,'STATUS_AR':4} # Pour la parallelisation
    self.parallel=False
    self.typePRJsimple=typePRJsimple
    self.repPlans=repPlans
    self.repScenarios=repScenarios
    self.repFiches=repFiches
    self.repTravail=repWorkDir
    self.version,self.versPRJ,self.versCFG=nngsrc.services.VERSION()

    for elem in glob.glob(os.path.join(self.repTravail,'*.*')):
      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
    self.repRessources=os.path.join(nngsrc.services.varNANGU,"ressources")
    self.repRapport=repRapport
    #self.locale=locale.getdefaultlocale()[1]
    self.dictionnaire=dico
    self.configFile=configxml

    self.lot=[]
    self.loop=-1
    self.projet=nngsrc.projet.Projet(execution,self.configFile,(repPlans,repScenarios,repFiches,repRapport),self.typePRJsimple,os.path.join(self.repTravail,"notesManuelles.bbc"),self.versCFG)
    if not self.projet.projetOK: return
    if self.projet.debug==2:
      self.version="AAAA.MM"
      self.projet.COMPUTERNAME="Fandene"
    if self.projet.MANTIS is not None and not os.path.exists(os.path.join(self.repRapport,"Mantis")):
        os.makedirs(os.path.join(self.repRapport,"Mantis"))
    self.qualite=nngsrc.qualite.Qualite(self.repRapport,self.projet)

    if self.projet is None:
      nngsrc.services.SRV_Terminal(None,None,None,"Le projet n'a pu être chargé correctement !",style="Er")
      return
    if self.qualite is None:
      nngsrc.services.SRV_Terminal(None,None,None,"Les éléments de gestion de qualité n'ont pu être chargés correctement !",style="Er")
      return
    if referenceSubst is not None:
      (_,self.separateurSuffixe,self.separateurChamps,self.NbChamps)=referenceSubst
    else:
      self.separateurSuffixe=self.projet.SEPARATEUR_SUFFIXE

    self.razVariables()
    self.jeuxDeDonnees=[]
    self.rapportActions=0
    self.rang=0
    self.actionVolee=None
    self.ajustementsTAG=None

    if self.projet.debug>1:
      print("Les encodages :")
      print("   default locale  =",locale.getdefaultlocale())
      print("   default encoding=",sys.getdefaultencoding())
      print("   stdout encoding =",sys.stdout.encoding)
      print("Confirmer les FT : %s"%self.projet.CONFIRMATION_FT)

    self.fonctions=nngsrc.fonctions.Fonctions(self.projet,self.qualite,self.constantes)
#    self.fonctionsIHM=nngsrc.fonctionsIHM.FonctionsIHM(self.projet,self.constantes)
#    self.listeFT={}
#    self.listeExig={}
#    self.listeFct={}

#   les variables 'SAISIE' ne sont plus rapportées dans le LaTeX
    self.listeKeyEnv={'CTX':['VARIABLE'],'IN':['PATH_IN','DATA_IN','PARAM'],'OUT':['PATH_OUT','DATA_OUT'],'REF':['PATH_REF','DATA_REF']}
    self.listeEnv=dict({'SAISIE'    :[self.projet.lexique.entree("LOCUT_SAISIE")],
                        'PATH_IN'   :[self.projet.lexique.entree("LOCUT_PATH_IN")],
                        'PATH_REF'  :[self.projet.lexique.entree("LOCUT_PATH_REF")],
                        'PATH_OUT'  :[self.projet.lexique.entree("LOCUT_PATH_OUT")],
                        'DATA_IN'   :[self.projet.lexique.entree("LOCUT_DATA_IN")],
                        'DATA_REF'  :[self.projet.lexique.entree("LOCUT_DATA_REF")],
                        'DATA_OUT'  :[self.projet.lexique.entree("LOCUT_DATA_OUT")],
                        'PARAM'     :[self.projet.lexique.entree("MOT_PARAM")],
                        'VARIABLE'  :[self.projet.lexique.entree("LOCUT_VARIABLES_PROJET")],
                        'ND'        :[self.projet.lexique.entree("LOCUT_ND")]})
    self.environnement=dict({'SAISIE'  :[self.projet.lexique.entree("LOCUT_SAISIE")],
                             'PATH_IN' :[self.projet.lexique.entree("LOCUT_PATH_IN")],
                             'PATH_REF':[self.projet.lexique.entree("LOCUT_PATH_REF")],
                             'PATH_OUT':[self.projet.lexique.entree("LOCUT_PATH_OUT")],
                             'DATA_IN' :[self.projet.lexique.entree("LOCUT_DATA_IN")],
                             'DATA_REF':[self.projet.lexique.entree("LOCUT_DATA_REF")],
                             'DATA_OUT':[self.projet.lexique.entree("LOCUT_DATA_OUT")],
                             'PARAM'   :[self.projet.lexique.entree("MOT_PARAM")],
                             'VARIABLE':[self.projet.lexique.entree("LOCUT_VARIABLES_PROJET")],
                             'ND'      :[self.projet.lexique.entree("LOCUT_ND")]})
    self.dico={}
    self.TopChrono=datetime.datetime.today()
#    self.operateur=''
    if not os.access(self.repRapport,os.F_OK):
      os.mkdir(self.repRapport)
    if not os.access(os.path.join(self.repRapport,"nng-analyses"),os.F_OK):
      os.mkdir(os.path.join(self.repRapport,"nng-analyses"))
    if not os.access(os.path.join(self.repRapport,"nng-analyses","variables"),os.F_OK):
      os.mkdir(os.path.join(self.repRapport,"nng-analyses","variables"))
    if not os.access(os.path.join(self.repRapport,"nng-analyses","variables","utiles"),os.F_OK):
      os.mkdir(os.path.join(self.repRapport,"nng-analyses","variables","utiles"))
    if not os.access(os.path.join(self.repRapport,"nng-analyses","variables","inutiles"),os.F_OK):
      os.mkdir(os.path.join(self.repRapport,"nng-analyses","variables","inutiles"))
    if self.projet.execution in ['X','K']:
      for var in glob.glob(os.path.join(self.repRapport,"nng-analyses","variables","utiles","*.var")):
        os.remove(var)
      for var in glob.glob(os.path.join(self.repRapport,"nng-analyses","variables","inutiles","*.var")):
        os.remove(var)
      for ficanalyse in ["ControleFichesLst.txt","IHMpngKO.txt","IHMpngInutilises.txt","IHMpngOK.txt"]:
        if os.access(os.path.join(self.repRapport,"nng-analyses",ficanalyse),os.F_OK):
          try:
            os.remove(os.path.join(self.repRapport,"nng-analyses",ficanalyse))
          except:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Impossible de détruire %s !"%os.path.join(self.repRapport,"nng-analyses",ficanalyse),style="Wr")
    self.nangu_OK=True

  #-----------------------------------------------------------------------------------
  # Fonction de traitement d'un repertoire
  #-----------------------------------------------------------------------------------
  def locTraiteRepertoire(self,input_dir,elem,lot,fichierListe=None):
    lotFiches=[]
    # Le repertoire contient un fichier <xxx>.lst de fiches
    if fichierListe is not None and os.access(os.path.join(input_dir,elem,fichierListe+".lst"),os.F_OK):
      lotFiches=locTraiteFichierListe(input_dir,elem,fichierListe+".lst")
    else:
      # Le repertoire contient un fichier fiches.lst de liste de fiches
      if os.access(os.path.join(input_dir,elem,'fiches.lst'),os.F_OK):
        lotFiches=locTraiteFichierListe(input_dir,elem,'fiches.lst')
      else:
        if elem[0]!=";":
          if os.path.isdir(os.path.join(input_dir,elem)):
            for elem2 in os.listdir(os.path.join(input_dir,elem)):
              if elem2.find('.xml') != -1:
                 lotFiches.append(os.path.join(elem,os.path.splitext(elem2)[0]))
          else:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, %s n'est pas un répertoire de fiches."%os.path.join(input_dir,elem),style="Er")
            self.notRep.append(os.path.join(input_dir,elem))
        # tri des fiches du repertoire specifique dans l'ordre alphabetique
        #lotFiches.sort(lambda x,y:cmp(x.lower(),y.lower()))
        lotFiches=sorted(lotFiches)
    for elem2 in lotFiches:
      lot.append(elem2)
    return lot

  #-----------------------------------------------------------------------------------
  # Fonction de traitement d'un fichier de listes
  #-----------------------------------------------------------------------------------
  def locTraiteFichierLST(self,scenar_dir,input_dir,lot,elem,referenceSubst=None):
    elem=elem.replace('.lst','').replace('.pv','')
    (rep,fic)=(os.path.dirname(elem),os.path.basename(elem))

    if self.typePRJsimple:
      if not os.access(os.path.join(scenar_dir,elem+".pv"),os.F_OK) and not os.access(os.path.join(scenar_dir,elem+".lst"),os.F_OK) :
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, le fichier %s est inexistant."%os.path.join(scenar_dir,elem+".pv"),style="Wr")
        return []
      if os.access(os.path.join(scenar_dir,elem+".pv"),os.F_OK): f=open(os.path.join(scenar_dir,elem+".pv"),'r',encoding='utf-8')
      if os.access(os.path.join(scenar_dir,elem+".lst"),os.F_OK): f=open(os.path.join(scenar_dir,elem+".lst"),'r',encoding='utf-8')
    else:
      if not os.access(os.path.join(scenar_dir,elem+".lst"),os.F_OK):
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, le fichier %s est inexistant."%os.path.join(scenar_dir,elem+".lst"),style="Wr")
        return []
      f=open(os.path.join(scenar_dir,elem+".lst"),'r',encoding='utf-8')

    # Traitement du sous titre
    fiche=f.readline()
    fiche=fiche.replace('\n','').replace('\r','')
    idx=fiche.find('SOUS-TITRE')
    if idx==-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, sous-titre du document dans le fichier %s inexistant."%os.path.join(scenar_dir,elem+".lst"),style="Wr")
    else:
      fiche=fiche[idx+len('SOUS-TITRE'):]
      while fiche!='' and fiche[0]==' ':
        fiche=fiche[1:]
      self.projet.PRODUIT_SOUS_TITRE=nngsrc.services.SRV_Encodage(fiche,"Sous Titre")
    # Traitement de la sous-référence
    fiche=f.readline()
    fiche=fiche.replace('\n','').replace('\r','')
    idx=fiche.find('SOUS-REFERENCE')
    if idx==-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, sous-reference du document dans le fichier %s inexistant."%os.path.join(scenar_dir,elem+".lst"),style="Wr")
    else:
      fiche=fiche[idx+len('SOUS-REFERENCE'):]
      while fiche!='' and fiche[0]==' ':
        fiche=fiche[1:]
      self.projet.PRODUIT_SOUS_REFERENCE=nngsrc.services.SRV_Encodage(fiche,"Sous Reference")
    if self.projet.version>=9.0:
      # Traitement de la methode
      fiche=f.readline()
      fiche=fiche.replace('\n','').replace('\r','')
      idx=fiche.find('METHODE')
      if idx==-1:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Attention, methode du document dans le fichier %s inexistant."%os.path.join(scenar_dir,elem+".lst"),style="Wr")
      else:
        fiche=fiche[idx+len('METHODE'):]
        while fiche!='' and fiche[0]==' ':
          fiche=fiche[1:]
        self.projet.DOCUMENT_METHODE=fiche
    # Traitement des fiches
    fiche="vide"
    reserveBloc=[]
    iter_cond=None
    dansBloc=False
    dansParall=False
    while fiche!= '':
      fiche=f.readline()
      if fiche!='' and fiche[0]!=';':
        fiche=fiche.replace('\n','').replace('\r','').replace(".xml",'')
        #print "DBG>",fiche,os.path.join(input_dir,fiche)
        if fiche!='':
          if fiche[:3]=="/-x": # debut de boucle iterative
            dansBloc=True
            iter_cond=fiche.replace('"','').replace("'",'').replace('/-x','')
            #print "DBG> boucle iter",iter_cond
            reserveBloc=[]
          elif fiche[:3]=="/-{": #début de boucle sur liste de valeurs
            dansBloc=True
            iter_cond=fiche[2:]
            #print "DBG> boucle liste",iter_cond
            reserveBloc=[]
          elif fiche[:3]=="/-(": #début de bloc conditionnel
            dansBloc=True
            iter_cond=fiche[2:]
            #print "DBG> condition",iter_cond
            reserveBloc=[]
          elif fiche[:3]=="/-|": #début de bloc de parallelisation
            dansBloc=True
            dansParall=True
            iter_cond=fiche[2:]
            #print "DBG> nbcpus",iter_cond
            reserveBloc=[]
          elif fiche=="\\--": # fin de boucle
            #print "DBG> fin de boucle ou condition"
            dansBloc=False
            if dansParall and not PARALLELISATION:
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, La parallélisation est impossible, le bloc sera traité séquentiellement.",style="Wr")
              lot.extend(reserveBloc)
            else:
              lot.append((iter_cond,reserveBloc))
            dansParall=False
#          elif fiche[0]=="|": # fiche dans le bloc
#            ffiche=fiche[1:].split(' ')[0]
#            if os.access(os.path.join(input_dir,ffiche)+'.xml',os.F_OK):
#              reserveBloc.append(fiche[1:])
          else: #fiche
            if sys.platform=="win32":
              fiche=fiche.replace('/','\\')
            else:
              fiche=fiche.replace('\\','/')
            lstfiche=fiche.split(' ')
            noxfiche=lstfiche[0]

            tagfiche=''
            if len(lstfiche)>1:
              tagfiche=lstfiche[1]
              if tagfiche[0]!='§':
                tagfiche=''
            if os.access(os.path.join(input_dir,noxfiche)+'.xml',os.F_OK):
              if dansBloc:
                reserveBloc.append(noxfiche)
              elif tagfiche=='':
                lot.append(fiche)
              else:
                lot.append(noxfiche+tagfiche)
          #  else:
          #    lot=self.locTraiteRepertoire(input_dir,fiche,lot,elem)
    f.close()
    return lot

  #-----------------------------------------------------------------------------------
  # Fonction de traitement d'un fichier de listes
  #-----------------------------------------------------------------------------------
  def locTraiteFichierPV(self,plan_dir,input_dir,elem,referenceSubst=None):
    rapports=nngsrc.services.SRV_LireContenuListe(os.path.join(plan_dir,elem+".pv"))
    lot={}
    num=1
    for rap in rapports[self.projet.tailleEntete:]:
      if rap!='' and rap[0]!=';':
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_SCENARIO')%rap,style="Wr")
        lot[num]=[self.locTraiteFichierLST(self.repScenarios,input_dir,[],rap.split(" :")[0],referenceSubst),self.projet.PRODUIT_SOUS_TITRE,rap.replace('.lst','')]#self.projet.PRODUIT_REFERENCE+self.projet.PRODUIT_SOUS_REFERENCE]
        self.projet.PRODUIT_SOUS_TITRE=''
        self.projet.PRODUIT_SOUS_REFERENCE=''
        num=num+1
    fiche=rapports[0].split(" ")
    if len(fiche)==2: fiche=fiche[1]
    else: fiche=''
    self.projet.PRODUIT_SOUS_TITRE=nngsrc.services.SRV_Encodage(fiche,"Sous Titre")
    fiche=rapports[1].split(" ")
    if len(fiche)==2: fiche=fiche[1]
    else: fiche=''
    self.projet.PRODUIT_SOUS_REFERENCE=nngsrc.services.SRV_Encodage(fiche,"Sous Reference")
    # Traitement de la methode
    fiche=rapports[2].split(" ")
    if len(fiche)==2: fiche=fiche[1]
    else: fiche=''
    self.projet.DOCUMENT_METHODE=fiche
    # Filtre d'Exigences
    filtre=nngsrc.services.SRV_LireContenuListe(os.path.join(plan_dir,elem+".exg"))
    action=0
    listeExigences=[]
    for f in range(len(filtre)):
      if filtre[f][0]==';': continue
      elif filtre[f]=="RETENU" and action==0: action=1
      elif filtre[f]=="EXCLU" and action==0: action=-1
      else: listeExigences.append(filtre[f])
    if action==1:
      keys=list(self.projet.exigences.keys())
      for exig in keys:
        if not exig in listeExigences:
          del self.projet.exigences[exig]
    elif action==-1:
      for exig in listeExigences:
        del self.projet.exigences[exig]
    return lot

  #---------------------------------------------------------------------------------
  def razVariables(self,RAZ=True):
    if RAZ:
      self.variables=[]
      self.variables.append(dict({'NOM':'PLAN'      ,'VARENVPRJ':('INT',None),'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':''           ,'UTILE':True ,'TYPE':'ND'    ,'PERSISTANCE':True,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_PLAN')}))
      self.variables.append(dict({'NOM':'SCENARIO'  ,'VARENVPRJ':('INT',None),'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':''           ,'UTILE':True ,'TYPE':'ND'    ,'PERSISTANCE':True,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_SCENARIO')}))
      self.variables.append(dict({'NOM':'OPERATEUR' ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':'<OPERATEUR>','UTILE':False,'TYPE':'SAISIE','PERSISTANCE':True,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_OPERATEUR')}))
      self.variables.append(dict({'NOM':'SAISIE'    ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':'<SAISIE>'   ,'UTILE':False,'TYPE':'SAISIE','PERSISTANCE':True,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_OPERATEUR')}))
      self.variables.append(dict({'NOM':'TIMER'     ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':'<TIMER>'    ,'UTILE':False,'TYPE':'SAISIE','PERSISTANCE':True,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_TIMER')}))
    vartemp=deepcopy(self.variables)
    for v in self.variables:
      if not v['PERSISTANCE']:
        vartemp.remove(v)
    self.variables=vartemp
    self.variables.append(dict({'NOM':'DEPOT_FICHES','VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':self.repFiches               ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_DEPOT_FICHES')}))
    self.variables.append(dict({'NOM':'PROD_NOM'    ,'VARENVPRJ':('INT',None),'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':self.projet.PRODUIT_NOM      ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_PROD_NOM')}))
    self.variables.append(dict({'NOM':'PROD_VER'    ,'VARENVPRJ':('INT',None),'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':self.projet.PRODUIT_VERSION  ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_PROD_VER')}))
    self.variables.append(dict({'NOM':'FICHE'       ,'VARENVPRJ':('INT',None),'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':''                           ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_FICHE')}))
    self.variables.append(dict({'NOM':'MODE'        ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':self.projet.execution        ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_MODE')}))
    self.variables.append(dict({'NOM':'ESPERLUETTE' ,'VARENVPRJ':None        ,'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':'&'                          ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':''}))
    self.variables.append(dict({'NOM':'PIPE'        ,'VARENVPRJ':None        ,'CONTEXTUEL':False,'INDEXER':False,'INDEX':None,'VALEUR':'|'                          ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':''}))
    self.variables.append(dict({'NOM':'ITER_FICHE'  ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':''                           ,'UTILE':False,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_ITER_FICHE')}))
    self.variables.append(dict({'NOM':'ITER_ACTIONS','VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':''                           ,'UTILE':False,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_ITER_ACTIONS')}))
    self.variables.append(dict({'NOM':'NANGUPRJ'    ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':os.getenv('NANGUPRJ','.')    ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_NANGUPRJ')}))
    self.variables.append(dict({'NOM':'NANGU'       ,'VARENVPRJ':('INT',None),'CONTEXTUEL':True ,'INDEXER':False,'INDEX':None,'VALEUR':nngsrc.services.varNANGU     ,'UTILE':True ,'TYPE':'ND','PERSISTANCE':False,'JDD':None,'COMMENTAIRE':self.projet.lexique.entree('VARIABLE_NANGU')}))

  #---------------------------------------------------------------------------------
  def TraiterEnvironnement(self,listeEnv,environnement,variables,ListResValeur):
    # Pour chaque variable du projet, si elle est utile, on la stocke dans listeEnv
    for e in variables:
      commentaire=e['COMMENTAIRE']
      if commentaire=='': commentaire=None
      if e['NOM'] not in ['PLAN','SCENARIO','FICHE']:
        if e['UTILE']:
          for orig in self.listeKeyEnv.keys():
            if e['TYPE'] in self.listeKeyEnv[orig]:
              if not [e['VALEUR'],commentaire,e['VARENVPRJ'],e['NOM']] in listeEnv[e['TYPE']]:
              #  print(e['NOM'])
              #  print(e)
                listeEnv[e['TYPE']].append([e['VALEUR'],commentaire,e['VARENVPRJ'],e['NOM'],e['JDD']])
    # Pour chaque nouvelle variable de la fiche, si elle est utile, on la stocke dans listeEnv
    for e in ListResValeur:
      if e:
        commentaire=e['COMMENTAIRE']
        if commentaire=='': commentaire=None
        if e['NOM'] not in ['PLAN','SCENARIO','FICHE']:
          if e['UTILE']:
            for orig in self.listeKeyEnv.keys():
              if e['TYPE'] in self.listeKeyEnv[orig]:
                if not [e['VALEUR'],commentaire,e['VARENVPRJ'],e['NOM']] in listeEnv[e['TYPE']]:
                  listeEnv[e['TYPE']].append([e['VALEUR'],commentaire,e['VARENVPRJ'],e['NOM'],e['JDD']])

    for typeData in listeEnv:
      lstInstances=[]
      for valeur in listeEnv[typeData][1:]:
        vinstance=nngsrc.services.SRV_Variables(str(valeur[0]),variables)
        cinstance=nngsrc.services.SRV_Variables(valeur[1],variables)
        oinstance=valeur[2]
        ninstance=valeur[3]
        jinstance=valeur[4]
        ajouter=True
        if vinstance:
          if vinstance.find('[')!=-1:
            ajouter=False
        if cinstance:
          if cinstance.find('[')!=-1:
            ajouter=False
        if ajouter:
          lstInstances.append([vinstance,cinstance,oinstance,ninstance,jinstance])
      for valeur in lstInstances:
        if not valeur in environnement[typeData]:
          environnement[typeData].extend([valeur])
#    print "LSTENV<",environnement
    return listeEnv, environnement

  #---------------------------------------------------------------------------------
  def ajouterVariableFonction(self,valeur,variable):
    if variable is not None:
      v=nngsrc.services.SRV_IndexVariable(variable,self.variables)
      if v!=-1:
        if valeur=="'*n*'": valeur='<'+variable+'>'
        self.variables[v]['VALEUR']=str(valeur)
        self.variables[v]['UTILE']=True
      else:
        self.variables.append(dict({'NOM':variable,'VARENVPRJ':None,'INDEXER':False,'INDEX':None,'VALEUR':'','CONTEXTUEL':True,
                                    'UTILE':False,'TYPE':'VARIABLE','PERSISTANCE':True,'COMMENTAIRE':'','FICHES':[],'JDD':None}))
        v=-1
        self.variables[v]['VALEUR']=str(valeur)
        self.variables[v]['UTILE']=True

  #---------------------------------------------------------------------------------
  def ChoixCommande(self,fonction,fiche,continuer,cmd,index,ident,condition,actLoopFT):
    listeVar=nngsrc.services.SRV_listeVar(self.variables)
    res=[]
    if fonction=="DETRUIRE_FICHIER":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction DETRUIRE_FICHIER obsolete en v5.5, remplacee par VALID_DetruitFichier.",style="Wr")
      fonction="VALID_DetruitFichier"
    if fonction=="DETRUIRE_REPERTOIRE":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction DETRUIRE_REPERTOIRE obsolete en v5.6, remplacee par VALID_DetruitRepertoire.",style="Wr")
      fonction="VALID_DetruitRepertoire"
    if fonction=="CREER_REPERTOIRE":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction CREER_REPERTOIRE obsolete en v5.6, remplacee par VALID_CreeRepertoire.",style="Wr")
      fonction="VALID_CreeRepertoire"
    if fonction=="VIDER_REPERTOIRE":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VIDER_REPERTOIRE obsolete en v5.6, remplacee par VALID_VideRepertoire.",style="Wr")
      fonction="VALID_VideRepertoire"
    if fonction=="VALID_ParseLigne":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VALID_ParseLigne obsolete, remplacee par VALID_InspecteFichier.",style="Wr")
      fonction="VALID_InspecteFichier"
    if fonction=="VALID_VarEnv":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VALID_VarEnv renommée en VALID_VariableEnvironnement depuis la version v5.0",style="Wr")
      fonction="VALID_VariableEnvironnement"
    if fonction=="VALID_AccesFichier":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_AccesFichier obsolete depuis v20.06.02, utiliser VALID_ControleAccesFichier ou VALID_ControleAccesRépertoires ",style="Wr")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"          Remplacé par VALID_ControleAccesFichier mais ne permet pas de contrôler les répertoires.",style="Wr")
      fonction="VALID_ControleAccesFichier"
    if fonction=="VALID_DetruitFichier":
      res=self.fonctions.VALID_DetruitFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CreeRepertoire":
      res=self.fonctions.VALID_CreeRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_VideRepertoire":
      res=self.fonctions.VALID_VideRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_DetruitRepertoire":
      res=self.fonctions.VALID_DetruitRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_NombreFichiersRepertoires":
      (res,(valeur,variable))=self.fonctions.VALID_NombreFichiersRepertoires(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_InspecteXML":
      (res,(valeur,variable))=self.fonctions.VALID_InspecteXML(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_InspecteJSON":
      (res,(valeur,variable))=self.fonctions.VALID_InspecteJSON(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_InspecteFichier":
      (res,(valeur,variable))=self.fonctions.VALID_InspecteFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_InspecteNetCDF":
      (res,(valeur,variable))=self.fonctions.VALID_InspecteNetCDF(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_Commande":
      (res,(valeur,variable))=self.fonctions.VALID_Commande(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_CommandeRecherche":
      res=self.fonctions.VALID_CommandeRecherche(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CommandeEx":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VALID_CommandeEx trop complexe. supprimée.",style="Wr")
#      res=self.fonctions.VALID_CommandeEx(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      res=None
    elif fonction=="VALID_CopieFichier":
      res=self.fonctions.VALID_CopieFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_DeplaceFichier":
      res=self.fonctions.VALID_DeplaceFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CopieRepertoire":
      res=self.fonctions.VALID_CopieRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_DeplaceRepertoire":
      res=self.fonctions.VALID_DeplaceRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_Compare":
      res=self.fonctions.VALID_Compare(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareBin":
      res=self.fonctions.VALID_CompareBin(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareLignes":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VALID_CompareLignes obsolete en v4.3, remplacee par VALID_CompareExtrait. Controlez les parametres",style="Wr")
      res=self.fonctions.VALID_CompareExtrait(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareExtrait":
      res=self.fonctions.VALID_CompareExtrait(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_Compare2FichiersLignes":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_Compare2FichiersLignes obsolete en v4.3, remplacee par VALID_CompareFichier",style="Wr")
      res=self.fonctions.VALID_CompareFichiers(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareFichiers":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_CompareFichiers obsolete, remplacee par VALID_CompareFichier",style="Wr")
      res=self.fonctions.VALID_CompareFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareFichier":
      res=self.fonctions.VALID_CompareFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareRepertoires":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_CompareRepertoires obsolete, remplacee par VALID_CompareRepertoire",style="Wr")
      res=self.fonctions.VALID_CompareRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareRepertoire":
      res=self.fonctions.VALID_CompareRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareValeur":
      res=self.fonctions.VALID_CompareValeur(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareChaines":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_CompareChaines obsolete, remplacee par VALID_CompareChaine",style="Wr")
      res=self.fonctions.VALID_CompareChaine(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_CompareChaine":
      res=self.fonctions.VALID_CompareChaine(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_RechercheTexte":
      res=self.fonctions.VALID_RechercheTexte(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_RechercheXTexte":
      res=self.fonctions.VALID_RechercheXTexte(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_RechercheMultipleTexte":
      (res,(valeur,variable))=self.fonctions.VALID_RechercheMultipleTexte(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_ControleAccesFichier":
      res=self.fonctions.VALID_ControleAccesFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_ControleAccesRepertoire":
      res=self.fonctions.VALID_ControleAccesRepertoire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_DateFichier":
      (res,(valeur,variable))=self.fonctions.VALID_DateFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_TailleFichier":
      (res,(valeur,variable))=self.fonctions.VALID_TailleFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_FichierImage":
      res=self.fonctions.VALID_FichierImage(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_FichierTexte":
      res=self.fonctions.VALID_FichierTexte(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_Resultat":
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION, Fonction VALID_Resultat obsolete en v5.0, remplacee par VALID_MessageAction. Controlez les parametres",style="Wr")
      res=self.fonctions.VALID_MessageAction(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_MessageAction":
      res=self.fonctions.VALID_MessageAction(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_MethodeIAD":
      res=self.fonctions.VALID_MethodeIAD(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_EtapeManuelle":
      res=self.fonctions.VALID_EtapeManuelle(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction=="VALID_SaisieOperateur":
      (res,(valeur,variable))=self.fonctions.VALID_SaisieOperateur(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_RecupereVarEnv":
      (res,(valeur,variable))=self.fonctions.VALID_RecupereVarEnv(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_RecupereNbFicRepert":
      (res,(valeur,variable))=self.fonctions.VALID_RecupereNbFicRepert(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_RecupereNbLignesFic":
      (res,(valeur,variable))=self.fonctions.VALID_RecupereNbLignesFic(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_Timer":
      (res,timer)=self.fonctions.VALID_Timer(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      v=nngsrc.services.SRV_IndexVariable('TIMER',self.variables)
      if v!=-1:
        self.variables[v]['VALEUR']=timer
        self.variables[v]['UTILE']=True
    elif fonction=="VALID_LitFichier":
      (res,(valeur,variable))=self.fonctions.VALID_LitFichier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
    elif fonction=="VALID_VariableEnvironnement":
      (res,(valeur,variable))=self.fonctions.VALID_VariableEnvironnement(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
      self.ajouterVariableFonction(valeur,variable)
#    elif fonction=="VALID_IHMrecherche":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMrecherche(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMnbOccurrence":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMnbOccurrence(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMclick":
#      if not self.deviceUInput:
#        self.deviceUInput=self.fonctionsIHM.StartUInput()
#      res=self.fonctionsIHM.VALID_IHMclick(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT,self.deviceUInput)
#    elif fonction=="VALID_IHMtouchesClavier":
#      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_IHMtouchesClavier obsolete en v6.0, remplacee par VALID_IHMclavier.",style="Wr")
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMclavier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMclavier":
#      if not self.deviceUInput:
#        self.deviceUInput=self.fonctionsIHM.StartUInput()
#      res=self.fonctionsIHM.VALID_IHMclavier(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT,self.deviceUInput)
#    elif fonction=="VALID_IHMtouche":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMtouche(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMtoucheWindows":
#      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ATTENTION Fonction VALID_IHMtoucheWindows obsolete en v6.0, utiliser VALID_IHMclavier a la place. Resultats et utilisation non garantie.",style="Wr")
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMtoucheWindows(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMecrire":
#      if not self.deviceUInput:
#        self.deviceUInput=self.fonctionsIHM.StartUInput()
#      res=self.fonctionsIHM.VALID_IHMecrire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT,self.deviceUInput)
#    elif fonction=="VALID_IHMlire":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      (res,(valeur,indice))=self.fonctionsIHM.VALID_IHMlire(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#      if indice:
#        v=nngsrc.services.SRV_IndexVariable(indice,self.variables)
#        if v!=-1:
#          if valeur=="'*n*'": valeur='<'+indice+'>'
#          self.variables[v]['VALEUR']=valeur
#          self.variables[v]['UTILE']=True
#    elif fonction=="VALID_IHMarret":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMarret(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMlancement":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMlancement(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMdragDrop":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMdragDrop(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMpositionAction":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMpositionAction(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMposition":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMposition(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMmenuDeroulant":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMmenuDeroulant(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMcapture":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMcapture(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMdeplacementAction":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMdeplacementAction(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMselection":
#      if not self.isStartSikuli:
#        self.fonctionsIHM.StartSikuli()
#        self.isStartSikuli=True
#      res=self.fonctionsIHM.VALID_IHMselection(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMsetSimilitude":
#      res=self.fonctionsIHM.VALID_IHMsetSimilitude(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMsetDelaiSouris":
#      res=self.fonctionsIHM.VALID_IHMsetSourisDelai(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
#    elif fonction=="VALID_IHMsetEcran":
#      res=self.fonctionsIHM.VALID_IHMsetEcran(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    elif fonction is None:
        res=self.fonctions.EtapeDeTest(fiche,continuer,cmd,listeVar,index,ident,condition,actLoopFT)
    else:
      nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"\nERREUR : Fonction '%s' non implantee !\n"%fonction,style="Er")
    return res

  #---------------------------------------------------------------------------------
  def RetraitValeursSubstitution(self,element,opestring):
    #print "DBG>",element
    if element is None: return None
    if type(element)==list:
      return [self.RetraitValeursSubstitution(e,opestring) for e in element]
    opeindex=0
    while element.find(opestring,opeindex)!=-1:
      op1=element.find(opestring,opeindex)+len(opestring)-1
      op2=element.find("]",op1)
      opeindex=op1
      element=element[:op1]+element[op2:]
    return element

  #---------------------------------------------------------------------------------
  def AjoutValeursSubstitution(self,element,opestring,valstring,texte=None):
    if element is None: return None
    if type(element)==list:
      return [self.AjoutValeursSubstitution(e,opestring,valstring,texte) for e in element]
    opeindex=0
    while element.find(opestring,opeindex)!=-1:         # [xxxx] ?
#      print "DBG-AjoutValeursSubstitution>", element, valstring
      op1=element.find(opestring,opeindex)              # op1=index de [xxxx]
      op2=element.find("]",op1)+1                       # op2=index de ]
      opeindex=op2
#      print "DBG>",element,valstring
      element=element[:op1]+opestring.replace(']','~')+valstring+']'+element[op2:]
#      print "   >",element,valstring
      if texte:
#        print "TXT>",texte
        op1=texte.find(opestring)
        op2=texte.find("]",op1)+1
        texte=texte[:op1]+valstring+texte[op2:]
    if texte:
      return element, texte
    return element

  #---------------------------------------------------------------------------------
  def GestionTags(self,cmd,fiche):
    texteFicheCommande="Fiche %s, tag de la commande %s\n"%(fiche,cmd['FONCTION'])
    if self.projet.execution=='K':
      TAG=cmd['TAG']
#      print "DBG>", "Tag de la commande", cmd['FONCTION']
      if TAG is None:
        nngsrc.fichesXML.tagActionsFicheXML(self.repRapport,self.repFiches,"%s.xml"%fiche,self.projet,)
      else:
        #print "DBG-TAG>",TAG
        #print "DBG-listeTags>",self.listeTags
        #print "DBG-qualite.tagFT>",self.qualite.tagFT
        if TAG in self.listeTags:
        #  f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a",encoding='utf-8')
        #  f.write("Fiche %s, tag de la commande %s\n" % (fiche,cmd['FONCTION']))
        #  f.close()
          if TAG in self.qualite.tagFT:
#            print "DBG>","ATTENTION doublon de TAG en FT !"
#            print "  COMM ",cmd["COMMENTAIRE"]
#            print "  FICHE",fiche
#            print "  TAG  ",TAG
            self.ajustementsTAG=True
            f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a",encoding='utf-8')
            f.write(texteFicheCommande)
            f.write("   > ATTENTION doublon de TAG en FT !\n")
            f.write("      COMM  %s\n"%cmd["COMMENTAIRE"])
            f.write("      FICHE %s\n"%fiche)
            f.write("      TAG   %s\n"%TAG)
            f.write("     Recherche de la FT...\n")
            f.close()
            ficTAG="%s : %s"%(self.projet.lexique.entree('MOT_FICHE'),fiche)
            comTAG=cmd["COMMENTAIRE"]
            trouve=False
            laFT=None
            for x in self.qualite.baseFT:
              libelle=x['LIBELLE']
              if x['TAG'].find(TAG)==0:
                laFT=x
                self.ajustementsTAG=True
                f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a",encoding='utf-8')
                f.write("     FT trouvee : \n")
                f.write("---\n%s\n---\n"%libelle)
                f.close()
                if not((ficTAG in libelle) and (comTAG in libelle)):
                  trouve=True
#                  print "DBG> RETAG s/ non FT :",TAG
                  self.ajustementsTAG=True
                  f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a",encoding='utf-8')
                  f.write("     Re-TAG s/ non FT\n")
                  f.close()
                  nngsrc.fichesXML.tagActionsFicheXML(self.repRapport,self.repFiches,"%s.xml"%fiche,self.projet,TAG)
            if not trouve:
              self.ajustementsTAG=True
              nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : Ambiguite de TAG s/ FT",style="Er",parallel=self.parallel)
#              print "DBG> Ambiguite de TAG s/ FT"
#              print "CMD>",fiche,cmd
#              print "FT >",laFT
              f=open(os.path.join(self.repRapport,"nng-analyses","ajustementsTAG.txt"),'a',encoding='utf-8')
              f.write("     Ambiguite entre une action de %s\n"%self.dicoTags[TAG])
              f.write("     et la FT:\n")
              f.write("       %s\n"%laFT['LIBELLE'])
              f.close()
              nngsrc.fichesXML.tagActionsFicheXML(self.repRapport,self.repFiches,"%s.xml"%self.dicoTags[TAG],self.projet,TAG)
          else:
 #           print "DBG> Re-TAG simple:",TAG
            self.ajustementsTAG=True
            #f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a",encoding='utf-8')
            #f.write("   > Re-TAG simple\n")
            #f.close()
            nngsrc.fichesXML.tagActionsFicheXML(self.repRapport,self.repFiches,"%s.xml"%fiche,self.projet,TAG)
        else:
#          print "Svg : fiche %s ,cmd %s, tag %s"%(fiche,cmd['FONCTION'],TAG)
#          f = open(os.path.join(self.repRapport, "nng-analyses", "ajustementsTAG.txt"), "a")
#          f.write("   > Pas de redondance, fiche %s ,cmd %s\n"%(fiche,cmd['FONCTION']))
#          f.close()
          self.listeTags.append(TAG)
          self.dicoTags[TAG]=fiche

  #---------------------------------------------------------------------------------
  def GestionBouclesActions(self,iterationLoop,libelleLoop,conditionLoop,externLoop,numAction,identLoop):
    nbloop=1
    count=1
    pasloop=1

    # L'itération de la fiche est n
    if iterationLoop=='n':
      nbloop=-1    # Boucle infinie
      identLoop+=1 # identification de la boucle
      # Ajustement du nombre de boucle par saisie dans le cas d'un cahier
      if self.projet.execution=='C':
        if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU)
        valeur=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"Veuillez entrer le nombre d'iterations (xn) du bloc d'actions %s "%libelleLoop,"SAISIE")
        nbloop=int(valeur)

    # L'itération de la fiche est une liste debut,fin,pas
    elif type(iterationLoop)!=int and len(iterationLoop.split(','))==3:
      identLoop+=1 # identification de la boucle
      listevar=nngsrc.services.SRV_listeVar(self.variables)
      iterationLoop=nngsrc.services.SRV_RemplacerVariable(iterationLoop,listevar)
      iterationLoop=nngsrc.services.SRV_Validateur(iterationLoop)
      tmp=iterationLoop.split(',')
      if self.projet.execution=='R':
        count=int(tmp[0])
        #print("DBG>",tmp)
        nbloop=int(tmp[1])
        pasloop=int(tmp[2])

    # L'itération de la fiche est une liste debut,fin,pas
    else:
      if type(iterationLoop)!=int:
        listevar=nngsrc.services.SRV_listeVar(self.variables)
        iterationLoop=nngsrc.services.SRV_RemplacerVariable(iterationLoop,listevar)
        iterationLoop=nngsrc.services.SRV_Validateur(iterationLoop)
        # TODO revoir cette partie sur le boucles d'iterations à variables
        # Pour le moment les calculs {{ }} ne sont pas gérés pour le plan
        if list(iterationLoop)[0] in ['[','<']:
          nbloop=-1
          variable=iterationLoop[1:].replace(']','')
          if self.projet.execution=='C' or (self.projet.execution=='R' and self.projet.rapportManuel):
            if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU)
            iterationLoop=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"Veuillez entrer le nombre d'iterations (saisie operateur) du bloc d'actions %s "%libelleLoop,"SAISIE")
            self.variables[nngsrc.services.SRV_IndexVariable(variable,self.variables)]['VALEUR']=iterationLoop
          elif self.projet.execution=='R':
            iterationLoop=self.variables[nngsrc.services.SRV_IndexVariable(variable,self.variables)]['VALEUR']
      try:
        nbloop=int(iterationLoop)
        if nbloop>1:
          identLoop+=1
      except:
        if iterationLoop!="?":
          nbloop=-2
          identLoop+=1
        else :
          nbloop=-1
          identLoop+=1
    if nbloop==-1:
      forever=True
    else :
      forever=False
    if self.projet.execution in ['P','S','X','K']:
      self.variables[nngsrc.services.SRV_IndexVariable("ITER_ACTIONS",self.variables)]['VALEUR']='<ITER_ACTIONS>'
    else:
      self.variables[nngsrc.services.SRV_IndexVariable("ITER_ACTIONS",self.variables)]['VALEUR']=str(count)
    itemIndexLoop=numAction
    self.loop=str(iterationLoop)

    DebutBoucle=False
    FinBoucle=False
    texteBoucleDeb=('','')
    texteBoucleFin=('','')
    if self.loop!="1":# and (self.projet.execution in ['P','S','X','K']):
      DebutBoucle=True

    return identLoop,itemIndexLoop,count,nbloop,pasloop,forever,DebutBoucle,FinBoucle,texteBoucleDeb,texteBoucleFin,iterationLoop,libelleLoop,conditionLoop,externLoop

  #---------------------------------------------------------------------------------
  def TBAgestionVariables(self,cmd,cmdCondition):
    txtComment=""
    for p in ['ETAPE','CONDITION','PARAMETRE','COMMENTAIRE','TROUVER_NON','TROUVER_OUI','VALIDER_OK','VALIDER_PB','RETOUR','LISTE']:
      if p in cmd:
        # Retrait des valeurs de substitution
        listeVariables=nngsrc.services.SRV_listeVar(self.variables)
        for (opestring,_,_) in listeVariables:
          opestring=opestring.replace(']','~')
          cmd[p]=self.RetraitValeursSubstitution(cmd[p],opestring)
        # Ajout des valeurs de substitution
        # Cas particulier de COMMENTAIRE
        if p=='COMMENTAIRE':
          if cmd[p]=="LOCUT_SANS_COMMENTAIRE": txtComment=self.projet.lexique.entree(cmd[p])
          else: txtComment=cmd[p]
          if txtComment:
            for (opestring,valstring,_) in listeVariables:
              (cmd[p],txtComment)=self.AjoutValeursSubstitution(cmd[p],opestring,valstring,txtComment)
        # Cas particulier de CONDITION
        elif p=='CONDITION':
          cmdCondition=cmd[p]
          if self.projet.execution=='R':
            if cmdCondition:
              for (opestring,valstring,_) in listeVariables:
                (cmd[p],cmdCondition)=self.AjoutValeursSubstitution(cmd[p],opestring,valstring,cmdCondition)
        # Cas général
        else:
          for (opestring,valstring,_) in listeVariables:
            cmd[p]=self.AjoutValeursSubstitution(cmd[p],opestring,str(valstring))
    return cmd,cmdCondition,txtComment

  #---------------------------------------------------------------------------------
  def ResultatDansRapport(self,cmd,r):
    # Le resultat est dans le rapport :
    # - si execution in 'S', 'P', 'C', 'Y'
    # - si rapport='oui' (0)
    # - si rapport='siNonOK' (1) et resultat(r)!=OK
    # - si rapport='siOK' (2) et resultat(r)=OK
    # - si debut ou fin de boucle
    return self.projet.execution in ['S','P','C'] or \
           cmd['RAPPORT']==0 or \
           (cmd['RAPPORT']==1 and r!=self.constantes['STATUS_OK']) or \
           (cmd['RAPPORT']==2 and r==self.constantes['STATUS_OK'])

  #---------------------------------------------------------------------------------
  def TraitementBlocActions(self,rang,fiche,tagFiche,description,numAction,continuer,arret,gestBoucles,subListCmd,tabVariables,listConditions,contexte):
    identLoop,itemIndexLoop,count,nbloop,pasloop,forever,DebutBoucle,FinBoucle,texteBoucleDeb,texteBoucleFin,iterationLoop,libelleLoop,conditionLoop,externLoop=gestBoucles
    varList,tabNbParamList,tabIndexlist=tabVariables
    faits_techniques,exigences,gestionFTFiche,matTracabilite,result=contexte

    # Sauvegarde de la commande brute pour répétition ultérieure
    subListCmdSVGBoucle=subListCmd

    valCondition=True
    if conditionLoop is not None:
      listeVariables=nngsrc.services.SRV_listeVar(self.variables)
      for (opestring,valstring,_) in listeVariables:
        aevaluer=nngsrc.services.SRV_RemplacerVariable(conditionLoop,listeVariables)
      valCondition=nngsrc.services.SRV_ReduireCondition(aevaluer,{})=='T'

    # Boucle sur le bloc d'actions
    DebutBoucleModeR=DebutBoucle
    while (forever or count<=nbloop or nbloop==-2) and valCondition:
      subListCmd=subListCmdSVGBoucle
      txtLoop=iterationLoop
      idactLoop="act"
      indexList=0
      index=0

      # Gestion de la liste de variables de la boucle
      for listTmp in varList:
        nomVar=listTmp[0]
        if indexList==0 :
          nbVariable=tabNbParamList[indexList]
          indexList1=(count-1)%nbVariable
          tabIndexlist[indexList]=indexList1+1
        else:
          changeIndex=True
          for i in range (indexList) :
            if tabIndexlist[i]!=1:
              changeIndex=False
          if changeIndex:
            nbVariable=tabNbParamList[indexList]
            valIndex=tabIndexlist[indexList]%nbVariable
            tabIndexlist[indexList]=valIndex+1
        index=tabIndexlist[indexList]
        self.variables[nngsrc.services.SRV_IndexVariable(nomVar,self.variables)]['VALEUR']=listTmp[index]
        indexList=indexList+1

      # Contrôle du nombre de boucle et traitement selon le mode de document
      if nbloop!=1:
        if self.projet.execution in ['P','S','X','K']:
          txtLoop="(x%s)"%iterationLoop
          idactLoop=txtLoop
          nbloop=1
          forever=False
        else:
          if externLoop!='':
            txtLoop="%s.b%d.it%d.act"%(externLoop,identLoop,count)
          else:
            txtLoop="b%d.it%d.act"%(identLoop,count)
#            print("Itération bloc action %s %s %s"%(libelleLoop,txtLoop,idactLoop))
          idactLoop=txtLoop
          if self.projet.debug>2:
            print("Itération bloc action %s %s %s"%(libelleLoop,txtLoop,idactLoop))
          numAction=itemIndexLoop

      # Gestion des actions du bloc d'actions
      while len(subListCmd)!=0:
        cmd=subListCmd[0]
        subListCmd=subListCmd[1:]
        cmdCondition=None

        # Gestion des valeurs de substitution sur des textes contenant des variables
        cmd,cmdCondition,txtComment=self.TBAgestionVariables(cmd,cmdCondition)

        # Initialisations diverses
        fonction=cmd['FONCTION']
        parametre=cmd['PARAMETRE']
        ident=cmd['ID']
        faits_techniques+=cmd['FAITS_TECHNIQUES']+","
        exigences+=cmd['EXIGENCES']+","
        resultatCommande=[]
        lancer=True

        # Gestion des conditions possibles
        # et controle de lancement
        condition=["noTOP",0]
        if (self.projet.execution=='R' and not self.projet.rapportManuel) or \
           (self.projet.rapportManuel and cmdCondition is not None and cmdCondition.find('TOP') == -1 and cmdCondition.find('CHRONO') == -1):
          if cmdCondition is not None:
#            nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"\t    Cond. "+nngsrc.services.SRV_retraitIndexation(cmdCondition),echo=True,parallel=self.parallel)
            lancer=nngsrc.services.SRV_Condition(nngsrc.services.SRV_retraitIndexation(cmdCondition),listConditions)
#            if not lancer:
#              nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"\t\tnon remplie\n",echo=True,parallel=self.parallel)
            if cmdCondition.find('TOP')!=-1 or cmdCondition.find('CHRONO')!=-1:
              condition=cmdCondition.split('=')
              condition[1]=nngsrc.services.SRV_Variables(condition[1],self.variables)
        else:
          if cmdCondition is not None:
            if cmd['APP_CONDITION']:
              lancer=nngsrc.services.SRV_ReduireCondition(cmdCondition,listConditions)!="F"
            else:
              lancer=True

        # boucle sur l'action
        actLoop=True
        cptActLoop=1
        while actLoop:
          # 1- Il s'agit ici d'une boucle à l'intérieur d'une action
          if cmd['BOUCLE'] is not None:
            txtActLoop=".tq%d"%cptActLoop
            self.variables[nngsrc.services.SRV_IndexVariable("ITER_ACTIONS",self.variables)]['VALEUR']=str(cptActLoop)
            actLoopFT=cptActLoop==cmd['BOUCLE'][1]
            # Gestion des valeurs de substitution sur des textes contenant des variables
            cmd,cmdCondition,txtComment=self.TBAgestionVariables(cmd,cmdCondition)
          else:
            txtActLoop=''
            actLoopFT=True

          # 2- Lancement de l'actions
          if lancer:
            # Condition 'TOP'
            if condition[0]=="TOP":
              print(self.projet.lexique.entree("LOCUT_ATTENTE_TOP_CHRONO"),condition[1])
              if   len(condition[1])==2: self.TopChrono=chrono(condition[1],"%S")
              elif len(condition[1])==5: self.TopChrono=chrono(condition[1],"%M:%S")
              elif len(condition[1])==8: self.TopChrono=chrono(condition[1],"%H:%M:%S")
              print(self.projet.lexique.entree("LOCUT_TOP_CHRONO_A"),self.TopChrono.strftime("%H:%M:%S"))
            # Condition 'CHRONO'
            elif condition[0]=="CHRONO":
              hms=condition[1].split('+')[1].split(":")
              (h,m,s)=(0,0,0)
              if   len(hms)==1: s=int(hms[0])
              elif len(hms)==2: (m,s)=(int(hms[0]),int(hms[1]))
              elif len(hms)==3: (h,m,s)=(int(hms[0]),int(hms[1]),int(hms[2]))
              t=self.TopChrono+datetime.timedelta(hours=h,minutes=m,seconds=s)
              print(self.projet.lexique.entree("LOCUT_ATTENDRE"),t.strftime("%H:%M:%S"))
              while t>datetime.datetime.today():
                continue

            # Index de l'action
            if self.projet.execution in ['P','S','X','K']: txtIndex="act%d"%numAction
            else: txtIndex="%s%d"%(idactLoop,numAction)+txtActLoop

            # Ancre de l'action
            txtPoint='.'
            if self.projet.execution=='R':
              txtIndexAnchor="[anchor=%s#%s]%s[/anchor]"%(fiche.replace('/','').replace('\\',''),txtIndex,txtIndex)
            elif cmd['RAPPORT']==-1:
              txtIndexAnchor=''
              txtIndex=''
              txtPoint=''
            else: txtIndexAnchor=txtIndex

            # Affichage de la commande à lancer
            if cmd['RAPPORT']!=-1 or self.projet.execution=='S':
              if self.projet.rapportManuel or self.projet.enregistrement!=0:
                nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
                if 'COMMENTAIRE' in cmd:# and self.projet.debug==0:
                  nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,txtIndex+txtPoint+nngsrc.services.SRV_retraitIndexation(txtComment),espacement=10)
                else:
                  nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,cmd['FONCTION'],espacement=10)
              else:
                if cmd['FONCTION'] is None:
                 # print("HOTFIX:",txtComment.split('|'))
                  nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,"%s : %s"%(self.projet.lexique.entree("MOT_ETAPE"),nngsrc.services.SRV_retraitIndexation(txtComment.split('|')[1])),style="||*",espacement=6,parallel=self.parallel)
                elif 'COMMENTAIRE' in cmd:
                  nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,txtIndex+txtPoint+nngsrc.services.SRV_retraitIndexation(txtComment),style="||",espacement=6,parallel=self.parallel)
                else:
                  if self.projet.debug==0:
                    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,cmd['FONCTION'],espacement=10,parallel=self.parallel)

            # Lancement de la commande de l'action
            #scenario=self.variables[nngsrc.services.SRV_IndexVariable('SCENARIO',self.variables)]['VALEUR']
            #scenario=scenario.split(self.projet.SEPARATEUR_SUFFIXE)[0]
            elementsFiche=(rang,fiche,self.reference,tagFiche,(txtIndex,cmd['TAG']),description,faits_techniques)
            resultatCommande=self.ChoixCommande(fonction,elementsFiche,continuer,cmd,txtIndexAnchor,ident,cmdCondition,actLoopFT)

            # Traitement du resulat de la commande
            if cmd['RAPPORT']!=-1 or self.projet.execution=='S':
              if (self.projet.execution=='R') and resultatCommande!=[]:
                (retourRes,_,gestionFT,elemMatTrac)=resultatCommande[0]
                if gestionFT is None:
                  nngsrc.services.SRV_afficher_droite_texte(self.projet.uiTerm,self.projet.debug,'',style="*r",longueur=0,parallel=self.parallel)
                  gestionFT=(_,_,"",_,_,_,_,_)
                if retourRes is not None:
                  gestionFTFiche.append(gestionFT)
                  if elemMatTrac:
                    matTracabilite.append(elemMatTrac)
                  (_,_,libelleFT,_,_,_,_,_)=gestionFT

                  (_,_,_,retour,_,coul)=nngsrc.services.SRV_codeResultat(retourRes,self.constantes,self.projet.DOCUMENT_LANGUE)
                  styleRetour=nngsrc.services.SRV_styleResultat(retourRes,self.constantes)

                  if self.projet.rapportManuel or self.projet.enregistrement!=0:
                    nngsrc.services.SRV_afficher_droite_texte(self.projet.uiTerm,self.projet.debug,retour,style=styleRetour,espacement=ESPACEMENT-len(libelleFT))
                  else:
                    nngsrc.services.SRV_afficher_droite_texte(self.projet.uiTerm,self.projet.debug,"..."+retour,style="*%s"%coul,longueur=14,parallel=self.parallel)
                    if len(libelleFT)>0:
                      nngsrc.services.SRV_afficher_droite_texte(self.projet.uiTerm,self.projet.debug,libelleFT,style="*E",longueur=20,parallel=self.parallel)
                    nngsrc.services.SRV_afficher_droite_texte(self.projet.uiTerm,self.projet.debug,'',style="*r",longueur=0,parallel=self.parallel)
              elif self.projet.execution in ['S','P','C'] and resultatCommande!=[]:
                nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'',style='*r',longueur=0,parallel=self.parallel)
                (retourRes,_,gestionFT,elemMatTrac)=resultatCommande[0]
                if elemMatTrac and cmd['RAPPORT']!=-1:
                  matTracabilite.append(elemMatTrac)
              else:
                (retourRes,_,gestionFT,elemMatTrac)=resultatCommande[0]
                if elemMatTrac and cmd['RAPPORT']!=-1:
                  matTracabilite.append(elemMatTrac)
                if self.projet.debug!=1:
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'',style='r',parallel=self.parallel)

            if DebutBoucle:
              texteBoucleDeb=self.rapport.Valid_DebutBoucle(('',''),identLoop,txtLoop,conditionLoop,varList,self.projet.execution)

            if self.projet.debug>2:
              if resultatCommande:
                (r,_,_,_)=resultatCommande[0]
                print("DBG>  RAPPORT :")
                print("DBG>    Param =",parametre)
                print("DBG>    Res   =",r)

          # 3- Etablissement du retour à 1 en cas de non mode rapport R
          if not self.projet.execution=='R':
            if resultatCommande:
              (retour,rapport,gft,mat)=resultatCommande[0]
              resultatCommande[0]=(1,rapport,gft,mat)

          # 4- Mise à jour de la condition
          if ident!='':
            if not resultatCommande:
              listConditions[ident]=0
            else:
              (listConditions[ident],_,_,_)=resultatCommande[0]
            listConditions[ident]=nngsrc.services.SRV_codeResultat(listConditions[ident],self.constantes,self.projet.DOCUMENT_LANGUE)[2]

          # 5- Etablissement de la fin de boucle
          if self.loop!="1" and len(subListCmd)==0 and (self.projet.execution in ['P','S','X','K']):
            FinBoucle=True

          # 6- Gestion de l'arret de la boucle
          if cmd['BOUCLE'] is not None:                # Si on est dans une boucle d'action :
            actLoop=cmd['BOUCLE'][0]!=retour           #      l'arrêt de la boucle est la comparaison au retour
            cptActLoop=cptActLoop+1                    #      compteur incrémenté
            if cptActLoop>cmd['BOUCLE'][1]:            #      si l'iteration max de la boucle est atteint
              actLoop=False                            #        l'arrêt de la boucle est validé
          else:
            actLoop=False                              # Sinon l'arrêt de la non boucle est validé

          # 7- Si on n'est pas en mode Rapport, l'arrêt de la boucle est validé (1 seule itération)
          if not self.projet.execution=='R':
            actLoop=False

          # 8- Reajustement du résultat de la commande en fonction du mode
          r=self.constantes['STATUS_OK']
          if resultatCommande:
            (r,_,g,m)=resultatCommande[0]
            if not self.ResultatDansRapport(cmd,r):
              resultatCommande[0]=(r,('',''),g,m)
            else:
              if DebutBoucle:
                (r,d,g,m)=resultatCommande[0]
                resultatCommande[0]=(r,(texteBoucleDeb[0]+d[0],texteBoucleDeb[1]+d[1]),g,m)
              if FinBoucle:
                (r,d,g,m)=resultatCommande[0]
                resultatCommande[0]=(r,(d[0]+texteBoucleFin[0],d[1]+texteBoucleFin[1]),g,m)

          # 9- Gestion de l'ajout du résultat de l'action au résultat global
          if DebutBoucle or FinBoucle or self.ResultatDansRapport(cmd,r):
            # Ajout du résultat et incrémentation du numéro de l'action
            result+=resultatCommande
            if not actLoop and cmd['RAPPORT']!=-1 and cmd['FONCTION'] is not None:
              numAction+=1

            # Si le blocage de la validation est autorisé
            if self.projet.BLOQUAGE:
              if resultatCommande:
                (retour,_,_,_)=resultatCommande[0]
                if retour in [self.constantes['STATUS_PB'],self.constantes['STATUS_AV']]:
                  if cmd['FT'] in self.projet.BLOQUAGE:
                    if self.projet.debug>1:
                      texte=self.projet.lexique.entree("LOCUT_ARRET_SUR_FT")%self.projet.lexique.entree("MOT_NIVEAU_CRITIQUE_%d"%cmd['FT'])
                      nngsrc.services.SRV_afficher_centrer_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*Er",parallel=self.parallel)
                    continuer=False
                  if cmd['FT'] in self.projet.ARRET_FT:
                    arret=True

            # Test d'arret
            if self.projet.ARRET:
              continuer=False
              arret=True

          # 10- RAZ de la notion de boucle
          DebutBoucle=False
          FinBoucle=False
          texteBoucleDeb=('','')
          texteBoucleFin=('','')

          # 11- Attente pas à pas.
          if self.projet.PasAPas is not None:
            if os.path.exists(self.projet.PasAPas):
              nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_PAS_A_PAS"),"CLAV_ENTREE")

      # Incrementation du compteur de la boucle
      count+=pasloop
      self.variables[nngsrc.services.SRV_IndexVariable("ITER_ACTIONS",self.variables)]['VALEUR']=str(count)
      # Demande de l'arrêt ou la continuité de la boucle en cas de boucle infinie
      if forever:
        if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU)
        resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_CONTINUER_BLOC_ACTIONS")%libelleLoop,attente=self.projet.lexique.entree("ATTENTE_OUINON"))
        if resultat.lower()=="n":
          forever=False

      if conditionLoop is not None:
        listeVariables=nngsrc.services.SRV_listeVar(self.variables)
        for (opestring,valstring,_) in listeVariables:
          aevaluer=nngsrc.services.SRV_RemplacerVariable(conditionLoop,listeVariables)
        valCondition=nngsrc.services.SRV_ReduireCondition(aevaluer,{})=='T'

    if DebutBoucleModeR:
      texteBoucleFin=self.rapport.Valid_FinBoucle(('',''),identLoop,self.projet.execution)
      result+=[(None,texteBoucleFin,None,None)]

    contexte=faits_techniques,exigences,gestionFTFiche,matTracabilite,result
    return numAction,continuer,arret,contexte

  #---------------------------------------------------------------------------------
  def TraiterCommandes(self,rang,fiche,tagFiche,externLoop,description,commandes,arret,log):
    result=[]
    numAction=1
    identLoop=0
    faits_techniques=''
    exigences=''
    continuer=not arret
    if externLoop!='':
      externLoop=externLoop[1:]
    num=2
    gestionFTFiche=[]
    matTracabilite=[]

    # Gestion des conditions
    listConditions={}
    for v in self.variables:
      if v['TYPE']=='ID':
        listConditions[v['NOM']]=v['VALEUR']

    # Boucles sur les actions d'un bloc d'actions
    # ou sur une seule action
    contexte=faits_techniques,exigences,gestionFTFiche,matTracabilite,result
    while commandes:
      subListCmd=commandes.pop(0)
      iterationLoop=subListCmd['nbiter']      # Nombre d'iterations de la boucle
      libelleLoop=subListCmd['libelle']       # Libelle de la boucle
      conditionLoop=subListCmd['condition']   # Condition
      cmd=subListCmd['actions']               # Commande
      varList=cmd[0]['LISTE']
      if varList is None:
        varList=[]

      # Gestion des tags
      for c in cmd:
        self.GestionTags(c,fiche)

      # Gestion des Variables
      tabNbParamList=[]
      tabIndexlist=[]
      for listTmp in varList:
        nomVar=listTmp[0]
        nbVar=len(listTmp)-1
        tabNbParamList.append(nbVar)
        tabIndexlist.append(0)
#        print 'DBG-newVar>',listTmp
        v=nngsrc.services.SRV_IndexVariable(nomVar,self.variables)
        if v==-1:
          self.variables.append(dict({'NOM':nomVar,'VARENVPRJ':None,'INDEXER':False,'CONTEXTUEL':True,'INDEX':None,'VALEUR':'','UTILE':False,'TYPE':'VARIABLE','PERSISTANCE':False,'COMMENTAIRE':''}))
      tabVariables=varList,tabNbParamList,tabIndexlist

      # Gestion des boucles d'actions
      gestBoucles=self.GestionBouclesActions(iterationLoop,libelleLoop,conditionLoop,externLoop,numAction,identLoop)
      identLoop,_,_,_,_,_,_,_,_,_,_,_,_,_=gestBoucles

      # Exécution des actions
      numAction,continuer,arret,contexte=self.TraitementBlocActions(rang,fiche,tagFiche,description,numAction,continuer,arret,gestBoucles,subListCmd['actions'],tabVariables,listConditions,contexte)

      # RAZ de la boucle éventuelle
      self.loop="0"

      # Gestion de la création de l'action en mode à la volée
      if not arret and self.projet.enregistrement!=0:
        nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
        (num,self.actionVolee)=nngsrc.fichesXML.creerAction(num,self.projet,self.repFiches,fiche)
        if num is not None:
          commandes.append(self.actionVolee)

    # Enregistrement de l'action en mode à la volée
    if self.projet.enregistrement!=0:
      nngsrc.fichesXML.terminerCreerFiche(self.repFiches,fiche,self.projet)

    numAction-=1
    faits_techniques,exigences,gestionFTFiche,matTracabilite,result=contexte
    return result,exigences,faits_techniques,arret,gestionFTFiche,matTracabilite,numAction

  #---------------------------------------------------------------------------------
  def LancementFiche(self,rang,ficheTag,count,arret,nbloop,loop,nbaffloop):
    txtLoop=None
    extLoop=''
    ficheTag=ficheTag.split('§')
    fiche=ficheTag[0]
    tagFiche=''
    self.qualite.reference=self.reference
    if len(ficheTag)>1:
      tagFiche=" §%s"%ficheTag[1]
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,'%s > %s %s%s'%(time.strftime("%H:%M:%S",time.localtime()),self.projet.lexique.entree('LOCUT_LANCEMENT_DE'),nngsrc.services.SRV_Encodage(fiche,"Nom de la fiche"),tagFiche),style="Or",parallel=self.parallel)
    if self.projet.rapportManuel:
      nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
      nngsrc.services.SRV_afficher_centrer_texte(self.projet.uiTerm,self.projet.debug,"%s%s"%(nngsrc.services.SRV_Encodage(fiche,"Nom de la fiche").replace('/',' - ').upper(),tagFiche),style="||Or")

    # dans la suite fiche contient le nom de la fiche sans l'extension '.xml'
    if os.path.splitext(fiche)[1] == '.xml':
      fiche=os.path.splitext(fiche)[0]

    repertFiche=os.path.dirname(fiche)

    if nbloop>1 or (loop=='n' and not self.projet.execution in ['P','S','X','K']):
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Itération %d"%count,style="Wr",parallel=self.parallel)
      extLoop="_x%d"%count
      txtLoop="%d"%count

    if os.access("execute_fin_%d.flg"%rang,os.F_OK):
      nngsrc.services.SRV_detruire("execute_fin_%d.flg"%rang,timeout=5,signaler=True)
    if not self.projet.execution in ['X','K']:
      self.razVariables(False)
    index=nngsrc.services.SRV_IndexVariable("ITER_FICHE", self.variables)
    self.variables[index]['VALEUR']=str(count)
    self.variables[index]['UTILE']=self.projet.execution in ['R','C']

    # Chargement ou creation a la volee de la fiche
    if self.projet.enregistrement==0:
      chargement=nngsrc.fichesXML.chargerFicheXML(self.dictionnaire,self.repFiches,fiche.split('§'),self.variables,self.jeuxDeDonnees,self.projet,self.parallel)
    else:
      (num,chargement)=nngsrc.fichesXML.creerFicheXML(self.dictionnaire,self.repFiches,fiche,self.variables,self.projet)

    if chargement is not None:
      (fichesvg,description,self.variables,self.jeuxDeDonnees,commandes,dictElements,self.rapportActions)=chargement
      if commandes is None:
        nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : aucune action !",style="Er",parallel=self.parallel)

      # Construction des fichiers du rapport de la fiche
      self.rapport.Valid_InitRapportFiche(self.rapportActions,os.path.basename(self.reference),rang,fiche+extLoop,tagFiche,repertFiche,txtLoop,nbaffloop)

      # Traitement des commandes d'une fiche
      #self.variables[nngsrc.services.SRV_IndexVariable("ITER_FICHE",self.variables)]['VALEUR']=str(count)
      traitementCommandes=self.TraiterCommandes(rang,fiche+extLoop,tagFiche,extLoop,description,commandes,arret,self.projet.log)
      (result,exigences,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes

      # Redige le rapport en rajoutant les exigences aux exigences globales
      (entite,methode,typo,nature,commentaire,exigencesglobales)=description
      exigences=nngsrc.services.SRV_traiterExigences(exigences,exigencesglobales)
      description=(entite,methode,typo,nature,commentaire,exigences)
      criticite=0
      if self.rapportActions!=-1:
        result,criticite=self.rapport.Valid_RapporterResultat(rang,result,fiche+extLoop,tagFiche,description,self.variables) # TODO : clash sur result car contient gestionFT au lieu de LibelleFT
      traitementCommandes=(result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)
     # print("Lancement = OK !!!!")
      return rang,traitementCommandes,extLoop,arret,self.variables
   # print("Lancement = KO !!!!")
    return None

  #---------------------------------------------------------------------------------
  def PostLancementFiche(self,fiche,resJobs,rang,precedChapitre,statusSection,criticiteSection):
#    print "DBG> --------------------------------------------------"
#    print "DBG> Post-EXECUTION DE",fiche
#    print "DBG> statusSection,precedRepertFiche,arret"
#    print "DBG>",resJobs
#    (traitementCommandes,_,arret)=resJobs
#    (result,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
#    print "DBG> result         ",result
#    print "DBG> exigences      ",exigences
#    print "DBG> entite         ",entite
#    print "DBG> faitstechniques",faitstechniques
#    print "DBG> gestionFTfiches"
#    for e in gestionFTfiches:
#      (FT,numBase,libelleFT,ft,elem)=e
#      print "DBG>       FT/FT     ",FT
#      print "DBG>       FT/numBase",numBase
#      print "DBG>       FT/libelle",libelleFT
#      print "DBG>       FT/ft     ",ft
#      print "DBG>       FT/elem   ",elem

    # Recuperation des elements
    (_,traitementCommandes,txtLoop,arret,variables)=resJobs
    (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes

    # dans la suite fiche contient le nom de la fiche sans l'extension '.xml'
    if os.path.splitext(fiche)[1] == '.xml':
      fiche=os.path.splitext(fiche)[0]

    # Reprise du bloc de code de LancementFiche dans le cas de parallelisation
    if self.projet.nivRapport==2:
      chapitre=nngsrc.services.SRV_Variables("[SCENARIO]",self.variables)
    else:
      chapitre=os.path.dirname(fiche)

    # construction de l'entree dans le menu
    #print("DBG1>")
    #print("Chapitre",chapitre)
    #print("précédent",precedChapitre)
    #print("statusSection",statusSection)
    if self.rapportActions!=-1 and chapitre!=precedChapitre:
      self.rapport.Valid_InitChapitre(chapitre) # HTML,TEX
      precedChapitre=chapitre
      #print("DBG1:",precedChapitre)

    # Recupere ses exigences et ft couverts
    self.qualite.QUAL_TraiterListes(faitstechniques,exigences,entite,result,self.constantes)

    (self.listeEnv,self.environnement)=self.TraiterEnvironnement(self.listeEnv,self.environnement,self.variables,variables)

    # Fin du rapport de fiche
    if os.access("execute_fin_%d.flg"%rang,os.F_OK):
      EXECUTE_FIN=1
      nngsrc.services.SRV_detruire("execute_fin_%d.flg"%rang,timeout=5,signaler=True)
    else:
      EXECUTE_FIN=0
    if result==0:
      statusSection.append(0)
    else:
      statusSection.append(result*EXECUTE_FIN)
    criticiteSection.append(criticite)
    tableFT=None
    if self.rapportActions!=-1:
      tableFT=self.rapport.html.Valid_TerminerRapportFiche(result,criticite,EXECUTE_FIN,"%s-%s-%s"%(os.path.basename(self.reference),str(rang),fiche+txtLoop),gestionFTfiches,self.rapportActions)
#     print "DBG>",tableFT
    self.qualite.QUAL_CouvertureMatrice(matTracabilite,rang,fiche,tableFT,gestionFTfiches,self.parallel)

    if self.rapportActions!=-1:
      self.rapport.Valid_TerminerRapportFiche(result,criticite,nbActions,EXECUTE_FIN,(os.path.basename(self.reference),str(rang),fiche+txtLoop),self.rapportActions) # HTML,TEX

#    print "DBG> --------------------------------------------------"
    return precedChapitre,statusSection,criticiteSection

  #---------------------------------------------------------------------------------
  def Scheduler(self,lot,repImage,nbControles,IDcahier):
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-- Nangu v"+self.version+"--------------------------",style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"%s      : (%s)"%(self.projet.lexique.entree('LOCUT_GENERATION_RAPPORT')%"HTML",self.projet.lexique.entree('MOT_TOUJOURS')),style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"%s     : %s"%(self.projet.lexique.entree('LOCUT_GENERATION_RAPPORT')%"LaTeX",self.projet.lexique.ouinon(self.projet.RAPPORT_TEX)),style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"%s : %s"%(self.projet.lexique.entree('LOCUT_MODE_EXECUTION'),self.projet.lexique.entree('MODE_%s'%self.projet.execution)),style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Ir")

    # Si le serveur Sikuli est reste actif sur un test precedent on le stoppe
    if self.isStartSikuli:
      self.fonctionsIHM.StopSikuli()

    #=========================================================
    # On ne rigole plus, on retient sa respiration, et on y va
    arret=False
    if type(lot)==dict:
      clefs=sorted(lot.keys())
#      clefs.sort()
#      self.projet.complexe=1
    else:
      clefs=[0]
      lot={0:[lot,'','']}
    statusSection=[]
    criticiteSection=[]
    precedChapitre=''
    self.reference="-Aucune-Reference-"
#    print "DBG>init-section"
    for cle in clefs:
      lelot=lot[cle]
      self.ficheslot=lelot[0]
      if lelot[2]!='':
        self.variables[nngsrc.services.SRV_IndexVariable('SCENARIO',self.variables)]['VALEUR']=lelot[2]
      if self.projet.enregistrement!=0:                            #v8.2rc2 Enregistrement à la volée
        self.ficheslot=["%s_%d"%(self.projet.nomFicheALaVolee,self.projet.enregistrement)]
        self.projet.enregistrement+=1
      elif not self.ficheslot:
        nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"Lot %s '%s' vide"%(lelot[1],lelot[2]),echo=False)
      if cle>0:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,u'%s >> %s [%s] -%s-'%(time.strftime("%H:%M:%S",time.localtime()),self.projet.lexique.entree("MOT_RAPPORT"),lelot[2],nngsrc.services.SRV_Encodage(lelot[1],"Titre du rapport")),style="Or")
        self.reference=lelot[2].split(self.separateurSuffixe)[0]
        self.rapport.Valid_DebutPartie(os.path.basename(self.reference),self.separateurSuffixe,lelot[1],lelot[2],self.typePRJsimple,self.repScenarios)
      else:
        self.reference="Contrôles"
        self.rapport.Valid_DebutPartie(os.path.basename(self.reference),self.separateurSuffixe,None,None,self.typePRJsimple,self.repScenarios)

      numFicReport=-1
      while self.ficheslot:
        numFicReport+=1
        fiche=self.ficheslot.pop(0)
        # Gestion des boucle dans le ficher de fiche .lst
        nbloop=1       # Une iteration par défaut
        nbaffloop=None # chaine permettant d'afficher le nombre de boucles
        loop=''        # chaine lue du nombre de boucles
        condition=None # condition sur un bloc de fiches
        bclFiche=None  # boucle sur une liste de valeurs
        nbcpus=1       # nombre de cpus pour parallelisation
        variabFiche=[] # variables sur une fiche

        # Bloc de fiches : boucle, condition, parallelisation
        if type(fiche)==tuple:
          (iter_cond,listeFiches)=fiche
          if iter_cond[0]=='(':
            condition=iter_cond
          elif iter_cond[0]=='{':
            iter_cond=iter_cond.replace('{','').replace('}','')
            bclFiche=(iter_cond.split('=')[0],iter_cond.split('=')[1].split(','))
            loop=str(len(iter_cond.split('=')[1].split(',')))
          elif iter_cond[0]=='|':
            nbcpus=int(iter_cond.replace('|','').replace('|',''))
          else:
            loop=iter_cond
        else:
          if len(fiche.rsplit(" ")) > 1:
            loop=fiche.rsplit(" ")[1]
            fiche=fiche.rsplit(" ")[0]
            # Recherche d'une boucle
            if loop[0]=='x':
              loop=loop[1:]
            elif loop[0]=='{':
              params=loop.replace('{','').replace('}','').split('|')
              for p in params:
                variabFiche.append((p.split('=')[0],p.split('=')[1]))
              loop=''
            else:
              nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : nombre de boucle",style="Er")
              exit (-1)

        if loop!='':
            if loop=='n':
              nbloop=-1
              if self.projet.execution=='C':
                if self.projet.SON_ACTIVATION: nngsrc.servicesSons.SRVson_Alarme(self.projet.SON_FICHIER_ALARME,nngsrc.services.varNANGU)
                valeur=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,"Veuillez entrer le nombre d'itérations sur la fiche souhaité ","SAISIE")
                nbloop=int(valeur)
              else:
                nbaffloop='n'
            else :
              try:
                nbloop=int(loop)
              except:
                nngsrc.services.SRV_Terminal(None,self.projet.debug,self.projet.log,"ERREUR : nombre de boucle",style="Er")
                exit (-1)

        if nbloop>1 and (self.projet.execution in ['P','S','X','K']):
          nbaffloop=str(nbloop)
          nbloop=1

        #==============================================
        # TOUT SE JOUE ICI !!!
        if nbloop==-1:
          forever=True
        else :
          forever=False
        count=1

        # parallelisation des fiches /-|<nbcpus>|
        if nbcpus!=1:

          # Parallelisation
          self.parallel=True
          self.fonctions.parallel=True
          self.rapport.html.parallel=True
          svgnumFT=self.qualite.newNumFT
          resJobs={}
          start_time = time.time()
          if simulationPP: nbcpus=1
          else:
            if nbcpus==0:
              #job_server = pp.Server(ppservers=ppservers,secret="password")
              listeJobs=mpJobs.mpJOBS(mpJobs.jobsPROCESS)
            else:
              #job_server = pp.Server(nbcpus, ppservers=ppservers,secret="password")
              listeJobs=mpJobs.mpJOBS(mpJobs.jobsPROCESS,nbcpus)
            nbcpus=listeJobs.nbCPU
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"----// Début de parallélisation sur %d CPUs //-------------------------------"%nbcpus,style="Ir")

          if simulationPP:
            lRang=range(len(listeFiches))
            lRang.reverse()
            for rang in lRang:
              resJobs[rang]=self.LancementFiche(rang,listeFiches[rang],count,arret,nbloop,loop,nbaffloop)
          else:
            fonctions=()
            modules=("time","nngsrc.services","nngsrc.servicesXML","fichesXML",)
            #jobs = [(rang, job_server.submit(self.LancementFiche,(rang,listeFiches[rang],count,arret,nbloop,loop,nbaffloop,), fonctions, modules)) for rang in range(len(listeFiches))]
            for rang in range(len(listeFiches)):
              listeJobs.Ajouter(self.LancementFiche,(rang,listeFiches[rang],count,arret,nbloop,loop,nbaffloop,),modules)

            resJobs=listeJobs.Executer(False)
            print(resJobs)
            for r in resJobs:
               rang, traitementCommandes,extLoop,arret,variables=r.get()
            print("Resultat d'exécution sous POOL",[r for r in resJobs])
#            print("Resultat d'exécution sous PROCESS",[(r[0],r[1]) for r in resJobs])

#            for rang, job in jobs:
#              resJobs[rang]=job()
#            job_server.print_stats()
            #print job_server.get_stats()
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"----// Fin de parallélisation //-------------------------------------------",style="Ir")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"----// Durée %f s"%(time.time()-start_time),style="Ir")
          self.qualite.newNumFT=svgnumFT
          for rang in range(len(listeFiches)):
            (_,traitementCommandes,_,arret,_)=resJobs[rang]
            (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
            (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(listeFiches[rang],resJobs[rang],rang,precedChapitre,statusSection,criticiteSection)
          self.parallel=False
          self.fonctions.parallel=False
          self.rapport.html.parallel=False
        # boucle sur des fiches /-{<variable>=<liste>}
        elif bclFiche:
          (variable,valeurs)=bclFiche
          v=nngsrc.services.SRV_IndexVariable(variable,self.variables,False)
          if v==-1:
            utile=self.projet.execution in ['R','C']
            self.variables.append(dict({'NOM':variable,'VARENVPRJ':None,'INDEXER':False,'CONTEXTUEL':True,'INDEX':None,'VALEUR':'','UTILE':utile,'TYPE':'VARIABLE','PERSISTANCE':True,'COMMENTAIRE':'','FICHES':[],'JDD':None}))
          for uneValeur in valeurs:
            for n in range(len(self.variables)):
              if self.variables[n]['NOM']==variable:
                self.variables[n]['VALEUR']=uneValeur
            for unefiche in listeFiches:
              resJobs=self.LancementFiche(0,unefiche,count,arret,nbloop,loop,nbaffloop)
              (_,traitementCommandes,_,arret,_)=resJobs
              (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
              (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(unefiche,resJobs,0,precedChapitre,statusSection,criticiteSection)
            count+=1
            if arret or self.projet.execution in ['P','S','X','K']:
              forever=False
            if forever:
              resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_VOULEZ_VOUS_CONTINUER"),attente=self.projet.lexique.entree("ATTENTE_OUINON"))
              if resultat.lower()=="n":
                forever=False
            if self.projet.rapportManuel:# or self.projet.enregistrement!=0:
              nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
              print()
            if not arret and self.projet.enregistrement!=0:
              if self.actionVolee=="f":
                self.ficheslot.append("%s_%d"%(self.projet.nomFicheALaVolee,self.projet.enregistrement))
                self.projet.enregistrement+=1
        # Fiche paramétrée {var=val|var=val...}
        elif variabFiche!=[]:
          for (variable,valeur) in variabFiche:
            v=nngsrc.services.SRV_IndexVariable(variable,self.variables,False)
            if v==-1:
              utile=self.projet.execution in ['R','C']
              self.variables.append(dict({'NOM':variable,'VARENVPRJ':None,'INDEXER':False,'CONTEXTUEL':False,'INDEX':None,'VALEUR':'','UTILE':utile,'TYPE':'VARIABLE','PERSISTANCE':True,'COMMENTAIRE':'','FICHES':[],'JDD':None}))
              v=nngsrc.services.SRV_IndexVariable(variable,self.variables,False)
            self.variables[v]['VALEUR']=valeur
          resJobs=self.LancementFiche(numFicReport,fiche,count,arret,nbloop,loop,nbaffloop)
          if resJobs is None:
            traitementCommandes=None
            arret=True
            variables=[]
          else:
            (_,traitementCommandes,_,arret,variables)=resJobs
            (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
            (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(fiche,resJobs,numFicReport,precedChapitre,statusSection,criticiteSection)
        # Autre...
        else:
          # bouche <fiche> x<iterateur> ou /-{x<iteration>}
          while forever or count<=nbloop:
            if type(fiche)!=tuple:
              resJobs=self.LancementFiche(numFicReport,fiche,count,arret,nbloop,loop,nbaffloop)
              if resJobs is None:
                traitementCommandes=None
                arret=True
                variables=[]
              else:
                (_,traitementCommandes,_,arret,variables)=resJobs
                (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
                (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(fiche,resJobs,numFicReport,precedChapitre,statusSection,criticiteSection)
            else:
              # condition /-(<variable><operateur><valeur>)
              if condition:
                listeVariables=nngsrc.services.SRV_listeVar(self.variables)
                for (opestring,valstring,_) in listeVariables:
                  aevaluer=nngsrc.services.SRV_RemplacerVariable(condition,listeVariables)
                lancer=nngsrc.services.SRV_ReduireCondition(aevaluer,{})
                if lancer=='T':
                  for unefiche in listeFiches:
                    resJobs=self.LancementFiche(0,unefiche,count,arret,nbloop,loop,nbaffloop)
                    (_,traitementCommandes,_,arret,_)=resJobs
                    (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
                    (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(unefiche,resJobs,0,precedChapitre,statusSection,criticiteSection)
              else:
                for unefiche in listeFiches:
                  resJobs=self.LancementFiche(0,unefiche,count,arret,nbloop,loop,nbaffloop)
                  (_,traitementCommandes,_,arret,_)=resJobs
                  (result,criticite,exigences,entite,faitstechniques,arret,gestionFTfiches,matTracabilite,nbActions)=traitementCommandes
                  (precedChapitre,statusSection,criticiteSection)=self.PostLancementFiche(unefiche,resJobs,0,precedChapitre,statusSection,criticiteSection)
            count+=1
            if arret or self.projet.execution in ['P','S','X','K']:
              forever=False
            if forever:
              resultat=nngsrc.services.SRV_AttenteOperateur(self.projet.uiTerm,self.projet.debug,self.projet.lexique.entree("LOCUT_VOULEZ_VOUS_CONTINUER"),attente=self.projet.lexique.entree("ATTENTE_OUINON"))
              if resultat.lower()=="n":
                forever=False
            if self.projet.rapportManuel:# or self.projet.enregistrement!=0:                            #v8.2rc2 Enregistrement à la volée
              nngsrc.services.SRV_afficher_separateur(self.projet.uiTerm,".")
              print()
            if not arret and self.projet.enregistrement!=0:                                              #v8.2rc2 Enregistrement à la volée
              if self.actionVolee=="f":
                self.ficheslot.append("%s_%d"%(self.projet.nomFicheALaVolee,self.projet.enregistrement))         #v8.2rc2 Enregistrement à la volée
                self.projet.enregistrement+=1
        #==============================================

      #print("DBG2>")
      #print("précédent",precedChapitre)
      #print("statusSection",statusSection)
      if precedChapitre!='':
        if self.constantes['STATUS_PB'] in statusSection:
          codeStatusSection=self.constantes['STATUS_PB']
        elif self.constantes['STATUS_AV'] in statusSection:
          codeStatusSection=self.constantes['STATUS_AV']
        elif self.constantes['STATUS_AR'] in statusSection:
          codeStatusSection=self.constantes['STATUS_AR']
        elif self.constantes['STATUS_NT'] in statusSection:
          codeStatusSection=self.constantes['STATUS_NT']
        else:
          codeStatusSection=self.constantes['STATUS_OK']
        finDuChapitre=self.projet.nivRapport==2 and not self.typePRJsimple
        #print("DBG-sheduler> Valid_TerminerChapitre")
        #print("DBG-sheduler> finDuChapitre",finDuChapitre)
        disp=nngsrc.services.SRV_codeResultat(codeStatusSection,self.constantes,self.projet.DOCUMENT_LANGUE)
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_SECTION_TERMINEE')%disp[3],style="r")
        self.rapport.Valid_TerminerChapitre(os.path.basename(self.reference),precedChapitre,codeStatusSection,criticiteSection,finDuChapitre)
        #print("DBG-sheduler> > RAZ statusSection")
        statusSection=[]
        criticiteSection=[]

    # On ferme le fichier de remarques
    if self.projet.nbRemarque!=0:
      f=open(self.projet.fichier_remarque,"a",encoding='utf-8')
      f.write("[/ol]\n")
      f.close()
    # Si actif on stoppe le serveur Sikuli
    if self.isStartSikuli:
      self.fonctionsIHM.StopSikuli()

    # On finalise les rapports
    resultat=self.rapport.Valid_Terminer(self.reference,self.rang,self.listeKeyEnv,self.environnement,self.jeuxDeDonnees,repImage,nbControles,IDcahier)

    self.projet.Sauvegarde(self.configFile,self.repFiches)
    if self.projet.ACTIVATION_BASEFT:
      self.qualite.TODOecrireXML(self.projet.PRODUIT_BASEFT,self.projet.lexique,self.projet.ajoutBaseFT,self.qualite.newNumFT)

    self.Avertissements(resultat)

    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"---- %s --------------------------------------------------------"%self.projet.lexique.entree('LOCUT_FIN_EXECUTION'),style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"%s : %s"%(self.projet.lexique.entree('LOCUT_RAPPORT_REFERENCE'),self.rapport.ficRapport),style="Ir")

    if self.projet.debug>1:
      print("Les encodages :")
      print("   default locale  =",locale.getdefaultlocale())
      print("   default encoding=",sys.getdefaultencoding())
      print("   stdout encoding =",sys.stdout.encoding)

  #-----------------------------------------------------------------------------------
  ## Fonction d'envoi de mail de resultat
  def mail(self,message,resultat,qualite=False):
    serveur = nngsrc.smtp.ServeurSMTP(self.projet.MAIL_SMTP,25)
    exped=self.projet.MAIL_EXPEDITEUR
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()
    sujet="[Nangu] %s : Bilan de validation du %s"%(self.projet.PRODUIT_NOM,t.strftime("%d/%m/%Y %H:%M"))
    envoi=False
    to=[]
    pjointes=[]
    if not qualite:
      if self.projet.MAIL_DESTINATAIRE1[0] and (self.projet.MAIL_DESTINATAIRE1[1] or self.projet.MAIL_DESTINATAIRE1[1]==resultat):
        to.append(self.projet.MAIL_DESTINATAIRE1[0])
        envoi=True
      if self.projet.MAIL_DESTINATAIRE2[0] and (self.projet.MAIL_DESTINATAIRE2[1] or self.projet.MAIL_DESTINATAIRE2[1]==resultat):
        to.append(self.projet.MAIL_DESTINATAIRE2[0])
        envoi=True
      if self.projet.MAIL_DESTINATAIRE3[0] and (self.projet.MAIL_DESTINATAIRE3[1] or self.projet.MAIL_DESTINATAIRE3[1]==resultat):
        to.append(self.projet.MAIL_DESTINATAIRE3[0])
        envoi=True
    else:
      if self.projet.MAIL_QUALITE[0] and (self.projet.MAIL_QUALITE[1] or self.projet.MAIL_QUALITE[1]==resultat):
        to.append(self.projet.MAIL_QUALITE[0])
        envoi=True
        fichierPJ='CSV.zip'
        ficzip=zipfile.ZipFile(fichierPJ,"w")
        for name in glob.glob(os.path.join(self.repRapport,'R','CSV',self.rapport.csv.ficRapport,"*")):
          ficzip.write(name, os.path.basename(name),zipfile.ZIP_DEFLATED)
        ficzip.close()
        pjointes.append(fichierPJ)
    if envoi:
      # création du mail correctement formaté (en-tête + corps) et encodé
      try:
          message = nngsrc.smtp.MessageSMTP(exped, to, [], [], sujet, message, pjointes, 'ISO-8859-15', 'plain')
          ok=True
      except:
          print(sys.exc_info()[1])
          ok=False

      # envoi du mail et affichage du résultat
      if ok: nngsrc.smtp.envoieSMTP(message, serveur)

      if qualite:
        nngsrc.services.SRV_detruire(fichierPJ,timeout=5,signaler=True)

  #-----------------------------------------------------------------------------------
  ## Fonction d'avertissement de fin de traitements
  # @param resultat : ... d'exécution
  def Avertissements(self,resultat):
    if self.projet.execution=='R':
      if self.projet.SON_ACTIVATION:
        if resultat:
          if self.projet.SON_FICHIER_OK!='': nngsrc.servicesSons.SRVson_playWAV(self.projet.SON_FICHIER_OK)
          else: nngsrc.servicesSons.SRVson_playWAV(os.path.join(nngsrc.services.varNANGU,"ressources","fin_ok.wav"))
        else:
          if self.projet.SON_FICHIER_NOK!='': nngsrc.servicesSons.SRVson_playWAV(self.projet.SON_FICHIER_NOK)
          else: nngsrc.servicesSons.SRVson_playWAV(os.path.join(nngsrc.services.varNANGU,"ressources","fin_nok.wav"))

      if self.projet.MAIL_ACTIVATION:
        message=nngsrc.services.SRV_LireContenuListe(self.rapport.csv.ficBilanMail)
        message='\n'.join(message)
        self.mail(message,resultat)
        self.mail(message,resultat,qualite=True)

  #-----------------------------------------------------------------------------------
  def controleNomemclature(self,listeDocument,fichier,repert=None):
    formater=False
    f=open(fichier,"w",encoding='utf-8')
    # calcul du nombre de separateurs pour sous-reference : NOMBRE_CHAMPS + 3 (-ed-rec-iter) - nombre de champs de PRODUIT_REFERENCE
    nbSeparateurs=self.projet.NOMBRE_CHAMPS+3-len(self.projet.PRODUIT_REFERENCE.split(self.projet.SEPARATEUR_CHAMPS))
    for document in listeDocument:
      if repert is not None:
        document=os.path.join(repert,document)
      contenu=nngsrc.services.SRV_LireContenu(document).split('\n')
      nbSeparSousRef=len(contenu[1].split(self.projet.SEPARATEUR_CHAMPS))-2
      if nbSeparSousRef!=nbSeparateurs:
        f.write("Separateur : %d vs %d attendu dans %s\n"%(nbSeparSousRef,nbSeparateurs,document))
    f.close()
    if not formater: nngsrc.services.SRV_detruire(fichier)
    return formater
  #-----------------------------------------------------------------------------------
  ## Fonction d'établissement du bilan des fichiers
  # @param[out] controleFiches      Fichier liste (.lst) des fiches à contrôler généré par la fonction au regard des scenarios demandés
  # @param listePlans               NC
  # @param lstRapports              Listes officielle de rapports à contrôler à comparer avec nng-analyses/lstRapports.log
  # @param scenariosDemandes=None   Liste des scenarios à traiter
  # @param referenceSubst=None
  # @param editer=True
  # @param comparer=True
  # @param uiTerm=None
  def nangu_BilanFiches(self,controleFiches,lstRapports,scenariosDemandes=None,editer=True,comparer=True,uiTerm=None):
    listeFiches=[]
    listeXml=[]
    listeRapTrouves=[]
    listeRapTrouvesPV={}
    listeRapports=[]
    formaterScenarios=False
    formaterPlans=False
    bilan=False
    codeRetour=nngsrc.services.CODE_OK
    self.projet.uiTerm=uiTerm
    nngsrc.services.SRV_detruire(os.path.join(self.repPlans,"planValidation.pv"))

    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---- Bilan des fiches -----------------------------------------------------",style="Ir")
    fbil=open(os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),"w",encoding='utf-8')

    # Recuperation de la liste des plans et scenarios
    if self.typePRJsimple:
      document="plan"
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-1- Recherche des %ss"%document,style="Ir")
      listeScenarios=[]
      for elem in glob.glob(os.path.join(self.repPlans,'*.pv')):
        listeScenarios.append(elem)
    else:
      document="scénario"
      if scenariosDemandes is None:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-1- Recherche des %ss"%document,style="Ir")
        listeScenarios=[]
        for elem in glob.glob(os.path.join(self.repScenarios,'*')):
          _,extension=os.path.splitext(elem)
          if os.path.isdir(elem):
            for elem2 in glob.glob(os.path.join(elem,'*.lst')):
              listeScenarios.append(elem2)
          elif extension==".lst":
            listeScenarios.append(elem)
        if self.projet.version>=9.0:
          formaterScenarios=self.controleNomemclature(listeScenarios,os.path.join(self.repScenarios,"nomemclature.txt"))
      else:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-1- Recuperation de la liste des %ss demandes"%document,style="Ir")
        for s in range(len(scenariosDemandes)):
          scenariosDemandes[s]=scenariosDemandes[s].split(':')[0]
        listeScenarios=scenariosDemandes
      listeScenarios=sorted(listeScenarios)
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'\n'.join(listeScenarios),style="Or")
      if not listeScenarios:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Pas de %s à étudier, arrêt."%document,style="Er")
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---------------------------------------------------------------------------",style="Ir")
        return nngsrc.services.CODE_ANOMAL
      else:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Liste des %ss établie."%document,style="r")

    # Recherche des fichiers liste dans scenarios
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-2- Recherche des fiches utilisés dans les %ss"%document,style="Ir")

    dicoScenarios={}
    for fichier in listeScenarios:
#      scenario=os.path.basename(fichier).replace('.lst','')
      scenario=fichier.replace(os.path.join(self.repScenarios,''),'').replace('.lst','')
      listeFichesScenarios=[]
      if scenario!="fiches":
        lignes=nngsrc.services.SRV_LireContenuListe(fichier)
        methode="T"
        if self.projet.version>=9.0:
          methode=lignes[self.projet.tailleEntete-1].replace('\n','').replace('\r','').split(' ')[1]
        listeRapTrouvesPV[scenario]=False
        for num in range(self.projet.tailleEntete,len(lignes)):
          fiche=lignes[num].replace('\\','/').split(' ')[0]
          if fiche!='':
            if fiche[0]==';':
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"   -fiche %s inactivée ou commentaire"%fiche[1:],style="Wr")
              codeRetour=nngsrc.services.CODE_AVERTI
            elif fiche[0]!='/' and fiche[0]!='\\':
              listeFichesScenarios.append(fiche.replace('.xml',''))
              if fiche not in listeFiches:
                listeFiches.append(fiche)
              lafiche=os.path.join(self.repFiches,fiche)
              if not os.access(lafiche,os.F_OK):
                bilan=True
                fbil.write("La fiche %s declaree dans %s n'existe pas.\n"%(lafiche,fichier))
                nngsrc.services.SRV_EditerTxt(fichier,editer)
      dicoScenarios[scenario]=(listeFichesScenarios,methode)
  #  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None, "CAHIERS : %s"%listeRapTrouvesPV,style="Or")
  #  if listeRapTrouvesPV.keys():
  #    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Liste des cahiers établie.",style="r")

    listeFiches=sorted(listeFiches)
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'\n'.join(listeFiches),style="Or")
    if not listeFiches:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Pas de fichier liste dans les %ss à étudier."%document,style="Er")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---------------------------------------------------------------------------",style="Ir")
#      return nngsrc.services.CODE_ANOMAL
    else:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Liste des fiches établie.",style="r")

    # Traitement des plans
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-3- Recherche des plans du projet",style="Ir")
    listePlans=[]
    try:
      for elem in glob.glob(os.path.join(self.repPlans,'*')):
        _,extension=os.path.splitext(elem)
        if os.path.isdir(elem):
          for elem2 in glob.glob(os.path.join(elem,'*.pv')):
            listePlans.append(elem2.replace(os.path.join(self.repPlans,''),''))
        elif extension=='.pv':
          listePlans.append(elem.replace(os.path.join(self.repPlans,''),''))
    except:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Répertoire %s incorrect, pas de chargement de plan possible"%self.repPlans,style="Er")
      codeRetour=nngsrc.services.CODE_ERREUR
    listePlans=sorted(listePlans)
    if self.projet.version>=9.0:
      formaterPlans=self.controleNomemclature(listePlans,os.path.join(self.repPlans,"nomemclature.txt"),self.repPlans)
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,'\n'.join(listePlans),style="Or")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Liste des plans établie.",style="r")
    if not listePlans:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    La liste des plans de validation (fichiers .pv dans %s) est vide"%self.repPlans,style="Er")
      codeRetour=nngsrc.services.CODE_ERREUR

    # Edition du plan de validation correspondant
    if not self.typePRJsimple:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-4- Contrôle de la couverture des scenarios par les plans de validation.",style="Ir")
      dicoPlans={}
      for plan in listePlans:
        listePlan=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repPlans,plan))
        # on accede directement aux scenarios du plan : :
        methode="T"
        if self.projet.version>=9.0:
          methode=listePlan[self.projet.tailleEntete-1].replace('\n','').replace('\r','').split(' ')[1]
        for l in range(self.projet.tailleEntete,len(listePlan)):
          listePlan[l]=listePlan[l].replace('.lst','')
          lstPln=listePlan[l]
          if listePlan[l][0]==';':
            lstPln=listePlan[l][1:]
          if lstPln in listeRapTrouvesPV.keys():
            listeRapTrouvesPV[lstPln]=True
        dicoPlans[plan.replace('.pv','')]=(listePlan[self.projet.tailleEntete:],methode)

      # Edition du plan de validation des scenarios non references
      TousTrouves=True
      for num in listeRapTrouvesPV.keys():
        TousTrouves=TousTrouves and listeRapTrouvesPV[num]
      if not TousTrouves:
        p=open(os.path.join(self.repPlans,"planValidation.pv"),"w",encoding='utf-8')
        p.write("SOUS-TITRE\nSOUS-REFERENCE\n")
        if self.projet.version>=9.0:
          p.write("METHODE\n")
        for num in listeRapTrouvesPV.keys():
          if not listeRapTrouvesPV[num]:
            p.write("%s.lst\n" % num)
        p.close()
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Un plan de validation 'planValidation.pv' est créé contenant les scénarios inutilisés.",style="Wr")
        nngsrc.services.SRV_EditerTxt(os.path.join(self.repPlans,"planValidation.pv"),editer)
        codeRetour=nngsrc.services.CODE_AVERTI
      else:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Les scénarios sont tous référencés dans les plans de validation.",style="r")

    # Enregistrement de la matrice Plan/scenarios/fiches ou scenarios/fiches
    fPSF=open(os.path.join(self.repRapport,"nng-analyses","BilanPSF.txt"),"w",encoding='utf-8')
    if self.typePRJsimple:
      for scenario in dicoScenarios:
        lstControles,methScenario=dicoScenarios[scenario]
        for controle in lstControles:
          fPSF.write("%s:%s:%s -.-%s-\n"%("-",os.path.basename(scenario),controle,methScenario))
    else:
      for plan in dicoPlans:
        lstScenarios,methPlan=dicoPlans[plan]
        for scenario in lstScenarios:
          if scenario[0]==';':
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Le scenario %s est commenté dans %s."%(scenario[1:],plan),style="Wr")
          else:
            if not scenario in dicoScenarios:
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Le scenario %s de %s n'existe pas."%(scenario,plan),style="Er")
              codeRetour=nngsrc.services.CODE_ERREUR
            else:
              lstControles,methScenario=dicoScenarios[scenario]
              for controle in lstControles:
                fPSF.write("%s:%s:%s -%s-%s-\n"%(os.path.basename(plan),os.path.basename(scenario),controle,methPlan,methScenario))
    fPSF.close()

    # Enregistrement du fichier liste des fiches à contrôler
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"-5- Enregistrement du fichier liste des fiches à contrôler",style="Ir")
    f=open(controleFiches,"w",encoding='utf-8')
    f.write("SOUS-TITRE\nSOUS-REFERENCE\n")
    if self.projet.version>=9.0:
      f.write("METHODE\n")
    for fiche in listeFiches:
      f.write("%s\n"%fiche)
    f.close()

    fbil.close()
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"=> Effectué sous %s"%controleFiches,style="r")

    # Avertissement
    if formaterScenarios:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Le fichier '%s' doit être consulté, des anomalies de formatage y sont référencées."%os.path.join(self.repScenarios,"nomemclature.txt"),style="Er")
      if codeRetour==nngsrc.services.CODE_OK: codeRetour=nngsrc.services.CODE_AVERTI
    if formaterPlans:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Le fichier '%s' doit être consulté, des anomalies de formatage y sont référencées."%os.path.join(self.repPlans,"nomemclature.txt"),style="Er")
      if codeRetour==nngsrc.services.CODE_OK: codeRetour=nngsrc.services.CODE_AVERTI
    if bilan:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    Le fichier '%s' doit être consulté, des anomalies y sont référencées."%os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),style="Er")
      nngsrc.services.SRV_EditerTxt(os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),editer)
      if codeRetour==nngsrc.services.CODE_OK: codeRetour=nngsrc.services.CODE_AVERTI

    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---------------------------------------------------------------------------",style="Ir")
    return codeRetour

  #---------------------------------------------------------------------------------
  @staticmethod
  def MajMethode(document,methode):
    f=open(document,"r",encoding='utf-8')
    contenu=f.readlines()
    f.close()
    contenu[2]="METHODE %s\n"%methode
    f=open(document,"w",encoding='utf-8')
    f.writelines(contenu)
    f.close()

  #---------------------------------------------------------------------------------
  def nangu_Controles(self,editer=True,comparer=True,uiTerm=None):
    f2=None
    correct=True
    self.projet.uiTerm=uiTerm
    codeRetour=nngsrc.services.CODE_OK

    # Traitement de la methode associee
    trouve=False
    f=open(os.path.join(self.repRapport,"nng-analyses","exigencesHorsMethode.txt"),"w",encoding='utf-8')
    for e in self.projet.exigences:
      exigence=self.projet.exigences[e]
      for m in exigence['methode']:
        if not m in self.projet.methode.keys():
          trouve=True
          f.write("%s\n"%e)
    f.close()
    if trouve:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Des exigences ont des méthodes de résolution inconnues. Voir 'nng-analyses/exigencesHorsMethode.txt'.",style="Er")
    elif os.path.exists(os.path.join(self.repRapport,"nng-analyses","BilanPSF.txt")):
      # Lecture du fichier bilanPSF
      fPSF=open(os.path.join(self.repRapport,"nng-analyses","BilanPSF.txt"),"r",encoding='utf-8')
      contenu=fPSF.readlines()
      fPSF.close()
      dicoPSF={}
      for ligne in contenu:
        ligne=ligne.replace('\n','').replace('\r','').split(' ')
        PSF=ligne[0].split(':')
        methodesPS=ligne[1].split('-')[1:-1]
        plan=PSF[0]
        scenario=PSF[1]
        fiche=PSF[2]
        if not plan in dicoPSF:
          dicoPSF[plan]=({},methodesPS[0])
        if not scenario in dicoPSF[plan][0]:
          dicoPSF[plan][0][scenario]=({},methodesPS[1])
        if not fiche in dicoPSF[plan][0][scenario][0]:
          dicoPSF[plan][0][scenario][0][fiche]={}
          for m in self.projet.methode.keys():
            dicoPSF[plan][0][scenario][0][fiche][m]=[]

      for g in self.qualite.matTracabilite:
        for element in self.qualite.matTracabilite[g]:
          ((_,fiche,_,_,_,(_,lblMethode,_,_,_,exigencesGlob),_),_,_,exigencesAct)=element
          methode=None
          for m in self.projet.methode.keys():
            if lblMethode==self.projet.methode[m]['terme']:
              methode=m
          for plan in dicoPSF:
            for scenario in dicoPSF[plan][0]:
              if fiche in dicoPSF[plan][0][scenario][0]:
                dicoPSF[plan][0][scenario][0][fiche]['methode']=methode
                for e in exigencesGlob.split(','):
                  if e!='' and e in self.projet.exigences:
                    exigence=self.projet.exigences[e]
                    if exigence['applicable']:
                      for m in exigence['methode']:
                        dicoPSF[plan][0][scenario][0][fiche][m].append(e)
                for e in exigencesAct.split(','):
                  if e!='' and e in self.projet.exigences:
                    exigence=self.projet.exigences[e]
                    if exigence['applicable']:
                      for m in exigence['methode']:
                        if not e in dicoPSF[plan][0][scenario][0][fiche][m]:
                          dicoPSF[plan][0][scenario][0][fiche][m].append(e)

      fctrlMeth=open(os.path.join(self.repRapport,"nng-analyses","ControleMethode.txt"),"w",encoding='utf-8')
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"---------------------------------------------------------------------------",style="Wr")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"Contrôle des méthodes des fiches en fonction des exigences",style="Wr")
      RAS1=True
      for plan in dicoPSF:
        for scenario in dicoPSF[plan][0]:
          for fiche in dicoPSF[plan][0][scenario][0]:
            if not 'methode' in dicoPSF[plan][0][scenario][0][fiche]:
              RAS1=False
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : La méthode de la fiche %s:%s:%s n'est pas rapportée. La fiche existe-t-elle ? Les actions de la fiche sont-elles exécutées ?"%(fiche,scenario,plan),style="Wr")
            else:
              ambiguiteDict={}
              ambiguiteList=[]
              for m in self.projet.methode.keys():
                if dicoPSF[plan][0][scenario][0][fiche][m]:
                  ambiguiteDict[m]=dicoPSF[plan][0][scenario][0][fiche][m]
                  ambiguiteList.append(m)
              if len(ambiguiteList)==1:
                # Si pas d'ambiguite : Mise à jour ou affectation
                if dicoPSF[plan][0][scenario][0][fiche]['methode'] is None:
                  RAS1=False
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : Affecter la méthode '%s' à la fiche %s:%s:%s"%(ambiguiteList[0],fiche,scenario,plan),style="Wr")
                  for p in dicoPSF:
                    for s in dicoPSF[p][0]:
                      if fiche in dicoPSF[p][0][s][0]:
                        dicoPSF[p][0][s][0][fiche]['methode']=ambiguiteList[0]
                elif ambiguiteList[0]!=dicoPSF[plan][0][scenario][0][fiche]['methode']:
                  RAS1=False
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : Mettre à jour la méthode ('%s' calculée vs '%s' affectée) de la fiche %s:%s:%s"%(ambiguiteList[0],dicoPSF[plan][0][scenario][0][fiche]['methode'],fiche,scenario,plan),style="Wr")
                  dicoPSF[plan][0][scenario][0][fiche]['methode']=ambiguiteList[0]
              else:
                if dicoPSF[plan][0][scenario][0][fiche]['methode'] is None:
                  RAS1=False
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"ERREUR : Ambiguïté de la méthode pour la fiche %s:%s:%s"%(fiche,scenario,plan),style="Er")
                  if len(ambiguiteList)>0:
                    for m in ambiguiteDict:
                      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"         - méthode %s : %s"%(m,','.join(ambiguiteDict[m])),style="Er")
                  else: nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"         - aucune méthode affectable à la fiche",style="Er")
                elif ambiguiteList:
                  if not dicoPSF[plan][0][scenario][0][fiche]['methode'] in ambiguiteList:
                    RAS1=False
                    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"ERREUR : Ambiguïté ('%s' calculée vs '%s' affectée) de la méthode de la fiche %s:%s:%s"%(ambiguiteList,dicoPSF[plan][0][scenario][0][fiche]['methode'],fiche,scenario,plan),style="Er")
                  else:
                    # On controle que toutes les exigences sont dans l'affecté. Si oui, pas de soucis.
                    listeExigFiche=[]
                    for m in self.projet.methode.keys():
                      listeExigFiche.extend(dicoPSF[plan][0][scenario][0][fiche][m])
                    listeExigFiche=list(set(listeExigFiche))
                    listeMethode=dicoPSF[plan][0][scenario][0][fiche][dicoPSF[plan][0][scenario][0][fiche]['methode']]
                    listeExigFiche.sort()
                    listeMethode.sort()
                    if listeExigFiche==listeMethode:
                      RAS1=False
                      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"Remarque : Des exigences à plusieurs méthodes (%s) sont dans la fiche %s:%s:%s"%(ambiguiteList,fiche,scenario,plan),style="Rr")
                    else:
                      RAS1=False
                      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : Ambiguïté ('%s' calculée vs '%s' affectée) de la méthode de la fiche %s:%s:%s"%(ambiguiteList,dicoPSF[plan][0][scenario][0][fiche]['methode'],fiche,scenario,plan),style="Wr")
      if RAS1:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth," ...RAS",style="Wr")
      RAS2=True
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"Contrôle des méthodes des scénarios et des plans en fonction du bilan PSF",style="Wr")
      for plan in dicoPSF:
        methodeP=[]
        for scenario in dicoPSF[plan][0]:
          methodeS=[]
          for fiche in dicoPSF[plan][0][scenario][0]:
            if 'methode' in dicoPSF[plan][0][scenario][0][fiche] and not dicoPSF[plan][0][scenario][0][fiche]['methode'] in methodeS:
              methodeS.append(dicoPSF[plan][0][scenario][0][fiche]['methode'])
          if len(methodeS)==1:
            if methodeS[0] is None:
              RAS2=False
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"ERREUR : Méthode du scénario %s : indéfinie à cause de fiches ambigües vs '%s' affectée."%(scenario,dicoPSF[plan][0][scenario][1]),style="Er")
            else:
              if methodeS[0]!=dicoPSF[plan][0][scenario][1]:
                if self.typePRJsimple:
                  RAS2=False
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"IMPORTANT : Mise à jour automatique de la méthode du plan %s : '%s' calculée vs '%s' affectée."%(scenario,methodeS[0],dicoPSF[plan][0][scenario][1]),style="Wr")
                  self.MajMethode(os.path.join(self.repPlans,scenario),methodeS[0])
                else:
                  RAS2=False
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"IMPORTANT : Mise à jour automatique de la méthode du scénario %s : '%s' calculée vs '%s' affectée."%(scenario,methodeS[0],dicoPSF[plan][0][scenario][1]),style="Wr")
                  self.MajMethode(os.path.join(self.repScenarios,"%s.lst"%scenario),methodeS[0])
            if not methodeS[0] in methodeP:
              methodeP.append(methodeS[0])
          else:
            RAS2=False
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : Ambiguïté ('%s' calculée vs '%s' affectée) de la méthode du scénario %s."%(methodeS,dicoPSF[plan][0][scenario][1],scenario),style="Wr")
        if len(methodeP)==1:
          if methodeP[0] is None:
            RAS2=False
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"ERREUR : Méthode du plan %s : indéfinie à cause de scénarios indéfinis vs '%s' affectée."%(plan,dicoPSF[plan][1]),style="Er")
          else:
            if methodeP[0]!=dicoPSF[plan][1]:
              RAS2=False
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"IMPORTANT : Mise à jour automatique de la Méthode du plan %s : '%s' calculée vs '%s' affectée."%(plan,methodeP[0],dicoPSF[plan][1]),style="Wr")
              if plan!='-': # Pour eviter prj<<Simple pour le moment. TODO
                self.MajMethode(os.path.join(self.repPlans,"%s.pv"%plan),methodeS[0])
        else:
          RAS2=False
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"AVERTISSEMENT : Ambiguïté ('%s' calculée vs '%s' affectée) de la méthode du plan %s."%(methodeP,dicoPSF[plan][1],plan),style="Wr")
      if RAS2:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth," ...RAS",style="Wr")
      if not (RAS1 and RAS2):
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"Corriger les erreurs de méthode de fiches et relancer le contrôle avant toute chose,",style="Wr")
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,fctrlMeth,"puis corriger les erreurs de méthode de scenarios et de plans.",style="Wr")
      fctrlMeth.close()

    flst=open(os.path.join(self.repRapport,"nng-analyses","ControleFichesLst.txt"),"w",encoding='utf-8')
    for repertoire in glob.glob(os.path.join(self.repFiches,"*")):
      if os.access(os.path.join(self.repFiches,repertoire,"fiches_complement.lst"),os.F_OK):
        os.remove(os.path.join(self.repFiches,repertoire,"fiches_complement.lst"))
      if os.path.isdir(repertoire) and os.path.basename(repertoire)[0]!='_':
        if os.access(os.path.join(self.repFiches,repertoire,"fiches.lst"),os.F_OK):
          fiches=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repFiches,repertoire,"fiches.lst"))
          for f in range(len(fiches)):
            fiches[f]=fiches[f].replace('.xml','')
            fiches[f]=fiches[f].split(' ')[0]
        else:
          fiches=[]

        for fichier in glob.glob(os.path.join(repertoire,"*.xml")):
          fichier=os.path.basename(fichier).replace('.xml','')

          if fichier in fiches:
            fiches.remove(fichier)
          else:
            if fichier[0]!='_':
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,flst,"\nLa fiche %s n'est pas répertoriée dans le 'fiches.lst' de %s."%(fichier,os.path.basename(repertoire)),echo=False)
              codeRetour=nngsrc.services.CODE_AVERTI
              if f2 is None:
                f2=open(os.path.join(self.repFiches,repertoire,"fiches_complement.lst"),"w",encoding='utf-8')
              f2.write(fichier+'.xml\n')
              correct=False
        if fiches:
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,flst,"\nLes fiches suivantes n'existent pas dans %s :"%os.path.basename(repertoire),echo=False)
          codeRetour=nngsrc.services.CODE_ANOMAL
          correct=False
          for f in fiches:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,flst,"%s/%s.xml"%(os.path.basename(repertoire),f),echo=False)
      if f2 is not None:
        f2.close()
        if editer: nngsrc.services.SRV_EditerTxt(os.path.join(self.repFiches,repertoire,"fiches_complement.lst"),editer)
        f2=None
    flst.close()
    if correct:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Les listes de fiches sont cohérentes.",style="Wr")
    else:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Les listes de fiches 'fiches.lst' et autres ne sont pas cohérentes.",style="Er")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Consulter le fichier %s"%os.path.join(self.repRapport,"nng-analyses","ControleFichesLst.txt"),style="Er")
      codeRetour=nngsrc.services.CODE_ANOMAL
      if editer: nngsrc.services.SRV_EditerTxt(os.path.join(self.repRapport,"nng-analyses","ControleFichesLst.txt"),editer)
    if os.access(os.path.join(self.repRapport,self.projet.execution,'nonList.txt'),os.F_OK):
      if editer: nngsrc.services.SRV_EditerTxt(os.path.join(self.repRapport,self.projet.execution,'nonList.txt'),editer)
      if editer:
        for fichier in self.projet.PRODUIT_EXIGENCES:
          nngsrc.services.SRV_EditerTxt(fichier,editer)
      if editer:
        for fichier in self.projet.PRODUIT_FONCTIONNALITES:
          nngsrc.services.SRV_EditerTxt(fichier,editer)
    doublons=0
    inconnus=0

    if inconnus>0:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Nombre de faits techniques errones %s/%s"%(inconnus,len(self.qualite.baseFT)),style="Er")
      codeRetour=nngsrc.services.CODE_ANOMAL
    if doublons>0:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Nombre de faits techniques doublons %d/%s"%(doublons,len(self.qualite.baseFT)),style="Er")
      codeRetour=nngsrc.services.CODE_ANOMAL

    if inconnus+doublons>0:
      if comparer:
        if os.getenv("COMPARE","-aucun_comparateur-")=="-aucun_comparateur-":
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Variable d'environnement 'COMPARE', non définie !",style="Rr")
          comparateur="echo"
        else:
          comparateur=os.getenv("COMPARE","-aucun_comparateur-")
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None," Lancement du comparateur %s"%comparateur,style="Ir")
        os.system("%s %s %s"%('"'+comparateur+'"',self.projet.PRODUIT_BASEFT,os.path.join(self.repRapport,"nng-analyses","FTerronnes.txt")))
    if self.ajustementsTAG and os.path.exists(os.path.join(self.repRapport,"nng-analyses","ajustementsTAG.txt")):
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Ajustements de TAG d'actions",style="Er")
      if editer: nngsrc.services.SRV_EditerTxt(os.path.join(self.repRapport,"nng-analyses","ajustementsTAG.txt"),editer)
    return codeRetour

  #-----------------------------------------------------------------------------------
  ## Bilan de Campagne
  def nangu_CampagneInit(self,repCampagne,ficCampagne,mode=None,nomCampagne="nomCampagne",idCampagne=None,uiTerm=None):
    if idCampagne is None: idCampagne=self.projet.PRODUIT_VERSION
    self.campagne=nngsrc.campagne.Campagne(repCampagne,self.repPlans,self.projet,ficCampagne,mode,nomCampagne,idCampagne)
    codesRetour=[]
#    for cahier in self.campagne.cahiersCampagne:
#      codesRetour.append(self.nangu_Validation([os.path.join(self.repPlans,cahier["cahier"])],uiTerm=uiTerm))
    codesRetour.append(self.campagne.CAMPinitier(self,mode))
    return max(codesRetour)

  #-----------------------------------------------------------------------------------
  ## Bilan de Campagne
  def nangu_Campagne(self,repCampagne,ficCampagne,redondance=False,uiTerm=None):
    redondance=True
    self.projet.uiTerm=uiTerm
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---- %s -----------------------------------------------------"%self.projet.lexique.entree('MOT_CAMPAGNE'),style="Ir")

    self.campagne=nngsrc.campagne.Campagne(repCampagne,self.repPlans,self.projet,ficCampagne)
    if self.campagne.repCampagne is None: return
    self.campagne.iterCampagne+=1
    self.rapport=nngsrc.rapport.Rapport(ficCampagne,self.constantes,self.repTravail,self.repRessources,self.repRapport,self.projet,self.qualite,self.version,bilan=os.path.join(self.campagne.repCampagne,self.campagne.refCampagne))

    # Recolte des rapports executes
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_1_RECOLTE'),style="r")
    csvBilans=recolteRapports(self.campagne.cahiersCampagne,self.repRapport,self.projet)

    # Des cahiers sont déroulés ...
    if '' in csvBilans:
      key=''
      csvBilans[key]=sorted(csvBilans[key])

      # Trie chronologique des rapports avec non redondance
      dictListe={}
      self.campagne.bilanRapports={}
      lastit=''
      for item in csvBilans[key]:
        it=item.split('-')[-2]
        if redondance or lastit!=it:
          dictListe[it]=item
        lastit=it
      self.campagne.lclefs=dictListe.keys()
      self.campagne.lclefs=sorted(self.campagne.lclefs)

      # Gestion des exigences
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_2_GESTION_EXIG_FT'),style="r")
      MatExigences={}
      for e in self.projet.exigences:
         MatExigences[e]=None
      #   initialisation des fichiers d'exigences et de controles
      fichierGrExig=os.path.join(self.repTravail,'graphiqueExig')
      fichierGrCont=os.path.join(self.repTravail,'graphiqueCont')
      nngsrc.tables.TABLEgenerePrologue(fichierGrExig+'.dat',["RAPPORT","NOM_COMPLET","NB_EXIG",'CRITIQUES','BLOQUANTS','MAJEURS','MINEURS','NON_QUALIFIES',"NB_EXIG_NOUV_COUV","NB_EXIG_NOUV_COUV_OK","NB_EXIG_TOT_COUV","NB_EXIG_TOT_COUV_OK"])
      nngsrc.tables.TABLEgenerePrologue(fichierGrCont+'.dat',["RAPPORT","NOM_COMPLET",'CRITIQUES','BLOQUANTS','MAJEURS','MINEURS','NON_QUALIFIES',"NB_CONTROLE","NB_CONTROLE_OK","NB_CONTROLE_AV"])
      fGrExig=open(fichierGrExig+'.dat',"a",encoding='utf-8')
      fGrCont=open(fichierGrCont+'.dat',"a",encoding='utf-8')

      label=''
      nbelem=0
      # Nombre d'exigences applicables total du projet
      nbexig=0
      for e in self.projet.exigences:
        if self.projet.exigences[e]['applicable']:
          nbexig+=1
      #
      cumulNbExigCouv=0  # Cumul du nombre d'exig couvertes
      cumulNbExigCouvOK=0 # Cumul du nombre d'exig couvertes OK
      nbctrl=0
      nbctrlAV=0
      nbctrlOK=0
      listeExigRapports=[]
      for item in self.campagne.lclefs:
#        print "--------------------------------------"
#        print "Rapport>",item
        nbExigCouvOK=0 # Nombre d'exig couvertes OK sur 1 rapport courant
        nbExigCouv=0  # Nombre d'exig couvertes sur le rapport courant
        elem=dictListe[item]
        # lecture du fichier csv du rapport
#        print "elem>",elem
        bilan=os.path.join(self.repRapport,self.projet.execution,"CSV",elem+".csv")
        billignes=nngsrc.services.SRV_LireContenuListe(bilan)

        # Ecriture du nom du rapport
        nomRapport=os.path.dirname(elem).split('-')[-1]
        fGrExig.write("%s %s"%(nomRapport,os.path.dirname(elem))) # (0,1)- matrice MTEvC
        fGrCont.write("%s %s"%(nomRapport,os.path.dirname(elem))) # (0,1)- matrice MTEvC
#        print elem,
        label=label+', "'+elem.replace("%s-%s_%s-"%(self.projet.PRODUIT_REFERENCE,self.campagne.nomCampagne,self.campagne.idCampagne),'').split('-')[0]+'" %d '%nbelem
        if nbelem==0:
          label=label[2:]
        nbelem+=1

        # recherche du nombre d'exigences dans matrice
        FTCOLONNE=5
        SEVERCOLONNE=6
        if self.projet.PRODUIT_EXIGENCES_DECLINEES:
          FTCOLONNE+=1
          SEVERCOLONNE+=1
      #  print "ExigInMat>",MatExigences.keys()
        for l in range(1,len(billignes)):
          ligne=billignes[l].replace('\r','').replace('\n','').split(';')
          if ligne[0] in MatExigences:
            # Si la matrice n'est pas affectée par l'exigence :
            if MatExigences[ligne[0]] is None:
              # on affecte la matrice de l'exigence avec la severite et la ft si present : colonne 5 et ft!='-'
              if ligne[FTCOLONNE]!='-':
                ftAssocie=nngsrc.qualite.TODOFTparID(self.qualite.baseFT,ligne[FTCOLONNE].split(' ')[0].replace(self.projet.REF_FT+'-',''))
                if ftAssocie['STATUT'] not in nngsrc.qualite.DejaClosFT:
                  MatExigences[ligne[0]]=(ligne[SEVERCOLONNE],ligne[FTCOLONNE].split(' ')[0]) # severite/ft
                else:
                  MatExigences[ligne[0]]=('n.q.','')
              else:
                MatExigences[ligne[0]]=('n.q.','')
            # Si la matrice est affectée par l'exigence :
            else:
              # on garde la severite la plus haute
              (sev,_)=MatExigences[ligne[0]]
              if ligne[FTCOLONNE]!='-':
                if self.projet.lexique.niveauCriticite(ligne[SEVERCOLONNE])>=self.projet.lexique.niveauCriticite(sev):
                  ftAssocie=nngsrc.qualite.TODOFTparID(self.qualite.baseFT,ligne[FTCOLONNE].split(' ')[0].replace(self.projet.REF_FT+'-',''))
                  if ftAssocie:
                    if ftAssocie['STATUT'] not in nngsrc.qualite.DejaClosFT:
                      MatExigences[ligne[0]]=(ligne[SEVERCOLONNE],ligne[FTCOLONNE].split(' ')[0])
                  else:
                      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"La FT '%s' est inaccessible"%ligne[FTCOLONNE],style="Er",parallel=self.parallel)

#          print "MatExigences>",ligne[0],MatExigences[ligne[0]]
        fGrExig.write(' %d'%nbexig) # (2)- nombre d'exigences

        # calcul du nombre d'exigences couvertes ok
        for ex in MatExigences.keys():
          if MatExigences[ex] is not None and not ex in listeExigRapports:
            nbExigCouv+=1
            listeExigRapports.append(ex)
            (sev,mant)=MatExigences[ex]
            if mant=='':
              nbExigCouvOK+=1
#            print "MatEx >",ex,MatExigences[ex]
#            print "nbExigCouv ",nbExigCouv
#            print "nbExigCouvOK",nbExigCouvOK

        # lecture du fichier bilan de couverture
        bilan=os.path.join(self.repRapport,self.projet.execution,"CSV",elem.replace("-MTEvC",'')+".csv")
        billignes=nngsrc.services.SRV_LireContenuListe(bilan)

        # Recuperation des criticites des faits techniques
        for texte in ['LOCUT_TAUX_EXIG_AVEC_FT_CRITIQUES','LOCUT_TAUX_EXIG_AVEC_FT_BLOQUANTS','LOCUT_TAUX_EXIG_AVEC_FT_MAJEURS','LOCUT_TAUX_EXIG_AVEC_FT_MINEURS','LOCUT_TAUX_EXIG_AVEC_FT_NQ']:
          v=0
          for n in range(len(billignes)):
            if billignes[n].find(self.projet.lexique.entree(texte))==0:
              l=billignes[n][billignes[n].find(',')+1:]
              v=int(l[:l.find(',')])
              break
          fGrExig.write(' %d'%v) # (3,7)- Criticité

        # Recuperation des etats des controles
        if len(billignes)!=BILAN_NB_LIGNES:
          self.campagne.bilanRapports[item]={'cahier':None}
        else:
          cahier=billignes[BILAN_NB_LIGNES-1].replace('\r','').replace('\n','').split(' : ')[1]
          self.campagne.bilanRapports[item]={'cahier':cahier}

        for texte in ['LOCUT_NB_CONTROLES_DOCUMENT','LOCUT_NB_CTRL_OK','LOCUT_NB_CTRL_AV','LOCUT_NB_CTRL_NOK','LOCUT_NB_CTRL_NT']:
          v=0
          for n in range(len(billignes)):
            if billignes[n].find(self.projet.lexique.entree(texte))!=-1:
              l=billignes[n][billignes[n].find(',')+1:]
              if l.find(',')>0: l=l[:l.find(',')]
              v=int(l)
              break
          fGrCont.write(' %d'%v) # (3,7)- Criticité
          if texte=='LOCUT_NB_CONTROLES_DOCUMENT':
            nbctrl=nbctrl+v
          elif texte=='LOCUT_NB_CTRL_OK':
            nbctrlOK=nbctrlOK+v
          elif texte=='LOCUT_NB_CTRL_AV':
            nbctrlAV=nbctrlAV+v
          self.campagne.bilanRapports[item][texte]=v
        fGrCont.write(' %d %d %s\n'%(nbctrl,nbctrlOK,nbctrlAV))

        # Recuperation des nouveautes et totaux
#        print "ECRIT : "
        fGrExig.write(' %d' % nbExigCouv)    # (8)- nombre d'exigences NOUVELLES, couvertes par le rapport
#        print "nombre d'exigences NOUVELLES, couvertes par le rapport>",nbExigCouv
        cumulNbExigCouv+=nbExigCouv
        fGrExig.write(' %d' % nbExigCouvOK)  # (9)- nombre d'exigences NOUVELLES, couvertes OK par le rapport
#        print "nombre d'exigences NOUVELLES, couvertes OK par le rapport>",nbExigCouvOK
        cumulNbExigCouvOK+=nbExigCouvOK
        fGrExig.write(' %d'%cumulNbExigCouv)  #  (9)- nombre TOTAL d'exigences couvertes par les rapports
#        print "nombre TOTAL d'exigences couvertes par les rapports>",cumulNbExigCouv
        fGrExig.write(' %d'%cumulNbExigCouvOK) # (10)- nombre TOTAL d'exigences couvertes OK par les rapports
#        print "nombre TOTAL d'exigences couvertes OK par les rapports>",cumulNbExigCouvOK
        fGrExig.write('\n')

        # Rapport Bilan
      #  if (not os.access(os.path.join(self.repRapport,self.projet.execution,"HTML",elem.split('\\')[0].split('/')[0],"campagne.cr"),os.F_OK)):
        dathtml=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repRapport,self.projet.execution,"HTML",elem.split('\\')[0].split('/')[0],"bandeau.html"))
        #RV=open(os.path.join(self.repRapport,self.projet.execution,"HTML",elem.split('\\')[0].split('/')[0],"campagne.cr"),"w")
        CRcampagne=os.path.join(self.campagne.repCampagne,self.campagne.refCampagne,"%s.cr"%elem.split('\\')[0].split('/')[0])
        RV=open(CRcampagne,"w",encoding='utf-8')
        RV.write('\n')
        RV.write('\n')
        # Ligne avec titre : <center>titre<br/>sous-titre<center>
        #if self.projet.debug==2: print("dathtml9",dathtml[9])
        ligne=dathtml[9].split('<center>')[1]
        RV.write('%s\n'%ligne.split('<br/>')[0]) # Titre
        ligne=ligne.split('<br/>')[1].split('</center>')[0]
        RV.write('%s\n'%ligne.replace('&eacute;','e')) # Sous-titre
        # Ligne avec date : ' <h2>Bilan xxxxx du xx/xx/xxxx de xx:xx:xx à xx:xx:xx</h2>'
        #if self.projet.debug==2: print("dathtml14",dathtml[14])
        ligne=dathtml[14].split(self.projet.lexique.entree('SPLIT_DECOUPE_CAMPAGNE_DU'))[1]
        RV.write('%s\n'%ligne.split(self.projet.lexique.entree('SPLIT_DECOUPE_CAMPAGNE_DE'))[0]) # Date
        ligne=ligne.replace('</h2>','').replace('\r','').replace('\n','').split(self.projet.lexique.entree('SPLIT_DECOUPE_CAMPAGNE_DE'))[1].split(self.projet.lexique.entree('SPLIT_DECOUPE_CAMPAGNE_A'))
        RV.write('%s\n'%ligne[0])
        RV.write('%s\n'%ligne[1])
        #if self.projet.debug==2: print("dathtml19",dathtml[19])
        ligne=dathtml[19].replace('</p>','').replace('\r','').replace('\n','').replace(' ','').split(":")#[1].split('-%s'%self.projet.lexique.entree('MOT_UTILISATEUR'))
        RV.write('%s\n'%ligne[1].split('-')[0]) # PC
        RV.write('%s\n'%ligne[2]) # login
        RV.write('\n')
        RV.write('\n')
        RV.close()

        # Lecture de ce même fichier pour feuille de route (deroulement)
        if os.access(CRcampagne, os.F_OK):
          RVlig=nngsrc.services.SRV_LireContenuListe(CRcampagne)
          for l in range(len(RVlig)):
            RVlig[l]=RVlig[l]
          self.rapport.Campagne_FeuilleRouteDeroulement(elem.split('\\')[0].split('/')[0],RVlig)

        self.rapport.Campagne_RecupereBilanFT(elem.split('\\')[0].split('/')[0])
        if os.access(os.path.join(self.repRapport,self.projet.execution,"CSV",elem.split('\\')[0].split('/')[0]+'-CSV.zip'),os.F_OK):
          for fic in os.listdir(os.path.join(self.repRapport,self.projet.execution,"CSV",elem.split('\\')[0].split('/')[0])):
            nngsrc.services.SRV_detruire(os.path.join(self.repRapport,self.projet.execution,"CSV",elem.split('\\')[0].split('/')[0],fic),timeout=5,signaler=True)
          os.rmdir(os.path.join(self.repRapport,self.projet.execution,"CSV",elem.split('\\')[0].split('/')[0]))
      fGrExig.close()
      fGrCont.close()
      nngsrc.tables.TABLEgenereEpilogue(fichierGrExig+'.dat')
      nngsrc.tables.TABLEgenereEpilogue(fichierGrCont+'.dat')

      # Graphique en camembert des exigences couvertes
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,self.projet.lexique.entree('LOCUT_3_GRAPHIQUES'),style="r")
      parametres=nngsrc.graphes.GRAPH_parametresDefaut(pMATPLOTLIB=True)

      CouvOK=cumulNbExigCouvOK
      CouvKO=cumulNbExigCouv-cumulNbExigCouvOK
      nonCouv=nbexig-CouvOK-CouvKO

      X=[ self.projet.lexique.entree('ABR_NON_COUVRANT') ,  '%s OK'%self.projet.lexique.entree('ABR_COUVRANT')  ,  '%s KO'%self.projet.lexique.entree('ABR_COUVRANT') ]
      Y=[ nonCouv     ,  CouvOK      ,  CouvKO     ]
      parametres['explode']=(0,0.1,0)
      nngsrc.graphes.GRAPH_Camembert(fichierGrExig,self.projet.lexique.entree('MOT_EXIGENCES'),X,Y,parametres)
      self.rapport.Campagne_CouvertureExigences(MatExigences,"%s.png"%fichierGrExig)

      depot=os.path.join(self.campagne.repCampagne,self.campagne.refCampagne,"TODO_%s_%s"%(self.campagne.nomCampagne,self.campagne.idCampagne))
      (fichierT,fichierO,fichierR)=self.nangu_Todo(self.projet.PRODUIT_VERSION,'-',depot,self.projet.uiTerm)
     # print "DBG>",fichierT
     # print "DBG>",fichierO
     # print "DBG>",fichierR
      self.rapport.Campagne_BilanFTtodo(fichierO,fichierR)

      if self.campagne.lclefs:
        #Generation du graphique fGrExig
        parametres=nngsrc.graphes.GRAPH_parametresDefaut(pMATPLOTLIB=True)
        parametres['largeur']=460
        parametres['hauteur']=320
        parametres['legende']=(0.5,-0.3,3)
        parametres['rotation']=45
        parametres['couleurs']=['#87CEEB','#D2B48C','#3CB371']

        donneeDAT=fichierGrExig+'.dat'
        donneeGraphique=fichierGrExig+'_gr'
#        X=("RAPPORT","lib")
#        Y=[("NB_EXIG","Nb Exigences"),("NB_EXIG_TOT_COUV","Nb Exigences Couv."),("NB_EXIG_TOT_COUV_OK","Nb Exigences Couv. OK")]
#        GRAPHgnuplot2D(donneeDAT,donneeGraphique,"Suivi des couvertures d'exigences",X,Y,parametres)
        contenuTab=nngsrc.tables.TABLElireFichier(donneeDAT)
        X=["RAPPORT"]
      #  valeursX=nngsrc.tables.TABLEextraire(contenuTab,X)[0]
        ticsX=nngsrc.tables.TABLEextraire(contenuTab,X,reels=False)[0]
        valeursX=range(len(ticsX))
        Y=["NB_EXIG","NB_EXIG_TOT_COUV","NB_EXIG_TOT_COUV_OK"]
        valeursY=nngsrc.tables.TABLEextraire(contenuTab,Y)
        titre=self.projet.lexique.entree('LOCUT_SUIVI_COUVERTURE_EXIGENCES')
        grX={'libelle':'','valeurs':valeursX,'tics':ticsX}
        grY=[{'trace':'lp','axe':'y1','valeurs':valeursY[0],'label':self.projet.lexique.entree('LOCUT_NB_EXIGENCES'),'libelle':''},
             {'trace':'lp','axe':'y1','valeurs':valeursY[1],'label':'%s %s'%(self.projet.lexique.entree('LOCUT_NB_EXIGENCES'),self.projet.lexique.entree('ABR_COUVRANT'))},
             {'trace':'lp','axe':'y1','valeurs':valeursY[2],'label':'%s %s %s'%(self.projet.lexique.entree('LOCUT_NB_EXIGENCES'),self.projet.lexique.entree('ABR_COUVRANT'),self.projet.lexique.entree('MOT_OK'))}]
        nngsrc.graphes.GRAPH_2D(donneeGraphique,titre,grX,grY,parametres)

        #Generation du graphique fGrCont
        donneeDAT=fichierGrCont+'.dat'
        donneeGraphique=fichierGrCont+'_gr'
        contenuTab=nngsrc.tables.TABLElireFichier(donneeDAT)
        X=["RAPPORT"]
       # valeursX=nngsrc.tables.TABLEextraire(contenuTab,X)[0]
        ticsX=nngsrc.tables.TABLEextraire(contenuTab,X,reels=False)[0]
        valeursX=range(len(ticsX))
        Y=["NB_CONTROLE","NB_CONTROLE_AV","NB_CONTROLE_OK"]
        valeursY=nngsrc.tables.TABLEextraire(contenuTab,Y)
        titre=self.projet.lexique.entree("LOCUT_SUIVI_CONTROLES")
        grX={'libelle':'','valeurs':valeursX,'tics':ticsX}
        grY=[{'trace':'lp','axe':'y1','valeurs':valeursY[0],'label':self.projet.lexique.entree("LOCUT_NB_CONTROLES_GR"),'libelle':''},
             {'trace':'lp','axe':'y1','valeurs':valeursY[1],'label':'%s %s'%(self.projet.lexique.entree("LOCUT_NB_CONTROLES_GR"),self.projet.lexique.entree("MOT_AV"))},
             {'trace':'lp','axe':'y1','valeurs':valeursY[2],'label':'%s %s'%(self.projet.lexique.entree("LOCUT_NB_CONTROLES_GR"),self.projet.lexique.entree("MOT_OK"))}]
        nngsrc.graphes.GRAPH_2D(donneeGraphique,titre,grX,grY,parametres)

      self.rapport.Campagne_BilanExigencesControles(fichierGrExig+'.dat',fichierGrExig+'_gr.png',fichierGrCont+'.dat',fichierGrCont+'_gr.png')
      self.rapport.Campagne_Terminer(self.campagne)

    for chCamp in self.campagne.cahiersCampagne:
      if chCamp["select"]=='X':
        for rap in self.campagne.lclefs:
          if chCamp['cahier']==self.campagne.bilanRapports[rap]['cahier']:
            bilan=self.campagne.bilanRapports[rap]
            if bilan['LOCUT_NB_CTRL_NOK']>0:
              chCamp['statut']='KO'
            elif bilan['LOCUT_NB_CTRL_AV']>0:
              chCamp['statut']='AV'
            elif bilan['LOCUT_NB_CTRL_NT']>0:
              chCamp['statut']='NT'
            else:
              chCamp['statut']='OK'

    self.campagne.CAMPecrire()
    #TODO vidage du workdir
    #os.rmdir(os.path.join(self.repTravail,'CSV'))
    #os.rmdir(os.path.join(self.repTravail,'HTML'))
    #os.rmdir(os.path.join(self.repTravail,'Markdown'))
    for elem in glob.glob(os.path.join(self.repTravail,'*.*')):
      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
    #os.rmdir(self.repTravail)
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"---- %s --------------------------------------------------------"%self.projet.lexique.entree('LOCUT_FIN_EXECUTION'),style="Ir")
    return self.campagne.cahiersCampagne

  #-----------------------------------------------------------------------------------
  def nangu_ExportProjet(self,GCFSPAT,ficProjetPRJ,idDepot,repExport,uiTerm=None):
    if repExport=='': return nngsrc.services.CODE_ERREUR
    codeRetour=nngsrc.services.CODE_OK
    self.projet.uiTerm=uiTerm
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---- Export du projet -----------------------------------------------------",style="Ir")

    projetExport=deepcopy(self.projet)
    qualiteExport=deepcopy(self.qualite)
    projetExport.DOCUMENT_ITERATION="-1"
    if self.projet.version<9.0:
      projetExport.VALIDATION_REFERENCE+="_%s"%idDepot
    else:
      projetExport.REFERENCE_IDENTIFIANT=projetExport.REFERENCE_IDENTIFIANT.replace('{VERSION}',"%s%s{VERSION}"%(idDepot,projetExport.SEPARATEUR_CHAMPS))
      projetExport.NOMBRE_CHAMPS+=1
    projetExport.REF_FT+="_%s"%idDepot
    projetExport.PRODUIT_BASEFT="%s_%s.xml"%(projetExport.PRODUIT_BASEFT.replace(".xml",''),idDepot)
    (Version,Projet,Types,Composants,Versions,Priorites,Gravites,Responsables,Statuts,Couleurs,Filtres,Caches)=qualiteExport.todoFT
    projetPRJ=chargeProjet(ficProjetPRJ)
    (_,_,(repConfiguration,_),(repCampagnes,repPlans,repScenarios,repFiches,repRapports,repAutres,repCourant),(lstRapports,controleFiches,radicalScenarios,referenceSubst),_,dicoEnv)=projetPRJ
    if self.projet.version<9.0:
      (reference,separateurSuffixe,separateurChamps,nombreChamps)=referenceSubst
    else:
      reference=None

    #-- Campagnes
    if GCFSPAT.find('G')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Campagne",style="r")
      repertoire=os.path.join(repExport,"Campagnes")
      if not os.path.exists(repertoire): os.makedirs(repertoire)
      for f in glob.glob(os.path.join(repCampagnes,'*.cpg')):
        dst=os.path.join(repertoire,os.path.basename(f))
        shutil.copy(f,dst)
    #-- Configurations
    if GCFSPAT.find('C')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Configuration",style="r")
      repertoire=os.path.join(repExport,"Configuration")
      if not os.path.exists(repertoire): os.makedirs(repertoire)
      #-- Faits techniques
      if GCFSPAT.find('T')==-1:
        qualiteExport.baseFT=[]
      projetExport.Sauvegarde(os.path.join(repExport,"Configuration","%s.xml"%idDepot),repFiches)
      nngsrc.services.SRV_copier(os.path.join(repConfiguration,"logoNangu.png"),os.path.join(repExport,"Configuration","logoNangu.png"))
      nngsrc.services.SRV_copier(os.path.join(repConfiguration,"logoClient.png"),os.path.join(repExport,"Configuration","logoClient.png"))
      nngsrc.services.SRV_copier(os.path.join(repConfiguration,"logoSociete.png"),os.path.join(repExport,"Configuration","logoSociete.png"))
    #-- Fiches
    if GCFSPAT.find('F')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Fiches",style="r")
      nngsrc.services.SRV_copierRepert(repFiches,os.path.join(repExport,"Fiches"))
    #-- Rapports
    # - Pas de copie ! -
    #-- Scénarios
    if GCFSPAT.find('S')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Scenarios",style="r")
      nngsrc.services.SRV_copierRepert(repScenarios,os.path.join(repExport,"Scenarios"))
    #-- Plans
    if GCFSPAT.find('P')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Plans",style="r")
      nngsrc.services.SRV_copierRepert(repPlans,os.path.join(repExport,"Plans"))
    #-- Autres
    if GCFSPAT.find('A')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Autres",style="r")
      for repertoire in repAutres.split(';'):
        if repertoire!='':
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"     %s"%repertoire,style="r")
          nngsrc.services.SRV_copierRepert(repertoire,os.path.join(repExport,os.path.basename(repertoire)))
    #-- Qualite
    repertoire=os.path.join(repExport,"Qualite")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export Qualite",style="r")
    if not os.path.exists(repertoire): os.makedirs(repertoire)
    for fichier in projetExport.PRODUIT_EXIGENCES:
      shutil.copy(fichier,repertoire)
    for fichier in projetExport.PRODUIT_FONCTIONNALITES:
      shutil.copy(fichier,repertoire)
    shutil.copy(projetExport.PRODUIT_METHODE,repertoire)
    nomBaseFT=os.path.join(repExport,"Qualite",os.path.basename(projetExport.PRODUIT_BASEFT))
    #-- FT
    if GCFSPAT.find('T')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export FT",style="r")
      qualiteExport.TODOecrireXML(nomBaseFT,self.projet.lexique,False,len(qualiteExport.baseFT))
    else:
      qualiteExport.todoFT=(Version,Projet,Types,Composants,Versions,Priorites,Gravites,Responsables,Statuts,Couleurs,Filtres,Caches)
      qualiteExport.TODOecrireXML(nomBaseFT,self.projet.lexique,False,len(qualiteExport.baseFT))
    #-- fichier <projet>.prj
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Export .prj",style="r")
    depotPrj=os.path.join(repExport,"%s.prj"%idDepot)
    configXml="%s.xml"%idDepot
    ecrireProjet(depotPrj,dicoEnv,self.typePRJsimple,radicalScenarios,reference,configXml)

    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"---- Fin d'export --------------------------------------------------------",style="Ir")
    return codeRetour

  #---------------------------------------------------------------------------------
  def nangu_ImportProjet(self,FRSPAT,ficProjetPRJ,idDepot,repImport,uiTerm=None):
    if repImport=='': return nngsrc.services.CODE_ERREUR
    codeRetour=nngsrc.services.CODE_OK
    self.projet.uiTerm=uiTerm
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"---- Import du projet -----------------------------------------------------",style="Ir")

    (Version,Projet,Types,Composants,Versions,Priorites,Gravites,Responsables,Statuts,Couleurs,Filtres,Caches)=self.qualite.todoFT
    projetPRJ=chargeProjet(ficProjetPRJ)
    (_,_,(repConfiguration,configurationCourante),(repCampagnes,repPlans,repScenarios,repFiches,repRapports,repAutres,repCourant),_,_,dicoEnv)=projetPRJ
    identifiant="%s%s"%(idDepot,self.projet.SEPARATEUR_CHAMPS)

    #-- Faits techniques
    if FRSPAT.find('T')>-1 or FRSPAT.find('R')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import FT",style="r")
      nomFicBaseFTDeport=os.path.join(repImport,"Qualite","%s_%s.xml"%(os.path.basename(self.projet.PRODUIT_BASEFT).replace('.xml',''),idDepot))

      if self.projet.MANTIS is not None:
        if not os.path.exists(os.path.join(self.repRapport,"Mantis")):
          os.makedirs(os.path.join(self.repRapport,"Mantis"))

      qualiteDeport=nngsrc.qualite.Qualite(self.repRapport,self.projet)
      qualiteDeport.TODOlireXML(nomFicBaseFTDeport,self.projet.lexique,self.projet.PRODUIT_NOM,self.projet.PRODUIT_VERSION)

#      len(qualiteExport.baseFT)
      maxIDFT=len(self.qualite.baseFT)
      maxNum=-1
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Nombre de FT de la base : %d"%maxIDFT,style="r")
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Nombre de FT du deport  : %d"%len(qualiteDeport.baseFT),style="r")
      lstDictFTdeport=[]
      nbDejaEmis=0
      # pour chaque ft du deport
      for ftDeport in qualiteDeport.baseFT:
        libelledeport=ftDeport['LIBELLE'].split('\n')
        statutFTdeport={'dejaEmis':False,'IDdepot':ftDeport['ID'],'IDbase':None}
        # 1- on regarde s'il est déjà émis.
        for ft in self.qualite.baseFT:
          libelle=ft['LIBELLE'].split('\n')
          if libelledeport[1:]==libelle[1:]:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    FT dejà émis :",style="Or")
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Deport(%s) %s"%(ftDeport['ID'],libelledeport[0]),style="Or")
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Base  (%s) %s"%(ft['ID'],libelle[0]),style="Or")
            statutFTdeport['dejaEmis']=True
            statutFTdeport['IDbase']=ft['ID']
            nbDejaEmis+=1
        # 2- s'il n'est pas déjà émis...
        if not statutFTdeport['dejaEmis']:
          # ...changement de l'ID
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"    FT non dejà émis :",style="Or")
         # num=int(ftDeport['ID'])-nbDejaEmis+maxIDFT
         # if num>maxNum:maxNum=num
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Deport(%s) %s"%(ftDeport['ID'],libelledeport[0]),style="Or")
          ftDeport['ID']="%06d"%(maxIDFT+1)
          statutFTdeport['IDbase']=ftDeport['ID']
          # ...changement du nom du rapport
          libelledeport[0]=libelledeport[0].replace(identifiant,'')
          num=int(libelledeport[0].split(self.projet.SEPARATEUR_CHAMPS)[-1])
          libelledeport[0]=self.projet.SEPARATEUR_CHAMPS.join(libelledeport[0].split(self.projet.SEPARATEUR_CHAMPS)[:-1])+self.projet.SEPARATEUR_CHAMPS+"%04d"%(int(self.projet.DOCUMENT_ITERATION)+num)
#          print "DBG-n>",ftDeport['ID'],libelledeport[0]
          ftDeport['LIBELLE']='\n'.join(libelledeport)
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Base  (%s) %s"%(ftDeport['ID'],libelledeport[0]),style="Or")
          # ... on l'ajoute dans la base
          maxIDFT+=1
          self.qualite.baseFT.append(ftDeport)
        # 3- on l'ajoute dans la liste des équivalences
        lstDictFTdeport.append(statutFTdeport)
      #self.projet.todoFT=(Version,Projet,Types,Composants,Versions,Priorites,Gravites,Responsables,Statuts,Couleurs,Filtres,Caches)
      #self.projet.baseFT=qualiteDeport.baseFT
      self.qualite.NUM_FT=maxIDFT
      self.qualite.TODOecrireXML(self.projet.PRODUIT_BASEFT,self.projet.lexique,False,len(self.qualite.baseFT))

    #-- Rapports
    if FRSPAT.find('R')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import Rapports",style="r")
      maxNum=-1
      #--1--- fichiers rapports CSV
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    CSV",style="r")
      listeRapports=[]
      for rapCSV in glob.glob(os.path.join(repImport,"Rapports",'R',"CSV","*")):
        if os.path.isdir(rapCSV):
          listeRapports.append(rapCSV)
      listeRapports.sort()
      for rapCSV in listeRapports:
        # recupération du nom de rapport et suppression de l'ID
        rapCSVdst=os.path.basename(rapCSV).replace(identifiant,'')
        # recuperation du numero d'iteration
        num=int(rapCSVdst.split('-')[-1])
        if num>maxNum:maxNum=num
        # creation du repertoire du rapport destination
        rapCSVdst='-'.join(rapCSVdst.split('-')[:-1])+'-'+"%04d"%(int(self.projet.DOCUMENT_ITERATION)+num)
        rapCSVdst=os.path.join(repRapports,'R',"CSV",rapCSVdst)
        if not os.path.exists(rapCSVdst): os.makedirs(rapCSVdst)
        # Renommage des FT (CSV)
        listeFichiers=[]
        for ficCSV in glob.glob(os.path.join(rapCSV,"*.csv")):
          listeFichiers.append(ficCSV)
        listeFichiers.sort()
        for ficCSV in listeFichiers:
          ficCSVdst=os.path.basename(ficCSV).replace(identifiant,'').split(self.projet.SEPARATEUR_CHAMPS)
          typeDoc=ficCSVdst[-1].split('.')[0]
          if typeDoc in ['MTCvE','MTCvF','MTFvC','MTEvC','ncE','ncF']:
            ficCSVdst[-2]="%04d"%(int(self.projet.DOCUMENT_ITERATION)+num)
          else:
            ficCSVdst[-1]="%04d.csv"%(int(self.projet.DOCUMENT_ITERATION)+num)
          ficCSVdst=os.path.join(repRapports,'R',"CSV",rapCSVdst,'-'.join(ficCSVdst))
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Depot %s"%ficCSV,style="Or")
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Base  %s"%ficCSVdst,style="Or")
          shutil.copy(ficCSV,ficCSVdst)
          for ft in lstDictFTdeport:
            if ft['dejaEmis']:
              ftDeport="%s_%s-%06d"%(self.projet.REF_FT,idDepot,int(ft['IDdepot']))
              ftBase="%s-%06d"%(self.projet.REF_FT,int(ft['IDbase']))

              if nngsrc.services.SRV_remplaceDsFichier(ficCSVdst,ftDeport,ftBase)!=-1:
                nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s => %s"%(ftDeport,ftBase),style="Or")
#              else:
#                ftDeport="%s_%s-%s"%(self.projet.REF_FT,idDepot,ft['IDdepot'])
#                ftBase="%s-%06d"%(self.projet.REF_FT,-int(ft['IDbase']))
#                if nngsrc.services.SRV_remplaceDsFichier(ficCSVdst,ftDeport,ftBase)!=-1:
#                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s => %s"%(ftDeport,ftBase),style="Or")
            else:
              ftDeport="%s_%s-%s"%(self.projet.REF_FT,idDepot,ft['IDdepot'])
              ftBase="%s-%s"%(self.projet.REF_FT,ft['IDbase'])
              if nngsrc.services.SRV_remplaceDsFichier(ficCSVdst,ftDeport,ftBase)!=-1:
                nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s => %s"%(ftDeport,ftBase),style="Or")
      #--2--- fichiers rapports HTML
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    HTML",style="r")
      listeRapports=[]
      for rapHTML in glob.glob(os.path.join(repImport,"Rapports",'R',"HTML","*")):
        listeRapports.append(rapHTML)
      listeRapports.sort()
      for rapHTML in listeRapports:
        if os.path.isdir(rapHTML):
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    %s"%rapHTML,style="r")
          # recupération du nom de rapport et suppression de l'ID
          rapHTMLdst=os.path.basename(rapHTML).replace("_%s"%idDepot,'')
          # recuperation du numero d'iteration
          num=int(rapHTMLdst.split('-')[-1])
          # copie dans le repertoire du rapport destination
          rapHTMLdst=rapHTMLdst.replace(identifiant,'')
          rapHTMLdst=self.projet.SEPARATEUR_CHAMPS.join(rapHTMLdst.split(self.projet.SEPARATEUR_CHAMPS)[:-1])+self.projet.SEPARATEUR_CHAMPS+"%04d"%(int(self.projet.DOCUMENT_ITERATION)+num)
          nngsrc.services.SRV_copierRepert(rapHTML,os.path.join(repRapports,'R',"HTML",rapHTMLdst))
          #- MAJ fichiers .html
          listeFichiers=[]
          for ficHTML in glob.glob(os.path.join(os.path.join(repRapports,'R',"HTML",rapHTMLdst),"*.html")):
            listeFichiers.append(ficHTML)
          listeFichiers.sort()
          for ficHTML in listeFichiers:
            if os.path.basename(ficHTML) in ['index.html','bandeau.html']:
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Fichier %s"%os.path.basename(ficHTML),style="Or")
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        Depot %s"%os.path.basename(rapHTML),style="Or")
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        Base  %s"%rapHTMLdst,style="Or")
              nngsrc.services.SRV_remplaceDsFichier(ficHTML,os.path.basename(rapHTML),rapHTMLdst)
          # Renommage des FT (HTML)
          for ficHTML in listeFichiers:
            if not os.path.basename(ficHTML) in ['index.html','bandeau.html','menu.html','vide.html']:
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Fichier %s"%os.path.basename(ficHTML),style="Or")
              for ft in lstDictFTdeport:
                if ft['dejaEmis']:
                  ftDeport="%s_%s-%06d"%(self.projet.REF_FT,idDepot,int(ft['IDdepot']))
                  ftBase="%s-%06d"%(self.projet.REF_FT,int(ft['IDbase']))
                  #SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT ? %s => %s"%(ftDeport,ftBase),style="Or")
                  if nngsrc.services.SRV_remplaceDsFichier(ficHTML,ftDeport,ftBase)!=-1:
                    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s => %s"%(ftDeport,ftBase),style="Or")
                    for qualif in ['',
                                   " (%s)"%nngsrc.rapportHTML.formateChaineHTML(self.projet.lexique.entree('MOT_CRITIQUE')),
                                   " (%s)"%nngsrc.rapportHTML.formateChaineHTML(self.projet.lexique.entree('MOT_BLOQUANT')),
                                   " (%s)"%nngsrc.rapportHTML.formateChaineHTML(self.projet.lexique.entree('MOT_MAJEUR')),
                                   " (%s)"%nngsrc.rapportHTML.formateChaineHTML(self.projet.lexique.entree('MOT_MINEUR'))]:
                      #SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT ? %s%s => %s%s (déjà émis)"%(ftBase,qualif,ftBase,qualif),style="Or")
                      if nngsrc.services.SRV_remplaceDsFichier(ficHTML,"%s%s</a>"%(ftBase,qualif),"%s%s (%s)</a>"%(ftBase,qualif,nngsrc.rapportHTML.formateChaineHTML(self.projet.lexique.entree('LOCUT_DEJA_EMIS'))))!=-1:
                        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s%s => %s%s (déjà émis)"%(ftBase,qualif,ftBase,qualif),style="Or")
                else:
                  ftDeport="%s_%s-%06d"%(self.projet.REF_FT,idDepot,int(ft['IDdepot']))
                  ftBase="%s-%06d"%(self.projet.REF_FT,int(ft['IDbase']))
                  if nngsrc.services.SRV_remplaceDsFichier(ficHTML,ftDeport,ftBase)!=-1:
                    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        FT : %s => %s"%(ftDeport,ftBase),style="Or")
      #--3--- fichiers rapports LaTeX
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    LaTeX",style="r")
      REF_FT_LaTeX=self.projet.REF_FT.replace('_','\\-\\_')
      listeRapports=[]
      for rapLaTeX in glob.glob(os.path.join(repImport,"Rapports",'R',"LaTeX","*")):
        listeRapports.append(rapLaTeX)
      listeRapports.sort()
      for rapLaTeX in listeRapports:
        if os.path.isdir(rapLaTeX):
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    %s"%rapLaTeX,style="r")
          # recupération du nom de rapport et suppression de l'ID
          rapLaTeXdst=os.path.basename(rapLaTeX).replace(identifiant,'')
          refLaTeXdst=os.path.basename(rapLaTeX).replace(identifiant,'')
          # recuperation du numero d'iteration
          num=int(rapLaTeXdst.split(self.projet.SEPARATEUR_CHAMPS)[-1])
          # copie dans le repertoire du rapport destination
          rapLaTeXdst=self.projet.SEPARATEUR_CHAMPS.join(rapLaTeXdst.split(self.projet.SEPARATEUR_CHAMPS)[:-1])+self.projet.SEPARATEUR_CHAMPS+"%04d"%(int(self.projet.DOCUMENT_ITERATION)+num)
          nngsrc.services.SRV_copierRepert(rapLaTeX,os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst))
          #- Fichier principal .tex : renommé
          src=os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst,"%s.tex"%os.path.basename(rapLaTeX))
          dst=os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst,"%s.tex"%rapLaTeXdst)
          os.rename(src,dst)
          cherche ="documentRevision{%s.%s.%d}"%(self.projet.DOCUMENT_EDITION,self.projet.DOCUMENT_REVISION,num)
          remplace="documentRevision{%s.%s.%d}"%(self.projet.DOCUMENT_EDITION,self.projet.DOCUMENT_REVISION,int(self.projet.DOCUMENT_ITERATION)+num)
          nngsrc.services.SRV_remplaceDsFichier(dst,cherche,remplace)
          #- Fichier principal .pdf : detruit
          fichier=os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst,"%s.pdf"%os.path.basename(rapLaTeX))
          if os.path.exists(fichier): os.remove(fichier)
          # Fichier de compilation .bat et .sh
          for ext in ['bat','sh']:
            fichier=os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst,"compiler.%s"%ext)
            if os.path.exists(fichier):
              nngsrc.services.SRV_remplaceDsFichier(fichier,os.path.basename(rapLaTeX),rapLaTeXdst)
          # Renommage des FT (LaTeX)
          listeFichiers=[]
          for ficLaTeX in glob.glob(os.path.join(os.path.join(repRapports,'R',"LaTeX",rapLaTeXdst),"*.tex")):
            listeFichiers.append(ficLaTeX)
          listeFichiers.sort()
          for ficLaTeX in listeFichiers:
            nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      Fichier %s"%os.path.basename(ficLaTeX),style="Or")
            if nngsrc.services.SRV_remplaceDsFichier(ficLaTeX,os.path.basename(rapLaTeX),refLaTeXdst)!=-1:
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        Depot %s"%os.path.basename(rapLaTeX),style="Or")
              nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        Base  %s"%refLaTeXdst,style="Or")
            for ft in lstDictFTdeport:
              if ft['dejaEmis']:
                ftDeport="%s\\-\\_%s-%06d"%(REF_FT_LaTeX,idDepot,int(ft['IDdepot']))
                ftBase="%s\\-%06d"%(REF_FT_LaTeX,int(ft['IDbase']))
                #SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT ? %s => %s"%(ftDeport,ftBase),style="Or")
                if nngsrc.services.SRV_remplaceDsFichier(ficLaTeX,ftDeport,ftBase)!=-1:
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s => %s"%(ftDeport,ftBase),style="Or")
                  for qualif in [''," (%s)"%nngsrc.rapportTEX.formateChaineLaTeX(self.projet.lexique.entree('MOT_CRITIQUE')),
                                    " (%s)"%nngsrc.rapportTEX.formateChaineLaTeX(self.projet.lexique.entree('MOT_BLOQUANT')),
                                    " (%s)"%nngsrc.rapportTEX.formateChaineLaTeX(self.projet.lexique.entree('MOT_MAJEUR')),
                                    " (%s)"%nngsrc.rapportTEX.formateChaineLaTeX(self.projet.lexique.entree('MOT_MINEUR'))]:
                    #SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT ? %s%s => %s%s (déjà émis)"%(ftBase,qualif,ftBase,qualif),style="Or")
                    if nngsrc.services.SRV_remplaceDsFichier(ficLaTeX,"%s%s</a>"%(ftBase,qualif),"%s%s (%s)</a>"%(ftBase,qualif,nngsrc.rapportTEX.formateChaineLaTeX(self.projet.lexique.entree('LOCUT_DEJA_EMIS'))))!=-1:
                      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"      FT : %s%s => %s%s (déjà émis)"%(ftBase,qualif,ftBase,qualif),style="Or")
              else:
                ftDeport="%s\\-\\_%s-%s"%(REF_FT_LaTeX,idDepot,ft['IDdepot'])
                ftBase="%s-%s"%(REF_FT_LaTeX,ft['IDbase'])
                if nngsrc.services.SRV_remplaceDsFichier(ficLaTeX,ftDeport,ftBase)!=-1:
                  nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"        FT : %s => %s"%(ftDeport,ftBase),style="Or")

            nngsrc.services.SRV_remplaceDsFichier(ficLaTeX,"\\-\\-%s"%idDepot,'')

      self.projet.DOCUMENT_ITERATION=str(int(self.projet.DOCUMENT_ITERATION)+maxNum)
      self.projet.Sauvegarde(os.path.join(repConfiguration,configurationCourante),repFiches)

    #-- Fiches
    if FRSPAT.find('F')>-1 or FRSPAT.find('f')>-1 :
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import Fiches",style="r")
      nngsrc.services.SRV_copierRepert(os.path.join(repImport,"Fiches"),repFiches,force=(FRSPAT.find('F')>-1))

    #-- Scenarios
    if FRSPAT.find('S')>-1 or FRSPAT.find('s')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import Scenarios",style="r")
      nngsrc.services.SRV_copierRepert(os.path.join(repImport,"Scenarios"),repScenarios,force=(FRSPAT.find('S')>-1))

    #-- Plans
    if FRSPAT.find('P')>-1 or FRSPAT.find('p')>-1 :
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import Plans",style="r")
      nngsrc.services.SRV_copierRepert(os.path.join(repImport,"Plans"),repPlans,force=(FRSPAT.find('P')>-1))

    #-- Autres
    if FRSPAT.find('A')>-1 or FRSPAT.find('a')>-1:
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log," - Import Autres",style="r")
      for repertoire in repAutres.split(';'):
        if repertoire!='':
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"    %s"%repertoire,style="r")
          nngsrc.services.SRV_copierRepert(os.path.join(repImport,os.path.basename(repertoire)),repertoire,force=(FRSPAT.find('A')>-1))

    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"---- Fin d'import --------------------------------------------------------",style="Ir")
    return codeRetour

  #-----------------------------------------------------------------------------------
  ## Fonction principale
  def nangu_Validation(self,arglist=None,referenceSubst=None,editer=True,uiTerm=None):
    if arglist is None: arglist=[]
    codeRetour=nngsrc.services.CODE_OK
    # Gestion du fichier des log
    self.qualite.listeFT={}
    self.qualite.listeExig={}
    self.qualite.listeFct={}
#    self.qualite.nbFTdejaEmis=0
#    if arglist<>[]:
#     if arglist[len(arglist)-1][:6]=='SUBST=':
#      subst=arglist[len(arglist)-1].replace('SUBST=','').split(',')
#      referenceSubst=[[subst[0],subst[1]],subst[2],[subst[3],subst[4]]]
#      arglist.remove(arglist[len(arglist)-1])

    self.projet.uiTerm=uiTerm
    self.projet.log=open('Nangu.log','w',encoding='utf-8')
    self.projet.log.write('['+ time.asctime(time.localtime()) + ']\n')
    lot=[]

    # Simple calcul du nombre de controles pour les statistiques finales
    nbControles=0
    for elem in os.listdir(self.repFiches):
      if elem.find('.xml') != -1 and elem[0]!='_':
        nbControles+=1
    for elem in os.listdir(self.repFiches):
      if elem.find('.xml') == -1:
        elemniv2=os.path.join(self.repFiches,elem)
        if os.path.isdir(elemniv2):
          for elem2 in os.listdir(elemniv2):
            if elem2.find('.xml') != -1 and elem2[0]!='_':
              nbControles+=1
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"------------------------------------------",style="Ir")
    nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_NB_CONTROLES')%nbControles,style="Ir")
    self.listeTags=[]
    self.dicoTags={}
    self.ajustementsTAG=False
    if self.projet.execution in ['X','K']:
      # Structuration des faits techniques
      if os.path.exists(os.path.join(self.repRapport,"nng-analyses","ajustementsTAG.txt")):
        os.remove(os.path.join(self.repRapport,"nng-analyses","ajustementsTAG.txt"))
      self.FICHbaseFT={}# TODO QUALITE ?
      for ft in self.qualite.baseFT:
        #print("DBG>",self.qualite.baseFT)
        #print("DBG>",ft['LIBELLE'].split('\n'))
        fichier=ft['LIBELLE'].split('\n')[1].replace('Fiche   : ','').split('@')[0]
        #print "DBG>",ft.split('@')
        if fichier in self.FICHbaseFT:
          self.FICHbaseFT[fichier].append(ft)
        else:
          self.FICHbaseFT[fichier]=[ft]
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Nombre de faits techniques recencés %s"%len(self.qualite.baseFT),style="Ir")

    # 1- Lancement sans fiche, fichier liste ou repertoire
    IDcahier=None
    if not arglist:
      self.projet.nivRapport=0
      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_LANCEMENT_RIEN'),style="Ir")
      # lister les fichiers de fiches .xml
      lotRep=[]
      for elem in os.listdir(self.repFiches):
        if elem.find('.xml') != -1 and elem[0]!='_':
          lot.append(os.path.splitext(elem)[0])
      for elem in os.listdir(self.repFiches):
        if elem.find('.xml') == -1:
          elemniv2=os.path.join(self.repFiches,elem)
          if os.path.isdir(elemniv2):
            lst=self.locTraiteRepertoire(os.path.join(self.repFiches,elem),elemniv2,[])
            for l in lst:
              l=l.replace(os.path.join(self.repFiches,''),'')
              lotRep.append(l)

      # tri des fiches dans l'ordre alphabetique
      #lot.sort(key=lambda x,y:cmp(x.lower(),y.lower()))
      lot=sorted(lot)
      # tri des repertoires dans l'ordre alphabetique
      #lotRep.sort(key=lambda x,y:cmp(x.lower(),y.lower()))
      lotRep=sorted(lotRep)
      # fiches seules en premier, puis les repertoires de fiches
      for elem in lotRep:
        lot.append(elem)

    # 2- Mode d'execution V : la donnée fournie en paramètre est un ficher liste de scenario #v8.2rc2 Enregistrement à la volée
    elif self.projet.execution[0]=="V":
      self.projet.nivRapport=0
      self.projet.nomFicheALaVolee=arglist[len(arglist)-2]+self.projet.PRODUIT_SOUS_REFERENCE
      self.projet.nomRapportALaVolee=self.projet.PRODUIT_REFERENCE+self.projet.PRODUIT_SOUS_REFERENCE+".lst"
      self.projet.nomRepertoireALaVolee="A-La-Volee%s"%self.projet.PRODUIT_SOUS_REFERENCE
      self.projet.nomScenarioALaVolee=arglist[len(arglist)-1]
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements")):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements"))
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches")):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches"))
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee)):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee))
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios")):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios"))
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios",self.projet.nomScenarioALaVolee)):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios",self.projet.nomScenarioALaVolee))
      if not os.path.exists(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee,self.projet.nomScenarioALaVolee)):
        os.mkdir(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee,self.projet.nomScenarioALaVolee))
      if os.access(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios",self.projet.nomScenarioALaVolee,self.projet.PRODUIT_REFERENCE+self.projet.PRODUIT_SOUS_REFERENCE+".lst"),os.F_OK):
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Enregistrement du scénario à la volée %s impossible car déjà existant."%os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios",self.projet.nomScenarioALaVolee,self.projet.PRODUIT_REFERENCE+self.projet.PRODUIT_SOUS_REFERENCE+".lst"),style="Er")
      elif os.access(os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee,self.projet.nomFicheALaVolee)+'_1.xml',os.F_OK):
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Enregistrement de fiches à la volée impossible car déjà existantes dans %s_*.xml"%os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee,self.projet.nomFicheALaVolee),style="Er")
      else:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"Enregistrement de document et fiches à la volée dans %s_*.xml"%os.path.join(self.repFiches,"..","magV-enregistrements","Fiches",self.projet.nomRepertoireALaVolee,self.projet.nomFicheALaVolee),style="Ir")
        self.projet.enregistrement=1
        f=open(os.path.join(self.repFiches,"..","magV-enregistrements","Scenarios",self.projet.nomScenarioALaVolee,self.projet.nomRapportALaVolee),"a",encoding='utf-8')
        f.write('SOUS-TITRE %s\n'%self.projet.PRODUIT_SOUS_TITRE)
        f.write('SOUS-REFERENCE %s\n'%self.projet.PRODUIT_SOUS_REFERENCE)
        if self.projet.version>=9.0:
          f.write('METHODE T\n')
        f.write('%s\n'%self.projet.nomRepertoireALaVolee)
        f.close()
        self.projet.execution=self.projet.execution[1]
        lot=[]

    # 3- Plus de quatre parametres : lancement avec fiche, fichier liste ou repertoire specifique
    else:
      for elem in arglist:
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_LANCEMENT_SUR')%elem,style="Ir")

        # Extension XML : la donnée fournie en paramètre est une fiche specifique
        if os.access(os.path.join(self.repFiches,elem)+'.xml',os.F_OK):
          self.projet.nivRapport = 0
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_FICHE')%os.path.join(self.repFiches,elem),style="Ir")
          lot.append(elem)

        # Extension LST : la donnée fournie en paramètre est un ficher liste de scenario
        elif os.access(os.path.join(self.repScenarios,elem)+'.lst',os.F_OK):
          self.projet.nivRapport = 1
          IDcahier=elem
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_SCENARIO')%os.path.join(self.repScenarios,elem),style="Ir")
          lot=self.locTraiteFichierLST(self.repScenarios,self.repFiches,lot,elem)
          self.variables[nngsrc.services.SRV_IndexVariable('SCENARIO',self.variables)]['VALEUR']=elem

        # Extension LST : la donnée fournie en paramètre est un ficher liste
        elif os.access(os.path.join(self.repFiches,elem)+'.lst',os.F_OK):
          self.projet.nivRapport = 1
          IDcahier=elem
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_LISTE')%os.path.join(self.repFiches,elem),style="Ir")
          lot=self.locTraiteFichierLST(self.repFiches,self.repFiches,lot,elem)

        # Extension PV : la donnée fournie en paramètre est un plan de validation
        elif os.access(os.path.join(self.repPlans,elem)+'.pv',os.F_OK):
          self.projet.nivRapport = 2
          IDcahier=elem
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_PLAN')%os.path.join(self.repPlans,elem),style="Ir")
          self.variables[nngsrc.services.SRV_IndexVariable('PLAN',self.variables)]['VALEUR']=elem
          if self.typePRJsimple:
            lot=self.locTraiteFichierLST(self.repPlans,self.repFiches,lot,elem)
          else:
            lot=self.locTraiteFichierPV(self.repPlans,self.repFiches,elem,referenceSubst)

        else:
          self.projet.nivRapport = 0
          nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,self.projet.lexique.entree('LOCUT_TRAITEMENT_REPERTOIRE')%elem,style="Ir")
          lot=self.locTraiteRepertoire(self.repFiches,elem,lot)

    self.rapport=nngsrc.rapport.Rapport(IDcahier,self.constantes,self.repTravail,self.repRessources,self.repRapport,self.projet,self.qualite,self.version,editRapport=(not self.projet.execution in ['X','K']))
    self.fonctions.rapport=self.rapport
#    self.fonctionsIHM.rapport=self.rapport

    # Contrôle de présence d'un répertoire d'images
    repImage=os.path.join(self.repFiches,"_images")
    if not os.access(repImage,os.F_OK):
      repImage=None

    # Lancement de Nangu sur le lot de fiches
    self.Scheduler(lot,repImage,nbControles,IDcahier)

#    print "Timer a supprimer avec nettoyage du log"

    for elem in os.listdir(self.repTravail):
      if (elem.find('.log')!=-1 and self.projet.debug<2) or \
          elem in ['bilan.txt','nonCouv.txt','nonList.txt','notesManuelles.bbc'] or \
          elem.find('Annexe')==0:
        ficdest=os.path.join(self.repRapport,self.projet.execution,elem)
        if os.access(ficdest,os.F_OK):
          nngsrc.services.SRV_detruire(ficdest,timeout=5,signaler=True)
        nngsrc.services.SRV_deplacer(os.path.join(self.repTravail,elem),os.path.join(self.repRapport,self.projet.execution),timeout=5,signaler=True)
      else:
        print("Attention : Non supprimé",elem)

    time.sleep(2)

    os.rmdir(self.repTravail)

    if self.projet.debug<2:
      for elem in glob.glob('*.err'):
        nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
      for elem in glob.glob('*.out'):
        nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
      for elem in glob.glob('*.flg'):
        nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)

    if self.projet.execution in ['X','K','S']:
      bilan=False
      for v in self.variables:
        if 'FICHES' in v:
          if v['UTILE']:
            f=open(os.path.join(self.repRapport,"nng-analyses","variables","utiles","%s.var"%v['NOM']),"w",encoding='utf-8')
          else:
            f=open(os.path.join(self.repRapport,"nng-analyses","variables","inutiles","%s.var"%v['NOM']),"w",encoding='utf-8')
          f.write("NOM               : %s\n"%v['NOM'])
          f.write("UTILE             : %s\n"%v['UTILE'])
          f.write("COMMENTAIRE       : %s\n"%v['COMMENTAIRE'])
          f.write("INDEXER           : %s\n"%v['INDEXER'])
          f.write("PERSISTANCE       : %s\n"%v['PERSISTANCE'])
          if v['VALEUR'] is None:
            print("Anomalie de valeur sur ",v)
          else:
            f.write("VALEUR (derniere) : %s\n"%v['VALEUR'])
          f.write("UTILISATION - FICHES  :\n")
          for fic in v['FICHES']:
            f.write("%s\n"%fic)
          f.close()

    if self.projet.execution in ['X','K']:
      f=open(os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),"a",encoding='utf-8')
      if len(self.projet.lstNoComment)!=0:
        f.write("\nFiches dont certaines actions ne sont pas commentées :\n")
        for e in self.projet.lstNoComment:
          f.write("%s\n"%e)
        bilan=True
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Des fiches ont des actions non commentées. Consulter le fichier '%s'."%os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),style="Wr")
      if len(self.projet.dicoVariables['indefini'])!=0:
        f.write("\nVariable non definies dans des fiches :\n")
        for e in self.projet.dicoVariables['indefini']:
          f.write("%s dans %s\n"%e)
        bilan=True
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Des variables sont indefinies. Consulter le fichier '%s'."%os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),style="Wr")
      if len(self.notRep)!=0:
        f.write("\nLes repertoires suivants n'existent pas mais sont references dans le fichier de liste en entree.\n")
        for r in self.notRep:
          f.write("%s\n"%r)
        bilan=True
        nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,None,"Des répertoires référencés n'existent pas. Consulter le fichier '%s'."%os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),style="Er")
      f.close()
      if bilan:
        nngsrc.services.SRV_EditerTxt(os.path.join(self.repRapport,"nng-analyses","BilanControleFiches.txt"),editer)
        codeRetour=nngsrc.services.CODE_ANOMAL
      if len(self.FICHbaseFT)>0:
        f=open(os.path.join(self.repRapport,"nng-analyses","FTerronnes.txt"),"w",encoding='utf-8')
        for fic in self.FICHbaseFT.keys():
          for ft in self.FICHbaseFT[fic]:
            ligne=ft['LIBELLE']
            f.write("%s\n"%ligne)
        f.close()
        codeRetour=nngsrc.services.CODE_ANOMAL

    self.projet.log.close()
    return codeRetour

  #-----------------------------------------------------------------------------------
  # recuperation des informations depuis le gestionnaire de FT
  def nangu_Todo(self,pversion,ptarget,pdepot,uiTerm=None,debug=None):
    date=time.strftime('-%d-%m-%y-%H-%M-%S-',time.localtime(time.time()))
    if not os.path.exists(pdepot):
      os.makedirs(pdepot)
    fichierT=os.path.join(pdepot,self.projet.PRODUIT_NOM.replace(' ','-')+date+"tous.csv")
    fichierO=os.path.join(pdepot,self.projet.PRODUIT_NOM.replace(' ','-')+date+"ouverts.csv")
    fichierR=os.path.join(pdepot,self.projet.PRODUIT_NOM.replace(' ','-')+date+"resolus.csv")
    if pversion=="-":
      pversion=None
    else:
      pversion=pversion.split(',')
    if ptarget=="-":
      ptarget=None
    else:
      ptarget=ptarget.split(',')

    ListeID=[]
    for t in self.qualite.baseFT:
      ListeID.append(t['ID'])
    ListeID=sorted(ListeID)
    # Tous les FT
    nngsrc.services.SRV_Terminal(uiTerm,debug,None,self.projet.lexique.entree('LOCUT_BILAN_TODO'),style="Or")
    nngsrc.services.SRV_Terminal(uiTerm,debug,None,"%s : %s"%(self.projet.lexique.entree('MOT_TOUS'),fichierT),style="Or")
    f=open(fichierT,"w",encoding='utf-8')
    f.write('ID;TYPE;VERSION;ECHEANCE;GRAVITE;STATUT;VERSION;LIBELLE\n')
    tkidold=''
    for tkid in ListeID:
      if tkid!=tkidold:
        for t in self.qualite.baseFT:
          if t['ID']==tkid:
            ligne=t['ID']+';'+t['TYPE']+';'+t['VERSION']+';'+t['ECHEANCE']+';'+self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%t['GRAVITE'])+';'+t['STATUT']+';'+'fixed_in_version_AD'+';'+t['LIBELLE'].replace('\n',', ').replace('\r','')+'\n'
            f.write(ligne)
            break
        tkidold=tkid
    f.close()
    # Tous les FT ouverts
    nngsrc.services.SRV_Terminal(uiTerm,debug,None,"%s : %s"%(self.projet.lexique.entree('MOT_OUVERTS'),fichierO),style="Or")
    f=open(fichierO,"w",encoding='utf-8')
    f.write('ID;VERSION;ECHEANCE;GRAVITE;STATUT;LIBELLE\n')
    tkidold=''
    for tkid in ListeID:
      if tkid!=tkidold:
        for t in self.qualite.baseFT:
          if t['ID']==tkid and t['TYPE']!="Evolution" and t['STATUT'] not in nngsrc.qualite.DejaClosFT:
            if pversion is None or (pversion and t['VERSION'] in pversion):
              ligne=t['ID']+';'+t['VERSION']+';'+t['ECHEANCE']+';'+self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%t['GRAVITE'])+';'+t['STATUT']+';'+t['LIBELLE'].replace('\n',', ').replace('\r','')+'\n'
              f.write(ligne)
              break
        tkidold=tkid
    f.close()
    # Tous les FT résolus (dans DejaClosFT)
    nngsrc.services.SRV_Terminal(uiTerm,debug,None,"%s : %s"%(self.projet.lexique.entree('MOT_RESOLUS'),fichierR),style="Or")
    f=open(fichierR,"w",encoding='utf-8')
    f.write('ID;VERSION;ECHEANCE;GRAVITE;STATUT;LIBELLE\n')
    for tkid in ListeID:
      for t in self.qualite.baseFT:
        if t['ID']==tkid and t['TYPE']!="Evolution" and t['STATUT'] in nngsrc.qualite.DejaClosFT:
          if ptarget is None or (ptarget and t['VERSION'] in ptarget):
            ligne=t['ID']+';'+t['VERSION']+';'+t['ECHEANCE']+';'+self.projet.lexique.entree('MOT_NIVEAU_CRITIQUE_%d'%t['GRAVITE'])+';'+t['STATUT']+';'+t['LIBELLE'].replace('\n',', ').replace('\r','')+'\n'
            f.write(ligne)
            break
    f.close()

    return fichierT, fichierO, fichierR

  #-----------------------------------------------------------------------------------
  # recuperation des informations depuis le gestionnaire de FT
  def nangu_MiseAJour(self,fichierPRJ,projetPRJ,uiTerm=None,debug=None):
    if self.projet.version==8.4:
      print("Mise à jour de la version v%s à la version v9.0."%self.projet.version)
      self.projet.version=9.0
      self.projet.strVersion='9.0'
      # Fichier methode.txt : ajout du fichier
      self.projet.PRODUIT_METHODE=os.path.join(self.repFiches,'..','Qualite','methode.txt')
      self.projet.REFERENCE_IDENTIFIANT="REF-{MODE}-MAG-{VERSION}{SOUSREF}"
      self.projet.SEPARATEUR_SUFFIXE=self.separateurSuffixe
      self.projet.SEPARATEUR_CHAMPS=self.separateurChamps
      self.projet.NOMBRE_CHAMPS=int(self.NbChamps)
      self.projet.REFERENCE_IDENTIFIANT="REF-{MODE}-MAG-{VERSION}{SOUSREF}"

      nngsrc.services.SRV_copier(os.path.join(os.getenv("NANGU","."),'ressources','methode.txt'),self.projet.PRODUIT_METHODE,signaler=True)
      # Exigences & fonctionnalités
      for fichier,terme in [(self.projet.PRODUIT_EXIGENCES[0],"Exigences"),(self.projet.PRODUIT_FONCTIONNALITES[0],"Fonctionnalités")]:
        f=open(fichier,"r",encoding='utf-8')
        contenu=f.readlines()
        f.close()
        f=open(fichier,"w",encoding='utf-8')
        f.write("INDEX 1\n")
        f.write("TYPE_EXIGENCES %s projet\n"%terme)
        f.write("COMMENTAIRE Les exigences %s sont des %s projet.\n"%(terme.lower(),terme.lower()))
        f.write("NOMBRE_COLONNE %d\n"%self.projet.MATTRAC_FORMAT_COLONNE)
        for ligne in contenu:
          ligne=ligne.replace('\n','').replace('\r','')+"|Ap|T\n"
          f.write(ligne)
      # Plans et scénarios
      for repert,extension in [(self.repPlans,'pv'),(self.repScenarios,'lst')]:
        for fichier in glob.glob(os.path.join(repert,"*.%s"%extension)):
          f=open(fichier,"r",encoding='utf-8')
          contenu=f.readlines()
          f.close()
          f=open(fichier,"w",encoding='utf-8')
          f.writelines(contenu[:2])
          f.write("METHODE T\n")
          f.writelines(contenu[2:])
          f.close()
      self.projet.Sauvegarde(self.configFile,self.repFiches)

      # Création du fichier de configuration projet
      ecrireProjet_MAJ(fichierPRJ,projetPRJ)
    else:
      print("Version v%s à jour."%self.projet.version)

#-----------------------------------------------------------------------------------
# recuperation des informations de version
def nangu_Version(fichier="Nangu_version.txt"):
  print("--------------------------------------")
  version,_,_=nngsrc.services.VERSION()
  print("Nangu version\n%s"%version)
  f=open(fichier,"w",encoding='utf-8')
  f.write("%s\n"%version)
  f.close()
  print("--------------------------------------")

#-----------------------------------------------------------------------------------
# Creation d'un nouveau projet
def nangu_ClefUtilisation(projetPRJ,fichierClef):
  (_,_,(repConfiguration,_),_,_,_,_,_)=projetPRJ

  if not os.path.exists(fichierClef):
    print("Le fichier clef '%s' est introuvable."%fichierClef)
    return

  fichierImage=os.path.join(repConfiguration,"logoNangu.png")
  if not os.path.exists(fichierImage): return

  f=open(fichierClef,'r',encoding='utf-8')
  clefComplete=f.readlines()
  f.close()
  clefComplete=clefComplete[0].replace('\n','').replace('\r','')
  clef=clefComplete[:-2]
  pixelCentre=int(clefComplete[-2:],16)
#  print clefComplete,clef,clefComplete[-2:],pixelCentre
  mpLicence.crypto.steganoCode(fichierImage,clef,pixelCentre,fichierImage)

#-----------------------------------------------------------------------------------
def ecrireProjet(chemin,dicoEnv,typePRJsimple,radicalScenarios,reference,configXml,autres=''):
  NANGUPRJ=os.getenv('NANGUPRJ','.')
  _,versPRJ,_=nngsrc.services.VERSION()
  f=open(chemin,"w",encoding='utf-8')
  f.write("version=%s\n"%versPRJ)
  if typePRJsimple:
    f.write("TypeProjet=simple\n")
  else:
    f.write("TypeProjet=complexe\n")
  f.write("Configuration=%s\n"%os.path.join("[NANGUPRJ]","Configuration"))
  f.write("Fiches=%s\n"%os.path.join("[NANGUPRJ]","Fiches"))
  f.write("Rapports=%s\n"%os.path.join("[NANGUPRJ]","Rapports"))
  f.write("Scenarios=%s\n"%os.path.join("[NANGUPRJ]","Scenarios"))
  f.write("Plans=%s\n"%os.path.join("[NANGUPRJ]","Plans"))
  f.write('Campagnes=%s\n'%os.path.join("[NANGUPRJ]","Campagnes"))
  f.write('Autres=%s\n'%autres)
  f.write("repertoireCourant=\n")
  f.write("configurationCourante=%s\n"%configXml)
  f.write("typeDocument=P\n")
  f.write("listeCourante=\n")
  f.write("planCourant=\n")
  f.write("menuRepertoires=N\n")
  f.write("menuListes=O\n")
  f.write("ouverts=-\n")
  f.write("resolus=-\n")
  f.write("lstRapports=listeRapports.cfg\n")
  f.write("controleFiches=controlerFiches.lst\n")
  f.write("-----Variables--------------------------\n")
  for k in dicoEnv.keys():
    f.write("%s=%s\n"%(k,dicoEnv[k].replace(NANGUPRJ,"[NANGUPRJ]")))
  f.close()

#-----------------------------------------------------------------------------------
# Creation d'un nouveau projet
def nangu_NouveauProjet(projet,repertoire,typePRJsimple):
    projet=projet.replace('.prj','')
    # Création du répertoire projet
    if not os.access(repertoire,os.F_OK):
      os.makedirs(repertoire)

    os.chdir(repertoire)
    repertoire=os.getcwd()

    # Extraction du projet type
   # print "Création du projet %s dans %s."%(projet,repertoire)
    if not os.path.exists(os.path.join(nngsrc.services.varNANGU,"ressources","projet.zip")):
      print("Le fichier %s est introuvable."%os.path.join(nngsrc.services.varNANGU,"ressources","projet.zip"))
      return 1

    ficzip=zipfile.ZipFile(os.path.join(nngsrc.services.varNANGU,"ressources","projet.zip"),"r")
    ficzip.extractall(repertoire)
    ficzip.close()

    # Création du répertoire des rapports
    if not os.access(os.path.join(repertoire,"Rapports"),os.F_OK):
      os.mkdir(os.path.join(repertoire,"Rapports"))

    # Création du fichier de configuration projet
    chemin=os.path.join(repertoire,str(projet)+".prj")
    autres="%s;%s;%s"%(os.path.join("[NANGUPRJ]","ComplementDoc"),os.path.join("[NANGUPRJ]","DonneesEntr"),os.path.join("[NANGUPRJ]","DonneesRef"))
    dico={"BINAIRES":os.path.join("[NANGUPRJ]","DonneesEntr","bin"),"DATAREF":os.path.join("[NANGUPRJ]","DonneesRef"),"DATAOUT":os.path.join("[NANGUPRJ]","DonneesProd"),"DATAIN":os.path.join("[NANGUPRJ]","DonneesEntr")}
    ecrireProjet(chemin,dico,typePRJsimple,'',"REF-%VAL-XXXX","config.xml",autres)

    # Création du fichier de de lancement de l'IHM
#    if sys.platform=="win32":
#      chemin=os.path.join(repertoire,"lancement.bat")
#      f=open(chemin,"w")
#      f.write("@echo off\n")
#      f.write("rem -----------------------------------------------\n")
#      f.write("set PYTHONPATH=%NANGU%;%PYTHONPATH%\n")
#      f.write("set NANGUPRJ=%CD%\n")
#      f.write("set NANGUWK=%CD%\work\n")
#      f.write("mkdir %NANGUWK%\n")
#      f.write("rem -----------------------------------------------\n")
#      f.write("set DATAIN=%NANGUPRJ%\\DonneesEntr\n")
#      f.write("set DATAREF=%NANGUPRJ%\\DonneesRef\n")
#      f.write("set DATAIHM=%NANGUPRJ%\\DonneesIhm\n")
#      f.write("set DATAOUT=%NANGUPRJ%\\DonneesProd\n")
#      f.write("set BINAIRES=%NANGUPRJ%\\DonneesEntr\\bin\n")
#      f.write("rem -----------------------------------------------\n\n")
#      f.write("python %%NANGU%%\nangu.py --gui %s.prj\n\n"%str(projet))
#      f.write("rem -----------------------------------------------\n")
#      f.write("rem Fin\n")
#      f.close()
#    else:
#      chemin=os.path.join(repertoire,"NANGUcfg.sh")
#      f=open(chemin,"w")
#      f.write("#!/usr/bin/env bash\n")
#      f.write("#-----------------------------------------------------------------------\n")
#      f.write("\n")
#      f.write("export DEPLOIEMENTS=\"cest-ou_cest-la\"\n")
#      f.write("\n")
#      f.write("#--[mpOUTILS]-----------------------------------------------------------\n")
#      f.write("export MPOUTILS=${DEPLOIEMENTS}/mpOutils\n")
#      f.write("export PYTHONPATH=${MPOUTILS}/mpJobs:${MPOUTILS}/mpGraphiques:${MPOUTILS}/mpLicence:${MPOUTILS}/mpFormule:${MPOUTILS}/TUI:${PYTHONPATH}\n")
#      f.write("export PATH=${MPOUTILS}/mpGraphiques:${PATH}\n")
#      f.write("\n")
#      f.write("#--[NANGU]--------------------------------------------------------------\n")
#      f.write("export NANGU=${DEPLOIEMENTS}/Nangu/nangu\n")
#      f.write("export PYTHONPATH=${NANGU}/nngsrc:${PYTHONPATH}\n")
#      f.write("export PATH=${NANGU}/bin:/usr/bin:$PATH\n")
#      f.write("export EDIT=geany\n")
#      f.write("export COMPARE=bcompare\n")
#      f.write("\n")
#      f.write("#--[PYTHON]-------------------------------------------------------------\n")
#      f.write("export PYTHONHOME=${DEPLOIEMENTS}/Python-2.7.11\n")
#      f.write("export LD_LIBRARY_PATH=${PYTHONHOME}/lib:${LD_LIBRARY_PATH}\n")
#      f.write("export PYTHONPATH=${PYTHONHOME}/lib:${PYTHONHOME}/share:${PYTHONPATH}\n")
#      f.write("export PYTHON=${PYTHONHOME}/bin/python\n")
#      f.write("\n")
#      f.write("#-----------------------------------------------------------------------\n")
#      f.write("# Fin\n")
#      f.close()
#      os.chmod(os.path.join(repertoire,"NANGUcfg.sh"),0o775)
#      chemin=os.path.join(repertoire,"lancement.sh")
#      f=open(chemin,"w")
#      f.write("#!/usr/bin/env bash\n")
#      f.write("#-----------------------------------------------------------------------\n")
#      f.write("\n")
#      f.write("#--[NANGU-Projet]-------------------------------------------------------\n")
#      f.write("export NANGUPRJ=${PWD}\n")
#      f.write("export NANGUWK=${PWD}/work\n")
#      f.write("mkdir -p ${NANGUWK}\n")
#      f.write("\n")
#      f.write("export DATAIN=${NANGUPRJ}/DonneesEntr\n")
#      f.write("export DATAREF=${NANGUPRJ}/DonneesRef\n")
#      f.write("export DATAIHM=${NANGUPRJ}/DonneesIhm\n")
#      f.write("export DATAOUT=${NANGUPRJ}/DonneesProd\n")
#      f.write("export BINAIRES=${NANGUPRJ}/DonneesEntr/bin\n")
#      f.write("#-----------------------------------------------------------------------\n")
#      f.write("\n")
#      f.write("${PYTHON} ${NANGU}/nangu.py --gui %s.prj\n"%projet)
#      f.write("\n")
#      f.write("#-----------------------------------------------------------------------\n")
#      f.write("# Fin\n")
#      f.close()
#      os.chmod(os.path.join(repertoire,"lancement.sh"),777)
    return 0

#-----------------------------------------------------------------------------------
def TrouverMatches(txt,bl):
  res=''
  ctmp='a'

  i=0
  j=0
  for ctmp in txt[i]:
    if bl[i]:
      res+=ctmp
      j+=1
    i+=1
  return res

#-----------------------------------------------------------------------------------
def JaroWinkler(t1,t2):
  t1=t1.replace(' ','')
  t2=t2.replace(' ','')
  l1=len(t1)
  l2=len(t2)
  if l1==0 or l2==0: return 0.0
  ecartMax=(max(l1,l2)/2)-1
  compteMatching=0
  b1=[]
  b2=[]
  ll=max(l1,l2)
  for i in range(ll): b1.append(False)
  for i in range(ll): b2.append(False)
#  print l1,l2,ll
#  print l1,max(i-ecartMax,0),min(i+ecartMax,l2)

  for i in range(l1):
    j=max(i-ecartMax,0)
    while j<min(i+ecartMax,l2):
      if t1[i]==t2[j]:
        b1[i]=True #Indique qu'on a bien trouvé ce caractère
        b2[j]=True
        compteMatching+=1 #Incrémente le nombre de caractères correspondants
        break
      j+=1
  if compteMatching==0: return 0.0

  t1Matche=TrouverMatches(t1,b1) #Génére la liste des caractères communs dans l'ordre de t1
  t2Matche=TrouverMatches(t2,b2)

  compteTransposition=0
  if t1Matche==t2Matche:
    for i in range(len(t1Matche)):
      if t1Matche[i]!=t2Matche[i]:
        compteTransposition+=1 #Calcul le nombre de transpositions
  else:
    compteTransposition=0

  distanceJaro=((float(compteMatching)/l1)+(float(compteMatching)/l2)+((compteMatching-compteTransposition/2.0)/compteMatching))/3.0

  longueurPrefix=0
  for i in range(min(3,min(l1,l2))+1): #longueur max : 4
    if t1[i]==t2[i]:
      longueurPrefix+=1
    else:
      break
  return distanceJaro+(longueurPrefix*0.1*(1-distanceJaro))

#---------------------------------------------------------------------------------
def retraitPath(chaine,ouv,ferm,remplace=None):
  idxDeb=chaine.find(ouv)
#  print idxDeb
  while idxDeb!=-1:
    chaine=chaine.replace(ouv,'_',1)
    idxfin=chaine.find(ferm)
#    print idxfin
#    print chaine
#    print ">",chaine[:idxDeb]
#    print ">",chaine[idxfin:]
    chaine=chaine[:idxDeb]+remplace+chaine[idxfin+len(ferm):]
    idxDeb=chaine.find(ouv)
  return chaine
#---------------------------------------------------------------------------------
def recupSousTexte(chaine,ouv,ferm):
  idxDeb=chaine.find(ouv)
  element=[]
  while idxDeb!=-1:
    chaine=chaine.replace(ouv,'_',1)
    idxfin=chaine.find(ferm)
#    print idxfin
#    print ">",chaine[idxDeb:idxfin]
    element.append(chaine[idxDeb+1:idxfin])
    chaine=chaine[:idxDeb]+chaine[idxfin+1:]
#    print ">",chaine
    idxDeb=chaine.find(ouv)
  return element,chaine
#---------------------------------------------------------------------------------
def initialiseFonctions():
  listeFonctions=[]
  listeBalises=["FONCTION","DESCRIPTION","PARAMETRES","RETOUR","TEXTE","IHM"]
  for fichier in ["fonctions.py"]:#,"fonctionsIHM.py"]:
    lignes=nngsrc.services.SRV_LireContenuListe(os.path.join(nngsrc.services.varNANGU,"nngsrc",fichier))
    for idligne in range(len(lignes)):
      if lignes[idligne][:19]=="  # Integration IHM":
        l=idligne+1
        dico={}
        for numLigne in range(len(listeBalises)):
          ligne=lignes[l+numLigne].replace('\n','').replace('\t','').split(" : ")
          entree=ligne[0].replace(' ','').replace('#','')
          if entree in listeBalises:
            dico[entree]=ligne[1]
        if entree in dico:listeFonctions.append(dico)
  return listeFonctions
#-----------------------------------------------------------------------------------
# Conception :
# Le fichier de fonctions contient dans le tag Lexique, la ref vers le texte à comparer
# le lexique contient le texte avec %s possibles
#
## Fonction de convertion d'un document texte en fiches
def nangu_ConvFiche(ficDocument,repFiches):
  if not os.path.exists(ficDocument):
    nngsrc.services.SRV_Terminal(None,None,None,"Le fichier '%s' n'existe pas"%ficDocument,style="Er")
    return
  document=nngsrc.services.SRV_LireContenuListe(ficDocument)
  (chaineXML,ficFiche)=conversionLignes(document)
  #print os.path.dirname(ficFiche)
  repert=os.path.join(repFiches,os.path.dirname(ficFiche))
  if not os.access(repert,os.F_OK):
    os.mkdir(repert)
  f=open(os.path.join(repFiches,ficFiche),"w",encoding='utf-8')
  f.write(chaineXML)
  f.close()

#-----------------------------------------------------------------------------------
def conversionLignes(document):
  listeFonctions=initialiseFonctions()
#  print listeFonctions
#  for fct in listeFonctions:
#    print fct
#    print fct["FONCTION"],":",fct["TEXTE"]
#  print
  bloc=None
  convDicoLex={}
  FICHEon=False
  ENVIRONNEMENTon=False
  ACTIONSon=False
  ficFiche=''
  for fct in listeFonctions:
    convDicoLex[fct["TEXTE"]]=(fct["FONCTION"],nngsrc.services.SRV_FormateChaineASCII(nngsrc.services.SRV_Encodage(fct["TEXTE"]).lower()).replace("[IHM:%s:MHI]","@@"))
  chaineXML=''
  for ligne in document:
    if ligne[0]!=';':
      ligne=ligne.split(" : ")
      if ligne[0]=="FICHE":
        FICHEon=True
        # creer une fiche
        ficFiche=ligne[1]
#        print "DBG>",ficFiche
        chaineXML+='<?xml version="1.0" encoding="utf-8"?>\n'
        chaineXML+='<FICHE version="4.0">\n'
        chaineXML+='  <DESCRIPTION>\n'
        chaineXML+='    <ENTITE></ENTITE>\n'
        chaineXML+='    <METHODE></METHODE>\n'
        chaineXML+='    <TYPE></TYPE>\n'
        chaineXML+='    <NATURE></NATURE>\n'
        chaineXML+='    <COMMENTAIRE></COMMENTAIRE>\n'
        chaineXML+='    <EXIGENCES></EXIGENCES>\n'
        chaineXML+='  </DESCRIPTION>\n'
      elif ligne[0]=="ENVIRONNEMENT":
        ENVIRONNEMENTon=True
        bloc="env"
        chaineXML+='  <ENVIRONNEMENT>\n'
      elif ligne[0]=="ACTIONS":
        ACTIONSon=True
        bloc="env"
        chaineXML+='  </ENVIRONNEMENT>\n'
        chaineXML+='  <ACTIONS>\n'
      elif ligne[0]=="FICHIER":
        chaineXML+='<FICHIER>%s</FICHIER>\n'%ligne[1]
      elif ligne[0]=="ELEMENT":
        valeur=ligne[2]
        if valeur[0]=='@':
          valeur='join('+valeur[1:].replace('@',')')
        #f=open(os.path.join(repFiches,ficFiche),"a")
        chaineXML+='    <ELEMENT>\n'
        chaineXML+='      <NOM>%s</NOM>\n'%ligne[1]
        chaineXML+='      <VALEUR>%s</VALEUR>\n'%valeur
        chaineXML+='      <TYPE>ND</TYPE>\n'
        chaineXML+='    </ELEMENT>\n'
      elif ligne[0]=="(BOUCLE":
        if len(ligne)==1:
          chaineXML+='    <BOUCLE>\n'
        else:
          chaineXML+='    <BOUCLE %s>\n'%ligne[1]
      elif ligne[0]=="BOUCLE)":
        chaineXML+='    </BOUCLE>\n'
      elif ligne[0]=="LISTE":
        chaineXML+='    <LISTE var="%s">%s</LISTE>\n'%(ligne[1],ligne[2])
      elif ligne[0]=="ACTION":
        print("-------------------------------")
        texte=ligne[1]
        formTexte=nngsrc.services.SRV_FormateChaineASCII(nngsrc.services.SRV_Encodage(texte.lower()))
        for (e,s) in [("{click_gauche}","%s"),("{click_droit}","%s"),("{double_click}","%s")]:
          formTexte=formTexte.replace(e,s)
        #formTexte=retraitPath(formTexte,'{@','@}',"'[ihm:%s:mhi]'")
        formTexte=retraitPath(formTexte,'{@','@}',"%s")
        formTexte=retraitPath(formTexte,'{','}',"'%s'")
        print("LIGNE TXT>",formTexte)

        maxim=0.0
        kmax=-1
#        print convDicoLex
        for key in convDicoLex.keys():
          (_,textLex)=convDicoLex[key]
          JW=jaro.jaro_winkler_metric(formTexte,textLex)
          print("JW>","%3.5f"%(JW*100.),textLex)
          if JW>maxim:
            maxim=JW
            kmax=key

        texte=nngsrc.services.SRV_Encodage(texte)
        print("JW-select>",convDicoLex[kmax])
        print("          ",maxim,kmax)
        for fct in listeFonctions:
          if fct["TEXTE"]==kmax:
            break
        print("DBG-Lexique>",convDicoLex[kmax])
        print("DBG-scoreJaro>",maxim)
        parametres=fct["PARAMETRES"].split("|")
        print("DBG-params>",parametres)
        nbParam=0
        for p in parametres:
          if p.find(':')>0:nbParam+=1
        nbRetour=0
        retour=''
        if fct["RETOUR"].find(":")>0:
          nbRretour=1
        par,texte=recupSousTexte(texte,'{','}')
#        print "DBG>",par,parametres
        if len(par)!=nbParam+nbRetour:
          print("Erreur. Le nombre de parametres ne correspond pas :",len(par),nbParam,nbRetour)
        else:
          if nbRetour>0:
            index= int(fct["RETOUR"][1:].split(':')[0]) - 1
            print("id-params>", index, parametres)
            retour=par[index]
          for nb in range(len(parametres)):
            print("PAR>",parametres[nb])
            if parametres[nb].find('{')>-1:
              avParam=parametres[nb][:parametres[nb].find('{')]
              parametres[nb]=parametres[nb][parametres[nb].find('{')+1:]
              apParam=parametres[nb][parametres[nb].find('}')+1:]
              parametres[nb]=parametres[nb][:parametres[nb].find('}')]

              param=int(parametres[nb].split(':')[0])
              parametres[nb]="%s%s%s"%(avParam,par[param-1],apParam)
              if parametres[nb][0]=='@':
                parametres[nb]='join('+parametres[nb][1:].replace('@',')')
        print("DBG-Params>",'|'.join(parametres))

        chaineXML+='    <ACTION>\n'
        chaineXML+='      <COMMENTAIRE>%s</COMMENTAIRE>\n'%fct["DESCRIPTION"]
        chaineXML+='      <FONCTION>%s</FONCTION>\n'%fct["FONCTION"]
        par=('|'.join(parametres))
        chaineXML+='      <PARAMETRE>%s</PARAMETRE>\n'%par
#        chaineXML+='      <ATTENTE>0.5</ATTENTE>\n'
        if fct["RETOUR"]!="None":
          chaineXML+='      <RETOUR>%s</RETOUR>\n'%retour
        chaineXML+='      <VALIDER>False</VALIDER>\n'
        chaineXML+='    </ACTION>\n'
      else:
        print("Champ '%s' inconnu"%ligne[0])
  if ACTIONSon: chaineXML+='  </ACTIONS>\n'
  if FICHEon: chaineXML+='</FICHE>\n'
  return chaineXML, ficFiche

#-----------------------------------------------------------------------------------
def creerWorkdir(pWorkDir):
  work_dir=os.path.join(pWorkDir,"workdir"+time.strftime('%d%m%y%H%M%S',time.localtime(time.time())))
  if os.path.exists(work_dir):
    time.sleep(1)
    return creerWorkdir(pWorkDir)
  os.mkdir(work_dir)
  return work_dir

#-----------------------------------------------------------------------------------
def LancementNangu(argv):
  work_dir=os.getenv('NANGUWK','.')
  codeRetour=nngsrc.services.CODE_OK
  if not os.path.exists(work_dir):
    os.mkdir(work_dir)
  if len(argv)==1:
#    SRV_DefautPrint(Fore.YELLOW)
    nngsrc.services.SRV_Print('Syntaxe : Nangu --vers')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --nouv <projet>.prj <repertoire_projet> [simple]')
#    nngsrc.services.SRV_Print('Syntaxe : Nangu --clef <projet>.prj <fichier>.clef')
#    nngsrc.services.SRV_Print('Syntaxe : Nangu --ft <projet>.prj {[version_cible]}')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --bilan <projet>.prj')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --ctrl <projet>.prj')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --camp <projet>.prj <fichier>.cpg [init]')
#    nngsrc.services.SRV_Print('Syntaxe : Nangu --gui <projet>.prj')
#    nngsrc.services.SRV_Print('Syntaxe : Nangu --tui <projet>.prj')
    #nngsrc.services.SRV_Print('Syntaxe : Nangu --conv <projet>.prj <repertoire_projet>')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --valid <E|S|P|C|M|R|Vx> <projet>.prj <repertoire_projet>')
#    nngsrc.services.SRV_Print('Syntaxe : Nangu --script <projet>.prj <document_pilote>')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --export <GCFSAT> <projet>.prj <identifiant> <repertoire_export>')
    nngsrc.services.SRV_Print('Syntaxe : Nangu --import <FRSAT> <projet>.prj <identifiant> <repertoire_import>')
    nngsrc.services.SRV_Print('Syntaxe : Nangu <S|P|C|M|R|V> <configuration>.xml <repertoire_Fiches> <repertoire_Rapports> {[<fiche>.xml ]*}')

#    SRV_ResetPrint()
  #---------------------------------------------
  # option -vers : version
  #---------------------------------------------
  elif len(sys.argv)==2 and sys.argv[1]=='--vers':
    nngsrc.nng.nangu_Version()

  #---------------------------------------------
  # option --ft : faits techniques
  #---------------------------------------------
  elif sys.argv[1]=='--ft':
    projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
    if projetPRJ is None:
      print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
      codeRetour=nngsrc.services.CODE_ERREUR
    else:
      (_,typePRJsimple,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(lstRapports,controleFiches,_,referenceSubst),_,_)=projetPRJ
      nngNANGU=nngsrc.nng.NANGU('X',typePRJsimple,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir))
      if len(sys.argv)==3:
        nngNANGU.nangu_Todo(nngNANGU.projet.PRODUIT_VERSION,'-',repRapports)
      elif len(sys.argv)==5:
        nngNANGU.nangu_Todo(sys.argv[3],sys.argv[4],repRapports)
      else:
        print(u'Syntaxe : Nangu --ft <projet>.prj <version>|- <version_cible>|- }')
  #---------------------------------------------
  # option --bilan : bilan des fiches
  #---------------------------------------------
  elif sys.argv[1]=='--bilan':
    if len(sys.argv)==3 or (len(sys.argv)==4 and sys.argv[3]=="--silence"):
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,typePRJsimple,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(lstRapports,controleFiches,_,referenceSubst),_,_)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('X',typePRJsimple,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir))
        codeRetour=nngNANGU.nangu_BilanFiches(os.path.join(repFiches,controleFiches),os.path.join(repConfiguration,lstRapports),scenariosDemandes=None,editer=(len(sys.argv)==3),comparer=(len(sys.argv)==3))
    else:
      print(u'Syntaxe : Nangu --bilan <projet>.prj')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --ctrl : controle des fiches
  #---------------------------------------------
  elif sys.argv[1]=='--ctrl':
    if len(sys.argv)==3 or (len(sys.argv)==4 and sys.argv[3]=="--silence"):
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,typePRJsimple,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(_,controleFiches,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('K',typePRJsimple,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
        codesRetour= [nngNANGU.nangu_Validation([controleFiches.replace('.lst', '')]),
                      nngNANGU.nangu_Controles(editer=(len(sys.argv) == 3), comparer=(len(sys.argv) == 3))]
        codeRetour=max(codesRetour)
    else:
      print(u'Syntaxe : Nangu --ctrl <projet>.prj')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --script : generation de script
  #---------------------------------------------
  elif sys.argv[1]=='--script':
    if len(sys.argv)==4:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,typePRJsimple,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(_,controleFiches,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('Y',typePRJsimple,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
#        codeRetour=nngNANGU.nangu_Validation([controleFiches.replace('.lst', '')])
        codeRetour=nngNANGU.nangu_Validation(list(sys.argv[3:]))
    else:
      print(u'Syntaxe : Nangu --script <projet>.prj <document_pilote>')
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --maj : Mise à jour
  #---------------------------------------------
  elif sys.argv[1]=='--maj':
    if len(sys.argv)==3:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,_,(repConfiguration,configurationCourante),(repCampagnes,repPlans,repScenarios,repFiches,repRapports,_,_),(_,_,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('X',False,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,work_dir,dico=dicoEnv)
        nngNANGU.nangu_MiseAJour(sys.argv[2],projetPRJ)
  #---------------------------------------------
  # option --camp : gestionnaire de campagnes
  #---------------------------------------------
  elif sys.argv[1]=='--camp':
    if len(sys.argv)>=4:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,_,(repConfiguration,configurationCourante),(repCampagnes,repPlans,repScenarios,repFiches,repRapports,_,_),(_,_,_,referenceSubst),_,dicoEnv)=projetPRJ
        if len(sys.argv)>4:
          nngNANGU=nngsrc.nng.NANGU('X',False,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
          if sys.argv[4]=="init":
            if len(sys.argv)==7:
              codeRetour=nngNANGU.nangu_CampagneInit(repCampagnes,sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6])
            elif len(sys.argv)==6:
              codeRetour=nngNANGU.nangu_CampagneInit(repCampagnes,sys.argv[3],sys.argv[4],sys.argv[5])
          elif len(sys.argv)>4 and sys.argv[4]=="maj":
            codeRetour=nngNANGU.nangu_CampagneInit(repCampagnes,sys.argv[3],sys.argv[4])
        else:
          nngNANGU=nngsrc.nng.NANGU('R',False,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir))
          listeCahiers=nngNANGU.nangu_Campagne(repCampagnes,sys.argv[3])
    else:
      print(u'Syntaxe : Nangu --camp <projet>.prj <fichier>.cpg [init nomCampagne [idCampagne]]')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --gui : lancement du projet avec gui
  #---------------------------------------------
#  elif sys.argv[1]=='--gui':
#    if len(sys.argv)==3:
#      if os.access(sys.argv[2],os.F_OK):
#        app=None
#        if NANGU_GUI:
#          app=PyQt5.QtWidgets.QApplication(sys.argv)
#        if app:
#          #pixmap=QtGui.QPixmap(os.path.join(os.getenv('NANGU','.'),"ressources","nangu.png"))
#          splash=PyQt5.QtWidgets.QSplashScreen(PyQt5.QtGui.QPixmap("nangu.png"),PyQt5.QtCore.Qt.WindowStaysOnTopHint)
#          splash.show()
#          splash.showMessage(' ')
#          app.processEvents()
#          #time.sleep(5)
#          menu=gui.qtmenu.Menu(sys.argv[2])
#          splash.finish(menu)
#          if menu.Valide:
#            menu.MenuPrincipal(app)
#      else:
#        print("Le fichier de projet %s n'existe pas."%sys.argv[2])
#        codeRetour=nngsrc.services.CODE_ERREUR
#    else:
#      print(u'Syntaxe : Nangu --gui <projet>.prj')
#      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --tui : lancement du projet avec tui
  #---------------------------------------------
#  elif sys.argv[1]=='--tui':
#    if len(sys.argv)==3:
#      if os.access(sys.argv[2],os.F_OK):
#        tui.TUI_menu.TUI()
#      else:
#        print("Le fichier de projet %s n'existe pas."%sys.argv[2])
#        codeRetour=nngsrc.services.CODE_ERREUR
#    else:
#      print(u'Syntaxe : Nangu --tui <projet>.prj')
##      print(u'SYSARG",sys.argv)
#      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --nouv : creation d'un projet
  #---------------------------------------------
  elif sys.argv[1]=='--nouv':
    if len(sys.argv)==4:
      codeRetour=nngsrc.nng.nangu_NouveauProjet(sys.argv[2],sys.argv[3],False)
    elif len(sys.argv)==5:
      codeRetour=nngsrc.nng.nangu_NouveauProjet(sys.argv[2],sys.argv[3],sys.argv[4]=='simple')
    else:
      print(u'Syntaxe : Nangu --nouv <projet>.prj <repertoire_projet> [simple]')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --clef : clef d'utilisation
  #---------------------------------------------
#  elif sys.argv[1]=='--clef':
#    if len(sys.argv)==4:
#      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[2])
#      if projetPRJ is None:
#        print("Le fichier de projet '%s' n'existe pas"%sys.argv[2])
#        codeRetour=nngsrc.services.CODE_ERREUR
#      else:
#        nngsrc.nng.nangu_ClefUtilisation(projetPRJ,sys.argv[3])
#    else:
#      print(u'Syntaxe : Nangu --clef <projet>.prj <fichier>.clef')
##      print(u'SYSARG",sys.argv)
#      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --export : export d'un projet
  #---------------------------------------------
  elif sys.argv[1]=='--export':
    if len(sys.argv)==6:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[3])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[3])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,_,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(_,_,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('X',False,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
        codeRetour=nngNANGU.nangu_ExportProjet(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    else:
      print(u'Syntaxe : Nangu --export <GCFSAT> <projet>.prj <identifiant> <repertoire_export>')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --import : import d'un projet
  #---------------------------------------------
  elif sys.argv[1]=='--import':
    if len(sys.argv)==6:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[3])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas"%sys.argv[3])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,_,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(_,_,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU('X',False,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
        codeRetour=nngNANGU.nangu_ImportProjet(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    else:
      print(u'Syntaxe : Nangu --import <FRSAT> <projet>.prj <identifiant> <repertoire_import>')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --conv : Convertion de fichiers textes en fiches
  #---------------------------------------------
  elif sys.argv[1]=='--conv':
    if len(sys.argv)==4:
      nngsrc.nng.nangu_ConvFiche(sys.argv[2],sys.argv[3])
    else:
      print(u'Syntaxe : Nangu --conv <document.txt> <repertoire_fiches>')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --valid : lancement de validation avec fichier de projet
  #---------------------------------------------
  elif sys.argv[1]=='--valid':
    if len(sys.argv)>=4:
      projetPRJ=nngsrc.nng.chargeProjet(sys.argv[3])
      if projetPRJ is None:
        print("Le fichier de projet '%s' n'existe pas ou n'est pas conforme."%sys.argv[3])
        codeRetour=nngsrc.services.CODE_ERREUR
      else:
        (_,typePRJsimple,(repConfiguration,configurationCourante),(_,repPlans,repScenarios,repFiches,repRapports,_,_),(_,_,_,referenceSubst),_,dicoEnv)=projetPRJ
        nngNANGU=nngsrc.nng.NANGU(sys.argv[2],typePRJsimple,os.path.join(repConfiguration,configurationCourante),referenceSubst,repPlans,repScenarios,repFiches,repRapports,creerWorkdir(work_dir),dico=dicoEnv)
        if nngNANGU.nangu_OK:
          codeRetour=nngNANGU.nangu_Validation(list(sys.argv[4:]))
    else:
      print(u'Syntaxe : Nangu --valid <E|S|P|C|M|R|Vx> <projet>.prj ...')
#      print(u'SYSARG",sys.argv)
      codeRetour=nngsrc.services.CODE_LIGCOM
  #---------------------------------------------
  # option --valid : lancement de validation ligne de commande
  #---------------------------------------------
  elif len(sys.argv)<6:
    # Moins de cinq paramètres : erreur sur la syntaxe de l'appel
#    SRV_DefautPrint(Fore.YELLOW)
    nngsrc.services.SRV_Print(u'Syntaxe : Nangu <E|S|P|C|M|R|V> <configuration>.xml <repertoire_Plans> <repertoire_Scenarios> <repertoire_Fiches> <repertoire_Rapports> {[<fiche>.xml ]*}')
#      print(u'SYSARG",sys.argv)
    codeRetour=nngsrc.services.CODE_LIGCOM
  else:
    # Cinq paramètres au moins : lancement sans fiche spécifique
    ok=True
    for e in [2,3,4,5]:
      if not os.access(sys.argv[e],os.F_OK):
        print()
        print("ATTENTION :")
        print("    Le fichier ou répertoire '%s' est inaccessible."%sys.argv[e])
#        print("    Syntaxe :")
#        print("       Nangu <E|S|P|C|M|R|V> <configuration>.xml <repertoire_Plans> <repertoire_Scenarios> <repertoire_Fiches> <repertoire_Rapports> {[<fiche>.xml ]*}")
        ok=False
        codeRetour=nngsrc.services.CODE_LIGCOM
    if ok:
      nngNANGU=nngsrc.nng.NANGU(sys.argv[1],False,sys.argv[2],sys.argv[3],None,sys.argv[4],sys.argv[5],sys.argv[6],creerWorkdir(work_dir))
      if len(sys.argv)==5:
        codeRetour=nngNANGU.nangu_Validation()
      else:
        codeRetour=nngNANGU.nangu_Validation(list(sys.argv[6:]))
  return codeRetour

#-----------------------------------------------------------------------------------
# Fin
