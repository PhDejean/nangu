#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import datetime,time,os
import nngsrc.services 
import nngsrc.servicesXML 

# Liste des types classes suivant leurs priorites lors de la recuperation de leur valeur dans les fichiers de configuration XML
LIST_PRIORITE_TYPES=[ int, float, str ]
STR_DEFAULT_SPLIT ="_"

#-----------------------------------------------------------------------------------
# Recuperation du fichier
#-----------------------------------------------------------------------------------
def recupNomfichier(el,nomBalise,dictParam={}):
  valeur=nngsrc.servicesXML.SRVxml_recupTexteNoeud(el,nomBalise)+' '
  fichier=valeur.split(' ')[0]
  parametre=valeur.split(' ')[1]
  for elem in parametre.split('|'):
    if elem!='':
      param="[%s]"%elem.split('=')[0]
      #v8.2 param='['+elem.split('=')[0]+']'
      val=elem.split('=')[1]
      dictParam[param]=val
  return fichier,dictParam

#-----------------------------------------------------------------------------------
# Traiter la lecture d'une variable
#-----------------------------------------------------------------------------------
def traiterVariableXML(dico,variable,projet,parallel,forcePersistance=False):
#  print ("\nVAR>",variable)
  # Initialisation
  dictVariable= {'NOM': None,'INDEX': None,'VALEUR': None,'VARENVPRJ':None,'CONTEXTUEL':True,
                 'TYPE': None,'COMMENTAIRE': None,'INDEXER': False,
                 'UTILE': False,'PERSISTANCE': False,'FICHES': [projet.fichierXml],'JDD':None}

  # Liste des balises d'une variable
  listBalisesRechercher=['NOM','INDEX','VALEUR','TYPE','COMMENTAIRE']
  # Recherche des elements du parametre
  dictVariable['PERSISTANCE']=nngsrc.servicesXML.SRVxml_recupValAttribut(variable,'pers')=='oui' or forcePersistance
#  for balise in listBalisesRechercher:
#    print (">",balise,nngsrc.servicesXML.SRVxml_recupNoeud(variable,balise))
#    print (">",nngsrc.servicesXML.SRVxml_recupTexteNoeud(variable,balise))

  for balise in listBalisesRechercher:
    ndBalise=nngsrc.servicesXML.SRVxml_recupNoeud(variable,balise)
    var=nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndBalise)
    # Si trouve alors on sauvegarde la valeur et on passe au fils suivant
    if ndBalise:
      if balise=='NOM':
        dictVariable['INDEXER']=(nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'indexer')=='oui')
      if balise=='COMMENTAIRE':
        if dictVariable['COMMENTAIRE']:
          dictVariable['COMMENTAIRE']=var+dictVariable['COMMENTAIRE']
        else:
          dictVariable['COMMENTAIRE']=var
      elif balise=='VALEUR':
        if nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'type')=='env':
          if var in dico:
            dictVariable[balise]=dico[var]
            dictVariable['VARENVPRJ']=('PRJ',var)
          else:
            if projet.execution in ['R']:
              dictVariable[balise]=os.getenv(var)
            else:
              dictVariable[balise]="{%s}"%var
            dictVariable['VARENVPRJ']=('ENV',var)
          if dictVariable[balise] is None:
            dictVariable[balise]=''
            nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : variable d'environnement '%s' indéfinie."%var,style="Er",parallel=parallel)
        elif nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'type')=='dic':
          if var in dico:
            dictVariable[balise]=dico[var]
            dictVariable['VARENVPRJ']=('PRJ',var)
        else:
          dictVariable[balise]=var
      else:
        dictVariable[balise]=var
  # Validite d'un parametre
  if dictVariable['TYPE'] is None:
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Type de la variable '%s' non indique."%dictVariable['NOM'],style="Er",parallel=parallel)
    return None
  if not dictVariable['TYPE'] in ['ND','ID','VARIABLE','SAISIE','PATH_IN','DATA_IN','PARAM','PATH_OUT','DATA_OUT','PATH_REF','DATA_REF']:
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"Type de la variable '%s' incorrect : %s."%(dictVariable['NOM'],dictVariable['TYPE']),style="r",parallel=parallel)
    return None
  if dictVariable['NOM'] is None:
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : récuperation d'une variable impossible.",style="Er",parallel=parallel)
    return None
  if dictVariable['VALEUR'] is None:
    dictVariable['VALEUR']=''
  # Retour du parametre
#  print ("VAR<",dictVariable)
  return dictVariable

#-----------------------------------------------------------------------------------
def lireVariableXML(variable):
#  print ("VAR>",variable)
  # Initialisation
  dictVariable= {'NOM': '', 'INDEXER': False, 'INDEX': '', 'VALEUR': '', 'TYPE': "ND", 'PERSISTANCE': False,
                 'COMMENTAIRE': '', 'ATTRIBUT': "-aucun-"}

  # Liste des balises d'une variable
  listBalisesRechercher=['COMMENTAIRE','NOM','INDEX','VALEUR','TYPE']
  # Recherche des elements du parametre
  dictVariable['PERSISTANCE']=(nngsrc.servicesXML.SRVxml_recupValAttribut(variable,'pers')=='oui')
  for balise in listBalisesRechercher:
    ndBalise = nngsrc.servicesXML.SRVxml_recupNoeud(variable, balise)
    var = nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndBalise)
    # Si trouve alors on sauvegarde la valeur et on passe au fils suivant
    if ndBalise is not None:
          if balise=='NOM':
            dictVariable['INDEXER']=(nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'indexer')=='oui') # 11/09 el remplacé par ndBalise
          if balise=='COMMENTAIRE':
            if dictVariable['COMMENTAIRE']:
              dictVariable['COMMENTAIRE']=var+dictVariable['COMMENTAIRE']
            else:
              dictVariable['COMMENTAIRE']=var
          elif balise=='VALEUR':
            dictVariable['ATTRIBUT']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'type') # 11/09 el remplacé par ndBalise
            dictVariable[balise]=var
          else:
            dictVariable[balise]=var
  return dictVariable

#-----------------------------------------------------------------------------------
def remplacerVariables(fichierXml,valeur,variables,variablesBCL,projet,parallel):
  remplacement=1
  while remplacement:
    remplacement=0
    for idx in range(len(variables)):
      if variables[idx]['NOM'] not in ['PIPE','ITER_FICHE','ITER_ACTIONS']:
        var=variables[idx]
        if var['TYPE'] is not None and var['TYPE']!='VARIABLE' and var['TYPE']!='SAISIE' and var['TYPE']!='ID':
          nom='['+var['NOM']+']'
          val=var['VALEUR']
          while valeur.find(nom)!=-1:
            if var['INDEXER']:
              if var['INDEX'] is not None:
                valeur=valeur.replace(nom,"MAGIDX:"+val+":IDX:"+var['INDEX']+":IDXMAG")
              else:
                if val is None:
                  nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"PLANTE : "+var['NOM']+" n'est pas correctement défini !",style="W",parallel=parallel)
                  break
                else:
                  if var['VARENVPRJ']:
                    _,index=var['VARENVPRJ']
                  else:
                    index=nom.replace('{','').replace('}','').replace('<','').replace('>','').replace('[','').replace(']','')
                  valeur=valeur.replace(nom,"MAGIDX:"+val+":IDX:"+index+":IDXMAG")
            else:
              try:
                valeur=valeur.replace(nom,val)
              except :
                nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : La valeur de '%s' est indefinie."%nom,style="Er",parallel=parallel)
                if projet.execution=='S':
                  if (nom,projet.fichierXml) not in projet.dicoVariables['indefini']:
                    projet.dicoVariables['indefini'].append((nom,projet.fichierXml))
                valeur=valeur.replace(nom,"indefini")
            remplacement=True
            variables[idx]['UTILE']=True
            if 'FICHES' in variables[idx]:
              if fichierXml not in variables[idx]['FICHES']:
                variables[idx]['FICHES'].append(fichierXml)
            else:
              variables[idx]['FICHES']=[fichierXml]
  if '[' in valeur:
    noimage=valeur
    listeSpeciaux=["[PIPE]","[ITER_FICHE]","[ITER_ACTIONS]","[IMAGE:","[quote]","[/quote]","[b]","[/b]","[i]","[/i]","[br/]","[u]","[/u]","[left]","[/left]","[right]","[/right]","[center]","[/center]","[ol]","[/ol]","[ul]","[/ul]","[li]","[/li]","[img]","[img scale=","[/img]","[table col=","[/table]","[tr]","[/tr]","[th]","[/th]","[hr/]","[td]","[/td]","[size=","[/size]"]
    listeSpeciaux.extend(variablesBCL)
    for var in variables:
#      print ("VAR>",var['NOM'],var['TYPE'])
      if var['TYPE']=='ND' or var['TYPE']=='SAISIE'or var['TYPE']=='VARIABLE' or var['TYPE']=='ID':
        listeSpeciaux.append('['+var['NOM']+']')
    for special in listeSpeciaux:
       noimage=noimage.replace(special,'')
    if '[' in noimage :
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Le texte '"+noimage+"' contient peut-être une variable non déclarée !",style="Wr",parallel=parallel)

  valeur=nngsrc.services.SRV_TraiterJoin(valeur)
  return valeur

#-----------------------------------------------------------------------------------
def chargerActionsXML(repFiches,fichierXml,parametres,projet,parallel):
  fichier=os.path.join(repFiches,nngsrc.services.SRV_TraiterJoin(fichierXml))
  listActionsXML=[]
  listVariablesBCL=[]

  if not os.access(fichier, os.F_OK):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier d'actions "+fichier+" inexistant.",style="Er",parallel=parallel)
  else:
    racine=nngsrc.servicesXML.SRVxml_parser(fichier)

    for fils in nngsrc.servicesXML.SRVxml_recupNoeudFils(racine):
      if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils) in ["ETAPE","ACTION","BLOC_ACTIONS"]:
        listActionsXML.append(fils)
      elif nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=='BOUCLE':
         listActionsXML.append(fils)
         for petitfils in nngsrc.servicesXML.SRVxml_recupNoeudFils(fils):
            if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="LISTE":
              listVariablesBCL.append("[%s]"%nngsrc.servicesXML.SRVxml_recupValAttribut(petitfils,'var'))
      elif nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="FICHIER":
        (AppelFichier,parametres)=recupNomfichier(fils,"FICHIER",parametres)
        lactions,lVariablesBCL=chargerActionsXML(repFiches,AppelFichier,parametres,projet,parallel)
        listActionsXML.extend(lactions)
        listVariablesBCL.extend(lVariablesBCL)

  #  listActionsXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(racine,'ACTION')
    substitueParametres(listActionsXML,parametres)
  return listActionsXML,listVariablesBCL

#-----------------------------------------------------------------------------------
def substitueParametres(listActionsXML,parametres):
  for action in listActionsXML:
#    print ("DBG-substitueParametres>",action)
    ndFils=nngsrc.servicesXML.SRVxml_recupNoeudFils(action)
    if action.nodeName=="ETAPE":
      for clef in parametres.keys():
        texte=nngsrc.servicesXML.SRVxml_recupTexteNoeud(action,'').replace(clef,parametres[clef])
        nngsrc.servicesXML.SRVxml_modifTexteNoeud(action,texte)
    else:
      for nd in ndFils:
#        print "Balise>",nngsrc.servicesXML.SRVxml_recupNomNoeud(nd)
#        print parametres.keys()
        for clef in parametres.keys():
          texte=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nd,'').replace(clef,parametres[clef])
#          print "  >",texte
          nngsrc.servicesXML.SRVxml_modifTexteNoeud(nd,texte)
#          print "  <",nngsrc.servicesXML.SRVxml_recupTexteNoeud(nd, '')
#          nd.nodeValue=nd.nodeValue.replace(clef,parametres[clef])
        substitueParametres([nd],parametres)

#-----------------------------------------------------------------------------------
def traiterEtapeXML(fichierXml,action,variables,variablesBCL,projet,parallel):
  # Initialisation
  dictAction=InitialiserDictAction()
  texte=nngsrc.servicesXML.SRVxml_recupTexteNoeud(action)
  niveau=nngsrc.servicesXML.SRVxml_recupValAttribut(action,'niv','1')
  dictAction['COMMENTAIRE']="%s|%s"%(niveau,remplacerVariables(fichierXml,texte,variables,variablesBCL,projet,parallel))
  return dictAction

#-----------------------------------------------------------------------------------
def traiterActionXML(fichierXml,rapportActions,action,variables,variablesBCL,projet,parallel):
  # Initialisation
  dictAction=InitialiserDictAction()
  Element=None
  # Liste des balises d'une variable
  listBalisesRechercher=['TAG','ELEMENT','FONCTION','PARAMETRE','RETOUR','RETOURS','COMMENTAIRE','VALIDER','VARIABLE','CHERCHER','BLOC','ID','CONDITION','EXIGENCES','FAITS_TECHNIQUES','ATTENTE','BOUCLE','LISTE']  #GFT
  listBalisesVariables=['PARAMETRE','COMMENTAIRE','RETOUR','RETOUR_OK','RETOUR_AV','RETOUR_NOK','TROUVER_NON','TROUVER_OUI','VALIDER_OK','VALIDER_PB','CONDITION','LISTE']
  # Recherche des elements du parametre
#  filsAction=nngsrc.servicesXML.SRVxml_recupNoeudFils(action)
  for balise in listBalisesRechercher:
    ndBalise = nngsrc.servicesXML.SRVxml_recupNoeud(action, balise)
    valeur = nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndBalise)
    # Si trouve alors on sauvegarde la valeur et on passe au fils suivant

    #print 'DBG-Action-Balise>',balise,valeur
    # Si trouve alors on sauvegarde la valeur et on passe au fils suivant
    if ndBalise:
          #--BLOC---------------------------
          if balise=="BLOC":
            filsBloc=nngsrc.servicesXML.SRVxml_recupNoeudFils(nngsrc.servicesXML.SRVxml_recupNoeud(action,balise))
#            print "DBG-filsBloc>",filsBloc
            for fils in filsBloc:
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="INTERVALLE":
                dictAction["INTERVALLE"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="EXCLUSIONS":
                filsExclus = nngsrc.servicesXML.SRVxml_recupNoeudFils(fils)
 #               print "DBG>-Exclus>",filsExclus
                for petitfils in filsExclus:
                  if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="LIGNES":
                    dictAction["EXCLURE_LIGNES"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(petitfils)
                  if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="EXTENSIONS":
                    dictAction["EXCLURE_EXTENSIONS"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(petitfils)
                  if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="TEXTES":
                    dictAction["EXCLURE_TEXTES"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(petitfils)
                    dictAction["EXCLURE_TEXTES"]=remplacerVariables(fichierXml,dictAction["EXCLURE_TEXTES"],variables,variablesBCL,projet,parallel)
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="REMPLACEMENTS":
                filsExclus = nngsrc.servicesXML.SRVxml_recupNoeudFils(fils)
 #               print "DBG>-Exclus>",filsExclus
                for petitfils in filsExclus:
                  if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="TEXTES":
                    dictAction["REMPLACER_TEXTES"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(petitfils)
                    dictAction["REMPLACER_TEXTES"]=remplacerVariables(fichierXml,dictAction["REMPLACER_TEXTES"],variables,variablesBCL,projet,parallel)
          #--RETOURS---------------------------
          elif balise=="RETOURS": #TODO : Utilisé seulement par VALID_CommandeEx
            dictAction['RETOUR_OK']=None
            dictAction['RETOUR_AV']=None
            dictAction['RETOUR_NOK']=None
            #print("DBG-RETOURS>",action)
            for elbloc in action.childNodes:
              if elbloc.nodeName=="RETOUR_OK":
                dictAction['RETOUR_OK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
              if elbloc.nodeName=="RETOUR_AV":
                dictAction['RETOUR_AV']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
              if elbloc.nodeName=="RETOUR_NOK":
                dictAction['RETOUR_NOK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
          #--CHERCHER---------------------------
          elif balise=='CHERCHER':
            dictAction['CASSE']=0
            if nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'casse')=='oui':
              dictAction['CASSE']=1
            for fils in nngsrc.servicesXML.SRVxml_recupNoeudFils(ndBalise):
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="CHAINES":
#                print "DBG-CHAINES-traiterAction>",fils
                if nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'validation'):
                  nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"Attention champ 'validation' obsolete",style="r",parallel=parallel)
                if nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'trouver')=='non':
                  dictAction['TROUVER_NON']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                elif nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'trouver')=='oui':
                  dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                elif nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'si_resultat')=='correct':
                  dictAction['VALIDER_OK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                elif nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'si_resultat')=='incorrect':
                  dictAction['VALIDER_PB']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                else:
                  dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
              elif nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="IMAGE":
#                print "DBG-IMAGE-traiterAction>",fils
                if nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'trouver')=='non':
                  dictAction['TROUVER_NON']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                elif nngsrc.servicesXML.SRVxml_recupValAttribut(fils,'trouver')=='oui':
                  dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
                else:
                  dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(fils)
          #--FONCTION---------------------------
          elif balise=='FONCTION':
            if rapportActions!=0:
              dictAction['RAPPORT']=rapportActions
            dictAction['FT']=0
            dictAction['LIBELLE']=nngsrc.servicesXML.SRVxml_recupValAttribut(action,'libelle',None)
            if nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'rapport')=='non':
              dictAction['RAPPORT']=-1
            elif nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'rapport')=='siNonOK':
              dictAction['RAPPORT']=1
            elif nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'rapport')=='siOK':
              dictAction['RAPPORT']=2
            if nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'sortie_vide')=='':
              dictAction['SORTIE_SI_VIDE']=projet.lexique.entree('LOCUT_AUCUNE_SORTIE') #"-aucune-sortie-"
            else:
              dictAction['SORTIE_SI_VIDE']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'sortie_vide')
            ft=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'ft')
            #if ft=="C": ft="Critique"
            #elif ft=="B": ft="Bloquant"
            #elif ft=="M": ft="Majeur"
            #elif ft=="m": ft="mineur"
            #else: ft="NQ"
            dictAction['FT']=projet.lexique.FTsurFiche(ft)
            dictAction[balise]=valeur
          #--PARAMETRE---------------------------
          elif balise=='PARAMETRE':
            dictAction['CASSEP']=0
            if nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'casse')=='oui':
              dictAction['CASSEP']=1
            dictAction['SIMILITUDE']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'similitude')
            dictAction['SOURIS']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'delaisouris')
            dictAction['ECRAN']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'ecran')
            dictAction['CLAVIER']=nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'clavier')
            dictAction[balise]=valeur
          elif balise=='ELEMENT':
            Element=nngsrc.servicesXML.SRVxml_recupTexteNoeud(action)
            dictAction['ELEMENT']=Element
          #--BOUCLE---------------------------
          elif balise=='BOUCLE':
            dictAction[balise]=valeur.split('|')
            if len(dictAction[balise])==1:
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Le nombre maximal d'iteration de boucle est absent. ",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"                Ce maximum est positionne a 10 par defaut.",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r",parallel=parallel)
              dictAction[balise].append('10')
            try:
              dictAction[balise][1]=int(dictAction[balise][1])
            except:
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Le nombre maximal d'iteration de boucle est incorrect. ",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"                Ce maximum est positionne a 10 par defaut.",style="r",parallel=parallel)
              nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r",parallel=parallel)
              dictAction[balise][1]=10
          #--CONDITION---------------------------
          elif balise=='CONDITION':
            dictAction["APP_CONDITION"]=(nngsrc.servicesXML.SRVxml_recupValAttribut(ndBalise,'horsrapport')=='oui')
            dictAction[balise]=valeur
          #--VALIDER---------------------------
          elif balise=='VALIDER':
            texte=nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndBalise)
            if texte=='True' or texte=='False':
              dictAction[balise]=eval(texte)
            elif texte=='oui' or texte=='non':
              dictAction[balise]=texte=='oui'
            else:
              dictAction[balise]=True
              dictAction['VALIDER_RAISON']=texte
          #--------------------------------------
          elif balise=='EXIGENCES':
            if projet.execution!='K':
              valeur=filtrerExigFct(valeur,projet.exigences,projet.filtreMethode)
            dictAction[balise]=valeur
          else:
            dictAction[balise]=valeur

  for idx in range(len(variables)):
    if variables[idx]['PERSISTANCE']:
      if type(variables[idx]['VALEUR'])==str:
        variables[idx]['VALEUR']=remplacerVariables(fichierXml,variables[idx]['VALEUR'],variables,variablesBCL,projet,parallel)

  for balise in listBalisesVariables:
    if balise in dictAction:
      if dictAction[balise] is not None:
        dictAction[balise]=remplacerVariables(fichierXml,dictAction[balise],variables,variablesBCL,projet,parallel)

  if projet.execution=='S' and dictAction['RAPPORT']==1 and not 'COMMENTAIRE' in dictAction:
    if projet.fichierXml not in projet.lstNoComment:
      projet.lstNoComment.append(projet.fichierXml)

  # Validite d'un parametre
  if dictAction['FONCTION']=='':
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR dans la recuperation d'une action.",style="Er",parallel=parallel)
    return None,None

  if projet.execution!='K' and dictAction['TAG'] is None and projet.ACTIVATION_BASEFT:
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Action non étiquetée. Lancer un contrôle !",style="Er",parallel=parallel)
    return None,None

  # Retour du parametre
  return dictAction,Element

#-----------------------------------------------------------------------------------
def InitialiserDictAction():
  dictAction= {"TAG": None, "LIBELLE": None, "ID": '', "CONDITION": None, "APP_CONDITION": False, 'BOUCLE': None,
               "LISTE": None, "EXIGENCES": '', "FAITS_TECHNIQUES": '', "ELEMENT": None, "VALIDER": False,
               "VALIDER_RAISON": '', "EXCLURE_LIGNES": '', "RETOUR_OK": None, "RETOUR_AV": None, "RETOUR_NOK": None,
               "CASSE": 0, "RAPPORT": 0, "FT": "n.q.", "SORTIE_SI_VIDE": "LOCUT_AUCUNE_SORTIE", "CASSEP": 0,
               "SIMILITUDE": None, "SOURIS": None, "ECRAN": None, "CLAVIER": None, "RETOUR": None, "RETOURS": None,
               "CHERCHER": None, "BLOC": None, "ATTENTE": 0, "COMMENTAIRE": "LOCUT_SANS_COMMENTAIRE",
               "VARIABLE": None, "FONCTION":None, "PARAMETRE":None}
  return dictAction
#-----------------------------------------------------------------------------------
# INUTILE ? def lireActionXML(action,variables,projet):
# INUTILE ?   print "lireActionXML>"
# INUTILE ?   # Initialisation
# INUTILE ?   dictAction=InitialiserDictAction()
# INUTILE ?   Element=None
# INUTILE ?   # Liste des balises d'une variable
# INUTILE ?   listBalisesRechercher=['ELEMENT','FONCTION','PARAMETRE','RETOUR','RETOURS','COMMENTAIRE','VALIDER','CHERCHER','BLOC','ID','CONDITION','EXIGENCES','FAITS_TECHNIQUES','ATTENTE','BOUCLE','LISTE']  #GFT
# INUTILE ?   listBalisesVariables=['PARAMETRE','COMMENTAIRE','RETOUR','RETOUR_OK','RETOUR_AV','RETOUR_NOK','TROUVER_NON','TROUVER_OUI','VALIDER_OK','VALIDER_PB','CONDITION','LISTE']
# INUTILE ?   # Recherche des elements du parametre
# INUTILE ?   for balise in listBalisesRechercher:
# INUTILE ?
# INUTILE ?         ndBalise = nngsrc.servicesXML.SRVxml_recupNoeud(action, balise)
# INUTILE ?         valeur = nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndBalise)
# INUTILE ?         #  Si trouve alors on sauvegarde la valeur et on passe au fils suivant
# INUTILE ?         if ndBalise:
# INUTILE ?           #--BLOC---------------------------
# INUTILE ?           if el.nodeName=="BLOC":
# INUTILE ?             for elbloc in el.childNodes:
# INUTILE ?               if elbloc.nodeName=="INTERVALLE":
# INUTILE ?                 dictAction["INTERVALLE"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?               if elbloc.nodeName=="EXCLUSIONS":
# INUTILE ?                 for elblocexclu in elbloc.childNodes:
# INUTILE ?                   if elblocexclu.nodeName=="LIGNES":
# INUTILE ?                     dictAction["LIGNES"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elblocexclu)
# INUTILE ?                   if elblocexclu.nodeName=="TEXTES":
# INUTILE ?                     dictAction["TEXTES"]=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elblocexclu)
# INUTILE ?           #--RETOURS---------------------------
# INUTILE ?           elif balise=="RETOURS":#el.nodeName
# INUTILE ?             dictAction['RETOUR_OK']=None
# INUTILE ?             dictAction['RETOUR_AV']=None
# INUTILE ?             dictAction['RETOUR_NOK']=None
# INUTILE ?             for elbloc in el.childNodes:
# INUTILE ?               if elbloc.nodeName=="RETOUR_OK":
# INUTILE ?                 dictAction['RETOUR_OK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?               if elbloc.nodeName=="RETOUR_AV":
# INUTILE ?                 dictAction['RETOUR_AV']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?               if elbloc.nodeName=="RETOUR_NOK":
# INUTILE ?                 dictAction['RETOUR_NOK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?           #--CHERCHER---------------------------
# INUTILE ?           elif balise=='CHERCHER':
# INUTILE ?             dictAction['CASSE']=0
# INUTILE ?             if nngsrc.servicesXML.SRVxml_recupValAttribut(el,'casse')=='oui':
# INUTILE ?               dictAction['CASSE']=1
# INUTILE ?             for elbloc in el.childNodes:
# INUTILE ?               if elbloc.nodeName=="CHAINES":
# INUTILE ?                 print "DBG-CHAINES-lireAction>"
# INUTILE ?                 if nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'trouver')=='non':
# INUTILE ?                   dictAction['TROUVER_NON']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 elif nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'trouver')=='oui':
# INUTILE ?                   dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 elif nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'si_resultat')=='correct':
# INUTILE ?                   dictAction['VALIDER_OK']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 elif nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'si_resultat')=='incorrect':
# INUTILE ?                   dictAction['VALIDER_PB']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 elif nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'validation'):
# INUTILE ?                   nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"Attention champ 'validation' obsolete",style="r")
# INUTILE ?                 else:
# INUTILE ?                   dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?               elif elbloc.nodeName=="IMAGE":
# INUTILE ?                 print "DBG-IMAGE-lireAction>"
# INUTILE ?                 if nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'trouver')=='non':
# INUTILE ?                   dictAction['TROUVER_NON']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 elif nngsrc.servicesXML.SRVxml_recupValAttribut(elbloc,'trouver')=='oui':
# INUTILE ?                   dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?                 else:
# INUTILE ?                   dictAction['TROUVER_OUI']=nngsrc.servicesXML.SRVxml_recupTexteNoeud(elbloc)
# INUTILE ?           #--FONCTION---------------------------
# INUTILE ?           elif balise=='FONCTION':
# INUTILE ? #            print "DBG-FONCTION>"
# INUTILE ?             dictAction['RAPPORT']=True
# INUTILE ?             dictAction['FT']="NQ"
# INUTILE ?             dictAction['LIBELLE']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'libelle')
# INUTILE ?             dictAction['RAPPORT']=(nngsrc.servicesXML.SRVxml_recupValAttribut(el,'rapport')!='non')
# INUTILE ?             dictAction['SORTIE_SI_VIDE']="-aucune-sortie-"
# INUTILE ?             if nngsrc.servicesXML.SRVxml_recupValAttribut(el,'retour'):
# INUTILE ?               dictAction['SORTIE_SI_VIDE']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'retour')
# INUTILE ?             ft=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'ft')
# INUTILE ?             if ft=="C": ft="Critique"
# INUTILE ?             elif ft=="B": ft="Bloquant"
# INUTILE ?             elif ft=="M": ft="Majeur"
# INUTILE ?             elif ft=="m": ft="mineur"
# INUTILE ?             else: ft="non qualifié"
# INUTILE ?             dictAction['FT']=ft
# INUTILE ?             dictAction[balise]=valeur
# INUTILE ?           #--PARAMETRE---------------------------
# INUTILE ?           elif balise=='PARAMETRE':
# INUTILE ?             dictAction['CASSEP']=0
# INUTILE ?             if nngsrc.servicesXML.SRVxml_recupValAttribut(el,'casse')=='oui':
# INUTILE ?               dictAction['CASSEP']=1
# INUTILE ?             dictAction['SIMILITUDE']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'similitude')
# INUTILE ?             dictAction['SOURIS']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'delaisouris')
# INUTILE ?             dictAction['ECRAN']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'ecran')
# INUTILE ?             dictAction['CLAVIER']=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'clavier')
# INUTILE ?             dictAction[balise]=valeur
# INUTILE ?           elif balise=='ELEMENT':
# INUTILE ?             Element=nngsrc.servicesXML.SRVxml_recupTexteNoeud(el)
# INUTILE ?             dictAction['ELEMENT']=Element
# INUTILE ?           #--BOUCLE---------------------------
# INUTILE ?           elif balise=='BOUCLE':
# INUTILE ?             dictAction[balise]=valeur.split('|')
# INUTILE ?             if len(dictAction[balise])==1:
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Le nombre maximal d'iteration de boucle est absent. ",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"                Ce maximum est positionne a 10 par defaut.",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r")
# INUTILE ?               dictAction[balise].append(u'10')
# INUTILE ?             try:
# INUTILE ?               dictAction[balise][1]=int(dictAction[balise][1])
# INUTILE ?             except:
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Le nombre maximal d'iteration de boucle est incorrect. ",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"                Ce maximum est positionne a 10 par defaut.",style="r")
# INUTILE ?               nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"----------------------------------------------------------------------------------------------",style="r")
# INUTILE ?               dictAction[balise][1]=10
# INUTILE ?           #--BOUCLE---------------------------
# INUTILE ?           elif balise=='CONDITION':
# INUTILE ?             dictAction["APP_CONDITION"]=nngsrc.servicesXML.SRVxml_recupValAttribut(el,'horsrapport')=='oui'
# INUTILE ?             dictAction[balise]=valeur
# INUTILE ?           #--------------------------------------
# INUTILE ?           else:
# INUTILE ?             dictAction[balise]=valeur
# INUTILE ?
# INUTILE ? #  for idx in range(len(variables)):
# INUTILE ? #    if variables[idx]['PERSISTANCE']:
# INUTILE ? #      variables[idx]['VALEUR']=remplacerVariables(fichierXml,variables[idx]['VALEUR'],variables,projet)
# INUTILE ?
# INUTILE ? #  for balise in listBalisesVariables:
# INUTILE ? #    if dictAction.has_key(balise):
# INUTILE ? #      if dictAction[balise] is not None and type(dictAction[balise])==unicode:
# INUTILE ? #        dictAction[balise]=remplacerVariables(fichierXml,dictAction[balise],variables,projet)
# INUTILE ?
# INUTILE ? #  if projet.execution=='S' and dictAction['RAPPORT']==1 and not dictAction.has_key('COMMENTAIRE'):
# INUTILE ? #    if projet.fichierXml not in projet.lstNoComment:
# INUTILE ? #      projet.lstNoComment.append(projet.fichierXml)
# INUTILE ?
# INUTILE ? #  # Validite d'un parametre
# INUTILE ? #  if dictAction['FONCTION']=='':
# INUTILE ? #    nngsrc.services.SRV_Terminal(None,projet.log,"ERREUR dans la recuperation d'une action.",style="Wr")
# INUTILE ? #    return None
# INUTILE ?   # Retour du parametre
# INUTILE ?   return (dictAction,Element)

#-------------------------------------------------------------------------------
def filtrerExigFct(chaine,dictExigFct,filtre):
  lstch=chaine.split(',')
  idxchaine=[]
  for exigFct in lstch:
    if filtreMethode(exigFct,dictExigFct,filtre):
      idxchaine.append(exigFct)
  return ','.join(idxchaine)

#-------------------------------------------------------------------------------
def filtreMethode(exigFct,dictExigFct,filtre):
    if exigFct in dictExigFct:
      if filtre is None: return True
      for m in dictExigFct[exigFct]['methode']:
          if m in filtre: return True
    return False

#-----------------------------------------------------------------------------------
def recuperationDescription(xmlDescription,projet):
  entite=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"ENTITE"),'')
  if projet.execution!='K' and not filtreMethode(entite,projet.fonctionnalites,projet.filtreMethode):
    entite=''
  methode=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"METHODE"),'')
  if methode=='':methode=projet.lexique.entree("LOCUT_ND")
  typo=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"TYPE"),'')
  if typo=='':typo=projet.lexique.entree("LOCUT_ND")
  nature=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"NATURE"),'')
  if nature=='':nature=projet.lexique.entree("LOCUT_ND")
  commentaire=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"COMMENTAIRE"),'')
  exigences=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(xmlDescription,"EXIGENCES"),'')
  if projet.execution!='K':
    exigences=filtrerExigFct(exigences,projet.exigences,projet.filtreMethode)
  listeNoeuds=nngsrc.servicesXML.SRVxml_recupTousNoeuds(xmlDescription,"DONNEES")
  listeJeuDeDonnees=[]
  for noeud in listeNoeuds:
    listeJeuDeDonnees.append(nngsrc.servicesXML.SRVxml_recupTexteNoeud(noeud,''))
  return entite, methode, typo, nature, commentaire, exigences, listeJeuDeDonnees

#-----------------------------------------------------------------------------------
def chargerJddXML(dico,repFiches,fichierXml,projet,parallel):
  fichier=os.path.join(repFiches,nngsrc.services.SRV_TraiterJoin(fichierXml))
  identifiant=''
  commentaire=''
  listVariables=[]

  if not os.access(fichier, os.F_OK):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier d'environnement "+fichier+" inexistant.",style="Er",parallel=parallel)
  else:
    racine=nngsrc.servicesXML.SRVxml_parser(fichier)
    if racine:                
      identifiant=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"IDENTIFIANT"),os.path.basename(fichierXml))
      commentaire=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(racine,"COMMENTAIRE"),'')
      listFichiersXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(racine,'FICHIER')
      listVariablesXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(racine,'DONNEE')
      for variable in listVariablesXML:
        res=traiterVariableXML(dico,variable,projet,parallel)
        if res is not None:
          res['JDD']=identifiant
          res['PERSISTANCE']=True
          listVariables.append(res)
      for fichier in listFichiersXML:
         _,vars=chargerJddXML(dico,repFiches,nngsrc.servicesXML.SRVxml_recupTexteNoeud(fichier),projet,parallel)
         listVariables.extend(vars)
    else:
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"------------------------------------------------------",style="Er",parallel=parallel)
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Le fichier XML %s semble incorrectement formé."%fichier,style="Er",parallel=parallel)
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"------------------------------------------------------",style="Er",parallel=parallel)
  return (identifiant, commentaire), listVariables

#-----------------------------------------------------------------------------------
def chargerEnvXML(dico,repFiches,fichierXml,projet,parallel):
  fichier=os.path.join(repFiches,nngsrc.services.SRV_TraiterJoin(fichierXml))
  listVariables=[]

  if not os.access(fichier, os.F_OK):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier d'environnement "+fichier+" inexistant.",style="Er",parallel=parallel)
  else:
    racine=nngsrc.servicesXML.SRVxml_parser(fichier)
    if racine:
      listVariablesXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(racine,'ELEMENT')
      for variable in listVariablesXML:
        res=traiterVariableXML(dico,variable,projet,parallel)
        if res is not None:
          listVariables.append(res)
      listVariablesXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(racine,'DONNEE')
      for variable in listVariablesXML:
        res=traiterVariableXML(dico,variable,projet,parallel,True)
        if res is not None:
          listVariables.append(res)
    else:
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"------------------------------------------------------",style="Er",parallel=parallel)
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Le fichier XML %s semble incorrectement formé."%fichier,style="Er",parallel=parallel)
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"------------------------------------------------------",style="Er",parallel=parallel)
  return listVariables

#-----------------------------------------------------------------------------------
def chargerFicheXML(dico,repFiches,fiche,listVariables,listJDD,projet,parallel):
  fichierXml="%s.xml"%fiche[0]
  projet.fichierXml=fichierXml
  if not os.access(os.path.join(repFiches,fichierXml), os.F_OK):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fiche %s inexistante\n"%fichierXml,style="Er",parallel=parallel)
    return None
  entite=''
  methode=''
  typo=''
  commentaire=''
  exigences=''
  idJDD=''
  descJDD=''
  appcondition=False
  description=('', u'Non précisé', u'Non précisé', "Pas de description", '', '')
  dictElements={}
  fiche=os.path.basename(fichierXml)
  fiche=os.path.splitext(fiche)[0]
  try:
    racine=nngsrc.servicesXML.SRVxml_parser(os.path.join(repFiches,fichierXml))
  except Exception as erreur:
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier xml %s"%fichierXml,style="Er",parallel=parallel)
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : %s"%str(erreur),style="Er",parallel=parallel)
    return None
  if nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)=="ENVIRONNEMENT":
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier d'environnement "+fichierXml,style="Er",parallel=parallel)
    return None
  if nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)=="DONNEES":
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fichier de données "+fichierXml,style="Er",parallel=parallel)
    return None
  if nngsrc.servicesXML.SRVxml_recupNomNoeud(racine)!="FICHE":
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : La fiche est incomplete (tag FICHE)",style="Er",parallel=parallel)
    return None
  if not nngsrc.servicesXML.SRVxml_recupValAttribut(racine,'version'):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : La fiche est incomplete (tag FICHE version)",style="Er",parallel=parallel)
    return None
  if nngsrc.servicesXML.SRVxml_recupValAttribut(racine,'version')!="4.0":
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : La version de la fiche est incorrecte (%s)"%nngsrc.servicesXML.SRVxml_recupValAttribut(racine,'version'),style="Er",parallel=parallel)
    return None
  # Traitement des sections
  listVariablesXML=[]
  listVariablesBCL=[]
  listEnvironnementsXML=[]
  listActionsXML=[]

  noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"DESCRIPTION")
#  print "DBG-DESCRIPTION>",noeud
  (entite,methode,typo,nature,commentaire,exigences,listeJeuDeDonnees)=recuperationDescription(noeud,projet)

  noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ENVIRONNEMENT")
#  print "DBG-ENVIRONNEMENT>",noeud

  if noeud:
    listEnvironnementsXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,'FICHIER')
    listVariablesXML=nngsrc.servicesXML.SRVxml_recupTousNoeuds(noeud,'ELEMENT')

  noeud=nngsrc.servicesXML.SRVxml_recupNoeud(racine,"ACTIONS")
  rapport=nngsrc.servicesXML.SRVxml_recupValAttribut(noeud,'rapport')
  if rapport=='non':
    rapportActions=-1
  elif rapport=='siNonOK':
    rapportActions=1
  elif rapport=='siOK':
    rapportActions=2
  else: rapportActions=0
#  print "DBG-ACTIONS>",noeud
#  print nngsrc.servicesXML.SRVxml_recupNoeudFils(noeud)

  for fils in nngsrc.servicesXML.SRVxml_recupNoeudFils(noeud):
    if nngsrc.servicesXML.SRVxml_recupNomNoeud(fils) in ["ETAPE","ACTION","BLOC_ACTIONS"]:
      listActionsXML.append(fils)
    elif nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=='BOUCLE':
      listActionsXML.append(fils)
      for petitfils in nngsrc.servicesXML.SRVxml_recupNoeudFils(fils):
          if nngsrc.servicesXML.SRVxml_recupNomNoeud(petitfils)=="LISTE":
            listVariablesBCL.append("[%s]"%nngsrc.servicesXML.SRVxml_recupValAttribut(petitfils,'var'))
    elif nngsrc.servicesXML.SRVxml_recupNomNoeud(fils)=="FICHIER":
      (AppelFichier,parametres)=recupNomfichier(fils,"FICHIER")
      lactions,lVariablesBCL=chargerActionsXML(repFiches,AppelFichier,parametres,projet,parallel)
      listActionsXML.extend(lactions)
      listVariablesBCL.extend(lVariablesBCL)

  if len(listeJeuDeDonnees)>0:
    for jeuDeDonnees in listeJeuDeDonnees:
      #print('Traitement du jeu de données %s'%jeuDeDonnees)
      # Traitement du jeu de données
      var=remplacerVariables(fichierXml,jeuDeDonnees,listVariables,listVariablesBCL,projet,parallel)
      JDD,listeVarFichier=chargerJddXML(dico,repFiches,var,projet,parallel)
      if not JDD in listJDD:
        listJDD.append(JDD)
      for variable in listeVarFichier:
        if nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables,False)==-1:
          listVariables.append(variable)
#          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['FICHES']=[(variable['UTILE'],variable['FICHES'])]
        else:
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['VALEUR']=variable['VALEUR']
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['COMMENTAIRE']=variable['COMMENTAIRE']
#          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['FICHES'].extend([(variable['UTILE'],variable['FICHES'])])
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['UTILE']=listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['UTILE'] or variable['UTILE']

  # Traitement des environnements
  for env in listEnvironnementsXML:
#    print "ListEnv>",listEnvironnementsXML
    if nngsrc.servicesXML.SRVxml_recupNomNoeud(env)=='FICHIER':
#      var=nngsrc.servicesXML.SRVxml_recupTexteNoeud(env,'')
#      print "DBG>",var,env
      var=nngsrc.servicesXML.SRVxml_recupTexteNoeud(env)
#      print "DBG-Fichier>",var,env
      var=remplacerVariables(fichierXml,var,listVariables,listVariablesBCL,projet,parallel)
      listeVarFichier=chargerEnvXML(dico,repFiches,var,projet,parallel)
      for variable in listeVarFichier:
        if nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables,False)==-1:
          listVariables.append(variable)
#          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['FICHES']=[(variable['UTILE'],variable['FICHES'])]
        else:
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['VALEUR']=variable['VALEUR']
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['COMMENTAIRE']=variable['COMMENTAIRE']
#          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['FICHES'].extend([(variable['UTILE'],variable['FICHES'])])
          listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['UTILE']=listVariables[nngsrc.services.SRV_IndexVariable(variable['NOM'],listVariables)]['UTILE'] or variable['UTILE']

#    for env in listVariables:
#      print "PDN>",env['NOM'],'=',env['VALEUR']

  # Traitement des Variables
  for variable in listVariablesXML:
    res=traiterVariableXML(dico,variable,projet,parallel)
    if res is not None:
      res['NOM']=remplacerVariables(fichierXml,res['NOM'],listVariables,listVariablesBCL,projet,parallel)
      idx=nngsrc.services.SRV_IndexVariable(res['NOM'],listVariables,False)
      if idx==-1:
        listVariables.append(res)
#        listVariables[idx]['FICHES']=[(res['UTILE'],res['FICHES'])]
      else:
        listVariables[idx]['VALEUR']=res['VALEUR']
        listVariables[idx]['COMMENTAIRE']=res['COMMENTAIRE']
        listVariables[idx]['TYPE']=res['TYPE']
        listVariables[idx]['INDEXER']=res['INDEXER']
        listVariables[idx]['PERSISTANCE']=res['PERSISTANCE']
#        if 'FICHES' in listVariables[idx]:
#          listVariables[idx]['FICHES'].extend([(res['UTILE'],res['FICHES'])])
#        else:
#          listVariables[idx]['FICHES']=[(res['UTILE'],res['FICHES'])]

  listVariables[nngsrc.services.SRV_IndexVariable('FICHE',listVariables)]['VALEUR']=fiche

#  for env in listVariables:
#    print "PDN>",env['NOM'],'=',env['VALEUR']

  # Traitement des Actions
  listActions=[]
  for action in listActionsXML:
    if nngsrc.servicesXML.SRVxml_recupNomNoeud(action)=="ETAPE" :
      etape=traiterEtapeXML(fichierXml,action,listVariables,listVariablesBCL,projet,parallel)
      listActions.append({'nbiter':1,'libelle':'','condition':None,'actions':[etape]})
    elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action)=="ACTION" :
      (res,elt)=traiterActionXML(fichierXml,rapportActions,action,listVariables,listVariablesBCL,projet,parallel)
      if res is None:
        return None
      else:
        subList={'nbiter':1,'libelle':'','condition':None,'actions':[res]}
        listActions.append(subList)
      if elt is not None:
        dictElements[elt]=None
    elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action)=="BLOC_ACTIONS":
      subList={'nbiter':1,'libelle':'','condition':None,'actions':[]}
      ndFils=nngsrc.servicesXML.SRVxml_recupNoeudFils(action)
#      print "DBG-BLOC_ACTIONS>",action
      ndCondition=ndFils[0]
      #(res,elt)=traiterActionXML(fichierXml,action2,listVariables,listVariablesBCL,projet)
      appcondition=nngsrc.servicesXML.SRVxml_recupValAttribut(ndCondition,'horsrapport')=='oui'
      condition=nngsrc.servicesXML.SRVxml_recupTexteNoeud(ndCondition,'')
      condition=remplacerVariables(fichierXml,condition,listVariables,listVariablesBCL,projet,parallel)
      for action2 in ndFils:
          #----ETAPE----------
          if nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="ETAPE" :
            res=traiterEtapeXML(fichierXml,action2,listVariables,listVariablesBCL,projet,parallel)
            if res["CONDITION"] is not None and condition is not None:
              res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
              res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
            elif condition is not None:
              res["CONDITION"]=condition
              res["APP_CONDITION"]=appcondition
            subList['actions'].append(res)
          #----ACTION----------
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="ACTION":
            (res,elt)=traiterActionXML(fichierXml,rapportActions,action2,listVariables,listVariablesBCL,projet,parallel)
            if res is None:
              return None
            if res["CONDITION"] is not None and condition is not None:
              res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
              res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
            elif condition is not None:
              res["CONDITION"]=condition
              res["APP_CONDITION"]=appcondition
            subList['actions'].append(res)
            if elt is not None:
              dictElements[elt]=None
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="FICHIER":
            (AppelFichier,parametres)=recupNomfichier(action2,"FICHIER")
            listActions2,lVariablesBCL2=chargerActionsXML(repFiches,AppelFichier,parametres,projet,parallel)

            for action3 in listActions2:
              #----ETAPE----------
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(action3)=="ETAPE":
                res=traiterEtapeXML(fichierXml,action3,listVariables,listVariablesBCL,projet,parallel)
                if res["CONDITION"] is not None:
                  res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
                  res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
                elif condition is not None:
                  res["CONDITION"]=condition
                  res["APP_CONDITION"]=appcondition
                subList['actions'].append(res)
              #----ACTION----------
              elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action3)=="ACTION":
                (res,elt)=traiterActionXML(fichierXml,rapportActions,action3,listVariables,listVariablesBCL,projet,parallel)
                if res is None:
                  return None
                if res["CONDITION"] is not None:
                  res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
                  res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
                elif condition is not None:
                  res["CONDITION"]=condition
                  res["APP_CONDITION"]=appcondition
                subList['actions'].append(res)
                if elt is not None:
                  dictElements[elt]=None
      listActions.append(subList)
    elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action)=='BOUCLE':
      #print "DBG-BOUCLE>"
      subList={'nbiter':1,'libelle':'','condition':None,'actions':[]}
      if nngsrc.servicesXML.SRVxml_recupValAttribut(action,'nbiterations'):
        subList['nbiter']=nngsrc.servicesXML.SRVxml_recupValAttribut(action,'nbiterations')
      elif nngsrc.servicesXML.SRVxml_recupValAttribut(action,'iterations'):
        subList['nbiter']=nngsrc.servicesXML.SRVxml_recupValAttribut(action,'iterations')
      else:
        subList['nbiter']="adLib"
      subList['libelle']=nngsrc.servicesXML.SRVxml_recupValAttribut(action,'libelle')
      varList=[]
      nbListe=1
      condition=None
      ndFils=nngsrc.servicesXML.SRVxml_recupNoeudFils(action)
      for action2 in ndFils:
#          print "BALISE>",nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)
          #----CONDITION----------(tant que)----------------
          if nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="CONDITION":
#            print "Gestion Condition"
            appcondition=nngsrc.servicesXML.SRVxml_recupValAttribut(action2,'horsrapport')=='oui'
            condition=nngsrc.servicesXML.SRVxml_recupTexteNoeud(action2)
            condition=remplacerVariables(fichierXml,condition,listVariables,listVariablesBCL,projet,parallel)
            subList['condition']=condition
            condition=None
          #----LISTE----------
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="LISTE":
            parametres=nngsrc.servicesXML.SRVxml_recupTexteNoeud(action2)
            parametres=remplacerVariables(fichierXml,parametres,listVariables,listVariablesBCL,projet,parallel)
            nomVar=nngsrc.servicesXML.SRVxml_recupValAttribut(action2,'var')
            listDeVar=[nomVar]
            listDeValeur=parametres.split(',')
            listDeVar.extend(listDeValeur)
            varList.insert(0,listDeVar)
#            varList.append(listDeVar)
            nbListe=nbListe*len(listDeValeur)
          #----ETAPE----------
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="ETAPE" :
            res=traiterEtapeXML(fichierXml,action2,listVariables,listVariablesBCL,projet,parallel)
            if res["CONDITION"] is not None and condition is not None:
              res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
            elif condition is not None:
              res["CONDITION"]=condition
            subList['actions'].append(res)
          #----ACTION----------
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="ACTION":
            (res,elt)=traiterActionXML(fichierXml,rapportActions,action2,listVariables,listVariablesBCL,projet,parallel)
            if res is None:
              return None
#            print "PDN>",res["CONDITION"]
            if res["CONDITION"] is not None and condition is not None:
              res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
            elif condition is not None:
              res["CONDITION"]=condition
#            print res["CONDITION"]
            subList['actions'].append(res)
            if elt is not None:
              dictElements[elt]=None
            res["LISTE"]=varList
            if len(varList)!=0:
              subList['nbiter']=nbListe
          #----FICHIER----------
          elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action2)=="FICHIER":
            (AppelFichier,parametres)=recupNomfichier(action2,"FICHIER")
            listActions2,lVariablesBCL2=chargerActionsXML(repFiches,AppelFichier,parametres,projet,parallel)
            for action3 in listActions2:
              res=None
              elt=None
              #----ETAPE----------
              if nngsrc.servicesXML.SRVxml_recupNomNoeud(action3)=="ETAPE" :
                res=traiterEtapeXML(fichierXml,action3,listVariables,listVariablesBCL,projet,parallel)
                if res["CONDITION"] is not None and condition is not None:
                  res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
                  res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
                elif condition is not None:
                  res["CONDITION"]=condition
                  res["APP_CONDITION"]=appcondition
                subList['actions'].append(res)
              #----ACTION----------
              elif nngsrc.servicesXML.SRVxml_recupNomNoeud(action3)=="ACTION":
                (res,elt)=traiterActionXML(fichierXml,rapportActions,action3,listVariables,listVariablesBCL,projet,parallel)
                if res is None:
                  return None
                if res["CONDITION"] is not None and condition is not None:
                  res["CONDITION"]="(%s)ET(%s)"%(condition,res["CONDITION"])
                  res["APP_CONDITION"]=res["APP_CONDITION"] or appcondition
                elif condition is not None:
                  res["CONDITION"]=condition
                  res["APP_CONDITION"]=appcondition
                subList['actions'].append(res)
                if elt is not None:
                  dictElements[elt]=None
              res["LISTE"]=varList
              if len(varList)!=0:
                subList['nbiter']=nbListe
#      print ("PDN>",subList)
#      for s in subList:
#        print(s,subList[s])
      listActions.append(subList)

  entite=remplacerVariables(fichierXml,entite,listVariables,listVariablesBCL,projet,parallel)
  methode=remplacerVariables(fichierXml,methode,listVariables,listVariablesBCL,projet,parallel)
  typo=remplacerVariables(fichierXml,typo,listVariables,listVariablesBCL,projet,parallel)
  nature=remplacerVariables(fichierXml,nature,listVariables,listVariablesBCL,projet,parallel)
  commentaire=remplacerVariables(fichierXml,commentaire,listVariables,listVariablesBCL,projet,parallel)
  exigences=remplacerVariables(fichierXml,exigences,listVariables,listVariablesBCL,projet,parallel)
  description=(entite,methode,typo,nature,commentaire,exigences)

  for var in listVariables:
    if var['UTILE']==False and projet.LOG_COMPLET:
      nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"Donnee d'environnement '"+var['NOM']+"' inutilisee.",echo=False,style="r",parallel=parallel)
  return fiche,description,listVariables,listJDD,listActions,dictElements,rapportActions

#-----------------------------------------------------------------------------------
def tagFichiersFicheXML(repFiches,fichiers):
  for fichier in fichiers:
    fichierXml=nngsrc.services.SRV_TraiterJoin(nngsrc.servicesXML.SRVxml_recupTexteNoeud(fichier).split(' ')[0])
    fiche=nngsrc.servicesXML.SRVxml_parser(os.path.join(repFiches,fichierXml))
    listeActions=nngsrc.servicesXML.SRVxml_recupTousNoeuds(fiche,'ACTION')
    #print "DBG-tagFichiers>",listeActions
    modif=False
    for numAct in range(len(listeActions)):
      #print "DBG>",nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG')
      if nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG') is None:
        modif=True
        tag=datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
        time.sleep(1)
        tag=tag[:len(tag)-3]
        eTag=nngsrc.servicesXML.SRVxml_creerNoeud(balise="TAG",texte=tag,pere=listeActions[numAct])
    if modif:
      if os.path.exists("%s.bak"%os.path.join(repFiches,fichierXml)): os.remove("%s.bak"%os.path.join(repFiches,fichierXml))
      os.rename(os.path.join(repFiches,fichierXml),"%s.bak"%os.path.join(repFiches,fichierXml))
      contenu="<?xml version='1.0' encoding='utf-8'?>\n"
      #print "DBG>",nngsrc.servicesXML.SRVxml_print(fiche)
      contenu+=nngsrc.servicesXML.SRVxml_print(fiche)
#      contenu+=etree.tostring(fiche, pretty_print=True,encoding='utf-8')
#      contenu=contenu.replace("</TAG><","</TAG>\n      <")
      f=open(os.path.join(repFiches,fichierXml),"w",encoding='utf-8')
      f.write("%s\n"%contenu)
      f.close()

#-----------------------------------------------------------------------------------
def tagActionsFicheXML(repRapport,repFiches,fichierXml,projet,TAG=None):
  if not os.access(os.path.join(repFiches, fichierXml), os.F_OK):
    nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"ERREUR : Fiche %s inexistante\n"%fichierXml,style="Er")
    return None
  racine=nngsrc.servicesXML.SRVxml_parser(os.path.join(repFiches,fichierXml))
  actions=nngsrc.servicesXML.SRVxml_recupNoeud(racine,'ACTIONS')
  fichiers=nngsrc.servicesXML.SRVxml_recupTousNoeuds(actions,'FICHIER')
  tagFichiersFicheXML(repFiches,fichiers)
  listeActions=nngsrc.servicesXML.SRVxml_recupTousNoeuds(actions,'ACTION')
#  print "DBG>",listeActions
  modif=False
  for numAct in range(len(listeActions)):
    curTag=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG'),None)
    curFct=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'FONCTION'),None)
    if curTag is None or curTag==TAG:
      modif=True
      tag=datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
      time.sleep(1)
      tag=tag[:len(tag)-3]
      if curTag is None:
        f=open(os.path.join(repRapport, "nng-analyses", "ajustementsTAG.txt"),"a",encoding='utf-8')
        f.write("     Ajout du TAG sur l'action %s dans la fiche %s.\n"%(curFct,fichierXml))
        f.close()
        nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Ajout du TAG sur l'action %s dans la fiche %s."%(curFct,fichierXml),style="Wr")
        nngsrc.servicesXML.SRVxml_creerNoeud(balise="TAG",texte=tag,pere=listeActions[numAct])
      else:
        f=open(os.path.join(repRapport, "nng-analyses", "ajustementsTAG.txt"),"a",encoding='utf-8')
        f.write("     Modification du TAG de l'action %s dans la fiche %s.\n"%(curFct,fichierXml))
        f.close()
        nngsrc.services.SRV_Terminal(projet.uiTerm, projet.debug, projet.log,"AVERTISSEMENT : Modification du TAG d'action dans la fiche %s." % fichierXml, style="Wr")
        eTAG=nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG')
        nngsrc.servicesXML.SRVxml_modifTexteNoeud(eTAG,tag)

  listeBlocsActions=nngsrc.servicesXML.SRVxml_recupTousNoeuds(actions,'BLOC_ACTIONS')
  for bloc in range(len(listeBlocsActions)):
    tagFichiersFicheXML(repFiches,nngsrc.servicesXML.SRVxml_recupTousNoeuds(listeBlocsActions[bloc],'FICHIER'))
    listeActions=nngsrc.servicesXML.SRVxml_recupTousNoeuds(listeBlocsActions[bloc],'ACTION')
    for numAct in range(len(listeActions)):
      if nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG') is None:
        curTag=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG'),None)
        curFct=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'FONCTION'),None)
        if curTag is None or curTag==TAG:
          modif=True
          tag= datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
          time.sleep(1)
          tag=tag[:len(tag)-3]
          if curTag is None:
            f=open(os.path.join(repRapport, "nng-analyses", "ajustementsTAG.txt"),"a",encoding='utf-8')
            f.write("     Ajout du TAG sur l'action %s (BLOC_ACTIONS) dans la fiche %s\n"%(curFct,fichierXml))
            f.close()
            nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Ajout du TAG sur l'action %s (BLOC_ACTIONS) dans la fiche %s"%(curFct,fichierXml),style="Wr")
            nngsrc.servicesXML.SRVxml_creerNoeud(balise="TAG",texte=tag,pere=listeActions[numAct])
          else:
            f=open(os.path.join(repRapport, "nng-analyses", "ajustementsTAG.txt"),"a",encoding='utf-8')
            f.write("     Modification du TAG de l'action %s dans la fiche %s.\n"%(curFct,fichierXml))
            f.close()
            nngsrc.services.SRV_Terminal(projet.uiTerm, projet.debug, projet.log,"AVERTISSEMENT : Modification du TAG de l'action %s dans la fiche %s." %(curFct,fichierXml),style="Wr")
            eTAG=nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG')
            nngsrc.servicesXML.SRVxml_modifTexteNoeud(eTAG,tag)

  listeBloucle=nngsrc.servicesXML.SRVxml_recupTousNoeuds(actions,'BOUCLE')
  for boucle in range(len(listeBloucle)):
    tagFichiersFicheXML(repFiches,nngsrc.servicesXML.SRVxml_recupTousNoeuds(listeBloucle[boucle],'FICHIER'))
    listeActions=nngsrc.servicesXML.SRVxml_recupTousNoeuds(listeBloucle[boucle],'ACTION')
    for numAct in range(len(listeActions)):
      curTag=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG'),None)
      curFct=nngsrc.servicesXML.SRVxml_recupTexteNoeud(nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'FONCTION'),None)
#      if nngsrc.servicesXML.SRVxml_recupNoeud(listeActions[numAct],'TAG') is None:
      if curTag is None:
        modif=True
        tag= datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
        time.sleep(1)
        tag=tag[:len(tag)-3]
        f=open(os.path.join(repRapport, "nng-analyses", "ajustementsTAG.txt"),"a",encoding='utf-8')
        f.write("     Ajout du TAG sur l'action %s (BOUCLE) dans la fiche %s\n"%(curFct,fichierXml))
        f.close()
        nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,projet.log,"AVERTISSEMENT : Ajout du TAG sur l'action %s (BOUCLE) dans la fiche %s"%(curFct,fichierXml),style="Wr")
        nngsrc.servicesXML.SRVxml_creerNoeud(balise="TAG",texte=tag,pere=listeActions[numAct])

  if modif:
    if os.path.exists("%s.bak"%os.path.join(repFiches,fichierXml)): os.remove("%s.bak"%os.path.join(repFiches,fichierXml))
    os.rename(os.path.join(repFiches,fichierXml),"%s.bak"%os.path.join(repFiches,fichierXml))
    contenu="<?xml version='1.0' encoding='utf-8'?>\n"
#    print "DBG>",nngsrc.servicesXML.SRVxml_print(racine)
    contenu+=nngsrc.servicesXML.SRVxml_print(racine)
#    contenu+=etree.tostring(fiche, pretty_print=True,encoding='utf-8')
#    contenu=contenu.replace("</TAG><","</TAG>\n      <")
    f=open(os.path.join(repFiches,fichierXml),"w",encoding='utf-8')
    f.write("%s\n"%contenu)
    f.close()

#-----------------------------------------------------------------------------------
def creerAction(num,projet,repFiches,fiche):
  nngsrc.services.SRV_Terminal(projet.uiTerm,projet.debug,None,projet.lexique.entree('LOCUT_DEMANDE_ACTION')%num,style="r")
  commentaire=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_OBJECTIF'),"SAISIE",style="%s<br>",espacement=2)
  if num>1 and commentaire.lower() in ["f","t"]:
    return None, commentaire.lower()

  f=open(os.path.join(repFiches,"..","magV-enregistrements","Fiches",projet.nomRepertoireALaVolee,fiche+".xml"),"a",encoding='utf-8')
  f.write('    <ACTION>\n')
  f.write('      <COMMENTAIRE>%s</COMMENTAIRE>\n'%commentaire)
  f.write('      <FONCTION>VALID_EtapeManuelle</FONCTION>\n')
  f.write('      <PARAMETRE>%s</PARAMETRE>\n'%commentaire)
  f.write('      <RETOUR>0</RETOUR>\n')
  f.write('    </ACTION>\n')
  f.close()

  return num+1,[1, '', {'VALIDER': 'non', 'VALIDER_RAISON':'', 'SIMILITUDE': None, 'FT': 0, 'BOUCLE': None, 'CASSEP': 0, 'SORTIE_SI_VIDE': self.projet.lexique.entree('LOCUT_AUCUNE_SORTIE'), 'EXIGENCES': '', 'LIBELLE': None, 'ECRAN': None, 'APP_CONDITION': False, 'ELEMENT': None, 'SOURIS': None, 'FAITS_TECHNIQUES': '', 'FONCTION': u'VALID_EtapeManuelle', 'LISTE': None, 'RAPPORT': 0, 'ID': '', 'CONDITION': None,
                         'PARAMETRE': commentaire, 'COMMENTAIRE': commentaire}]

#-----------------------------------------------------------------------------------
def creerFicheXML(dico,repFiches,fichierXml,listVariables,projet):
  fiche=os.path.basename(fichierXml)
  fiche=os.path.splitext(fiche)[0]

  commentaire=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_COMMENTAIRE'),"SAISIE",style="%s<br>",espacement=2)

  entite=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_ENTITE'),"SAISIE",style="%s<br>",espacement=2)
  if entite=='': entite="Entite AD"

  methode=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_METHODE'),"SAISIE",style="%s<br>",espacement=2)
  if methode=='': methode="Test"

  typo=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_TYPO'),"SAISIE",style="%s<br>",espacement=2)
  if typo=='': typo="Nominal"

  nature=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_NATURE'),"SAISIE",style="%s<br>",espacement=2)
  if nature=='': nature="Fonctionnel"

  exigences=nngsrc.services.SRV_AttenteOperateur(projet.uiTerm,projet.debug,projet.lexique.entree('LOCUT_DEMANDE_EXIGENCES'),"SAISIE",style="%s<br>",espacement=2)
  if exigences=='': exigences=''

  description=(entite,methode,typo,nature,commentaire,exigences)
  dictElements={}

  debuterFicheXML(os.path.join(repFiches,"..","magV-enregistrements","Fiches",projet.nomRepertoireALaVolee,fiche+".xml"),(entite,methode,typo,nature,commentaire,exigences))
  (num,action)=creerAction(1,projet,repFiches,fiche)
  listActions=[action]

  return num, (fiche, description, listVariables, listActions, dictElements,True)

#-----------------------------------------------------------------------------------
def debuterFicheXML(ficFiche,description=('','','','','','')):
  repFiches=os.path.dirname(ficFiche)
  if not os.path.isdir(repFiches):
    os.makedirs(repFiches)
  (entite,methode,typo,nature,commentaire,exigences)=description
  f=open(ficFiche,"w",encoding='utf-8')
  f.write('<?xml version="1.0" encoding="utf-8"?>\n')
  f.write('<FICHE>\n')
  f.write('  <DESCRIPTION>\n')
  f.write('    <ENTITE>%s</ENTITE>\n'%entite)
  f.write('    <METHODE>%s</METHODE>\n'%methode)
  f.write('    <TYPE>%s</TYPE>\n'%typo)
  f.write('    <NATURE>%s</NATURE>\n'%nature)
  f.write('    <COMMENTAIRE>%s</COMMENTAIRE>\n'%commentaire)
  f.write('    <EXIGENCES>%s</EXIGENCES>\n'%exigences)
  f.write('  </DESCRIPTION>\n')
  f.close()

#-----------------------------------------------------------------------------------
def terminerCreerFiche(repFiches,fiche,projet):
  f=open(os.path.join(repFiches,"..","magV-enregistrements","Fiches",projet.nomRepertoireALaVolee,fiche+".xml"),"a",encoding='utf-8')
  f.write('  </ACTIONS>\n')
  f.write('</FICHE>\n')
  f.close()
  f=open(os.path.join(repFiches,"..","magV-enregistrements","Fiches",projet.nomRepertoireALaVolee,projet.nomScenarioALaVolee,projet.nomRapportALaVolee),"a",encoding='utf-8')
  f.write('%s.xml\n'%fiche)
  f.close()
  f=open(os.path.join(repFiches,"..","magV-enregistrements","Fiches",projet.nomRepertoireALaVolee,"fiches.lst"),"a",encoding='utf-8')
  f.write('%s.xml\n'%fiche)
  f.close()

#-----------------------------------------------------------------------------------
