#!/usr/bin/python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

#===============================================================================
#  Chargement de l'environnement
#===============================================================================
import os,time,math,PIL.Image
import numpy as np

try:
  import matplotlib
  matplotlib.use('agg')
  import matplotlib.pyplot as plt
  from mpl_toolkits.mplot3d import Axes3D
  from matplotlib.colors import LinearSegmentedColormap
  import matplotlib.path as path
  import matplotlib.patches as patches
  MATPLOTLIB=True
  GNUPLOT=False
except:
  MATPLOTLIB=False
  GNUPLOT=False
MARGIN=0.05

#-----------------------------------------------------------------------------------
def palette(pPalette=None):
  #COULEURS=[   "lime",     "aqua",         "red",    "blue",  "yellow",    "fushia",      "purple" ,"teal",   "olive",  "gray"   ,"marron", "orange",]
  #COULEURS  = ['#C0FF80',    '#80ffff'    ,'#ff0000','#0000ff','#ffff00',  '#ff80ff',     '#8000ff','#008080','#808000','#808080','#800000','#ff8000' ]


  #COULEURS=['SkyBlue','MediumSeaGreen','LightCoral','Crimson','PaleTurquoise','Thistle','Yellow' ,'Tan'    ,'Silver']
  #COULEURS=['#87CEEB','#3CB371'       ,'#F08080'   ,'#DC143C','#AFEEEE'      ,'#D8BFD8','#FFFF00','#D2B48C','#C0C0C0']

  #          Bleu-gris Vert-anis Rose      Jaune     Chair     Gris      Vert-d'eau Orange    Bleu ciel  Vert      Vieux Rose Bleu      Vert      Rouge
  couleurs= ['#8DA0CB','#A6D854','#E78AC3','#FFD92F','#E5C494','#B3B3B3','#66C2A5', '#FC8D62','#A6CEE3', '#B2DF8A','#FB9A99', '#1F78B4','#33A02C','#E31A1C']
  return couleurs

#-----------------------------------------------------------------------------------
def GRAPH_parametresDefaut(pMATPLOTLIB=True,pGNUPLOT=True,pPalette=None):
  parametres={
              'grille':None,
              'largbar':0.8,
              'alpha':0.5,
              'largeur':800,
              'hauteur':600,
              'legende':'*',
              'rotation':45,
              'amplitude':True,
              'ylim':None,
              'lignes':'-',
              'points':'.',
              'couleurs':palette(pPalette),
              'lut':16,
              'format':'%1.1f%%',
              'shadow':True,
              'explode':None,
              'ticsX':None,
              'offset':'0,1',
              'doubleAxe':False,
              'dpi':100,
              'MATPLOTLIB':MATPLOTLIB and pMATPLOTLIB,
              'GNUPLOT':GNUPLOT and pGNUPLOT,
              'export':'png',
              '8b':True
              }
  return parametres

#-----------------------------------------------------------------------------------
def convertLut(pFichierPNG,pNbCoul):
  if not os.path.exists(pFichierPNG): return
#  res=pFichierPNG.replace('.png','wu.png')
  res=pFichierPNG
#  respil=pFichierPNG.replace('.png','wupil.png')
  respil=pFichierPNG
#  print 'Wu -im %s -nb %d -res %s --sf'%(pFichierPNG,pNbCoul*2,res)
#  os.system('Wu -im %s -nb %d -res %s --sf'%(pFichierPNG,pNbCoul*2,res))
  im32=PIL.Image.open(res)
#  print "im32.mode",im32.mode
  if im32.mode=='RGB':
    im8=im32.convert('RGB').convert('P',palette=PIL.Image.ADAPTIVE,colors=pNbCoul)
    im8.save(respil,format='PNG')

#-----------------------------------------------------------------------------------
def matplotCreationFigure(pTitre,pParametres,ticsX,libelleX,ticsY=None,libelleY=None,ticsZ=None,polar=False):
  grille=pParametres['grille']
  largeur=pParametres['largeur']
  hauteur=pParametres['hauteur']

  figure=plt.figure(figsize=(largeur/100.,hauteur/100.))

  if ticsZ is not None:
    #axes3D=plt.axes(projection='3d')
    axe1=figure.add_subplot(111,projection='3d')
  else:
    axe1=figure.add_subplot(111,polar=polar)
  axe1.margins(MARGIN)
  plt.title(pTitre,fontsize=10)


  # Gestion des tics des coordonnees
  if ticsX is not None:
    if ticsX!="off": plt.tick_params(axis='x',labelsize=8)
    else: plt.tick_params(axis='x',labelbottom='off',labeltop='off',labelsize=8)

  if ticsY:
    if ticsY!="off": plt.tick_params(axis='y',labelsize=8)
    else: plt.tick_params(axis='y',labelleft='off',labelright='off',labelsize=8)

  if ticsZ:
    if ticsZ!="off": plt.tick_params(axis='z',labelsize=8)
    else: plt.tick_params(axis='z',labelleft='off',labelright='off',labelsize=8)

  # Gestion des labels des coordonnees
  plt.xlabel(libelleX,fontsize=8)
  if libelleY: plt.ylabel(libelleY,fontsize=8)

  # Affichage de la grille
  if grille:
    plt.grid(which='both',axis=grille)

  return figure,axe1

##-----------------------------------------------------------------------------------
#def palette():
#  #COULEURS=[   "lime",     "aqua",         "red",    "blue",  "yellow",    "fushia",      "purple" ,"teal",   "olive",  "gray"   ,"marron", "orange",]
#  #COULEURS  = ['#C0FF80',    '#80ffff'    ,'#ff0000','#0000ff','#ffff00',  '#ff80ff',     '#8000ff','#008080','#808000','#808080','#800000','#ff8000' ]
#
#  #          Bleu-gris Vert-anis Rose      Jaune     Chair     Gris      Vert-d'eau Orange    Bleu ciel  Vert      Vieux Rose Bleu      Vert      Rouge
#  COULEURS= ['#8DA0CB','#A6D854','#E78AC3','#FFD92F','#E5C494','#B3B3B3','#66C2A5', '#FC8D62','#A6CEE3', '#B2DF8A','#FB9A99', '#1F78B4','#33A02C','#E31A1C']
#
#  #COULEURS=['SkyBlue','MediumSeaGreen','LightCoral','Crimson','PaleTurquoise','Thistle','Yellow' ,'Tan'    ,'Silver']
#  #COULEURS=['#87CEEB','#3CB371'       ,'#F08080'   ,'#DC143C','#AFEEEE'      ,'#D8BFD8','#FFFF00','#D2B48C','#C0C0C0']
#  return COULEURS
#
#-----------------------------------------------------------------------------------
def localisation(loc):
  if loc=="*": return 'best',None
  out=False
  if loc[0]=="O":
    out=True
    loc=loc[1:]
  if loc=="HD":return 'upper right',out
  elif loc=="HG":return 'upper left',out
  elif loc=="BG":return 'lower left',out
  elif loc=="BD":return 'lower right',out
  elif loc=="CG":return 'center left',out
  elif loc=="CD":return 'center right',out
  elif loc=="BC":return 'lower center',out
  elif loc=="HC":return 'upper center',out
  else: return 'best',None

##-----------------------------------------------------------------------------------
#def calculs(v):
#  somme=float(v[0]+v[1]+v[2])
#  p=[0,0,0]
#  pn=[0,0,0]
#  pcn=[0,0,0]
#  pnr=[0,0,0]
#  x=[0,0,0]
#  y=[0,0,0]
#  for e in range(3):
#    p[e]=float(v[e]*100)/somme
#    pcn[e]=p[e]
#  pcn[1]+=p[0]
#  pcn[2]+=p[0]+p[1]
#  for e in range(3):
#    pn[e]=p[e]*3.6
#    pcn[e]=int(round(pcn[e]*3.6))
#    pnr[e]=pn[e]*math.pi/180
#  x[0]=0.5*math.cos(pnr[0]*0.5)
#  y[0]=0.5*math.sin(pnr[0]*0.5)
#  x[1]=0.5*math.cos(pnr[0]+pnr[1]*0.5)
#  y[1]=0.5*math.sin(pnr[0]+pnr[1]*0.5)
#  x[2]=0.5*math.cos(pnr[0]+pnr[1]+pnr[2]*0.5)
#  y[2]=0.5*math.sin(pnr[0]+pnr[1]+pnr[2]*0.5)
#  return (p,x,y,pcn)
#
##-----------------------------------------------------------------------------------
def lireContenuListe(fichier,encodage="utf-8"):
  contenu=[]
  if os.access(fichier, os.F_OK):
    f=open(fichier,"r")
    contenu=f.read().decode(encodage)
    f.close()
    contenu=contenu.replace('\r','').split('\n')
    if contenu[-1]=='':
      contenu=contenu[:-1]
  return contenu

#-----------------------------------------------------------------------------------
def lireDonnees(donneeDAT):
  contenuDAT=lireContenuListe(donneeDAT)
  ligneMesures=contenuDAT[3].split(' ')[1:]
  for numLigne in range(len(contenuDAT)):
    if contenuDAT[numLigne][0]!='#':
      contenuDAT[numLigne]=contenuDAT[numLigne].split(' ')
  return contenuDAT,ligneMesures

##-----------------------------------------------------------------------------------
def labelTics(axe,ligneMesures):
  tics=axe['label']
  label=axe['colTrace']
  index=ligneMesures.index(label)
  return label,tics,index

#-----------------------------------------------------------------------------------
def enteteFichier(donneeGraphique,courbe,axe,labelX,labelY=None):
  # - Entete
  fdat=open("%s-%s.dat"%(donneeGraphique,courbe['colTrace']),"w")
  fdat.write("#\n# %s - %s\n#\n"%(donneeGraphique,courbe['colTrace']))
  if not labelY: fdat.write("# %s"%labelX)
  else: fdat.write("# %s %s"%(labelX,labelY))
  tics=courbe['label']
  axeLabel=courbe['colTrace']
#  for axeTuple in axe:
#    if len(axeTuple)==1: (axeLabel,)=axeTuple
#    elif len(axeTuple)==2: (axeLabel,_)=axeTuple
#    else: (axeLabel,_,_)=axeTuple
  fdat.write(" %s"%axeLabel)
  fdat.write("\n")
  return fdat,1

#-----------------------------------------------------------------------------------
#def donneesFiltrees(fdat,contenuDAT,ligneMesures,courbe,axe,colX,colY=None):
#  idxCourbe=ligneMesures.index(courbe['colTrace'])
#  for ligne in contenuDAT:
#    if ligne[0]!='#':
#      if courbe[1]=='*' or ligne[idxCourbe]==courbe[1]:
#        if not colY: fdat.write("%s"%ligne[colX])
#        else: fdat.write("%s %s"%(ligne[colX],ligne[colY]))
#        for axeTuple in axe:
#          (_,_,col)=labelTics(axeTuple,ligneMesures)
#          fdat.write(" %s"%ligne[col])
#        fdat.write("\n")
#  fdat.close()
#
#def lectureFichierCourbe(donneeGraphique,courbe):
#  fichierDat="%s-%s.dat"%(donneeGraphique,courbe.replace('*','x'))
#  contenu=services.SRV_LireContenu(fichierDat)
#  contenu=contenu.split('\n')
#  if len(contenu)<6: return(None,None,None,None,None)
#
#  valeursX=[]
#  valeursXs=[]
#  for ligne in range(len(contenu[4:])):
#    if contenu[ligne+4]!='':
#      if contenu[ligne+4].replace('.','').isdigit():
#        valeursX.append(float(ligne.split(' ')[0]))
#        valeursXs.append(ligne.split(' ')[0])
#      else:
#        valeursX.append(ligne+1)
#        valeursXs.append(contenu[ligne+4].split(' ')[0])
#
#  return (valeursX,valeursXs,arange(len(valeursX)),contenu,fichierDat)
#-----------------------------------------------------------------------------------
def valeursTrace(axe,valeurs,numCourbe,lim,vmin,vmax,flagAnnotations):
  #  Gestion des limites
  limmin=None;  limmax=None
  valideYlim=False
  if lim:
    (limmin,limmax)=lim
    for v in valeurs:
      if v>limmax: valideYlim=True
  if not valideYlim:
    lim=None
  if lim and flagAnnotations:
    (limmin,limmax)=lim
    valeursTrace=[]
    for v in valeurs:
      if v>limmax: valeursTrace.append(limmax)
      else: valeursTrace.append(v)
  else:
    valeursTrace=valeurs
#  return min(vmin,min(valeurs)),max(vmax,max(valeurs)),valeursTrace,limmin,limmax,lim
  return None,None,valeursTrace,limmin,limmax,lim # TODO car vmin et vmax sont toujours None : en attente !

#-----------------------------------------------------------------------------------
def GRAPH_Histo(pDonneeGraphique,pTitre,pX,pY,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  doubleAxe=pParametres['doubleAxe']
  largbar=pParametres['largbar']
  alpha=pParametres['alpha']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  ticsX=pX['tics']
  valeursX=pX['valeurs']

  ticsY=None

  # Creation de la figure
  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,'on',libelleX)
#  axe1=figure.add_subplot(111)
  axe1.margins(MARGIN)
  if doubleAxe:
    axe2=axe1.twinx()
    axe2.margins(MARGIN)

  # Donnees a tracer
  numCouleur=0
  ylabels={'y1':None,'y2':None}
  for courbe in pY:
    labelY=''
    if 'label' in courbe: labelY=courbe['label']
    valeursY=courbe['valeurs']
    flagAnnotations='annotations' in courbe
    if flagAnnotations:
      dAnnotationX,dAnnotationY,annotations=courbe['annotations']
      if annotations is None:
        annotations=valeursY

    # Valeurs et limites
    ymin=None; ymax=None
    (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,flagAnnotations)

    # Trace
    axe='y1'
    if 'axe' in courbe: axe=courbe['axe']
    if axe=='y1':
      axe1.bar(valeursX,valeursY,width=largbar,alpha=alpha,color=couleurs[numCouleur],label=labelY)
    if doubleAxe:
      if axe=='y2':
        axe2.bar(valeursX,valeursY,width=largbar,alpha=alpha,color=couleurs[numCouleur],label=labelY)
    if 'libelle' in courbe :ylabels[axe]=courbe['libelle']
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

    # Annotations des valeurs
    if flagAnnotations:
      positionsX=range(len(annotations))
      if axe=='y1':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe1.annotate(a,(x+dAnnotationX,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')
      if axe=='y2':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe2.annotate(a,(x+dAnnotationX,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')

  # Affichage des labels et ticks
  h1,l1=axe1.get_legend_handles_labels()
  axe1.set_xticklabels(['']+ticsX)
  axe1.set_ylabel(ylabels['y1'],fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(45)
  h2=[]
  l2=[]
  if doubleAxe:
    h2, l2 = axe2.get_legend_handles_labels()
    axe2.set_ylabel(ylabels['y2'],fontsize=8)
    axe2.tick_params(axis="both", labelsize=8)
  if doubleAxe:
    for tick in axe2.get_xticklabels():
      tick.set_rotation(45)

  # Legende
  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      axe1.legend(h1+h2,l1+l2,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      axe1.legend(h1+h2,l1+l2,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
    if doubleAxe: axe2.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)
    if doubleAxe: axe2.grid(linewidth=0)

  # Amplitude et limite du graphique
  if not amplitude:
    ymin=min(ymin); ymax=max(ymax)
    plt.ylim([(ymin-0.5*(ymax-ymin)), (ymax+0.5*(ymax-ymin))])
  elif ylim:
    (ylimmin,ylimmax)=ylim
    plt.ylim([ylimmin,ylimmax])

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
# Multi histo : https://stackoverflow.com/questions/50757879/matplotlib-legend-with-axis

def GRAPH_MultiHisto(pDonneeGraphique,pTitre,pX,pY,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  doubleAxe=pParametres['doubleAxe']
  largbar=pParametres['largbar']
  alpha=pParametres['alpha']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  ticsX=pX['tics']
  valeursX=[]
  for v in pX['valeurs']:
    valeursX.append(v)
  ticsY=None

  # Creation de la figure
  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,'on',libelleX)
  #axe1=figure.add_subplot(111)
  axe1.margins(MARGIN)
  if doubleAxe:
    axe2=axe1.twinx()
    axe2.margins(MARGIN)

  # Lecture du fichier des donnees a tracer
  numCouleur=0
  ylabels={'y1':None,'y2':None}
  decalage=pY[0]['decalage']
  for courbe in pY:
    labelY=''
    if 'label' in courbe: labelY=courbe['label']
    valeursY=courbe['valeurs']
    flagAnnotations='annotations' in courbe
    if flagAnnotations:
      dAnnotationX,dAnnotationY,annotations=courbe['annotations']
      if annotations is None:
        annotations=valeursY
    for n in range(len(pX['valeurs'])):
      valeursX[n]=float(pX['valeurs'][n])+courbe['decalage']

    # Valeurs et limites
    ymin=None; ymax=None
    (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,flagAnnotations)

    # Trace
    axe='y1'
    if 'axe' in courbe: axe=courbe['axe']
    if axe=='y1':
      axe1.bar(valeursX,valeursY,width=largbar,alpha=alpha,color=couleurs[numCouleur],label=labelY)
#      plt.bar(positionsX+largbar*numCouleur,valeursY,width=largbar,alpha=alpha,color=couleurs[numCouleur],label='%s %s'%(labelY,courbe))
    if axe=='y2':
      axe2.bar(valeursX,valeursY,width=largbar,alpha=alpha,color=couleurs[numCouleur],label=labelY)
    if 'libelle' in courbe :ylabels[axe]=courbe['libelle']
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

    # Annotations des valeurs
    if flagAnnotations:
      positionsX=range(len(annotations))
      if axe=='y1':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe1.annotate(a,(1+decalage+x+dAnnotationX+(numCouleur-1)*largbar,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')
      if axe=='y2':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe2.annotate(a,(1+decalage+x+dAnnotationX+(numCouleur-1)*largbar,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')

  # Affichage des labels et ticks
  axe1.set_xticks(pX['valeurs'])
  axe1.set_xticklabels(ticsX)

  h1, l1 = axe1.get_legend_handles_labels()
  axe1.set_ylabel(ylabels['y1'],fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(45)
  h2=[]
  l2=[]
  if doubleAxe:
    h2, l2 = axe2.get_legend_handles_labels()
    axe2.set_ylabel(ylabels['y2'],fontsize=8)
    axe2.tick_params(axis="both", labelsize=8)
    for tick in axe2.get_xticklabels():
      tick.set_rotation(45)

  # Legende
  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      axe1.legend(h1+h2,l1+l2,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      axe1.legend(h1+h2,l1+l2,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
    if doubleAxe: axe2.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)
    if doubleAxe: axe2.grid(linewidth=0)

  # Amplitude et limite du graphique
  if not amplitude:
    ymin=min(ymin); ymax=max(ymax)
    plt.ylim([(ymin-0.5*(ymax-ymin)), (ymax+0.5*(ymax-ymin))])
  elif ylim:
    (ylimmin,ylimmax)=ylim
    plt.ylim([ylimmin,ylimmax])

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_2D(pDonneeGraphique,pTitre,pX,pY,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  doubleAxe=pParametres['doubleAxe']
  lignes=pParametres['lignes']
  points=pParametres['points']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  ticsX=None
  if 'tics' in pX:
    ticsX=pX['tics']
  valeursX=pX['valeurs']

  # Creation de la figure
  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,'on',libelleX)
  #axe1=figure.add_subplot(111)
  axe1.margins(MARGIN)
  if doubleAxe:
    axe2 = axe1.twinx()
    axe2.margins(MARGIN)

  # Lecture du fichier des donnees a tracer
  numCouleur=0
  ylabels={'y1':None,'y2':None}
  for courbe in pY:
    labelY=''
    if 'label' in courbe: labelY=courbe['label']
    valeursY=courbe['valeurs']
    flagAnnotations='annotations' in courbe
    if flagAnnotations:
      dAnnotationX,dAnnotationY,annotations=courbe['annotations']
      if annotations is None:
        annotations=valeursY

    # Valeurs et limites
    ymin=None; ymax=None
    (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,flagAnnotations)

    # Lignes et Points
    linewidth=0
    if 'l' in courbe['trace']: linewidth=1.0
    marker=None
    if 'p' in courbe['trace']: marker=points

    # Trace
    axe='y1'
    if 'axe' in courbe: axe=courbe['axe']
    if axe=='y1':
      axe1.plot(valeursX,valeursY,linewidth=linewidth,linestyle=lignes,marker=marker,color=couleurs[numCouleur],label=labelY)
    if axe=='y2':
      axe2.plot(valeursX,valeursY,linewidth=linewidth,linestyle=lignes,marker=marker,color=couleurs[numCouleur],label=labelY)
    if 'libelle' in courbe :ylabels[axe]=courbe['libelle']
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

    # Annotations des valeurs
    if flagAnnotations:
      positionsX=range(len(annotations))
      if axe=='y1':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe1.annotate(a,(x+dAnnotationX,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')
      if axe=='y2':
        for x,a,y in zip(positionsX,annotations,valeursY):
          axe2.annotate(a,(x+dAnnotationX,y+dAnnotationY),fontsize=6,rotation=rotation,ha='left',va='bottom')

  # Affichage des labels et ticks
  if ticsX is not None:
    axe1.set_xticks(pX['valeurs'])
    axe1.set_xticklabels(ticsX)

  # Legende
  h1, l1 = axe1.get_legend_handles_labels()
  axe1.set_ylabel(ylabels['y1'],fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(45)
  h2=[]
  l2=[]
  if doubleAxe:
    h2, l2 = axe2.get_legend_handles_labels()
    axe2.set_ylabel(ylabels['y2'],fontsize=8)
    axe2.tick_params(axis="both", labelsize=8)
  if doubleAxe:
    for tick in axe2.get_xticklabels():
      tick.set_rotation(45)
#  if ticsX is not None:
#    axe1.set_xticklabels(ticsX)

  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      axe1.legend(h1+h2,l1+l2,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      axe1.legend(h1+h2,l1+l2,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
    if doubleAxe: axe2.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)
    if doubleAxe: axe2.grid(linewidth=0)

  # Amplitude et limite du graphique
  if not amplitude:
    ymin=min(ymin); ymax=max(ymax)
    plt.ylim([(ymin-0.5*(ymax-ymin)), (ymax+0.5*(ymax-ymin))])
  elif ylim:
    (ylimmin,ylimmax)=ylim
    plt.ylim([ylimmin,ylimmax])

 # plt.xticks(rotation=rotation)

  # Affichage des ticks
#WAIT   if ticsX!=None and ticsX=='lib':
#WAIT     plt.xticks(valeursX,valeursXs,rotation=rotation)
#WAIT   elif ticsX!=None and ticsX!='off':
#WAIT     if type(ticsX)==tuple:
#WAIT       if len(ticsX)==1:
#WAIT         (tics,)=ticsX
#WAIT         plt.xticks(tics)
#WAIT       else:
#WAIT         (tics,labtics)=ticsX
#WAIT         plt.xticks(tics,labtics)
#WAIT     else:

# INTERET ?  xmin,xmax=plt.xlim()
# INTERET ?  plt.xticks(arange(xmin+MARGIN,xmax-MARGIN,dticsX))
# INTERET ?  if ticsY!=None and ticsY!='off':
# INTERET ?    if type(ticsY)==list:
# INTERET ?      if len(ticsY)==1:
# INTERET ?        (tics,)=ticsY
# INTERET ?        plt.yticks(tics)
# INTERET ?      else:
# INTERET ?        (tics,labtics)=ticsY
# INTERET ?        plt.yticks(tics,labtics)
# INTERET ?    else:
# INTERET ?     ymin,ymax=plt.ylim()
# INTERET ?     plt.yticks(arange(ymin,ymax,ticsY))

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
#  try:
  plt.tight_layout()
  plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
#  except:
#    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_2DZcoul(pDonneeGraphique,pTitre,pX,pY,pZ,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  valeursX=pX['valeurs']

  libelleY=pY['libelle']
  valeursY=pY['valeurs']

  valeursZ=pZ['valeurs']
  format=pZ['format']

  # Creation de la figure
  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,'on',libelleX)

  for n in range(len(valeursZ)):
    valeursZ[n]=(int(valeursZ[n]))
  minZ=min(valeursZ)
  maxZ=max(valeursZ)

  # Valeurs et limites
  ymin=None; ymax=None
  (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,None)

#  linewidth=0
#  if 'l' in pY['trace']: linewidth=1.0
#  marker=None
#  if 'p' in pY['trace']: marker=points
  # Trace
  cm = LinearSegmentedColormap.from_list('my_list',couleurs[minZ:maxZ+1], N=maxZ-minZ+1)
  sctr=axe1.scatter(valeursX,valeursY,c=[x-minZ for x in valeursZ],edgecolors='none',cmap=cm,s=16.)
  #print(plt.rcParams['lines.markersize'] ** 2)

  # Affichage des labels et ticks
  h1,l1=axe1.get_legend_handles_labels()
  axe1.set_ylabel(libelleY,fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(rotation)

  # Legende
#  if legende!=False:
#    if legende==True or type(legende)==str:
#      plt.legend(loc=localisation(legende),fontsize=6)
#    else:
#      (x,y,ncol)=legende
#      plt.legend(loc='center',bbox_to_anchor=(x,y),ncol=ncol,fontsize=6)
#  if legende:
#    plt.legend(loc='upper left',bbox_to_anchor=(1,1),fontsize=6)
  if legende is not None:
    #axe1.legend(h1+h2,l1+l2,loc=localisation(legende),fontsize=6)
    cbar=plt.colorbar(sctr,ax=axe1)
    cbar.set_ticks(range(maxZ-minZ+1)) # add the labels
    cbar.set_ticklabels([format%x for x in range(minZ,maxZ+1)]) # add the labels
    cbar.ax.tick_params(labelsize=8)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)

  # Amplitude et limite du graphique
  if not amplitude:
    ymin=min(ymin); ymax=max(ymax)
    plt.ylim([(ymin-0.5*(ymax-ymin)), (ymax+0.5*(ymax-ymin))])
  elif ylim:
    (ylimmin,ylimmax)=ylim
    plt.ylim([ylimmin,ylimmax])


 # plt.xticks(rotation=rotation)

  # Affichage des ticks
#WAIT   if ticsX!=None and ticsX=='lib':
#WAIT     plt.xticks(valeursX,valeursXs,rotation=rotation)
#WAIT   elif ticsX!=None and ticsX!='off':
#WAIT     if type(ticsX)==tuple:
#WAIT       if len(ticsX)==1:
#WAIT         (tics,)=ticsX
#WAIT         plt.xticks(tics)
#WAIT       else:
#WAIT         (tics,labtics)=ticsX
#WAIT         plt.xticks(tics,labtics)
#WAIT     else:

# INTERET ?  xmin,xmax=plt.xlim()
# INTERET ?  plt.xticks(arange(xmin,xmax,dticsX))
# INTERET ?  if ticsY!=None and ticsY!='off':
# INTERET ?    if type(ticsY)==list:
# INTERET ?      if len(ticsY)==1:
# INTERET ?        (tics,)=ticsY
# INTERET ?        plt.yticks(tics)
# INTERET ?      else:
# INTERET ?        (tics,labtics)=ticsY
# INTERET ?        plt.yticks(tics,labtics)
# INTERET ?    else:
# INTERET ?     ymin,ymax=plt.ylim()
# INTERET ?     plt.yticks(arange(ymin,ymax,ticsY))

  # Legende
#  if legende!=False:
#    if legende==True or type(legende)==str:
#      plt.legend(loc=localisation(legende),fontsize=6)
#    else:
#      (x,y,ncol)=legende
#      plt.legend(loc='center',bbox_to_anchor=(x,y),ncol=ncol,fontsize=6)
#  if legende:
#    plt.legend(loc='upper left',bbox_to_anchor=(1,1),fontsize=6)

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
#    donneeDAT,donneeGraphique,titre,X,Y,Z,filtre=[],grille=True,lignes='-',points=None,largbar=0.8,alpha=0.5,largeur=400,hauteur=300,legende=True,amplitude=True):

def GRAPH_3D(pDonneeGraphique,pTitre,pX,pY,pZ,pParametres):
#    donneeDAT,donneeGraphique,titre,X,Y,Z,filtre=[],grille=True,lignes='-',points=None,largbar=0.8,alpha=0.5,largeur=400,hauteur=300,legende=True,amplitude=True):
  grille=pParametres['grille']
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  valeursX=pX['valeurs']

  libelleY=pY['libelle']
  valeursY=pY['valeurs']

  valeursZ=pZ['valeurs']
  format=pZ['format']

  # Creation de la figure
  (figure,axe)=matplotCreationFigure(pTitre,pParametres,'on',libelleX,ticsZ='on')

  for n in range(len(valeursZ)):
    valeursZ[n]=(int(valeursZ[n]))
  minZ=min(valeursZ)
  maxZ=max(valeursZ)

  # Valeurs et limites
  ymin=None; ymax=None
  (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,None)

  for n in range(len(valeursZ)):
    valeursZ[n]=(int(valeursZ[n]))
  minZ=min(valeursZ)
  maxZ=max(valeursZ)

  # Valeurs et limites
  ymin=None; ymax=None
  (ymin,ymax,valeursY,ylimmin,ylimmax,ylim)=valeursTrace(None,valeursY,None,ylim,ymin,ymax,None)

  numCouleur=1

#  linewidth=0
#  if 'l' in pY['trace']: linewidth=1.0
#  marker=None
#  if 'p' in pY['trace']: marker=points
  # Trace

# Pour faire comme 2DZCoul en 3D
#  cm = LinearSegmentedColormap.from_list('my_list',couleurs[minZ:maxZ+1], N=maxZ-minZ+1)
#  axe.scatter(valeursX,valeursY,c=[x-minZ for x in valeursZ],edgecolors='none',cmap=cm,s=16.)

#  axe.scatter(valeursX,valeursY,valeursZ,c=couleurs[numCouleur],edgecolors='none',s=16.)
  axe.plot_wireframe(np.array([valeursX]),np.array([valeursY]),np.array([valeursZ]),color=couleurs[numCouleur])
#  axe.plot_surface(valeursX,valeursY,valeursZ,color=couleurs[numCouleur])

  #print(plt.rcParams['lines.markersize'] ** 2)

  # Affichage des labels et ticks
  h1,l1=axe.get_legend_handles_labels()
  axe.set_ylabel(libelleY,fontsize=8)
  axe.tick_params(axis="both", labelsize=8)
  for tick in axe.get_xticklabels():
    tick.set_rotation(rotation)

  # Legende
#  if legende!=False:
#    if legende==True or type(legende)==str:
#      plt.legend(loc=localisation(legende),fontsize=6)
#    else:
#      (x,y,ncol)=legende
#      plt.legend(loc='center',bbox_to_anchor=(x,y),ncol=ncol,fontsize=6)
#  if legende:
#    plt.legend(loc='upper left',bbox_to_anchor=(1,1),fontsize=6)
#  if legende is not None:
#    #axe1.legend(h1+h2,l1+l2,loc=localisation(legende),fontsize=6)
#    cbar=plt.colorbar(sctr,ax=axe1)
#    cbar.set_ticks(range(maxZ-minZ+1)) # add the labels
#    cbar.set_ticklabels([format%x for x in range(minZ,maxZ+1)]) # add the labels
#    cbar.ax.tick_params(labelsize=8)

  # Grille
  if grille:
    axe.grid(axis=grille,linewidth=0.5)
  else:
    axe.grid(linewidth=0)

  # Amplitude et limite du graphique
  if not amplitude:
    ymin=min(ymin); ymax=max(ymax)
    plt.ylim([(ymin-0.5*(ymax-ymin)), (ymax+0.5*(ymax-ymin))])
  elif ylim:
    (ylimmin,ylimmax)=ylim
    plt.ylim([ylimmin,ylimmax])


 # plt.xticks(rotation=rotation)

  # Affichage des ticks
#WAIT   if ticsX!=None and ticsX=='lib':
#WAIT     plt.xticks(valeursX,valeursXs,rotation=rotation)
#WAIT   elif ticsX!=None and ticsX!='off':
#WAIT     if type(ticsX)==tuple:
#WAIT       if len(ticsX)==1:
#WAIT         (tics,)=ticsX
#WAIT         plt.xticks(tics)
#WAIT       else:
#WAIT         (tics,labtics)=ticsX
#WAIT         plt.xticks(tics,labtics)
#WAIT     else:

# INTERET ?  xmin,xmax=plt.xlim()
# INTERET ?  plt.xticks(arange(xmin,xmax,dticsX))
# INTERET ?  if ticsY!=None and ticsY!='off':
# INTERET ?    if type(ticsY)==list:
# INTERET ?      if len(ticsY)==1:
# INTERET ?        (tics,)=ticsY
# INTERET ?        plt.yticks(tics)
# INTERET ?      else:
# INTERET ?        (tics,labtics)=ticsY
# INTERET ?        plt.yticks(tics,labtics)
# INTERET ?    else:
# INTERET ?     ymin,ymax=plt.ylim()
# INTERET ?     plt.yticks(arange(ymin,ymax,ticsY))

  # Legende
#  if legende!=False:
#    if legende==True or type(legende)==str:
#      plt.legend(loc=localisation(legende),fontsize=6)
#    else:
#      (x,y,ncol)=legende
#      plt.legend(loc='center',bbox_to_anchor=(x,y),ncol=ncol,fontsize=6)
#  if legende:
#    plt.legend(loc='upper left',bbox_to_anchor=(1,1),fontsize=6)

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)


#OLD 3D  # Trace
#OLD 3D  for numCourbe in range(nbColAccu):
#OLD 3D    zTuple=Z[numCourbe]
#OLD 3D    if len(zTuple)==1: (labelZ,)=zTuple
#OLD 3D    else: (labelZ,_)=zTuple
#OLD 3D    valeursZ=[float(ligne.split(' ')[numCourbe+1]) for ligne in contenu[4:] if ligne!='']
#OLD 3D#    ax.plot(valeursX,valeursY,valeursZ,marker=points,color=COULEURS[numCourbe],linestyle="none")
#OLD 3D#    ax.bar(valeursX,valeursY,valeursZ,marker=points,color=COULEURS[numCourbe],linestyle="none")
#OLD 3D#    ax.plot_wireframe(valeursX,valeursY,valeursZ,color=COULEURS[numCourbe],label='%s'%labelZ)
#OLD 3D    ax.plot_surface(valeursX,valeursY,valeursZ,color=COULEURS[numCourbe],label='%s'%labelZ)
#OLD 3D
#OLD 3D  # Affichage des ticks
#OLD 3D  if ticsX is not None and ticsX!='off':
#OLD 3D    if type(ticsX)==tuple:
#OLD 3D      if len(ticsX)==1:
#OLD 3D        (tics,)=ticsX
#OLD 3D        plt.xticks(tics)
#OLD 3D      else:
#OLD 3D        (tics,labtics)=ticsX
#OLD 3D        plt.xticks(tics,labtics)
#OLD 3D    else:
#OLD 3D     xmin,xmax=plt.xlim()
#OLD 3D     plt.xticks(arange(xmin,xmax,ticsX))
#OLD 3D  if ticsY is not None and ticsY!='off':
#OLD 3D    if type(ticsY)==list:
#OLD 3D      if len(ticsY)==1:
#OLD 3D        (tics,)=ticsY
#OLD 3D        plt.yticks(tics)
#OLD 3D      else:
#OLD 3D        (tics,labtics)=ticsY
#OLD 3D        plt.yticks(tics,labtics)
#OLD 3D    else:
#OLD 3D     ymin,ymax=plt.ylim()
#OLD 3D     plt.yticks(arange(ymin,ymax,ticsY))
#OLD 3D  if ticsZ is not None and ticsZ!='off':
#OLD 3D    if type(ticsZ)==list:
#OLD 3D      if len(ticsZ)==1:
#OLD 3D        (tics,)=ticsZ
#OLD 3D        plt.zticks(tics)
#OLD 3D      else:
#OLD 3D        (tics,labtics)=ticsZ
#OLD 3D        plt.zticks(tics,labtics)
#OLD 3D    else:
#OLD 3D     zmin,zmax=plt.zlim()
#OLD 3D     plt.zticks(arange(zmin,zmax,ticsZ))
#OLD 3D
#OLD 3D  # Legende
#OLD 3D  if legende:
#OLD 3D    plt.legend(loc='upper left',bbox_to_anchor=(1,1),fontsize=6)
#OLD 3D
#OLD 3D  # Sauvegarde
#OLD 3D  fichierIMG="%s.%s"%(pDonneeGraphique,export)
#OLD 3D  try:
#OLD 3D    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
#OLD 3D  except:
#OLD 3D    print("Erreur a la creation du fichier %s"%export)
#OLD 3D  plt.cla()
#OLD 3D  plt.close(figure)
#OLD 3D  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_Camembert(donneeGraphique,titre,X,Y,pParametres):
  largeur=pParametres['largeur']
  hauteur=pParametres['hauteur']
  rotation=pParametres['rotation']
  format=pParametres['format'] #'%1.1f%%'
  shadow=pParametres['shadow']
  explode=pParametres['explode']
  couleurs=pParametres['couleurs']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Creation de la figure
  figure=plt.figure(figsize=(largeur/100.,hauteur/100.))
  plt.title(titre,fontsize=10)

  # Lecture du fichier des donnees a tracer
  numCouleur=len(X)

  plt.rcParams['font.size'] = 8.0
  plt.rcParams['axes.titlesize'] = 10.0
  plt.rcParams['xtick.labelsize'] = 8.0
  plt.rcParams['legend.fontsize'] = 8.0

  plt.pie(Y, explode=explode, labels=X, colors=couleurs[:numCouleur], autopct=format, shadow=shadow, startangle=rotation)

  # Sauvegarde
  fichierIMG="%s.png"%donneeGraphique
  try:
    plt.savefig(fichierIMG,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_Hligne(donneeGraphique,pTitre,pX,pY,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  largeur=pParametres['largeur']
  hauteur=pParametres['hauteur']
  rotation=pParametres['rotation']
  format=pParametres['format'] #'%1.1f%%'
  shadow=pParametres['shadow']
  explode=pParametres['explode']
  lignes=pParametres['lignes']
  points=pParametres['points']
  couleurs=pParametres['couleurs']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  ticsX=None
  if 'tics' in pX:
    ticsX=pX['tics']
  libelleY=pY[0]['libelle']
  ticsY=None
  if 'tics' in pY[0]:
    ticsY=pY[0]['tics']
  valeursX=len(ticsY)

  # Creation de la figure
  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,'on',libelleX,ticsY=ticsY,libelleY=libelleY)
  #axe1=figure.add_subplot(111)
  axe1.margins(MARGIN)

  # Lecture du fichier des donnees a tracer
  numCouleur=0
  for numCourbe in range(len(pY)):
    # Trace
    axe='y1'
#    if 'axe' in courbe: axe=courbe['axe']
#   axe1.plot(valeursX,valeursY,linewidth=linewidth,linestyle=lignes,marker=marker,color=couleurs[numCouleur],label=labelY.decode('utf-8'))
    valeursY=pY[numCourbe]['valeurs']
    labelY=''
    if 'label' in pY[numCourbe]: labelY=pY[numCourbe]['label']
    for idx in range(valeursX):
      flagAnnotations='annotations' in pY[numCourbe]
      if flagAnnotations:
        dAnnotationX,dAnnotationY,annotations=pY[numCourbe]['annotations']

      plt.hlines(y=idx, xmin=valeursY[idx][0], xmax=valeursY[idx][2], colors=couleurs[numCouleur])
      label=None
      if idx==0: label=labelY
      axe1.plot([valeursY[idx][0],valeursY[idx][1],valeursY[idx][2]],[idx,idx,idx],linestyle=lignes,linewidth=20.0,marker=points,color=couleurs[numCouleur],label=label)
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

    # Annotations des valeurs
    if flagAnnotations:
      positionsX=[]
      positionsY=[]
      for idx in range(valeursX):
        positionsX.append(idx)
        positionsY.append(valeursY[idx][1])
      if axe=='y1':
        for x,a,y in zip(positionsX,annotations,positionsY):
          axe1.annotate(a,(y+dAnnotationY,x+dAnnotationX),fontsize=6,rotation=rotation,ha='left',va='bottom')

  # Affichage des labels et ticks
  if ticsX is not None:
    axe1.set_xticks(pX['valeurs'])
    axe1.set_xticklabels(ticsX)
  if ticsY is not None:
    axe1.set_yticks(range(valeursX))
    axe1.set_yticklabels(ticsY)

  # Legende
  h1, l1 = axe1.get_legend_handles_labels()
#  axe1.set_ylabel(ylabels['y1'],fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(45)
  h2=[]
  l2=[]

  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      leg=axe1.legend(h1+h2,l1+l2,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      leg=axe1.legend(h1+h2,l1+l2,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)
    leg_lines = leg.get_lines()
    plt.setp(leg_lines, linewidth=0.5)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)

  # Sauvegarde
  fichierIMG="%s.png"%donneeGraphique
  try:
    plt.savefig(fichierIMG,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_Radar(pDonneeGraphique,pTitre,pX,pY,pParametres):
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  alpha=pParametres['alpha']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  valeursX=pX['valeurs']
  nbValeurs=len(valeursX)

  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,None,'',polar=True)

  # Set ticks to the number of properties (in radians)
  t = np.arange(0,2*np.pi,2*np.pi/nbValeurs)
  plt.xticks(t, [])

  # Set yticks from 0 to 10
  plt.yticks(np.linspace(0,1.,11),fontsize=6)

  # Tracé
  numCouleur=0
  for numCourbe in range(len(pY)):
    labelY=None
    if 'label' in pY[numCourbe]: labelY=pY[numCourbe]['label']

    # Lignes et Points
    ligne='l' in pY[numCourbe]['trace']
    points='p' in pY[numCourbe]['trace']
    surface='s' in pY[numCourbe]['trace']
    trous='t' in pY[numCourbe]['trace']

    valeursY=pY[numCourbe]['valeurs']
    valeursT=None
    if trous:
      valeursY=pY[numCourbe]['valeurs'][0]
      valeursT=pY[numCourbe]['valeurs'][1]

    coord=[(x,y) for x,y in zip(t,valeursY)]
    coord.append(coord[0])
    coord=np.array(coord)
    codes=[path.Path.MOVETO,] + [path.Path.LINETO,]*(len(valeursY) -1) + [ path.Path.CLOSEPOLY ]
    _path = path.Path(coord, codes)
    if surface:
      _patch = patches.PathPatch(_path, fill=True, color=couleurs[numCouleur], linewidth=0, alpha=alpha,label=labelY)
      axe1.add_patch(_patch)
      labelY=None
    if ligne:
      _patch = patches.PathPatch(_path, fill=False, color=couleurs[numCouleur], linewidth = 1,label=labelY)
      axe1.add_patch(_patch)
    if trous:
      coord=[(x,y) for x,y in zip(t,valeursT)]
      coord.append(coord[0])
      coord=np.array(coord)
      codes=[path.Path.MOVETO,] + [path.Path.LINETO,]*(len(valeursT) -1) + [ path.Path.CLOSEPOLY ]
      _path = path.Path(coord, codes)
      if surface:
        _patch = patches.PathPatch(_path, fill=True, color='#ffffff', linewidth=0, alpha=1.0)
        axe1.add_patch(_patch)
      if ligne:
        _patch = patches.PathPatch(_path, fill=False, color=couleurs[numCouleur], linewidth = 1,label=None)
        axe1.add_patch(_patch)
    # Draw circles at value coord
    if points:
      plt.scatter(coord[:,0],coord[:,1], linewidth=1, s=5, color=couleurs[numCouleur], edgecolor=couleurs[numCouleur], zorder=10)
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

  # Set axes limits
#  plt.ylim(0,10)

  # ytick labels
  for i in range(nbValeurs):
      angle_rad = i/float(nbValeurs)*2*np.pi
      ha = "right"
      if angle_rad < np.pi/2 or angle_rad > 3*np.pi/2: ha = "left"
      plt.text(angle_rad, 1.075, valeursX[i], size=8, horizontalalignment=ha, verticalalignment="center")

  # Legende
  h1, l1 = axe1.get_legend_handles_labels()
  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      axe1.legend(h1,l1,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      axe1.legend(h1,l1,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_Hligne(donneeGraphique,pTitre,pX,pY,pParametres):
  grille=pParametres['grille']
  legende=pParametres['legende']
  largeur=pParametres['largeur']
  hauteur=pParametres['hauteur']
  rotation=pParametres['rotation']
  format=pParametres['format'] #'%1.1f%%'
  shadow=pParametres['shadow']
  explode=pParametres['explode']
  lignes=pParametres['lignes']
  points=pParametres['points']
  couleurs=pParametres['couleurs']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  # Recuperation des abscisses des points a tracer
  libelleX=pX['libelle']
  ticsX=None
  if pX.has_key('tics'):
    ticsX=pX['tics']
  libelleY=pY[0]['libelle']
  ticsY=None
  if pY[0].has_key('tics'):
    ticsY=pY[0]['tics']
  valeursX=len(ticsY)

  # Creation de la figure
  (figure,_)=matplotCreationFigure(pTitre,pParametres,'on',libelleX,ticsY=ticsY,libelleY=libelleY)
  axe1=figure.add_subplot(111)
  axe1.margins(MARGIN)

  # Lecture du fichier des donnees a tracer
  numCouleur=0
  for numCourbe in range(len(pY)):
    # Trace
    axe='y1'
#    if courbe.has_key('axe'): axe=courbe['axe']
#   axe1.plot(valeursX,valeursY,linewidth=linewidth,linestyle=lignes,marker=marker,color=couleurs[numCouleur],label=labelY.decode('utf-8'))
    valeursY=pY[numCourbe]['valeurs']
    labelY=''
    if pY[numCourbe].has_key('label'): labelY=pY[numCourbe]['label']
    for idx in range(valeursX):
      flagAnnotations=pY[numCourbe].has_key('annotations')
      if flagAnnotations:
        dAnnotationX,dAnnotationY,annotations=pY[numCourbe]['annotations']

      plt.hlines(y=idx, xmin=valeursY[idx][0], xmax=valeursY[idx][2], colors=couleurs[numCouleur])
      label=None
      if idx==0: label=labelY.decode('utf-8')
      axe1.plot([valeursY[idx][0],valeursY[idx][1],valeursY[idx][2]],[idx,idx,idx],linestyle=lignes,linewidth=20.0,marker=points,color=couleurs[numCouleur],label=label)
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

    # Annotations des valeurs
    if flagAnnotations:
      positionsX=[]
      positionsY=[]
      for idx in range(valeursX):
        positionsX.append(idx)
        positionsY.append(valeursY[idx][1])
      if axe=='y1':
        for x,a,y in zip(positionsX,annotations,positionsY):
          axe1.annotate(a,(y+dAnnotationY,x+dAnnotationX),fontsize=6,rotation=rotation,ha='left',va='bottom')

  # Affichage des labels et ticks
  if ticsX is not None:
    axe1.set_xticks(pX['valeurs'])
    axe1.set_xticklabels(ticsX)
  if ticsY is not None:
    axe1.set_yticks(range(valeursX))
    axe1.set_yticklabels(ticsY)

  # Legende
  h1, l1 = axe1.get_legend_handles_labels()
#  axe1.set_ylabel(ylabels['y1'],fontsize=8)
  axe1.tick_params(axis="both", labelsize=8)
  for tick in axe1.get_xticklabels():
    tick.set_rotation(45)
  h2=[]
  l2=[]

  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      leg=axe1.legend(h1+h2,l1+l2,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      leg=axe1.legend(h1+h2,l1+l2,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)
    leg_lines = leg.get_lines()
    plt.setp(leg_lines, linewidth=0.5)

  # Grille
  if grille:
    axe1.grid(axis=grille,linewidth=0.5)
  else:
    axe1.grid(linewidth=0)

  # Sauvegarde
  fichierIMG="%s.png"%donneeGraphique
  try:
    plt.savefig(fichierIMG,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#-----------------------------------------------------------------------------------
def GRAPH_Radar(pDonneeGraphique,pTitre,pX,pY,pParametres):
  legende=pParametres['legende']
  rotation=pParametres['rotation']
  amplitude=pParametres['amplitude']
  ylim=pParametres['ylim']
  couleurs=pParametres['couleurs']
  alpha=pParametres['alpha']
  dpi=pParametres['dpi']
  lut=pParametres['lut']
  export=pParametres['export']
  convert=pParametres['8b']

  valeursX=pX['valeurs']
  nbValeurs=len(valeursX)

  (figure,axe1)=matplotCreationFigure(pTitre,pParametres,None,'',polar=True)

  # Set ticks to the number of properties (in radians)
  t = np.arange(0,2*np.pi,2*np.pi/nbValeurs)
  plt.xticks(t, [])

  # Set yticks from 0 to 10
  plt.yticks(np.linspace(0,1.,11),fontsize=6)

  # Tracé
  numCouleur=0
  for numCourbe in range(len(pY)):
    labelY=None
    if pY[numCourbe].has_key('label'): labelY=pY[numCourbe]['label'].decode('utf-8')

    # Lignes et Points
    ligne='l' in pY[numCourbe]['trace']
    points='p' in pY[numCourbe]['trace']
    surface='s' in pY[numCourbe]['trace']
    trous='t' in pY[numCourbe]['trace']

    valeursY=pY[numCourbe]['valeurs']
    valeursT=None
    if trous:
      valeursY=pY[numCourbe]['valeurs'][0]
      valeursT=pY[numCourbe]['valeurs'][1]

    coord=[(x,y) for x,y in zip(t,valeursY)]
    coord.append(coord[0])
    coord=np.array(coord)
    codes=[path.Path.MOVETO,] + [path.Path.LINETO,]*(len(valeursY) -1) + [ path.Path.CLOSEPOLY ]
    _path = path.Path(coord, codes)
    if surface:
      _patch = patches.PathPatch(_path, fill=True, color=couleurs[numCouleur], linewidth=0, alpha=alpha,label=labelY)
      axe1.add_patch(_patch)
      labelY=None
    if ligne:
      _patch = patches.PathPatch(_path, fill=False, color=couleurs[numCouleur], linewidth = 1,label=labelY)
      axe1.add_patch(_patch)
    if trous:
      coord=[(x,y) for x,y in zip(t,valeursT)]
      coord.append(coord[0])
      coord=np.array(coord)
      codes=[path.Path.MOVETO,] + [path.Path.LINETO,]*(len(valeursT) -1) + [ path.Path.CLOSEPOLY ]
      _path = path.Path(coord, codes)
      if surface:
        _patch = patches.PathPatch(_path, fill=True, color='#ffffff', linewidth=0, alpha=1.0)
        axe1.add_patch(_patch)
      if ligne:
        _patch = patches.PathPatch(_path, fill=False, color=couleurs[numCouleur], linewidth = 1,label=None)
        axe1.add_patch(_patch)
    # Draw circles at value coord
    if points:
      plt.scatter(coord[:,0],coord[:,1], linewidth=1, s=5, color=couleurs[numCouleur], edgecolor=couleurs[numCouleur], zorder=10)
    numCouleur+=1
    if numCouleur==len(couleurs):
      numCouleur=0

  # Set axes limits
#  plt.ylim(0,10)

  # ytick labels
  for i in range(nbValeurs):
      angle_rad = i/float(nbValeurs)*2*np.pi
      ha = "right"
      if angle_rad < np.pi/2 or angle_rad > 3*np.pi/2: ha = "left"
      plt.text(angle_rad, 1.075, valeursX[i], size=8, horizontalalignment=ha, verticalalignment="center")

  # Legende
  h1, l1 = axe1.get_legend_handles_labels()
  if legende is not None:
    loc,out=localisation(legende)
    if out is None:
      axe1.legend(h1,l1,loc=loc,fontsize=6,facecolor="white",framealpha=1.)
    else:
      axe1.legend(h1,l1,loc=loc,bbox_to_anchor=(1,1),fontsize=6,facecolor="white",framealpha=1.)

  # Sauvegarde
  fichierIMG="%s.%s"%(pDonneeGraphique,export)
  try:
    plt.tight_layout()
    plt.savefig(fichierIMG,dpi=dpi,bbox_inches="tight")
  except:
    print("Erreur a la creation du fichier %s"%export)
  plt.cla()
  plt.close(figure)
  if convert and export=="png": convertLut(fichierIMG,lut)

#===============================================================================
# FIN
