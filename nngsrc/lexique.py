#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import nngsrc.lexiqueFR
import nngsrc.lexiqueEN
import nngsrc.services

#-------------------------------------------------------------------------------
class Lexique:
  langue="FR"
  lexique    = {'FR': nngsrc.lexiqueFR.lexFR,    'EN': nngsrc.lexiqueEN.lexEN  }
  lexiqueIHM = {'FR': nngsrc.lexiqueFR.lexIhmFR, 'EN': nngsrc.lexiqueEN.lexIhmEN  }

  #-------------------------------------------------------------------------------
  def __init__(self,langue):
    #recuperation du langage
    self.langue=langue.upper()

  #-------------------------------------------------------------------------------
  # recuperation du mot dans la langue choisi
  #-------------------------------------------------------------------------------
  def entree(self,mot):
    if type(mot)!=str: return mot
    try:
      ret=self.lexique[self.langue][mot]
    except:
      print("ATTENTION : Entree du lexique '%s' inconnue."%mot)
      ret=mot
    return ret
  #-------------------------------------------------------------------------------
  # recuperation du mot dans la langue choisi
  #-------------------------------------------------------------------------------
  def entreeIHM(self,mot):
    try:
      ret=self.lexiqueIHM[self.langue][mot]
    except:
      print("ATTENTION : Entree du lexique IHM '%s' inconnue."%mot)
      ret="*%s*"%mot
    return ret
  #-------------------------------------------------------------------------------
  def date(self,ladate):
    if self.langue=="EN":
      return ladate.strftime("%Y/%m/%d")
    else:
      return ladate.strftime("%d/%m/%Y")
  #-------------------------------------------------------------------------------
  def ouinon(self,texte):
    if self.langue=="EN":
      if texte=="oui": return "yes"
      else: return "no"
    else:
      return texte
  #-------------------------------------------------------------------------------
  def dateheure(self):
    if self.langue=="EN":
      return "%Y/%m/%d %H:%M"
    else:
      return "%d/%m/%Y %H:%M"
  #-------------------------------------------------------------------------------
  def fr(self):
    return self.lang=="FR"
  #-------------------------------------------------------------------------------
  def methode(self,mot):
    if self.langue=="EN":
      return "%s document"%mot
    elif self.langue=="FR":
      if mot[0] in ['A','E','I','O','U','Y']:
        return "Document d'%s"%mot.lower()
      else:
        return "Document de %s"%mot.lower()
    else:
        return mot
  #-------------------------------------------------------------------------------
  def articleMethode(self,mot):
    if self.langue=="EN":
      if mot[0] in ['A','E','I','O','U','Y']:
        return "an"
      else:
        return "a"
    elif self.langue=="FR":
      if mot[0] in ['A','E','I','O','U','Y']:
        return "d'"
      else:
        return "de "
    else:
        return "***"
  #-------------------------------------------------------------------------------
  def niveauCriticite(self,niveau):
    for niv in range(5):
      mot="MOT_NIVEAU_CRITIQUE_%d"%niv
      if self.entree(mot)==niveau: return niv
    return 4

  #-------------------------------------------------------------------------------
  def FTsurFiche(self,ft):
    if self.langue in ["EN","FR"]:
      if ft=="C":   return nngsrc.services.FT_CRI
      elif ft=="B": return nngsrc.services.FT_BLO
      elif ft=="M": return nngsrc.services.FT_MAJ
      elif ft=="m": return nngsrc.services.FT_MIN
    return nngsrc.services.FT_NQ

  #-------------------------------------------------------------------------------
  def deltaTime(self,lst_resAttendu):
    delta=0
    quant=''
    if lst_resAttendu[1]=="MAINTENANT":
      quant="'%s'"%self.lexique[self.langue]["MOT_MAINTENANT"]
      resComment=self.lexique[self.langue]["LOCUT_%s"%lst_resAttendu[0]]%quant
    else:
      if lst_resAttendu[2]=="AN" or lst_resAttendu[2]=="ANS":
        delta=int(lst_resAttendu[1])*365*24*3600-1
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      elif lst_resAttendu[2]=="MOIS":
        delta=int(lst_resAttendu[1])*30*24*3600-1
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      elif lst_resAttendu[2]=="JOUR" or lst_resAttendu[2]=="JOURS" :
        delta=int(lst_resAttendu[1])*24*3600-1
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      elif lst_resAttendu[2]=="HEURE" or lst_resAttendu[2]=="HEURES" :
        delta=int(lst_resAttendu[1])*3600-1
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      elif lst_resAttendu[2]=="MINUTE" or lst_resAttendu[2]=="MINUTES" :
        delta=int(lst_resAttendu[1])*60-1
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      elif lst_resAttendu[2]=="SECONDE" or lst_resAttendu[2]=="SECONDES" :
        delta=int(lst_resAttendu[1])
        quant=self.lexique[self.langue]["MOT_%s"%lst_resAttendu[2]]
      resComment="%s %s"%(self.lexique[self.langue]["LOCUT_%s"%lst_resAttendu[0]]%lst_resAttendu[1],quant)

    return delta,resComment
#---------------------------------------------------------------------------------
# Fin
