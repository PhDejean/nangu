#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,time,datetime,sys,glob,tarfile
import nngsrc.services
import nngsrc.qualite

#-------------------------------------------------------------------------------

Atester="> "
TResult=""
DebFin ="!"
itemSCEAct="*"
itemSCE="o"
#REPERTRAPPORT="LaTeX"
#DOCEXTENSION=".tex"
#SDL="\n"
#CLEARPAGE="\\clearpage\n\n"

LISTE_EXTENSION=[ ]

#-------------------------------------------------------------------------------
def max_dispchr(res1,res2):
  if res1=='': return res2
  if res1=="OK":  return res2
  return res1

#-------------------------------------------------------------------------------
def sansAccent(texte):
  for s,a in [('o','ô'),('e','é'),('e','è'),('e','ê'),('e','ë'),('u','û')]:
    texte=texte.replace(a,s)
  return texte

#-------------------------------------------------------------------------------
def rapport_bilan_fin():
  ligne="\\end{tabular}\n"
  ligne+="\\normalsize\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan_debut(nbcol):
  ligne="\\newcolumntype{R}[1]{>{\\raggedleft\\hspace{0pt}}p{#1}}\n"
  ligne+="\\small\n"
  if nbcol==2:  ligne+="\\begin{tabular}{p{11cm}R{1cm}}\n"
  else:         ligne+="\\begin{tabular}{p{11cm}R{1cm}R{1.5cm}R{1.5cm}}\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan(nbcol,texte,valeur=None,soit=None,pourcent=None,italique=False):
  if italique:
    ligne="\\textit{-%s}"%texte
  else:
    ligne=texte

  if valeur=='':
    colonne=" \_ "
  else:
    colonne="%d"%valeur
  if italique:
    ligne+="&\\textit{%s}"%colonne
  else:
    ligne+="&%s"%colonne

  if soit:
    if italique:
      ligne+="&\\textit{%s}"%soit
    else:
      ligne+="&%s"%soit
  elif nbcol==4:
    ligne+="&"

  if pourcent is not None:
    if pourcent=='':
      colonne=" \_ \%"#
    else:
      colonne="%4.1f\%%"%(100*pourcent)
    if italique:
      ligne+="&\\textit{%s}"%colonne
    else:
      ligne+="&%s"%colonne
  elif nbcol>=3:
    ligne+="&"

  ligne+="\\tabularnewline\n"
  return ligne

#-------------------------------------------------------------------------------
def rapport_bilan_separatrice(nbcol):
  return "\\hline\n"

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Fonction de définition des titres de chapitre et paragraphes
#-------------------------------------------------------------------------------
def Titre(num,chaine):
  if   num==1: return Titre1(chaine)
  elif num==2: return Titre2(chaine)
  elif num==3: return Titre3(chaine)
  elif num==4: return Titre4(chaine)
def Titre1(chaine,txtLabel=None):
  chaine=chaine.replace("--","$NANGU$")
  chaine=chaine.replace("-"," ")
  chaine=chaine.replace("$NANGU$","-")
  return "========[%s}========"%(chaine)
def Titre2(chaine,txtLabel=None):
  label=''
  if txtLabel is not None: label="\\label{%s}"%txtLabel
  return "===%s===%s"%(chaine,label)
def Titre3(chaine):
  return "--%s--"%chaine
def Titre4(chaine):
  return "* %s"%BF(IT(LG(chaine)))

#-------------------------------------------------------------------------------
# Fonction d'indexation des exigences ou FT
#-------------------------------------------------------------------------------
def indexerExigFT(chaine,formater=False):
  if chaine is None:
    return chaine
  lstch=chaine.split(',')
  idxchaine=''
  for e in lstch:
    if e.find("MAGIDX")==-1:
      e=e.replace(" ",'')
      if formater:
        e=fmtCh(e)
      idxchaine+=", "+e+"\\index{exigences}{"+e+"}"
    else:
      idxchaine+=", "+e
  idxchaine=idxchaine[2:]
  idxchaine=idxchaine.replace("\\index{exigences}{\\n","\\index{exigences}{").replace("\\n}","}")
  return idxchaine

#-------------------------------------------------------------------------------
def largeursTableau(table):
  largeurs=[0]*len(table)
  for numCol in range(len(table)):
    for element in table[numCol]:
      if len(element)>largeurs[numCol]:
        largeurs[numCol]=len(element)
  for numCol in range(len(table)):
    for numElem in range(len(table[numCol])):
      table[numCol][numElem]=fmtCh(table[numCol][numElem])#,formatage=largeurs[numCol]
  return largeurs,table

#-------------------------------------------------------------------------------
# Mise en tableau des documents applicables, de référence et autre...
#-------------------------------------------------------------------------------
def MiseEnTableau(fichiersTables,ID=None,compteur=0):
  texte=''
  for ficTable in fichiersTables:
    if os.access(ficTable,os.F_OK):
      lignes=nngsrc.services.SRV_LireContenuListe(ficTable)
      if ID:
        for e in range(len(lignes)):
          if len(lignes[e])>1:
            texte+="{\\small %s%02d} & "%(ID,compteur)
            ligne=lignes[e].replace('\n','').replace('\r','').split('&')
            try:
              texte+="{\\small %s} & {\\small %s}\\\\\n"%(fmtCh(ligne[0]),fmtCh(ligne[1]))
            except:
              print("ERR:",ficTable,ligne)
            compteur+=1
      else:
        for e in range(len(lignes)):
          if len(lignes[e])>1:
            ligne=lignes[e].replace('\n','').replace('\r','').split('&')
            try:
              texte+="{\\small %s} & {\\small %s}\\\\\n"%(fmtCh(ligne[0]),fmtCh(ligne[1]))
            except:
              print("ERR:",ficTable,ligne)
  return texte,compteur-1

#-------------------------------------------------------------------------------
def ecritureTable(f,table,entete,double=True):
#  (largeurs,table)=largeursTableau(table)
  for numLig in range(len(table[0])):
    if numLig>0: print("\\hline\n")
    print("%s"%table[0][numLig])
    for numCol in range(1,len(table)):
      print("&%s"%table[numCol][numLig])
    print("\\\\\n")

#-------------------------------------------------------------------------------
# Fonction de formatage des chaines pour sortie rapport
#-------------------------------------------------------------------------------
def formateChaineLaTeX(objet,alaligne=0,complet=True,tabbing=False,repTravail=None,formatage=0):
  return fmtCh(objet)#,alaligne,complet,tabbing,repTravail,formatage)
#-------------------------------------------------------------------------------
#  chaine=chaine.replace(unicode('\xee\x80\x81','cp850'),"RIGHT")
#  chaine=chaine.replace(unicode('\xee\x80\xbb','cp850'),"NUM_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\x80',codage),"UP")
#  chaine=chaine.replace(unicode('\xee\x80\x82',codage),"DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x83',codage),"LEFT")
#  chaine=chaine.replace(unicode('\xee\x80\x84',codage),"PAGE_UP")
#  chaine=chaine.replace(unicode('\xee\x80\x85',codage),"PAGE_DOWN")
#  chaine=chaine.replace(unicode('\xee\x80\x86',codage),"DELETE")
#  chaine=chaine.replace(unicode('\xee\x80\x87',codage),"END")
#  chaine=chaine.replace(unicode('\xee\x80\x88',codage),"HOME")
#  chaine=chaine.replace(unicode('\xee\x80\x89',codage),"INSERT")
#
#  chaine=chaine.replace(unicode('\xee\x80\x91',codage),"F1")
#  chaine=chaine.replace(unicode('\xee\x80\x92',codage),"F2")
#  chaine=chaine.replace(unicode('\xee\x80\x93',codage),"F3")
#  chaine=chaine.replace(unicode('\xee\x80\x94',codage),"F4")
#  chaine=chaine.replace(unicode('\xee\x80\x95',codage),"F5")
#  chaine=chaine.replace(unicode('\xee\x80\x96',codage),"F6")
#  chaine=chaine.replace(unicode('\xee\x80\x97',codage),"F7")
#  chaine=chaine.replace(unicode('\xee\x80\x98',codage),"F8")
#  chaine=chaine.replace(unicode('\xee\x80\x99',codage),"F9")
#  chaine=chaine.replace(unicode('\xee\x80\x9a',codage),"F10")
#  chaine=chaine.replace(unicode('\xee\x80\x9b',codage),"F11")
#  chaine=chaine.replace(unicode('\xee\x80\x9c',codage),"F12")
#
#  chaine=chaine.replace(unicode('\xee\x80\xa0',codage),"SHIFT")
#  chaine=chaine.replace(unicode('\xee\x80\xa4',codage),"PRINTSCREEN")
#  chaine=chaine.replace(unicode('\xee\x80\xa5',codage),"SCROLL_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xa6',codage),"PAUSE")
#  chaine=chaine.replace(unicode('\xee\x80\xa7',codage),"CAPS_LOCK")
#
#  chaine=chaine.replace(unicode('\xee\x80\xb0',codage),"NUM0")
#  chaine=chaine.replace(unicode('\xee\x80\xb1',codage),"NUM1")
#  chaine=chaine.replace(unicode('\xee\x80\xb2',codage),"NUM2")
#  chaine=chaine.replace(unicode('\xee\x80\xb3',codage),"NUM3")
#  chaine=chaine.replace(unicode('\xee\x80\xb4',codage),"NUM4")
#  chaine=chaine.replace(unicode('\xee\x80\xb5',codage),"NUM5")
#  chaine=chaine.replace(unicode('\xee\x80\xb6',codage),"NUM6")
#  chaine=chaine.replace(unicode('\xee\x80\xb7',codage),"NUM7")
#  chaine=chaine.replace(unicode('\xee\x80\xb8',codage),"NUM8")
#  chaine=chaine.replace(unicode('\xee\x80\xb9',codage),"NUM9")
#  chaine=chaine.replace(unicode('\xee\x80\xba',codage),"SEPARATOR")
#  chaine=chaine.replace(unicode('\xee\x80\xbb',codage),"NUM_LOCK")
#  chaine=chaine.replace(unicode('\xee\x80\xbc',codage),"ADD")
#  chaine=chaine.replace(unicode('\xee\x80\xbd',codage),"MINUS")
#  chaine=chaine.replace(unicode('\xee\x80\xbe',codage),"MULTIPLY")
#  chaine=chaine.replace(unicode('\xee\x80\xbf',codage),"DIVIDE")
#
#  chaine=chaine.replace(unicode('\x1b',codage),"ESC")
#  chaine=chaine.replace(unicode('\x08',codage),"BACKSPACE")

def fmtCh(objet,tt=False,repTravail=None):#,alaligne=0,complet=True,tabbing=False,formatage=0):,alaligne,complet,tabbing,repTravail,formatage
  if objet is None:
    return objet
  if type(objet)==str:
    chaine=objet
  elif type(objet)==list:
    return [fmtCh(ch) for ch in objet]
  else:
    chaine=str(objet)

  idxDeb=chaine.find("[IMAGE:")
  lstIMA=[]
  idx=-1
  while idxDeb!=-1:
    idxfin=chaine.find("]")
    aidx=chaine[idxDeb+7:idxfin]
    if os.access(aidx,os.F_OK):
      nngsrc.services.SRV_copier(aidx,os.path.join(repTravail,os.path.basename(aidx)),timeout=5,signaler=True)
    idx+=1
    chaine=chaine.replace("[IMAGE:"+aidx+"]","[IMAGE:"+str(idx)+"]")
    lstIMA.append("\\includegraphics[width=15.00cm]{"+aidx+"}")
    idxDeb=chaine.find("[IMAGE:")

  lstIHM=[]
  idIHM=-1
  idxDeb=chaine.find("[IHM:")
  while idxDeb!=-1:
    idxfin=chaine.find(":MHI]")
    aidx=chaine[idxDeb+5:idxfin]
    idxdim=aidx.find("#")
    # recherche de #l ou #h
    echelle="height=10mm"
    if idxdim!=-1:
      fichier=nngsrc.services.SRV_retraitIndexation(aidx[:idxdim])
      if aidx[idxdim+1]=="l":
        echelle="width=%smm"%(aidx[idxdim+2:])
      elif aidx[idxdim+1]=="h":
        echelle="height=%smm"%(aidx[idxdim+2:])
      else:
        echelle="height=10mm"
    else:
      fichier=nngsrc.services.SRV_retraitIndexation(aidx)

    if not os.path.exists(fichier):
      fichier=os.path.join(nngsrc.services.varNANGU,'ressources','FichierInexistant.png')

    destaidx=os.path.join(repTravail,REPERTRAPPORT,os.path.basename(fichier))
    if os.access(fichier,os.F_OK) and not os.access(destaidx,os.F_OK) :
      nngsrc.services.SRV_copier(fichier,destaidx,timeout=5,signaler=True)
    idIHM+=1
    chaine=chaine.replace("[IHM:"+aidx+":MHI]","[MHI:"+str(idIHM)+"-")
    lstIHM.append("\\includegraphics[%s]{%s}"%(echelle,os.path.basename(fichier)))
    idxDeb=chaine.find("[IHM:")

  # indexation MAGIDX:blabla:IDXMAG
  idxdeb=chaine.find("MAGIDX:")
  while idxdeb!=-1:
    idxfin=chaine.find(":IDX:")
    aidx=chaine[idxdeb+7:idxfin]
    labidx=chaine.find(":IDXMAG")
    lidx=chaine[idxfin+5:labidx]
    chaine=chaine.replace("MAGIDX:"+aidx+":IDX:"+lidx+":IDXMAG",aidx)#+"\\index{general}{"+lidx.replace("$\\backslash$",'/')+"}")
    idxdeb=chaine.find("MAGIDX:")

  if idx!=-1:
    for i in range(idx+1):
      chaine=chaine.replace("[IMAGE:"+str(i)+"]",lstIMA[i])

  if idIHM!=-1:
    for i in range(idIHM+1):
      chaine=chaine.replace("[MHI:"+str(i)+"-",lstIHM[i])

  return chaine

#-------------------------------------------------------------------------------
def fmtStyle(sty,valeur):
  if sty=="tt":
    return TT(valeur)
  if sty=="it":
    return IT(valeur)
  if sty=="ittt":
    return IT(TT(valeur))
  return valeur
#-------------------------------------------------------------------------------
def IT(chaine):
  if chaine=='': return chaine
  return chaine

#-------------------------------------------------------------------------------
def BF(chaine):
  if chaine=='': return chaine
  return chaine

#-------------------------------------------------------------------------------
def TT(chaine):
  if chaine=='': return chaine
  return chaine

#-------------------------------------------------------------------------------
def FT(chaine):
  if chaine=='': return chaine
  return chaine

#-------------------------------------------------------------------------------
def LG(chaine):
  if chaine=='': return chaine
  return chaine

#-------------------------------------------------------------------------------
def retraitSautAvant(texte,sousTexte,deb,dBloc):
  if texte[deb-len(dBloc)-3:deb-len(dBloc)]=='\\\\\n':
    deb-=3
    if sousTexte: sousTexte='\n'+sousTexte
  return deb,sousTexte
def retraitSautApres(texte,sousTexte,fin,fBloc):
  if texte[fin+len(fBloc):fin+len(fBloc)+3]=='\\\\\n':
    fin+=3
#    sousTexte=sousTexte+'\n'
  return fin
#-------------------------------------------------------------------------------
def convertionBloc(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  sousTexte=texte[deb:fin]
  if dBloc=='[b]':                                                       # gras
    sousTexte=BF(sousTexte)
  elif dBloc=='[i]':                                                     # italique
    sousTexte=IT(sousTexte)
  elif dBloc=='[u]':                                                     # souligne
    sousTexte='\\underline{%s}'%sousTexte
  elif dBloc=='[font=Courier]':                                          # Courier
    sousTexte='\\texttt{%s}'%sousTexte
  elif dBloc=='[font=Serif]':                                            # Serif
    sousTexte='\\textrm{%s}'%sousTexte
  elif dBloc=='[size=7]':                                                # tiny
    sousTexte='{\\tiny %s}'%sousTexte
  elif dBloc=='[size=8]':                                                # scriptsize
    sousTexte='{\\scriptsize %s}'%sousTexte
  elif dBloc=='[size=9]':                                                # footnotesize
    sousTexte='{\\footnotesize %s}'%sousTexte
  elif dBloc=='[size=10]':                                               # small
    sousTexte='{\\small %s}'%sousTexte
  elif dBloc=='[size=11]':                                               # normalsize
    sousTexte='{\\normalsize %s}'%sousTexte
  elif dBloc=='[size=12]':                                               # large
    sousTexte='{\\large %s}'%sousTexte
  elif dBloc=='[size=13]':                                               # Large
    sousTexte='{\\Large %s}'%sousTexte
  elif dBloc=='[size=14]':                                               # LARGE
    sousTexte='{\\LARGE %s}'%sousTexte
  elif dBloc=='[size=15]':                                               # huge
    sousTexte='{\\huge %s}'%sousTexte
  elif dBloc=='[size=16]':                                               # Huge
    sousTexte='{\\Huge %s}'%sousTexte

  elif dBloc=='[color=red]':                                             # red
    sousTexte='\\textcolor{red}{%s}'%sousTexte
  elif dBloc=='[color=green]':                                           # green
    sousTexte='\\textcolor{green}{%s}'%sousTexte
  elif dBloc=='[color=blue]':                                            # blue
    sousTexte='\\textcolor{blue}{%s}'%sousTexte
  elif dBloc=='[color=cyan]':                                            # cyan
    sousTexte='\\textcolor{cyan}{%s}'%sousTexte
  elif dBloc=='[color=magenta]':                                         # magenta
    sousTexte='\\textcolor{magenta}{%s}'%sousTexte
  elif dBloc=='[color=yellow]':                                          # yellow
    sousTexte='\\textcolor{yellow}{%s}'%sousTexte

  elif dBloc=='[title=1]':                                               # titre1
    sousTexte=Titre1(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=2]':                                               # titre2
    sousTexte=Titre2(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=3]':                                               # titre3
    sousTexte=Titre3(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[title=4]':                                               # titre4
    sousTexte=Titre4(sousTexte)
    deb,sousTexte=retraitSautAvant(texte,sousTexte,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[li]':                                                    # item
    sousTexte='\\item %s\n'%sousTexte
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[ol]':                                                    # enumerate
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{enumerate}\n%s\\end{enumerate}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[ul]':                                                    # itemize
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{itemize}\n\\setlength{\\itemsep}{0pt}\n%s\\end{itemize}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[left]':                                                  # left
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{flushleft}\n%s\\end{flushleft}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[right]':                                                 # right
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{flushright}\n%s\\end{flushright}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[center]':                                                # center
    sousTexte=sousTexte[3:]
    sousTexte='\n\\begin{center}\n%s\\end{center}\n'%sousTexte
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  elif dBloc=='[quote]':                                                 # label
    sousTexte='%s\\index{general}{%s}'%(sousTexte,sousTexte)

  elif dBloc=='[img]':                                                   # includegraphics
    sousTexte='\\includegraphics{%s}'%sousTexte

  elif dBloc=='[table col=':                                             # table
    icol=sousTexte.find(']')
    colonnes=sousTexte[:icol]
    sousTexte=sousTexte[icol+4:]
    sousTexte='\n\\begin{nngtabular}{%s}\n%s\\end{nngtabular}\n'%(colonnes,sousTexte)
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[tr][th]':                                                # table
    sousTexte='{%s}\n'%sousTexte
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)
  elif dBloc=='[tr][td]':                                                # table
    sousTexte='%s'%sousTexte

  elif dBloc=='[anchor=':                                                # lien
    icol=sousTexte.find(']')
    sousTexte=sousTexte[icol+1:]
    deb,_=retraitSautAvant(texte,None,deb,dBloc)
    fin=retraitSautApres(texte,sousTexte,fin,fBloc)

  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+convertionBloc(dBloc,fBloc,texte[fin:])

#-------------------------------------------------------------------------------
def convertionBlocImg(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  taille=''
  if texte[deb]!=']': # on récupère une taille
    taille="[%s]"%texte[deb+1:deb+1+texte[deb+1:].find(']')]
    decalage=len(taille)
  else:
    decalage=1
  fichier=texte[deb+decalage:fin].replace('\\-\\_','_')
  sousTexte='\\includegraphics%s{%s}'%(taille,fichier)
  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+texte[fin:],fichier

#-------------------------------------------------------------------------------
def convertionBlocInc(dBloc,fBloc,texte):
  deb=texte.find(dBloc)
  fin=texte.find(fBloc)
  if deb==-1: return texte
  deb+=len(dBloc)
  sousTexte=texte[deb:fin]
  fichierAutre=sousTexte.replace('\\-\\_','_')
  sousTexte='\\input %s\n'%fichierAutre
  deb-=len(dBloc)
  fin+=len(fBloc)
  return texte[:deb]+sousTexte+texte[fin:],fichierAutre

#-----------------------------------------------------------------------------------
# Classe du rapport
#-----------------------------------------------------------------------------------
class TXT:

  #---------------------------------------------------------------------------------
  # Initialisation de la classe
  #---------------------------------------------------------------------------------
  def __init__(self,repTravail,repRessources,repRapport,ficRapport,projet,qualite,version,bilan,constantes,IDcahier):
    # Parametres communs du rapport
    self.repTravail=repTravail
    self.repRapport=repRapport
    self.projet=projet
    self.qualite=qualite
    self.constantes=constantes
    self.repCampagne=bilan
    self.repRessources=repRessources

    # Parametres specifique du rapport
    self.ficRapport=ficRapport
    self.numAnnexe=0
    self.indexGeneral=0
    self.indexExigences=0

    # Creation des repertoires
#    if not os.path.exists(os.path.join(self.repTravail,REPERTRAPPORT)):
#      os.mkdir(os.path.join(self.repTravail,REPERTRAPPORT))
#    for elem in glob.glob(os.path.join(self.repTravail,REPERTRAPPORT,'*%s'%DOCEXTENSION)):
#      nngsrc.services.SRV_detruire(elem,timeout=5,signaler=True)
#    if not os.path.exists(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT)):
#      os.mkdir(os.path.join(self.repRapport,self.projet.execution,REPERTRAPPORT))

    # Elements des Faits techniques
    if self.projet.REF_FT is None:
      self.refFT="%s-FT"%(self.projet.VALIDATION_REFERENCE+'-'+self.projet.DOCUMENT_EDITION+'-'+self.projet.DOCUMENT_REVISION)
    else:
      self.refFT=self.projet.REF_FT

    # Titre, sous-tire version et nom du projet
    if self.projet.PRODUIT_TITRE != '':
      titre=fmtCh(self.projet.PRODUIT_TITRE)
    else:
      titre=self.fmtTxt("MOT_RAPPORT")
    sstitre=fmtCh(self.projet.PRODUIT_SOUS_TITRE)
    if sstitre=='':
      sstitre=fmtCh(self.projet.PRODUIT_SOUS_REFERENCE[1:])
    if self.projet.PRODUIT_NOM != '':
      nom=fmtCh(self.projet.PRODUIT_NOM)
    else:
      nom=self.fmtTxt("LOCUT_SANS_NOM")
    if self.projet.PRODUIT_SOCIETE=='':
      self.projet.PRODUIT_SOCIETE="N.I."
    self.version=version
    self.listeBBCimageInclusion=[]
    # Type de document
    if self.projet.execution=='P':
      doctype=self.projet.lexique.entree('DOCTYPE_P')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='C':
      doctype=self.projet.lexique.entree('DOCTYPE_C')%self.projet.PRODUIT_TYPE
    elif self.projet.execution=='S':
      doctype=self.projet.lexique.entree('DOCTYPE_S')%self.projet.PRODUIT_TYPE
    else:
      doctype=self.projet.lexique.entree('DOCTYPE_R')%self.projet.PRODUIT_TYPE
 #   doctype=fmtCh(doctype)
    methode="Test"
    if self.projet.DOCUMENT_METHODE is not None: # C'est le cas dans le mode campagne
      methode=self.projet.methode[self.projet.DOCUMENT_METHODE]['terme']
    docMethode=self.projet.lexique.methode(methode)
    if bilan:
      self.Campagne_InitRapport(nom)
    else:
      self.Valid_InitRapport(nom,titre,sstitre,doctype,docMethode,IDcahier)

  #-------------------------------------------------------------------------------
  def TexteConversion(self,texte,dirname='',varoperateur=None):
    if varoperateur:
      for v in varoperateur:
        if v['UTILE']:
          nom='['+v['NOM']+']'
          valeur=v['VALEUR']
          while texte.find(nom)!=-1:
            texte=texte.replace(nom,valeur)
    texte=fmtCh(texte)
    texte=texte.replace('\r','')
    texte=texte.replace('\n\n','\n$SDL$\n')
    texte=texte.replace('\n','\\\\\n')

    texte=convertionBloc('[b]','[/\\-b]',texte)
    texte=convertionBloc('[i]','[/\\-i]',texte)
    texte=convertionBloc('[u]','[/\\-u]',texte)
    texte=convertionBloc('[font=Courier]','[/\\-font]',texte)
    texte=convertionBloc('[font=Serif]','[/\\-font]',texte)

    texte=convertionBloc('[title=1]','[/\\-title]',texte)
    texte=convertionBloc('[title=2]','[/\\-title]',texte)
    texte=convertionBloc('[title=3]','[/\\-title]',texte)
    texte=convertionBloc('[title=4]','[/\\-title]',texte)

    if texte.find('[quote]')>=0:
      self.indexGeneral+=1
      texte=convertionBloc('[quote]','[/\\-quote]',texte)

    while texte.find('[inc]')!=-1:
      texte,fichierAutre=convertionBlocInc('[inc]','[/\\-inc]',texte)
      bbcode=nngsrc.services.SRV_LireContenu(os.path.join(dirname,fichierAutre+".bbc"))
      bbcode=self.TexteConversion(bbcode,dirname)
      fi=open(os.path.join(self.repTravail,REPERTRAPPORT,fichierAutre+DOCEXTENSION),"w",encoding='utf-8')
      fi.write(bbcode)
      fi.close()

    while texte.find('[img')!=-1:
      texte,fichierImg=convertionBlocImg('[img','[/\\-img]',texte)
      self.listeBBCimageInclusion.append(os.path.join(dirname,fichierImg))

    texte=convertionBloc('[color=red]','[/\\-color]',texte)
    texte=convertionBloc('[color=green]','[/\\-color]',texte)
    texte=convertionBloc('[color=blue]','[/\\-color]',texte)
    texte=convertionBloc('[color=cyan]','[/\\-color]',texte)
    texte=convertionBloc('[color=magenta]','[/\\-color]',texte)
    texte=convertionBloc('[color=yellow]','[/\\-color]',texte)

    texte=convertionBloc('[center]','[/\\-center]',texte)
    texte=convertionBloc('[left]','[/\\-left]',texte)
    texte=convertionBloc('[right]','[/\\-right]',texte)

    texte=convertionBloc('[size=7]','[/\\-size]',texte)
    texte=convertionBloc('[size=8]','[/\\-size]',texte)
    texte=convertionBloc('[size=9]','[/\\-size]',texte)
    texte=convertionBloc('[size=10]','[/\\-size]',texte)
    texte=convertionBloc('[size=11]','[/\\-size]',texte) # Par défaut
    texte=convertionBloc('[size=12]','[/\\-size]',texte)
    texte=convertionBloc('[size=13]','[/\\-size]',texte)
    texte=convertionBloc('[size=14]','[/\\-size]',texte)
    texte=convertionBloc('[size=15]','[/\\-size]',texte)
    texte=convertionBloc('[size=16]','[/\\-size]',texte)

    texte=convertionBloc('[ul]','[/\\-ul]',texte)
    texte=convertionBloc('[ol]','[/\\-ol]',texte)
    texte=convertionBloc('[li]','[/\\-li]',texte)

    texte=convertionBloc('[anchor=','[/anchor]',texte)

    texte=convertionBloc('[table col=','[/\\-table]',texte)
    texte=convertionBloc('[tr][th]','[/\-th][/\\-tr]',texte)
    texte=convertionBloc('[tr][td]','[/\-td][/\\-tr]',texte)
    texte=texte.replace('[/\\-td][td]',' & ')
    texte=texte.replace('[/\\-th][th]',' & ')
    texte=texte.replace('[hr/\-]','\\hline')

    texte=texte.replace('[br/\\-]\\\\\n','\\-\\\\\n')
    texte=texte.replace('[br/\\-]','\\\\\n')
    texte=texte.replace('$SDL$\\\\\n','\\-\\\\\n')
    texte=texte.replace('$SDL$\n','\\-\\\\\n')
    if texte[-3:]=='\\\\\n':
      texte=texte[:-3]+'\n'
    return texte

  #-------------------------------------------------------------------------------
  def inclureFichier(self,f,fichier,bbc=False):
    fichierAinclure=os.path.basename(fichier)+DOCEXTENSION
    if bbc:
      bbcode=nngsrc.services.SRV_LireContenu(fichier+".bbc")
  #  bbcode=nngsrc.services.SRV_Encodage(bbcode,"ERREUR -%s- : Recuperation du fichier .txt"%REPERTRAPPORT)
      bbcode=self.TexteConversion(bbcode,os.path.dirname(fichier))
      fi=open(os.path.join(self.repTravail,REPERTRAPPORT,fichierAinclure),"w",encoding='utf-8')
      fi.write(bbcode)
      fi.close()
    print("\\input %s\n"%os.path.basename(fichierAinclure))

  #-------------------------------------------------------------------------------
  # formatage du lexique
  #-------------------------------------------------------------------------------
  def fmtTxt(self,motclef,complement=None):
    texte=self.projet.lexique.entree(motclef).replace('\n','')
    if complement is not None:
      return fmtCh(texte%self.projet.lexique.entree(complement))
    return fmtCh(texte)

  #-------------------------------------------------------------------------------
  def quelItem(self,inRapport):
    if inRapport!=-1:
      return itemSCEAct
    else:
      return itemSCE

  #-------------------------------------------------------------------------------
  # Ecriture du bloc commentaire de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_description_debut(self,texte=None,inRapport=0):
    if texte is not None:
      #print("%-------------------------------------------------\n\\renewcommand{\\arraystretch}{0.8}\n\\begin{tabularx}{16cm}{|Xp{2.5cm}|}\n\\hline\n\\rowcolor{nnggrey} & {\\scriptsize "+texte+"} \\\\\n")
      print(texte)
#    else:
#      print("%-------------------------------------------------\n\\renewcommand{\\arraystretch}{0.8}\n\\begin{tabularx}{16cm}{|X|}\n\\hline\n\\rowcolor{nnggrey} \\\\\n")

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_description_milieu(self,normal='',petit='',item='',exigence='',ft=None,inRapport=None):
    esperluette="&"
    texte=''
    if item!='':
      texte+=item+" - "
    texte+=normal
    if exigence!='' and exigence is not None:
      exigence=','.join(["[%s]"%e for e in exigence.split(',')])
      message="  %s %s"%(self.fmtTxt("LOCUT_EXIGENCES_SPECIFIQUES"),exigence)
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,message,style="||Nr")

    if ft is not None:
      if ft!=-1:
        if self.projet.execution=='S':
          texte+="%s\\\\ \\hspace*{1pc}{\\scriptsize %s '%s'}"%(esperluette,self.fmtTxt("LOCUT_ACTION_JUGEE"),self.fmtTxt("MOT_NIVEAU_CRITIQUE_%d"%ft))
        else:
          texte+="%s{\\scriptsize '%s'}"%(esperluette,self.fmtTxt("MOT_NIVEAU_CRITIQUE_%d"%ft))
      else:
        texte+="%s "%esperluette
    else:
      texte+=" "
    if inRapport is not None:
      texte="%s %s"%(self.quelItem(inRapport),texte)
    if texte!=" " and inRapport!=-1:
      print(texte)

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  def rapport_description_fin(self):
    print(" |   --------------------------------")

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_contenu_deb(self,message):
    if self.projet.execution=='S':
      if message=='':
        return ''
      else:
        return "\\textit{%s}\\\\\n"%message
    return "\\begin{tabularx}{16cm}{|X|}\n\\hline\n%s\\\\\n"%BF(message)

  # 2- Ecriture du contenu du bloc
  def rapport_contenu_mil(self,action,option):
    if self.projet.execution=='S':
      if action+str(option)=='':
        return ''
      else:
        return "%s %s\\\\\n"%(action,str(option))
    if type(option)==list:
      texte=''
      for (un,deux) in option:
        texte+="%s\\\\\n"%(action%(TT(fmtCh(un)),TT(fmtCh(deux))))
      return texte
    return "%s %s\\\\\n"%(action,str(option))

  # 3- Ecriture de la fin du bloc
  def rapport_contenu_fin(self):
    if self.projet.execution=='S':
      return ''
    return "\\hline\n\\end{tabularx}\\\\\n"

  # T- Tout en un
  def rapport_contenu(self,message,action,option,lien=False):
    texte =self.rapport_contenu_deb(message)
    texte+=self.rapport_contenu_mil(action,option)
    texte+=self.rapport_contenu_fin()
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc contenu
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  @staticmethod
  def rapport_contenuMode_deb(clef):
    #texte="\\begin{tabularx}{16cm}{|X|}\n\\hline\n%s\\\\\n"%BF(clef)
    texte="\\begin{longtable}{|p{15.55cm}|}\n\\hline\n%s\\\\\n"%BF(clef)
    return texte

  # 2- Ecriture du contenu du bloc
  @staticmethod
  def rapport_contenuMode_mil(valeur, substit, style):
    if len(substit)>0:
      valeur+=" "
      valeur=valeur.split("%s")
      #texte=fmtCh(valeur[0])
      texte=valeur[0]
      for e in range(len(substit)):
        texte+="%s%s"%(fmtStyle(style,substit[e]),valeur[e+1])
    else:
      texte=fmtStyle(style,valeur)
    return texte

  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_contenuMode_fin():
    #texte="\\hline\n\\end{tabularx}\\\\\n"
    texte="\\hline\n\\end{longtable}\n"
    return texte

  # T- Tout en un
  def rapport_contenuMode(self,clef,valeur,substit,style,lien=False,eol='r'):
    message="- %s : %s"%(clef,self.rapport_contenuMode_mil(valeur,substit,style))
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,message,style="||N%s"%eol)

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action - Cas : Plan de Validation
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_aTester_debut(self,message,retour=None):
    texte=''
    if self.projet.execution=='S':
      texte=''
    elif self.projet.execution=='P':
      # Deux colonnes
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|}\n\\hline\n%s&%s\\\\\n"%(Atester,BF(message))
    else:
      if retour:
        # Quatre colonnes
        texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{3.2cm}|p{1.2cm}|}\n\\hline\n%s&\\textbf{%s} & %s &%s\\\\\n"%(Atester,message,retour,TResult)
      else:
        # Trois colonnes
        texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{1.2cm}|}\n\\hline\n%s&\\textbf{%s} &%s\\\\\n"%(Atester,message,TResult)
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_aTester_milieu(self,action,objet,inRapport,bilan='',suspendu=False,raison='',cssBilan=None,retour=None,valeur=''):
    if suspendu: bilan="SUS"
    if type(action)!=str:
      action=str(action)
    if type(objet)!=str:
      objet=str(objet)
    if type(valeur)!=str:
      valeur=str(valeur)
    if self.projet.execution=='S':
      texte=''
    elif self.projet.execution=='P':
      # Deux colonnes
      texte="&%s%s\\\\\n"%(action,FT(TT(objet)))
    else:
      if retour is None:
        # Trois colonnes
        texte="&%s%s&\\\\\n"%(action,FT(TT(objet)))
      else:
        # Quatre colonnes
        texte="&%s%s&%s&%s\\\\\n"%(action,FT(TT(objet)),BF(TT(valeur)),bilan)
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  def rapport_aTester_fin(self,bilan=None,raison=''):
    if self.projet.execution=='S':
      return ''
    texte=''
    if bilan=='AR': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='areserve'>AR</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  # T- Tout en un
  def rapport_aTester(self,message,action,objet,inRapport,bilan='',suspendu=False,raison=None,cssBilan=None,retour=None,valeur=None):
    texte =self.rapport_aTester_debut(message,retour)
    texte+=self.rapport_aTester_milieu(action,objet,inRapport,bilan,suspendu,raison,cssBilan,retour,valeur)
    texte+=self.rapport_aTester_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  def Valid_InitRapport(self,nom,titre,sstitre,doctype,docMethode,IDcahier):
    if self.projet.debug==2:
      t=datetime.datetime(2016,2,29,12,34,56)
    else:
      t=datetime.datetime.now()

    print("====================================================")
    print("Projet    : %s"%nom)
    #-  Type de document
    print("Type      : %s"%fmtCh(doctype))
    print("Methode   : %s"%fmtCh(docMethode))
    #-    Titre et sous-titre du document
    print("Titre     : %s"%titre)
    print("SsTitre   : %s"%sstitre)
    #-  PC
    print("Poste     : %s"%fmtCh(self.projet.COMPUTERNAME))
    #-  User
    print("Compte    : %s"%fmtCh(self.projet.USERNAME))
    #-  Référence du document
    if self.projet.version<9.0:
      if not self.projet.execution in ['P','S']:
        print("Reference : %s-%s"%(fmtCh(self.projet.PRODUIT_REFERENCE,self.projet.PRODUIT_VERSION+self.projet.PRODUIT_SOUS_REFERENCE)))
    else:
      if not self.projet.execution in ['P','S']:
        print("Reference : %s"%(fmtCh(self.projet.REFERENCE_IDENTIFIANT.replace("{MODE}",self.projet.execution).replace("{VERSION}",self.projet.PRODUIT_VERSION).replace("{SOUSREF}",self.projet.PRODUIT_SOUS_REFERENCE))))

    #-  Edition/Révision du document
    if not self.projet.execution in ['P','S']:
      print("Revision  : %s.%s.%s"%(fmtCh(self.projet.DOCUMENT_EDITION),fmtCh(self.projet.DOCUMENT_REVISION),fmtCh(self.projet.DOCUMENT_ITERATION)))
    #-  Date du document
    print("Date      : %s"%self.projet.lexique.date(t))

    methode="Test"
    if self.projet.version>=9.0 and self.projet.methode!={} and self.projet.DOCUMENT_METHODE is not None:
      print(Titre2(self.fmtTxt('TITRE2_METHODOLOGIE')))
      print(self.fmtTxt('PRESENTATION_METHODOLOGIE'))
      table=[[],[],[]]
      for item in self.projet.methode.keys():
        abbrev=BF(item)
        terme=BF(fmtCh(self.projet.methode[item]['terme']))
        description=fmtCh(self.projet.methode[item]['description'])
        table[0].append(terme)
        table[1].append("(%s)"%abbrev)
        table[2].append(description)
      methode=self.projet.methode[self.projet.DOCUMENT_METHODE]['terme']
    print(fmtCh(self.projet.lexique.entree("LOCUT_METHODE_DOCUMENT")%(self.projet.lexique.articleMethode(methode),methode.lower())))
    print("====================================================")

  #-------------------------------------------------------------------------------
  # Finalisation du rapport
  #-------------------------------------------------------------------------------
  def Valid_TerminerRapportFiche(self,html,result,criticite,EXECUTE_FIN,pathFiche,rapportActions):
    self.Valid_DeroulementTests(pathFiche,result,criticite)

  #-------------------------------------------------------------------------------
  def Valid_RapporterResultat(self,resFiche,html,description,pathFiche,tagFiche,varoperateur,rapport):
#    rapportFTfiche=html.rapportFTfiche
#    rapportFTficheReserves=html.rapportFTficheReserves
#    lstFTreOuvert=html.lstFTfichereOuvert
#    lstFTaClore=html.lstFTficheaClore

    fiche=os.path.basename(pathFiche)
    fiche=fiche.replace('\\','_').replace('/','_')
    (entite,methode,typo,nature,commentaire,exigences)=description
    if entite=='':
      entite=self.projet.lexique.entree('LOCUT_ND')
    exigences=exigences.replace(";",",").replace(",",", ")

    # Type,Entité,Méthode
    texte ="\\noindent\n"
    if self.projet.execution!='S':
      texte+="\\begin{tabularx}{16cm}{|Xr|}\n"
      texte+="\\hline\n"
      texte+="\\rowcolor{nngblue} \\textbf{%s : }%s & \\textbf{%s : } %s\\\\\n"%(self.fmtTxt('MOT_TYPE'),fmtCh(typo),self.fmtTxt('MOT_FONCTIONNALITE'),fmtCh(entite))
      texte+="\\multicolumn{2}{|>{\\columncolor{nngblue}}c|}{\\textbf{%s%s}} \\\\\n"%(fmtCh(fiche),fmtCh(tagFiche))
      texte+="\\rowcolor{nngblue} \\textbf{%s : }%s & \\textbf{%s : }%s  \\\\\n"%(self.fmtTxt('MOT_METHODE'),fmtCh(methode),self.fmtTxt('MOT_NATURE'),fmtCh(nature))
      texte+="\\hline\n"
      texte+="\\end{tabularx}\\\\\n"
      texte+="\\begin{tabularx}{16cm}{|X|}\n"
      texte+="\\hline\n"
      # ajout de la description
      texte+="\\textbf{%s : }%s\\\\\n"%(self.fmtTxt('MOT_OBJECTIF'),self.TexteConversion(commentaire,os.path.join(self.projet.FICHES,os.path.dirname(pathFiche)),varoperateur))
      # ajout des exigences
      if exigences!='':
        texte+="\\hline\n"
        exigences=indexerExigFT(exigences,True)
        exigences=','.join(["[%s]"%e for e in exigences.split(',')])
        texte+="\\textbf{%s : }{\\scriptsize %s}\\\\\n"%(self.fmtTxt('LOCUT_COUVERTURE_EXIGENCES'),exigences)
        self.indexExigences+=1
      texte+="\\hline\n"
      texte+="\\end{tabularx}\\\\\n"
    else:
      texte+=Titre4(self.fmtTxt('MOT_DESCRIPTION'))
      texte+="{\\renewcommand{\\arraystretch}{1}\n"
      texte+="\\begin{tabular}{ll}\n"
      texte+="%s&%s\\\\\n"%(BF(self.fmtTxt('MOT_ENTITE')),fmtCh(entite))
      texte+="%s&%s\\\\\n"%(BF(self.fmtTxt('MOT_TYPE')),fmtCh(typo))
      texte+="%s&%s\\\\\n"%(BF(self.fmtTxt('MOT_NATURE')),fmtCh(nature))
      texte+="%s&%s\\\\\n"%(BF(self.fmtTxt('MOT_METHODE')),fmtCh(methode))
      texte +="\\end{tabular}}\\\\\n"
      # ajout de la description
      texte+="\\vspace{1pc}\\\\\n\\textbf{"+self.fmtTxt('MOT_OBJECTIF')+" : }%s \\\\\n"%self.TexteConversion(commentaire,os.path.join(self.projet.FICHES,os.path.dirname(pathFiche)),varoperateur)
      # ajout des exigences
      if exigences!='':
        exigences=indexerExigFT(exigences,True)
        exigences=','.join(["[%s]"%e for e in exigences.split(',')])
        texte+="\\textbf{%s : }{\\scriptsize %s}\\\\\n\n"%(self.fmtTxt('LOCUT_COUVERTURE_EXIGENCES'),exigences)
        self.indexExigences+=1
      # ajout des actions
      texte+=Titre4(self.fmtTxt('MOT_ACTIONS'))
#      texte+=rapport

    if not self.projet.execution in ['P','S']:
      texte +="%=====================================================\n"
      texte +="\\begin{small}\n"
      texte +="\\begin{tabularx}{16cm}{|X|}\n"
      texte +="\\hline\n"
      texte +="\\rowcolor{nngblue} \\textbf{"+self.fmtTxt('MOT_ACTIONS')+"}\\\\\n"
      texte +="\\hline\n"
      texte +="\\end{tabularx}\n"
      texte +="\\end{small}\\\\\n"
#    elif self.projet.execution=='S':
#
#      texte +="\\vspace{1pc}\\\\\n"
#      texte +=Titre4(self.fmtTxt('MOT_ACTIONS'))
##      texte +="\\begin{small}\n"
##      texte +="\\begin{itemize}\n"
    elif self.projet.execution=='P':
      texte +="%=====================================================\n"
      texte +="\\begin{small}\n"
      texte +="\\begin{tabularx}{16cm}{|Xr|}\n"
      texte +="\\hline\n"
      texte +="\\rowcolor{nngblue} \\textbf{"+self.fmtTxt('MOT_ACTIONS')+"} \\\\\n"
      texte +="\\hline\n"
      texte +="\\end{tabularx}\n"
      texte +="\\end{small}\\\\\n"

    texte+=rapport
    f=open(self.ficRapportFiche,"a",encoding='utf-8')
    # ajout du rapport de la fiche
#    disp=nngsrc.services.SRV_codeResultat(resFiche,self.constantes)
    # ajout du bilan
#    try:
    print(texte)
#    except :
#      print("------------------------------------------------------")
#      print(texte.encode(sys.stdout.encoding))
#      nngsrc.services.SRV_Terminal(self.projet.uiTerm,self.projet.debug,self.projet.log,"ERREUR LaTeX : Ecriture du rapport de la fiche %s avec encodage impossible"%fiche,style="Er")
#      print("------------------------------------------------------")
    if not self.projet.execution in ['P','S']:
      # ecriture du tableau de controle en fin de test
      print('%=====================================================\n')
      print("\\begin{tabularx}{16cm}{|X|}\n\\hline\n")
      print("\\rowcolor{nngblue}\\textbf{"+self.fmtTxt('MOT_CONTROLE')+"} \\\\\n")
      print("\\end{tabularx}\\\\\n")
      print("\\begin{tabularx}{16cm}{|X|X|X|X|X|X|}\n\\hline\n")
      if self.projet.debug==2:
        t=datetime.datetime(2016,2,29,12,34,56)
      else:
        t=datetime.datetime.now()
      case_OK="$\\Box$~"
      date_OK=''
      case_AV="$\\Box$~"
      date_AV=''
      case_AR="$\\Box$~"
      date_AR=''
      case_PB="$\\Box$~"
      date_PB=''
      case_NT="$\\Box$~"
      date_NT=''
      if self.projet.execution=='R':
        if resFiche==self.constantes['STATUS_OK']:
          case_OK="$\\boxtimes$~"
          date_OK=t.strftime("%d/%m/%Y")
        if resFiche==self.constantes['STATUS_AR']:
          case_AR="$\\boxtimes$~"
          date_AR=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_AV']:
          case_AV="$\\boxtimes$~"
          date_AV=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_PB']:
          case_PB="$\\boxtimes$~"
          date_PB=t.strftime("%d/%m/%Y")
        elif resFiche==self.constantes['STATUS_NT']:
          case_NT="$\\boxtimes$~"
          date_NT=t.strftime("%d/%m/%Y")
      print("%s & %s%s & %s%s & %s%s & %s%s & %s%s\\\\\n"%(self.fmtTxt('MOT_BILAN'),case_OK,self.fmtTxt('LOCUT_OK').replace(' ','~'),case_AV,self.fmtTxt('LOCUT_A_VALIDER').replace(' ','~'),case_AR,self.fmtTxt('LOCUT_AVEC_RESERVE').replace(' ','~'),case_PB,self.fmtTxt('LOCUT_NOK').replace(' ','~'),case_NT,self.fmtTxt('LOCUT_NON_TESTE').replace(' ','~')))
      print("%s & %s & %s & %s & %s & %s\\\\\n\\hline\n"%(self.fmtTxt('MOT_DATE'),date_OK,date_AV,date_AR,date_PB,date_NT))
      print("\\end{tabularx}\n\\\\")
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_DeroulementTests(self,pathFiche,result,criticite):
    # ecriture de la grille de deroulement des tests
    grillePath=os.path.join(self.repTravail,"bilanControles.txt")
    reference,rang,fiche=pathFiche
    pathFiche="%s-%s-%s"%(reference,rang,fiche)
    f=open(grillePath,"a",encoding='utf-8')
    if self.projet.nivRapport==1:  # Niveau scenario
      pathFiche=pathFiche.replace('/',':').replace('\\',':').split('-')
      pathFiche='-'.join(pathFiche[2:])
    disp=nngsrc.services.SRV_codeResultat(result,self.constantes,self.projet.DOCUMENT_LANGUE)
    mot_criticite=self.fmtTxt("MOT_NIVEAU_CRITIQUE_%s"%criticite)
    f.write("%s#%s#(%s)#%s\n"%(fmtCh(pathFiche),fmtCh(disp[3]),mot_criticite,fmtCh(reference)))
    f.close()

  #-------------------------------------------------------------------------------
  def Valid_Terminer(self):
    grillePath=os.path.join(self.repTravail,"bilanControles.txt")
    f=open(grillePath,"r")
    contenu=f.readlines()
    f.close()
    lmax=[len("Fiche"),len("Bilan"),len("Criticité")]
    for n in range(len(contenu)):
      contenu[n]=contenu[n].replace('\n','').split('#')
      for e in range(3):
        if lmax[e]<len(contenu[n][e]): lmax[e]=len(contenu[n][e])
    bilanScenarios={}

    longeur=sum(lmax)+11
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'-'*(sum(lmax)+8),style="++*Rr",longueur=longeur)
    texte=" Fiche%s | Bilan%s | Criticité%s "%(' '*(lmax[0]-len("Fiche")),' '*(lmax[1]-len("Bilan")),' '*(lmax[2]-len("Criticité")))
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*Rr",longueur=longeur)
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'-'*(sum(lmax)+8),style="||*Rr",longueur=longeur)
    for n in range(len(contenu)):
      texte=""
      for e in range(3):
        texte+="| %s%s "%(contenu[n][e],' '*(lmax[e]-len(contenu[n][e])))
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte[1:],style="||*Rr",longueur=longeur)

      if not contenu[n][3] in bilanScenarios:
        bilanScenarios[contenu[n][3]]=contenu[n][1]
      else:
        courant=bilanScenarios[contenu[n][3]]
        if courant=="OK" and contenu[n][1]!="OK":
          bilanScenarios[contenu[n][3]]=contenu[n][1]
        elif courant=="A valider" and contenu[n][1]=="Non OK":
          bilanScenarios[contenu[n][3]]=contenu[n][1]

    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'-'*(sum(lmax)+8),style="++*Rr",longueur=longeur)

    # Rapport Scenarios
    texte=" Fiche%s | Bilan%s | Criticité%s "%(' '*(lmax[0]-len("Fiche")),' '*(lmax[1]-len("Bilan")),' '*(lmax[2]-len("Criticité")))
    texte=texte.replace('Fiche   ','Scenario')
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*Rr",longueur=longeur)
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'-'*(sum(lmax)+8),style="||*Rr",longueur=longeur)

    for scenario in bilanScenarios.keys():
      texte=""
      texte+="| %s%s "%(scenario,' '*(lmax[0]-len(scenario)))
      texte+="| %s%s "%(bilanScenarios[scenario],' '*(lmax[1]-len(bilanScenarios[scenario])))
      texte+="| %s "%('-'*(lmax[2]))
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte[1:],style="||*Rr",longueur=longeur)

    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,'-'*(sum(lmax)+8),style="++*Rr",longueur=longeur)

    nngsrc.services.SRV_detruire(grillePath,timeout=5,signaler=True)

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  def rapport_2colonnes_debut(self,clfAction,bilan):
    if self.projet.execution=='P':
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|}\n"
      texte+="\\hline\n%s&%s\\\\\n"%(Atester,BF(fmtCh(clfAction)))
    else:
      texte="\\begin{tabularx}{16cm}{|p{0.3cm}X|p{1.5cm}|}\n"
      texte+="\\hline\n%s&\\textbf{%s} &%s\\\\\n"%(Atester,fmtCh(clfAction),bilan) #TResult)
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  def rapport_2colonnes_milieu(self,inRapport,valeur,style,bilan='',suspendu=False,raison=None,cssBilan=''):
  #  if option!='': option+="O"
    if suspendu: bilan="SUS"
    if type(valeur)==str:
      if valeur=="'*n*'": valeur=''
      if self.projet.execution=='P':
        texte="&%s\\\\\n"%(fmtStyle(style,fmtCh(valeur)))
      else:
        texte="&%s&%s\\\\\n"%(fmtStyle(style,fmtCh(valeur)),bilan)
    else:
      texte=''
      for e in valeur:
        if type(e) is str:
          if e=="'*n*'": e=''
          if self.projet.execution=='P':
            texte+="&%s\\\\\n"%(fmtStyle(style,fmtCh(e)))
          else:
            texte+="&%s&%s\\\\\n"%(fmtStyle(style,fmtCh(e)),bilan)
        else:
          com,doc,titre=e
          val=self.fmtTxt(com)
          sty='tt'
          if com=='LOCUT_SUIVRE_LIEN' or com=='LOCUT_CLIQUER_DEPLIER':
            self.numAnnexe+=1
            document=doc.split("|")
            sizeh=None
            sizel=None
            for s in range(len(document)-1):
              if document[s+1][0]=='h': sizeh=document[s+1][1:]
              if document[s+1][0]=='l': sizel=document[s+1][1:]
            #self.Valid_DonneesAnnexe(self.projet.execution,document[0],titre,sizeh,sizel)
            com=''
            val="%s (%s)"%(self.fmtTxt('LOCUT_VOIR_DONNEE_ANNEXE'),self.fmtTxt('LOCUT_ANNEXE') + str(self.numAnnexe))
            sty='it'
          else:
            com=self.fmtTxt(com)
            val=fmtCh(doc)#,2
          if self.projet.execution=='P':
            texte+="&%s %s\\\\\n"%(com,fmtStyle(sty,val))
          else:
            texte+="&%s %s&%s\\\\\n"%(com,fmtStyle(sty,val),bilan)

        bilan=''
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_2colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  def rapport_2colonnes(self,inRapport,clfAction,valAction,styAction,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,valeur=''):
    texte =self.rapport_2colonnes_debut(clfAction,TResult+BF(self.fmtTxt('ABR_BILAN')))
    texte+=self.rapport_2colonnes_milieu(inRapport,valAction,styAction,bilan,suspendu,raison,cssBilan)
    texte+=self.rapport_2colonnes_fin(bilan,raison)
    return texte

  #-------------------------------------------------------------------------------
  # Ecriture du bloc de l'action
  #-------------------------------------------------------------------------------
  # 1- Ecriture du début du bloc
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  def rapport_3colonnes_debut(self,clfAction,clfConstat,bilan,calibration):
    texte=[]
    tester=Atester
    calibration=calibration[1]
    while len(clfAction)>calibration:
      texte.append([tester,BF(clfAction[:calibration]),BF(clfConstat),bilan])
      tester=""
      clfAction=clfAction[calibration:]
      clfConstat=""
      bilan=""
    texte.append([tester,BF(clfAction),BF(clfConstat),bilan])
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  def rapport_3colonnes_action(self,valAction,styAction):
    texte=[""]
    #for e in range(len(valAction)):
    texte.append(fmtCh(' '.join(valAction)))
    texte.append("")
    texte.append("")
    return texte

  #-------------------------------------------------------------------------------
  # 2- Ecriture du contenu du bloc
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  def rapport_3colonnes_milieu(self,inRapport,clfAttendu,valAttendu,valConstat,styConstat,bilan='',suspendu=False,raison=None,cssBilan=''):
    if suspendu: bilan="SUS"
    texte=[""]
    texte.append("%s '%s'"%(clfAttendu,TT(valAttendu)))
    if self.projet.execution!='P':
      if type(valConstat)==str:
        if valConstat=="'*n*'": valConstat=''
        texte.append(fmtStyle(styConstat,valConstat))
        texte.append(bilan)
    return texte

  #-------------------------------------------------------------------------------
  # 3- Ecriture de la fin du bloc
  @staticmethod
  def rapport_3colonnes_fin(bilan='', raison=''):
    texte=''
    if bilan=='AV': #TODO
      if raison!='':
        texte+="    </table>\n"
        texte+="    <table class='rt'>\n"
        texte+="        <tr><td class='rttest'><font class='avalider'>AV</font></td><td>%s</td></tr>\n"%raison
    texte+="\\hline\n\\end{tabularx}\\\\\n"
    return texte

  #-------------------------------------------------------------------------------
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   deb|> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #   mil|  VAL_ACTION              |             |           |        |  VAL_ACTION                                        |
  #   mil|  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #                                                                <-- fin... plus tard
  def rapport_3colonnes(self,inRapport,clfAction,valact,styact,clfConstat,valConstat,styConstat,clfAttendu,valAttendu,bilan='',suspendu=False,raison=None,cssBilan='',retour=None,vvaleur=''):
    calibration=[2,74,30,6]
    table=self.rapport_3colonnes_debut(clfAction,clfConstat,TResult+BF(self.fmtTxt('ABR_BILAN')),calibration)
    if len(valact)>0:
      table.append(self.rapport_3colonnes_action(valact,styact))
    vBilan=bilan
    for num in range(len(clfAttendu)):
      table.append(self.rapport_3colonnes_milieu(inRapport,clfAttendu[num],valAttendu[num],valConstat[num],styConstat,vBilan,suspendu,raison,cssBilan))
      if num==0:
        cssBilan=''
        vBilan=''
    #table.append(self.rapport_3colonnes_fin(bilan,raison))
    texte="+%s+"%('-'*115)
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*Rr")
    for ligne in table:
      texte="|"
      for numcol in range(len(ligne)):
        texteligne=ligne[numcol]
        while len(texteligne)<calibration[numcol]:
          texteligne+=" "
        texte+=texteligne+"|"
      nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*Rr")
    texte="+%s+"%('-'*115)
    nngsrc.services.SRV_afficher_texte(self.projet.uiTerm,self.projet.debug,texte,style="||*R")

  #-------------------------------------------------------------------------------
  def Valid_BilanMode(self,fiche,params,etatAction,FTconfirme,FTreserve,numFT,etatFT,execution):
    CTL_MODE=params['CTL_MODE']
    CTL_COMMENT=params['CTL_COMMENT'].split('\n')
    CTL_BOUCLE=params['CTL_BOUCLE']
    CTL_FT=params['CTL_FT']
    CTL_EXIGENCES=params['CTL_EXIGENCES']
    CTL_INDEX=self.TexteConversion(str(params['CTL_INDEX']),'')
    CTL_ID=params['CTL_ID']
    CTL_CONDITION=params['CTL_CONDITION']
    CTL_FTQUALIF=params['CTL_FTQUALIF']
    if CTL_FTQUALIF==0:
      CTL_FTQUALIF=None
    inRapport=params['CTL_RAPPORT']
    if inRapport==-1: return

    #----------------------------------
    # Bandeau de description de l'action
    #
    #--1- Item et Description ---
    sautBandeau=True
    ftech=CTL_FTQUALIF

    print()
    if ftech:
      self.rapport_description_debut("%s"%(self.fmtTxt('LOCUT_ACTION_JUGEE')))
    #
    #--2- Ajout des exigences éventuelles ---
    if CTL_EXIGENCES!='':
      # ok
      self.rapport_description_milieu(exigence=CTL_EXIGENCES,ft=ftech)
      sautBandeau=False
    #
    #--3- Ajout des faits techniques éventuels ---
    if CTL_FT!='' and CTL_FT is not None :
      self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_FAITS_TECHS')+indexerExigFT(CTL_FT)),ft=ftech)
      sautBandeau=False
    #
    #--4- Identifiant de condition ---
    if CTL_ID!='':
      self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_IDENTIFIANT')+CTL_ID),ft=ftech)#,2,False))
      sautBandeau=False
    #
    if CTL_CONDITION is not None:
      CTL_CONDITION=CTL_CONDITION.replace('[','<').replace(']','>')
      self.rapport_description_milieu(petit=fmtCh(self.projet.lexique.entree('LOCUT_CONDITION_EXEC')+CTL_CONDITION),ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--5- Identifiant de boucle ---
    if CTL_BOUCLE is not None:
      self.rapport_description_milieu(petit=self.fmtTxt('LOCUT_BOUCLE_ACTION')+CTL_BOUCLE[0],ft=ftech)#,2,False))
      sautBandeau=False
    #
    #--6- Fin du bandeau ---
    if sautBandeau:
      self.rapport_description_milieu(ft=ftech)

    #self.rapport_description_fin()
    #----------------------------------

  # Rapport Bilan de l'action
  # Mode 1,2              CAHIER/RAPPORT                                                      PLAN
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |----------------------------------------+-----------|        |----------------------------------------------------|
  #   2  |> CLF_ACTION                            | CLF_BILAN |     2  |> CLF_ACTION                                        |
  #      |  VAL_ACTION                            | VAL_BILAN |        |  VAL_ACTION                                        |
  #      +----------------------------------------------------+        +----------------------------------------------------+
  # Mode 1,3,1
  #      +----------------------------------------------------+        +----------------------------------------------------+
  #   1  | CLF_COMMENT                                        |     1  | CLF_COMMENT                                        |
  #      | VAL_COMMENT                                        |        | VAL_COMMENT                                        |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   3  |> CLF_ACTION              | CLF_CONSTAT | CLF_BILAN |     3  |> CLF_ACTION                                        |
  #      |  CLF_ATTENDU  VAL_ATTENDU| VAL_CONSTAT | VAL_BILAN |        |  CLF_ATTENDU  VAL_ATTENDU                          |
  #      |--------------------------+-------------+-----------|        |----------------------------------------------------|
  #   1  | CLF_RAPPORT                                        |     1  | CLF_RAPPORT                                        |
  #      | VAL_RAPPORT                                        |        |                                                    |
  #      +----------------------------------------------------+        +----------------------------------------------------+
    if self.projet.execution!='S' and inRapport==0:
      # Mode niveau 1 à 1
      clef=self.fmtTxt(params['CLF_COMMENT'])
      valeur=fmtCh(params['VAL_COMMENT'].replace('%s','£s'),tt=True).replace('£s','%s')
      substit=[fmtCh(x) for x in params['SUB_COMMENT']]
      style=params['STY_COMMENT']
      eol='r'
      if CTL_MODE[0]=='0': eol=''
      self.rapport_contenuMode(clef,valeur,substit,style,eol=eol)
      # Mode niveau 2 à 1
      if len(CTL_MODE)>1 and CTL_MODE[1]=='1':
        clef=self.fmtTxt(params['CLF_COMPLEMENT'])
        valeur=fmtCh(params['VAL_COMPLEMENT'].replace('%s','£s')).replace('£s','%s')
        substit=[fmtCh(x) for x in params['SUB_COMPLEMENT']]
        style=params['STY_COMPLEMENT']
        liste=params['LST_COMPLEMENT']
        if len(liste)>0:
          self.rapport_contenuMode_deb(clef)
          self.rapport_contenuMode_mil(valeur,substit,style)
          for c,v in liste:
            if type(v)==list:
              self.rapport_contenu_mil(fmtCh(self.projet.lexique.entree(c).replace('%s','£s')).replace('£s','%s'),v)
            else:
              self.rapport_contenu_mil(self.fmtTxt(c),fmtCh(v))
          self.rapport_contenuMode_fin()
      # Mode niveau 3 à 2
      if len(CTL_MODE)>2 and CTL_MODE[2]=='2':
        clf=params['CLF_ACTION']
        val=params['VAL_ACTION']
        sty=params['STY_ACTION']
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          #(etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          self.rapport_2colonnes(inRapport,clf,val,sty,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          self.rapport_2colonnes(inRapport,clf,val,sty)
        elif execution=='P':
          self.rapport_2colonnes(inRapport,clf,val,sty)
      # Mode niveau 3 à 3
      if len(CTL_MODE)>2 and CTL_MODE[2]=='3':
        clfact=fmtCh(params['CLF_ACTION'])
        valact=params['VAL_ACTION']
        styact=params['STY_ACTION']
        clfcst=self.fmtTxt(params['CLF_CONSTAT'])
        valcst=fmtCh(params['VAL_CONSTAT'])#,2
        stycst=params['STY_CONSTAT']
        clfatt=[self.fmtTxt(x) for x in params['CLF_ATTENDU']]
        valatt=fmtCh(params['VAL_ATTENDU'])#,2
        disp=nngsrc.services.SRV_codeResultat(params['VAL_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
        #(etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
        self.rapport_3colonnes(inRapport,clfact,valact,styact,clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
      # Mode niveau 4 à 1
      if len(CTL_MODE)>3 and CTL_MODE[3]=='1':
        clef=self.fmtTxt(params['CLF_RAPPORT'])
        valeur=params['VAL_RAPPORT'].replace('\n','%ficn%')
        valeur=fmtCh(valeur,tt=True)
        style=params['STY_RAPPORT']
        print()
        self.rapport_contenuMode(clef,valeur,[],style,eol="")
      # Mode niveau 4 à 3
      if len(CTL_MODE)>4 and CTL_MODE[4]=='3':
        clfact=fmtCh(params['CLF2_ACTION'])
        clfcst=self.fmtTxt(params['CLF2_CONSTAT'])
        valcst=[fmtCh(x) for x in params['VAL2_CONSTAT']]#,2)
        stycst=params['STY2_CONSTAT']
        clfatt=[fmtCh(x) for x in params['CLF2_ATTENDU']]
        valatt=[fmtCh(x) for x in params['VAL2_ATTENDU']]#,2)
        if execution=='R':
          disp=nngsrc.services.SRV_codeResultat(params['VAL2_BILAN'],self.constantes,self.projet.DOCUMENT_LANGUE)
          #(etatAction,disp)=self.GestionConfirmationFT(etatAction,disp,FTconfirme,FTreserve,etatFT)
          self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt,bilan=disp[2],suspendu=etatFT=='MOT_SUSPENDU',raison=fmtCh(params['CTL_VALRAISON']),cssBilan=disp[0])
        elif execution=='C':
          self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)
        elif execution=='P':
          self.rapport_3colonnes(inRapport,clfact,[],[],clfcst,valcst,stycst,clfatt,valatt)

    #----------------------------------
    # Remplacement de l'ancre
    # sans objet

    #----------------------------------
    # Fin et export

  #-------------------------------------------------------------------------------
  def EtapeDeTest(self,pEtape,pNum):
    pEtape=self.TexteConversion(pEtape,"").split('|')
    niveau=pEtape[0]
    etape=pEtape[1]
    if etape[-1]!='.':etape+='.'
    libelle=etape
#    libelle=str(pNum)
#    if len(pEtape)==3:
#      libelle=pEtape[2][:-1]

    if self.projet.etapesSeules:
      resDocument="\\includegraphics[scale=0.4]{etape%s.png} %s\\\\\n"%(niveau,libelle)
    else:
      resDocument="\\begin{tabularx}{16cm}{|X|}\n"
      resDocument+="\\cellcolor{nngecru} \\includegraphics[scale=0.4]{etape%s.png} %s\\\\\n"%(niveau,libelle)
      resDocument+="\\end{tabularx}\\\n\n"
    return resDocument

#-------------------------------------------------------------------------------
# FIN
