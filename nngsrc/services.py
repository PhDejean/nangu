#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,os.path,sys,glob,shutil,locale,time,zipfile,filecmp,subprocess,datetime

try:
  from colorama import *
  init(autoreset=True)
  COLORAMA=True
except:
  print("RESTRICTION (services) : Module 'Colorama' non installé")
  print("              => Les affichages ne seront pas mis en couleur.")
  COLORAMA=False

if COLORAMA:
  FRED    = Fore.RED
  FGREEN  = Fore.GREEN
  FYELLOW = Fore.YELLOW
  FWHITE  = Fore.WHITE
  FCYAN   = Fore.CYAN
  BBLACK  = Back.BLACK
  SBRIGHT = Style.BRIGHT
  SNORMAL = Style.NORMAL
else:
  FRED    = ''
  FGREEN  = ''
  FYELLOW = ''
  FWHITE  = ''
  FCYAN   = ''
  BBLACK  = ''
  SBRIGHT = ''
  SNORMAL = ''


# Mis en commanetaire pour sortir SRVImage()
#try:
#  from PyQt5 import QtGui, QtCore
#except:
#  pass

LONGUEUR=120

CODE_OK     = 0 # Tout s’est bien passé !
CODE_LIGCOM = 1 # Il s’agit d’anomalies de ligne de commande : les paramètres sont insuffisants, la syntaxe est rappelée.
CODE_AVERTI = 2 # Il s’agit d’avertissements retournés lors d’un fonctionnement normal de Nangu.
CODE_ANOMAL = 3 # Il s’agit d’anomalies retournés lors d’un fonctionnement normal de Nangu.
CODE_ERREUR = 4 # Il s’agit d’anomalies de fonctionnement de Nangu.
CODE_LICENC = 5 # Il s’agit d’un problème de licence d’utilisation de Nangu.

FT_CRI = 4
FT_BLO = 3
FT_MAJ = 2
FT_MIN = 1
FT_NQ  = 0

varNANGU=os.getenv("NANGU",None)
if varNANGU is None:
  print("La variable d'environnement NANGU n'est pas définie !")

#-------------------------------------------------------------------------------
# https://www.techno-science.net/glossaire-definition/Villes-du-Senegal-page-2.html
def VERSION():
  version="23.12.05-Lambaye"
  versPRJ="21.05.23"
  versCFG="21.05.23"
  return version,versPRJ,versCFG

#-------------------------------------------------------------------------------
def sys_stdout_encoding():
  return sys.stdout.encoding

#-------------------------------------------------------------------------------
def SRV_listeKeysDict(dictionnaire):
  liste=dictionnaire.keys()
  liste=sorted(liste)
  return liste

#-------------------------------------------------------------------------------
def SRV_dateOuAuto(date,projet):
  if projet.debug==2:
    return "29/02/2016"
  elif date.find("ajdh")==0:
    t=datetime.datetime.now()
    jours=0
    if date.find('+')!=-1:
      jours=int(date.split('+')[1])
    t=t+datetime.timedelta(days=jours)
    date=projet.lexique.date(t)
  return date

#-------------------------------------------------------------------------------
def styleTexte(debug,style):
  stRet=(style[-1]=='r')
  style=style.split('*')
  stDeb=''
  stFin=''
  stCoul=''
  if len(style[0])!=0:
    stDeb=style[0][0]
    stFin=style[0][1]
  if len(style)==2:
    stCoul=''
    if debug!=2:
      stCoul=FGREEN+SBRIGHT
      if len(style[1])>0:
        if   style[1][0]=="W":
          stCoul=FYELLOW+SBRIGHT
        elif style[1][0]=="N":
          stCoul=FWHITE+SNORMAL
        elif style[1][0]=="O":
          stCoul=FWHITE+SBRIGHT
        elif style[1][0]=="I":
          stCoul=FYELLOW+SNORMAL
        elif style[1][0]=="E":
          stCoul=FRED+SBRIGHT
        elif style[1][0]=="R":
          stCoul=FCYAN+SBRIGHT
  return stDeb,stFin,stCoul,stRet

#-------------------------------------------------------------------------------
## Affichage d'un texte sur une ligne avec formatage sur LONGUEUR caracteres
# @param self : l'instance de la classe
# @param pTexte : texte a afficher
def SRV_afficher_texte(ui,debug,pTexte,style="*r",espacement=None,longueur=LONGUEUR,parallel=False):
  if parallel: return
  if ui:
    SRV_Terminal(ui,debug,None,pTexte.encode(sys.stdout.encoding),style=style,espacement=espacement,parallel=parallel)
  else:
    stDeb,stFin,stCoul,stRet=styleTexte(debug,style)
    pTexte="%s %s %s"%(stCoul,stDeb,pTexte)
    if longueur==0: longueur=len(pTexte)
    n=longueur
    while len(pTexte)-len(stCoul)>n:
      SRV_Print("%s%s %s"%(stCoul,pTexte[:n+len(stCoul)],stFin))
      pTexte="%s %s     %s"%(stCoul,stDeb,pTexte[n+len(stCoul):])
    while len(pTexte)-len(stCoul)<n:
      pTexte=pTexte+" "
    SRV_Print("%s%s %s"%(stCoul,pTexte,stFin),end=stRet)

#---------------------------------------------------------------------------------
## Affichage d'un texte sur une ligne avec formatage sur LONGUEUR caracteres
# @param self : l'instance de la classe
# @param pTexte : texte a afficher
def SRV_afficher_separateur(ui,debfin,elem='=',longueur=LONGUEUR):
  if not ui:
    n=longueur
    pTexte=" "+debfin
    while len(pTexte)<=n:
      pTexte=pTexte+elem
    SRV_Print(pTexte+debfin)

#---------------------------------------------------------------------------------
## Affichage d'un texte centre sur une ligne avec formatage sur LONGUEUR caracteres
# @param self : l'instance de la classe
# @param pTexte : texte a afficher
def SRV_afficher_centrer_texte(ui,debug,pTexte,style="*r",espacement=None,longueur=LONGUEUR,parallel=False):
  if parallel: return
  if ui:
    SRV_Terminal(ui,debug,None,pTexte.encode(sys.stdout.encoding),style=style,espacement=espacement,parallel=parallel)
  else:
    stDeb,stFin,stCoul,stRet=styleTexte(debug,style)
    if longueur<4: longueur=4
    n=longueur-2
    if len(pTexte)%2!=0: pTexte=pTexte+" "
    while len(pTexte)<n:
      pTexte=" "+pTexte+" "
    SRV_Print("%s %s%s %s"%(stCoul,stDeb,pTexte,stFin),end=stRet)

#---------------------------------------------------------------------------------
## Affichage d'un texte centre sur une ligne avec formatage sur LONGUEUR caracteres
# @param self : l'instance de la classe
# @param pTexte : texte a afficher
def SRV_afficher_droite_texte(ui,debug,pTexte,style,espacement=None,longueur=LONGUEUR,debordement=False,parallel=False):
  if parallel:return
  if ui:
    SRV_Terminal(ui,debug,None,pTexte,style=style,espacement=espacement,parallel=parallel)
  else:
    stDeb,stFin,stCoul,stRet=styleTexte(debug,style)
    n=longueur-2
    while len(pTexte)<n:
      pTexte=" "+pTexte
    SRV_Print("%s %s %s %s"%(stCoul,stDeb,pTexte,stFin),end=stRet)

#-------------------------------------------------------------------------------
#def SRV_CharDet(e,aff=False):
#  if aff: print "CHARDET",chardet.detect(e),
#  codage=chardet.detect(e)['encoding']
#  if aff: print e.decode(codage)
#  return e.decode(codage)

#-------------------------------------------------------------------------------
def SRV_codeResultatFR(res,constantes):
    if res==-constantes['STATUS_OK']:
      return "correct","Correct (absent)>","OK","OK","corrects",''
    elif res==constantes['STATUS_OK']:
      return "correct","Correct","OK","OK","corrects",''
    elif res==constantes['STATUS_PB']:
      return "incorrect","Incorrect","NOK","Non OK","incorrects","E"
    elif res==constantes['STATUS_AV']:
      return "avalider","A valider","AV","A valider","à valider","W"
    elif res==constantes['STATUS_NT']:
      return "nonteste","Non Testé","NT","Non testé","non effectués","I"
    elif res==constantes['STATUS_AR']:
      return "avecreserve","Avec Réserve","AR","Avec Réserve","avec réserve","R"
    else:
      SRV_Print("Code resultat %d inconnu"%res)
    return '','','','','',''
#-------------------------------------------------------------------------------
def SRV_codeResultatEN(res,constantes):
    if res==-constantes['STATUS_OK']:
      return "correct","Correct (absent)>","OK","OK","correct",''
    elif res==constantes['STATUS_OK']:
      return "correct","Correct","OK","OK","Correct",''
    elif res==constantes['STATUS_PB']:
      return "incorrect","Incorrect","NOK","Not OK","incorrect","E"
    elif res==constantes['STATUS_AV']:
      return "tobevalidated","To be Validated","AV","To be validated","to be validated","W"
    elif res==constantes['STATUS_NT']:
      return "nottested","Not tested","NT","Not tested","not done","I"
    elif res==constantes['STATUS_AR']:
      return "withreservation","With Reservation","AR","With reservation","with reservation","R"
    else:
      SRV_Print("Code resultat %d inconnu"%res)
    return '','','','','',''
#-------------------------------------------------------------------------------
def SRV_codeResultatNO(res,constantes):
    return "TODO","TODO","TODO","TODO","TODO"
#-------------------------------------------------------------------------------
def SRV_codeResultat(res,constantes,langue):
    if langue == 'FR' :
        return SRV_codeResultatFR(res,constantes)
    elif langue == 'EN' :
        return SRV_codeResultatEN(res,constantes)
    elif langue == 'NO' :
        return SRV_codeResultatNO(res,constantes)
    else:
        SRV_Print("lexique non disponible pour la langue %s. Lexique français appliqué."%langue)
        return SRV_codeResultatFR(res,constantes)
#-------------------------------------------------------------------------------
def SRV_resultatCode(code,constantes):
    if code=="OK":
       return constantes['STATUS_OK']
    elif code=="NOK":
       return constantes['STATUS_PB']
    elif code=="AV":
       return constantes['STATUS_AV']
    elif code=="NT":
       return constantes['STATUS_NT']
    elif code=="AR":
       return constantes['STATUS_AR']
    else:
       SRV_Print("Code resultat %s inconnu"%code)
    return -constantes['STATUS_OK']

#-------------------------------------------------------------------------------
def SRV_imageResultat(res,constantes):
    if res==constantes['STATUS_AV']:
       return "orange"
    elif res==constantes['STATUS_OK']:
       return "vert"
    elif res==constantes['STATUS_PB']:
       return "rouge"
    elif res==constantes['STATUS_AR']:
       return "violet"
    else:
       return "noir"
#-------------------------------------------------------------------------------
def SRV_styleResultat(res,constantes):
    if res==constantes['STATUS_AV']:
       return "<font color=yellow>%s</font><br>"
    elif res==constantes['STATUS_OK']:
       return "<font color=lime>%s</font><br>"
    elif res==constantes['STATUS_PB']:
       return "<font color=red>%s</font><br>"
    elif res==constantes['STATUS_AR']:
       return "<font color=violet>%s</font><br>"
    else:
       return "<font color=black>%s</font><br>"

#---------------------------------------------------------------------------------
def SRV_changePermission(path,perm=0o775,recursif=True):
  try:
    os.chmod(path,perm)
  except:
    return False
  if recursif:
    for file_ in os.listdir(path):
      filePath=os.path.join(path,file_)
      if os.path.isdir(filePath):
        return SRV_changePermission(filePath)
      else:
        try:
          os.chmod(filePath,perm)
        except:
          return False
  return True

#-------------------------------------------------------------------------------
def SRV_copier(source,destination,timeout=10,signaler=False):
  """ Copie le fichier apres destruction
  """
#  print "#%d# SRV_copier ZZzz..%s vers %s"%(timeout,source,destination)
  if os.access(destination, os.F_OK):
    SRV_detruire(destination,timeout,signaler)
  if os.access(source, os.F_OK):
    encore=True
    while encore:
      try:
        shutil.copyfile(source,destination)
        encore=False
      except:
        timeout=timeout-1
#        print "#%d# SRV_copier ZZzz.."%timeout
        if timeout>=0:
          time.sleep(1)
        else:
          encore=False
  if signaler==True and timeout<0:
    SRV_Print("Le fichier "+source+" n'existe pas.")
  return not (timeout<0)

#---------------------------------------------------------------------------------
def SRV_copierRepert(src,dst,force=True):
  names=os.listdir(src)
  if not os.path.exists(dst): os.makedirs(dst)
  for name in names:
    srcname=os.path.join(src,name)
    dstname=os.path.join(dst,name)
    if os.path.isdir(srcname):
      SRV_copierRepert(srcname,dstname,force)
    else:
      if force or not os.path.exists(dstname): shutil.copy(srcname,dstname)
      else:
        (ok,diff,err)=filecmp.cmpfiles(src, dst, [name])
        if len(ok)==0:
          print("Conflit :",srcname,dstname)

#---------------------------------------------------------------------------------
def SRV_remplaceDsFichier(fichier,cherche,remplace):
  contenu=SRV_LireContenu(fichier)
  n=contenu.find(cherche)
  contenu=contenu.replace(cherche,remplace)
  f=open(fichier,"w",encoding='utf-8')
  f.write(contenu)
  f.close()
  return n

#-------------------------------------------------------------------------------
def SRV_detruire(fichier,timeout=10,signaler=False):
  """ Detruit le fichier
  """
#  print "#%d# SRV_detruire ZZzz..%s"%(timeout,fichier)
  noidx=SRV_retraitIndexation(fichier)
  if os.access(noidx,os.W_OK):
    encore=True
    while encore:
      try:
        os.remove(noidx)
        encore=False
      except:
        timeout=timeout-1
#        print "#%d# SRV_detruire ZZzz.."%timeout
        if timeout>=0:
          time.sleep(1)
        else:
          encore=False
  if signaler==True and timeout<0:
    print("Le fichier "+noidx+" n'existe pas.")
  return not (timeout<0)

#-------------------------------------------------------------------------------
def SRV_acceder(fichier,mode,timeout=10,signaler=False):
  """ Accéder au fichier
  """
#  print "#%d# SRV_acceder ZZzz..%s"%(timeout,fichier)
  encore=True
  while encore:
    if os.access(fichier,mode):
      encore=False
    else:
      timeout=timeout-1
#      print "#%d# SRV_acceder ZZzz.."%timeout
      if timeout>=0:
        time.sleep(1)
      else:
        encore=False
  if signaler==True and timeout<0:
    print("Le fichier "+fichier+" n'est pas accessible.")
  return not (timeout<0)

#-------------------------------------------------------------------------------
def SRV_deplacer(fichier,destination,timeout=10,signaler=False):
  """ Deplace le fichier
  """
#  print "#%d# SRV_deplace ZZzz..%s"%(timeout,fichier)
  if os.access(fichier,os.W_OK):
    encore=True
    while encore:
      try:
        shutil.move(fichier,destination)
        encore=False
      except:
        timeout=timeout-1
#        print "#%d# SRV_deplace ZZzz.."%timeout
        if timeout>=0:
          time.sleep(1)
        else:
          encore=False
  else:
    timeout=-1
  if signaler==True and timeout<0:
    print("Le fichier "+fichier+" ne peut pas etre deplace vers "+destination+".")
  return not (timeout<0)

#-------------------------------------------------------------------------------
def SRV_retraitIndexation(chaine):
  noidx=chaine
  idxdeb=noidx.find("MAGIDX:")
  while idxdeb!=-1:
    idxfin=noidx.find(":IDX:")
    aidx=noidx[idxdeb+7:idxfin]
    labidx=noidx.find(":IDXMAG")
    lidx=noidx[idxfin+5:labidx]
    noidx=noidx.replace("MAGIDX:"+aidx+":IDX:"+lidx+":IDXMAG",aidx)
    idxdeb=noidx.find("MAGIDX:")
  return noidx

#-------------------------------------------------------------------------------
def SRV_creerPath(repert,timeout=10,signaler=False):
    """ Cree le repertoire
    """
    noidx=SRV_retraitIndexation(repert)
    encore=True
    while encore:
      try:
        if not os.path.isdir(noidx):
          os.makedirs(noidx)
        encore=False
      except:
        timeout=timeout-1
#        print "#%d# SRV_detruire ZZzz.."%timeout
        if timeout>=0:
          time.sleep(1)
        else:
          encore=False
    if signaler==True and timeout<0:
      print("Le repertoire "+noidx+" ne peut pas etre cree.")
    return not (timeout<0)

#-------------------------------------------------------------------------------
def SRV_detruirePath(repert,timeout=10,signaler=False):
  """ Detruit le repertoire
  """
  noidx=SRV_retraitIndexation(repert)
  if os.access(noidx,os.F_OK):
    encore=True
    while encore:
      try:
        if os.path.islink(noidx):
          os.remove(noidx)
        else:
          os.rmdir(noidx)
        encore=False
      except:
        timeout=timeout-1
#        print "#%d# SRV_detruire ZZzz.."%timeout
        if timeout>=0:
          time.sleep(1)
        else:
          encore=False
  if signaler==True and timeout<0:
    print("Le repertoire "+noidx+" n'existe pas.")
  return not (timeout<0)

#-------------------------------------------------------------------------------
def SRV_razPath(repert,timeout=10,signaler=False,creer=True):
    """ Vide le répertoire ou le crée
    """
    noidx=SRV_retraitIndexation(repert)
    res=True
    if not os.path.exists(noidx):
      if creer: os.makedirs(noidx)
    else:
      for elem in glob.glob(os.path.join(noidx,'*')):
        if os.path.isdir(elem):
          if SRV_razPath(elem,timeout,signaler,False):
            if not SRV_detruirePath(elem,timeout,signaler):
              return False
          else:
            return False
        elif os.access(elem, os.F_OK):
          if not SRV_detruire(elem,timeout,signaler):
            return False
      for elem in glob.glob(os.path.join(noidx,'*.*')):
        if os.access(elem, os.F_OK):
          if not SRV_detruire(elem,timeout,signaler):
            return False
    return res

#-------------------------------------------------------------------------------
def SRV_def_controle(res,vf):
    controle=''
    if res==os.F_OK:
      if vf:
        controle="Existence"
      else:
        controle="Absence"
    elif res==os.R_OK:
      if vf:
        controle="Accès en lecture"
      else:
        controle="Accès impossible"
    elif res==os.W_OK:
      if vf:
        controle="Accès en écriture"
      else:
        controle="Accès impossible"
    elif res==os.X_OK:
      if vf:
        controle="Exécutable"
      else:
        controle="Non exécutable"
    return controle

#-------------------------------------------------------------------------------
def SRV_AttenteOperateur(ui,debug,message,attente,style="r",espacement=10):
 # if ui:
 #   if message!=" ":
 #     SRV_Terminal(ui,None,message,style=style,espacement=espacement)
 #   if attente=="CLAV_ENTREE":
 #     ui.gbxCommentaire.setVisible(False)
 #     ui.btnEntree.setVisible(True)
##      SRV_Terminal(ui,None,"EntreeEventLoop",style="W")
 #     l=ui.btnEntreeEventLoop.exec_()
 #     return l
 #   if attente=="SAISIE":
 #     ui.gbxCommentaire.setVisible(False)
 #     ui.btnEntree.setVisible(False)
 #     ui.lneEditTerminal.setVisible(True)
 #     ui.lblIci.setVisible(True)
 #     ui.lneEditTerminal.clear()
 #     ui.lneEditTerminal.setFocus()
##      SRV_Terminal(ui,None,"EditTerminalEventLoop",style="W")
 #     l=ui.lneEditTerminalEventLoop.exec_()
 #     return str(ui.lneEditTerminal.text())
 #   elif attente=="VALIDATION":
 #     ui.lneRemarque.clear()
 #     ui.lneCommentaire.clear()
 #     ui.gbxCommentaire.setVisible(True)
 #     l=-1
 #     while l==-1:
##        SRV_Terminal(ui,None,"ValiderEventLoop",style="W")
 #       l=ui.btnValiderEventLoop.exec_()
 #     if ui.radOK.isChecked():
 #       validation=("o",'',ui.lneRemarque.toPlainText())
 #     elif ui.radNT.isChecked():
 #       validation=("n",'',ui.lneRemarque.toPlainText())
 #     elif ui.radStop.isChecked():
 #       validation=("t",'',ui.lneRemarque.toPlainText())
 #     elif ui.radKO.isChecked():
 #       validation=("k",ui.lneCommentaire.toPlainText(),ui.lneRemarque.toPlainText())
 #     return validation
 #   elif attente=="CONFIRMATION":
 #     ui.lneRemarque.clear()
 #     ui.lneCommentaire.clear()
 #     ui.gbxCommentaire.setVisible(True)
 #     l=-1
 #     while l==-1:
##        SRV_Terminal(ui,None,"ValiderEventLoop",style="W")
 #       l=ui.btnValiderEventLoop.exec_()
 #     if ui.radOK.isChecked():
 #       validation=("o",'',ui.lneRemarque.toPlainText())
 #     elif ui.radNT.isChecked():
 #       validation=("n",'',ui.lneRemarque.toPlainText())
 #     elif ui.radStop.isChecked():
 #       validation=("t",'',ui.lneRemarque.toPlainText())
 #     elif ui.radKO.isChecked():
 #       validation=("k",ui.lneCommentaire.toPlainText(),ui.lneRemarque.toPlainText())
 #     return validation
 # else:
    n=espacement
    while n>0:
      message=" "+message
      n-=1
#    print "DBG>",message
#    print type(message),sys.stdout.encoding
    couleur=''
    if debug!=2 : couleur=FWHITE
    reponse=input(couleur+message)
    if debug==2 :
      print(" ([DEBUG] Valeur entree : [%s])"%str(reponse))
    if attente=="OUINON":
      while not reponse.lower() in ["n","o"]:
        reponse=input("Veuillez répondre par (O/N)")
        if debug==2 :
          print(" ([DEBUG] Valeur entree : [%s])"%str(reponse))
    if attente=="YESNO":
      while not reponse.lower() in ["n","y"]:
        reponse=input("Please answer by (Y/N)")
        if debug==2 :
          print(" ([DEBUG] Valeur entree : [%s])"%str(reponse))
    return reponse

#-------------------------------------------------------------------------------
#def SRV_Image(ui,fichiers):
#  fichiers=fichiers.split(" ")
#  for fichier in fichiers:
#    w_vue=ui.vue.width()-2
#    h_vue=ui.vue.height()-2
##    print "[%s]"%fichier
#    current_image=QtGui.QImage(fichier)
#    pixmap=QtGui.QPixmap.fromImage(current_image.scaled(w_vue, h_vue,QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation))
#    w_pix=pixmap.width()
#    h_pix=pixmap.height()
#    scene=QtGui.QGraphicsScene()
#    scene.setSceneRect(0, 0, w_pix, h_pix)
#    scene.addPixmap(pixmap)
#    ui.vue.setScene(scene)
#  ui.dockImage.show()

#-------------------------------------------------------------------------------
def SRV_Print(chaine,end=True):
  if end: print(chaine)
  else: print(chaine,end='')

#-------------------------------------------------------------------------------continu=False,
def SRV_Terminal(terminal,debug,fichier,chaine,echo=True,style="*",espacement=None,parallel=False):
  if parallel: return
  if sys.stdout.encoding is None:
    echo=False
  if echo:
    nbsp=''
    if espacement:
      nb=0
      while nb<=espacement:
        nbsp=nbsp+" "
        nb+=1
    if terminal:
      couleur=QtGui.QColor("#FFE1AA")
      if   style[0]=="W": #  Warning
        couleur=QtGui.QColor("#4D658D")
      elif style[0]=="O": # Bold
        couleur=QtGui.QColor("#55AA55")
      elif style[0]=="I": # Italic
        couleur=QtGui.QColor("#805A15")
      elif style[0]=="E": # Error
        couleur=QtGui.QColor("#D46A6A")
      elif style[0]=="R": # Error
        couleur=QtGui.QColor("#D46A6A")
      terminal.moveCursor(QtGui.QTextCursor.End)
      terminal.setTextColor(couleur)
      if style[-1]=="r":
        terminal.insertPlainText(nbsp+chaine+'\n')
      else:
        terminal.insertPlainText(nbsp+chaine)
      terminal.repaint()
    else:
      couleur=''
      if debug!=2:
        couleur=FGREEN+SBRIGHT
        if   style[0]=="W":
          couleur=FYELLOW+SBRIGHT
        elif style[0]=="O":
          couleur=FWHITE+SBRIGHT
        elif style[0]=="I":
          couleur=FYELLOW+SNORMAL
        elif style[0]=="E":
          couleur=FRED+SBRIGHT
        elif style[0]=="R":
          couleur=FCYAN+SBRIGHT
      if style[-1]=="r":
        SRV_Print(couleur+nbsp+chaine)
      else:
        SRV_Print(couleur+nbsp+chaine,end=False)
  if fichier:
    fichier.write(chaine+'\n')

#-------------------------------------------------------------------------------
def SRV_traiterExigences(exigences,exigencesglobales):
  sdl=False
  if exigencesglobales.find("\\n")==0:
    sdl=True
    exigencesglobales=exigencesglobales.replace("\\n",'')
  exigences=exigencesglobales+","+exigences
  while exigences.find(",,")!=-1:
    exigences=exigences.replace(",,",",")

  lstch=exigences[:len(exigences)-1].split(',')
  lstch.sort()
  ch_exigences=''
  for e in lstch:
    if ch_exigences.find(e)==-1:
      ch_exigences=ch_exigences+","+e

  if sdl:
    ch_exigences="\\n"+ch_exigences[1:]
  else:
    ch_exigences=ch_exigences[1:]
  return ch_exigences

#-----------------------------------------------------------------------------------
def SRV_TraiterJoin(chaine):
  if chaine.find('..')!=-1:
    chaine=SRV_retraitIndexation(chaine)
  chaine=chaine.replace('(x86)','*x86*')
  while chaine.find('join(')!=-1:
    joindre=chaine[chaine.find('join('):]
    joindre=joindre[joindre.find('(')+1:joindre.find(')')]
    listJoindre=joindre.split(',')
    path=''
    for addpath in listJoindre:
      addpath=addpath.replace('"','')
      path=os.path.normpath(os.path.join(path,addpath))
    chaine=chaine.replace('join('+joindre+')',path)
  chaine=chaine.replace('*x86*','(x86)')
  return chaine

#-----------------------------------------------------------------------------------
def SRV_Variables(chaine,variables):
  #print "SRV>",chaine,variables
  remplacement=True
  if chaine:
    while remplacement:
      remplacement=False
      for var in variables:
        nom='['+var['NOM']+']'
        val=var['VALEUR']
#        print "DBG>",type(chaine),chaine
#        print "DBG>",type(nom),nom
        while chaine.find(str(nom))!=-1:
          if var['INDEXER']:
            if var['INDEX'] is not None:
              chaine=chaine.replace(nom,"MAGIDX:"+val+":IDX:"+var['INDEX']+":IDXMAG")
            else:
              chaine=chaine.replace(nom,"MAGIDX:"+val+":IDX:"+val+":IDXMAG")
          else:
            if val:
              chaine=chaine.replace(nom,val)
            else:
              chaine=chaine.replace(nom,"indefini")
          remplacement=True

    chaine=SRV_TraiterJoin(chaine)
  return chaine

#-----------------------------------------------------------------------------------
def SRV_EssaiEncodage(chaine,encodage,message=''):
  try:
    echaine=chaine.decode(encodage)
#    print "Encodage :",encodage
  except :
    echaine=None
  return echaine

#-----------------------------------------------------------------------------------
def SRV_Encodage(chaine,message=''):
  return chaine
#  if isinstance(chaine,unicode):
#    return chaine
#  try:
#    echaine=unicode(chaine)
#  except:
#    echaine=SRV_EssaiEncodage(chaine,locale.getdefaultlocale()[1],message)
#    if echaine==None:
#      echaine=SRV_EssaiEncodage(chaine,sys.getdefaultencoding(),message)
#      if echaine==None:
#        echaine=SRV_EssaiEncodage(chaine,sys.stdout.encoding,message)
#        if echaine==None:
#          echaine=SRV_EssaiEncodage(chaine,"iso-8859-1",message) # Western Europe
#          if echaine==None:
#            echaine=SRV_EssaiEncodage(chaine,"cp1252",message) # Western Europe
#            if echaine==None:
#              echaine=SRV_EssaiEncodage(chaine,"cp850",message) # Western Europe
#              if echaine==None:
#                SRV_Print("ERREUR d'encodage %s"%message)
#                echaine="encodage impossible"
#                SRV_Print(chaine)
#  return echaine

#-------------------------------------------------------------------------------
# Fonction de formatage des chaines pour sortie rapport
#-------------------------------------------------------------------------------
def SRV_FormateChaineASCII(objet):
  if objet is None:
    return objet
  if type(objet)!=str:
    chaine=str(objet)
  else:
    chaine=objet

  codage=locale.getdefaultlocale()[1]
#  chaine=chaine.replace(unicode('\xc3\xa9',codage),"e")
#  chaine=chaine.replace(unicode('\xc9','iso-8859-1'),'E')
#  chaine=chaine.replace(unicode('\xe0','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe1','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe2','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe3','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe4','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe5','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe6','iso-8859-1'),'a')
#  chaine=chaine.replace(unicode('\xe7','iso-8859-1'),'c')
#  chaine=chaine.replace(unicode('\xe8','iso-8859-1'),'e')
#  chaine=chaine.replace(unicode('\xe9','iso-8859-1'),'e')
#  chaine=chaine.replace(unicode('\xea','iso-8859-1'),'e')
#  chaine=chaine.replace(unicode('\xeb','iso-8859-1'),'e')
#  chaine=chaine.replace(unicode('\xec','iso-8859-1'),'i')
#  chaine=chaine.replace(unicode('\xed','iso-8859-1'),'i')
#  chaine=chaine.replace(unicode('\xee','iso-8859-1'),'i')
#  chaine=chaine.replace(unicode('\xef','iso-8859-1'),'i')
#  chaine=chaine.replace(unicode('\xf0','iso-8859-1'),'e')
#  chaine=chaine.replace(unicode('\xf1','iso-8859-1'),'n')
#  chaine=chaine.replace(unicode('\xf2','iso-8859-1'),'o')
#  chaine=chaine.replace(unicode('\xf3','iso-8859-1'),'o')
#  chaine=chaine.replace(unicode('\xf4','iso-8859-1'),'o')
#  chaine=chaine.replace(unicode('\xf5','iso-8859-1'),'o')
#  chaine=chaine.replace(unicode('\xf6','iso-8859-1'),'o')
  return chaine

#-----------------------------------------------------------------------------------
def SRV_LireContenu(fichier,defaut='',encodage="utf-8"):
  contenu=defaut
  if os.access(fichier, os.F_OK):
    f=open(fichier,"r",encoding='utf-8')
    contenu=f.read()
    f.close()
  return contenu
#-----------------------------------------------------------------------------------
def SRV_LireContenuListe(fichier,carcomment=None,encodage="utf-8"):
  contenuNoComment=SRV_LireContenu(fichier,None)
  if contenuNoComment is None:
    return []
  contenuNoComment=contenuNoComment.replace('\r','').split('\n')
  if contenuNoComment[-1]=='':
    contenuNoComment=contenuNoComment[:-1]
  if carcomment is None:
    contenu=contenuNoComment
  else:
    contenu=[]
    for ligne in contenuNoComment:
      if ligne[0]!=carcomment:
        contenu.append(ligne)
  return contenu
#-----------------------------------------------------------------------------------
#def tabCodage(codage):
#  print "[%s]"%codage
#  if "ASCII" in codage: codage="ascii"
#  elif codage=="ISO-8859 text": codage="latin_1"
#  else: codage="utf_8"
#  print codage
#  return codage
#-----------------------------------------------------------------------------------
#def SRV_decodeMagic(chaine,codage=None):
#  return chaine
#  if codage==None:
#    if sys.platform=="win32":
#      m=magic.Magic(magic_file=os.getenv('MAGIC_FILE','.'),mime_encoding=True)
#    else:
##      m=magic.Magic(mime_encoding=True)
#      m=magic.Magic(flags=magic.MAGIC_MIME_ENCODING)
##    codage=m.from_buffer(chaine)
#    codage=m.id_buffer(chaine)
#  if codage=="binary": return chaine
#  if codage=="data": return chaine
  #if codage=='ps database from kernel a': print "\nDBG-m agic>",chaine,codage,tabCodage(codage)
#  return chaine.decode(tabCodage(codage))

#-----------------------------------------------------------------------------------
def SRV_RemplacerVariable(chaine,listeVariables):
  if chaine:
    if chaine.find("[")!=-1 and chaine.find("]")!=-1:
      for (opestring,valstring,_) in listeVariables:
        if chaine.find(opestring)!=-1:
           chaine=chaine.replace(opestring,valstring)
  return chaine

#-----------------------------------------------------------------------------------
def SRV_CleanOperateur(listeVariables,chaine,execution):
  if chaine is not None:
#    print
#    print "CLOP>",chaine
    for (opestring,valstring,contstring) in listeVariables:
      if valstring is not None:
        opestring=opestring.replace(']','~')
        while chaine.find(opestring)!=-1:
          op1=chaine.find(opestring)
          op2=op1+len(opestring)
          op3=chaine.find("]",op2)
          if execution in ['R','C'] or not contstring:
            chaine=chaine[:op1]+chaine[op2:op3]+chaine[op3+1:]
          else:
            chaine=chaine[:op1]+'<'+chaine[op1+1:op1+len(opestring)-1]+'>'+chaine[op3+1:]
    if execution in ['R','C']:
      chaine=SRV_Validateur(chaine,execution) # Calcule les opérations {{  }}
  chaine=SRV_RemplacerVariable(chaine,listeVariables)
#  print "CLOP<",chaine
  return chaine

#---------------------------------------------------------------------------------
def SRV_Validateur(chaine,execution='R'):
#  print
#  print "CHX>",chaine
#  print
  resChaine=chaine
  while chaine.find("{{")!=-1:
    op1=chaine.find("{{")
    op2=op1+2
    op3=chaine.find("}}",op2)
    try:
#~      print chaine
      #[:op1],chaineEval,chaine[op3+2:]
      chaineEval=str(eval(chaine[op2:op3]))
      chaine=chaine[:op1]+chaineEval+chaine[op3+2:]
    except:
      if execution!='C':
        chaineEval="Impossible d'évaluer '%s'"%chaine[op2:op3]
        chaine="ATTENTION : ERREUR DE SYNTAXE EVENTUELLE ! \n"+chaine[:op1]+chaineEval+chaine[op3+2:]
      else:
        chaine=chaine[:op1]+chaine[op2:op3]+chaine[op3+2:]
        #print(resChaine)
  return chaine

#---------------------------------------------------------------------------------
def SRV_retraitIHMimage(chaine,pathImage=True):
  chaineRet=''
  idxDeb=chaine.find("[IHM:")
  while idxDeb!=-1:
    idxfin=chaine.find(":MHI]")
    image=chaine[idxDeb+5:idxfin]
    if not pathImage :
      image="(voir image : %s)"%os.path.basename(image)
    chaineRet+=chaine[:idxDeb]+image
    chaine=chaine[idxfin+5:]
    idxDeb=chaine.find("[IHM:")
  chaineRet+=chaine
  return chaineRet

#---------------------------------------------------------------------------------
def SRV_ecrireFichierRemarque(fiche,indexAction,remarque,fichier_remarque,nbRemarque,ficheCourante):
  (_,lafiche,_,_,_,_,_)=fiche
  baselafiche=os.path.basename(lafiche)
  if os.path.isfile(fichier_remarque):
    if nbRemarque==0:
      mode='w'
    else :
      mode='a'
  else:
    mode='w'
  f=open(fichier_remarque,mode,encoding='utf-8')
  if ficheCourante!=baselafiche:
    if nbRemarque!=0:
      f.write("[/ol]\n")
    f.write("[b]Fiche %s[/b]\n"%baselafiche)
    f.write("[ol]\n")
  f.write("[li][goto=%s#%s]%s[/goto] : %s[/li]\n"%(lafiche.replace('/','_').replace('\\','_'),indexAction,indexAction,remarque.encode('utf-8')))
  f.close()
  return nbRemarque+1,baselafiche

#---------------------------------------------------------------------------------
def SRV_IndexVariable(nom,variables,avert=True):
  if variables is None:
    return -1
  for v in range(len(variables)):
    if variables[v]['NOM']==nom:
      return v
  if avert:
    SRV_Print("Variable '%s' inconnue"%nom)
  return -1

#---------------------------------------------------------------------------------
def SRV_listeVar(variables):
  liste=[]
  for v in variables:
    liste.append(('['+v['NOM']+']',v['VALEUR'],v['CONTEXTUEL']))
  return liste

#---------------------------------------------------------------------------------
def SRV_Condition(condition,listConditions):
#  print "DBG>",condition
  et=condition.find(')ET(')
  ou=condition.find(')OU(')
  if et!=-1:
    C1=condition[:et+1]
    C2=condition[et+len(')ET'):]
    return SRV_Condition(C1, listConditions) and SRV_Condition(C2, listConditions)
  elif ou!=-1:
    C1=condition[:ou+1]
    C2=condition[ou+len(')OU'):]
    return SRV_Condition(C1, listConditions) or SRV_Condition(C2, listConditions)
  else:
    lancer=True
    condition=condition.replace('(','').replace(')','')
    condition=condition.split('=')
    if condition[0] not in ["TOP","CHRONO"]:
      ID=condition[0].replace('[','').replace(']','')
      if ID in listConditions:
        if listConditions[ID]!=condition[1]:
          lancer=False
      elif ID[-1]=='!' and ID[:-1] in listConditions:
        if listConditions[ID[:-1]]==condition[1]:
          lancer=False
      else:
        if condition[0][len(condition[0])-1]=='!':
          lancer=condition[0][:len(condition[0])-1]!=condition[1]
        elif condition[0][len(condition[0])-1]=='<':
          lancer=condition[0][:len(condition[0])-1]<=condition[1]
        elif condition[1][0]=='>':
          lancer=condition[0]>=condition[1][1:]
        else:
          lancer=condition[0]==condition[1]
    return lancer

#---------------------------------------------------------------------------------
def SRV_ReduireCondition(condition,listConditions):
  et=condition.find(')ET(')
  ou=condition.find(')OU(')
  if et!=-1:
    C1=condition[:et+1]
    C2=condition[et+len(')ET'):]
    red1=SRV_ReduireCondition(C1,listConditions)
    red2=SRV_ReduireCondition(C2,listConditions)
    if red1=="F" or red2=="F":
      return "F"
    elif red1=="T" and red2=="T":
      return "T"
    elif red1=="T":
      return red2
    elif red2=="T":
      return red1
    return "(%s)ET(%s)"%(red1,red2)
  elif ou!=-1:
    C1=condition[:ou+1]
    C2=condition[ou+len(')OU'):]
    red1=SRV_ReduireCondition(C1,listConditions)
    red2=SRV_ReduireCondition(C2,listConditions)
    if red1=="T" or red2=="T":
      return "T"
    elif red1=="F" and red2=="F":
      return "F"
    elif red1=="F":
      return red2
    elif red2=="F":
      return red1
    return "(%s)OU(%s)"%(red1,red2)
  else:
    lancer=condition
    condition=condition.replace('(','').replace(')','')
    condition=condition.split('=')
    if condition[0] not in ["TOP","CHRONO"]:
#      print "PDN>COND",condition
#      if condition[1] in ["OK","NOK","AV","NT"]:
#        condition[1]=SRV_resultatCode(condition[1])
      ID=condition[0].replace('[','').replace(']','')
#      print "PDN>ID",ID
      if ID in listConditions:
#        print "PDN>EV",  listConditions[ID],condition[1]
#        if listConditions[ID]!=condition[1]:
#          lancer='F'
        lancer='T' # Debloquer quelle que soit la valeur !!!
      else:
        #if listConditions[condition[0]]!=condition[1]:
        #  lancer=condition
      #else:
        if condition[0][len(condition[0])-1]=='!':
          if condition[0][:len(condition[0])-1]!=condition[1]:
            lancer="T"
          else:
            lancer="F"
        elif condition[0][len(condition[0])-1]=='<':
          if condition[0][:len(condition[0])-1]<=condition[1]:
            lancer="T"
          else:
            lancer="F"
        elif condition[1][0]=='>':
          if condition[0]>=condition[1][1:]:
            lancer="T"
          else:
            lancer="F"
        else:
          if condition[0]==condition[1]:
            lancer="T"
          else:
            lancer="F"
    return lancer

#---------------------------------------------------------------------------------
def SRV_EditerTxt(fichier,activer=True):
  if activer:
    if os.getenv("EDIT","-aucun éditeur-")=="-aucun éditeur-":
      print("Variable d'environnement 'EDIT', non définie !")
      editeur="echo"
    else:
      editeur=os.getenv("EDIT","-aucun éditeur-")
    os.system("%s %s"%(editeur,fichier))

#---------------------------------------------------------------------------------
def SRV_ZipFicrep(ficrep,archive,ficzip):
  ficreps=os.listdir(ficrep)
  for p in ficreps:
    p=os.path.join(ficrep,p)
    if os.path.isdir(p):
      SRV_ZipFicrep(p,archive,ficzip)
    elif os.path.basename(p)!=os.path.basename(ficzip):
      SRV_Print("archivage de %s"%p)
      archive.write(p)
  return

#---------------------------------------------------------------------------------
def SRV_Zip(ficrep,ficzip):
  ici=os.getcwd()
  os.chdir(ficrep)
  archive=zipfile.ZipFile(ficzip,"w",zipfile.ZIP_DEFLATED)
  if os.path.isdir('.'):
    SRV_ZipFicrep('.',archive,ficzip)
  else:
    archive.write(ficrep)
  archive.close()
  os.chdir(ici)

  return "Compression de \""+ficrep+"\" reussie !"

#-------------------------------------------------------------------------------
def process_exit_status(sta):
    if sys.platform!="win32" or sys.platform=="mingw32":
#      sta=sta >> 8
      if sta>127:
        sta=sta-256
    return sta

#-----------------------------------------------------------------------------------
def SRV_Execute(pCommande,pMode,pMessage=None,pStatut=None):
#  print ">"+pCommande
  if pMessage:
    print(pMessage)
  proc=subprocess.Popen(pCommande,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
  (out,err)=proc.communicate()
  sta=process_exit_status(proc.returncode)
#  print()
#  print("DBG-sta>",sta)
#  print("DBG-out>",out)
#  print("DBG-err>",err)
  sortie=out+err
  sortie=sortie.rstrip()
#  if len(sortie)>1 and sortie[-1]==b'\r\n':
#    sortie=sortie[:-2]
#  if len(sortie)>0 and sortie[-1]==b'\n':
#    sortie=sortie[:-1]
#  print("DBG-sortie>",sortie)
  try:
    sortie=sortie.decode("utf-8")
  except:
    sortie=str(sortie)
  return sta,sortie

#---------------------------------------------------------------------------------
def print_ncattr(nc_fid,key):
  try:
      print("\t\ttype:", repr(nc_fid.variables[key].dtype))
      for ncattr in nc_fid.variables[key].ncattrs():
          print('\t\t%s:' % ncattr,repr(nc_fid.variables[key].getncattr(ncattr)))
  except KeyError:
      print("\t\tWARNING: %s does not contain variable attributes" % key)

#---------------------------------------------------------------------------------
def ncdump(nc_fid,champ,verb):
  # NetCDF global attributes
  nc_attrs = nc_fid.ncattrs()
  if verb:
      print("NetCDF Global Attributes:")
      for nc_attr in nc_attrs:
          print('\t%s:' % nc_attr, repr(nc_fid.getncattr(nc_attr)))
  nc_dims = [dim for dim in nc_fid.dimensions]  # list of nc dimensions
  # Dimension shape information.
  if verb:
      print("NetCDF dimension information:")
      for dim in nc_dims:
          print("\tName:", dim )
          print("\t\tsize:", len(nc_fid.dimensions[dim]))
          print_ncattr(nc_fid,dim)
  # Variable information.
  nc_vars=[]
  if champ=='V' and len(nc_dims)==0:
    return nc_fid.getValue()
  elif champ!='V':
      nc_vars = [var for var in nc_fid.variables]  # list of nc variables
      if verb:
          print("NetCDF variable information:")
          for var in nc_vars:
              if var not in nc_dims:
                  print('\tName:', var)
                  print("\t\tdimensions:", nc_fid.variables[var].dimensions)
                  print("\t\tsize:", nc_fid.variables[var].size)
                  print_ncattr(nc_fid,var)
  return nc_attrs, nc_dims, nc_vars
#----------------------------------------------------------------
def SRV_recursNCF(ncf,pListParam,pChamp):
  if pListParam==[]:
    return ncdump(ncf,pChamp,False)
  if pListParam[0][0]=='A':
    if pListParam[0][2:] in ncf.ncattrs():
      return ncf.getncattr(pListParam[0][2:])
    else: return "'%s' non trouvé"% pListParam[0][2:]
  elif pListParam[0][0]=='G':
    if pListParam[0][2:] in ncf.groups:
      return SRV_recursNCF(ncf.groups[pListParam[0][2:]],pListParam[1:],pListParam[0][0])
    else: return "'%s' non trouvé"% pListParam[0][2:]
  elif pListParam[0][0]=='V':
    if pListParam[0][2:] in ncf.variables:
      return SRV_recursNCF(ncf.variables[pListParam[0][2:]],pListParam[1:],pListParam[0][0])
    else: return "'%s' non trouvé"% pListParam[0][2:]
#----------------------------------------------------------------

