#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------


import os, copy, glob

import nngsrc.services
import nngsrc.nng

#-----------------------------------------------------------------------------------

class Campagne:
  repCampagne=None
  nomCampagne=''
  idCampagne=''
  iterCampagne=0
  cahiersCampagne=[]

  #---------------------------------------------------------------------------------
  def CAMPlire(self):
    contenu=nngsrc.services.SRV_LireContenuListe(os.path.join(self.repCampagne,self.ficCampagne))

    self.nomCampagne=contenu[0].split(' ')[1]
    self.idCampagne=contenu[1].split(' ')[1]
    self.iterCampagne=int(contenu[2].split(' ')[1])
    self.cahiersCampagne=[]
    for cC in range(len(contenu)-3):
      ligne=contenu[cC+3].split(' ')
      dicoCahier= {"cahier": ligne[0], "select": ligne[1], "statut": ligne[2], "nbExgCouv": int(ligne[3])}
      self.cahiersCampagne.append(dicoCahier)

  #---------------------------------------------------------------------------------
  def CAMPecrire(self,fichier=None):
    if fichier is None:
      fichier=os.path.join(self.repCampagne,self.ficCampagne)
    f=open(fichier,"w",encoding='utf-8')
    f.write("SOUS-TITRE %s\n"%self.nomCampagne)
    f.write("SOUS-REFERENCE %s\n"%self.idCampagne)
    f.write("ITERATION %d\n"%self.iterCampagne)
    for cahier in self.cahiersCampagne:
      f.write("%s"%cahier["cahier"])
      f.write(" %s"%cahier["select"])
      f.write(" %s"%cahier["statut"])
      f.write(" %s"%cahier["nbExgCouv"])
      f.write("\n")
    f.close()

  #---------------------------------------------------------------------------------
  def CAMPinitier(self,nng,mode):
    for cahier in range(len(self.cahiersCampagne)):
      pval=self.cahiersCampagne[cahier]["cahier"]
      if pval!="planValidation":
        fichier=os.path.join(nng.repPlans,"%s.pv"%pval)
        if os.access(fichier,os.F_OK):
          contenu=nngsrc.services.SRV_LireContenuListe(os.path.join(nng.repPlans,"%s.pv"%pval))
          fCSV=nng.projet.REFERENCE_IDENTIFIANT.replace("{MODE}",'X')
          fCSV+=nng.projet.SEPARATEUR_CHAMPS+self.idCampagne
          fCSV+=contenu[1].split(' ')[1]
          fCSV+=nng.projet.SEPARATEUR_CHAMPS+nng.projet.DOCUMENT_EDITION+nng.projet.SEPARATEUR_CHAMPS+nng.projet.DOCUMENT_REVISION
          fCSV+="%s%04d"%(nng.projet.SEPARATEUR_CHAMPS,int(nng.projet.DOCUMENT_ITERATION))

          fichier=os.path.join(nng.repRapport,'X','CSV',fCSV,"%s.csv"%fCSV)
          if os.path.exists(fichier):
            contenu=nngsrc.services.SRV_LireContenuListe(fichier)
            nbExgCouv=int(contenu[5].split(',')[1])
          else:
            nbExgCouv=0
          self.cahiersCampagne[cahier]["nbExgCouv"]=nbExgCouv
          if mode!="maj":
            self.cahiersCampagne[cahier]["statut"]="NT"
    self.CAMPecrire()
    return nngsrc.services.CODE_OK

  #---------------------------------------------------------------------------------
  def ordonneCahiersCampagne(self):
    cahiers=[]
    for cahier in self.cahiersCampagne:
      cahiers.append(cahier['cahier'])
    cahiers.sort()
#    print ("DBG>",cahiers)
    copie=copy.deepcopy(self.cahiersCampagne)
    self.cahiersCampagne=[]
    for cahier in cahiers:
      for cahierCampagne in copie:
        if cahier==cahierCampagne['cahier']:
          self.cahiersCampagne.append(cahierCampagne)

  #---------------------------------------------------------------------------------
  def __init__(self,repertoire,repPlans,projet,fichier,mode=None,nomCampagne="nomCampagne",idCampagne="idCampagne"):
    self.repCampagne=repertoire
    if not os.access(self.repCampagne,os.F_OK):
      os.mkdir(self.repCampagne)
    self.ficCampagne=fichier
    if mode!="init" and os.access(os.path.join(repertoire,self.ficCampagne),os.F_OK):
      self.CAMPlire()
      self.nomCampagne=self.nomCampagne.replace('.','').replace(' ','').replace('_','')
      self.idCampagne=self.idCampagne.replace('.','').replace(' ','').replace('_','')
    elif mode=="init":
      self.cahiersCampagne=[]
      self.nomCampagne=nomCampagne.replace('.','').replace(' ','').replace('_','')
      self.idCampagne=idCampagne.replace('.','').replace(' ','').replace('_','')
    else:
      self.cahiersCampagne=[]
      self.nomCampagne="nomCampagne"
      self.idCampagne="idCampagne"

    listePlans=[]
    for elem in glob.glob(os.path.join(repPlans,'*')):
      _,extension=os.path.splitext(elem)
      if os.path.isdir(elem):
        for elem2 in glob.glob(os.path.join(elem,'*.pv')):
          cahier=elem2.replace(os.path.join(repPlans,''),'').replace('.pv','')
          if cahier[0]!='_': listePlans.append(cahier)
      elif extension=='.pv':
        cahier=elem.replace(os.path.join(repPlans,''),'').replace('.pv','')
        if cahier[0]!='_': listePlans.append(cahier)
    for cahier in listePlans:
      present=False
      for cahierCampagne in self.cahiersCampagne:
        if cahierCampagne["cahier"]==cahier:
          present=True
      if not present:
        dicoCahier= {"cahier": cahier, "select": "_", "statut": "NT", "nbExgCouv": 0}
        self.cahiersCampagne.append(dicoCahier)

    self.ordonneCahiersCampagne()
    self.CAMPecrire()

    # Controle de l'espace de depot et prise de reference pour la campagne
    # Pas de campagne avant la version 9.0
    self.refCampagne=projet.REFERENCE_IDENTIFIANT.replace('{MODE}','B').replace('{VERSION}',self.nomCampagne.upper()).replace('{SOUSREF}',"-V%s"%self.idCampagne)
    if not os.path.isdir(os.path.join(self.repCampagne,self.refCampagne)):
      os.makedirs(os.path.join(self.repCampagne,self.refCampagne))

#-----------------------------------------------------------------------------------
# Fin
