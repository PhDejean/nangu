#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
#  NANGU
#  Copyright (C) 2021 Philippe DEJEAN & MAGELLIUM — Tous droits réservés.
#  
#  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le
#  modifier suivant les termes de la “GNU General Public License” telle que
#  publiée par la Free Software Foundation : soit la version 3 de cette
#  licence, soit (à votre gré) toute version ultérieure.
#  
#  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS
#  AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ
#  ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale
#  Publique GNU pour plus de détails.
#-------------------------------------------------------------------------------

import os,os.path
import nngsrc.services 

try:
  import pyglet
  WAVE=True
except:
  nngsrc.services.SRV_Print(nngsrc.services.FRED+nngsrc.services.SNORMAL+u"RESTRICTION (servicesSons) : Module son 'pyglet' non installe.")
  nngsrc.services.SRV_Print(nngsrc.services.FRED+nngsrc.services.SNORMAL+u"              => Les services de gestion des sons sont desactives.")
  WAVE=False
  MBROLA_PATH=None
else:
  MBROLA_PATH=os.getenv("MBROLA_PATH",None)

#print "MBROLA_PATH :",MBROLA_PATH
#pyglet.lib.load_library('avbin')

#-----------------------------------------------------------------------------------
## Fonction de jeu d'un fichier wave
# @param pFichier : fichier wave à jouer
def SRVson_playWAV(pFichier):
  if WAVE:
    def exiter(dt):
      pyglet.app.exit()

    pyglet.have_avbin = False
    try:
      player = pyglet.media.Player()
    except:
      nngsrc.services.SRV_Print(nngsrc.services.FRED+nngsrc.services.SNORMAL+u"RESTRICTION (servicesSons) : Module son 'pyglet' non appelable. Contrôler.")
    else:
      music = pyglet.media.load(pFichier)
      player.queue(music)
      player.play()
      pyglet.clock.schedule_once(exiter, music.duration)
      pyglet.app.run()  

#-----------------------------------------------------------------------------------
def SRVson_Prononcer(texte,parallel=False):
  if parallel: return
  if MBROLA_PATH:
    os.system('espeak -q -x -v %s --pho --phonout=prononcer.pho "%s"'%(os.path.join("mb","mb-fr4"),texte.encode(nngsrc.services.sys_stdout_encoding())))
    ll=nngsrc.services.SRV_LireContenuListe("prononcer.pho")
    f=open("prononcer.pho","w",encoding='utf-8')
    for l in range(len(ll)):
      if l!=0:
        f.write(ll[l])
    f.close()
    os.system('mbrola -e -C "n n2" %s prononcer.pho prononcer.wav'%os.path.join(MBROLA_PATH,"fr4","fr4"))
    SRVson_playWAV('prononcer.wav')

#-----------------------------------------------------------------------------------
def SRVson_Alarme(SON_FICHIER_ALARME,varNANGU,parallel=False):
  if parallel: return
  if SON_FICHIER_ALARME!='': SRVson_playWAV(SON_FICHIER_ALARME)
  else: SRVson_playWAV(os.path.join(varNANGU,"ressources","alarm.wav"))

#---------------------------------------------------------------------------------
# Fin
