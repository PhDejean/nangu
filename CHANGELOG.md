# Changelog
## 22.03.26-Kabatoki
  * Ajout de l'interface textuelle TUI (beta)

## 21.09.08-Joal
  * Ajout du profilage
  * Fin de retrait de la sortie markdown
  * Correction des ambiguités dans les méthodes IATD
  * Compléments sur les lexiques

## 21.06.19-Gallade
  * Correction de la boucle conditionnelle.

## 21.06.13-Gallade
  * Documentation de l'utilisation des variables d'environnement et surcharge dans les fichiers projet.

## 21.05.29-Fadial
  * Les fonctions à variable sans 'retour attendu' ne sortent plus en NOK.
  * Mise en place des niveaux d'étapes.

## 21.05.23-Fadial
  * Première version en Open Source
